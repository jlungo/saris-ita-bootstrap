<?php ob_start();?>
<?php 
#get connected to the database and verfy current session
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');
# initialise globals
include_once('administratorMenu.php');

# include the header
global $szSection, $szSubSection;
$szSection = 'Manage Users';
$szSubSection = '';
$szTitle = 'Role Management';
include_once('administratorheader.php');
?>


<form action="<?php echo $_SERVER['PHP_SELF']?>" method="get" class="style24">
	<div align="right">
		<span class="style42"><font face="Verdana"><b>Search</b> </font> </span>
		<font color="006699" face="Verdana"><b> <input type="text"
				name="content" size="15"value"">
		</b> </font><font color="#FFFF00" face="Verdana"><b> <input
				type="submit" value="GO" name="go">
		</b> </font>
	</div>
</form>
<?php
if(isset($_GET['content']) ){
	$key=trim($_GET['content']);
	if($key<>''){
		$sql="SELECT FullName, RegNo, UserName, Password, Position, AuthLevel, LastLogin
		FROM security
		WHERE (FullName LIKE '%$key%' OR UserName LIKE '%$key%' OR RegNo LIKE '%$key%') AND Module=1 ORDER BY FullName";
	}else{
		$sql = "SELECT FullName, RegNo, UserName, Password, Position, AuthLevel, LastLogin
		FROM security WHERE Module=1 ORDER BY FullName";
	}

	$result=$zalongwa->query($sql) or die($zalongwa->connect_error);
	$totalRows_query = $result->num_rows;
	/* Printing Results in html */
	if ($result->num_rows > 0){
		echo "<p>Total Records Found: $totalRows_query </p>";
		echo "<table border='1'>";
		echo "<tr><td> S/No </td><td> Name </td><td> RegNo </td><td> UserName </td><td> Position </td><td> Last Login</td><td>Action</td></tr>";
		$i=1;
		while($row = $result->fetch_array()) {
			$login = stripslashes($row["UserName"]);
			$Name = stripslashes($row["FullName"]);
			$RegNo = stripslashes($row["RegNo"]);
			$pwd = stripslashes($row["Password"]);
			$position = stripslashes($row["Position"]);
			$last = stripslashes($row["LastLogin"]);
			echo "<tr><td>$i</td>";
			echo "<td>$Name</td>";
			echo "<td>$RegNo</td>";
			echo "<td>$login</td>";
			echo "<td>$position</td>";
			echo "<td>$last</td>";
			echo "<td><a href=\"blockrole.php?login=$login&block=1\">Roles</a></td>";
			$i=$i+1;
		}
		echo "</table>";
	}else{
		$key= stripslashes($key);
		echo "Sorry, No Records Found <br>";
		echo "That Match With Your Searck Key \"$key \" ";
	}
}
if(isset($_GET['block'])){
	$username=$_GET['login'];
	$sql="SELECT FullName, RegNo, UserName, Password, Position, AuthLevel, LastLogin
	FROM security
	WHERE UserName='$username'";
	$roleblockedsql="SELECT role FROM blockedrole WHERE username='$username'";
	$roleblockedresult=$zalongwa->query($roleblockedsql);
	$roleblocked=array();
	$j=1;
	while($roleblockedrow=$roleblockedresult->fetch_array()){
		$roleblocked[$j]=$roleblockedrow['role'];
		$j++;
	}
	$result=$zalongwa->query($sql) or die($zalongwa->connect_error);
	if ($result->num_rows > 0){
		echo "<p>Uncheck to block/Check to unblock role to <b>$username</b></p>";
		echo '<form name="role" method="post">';
		echo "<table border='0'><tr>";
		$sqlrole="SELECT * FROM userrole";
		$resultrole=$zalongwa->query($sqlrole);
		$i=0;
		while($rowrole=$resultrole->fetch_array()){

			$checked=(in_array($rowrole['role'], $roleblocked))?' ':' checked="checked"';
			$onclick=(in_array($rowrole['role'], $roleblocked))?" onclick=\"unblockrole('".$username."','".$rowrole['role']."')\"":" onclick=\"blockrole('".$username."','".$rowrole['role']."')\"";
			$onchange=(in_array($rowrole['role'], $roleblocked)) ? " onchange=\"confirmdelete('Do You want to unblock?')\"" : "onchange=\"confirmdelete('Do You want to block?')\"";
			$break=($i%3==0)?'</tr><tr>':'';
			echo $break;
			?>
<td><input type="checkbox" name="task[]"
	value="<?php echo $rowrole['role'];?>" <?php echo $checked.$onclick;?> />
	<?php echo $rowrole['role'];?>
</td>

<?php 
$i++;
		}
		?>
</tr>
<tr>
<td></td>
	<td><div id="sms"></div></td>
</tr>



<?php
echo "</table></form>";
	}



}
?>
<?php
echo '<br/><br/><br/>';
# include the footer
include('../footer/footer.php');
?>
<?php ob_end_flush() ?>
