<?php

require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

# include the header
include('administratorMenu.php');

$szSection = 'Communication';
$szSubSection = 'Check Message';
$szTitle = 'User Communication';

include('administratorheader.php');


$maxRows_studentsuggestion = 1;
$pageNum_studentsuggestion = 0;

if (isset($_GET['pageNum_studentsuggestion'])) {
    $pageNum_studentsuggestion = $_GET['pageNum_studentsuggestion'];
}

$startRow_studentsuggestion = $pageNum_studentsuggestion * $maxRows_studentsuggestion;

$colname_studentsuggestion = "1";

if (isset($_COOKIE['RegNo'])) {
    $colname_studentsuggestion = (get_magic_quotes_gpc()) ? $_COOKIE['RegNo'] : addslashes($_COOKIE['RegNo']);
}

//mysql_select_db($database_zalongwa, $zalongwa);
$query_studentsuggestion = "SELECT id, received, fromid, toid, message,replied FROM suggestion 
							WHERE toid = '$RegNo' or toid = '$username' or toid=4 ORDER BY received DESC";
$query_limit_studentsuggestion = sprintf("%s LIMIT %d, %d", $query_studentsuggestion, $startRow_studentsuggestion, $maxRows_studentsuggestion);

$studentsuggestion = $zalongwa->query($query_limit_studentsuggestion) or die($zalongwa->connect_error);
$row_studentsuggestion = $studentsuggestion->fetch_assoc();

if (isset($_GET['totalRows_studentsuggestion'])) {
    $totalRows_studentsuggestion = $_GET['totalRows_studentsuggestion'];
} else {
    $all_studentsuggestion = $zalongwa->query($query_studentsuggestion);
    $totalRows_studentsuggestion = $all_studentsuggestion->num_rows;
}

$totalPages_studentsuggestion = ceil($totalRows_studentsuggestion / $maxRows_studentsuggestion) - 1;

$queryString_studentsuggestion = "";

if (!empty($_SERVER['QUERY_STRING'])) {
    $params = explode("&", $_SERVER['QUERY_STRING']);
    $newParams = array();

    foreach ($params as $param) {
        if (stristr($param, "pageNum_studentsuggestion") == false && stristr($param, "totalRows_studentsuggestion") == false) {
            array_push($newParams, $param);
        }
    }

    if (count($newParams) != 0) {
        $queryString_studentsuggestion = "&" . htmlentities(implode("&", $newParams));
    }

}

$queryString_studentsuggestion = sprintf("&totalRows_studentsuggestion=%d%s", $totalRows_studentsuggestion, $queryString_studentsuggestion);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="https://cdn.datatables.net/responsive/2.2.0/css/responsive.bootstrap4.min.css">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>

    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <style type="text/css">
        <!--
        .style1 {
            color: #FFFFFF
        }
        th {
            width: 70px;
        }

        .style2 {
            color: #990000
        }

        -->
    </style>


</head>
<body>
<!-- navbar -->
<?php //include '../academic/academicNavBar.php'; ?>

<div class="container-flex">
</div>
<!--<div class="container">-->
    <table class="table table-striped table-bordered table-responsive" width="100%" cellspacing="0">


        <thead class="table-inverse">
           <tr>
               <th>Date</th>
               <th>From</th>
               <th width="60px">Message</th>
               <th>Action</th>
               <th>Comments</th>
           </tr>

     </thead>

        <tbody>
        <?php do { ?>
        <tr>

            <td style="width:20px;border-bottom:1px solid #D0D7E5;"><?php echo $row_studentsuggestion['received']; ?></td>

            <td style="width:20px;border-bottom:1px solid #D0D7E5;"><?php $from = $row_studentsuggestion['fromid'];
                $id = $row_studentsuggestion['id'];
                //select student
                $qstudent = "SELECT Name, RegNo, ProgrammeofStudy from student WHERE RegNo = '$from' ";
                $dbstudent = $zalongwa->query($qstudent) or die("This student has no results" . $zalongwa->connect_error);

                if ($rows = $dbstudent->num_rows != 0) {
                    $row_result = $dbstudent->fetch_array();
                    $name = $row_result['Name'];
                    $regno = $row_result['RegNo'];
                    $degree = $row_result['ProgrammeofStudy'];

                    //get degree name
                    $qdegree = "Select Title from programme where ProgrammeCode = '$degree' ";
                    $dbdegree = $zalongwa->query($qdegree);
                    $row_degree = $dbdegree->fetch_array();
                    $programme = $row_degree['Title'];

                    echo "$name - $regno - $programme";
                } else {
                    $user = $zalongwa->query("SELECT * FROM security WHERE RegNo='$from'");
                    $user = $user->fetch_array();

                    echo "$user[FullName]($user[UserName]) - $user[RegNo]";
                }
                //echo $row_studentsuggestion['fromid'];
                ?></td>
            <!--                <td valign="top">-->
            <!--                    <div align="right">Message:</div>-->
            <!--                 </td>-->
            <td width="100%"><?php echo $row_studentsuggestion['message']; ?></td>

            <!--                <td valign="top">-->
            <!--                    <div align="right" class="style2">Comments:</div>-->
            <!--                </td>-->

            <td><?php echo "<a href=\"admissionSuggestionBox.php?from=$from&id=$id\">Reply Message</a>" ?></td>
            <td><span class="style2"><?php echo $row_studentsuggestion['replied']; ?></span></td>

        </tr>
        </tbody>


        <?php } while ($row_studentsuggestion = $studentsuggestion->fetch_assoc()); ?>
    </table>


    <p>
        <a href="<?php printf("%s?pageNum_studentsuggestion=%d%s", $currentPage, max(0, $pageNum_studentsuggestion - 1), $queryString_studentsuggestion); ?>">Previous</a>
        Message: <?php echo min($startRow_studentsuggestion + $maxRows_studentsuggestion, $totalRows_studentsuggestion) ?>
        of <?php echo $totalRows_studentsuggestion ?> <span class="style64 style1">...</span><a
                href="<?php printf("%s?pageNum_studentsuggestion=%d%s", $currentPage, min($totalPages_studentsuggestion, $pageNum_studentsuggestion + 1), $queryString_studentsuggestion); ?>">Next</a>
        <span class="style64 style1">.......</span>
    </p>
<!--</div>-->
<br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
Modernaizer here check if not svg supported replace with png
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<!--script for datatable-->
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details for ' + data[0] + ' ' + data[1];
                        }
                    }),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            }
        });
    });
</script>
</body>
</html>
