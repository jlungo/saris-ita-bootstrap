<?php
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('administratorMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Manage Users';
	$szSubSection = '';
	$szTitle = 'User Visit Information';
	include('administratorheader.php');
?>
<form action="adminmanageuser.php" method="get" class="style24">
            <div align="right"><span class="style67"><font face="Verdana"><b>Search</b></font></span> 
              <font color="006699" face="Verdana"><b> 
              <input type="text" name="content" size="15">
              </b></font><font color="#FFFF00" face="Verdana"><b> 
              <input type="submit" value="GO" name="go">
              </b></font>            </div>
        </form>
          	<?php
require_once('../Connections/zalongwa.php');
include 'drop_menu_func.php';
include 'add_user_drop_menu.php'; 

#populate module combo box
	$query_module = "SELECT moduleid, modulename FROM modules";
	$module = mysqli_query($zalongwa, $query_module) or die(mysqli_error($zalongwa));
	$row_module = mysqli_fetch_assoc($module);
	$totalRows_module = mysqli_num_rows($module);

#populate privileges combo box
	$query_privilege = "SELECT privilegeID, privilegename FROM privilege";
	$privilege = mysqli_query($zalongwa, $query_privilege) or die(mysqli_error($zalongwa));
	$row_privilege = mysqli_fetch_assoc($privilege);
	$totalRows_privilege = mysqli_num_rows($privilege);

#populate department combo box
	$query_dept = "SELECT DeptID, DeptName FROM department ORDER BY DeptName";
	$dept = mysqli_query($zalongwa, $query_dept) or die(mysqli_error($zalongwa));
	$row_dept = mysqli_fetch_assoc($dept);
	$totalRows_dept = mysqli_num_rows($dept);
#populate faculty combo box
	$query_faculty = "SELECT FacultyID, FacultyName FROM faculty ORDER BY FacultyName";
	$faculty = mysqli_query($zalongwa, $query_faculty) or die(mysqli_error($zalongwa));
	$row_faculty = mysqli_fetch_assoc($faculty);
	$totalRows_faculty = mysqli_num_rows($faculty);
	 
$login=$_GET['login'];
$sql = "SELECT * FROM security WHERE UserName='$login' ORDER BY FullName";

$result = @mysqli_query($zalongwa, $sql) or die("Cannot query the database.<br>" . mysqli_error($zalongwa));
$query = @mysqli_query($zalongwa, $sql) or die("Cannot query the database.<br>" . mysqli_error($zalongwa));

//$all_query = mysqli_query($zalongwa, $query);
$totalRows_query = mysqli_num_rows($query);
/* Printing Results in html */
if (mysqli_num_rows($query) > 0){
while($result = mysqli_fetch_array($query)) {
		$login = stripslashes($result["UserName"]);
		$Name = stripslashes($result["FullName"]);
		$RegNo = stripslashes($result["RegNo"]);
		$currentprivilege = stripslashes($result["PrivilegeID"]);
		$currentmodule = stripslashes($result["Module"]);
		$currentfaculty = stripslashes($result["Faculty"]);
		$currentdept = stripslashes($result["Dept"]);
		$FacultyDean=$result["FacultyDean"];
		$DeptHead=$result["DeptHead"];
		$position = stripslashes($result["Position"]);
		$sqlpos="SELECT * FROM tbl_position WHERE Name='$position'";
		$resultpos=mysqli_query($zalongwa, $sqlpos);
		$rowpos=mysqli_fetch_array($resultpos);
		$posId=$rowpos['Id'];
		
		$last = stripslashes($result["LastLogin"]);
	}
}else{
$key= stripslashes($key);
echo "Sorry, No Records Found <br>";
echo "That Match With Your Search Key \"$key \" ";
}

#Update database values
if (isset($_POST['action']) && ($_POST['action'] == "Update"))
	{
		$position=(isset($_POST['position'])) ? addslashes($_POST['position']) : "";
		$module=addslashes($_POST['module']);
		$login=addslashes($_GET['login']);
		$dept=(isset($_POST['dept'])) ? addslashes($_POST['dept']) : '';
		$faculty=(isset($_POST['faculty'])) ? addslashes($_POST['faculty']) : '';
		if($module==1){
			if($position==1 || $position==2  || $position==3){
			 $priv=2;
			   if($position==1){
				  $FacultyDean=1;
				  $DeptHead=0;
				  }elseif($position==2){
				$FacultyDean=0;
				$DeptHead=1;
					  }else{
					$FacultyDean=0;
				   $DeptHead=0;  
						  }
			}else{
			   $priv=3;
			   $FacultyDean=0;
			   $DeptHead=0;	
				}
			}elseif($module==3){
				$priv=4;
				}elseif($module==6){
					$priv=5;
					}else{
			   $priv=2;
			   $FacultyDean=0;
			   $DeptHead=0;	
				}
			if($module<>3 && $module<>6){	
			$sqlpos="SELECT Name FROM  tbl_position WHERE Id='$position'";
		  $resultpos=mysqli_query($zalongwa, $sqlpos);
		  $rowpos=mysqli_fetch_array($resultpos);
		  $importpos=$rowpos['Name'];
	         }else{
			$importpos= ($module==3) ? 'Student' : 'Blocked';	 
				 }
		  
		  $sql="UPDATE security Set Position='$importpos', Module = '$module', Dept = '$dept', Faculty = '$faculty',
			PrivilegeID='$priv',DeptHead='$DeptHead' WHERE UserName='$login'";
		$result = @mysqli_query($zalongwa, $sql) or die("Cannot query the database.<br>" . mysqli_error($zalongwa));
		echo '<meta http-equiv = "refresh" content ="0; 
		url = adminmanageuser.php?content='.$login.'">';
		exit;
mysqli_free_result($result);
mysqli_close($zalongwa);
	}
	?>
<form action="#" method="post" name="updateuser" id="updateuser">
<table width="200" border="1" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
              <tr>
                <td width="81"><div align="right"><strong>Name:</strong></div></td>
                <td width="109"><?php echo $Name; ?> </td>
              </tr>
              <tr>
                <td><div align="right"><strong>RegNo:</strong></div></td>
                <td><?php echo $RegNo; ?></td>
              </tr>
              <tr>
                <td nowrap><div align="right"><strong>User Name:</strong></div></td>
                <td><?php echo $login; ?>
                  <input name="login" type="hidden" id="login" value="<?php echo $login; ?>"></td>
              </tr>
              <tr>
                <td><div align="right"><strong>Module:</strong></div></td>
                <td><select name="module" id="module" onchange="showPosition(this.value,'1')">
                 <?php dropMenu('moduleid','modulename','modules',$currentmodule)?>
            		<?php
							do {  
									?>
            		<option value="<?php echo $row_module['moduleid']?>"><?php echo $row_module['modulename']?></option>
           				 <?php
								} while ($row_module = mysqli_fetch_assoc($module));
  								$rows = mysqli_num_rows($module);
  								if($rows > 0) {
      								mysqli_data_seek($module, 0);
	  								$row_module = mysqli_fetch_assoc($module);
  								}
						?>
                </select></td>
              </tr>
              <tr>
              <tr id="position">
                <td><div align="right"><strong>Position:</strong></div></td>
                <td><select name="position" id="position1">
				<option value="<?php echo $posId; ?>" selected><?php echo $position; ?></option>
                </select></td>
             <!-- </tr>
                <td><div align="right"><strong>Privilege:</strong></div></td>
                <td><select name="priv" id="priv">
				    <?php dropMenu('privilegeID','privilegename','privilege',$currentprivilege)?>
                </select></td>
              </tr>-->
			   <tr id="fac_dep">
				   <?php
				   if($DeptHead){
				   ?>
                <td><div align="right"><strong>Department:</strong></div></td>
                <td><select name="dept" id="dept">
				   <?php dropMenu('DeptID','DeptName','department',$currentdept)?>
                </select></td>
                <?php
			    }
			    if($FacultyDean){
                ?>
                <td><div align="right"><strong>Faculty:</strong></div></td>
                <td><select name="faculty" id="faculty">
				 <?php dropMenu('FacultyID','FacultyName','faculty',$currentfaculty)?>
                </select></td>
                <?php
			  }                
                ?>
              </tr>
			  <tr>
                <td><div align="right"><strong>Update:</strong></div></td>
                <td><input type="submit" name="action" value="Update"></td>
              </tr>
  </table>
</form>            
            	
<?php

	# include the footer
	include('../footer/footer.php');
?>
