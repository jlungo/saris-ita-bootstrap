<?php 
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('administratorMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Manage Users';
	$szSubSection = 'New User';
	$szTitle = 'List of Database Users';
	include('administratorheader.php');
		
?>

<form action="adminmanageuser.php" method="get" class="style24">
            <div align="right"><span class="style67"><font face="Verdana"><b>Search</b></font></span> 
              <font color="006699" face="Verdana"><b> 
              <input type="text" name="content" size="15">
              </b></font><font color="#FFFF00" face="Verdana"><b> 
              <input type="submit" value="GO" name="go">
              </b></font>            </div>
        </form>
        <?php
 if(isset($_POST['Submit']) && $_POST['Submit'] !=""){
		$today = date("F j, Y");   
	    $radio = addslashes($_POST['radiobutton']);
		$LastName = addslashes($_POST['txtLastName']);
		$FirstName = addslashes($_POST['txtFirstName']);
		$byear = addslashes($_POST['txtYear']);
		$bmon = addslashes($_POST['txtMonth']);
		$bday = addslashes($_POST['txtDay']);
		$id = addslashes($_POST['txtRegNo']);
		$selectPosition = ($radio==1) ? 'Student':($_POST['selectPosition']);
		$selectModule = $_POST['selectModule'];
		$selectFaculty = (isset($_POST['selectFaculty'])) ? $_POST['selectFaculty'] : '';
		$selectDepartment = (isset($_POST['selectDepartment'])) ? $_POST['selectDepartment'] :'';
		$username = addslashes($_POST['txtLogin']);
		if($radio <> 1){ 
		$PWD = addslashes($_POST['txtPWD']);
		$fullname = $LastName . ", " . $FirstName;
	    }
		$Login=$username.$PWD;
		$Email = addslashes($_POST['txtEmail']);
		$bdate = $bday . " - " . $bmon . " - " . $byear;
		if($radio==1) {
			#check if reg for student exist
			$sqlstud="SELECT * FROM student WHERE RegNo='$id' LIMIT 1";
			$resultstud=$zalongwa->query($sqlstud);
			$numstud=$resultstud->num_rows;
			if($numstud<>1){
				    $stud_create=1;
					$_SESSION['loginerror'] = $id. ' - is Invalid Reg No! ';
  		     //echo '<meta http-equiv = "refresh" content ="0; url = registration.php">'
				}else{
					$rowstud=$resultstud->fetch_array();
					$Name=$rowstud['Name'];
					$fullname = $Name;
					$Name=explode(',',$Name);
					$LastName=trim($Name[0]);
					$PWD=strtoupper($LastName);
					}
			}
		if($selectPosition==''){
			$_SESSION['loginerror'] = 'Position can not be empty';
			}

		if(strlen($bdate)<>14){
		$_SESSION['loginerror'] = $bdate. ' - is Invalid Date of Birth! ';
  		//echo '<meta http-equiv = "refresh" content ="0; url = registration.php">';
		}

		#check username
		if (!preg_match("@^(([A-Za-z0-9!#$%&*+/=?^_`{|}~-][A-Za-z0-9!#$%&*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$@", $username)){
		$_SESSION['loginerror'] = $username.' - is a Bad Username! ';
  		//echo '<meta http-equiv = "refresh" content ="0; url = registration.php">';
		 }

		#check password
		if($radio<>1){
		if (!preg_match("@^(([A-Za-z0-9!#$%&*+/=?^_`{|}~-][A-Za-z0-9!#$%&*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/@", $PWD)){
		$_SESSION['loginerror'] = 'Alphanumeric characters are not allowed,Bad Password format! ';
  		//echo '<meta http-equiv = "refresh" content ="0; url = registration.php">';
		 }
	    }
		#check if use has submitted valid email address
		function check_email_address($Email) {
		  // First, we check that there's one @ symbol, and that the lengths are right
		  if (!preg_match("/[^@]{1,64}@[^@]{1,255}/", $Email)) {
			// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			return false;
	  }
	  // Split it into sections to make life easier
	  $email_array = explode("@", $Email);
	  $local_array = explode(".", $email_array[0]);
	  for ($i = 0; $i < sizeof($local_array); $i++) {
		 if (!preg_match("@^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$@", $local_array[$i])) {
		  return false;
		}
	  }
	  if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
		$domain_array = explode(".", $email_array[1]);
		if (sizeof($domain_array) < 2) {
			return false; // Not enough parts to domain
		}
		for ($i = 0; $i < sizeof($domain_array); $i++) {
		  if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
			return false;
		  }
		}
		  }
		  return true;
		}

		if($Email<>''){
				if (check_email_address($Email)) {
				  //echo $Email . ' is a valid email address. <br>';
					} else {
				$_SESSION['loginerror'] = $Email . '==> is not a valid email address! ';
				//echo '<meta http-equiv = "refresh" content ="0; url = registration.php">';
				}
	}
	# check if user has created valid username and password

	if(strlen($id)<1){
	$_SESSION['loginerror'] = 'Invalid Identity Registration Number! ';
	//echo '<meta http-equiv = "refresh" content ="0; url = registration.php">';
	}
	if(strlen($username)<5){
	$_SESSION['loginerror'] = 'User Name too short, User Name must be at least 5 characters! ';
	//echo '<meta http-equiv = "refresh" content ="0; url = registration.php">';
	}
	if(strlen($PWD)<5 && $radio <>1){
	$_SESSION['loginerror'] = 'Password too short, Password must be at least 5 characters! ';
	//echo '<meta http-equiv = "refresh" content ="0; url = registration.php">';
	}
	// search for existing users

	   #check if username exist
	  $sql ="SELECT UserName			
			  FROM security WHERE UserName='$username'";
	   $result = $zalongwa->query($sql);
	   $usernameFound = $result->num_rows;
	   $rawusername = $result->fetch_array();
	   if ($usernameFound>0) {
			$login     = $rawusername['UserName'];
					$_SESSION['loginerror'] = " Registration NOT successful! <br> Some one is already using this USERNAME: '".$username."'
											<br>Please Select Another Username";
					//echo '<meta http-equiv = "refresh" content ="0; url = registration.php">';
			}
	   #check if regno exist
	   $sql ="SELECT RegNo  			
			  FROM security WHERE RegNo = '$id'";
	   $result = $zalongwa->query($sql);
	   $noFound = $result->num_rows;
       $rawRegNo = $result->fetch_array();
	   if ($noFound>0) {
			$userregno = $rawRegNo['userregno'];
					$_SESSION['loginerror'] = "Registration NOT Successful! <br>
											  Re-registration is not allowed in ZALONGWA DATABASE<br>
											  There is already a user using this RegNo: ".$id."<br>
											  Please Sign in with your username and password!";
					echo '<meta http-equiv = "refresh" content ="0; url = adminadduser.php">';
					exit;
			}


		#if no error captured register the candidate
		if ($_SESSION['loginerror']==""){
			// Generate SSHA jlungo hash
			$hash = "{jlungo-hash}" . base64_encode(pack("H*", sha1($PWD)));

			//create account
			if($radio==1){
				$stud_create=1;
				  $query = "INSERT INTO security (UserName, Password, FullName, RegNo, Position, AuthLevel, Email,DOB, LastLogin, Registered)
					 VALUES ('$username', '$hash', '$fullname', '$id', '$selectPosition', 'user', '$Email','$bdate', now(), now())";
				}else{
			 if($selectModule==1){
			 if($selectPosition==1 || $selectPosition==2 || $selectPosition==3){
			  $selectPrivilege=2;
			  if($selectPosition==1){
				  $FacultyDean=1;
				  $DeptHead=0;
				  }elseif($selectPosition==2){
					  $FacultyDean=0;
				      $DeptHead=1;
					  }else{
				   $FacultyDean=0;
				   $DeptHead=0;
					  }
		  }else{
			   $selectPrivilege=3;
			   $FacultyDean=0;
				$DeptHead=0;
		   }
		     }else{
				   $selectPrivilege=2;
				   $FacultyDean=0;
				   $DeptHead=0;
				 }
           $sqlpos="SELECT Name FROM  tbl_position WHERE Id='$selectPosition'";
		   $resultpos=$zalongwa->query($sqlpos);
		   $rowpos=$resultpos->fetch_array();
		  $importpos=$rowpos['Name'];
			  $query = "INSERT INTO security (UserName, Password, FullName, RegNo, Position, AuthLevel,Module,PrivilegeID,Dept,Faculty, Email,DOB, LastLogin, Registered,DeptHead)
					 VALUES ('$username', '$hash', '$fullname', '$id', '$importpos', 'user','$selectModule','$selectPrivilege','$selectDepartment','$selectFaculty', '$Email','$bdate', now(), now(),$DeptHead)";
				 }
			$result = $zalongwa->query($query) or die("Query Failed<br>");
			if($result){
				echo '<meta http-equiv = "refresh" content ="0; url = adminadduser.php?success=1">';
					exit;
				}
		}
}
?>
<SCRIPT ID=clientEventHandlersJS LANGUAGE=javascript>
<!--
function fmAdd_onsubmit() {
	if(fmAdd.radiobutton[0].value !='1'){
if (fmAdd.txtLastName.value == "" || fmAdd.txtFirstName.value == ""  || fmAdd.txtLogin.value=="" || fmAdd.txtPWD.value=="" || fmAdd.txtRePWD.value=="")
	{window.alert("ZALONGWA System Asks You to Fill in the Blank Text Fields");
	return false;
	}
	 if (fmAdd.txtPWD.value != fmAdd.txtRePWD.value){
		window.alert("Password Texts donot Match, Enter them again, ZALONGWA");
		return false;
	}
}
}
//-->
</SCRIPT>
<script src="css/zalongwa.js" type="text/javascript"> </script>
<body onLoad="f_setfocus();">
<style type="text/css">
<!--
.style2 {color: #FFFFCC}
.style3 {color: #FFFFFF}
.style4 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.style5 {color: #000000}
-->
</style>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align=center><br>
	  <br>
		<!-- Registration Form Starts -->
		<table border="0" cellspacing="0" cellpadding="0" width=540 style="border:2px solid rgb(119,119,119)">
		<tr>
			<td align=left>
			
			
			<table border="0" cellspacing="0" cellpadding='0' width='100%' background="themes/images/loginTopHeaderBg.gif">
			<tr>
				<td align=left>&nbsp;</td>
				<td align=right><!--img src="themes/images/loginTopVersion.gif"--></td>
			</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding='6' width='100%'>
			<tr>
				<td align=left valign=top class=small style="padding:10px">
					<!-- Sign in box -->
					<div align="center"><br>
				  USER REGISTRATION FORM</div>
					<FORM action="<?php echo $_SERVER['PHP_SELF']?>" method=post enctype="application/x-www-form-urlencoded" name=fmAdd id=fmAdd onsubmit="return fmAdd_onsubmit()" LANGUAGE=javascript>
       <TABLE width="100%" BORDER=0 align="center" CELLPADDING=0 CELLSPACING=0 bordercolor="#006600" bgcolor="#cccccc">
							<?php
							if ($_SESSION['loginerror']!="")
							{
							?>
							<tr>
								<td colspan="6"><b class="small">
									<font color="Brown">
								<div align="center"></div><?php echo $_SESSION['loginerror'];$_SESSION['loginerror']='';?></div>
								</font>
								</b>
								</td>
							</tr>
							<?php
							}elseif(isset($_GET['success'])){
							?>
							<tr>
								<td colspan="6"><b class="small">
									<font color="green">
								<div align="center"></div><?php echo "Created successfully!!<br/>"; ?></div>
								</font>
								</b>
								</td>
							</tr>
								<?php
							}
								?>
							   <TR align="center">
                            <TD>
								Student
                          <input type="radio" name="radiobutton" value="1" id="fby"/>
                             </TD>
                             <TD>&nbsp;&nbsp;&nbsp;</TD>
                             <TD id='fbytd'>
						   Staff		 
                          <input type="radio" name="radiobutton" value="0" id="fby2"/>
                          </TD>
                            <?php if(!$stud_create) {?>  
                          </TR>
							<TR class="hideit">
                            <TD VALIGN=MIDDLE ALIGN=RIGHT colspan="4" height="28" nowrap <?php echo ($missingLastname)?'style=" color:#990000"':'';?>><div align="right" class="large"><font color="#0000CC">LAST NAME:</font></div></TD>
                            <TD colspan="2" ALIGN=LEFT VALIGN=MIDDLE><div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000">
                                  <INPUT TYPE="text" SIZE="29" name="txtLastName" value="<?php echo((isset($_POST["txtLastName"]))?$_POST["txtLastName"]:"") ?>">
</font><font color="#000000"><span class="large style4"><font color="#0000CC">(LASTNAME)</font></span> </font></div></TD>
                            
                          </TR>
                          <TR class="hideit">
                            <TD VALIGN=MIDDLE ALIGN=RIGHT colspan="4" height="28" nowrap><div align="right" class="large"><font color="#0000CC">FIRST NAME: </font></div></TD>
                            <TD colspan="2" ALIGN=LEFT VALIGN=MIDDLE><div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000">
                                  <INPUT TYPE="text" SIZE="29" name="txtFirstName" value="<?php echo((isset($_POST["txtFirstName"]))?$_POST["txtFirstName"]:"") ?>">
</font><font color="#000000"><span class="large style4"><font color="#0000CC">(Firstname)</font></span> </font></div></TD>
                          </TR>
                            <?php }?> 
						    <TR>
                            <TD VALIGN=MIDDLE ALIGN=RIGHT colspan="4" height="28" nowrap><div align="right" class="large"><font color="#0000CC">DATE OF BIRTH: </font></div></TD>
                            <TD colspan="2" ALIGN=LEFT VALIGN=MIDDLE nowrap><div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000">
                            <select name="txtDay" id="select">
							<option value="<?php echo((isset($_POST["txtDay"]))?$_POST["txtDay"]:"") ?>"><?php echo((isset($_POST["txtDay"]))?$_POST["txtDay"]:"") ?></option>  
                              <option value=>---</option>
                              <option value="01">01</option>
                              <option value="02">02</option>
                              <option value="03">03</option>
                              <option value="04">04</option>
                              <option value="05">05</option>
                              <option value="06">06</option>
                              <option value="07">07</option>
                              <option value="08">08</option>
                              <option value="09">09</option>
                              <option value="10">10</option>
                              <option value="11">11</option>
                              <option value="12">12</option>
                              <option value="13">13</option>
                              <option value="14">14</option>
                              <option value="15">15</option>
                              <option value="16">16</option>
                              <option value="17">17</option>
                              <option value="18">18</option>
                              <option value="19">19</option>
                              <option value="20">20</option>
                              <option value="21">21</option>
                              <option value="22">22</option>
                              <option value="23">23</option>
                              <option value="24">24</option>
                              <option value="25">25</option>
                              <option value="26">26</option>
                              <option value="27">27</option>
                              <option value="28">28</option>
                              <option value="29">29</option>
                              <option value="30">30</option>
                              <option value="31">31</option>
                            </select>
                            <select name="txtMonth" id="txtMonth">
							<option value="<?php echo((isset($_POST["txtMonth"]))?$_POST["txtMonth"]:"") ?>"><?php echo((isset($_POST["txtMonth"]))?$_POST["txtMonth"]:"") ?></option>  
                              <option value=>-----------</option>
                              <option value="01">January</option>
                              <option value="02">February</option>
                              <option value="03">March</option>
                              <option value="04">April</option>
                              <option value="05">May</option>
                              <option value="06">June</option>
                              <option value="07">July</option>
                              <option value="08">August</option>
                              <option value="09">September</option>
                              <option value="10">October</option>
                              <option value="11">November</option>
                              <option value="12">December</option>
                            </select>
                            <input name="txtYear" type="text" id="txtYear" size="3" maxlength="4" value="<?php echo((isset($_POST["txtYear"]))?$_POST["txtYear"]:"") ?>">
                            </font><font color="#000000"><span class="large style4"><font color="#0000CC">(dd-mm-<font color="#0000CC">yyyy</font>)</font></span> </font></div></TD>
                          </TR>
                          <TR>
                            <TD VALIGN=middle height="28" colspan="4" ALIGN=right>
                            <div align="right" class="large"><font color="#0000CC">ID RegNo: </font></div></TD>
                            <TD colspan="2" ALIGN=LEFT VALIGN=TOP><div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000">
                                  <input name="txtRegNo" type="text" id="txtRegNo" size="29" value="<?php echo((isset($_POST["txtRegNo"]))?$_POST["txtRegNo"]:"") ?>">
                            </font><font color="#0000CC"></font></div></TD>
                          </TR>
                          <!--javascript-->	
<script type="text/javascript">
 $(document).ready(function(){
$('#fby2').hide();	 
$('#fbytd').hide();	
$('#fby').change(function(){
   onchange_val = $(this).val();

   if(onchange_val == 1){
 $('.hideit').hide();
 $('#fby2').show();
 $('#fbytd').show();	
   }else{
	 $('.hideit').show();
           
   }
	
});

$('#fby2').change(function(){

   $('.hideit').show();
   $('#fby2').hide();
   $('#fbytd').hide();		
});
});
</script>
                          
                         <?php if(!$stud_create) {?> 
                          <?php include 'add_user_drop_menu.php'; ?>
                                                              <TR class='hideit'>
                            <TD colspan="4" VALIGN=MIDDLE ALIGN=RIGHT nowrap ><div align="right" class="large"><font color="#0000CC">MODULE:</font></div></TD>
                            <TD colspan="2" ALIGN=LEFT VALIGN=MIDDLE ><div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000">
                                  <select name="selectModule" id="selectModule" onchange="showPosition(this.value,'0')">
							<option value="<?php echo((isset($_POST["selectModule"]))?$_POST["selectModule"]:"") ?>"><?php echo((isset($_POST["selectModule"]))?:"SELECT MODULE") ?></option>  
                                 
                                     <?php addDropMenu('modules','moduleid','modulename');?>
                                   
                                  </select>
                            </font></div></TD>
                          </TR>
                          
                          <TR class='hideit' id="position">
                          </TR>
                          
                                
                                    <TR class='hideit' id="fac_dep">
                           
                          </TR>
                               
                          
                            <!-- <TR class='hideit'>
                            <TD colspan="4" VALIGN=MIDDLE ALIGN=RIGHT nowrap ><div align="right" class="large"><font color="#0000CC">PRIVILEGE:</font></div></TD>
                            <TD colspan="2" ALIGN=LEFT VALIGN=MIDDLE ><div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000">
                                  <select name="selectPrivilege" id="selectPrivilege">
							<option value="<?php echo((isset($_POST["selectPrivilege"]))?$_POST["selectPrivilege"]:"") ?>"><?php echo((isset($_POST["selectPrivilege"]))?$_POST["selectPrivilege"]:"") ?></option>  
                              
                               <?php  addDropMenu('privilege','privilegeID','privilegename')?>
                                  </select>
                            </font></div></TD>
                          </TR>-->
                            <?php } ?>
                            
                          
                          <TR>
                            <TD height="19" colspan="4" ALIGN=RIGHT VALIGN=MIDDLE nowrap><div align="right" class="large"><font color="#0000CC">USERNAME: </font></div></TD>
                            <TD colspan="2" ALIGN=LEFT VALIGN=MIDDLE><div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000">
                                  <input name="txtLogin" type="text" id="txtLogin" size="29" value="<?php echo((isset($_POST["txtLogin"]))?$_POST["txtLogin"]:"") ?>">
</font><font color="#000000"><span class="large style4"><font color="#0000CC">(flastname)</font></span></font></div></TD>
                          </TR>
                          
                           <?php if(!$stud_create) {?>  
                          <TR class='hideit'>
                            <TD height="19" colspan="4" rowspan="2" ALIGN=RIGHT VALIGN=MIDDLE><div align="right" class="large"><font color="#0000CC">PASSWORD:</font></div></TD>
                            <TD colspan="2" rowspan="2" ALIGN=LEFT VALIGN=MIDDLE><div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000">
                                  <input name="txtPWD" type="password" id="txtPWD" size="29" >
                            </font></div></TD>
                            
                          </TR>
                          <TR class='hideit'>
                            
                          </TR>
                          <TR class='hideit'>
                            <TD height="19" colspan="4" rowspan="2" ALIGN=RIGHT VALIGN=MIDDLE nowrap><div align="right" class="large"><font color="#0000CC">RE-ENTER PASSWORD: </font></div></TD>
                            <TD colspan="2" rowspan="2" ALIGN=LEFT VALIGN=MIDDLE><div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000">
                                  <input name="txtRePWD" type="password" id="txtRePWD" size="29" >
                            </font></div></TD>
                           
                          </TR>
                          <?php } ?>
                          <TR>
                           
                          </TR>
                          <TR>
                            <TD colspan="4" rowspan="2"><div align="right"></div>                              <div align="right"></div>                              <div align="right"><font color="#0000CC">EMAIL:</font></div></TD>
                            <TD colspan="2" rowspan="2" ALIGN=LEFT VALIGN=MIDDLE><div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#0000CC">
                                  <input type="text" size="29" name="txtEmail" value="<?php echo((isset($_POST["txtEmail"]))?$_POST["txtEmail"]:"") ?>">
                            </font></div></TD>
                           
                          </TR>
                       
                          <TR>
                            <TD height="19"><div align="right"></div></TD>
                            <TD width="113" align="right" valign="bottom"><div align="right"><span class="style2"></span></div></TD>
                            <TD VALIGN=MIDDLE ALIGN=LEFT ><div align="right"><span class="style2"><span class="style3"><span class="style2"><span class="style2"></span></span></span></span></div></TD>
                            <TD VALIGN=TOP ALIGN=LEFT><div align="right"><span class="style2"><span class="style3"><span class="style2"><span class="style2"></span></span></span></span></div></TD>
                            <TD valign="top" nowrap >                              <div align="left" class="style2">
                                  <div align="center">
                                    <input type="submit" value="Create Account" name="Submit">
                                    <span class="style45 style16"></span>
                                    <input type="reset" value=" Reset" name="Reset">
                                  </div>
                            </div></TD>
                            
                            <TD valign="top" nowrap ><div align="right"></div></TD>
                          </TR>
          </table>
          <input type="hidden" name="MM_insert" value="true">
          </FORM>				
			 </td>
			  </table>
			
			</td>
		</tr>
	  </table>
	
			<!-- Shadow -->
			<table border=0 cellspacing=0 cellpadding=0 width=640>
			<tr>
				<td>&nbsp;</td>
				<td width=100% background="themes/images/loginBottomShadowBg.gif">&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
	  </table>
	</td>
</tr>
</table>
