<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">
            <?php
            $sql_std = "SELECT * FROM student WHERE RegNo='$RegNo'";
            $std = mysqli_query($zalongwa, $sql_std);
            $std_row = mysqli_fetch_array($std);
            $totalRows_std = mysqli_num_rows($std);
            if ($totalRows_std > 0) {
                #get post variables
                $id = $std_row['Id'];
                $reg = $std_row['RegNo'];
                $edit = $_GET['edit'];
                if ($edit == yes) {
                    $state2 = "submit,Save Changes";
                    $state3 = "readonly";
                    $state4 = "";
                    $label_edit = "";
                } else {
                    $state2 = "hidden";
                    $state3 = "readonly";
                    $state4 = "disabled";
                    $label_edit = "<a href='$_SERVER[PHP_SELF]?id=$id&RegNo=$reg&edit=yes' class='btn btn-primary'><i class=\"fa fa-pencil-square\"></i> Click here to make changes</a>";
                }
                $sql = "SELECT * FROM student WHERE Id ='$id' and RegNo='$reg'";
                $update = mysqli_query($zalongwa, $sql);
                $update_row = mysqli_fetch_array($update);
                $totalRows_update = mysqli_num_rows($update);
                $regno = $update_row['RegNo'];
                $stdid = $update_row['Id'];
                $AdmissionNo = $update_row['AdmissionNo'];
                $programofstudy = $update_row['ProgrammeofStudy'];
                $degree = $programofstudy;
                $faculty = $update_row['Faculty'];
                $ayear = $update_row['EntryYear'];
                $combi = $update_row['Subject'];
                $campus = $update_row['Campus'];
                $class = $update_row['Class'];
                $manner = $update_row['MannerofEntry'];
                $rawname = $update_row['Name'];
                $expsurname = explode(",", $rawname);
                $surname = strtoupper($expsurname[0]);
                $othername = $expsurname[1];
                $expothername = explode(" ", $othername);
                $firstname = $expothername[1];
                $middlename = $expothername[2] . ' ' . $expothername[3];
                $dtDOB = $update_row['DBirth'];
                $age = $update_row['age'];
                $sex = $update_row['Sex'];
                $sponsor = $update_row['Sponsor'];
                $nationality = $update_row['Nationality'];
                $country = $update_row['Country'];
                $district = $update_row['District'];
                $region = $update_row['Region'];
                $maritalstatus = $update_row['MaritalStatus'];
                $address = $update_row['Address'];
                $religion = $update_row['Religion'];
                $denomination = $update_row['Denomination'];
                $postaladdress = $update_row['postaladdress'];
                $residenceaddress = addslashes($update_row['residenceaddress']);
                $disabilityCategory = $update_row['disabilityCategory'];
                $status = $update_row['Status'];
                $gyear = $update_row['GradYear'];
                $phone1 = $update_row['Phone'];
                $email1 = $update_row['Email'];
                $formsix = $update_row['form6no'];
                $formfour = $update_row['form4no'];
                $diploma = $update_row['form7no'];
                $School_attended_olevel = $update_row['School_attended_olevel'];
                $School_attended_alevel = $update_row['School_attended_alevel'];
                $name = $surname . ", " . $firstname . " " . $middlename;
                //Added fields
                $account_number = $update_row['account_number'];
                $bank_branch_name = $update_row['bank_branch_name'];
                $bank_name = $update_row['bank_name'];
                $form4no = $update_row['form4no'];
                $form4name = $update_row['form4name'];
                $form6name = $update_row['form6name'];
                $form6no = $update_row['form6no'];
                $form7name = $update_row['form7name'];
                $form7no = $update_row['form7no'];
                $paddress = $update_row['paddress'];
                $currentaddaress = $update_row['currentaddaress'];
                $f4year = $update_row['f4year'];
                $f6year = $update_row['f6year'];
                $f7year = $update_row['f7year'];
                #sponsor info
                $relative = $update_row['relative'];
                $relative_phone = $update_row['relative_phone'];
                $relative_address = $update_row['relative_address'];
                $relative_job = $update_row['relative_job'];
                #next of kin info
                $kin = $update_row['kin'];
                $kin_phone = $update_row['kin_phone'];
                $kin_address = $update_row['kin_address'];
                $kin_job = $update_row['kin_job'];
                $studylevel = $update_row['studylevel'];
                //***********
            } else {
                $state2 = "submit,Save Record";
                $state3 = "";
                $state4 = "";
            }

            if (isset($_POST['save'])) {
                $disabilityCategory = $_POST['disabilityCategory'];
                $sponsorname = addslashes($_POST['sponsor']);
                $sponsor_phone = addslashes($_POST['sponsor_phone']);
                $sponsor_address = addslashes($_POST['sponsor_address']);
                $sponsor_job = addslashes($_POST['sponsor_job']);
                $studylevel = addslashes($_POST['studylevel']);
                $kin = addslashes($_POST['kin']);
                $kin_phone = addslashes($_POST['kin_phone']);
                $kin_address = addslashes($_POST['kin_address']);
                $kin_job = addslashes($_POST['kin_job']);
                $regno = trim(addslashes($_POST['regno']));
                $stdid = trim(addslashes($_POST['stdid']));
                $AdmissionNo = addslashes($_POST['AdmissionNo']);
                $degree = addslashes($_POST['degree']);
                $faculty = (isset($_POST['faculty'])) ? addslashes($_POST['faculty']) : '';
                $ayear = addslashes($_POST['ayear']);
                $combi = (isset($_POST['combi'])) ? addslashes($_POST['combi']) : '';
                $campus = addslashes($_POST['campus']);
                $class = addslashes($_POST['class']);
                $manner = addslashes($_POST['manner']);
                //$byear = addslashes($_POST['txtYear']);
                //$bmon = addslashes($_POST['txtMonth']);
                //$bday = addslashes($_POST['txtDay']);
                //$dtDOB = $bday . " - " . $bmon . " - " . $byear;
                $surname = strtoupper(addslashes($_POST['surname']));
                $firstname = addslashes($_POST['firstname']);
                $middlename = addslashes($_POST['middlename']);
                $dtDOB = $_POST['dtDOB'];
                $age = (isset($_POST['age'])) ? $_POST['age'] : '';
                $sex = $_POST['sex'];
                $sponsor = $_POST['txtSponsor'];
                $country = $_POST['country'];
                $nationality = $_POST['nationality'];
                $district = addslashes($_POST['district']);
                $region = addslashes($_POST['region']);
                $maritalstatus = $_POST['maritalstatus'];
                $address = (isset($_POST['address'])) ? strtoupper($_POST['address']) : '';
                $religion = (isset($_POST['religion'])) ? $_POST['religion'] : '';
                $denomination = $_POST['denomination'];
                $postaladdress = strtoupper(addslashes($_POST['paddress']));
                //$residenceaddress = strtoupper(addslashes($_POST['residenceaddress']));
                //$disability = $_POST['disability'];
                $status = $_POST['status'];
                $gyear = $_POST['dtDate'];
                $phone1 = $_POST['phone'];
                $email1 = $_POST['email'];
                $formsix = $_POST['form6no'];
                $formfour = $_POST['form4no'];
                $diploma = $_POST['form7no'];
                $studylevel = $_POST['studylevel'];
                $f4year = $_POST['f4year'];
                $f6year = $_POST['f6year'];
                $f7year = $_POST['f7year'];
                $denomination = $_POST['denomination'];
                $name = $surname . ", " . $firstname . " " . $middlename;

                //Added fields
                $account_number = $_POST['account_number'];
                $bank_branch_name = $_POST['bank_branch_name'];
                $bank_name = $_POST['bank_name'];
                $form4no = $_POST['form4no'];
                $form4name = $_POST['form4name'];
                $form6name = $_POST['form6name'];
                $form6no = $_POST['form6no'];
                $form7name = $_POST['form7name'];
                $form7no = $_POST['form7no'];
                //$paddress=$_POST['paddress'];
                $currentaddaress = $_POST['currentaddaress'];

                //*************
                //FORMATING ERRORS
                $error_array = array();
                $key = 0;
                if (!$formsix || !$formfour || !$diploma || !$phone1 || !$email1 || !$regno || !$degree || !$faculty || !$ayear || !$combi || !$campus || !$manner || !$byear || !$bmon || !$bday || !$dtDOB || !$surname || !$firstname || !$dtDOB || !$age || !$sex || !$sponsor || !$country || !$maritalstatus || !$address || !$religion || !$denomination || !$postaladdress || !$residenceaddress || !$status || !$gyear || !$name) {
                    if (!$regno || !$AdmissionNo) {
                        $regno_error = "<br/><font color='red'>Registration Number Must be Filled</font>";
                        $error_array[$key] = "error $key ";
                        $key++;
                    }
                    if (!$phone1) {
                        //$phone1_error="<font color='red'>Phone Number Must be Filled</font>";
                    }
                    if (!$studylevel) {
                        $studylevel_error = "<br/><font color='red'>Study Level Must be Filled</font>";
                        $error_array[$key] = "error $key ";
                        $key++;
                    }
                    if (!$email1) {
                        //$email1_error="<font color='red'>Email Must be Filled</font>";
                    }
                    if (!$formsix) {
                        //$formsix_error="<br/><font color='red'>Form Six Necta Number Must be Filled</font>";
                    }
                    if (!$formfour) {
                        $formfour_error = "<br/><font color='red'>Form Four Necta Number Must be Filled</font>";
                        $error_array[$key] = "error $key ";
                        $key++;
                    }

                    if (!$diploma) {
                        //$diploma_error="<font color='red'>Diploma Necta Number Must be Filled</font>";
                    }
                    if (!$degree) {
                        $degree_error = "<br/><font color='red'>Study Programme Must be Filled</font>";
                        $error_array[$key] = "error $key ";
                        $key++;
                    }

                    if (!$faculty) {
                        //$faculty_error="<font color='red'>Faculty  Must be Filled</font>";
                    }

                    if (!$ayear) {
                        $ayear_error = "<br/><font color='red'>Admission Year Must be Filled</font>";
                        $error_array[$key] = "error $key ";
                        $key++;
                    }

                    if (!$combi) {
                        //$combination_error="<font color='red'>Combination  Must be Filled</font>";
                    }
                    if (!$campus) {
                        $campus_error = "<br/><font color='red'>Campus Name Must be Filled</font>";
                        $error_array[$key] = "error $key ";
                        $key++;
                    }
                    if (!$manner) {
                        //$manner_error="<font color='red'>Manner Must be Filled</font>";
                    }

                    /*
                    if(!$byear)
                    {
                    //$byear_error="<font color='red'>Birth Date Must be Filled</font>";
                    }
                    /if(!$bmon)
                    {
                    //$bmon_error="<font color='red'>Month Must be Filled</font>";
                    }
                    */
                    if (!$sex) {
                        $sex_error = "<br/><font color='red'>Gender Must be Filled</font>";
                        $error_array[$key] = "error $key ";
                        $key++;
                    }

                    /*if(!$bday)
                    {
                    //$bday_error="<font color='red'>Birth day Must be Filled</font>";
                    }*/
                    if (!$dtDOB) {
                        $dtDOB_error = "<br/><font color='red'>Date of birth Must be Filled</font>";
                        $error_array[$key] = "error $key ";
                        $key++;
                    }

                    if (!$surname) {
                        $surname_error = "<br/><font color='red'>Surname Must be Filled</font>";
                        $error_array[$key] = "error $key ";
                        $key++;
                    }

                    if (!$firstname) {
                        $firstname_error = "<br/><font color='red'>First Name Must be Filled</font>";
                        $error_array[$key] = "error $key ";
                        $key++;
                    }

                    if (!$middlename) {
                        //$middlename_error="<font color='red'>Middle Name Must be Filled</font>";
                    }

                    if (!$sponsor) {
                        $sponsor_error = "<br/><font color='red'>Sponsor Must be Filled</font>";
                        $error_array[$key] = "error $key ";
                        $key++;
                    }
                    if (!$country) {
                        //$country_error="<br/><font color='red'>Country Must be Filled</font>";
                    }
                    if (!$region) {
                        //$region_error="<font color='red'>Region Must be Filled</font>";
                    }
                    if (!$maritalstatus) {
                        //$maritalstatus_error="<br/><font color='red'>Marital Status Must be Filled</font>";
                    }
                    if (!$address) {
                        //$address_error="<font color='red'>Address Must be Filled</font>";

                    }
                    /*
                    if(!@$religion)
                    {
                    //$religion_error="<font color='red'>Religion Must be Filled</font>";
                    }*/
                    if (!$denomination) {
                        //$denomination_error="<font color='red'>Denomination Must be Filled</font>";
                    }
                    if (!$postaladdress) {
                        //$postaladdress_error="<br/><font color='red'>Postal Address Must be Filled</font>";
                        //$error_array[$key]="error $key ";
                        //$key++;
                    }
                    /*
                    if(!$residenceaddress)
                    {
                    //$residenceaddress_error="<br/><font color='red'>Residentaddress Address Must be Filled</font>";
                    //$error_array[$key]="error $key ";
                    //$key++;
                    }*/

                    /*if(!$disability)
                    {
                    //$disability_error="<font color='red'>Disability Must be Filled</font>";
                    }*/

                    if (!$status) {
                        $status_error = "<br/><font color='red'>Status Address Must be Filled</font>";
                        $error_array[$key] = "error $key ";
                        $key++;
                    }
                    if (!$gyear) {
                        //$gyear_error="<font color='red'>Gyear Address Must be Filled</font>";
                    }
                    if (!$name) {
                        //$name_error="<font color='red'>Name Address Must be Filled</font>";
                    }

                    if (!$class) {
                        //$class_error="<br/><font color='red'>Class stream Must be Filled</font>";
                    }

                }
                form();

                if (!empty($error_array)) {
                    // there is errors
                } else {
#check if RegNo Exist
                    if ($regno <> '') {
                        $qRegNo = "SELECT RegNo FROM student WHERE RegNo = '$regno'";
                        $dbRegNo = mysqli_query($zalongwa, $qRegNo);
                        $total = mysqli_num_rows($dbRegNo);
                        if ($total == 1) {
                            echo "
		<table>
		<tr><td><img src='../includes/img/error.gif'></td>
		<td>
		ZALONGWA SARIS database system has found this,<br> 
		Registration Number $regno already in use.
		<br><a href='./admissionRegistrationForm.php'>Go Back and Insert Newone!</a>
		</td></tr></table>";
                        } else {
                            #insert record
                            $sql = "INSERT INTO student
		(Name,AdmissionNo,
		Sex,DBirth,
		MannerofEntry,MaritalStatus,
		Campus,ProgrammeofStudy,
		Faculty,
		Sponsor,GradYear,
		EntryYear,Status,
		Address,Nationality,
		Region,District,Country,
		Received,user,
		Denomination, Religion,
		Disability,formfour,
		formsix,diploma,
		f4year,f6year,f7year,
		relative,relative_phone,relative_address,relative_job,
		kin,kin_phone,
		kin_address,kin_job,
		disabilityCategory,Subject,
		account_number,
		bank_branch_name,
		bank_name,
		form4no,
		form4name,
		form6name,
		form6no,
		form7name,
		form7no,
		paddress,
		Phone,
		Email,
		currentaddaress,
		RegNo,
		Class,
		studylevel
		) 
		VALUES
		('$name','$AdmissionNo',
		'$sex','$dtDOB',
		'$manner','$maritalstatus',
		'$campus','$degree',
		'$faculty',' $sponsor',
		'$gyear','$ayear',
		'$status','$address',
		'$nationality',
		'$region','$district',
		'$country',now(),
		'$username','$denomination', 
		'$religion','$disability',
		'$formfour','$formsix',
		'$diploma','$f4year',
		'$f6year','$f7year',
		'$sponsorname','$sponsor_phone','$sponsor_address','$sponsor_job',
		'$kin','$kin_phone',
		'$kin_address','$kin_job',
		'$disabilityCategory','$Subject',
		'$account_number',
		'$bank_branch_name',
		'$bank_name',
		'$form4no',
		'$form4name',
		'$form6name',
		'$form6no',
		'$form7name',
		'$form7no',
		'$paddress',
		'$phone1',
		'$email1',
		'$currentaddaress',
		'$regno',
		'$class',
		'$studylevel'
		)";
                            //echo $sql;
                            $dbstudent = mysqli_query($zalongwa, $sql);
                            if (!$dbstudent) {
                                echo "Admision Record Cannot be Saved";
                            } else {
                                echo "Admision Record Saved Successfuly";
                            }
                        }
                    }
                }
            } else {
                form();
            }

            //*************REGISTRATION FORM
            function form()
            {
                global $state1, $state2, $state3, $label_edit, $state4, $AdmissionNo, $stdid, $studylevel;
                global $disabilityCategory, $studylevel, $father, $father_job, $mother, $mother_job, $father_address,
                       $father_phone, $mother_address, $mother_phone,
                       $brother, $brother_phone, $brother_address, $brother_job, $class,
                       $sister, $sister_phone, $sister_address, $sister_job,
                       $spouse, $spouse_phone, $spouse_address, $spouse_job,
                       $kin, $kin_phone, $kin_address, $kin_job, $School_attended_olevel, $School_attended_to_alevel, $School_attended_alevel, $School_attended_from_alevel,
                       $relative, $relative_phone, $relative_address, $relative_job, $School_attended_to_olevel, $School_attended_from_olevel,
                       $txtYear, $txtDay, $txtMonth, $sponsor, $maritalstatus, $country, $nationality, $formsix, $formfour, $diploma, $phone1, $email1, $regno, $degree, $faculty, $ayear, $combi, $campus, $manner, $byear, $bmon, $bday, $dtDOB, $surname, $firstname, $middlename, $dtDOB, $age, $sex, $sponsor, $country, $district, $region, $maritalstatus, $address, $religion, $denomination, $postaladdress, $residenceaddress, $disability, $status, $gyear, $name, $ayear, $campus,
                       $kin, $kin_phone, $kin_address, $kin_job, $f4year, $f6year, $f7year;
                global $disabilityCategory_erro, $studylevel_error, $maritalstatus_error, $country_error, $ayear_error, $formsix_error, $formfour_error, $diploma_error, $dbirth_error, $date_error, $phone1_error, $email1_error, $regno_error, $degree_error, $faculty_error, $ayear_error, $combi_error, $campus_error, $manner_error, $byear_error, $bmon_error, $bday_error, $dtDOB_error, $surname_error, $firstname_error, $middlename_error, $dtDOB_error, $age_error, $sex_error, $sponsor_error, $country_error, $district_error, $region_error, $maritalstatus_error, $address_error, $religion_error, $denomination_error, $postaladdress_error, $residenceaddress_error, $disability_error, $status_error, $gyear_error, $name_error;
                global $account_number, $bank_branch_name, $bank_name, $form4no, $form4name, $form6name, $form6no, $form7name, $form7no, $paddress, $currentaddaress;
                global $zalongwa;
                global $class_error;
                ?>
                <form action=" <?php echo $_SERVER['PHP_SELF'] ?>" method="POST" name='admission'>
                    <div class="col-md-6 offset-md-3">
                        <?php echo $label_edit; ?>&nbsp;
                    </div>
                    <?php
                    $state2array = explode(',', $state2);
                    $inputtype = $state2array[0];
                    $inputvalue = $state2array[1];
                    $inputname = ($inputvalue == 'Save Record') ? 'save' : 'actionupdate';
                    ?>
                    <div style="margin: 15px 0;">
                        <input name="<?php echo $inputname; ?>" type="<?php echo $inputtype; ?>"
                               class="btn btn-success btn-block" value="<?php echo $inputvalue; ?>">
                    </div>
                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                       aria-expanded="true"
                                       aria-controls="collapseOne">
                                        Study Programme Information

                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-block ">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label" for="sel1">Year of Admission:</label>
                                        <div class="col-sm-8">
                                            <select name="ayear" id="select"
                                                    class="vform form-control" <?php echo $state4; ?>>
                                                <?php
                                                if (!$ayear) {
                                                    echo "<option value=''>[Select Year]</option>";
                                                } else {
                                                    echo "<option value='$ayear'>$ayear</option>";
                                                }

                                                ?>
                                            </select>
                                            <?php echo $ayear_error;
                                            ?>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-xs row">
                                        <label class="col-sm-4 col-form-label" for="example-text-inputx">Admission
                                            No:*</label>
                                        <div class="col-sm-8">
                                            <input name="hiddenregno" type="hidden" id="hiddenregno"
                                                   value="7070" <?php echo $state3; ?> <?php echo $state4; ?> />
                                            <input name="stdid" type="hidden" id="stdid"
                                                   value="<?php echo $stdid; ?>" <?php echo $state3; ?> <?php echo $state4; ?> />
                                            <input name="AdmissionNo" type="text" id="AdmissionNo" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $AdmissionNo; ?>" <?php echo $state3; ?> <?php echo $state4; ?> />
                                            <?php echo $regno_error; ?>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label" for="example-text-input3">Registration
                                            No:*</label>
                                        <div class="col-sm-8">
                                            <input name="regno" type="text" class="form-control"
                                                   aria-describedby="textHelp"
                                                   id="regno"
                                                   value="<?php echo $regno; ?>"<?php echo $state3; ?> <?php echo $state4; ?>>
                                            <?php echo $regno_error; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label" for="sel1">Campus:</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="campus" <?php echo $state4; ?>>
                                                <?php
                                                if (!$campus) {
                                                    echo "<option value=''>[Select Campus]</option>";
                                                } else {
                                                    $query_campus1 = mysqli_query($zalongwa, "SELECT CampusID, Campus FROM campus where CampusID='$campus'");
                                                    $camp = mysqli_fetch_array($query_campus1);
                                                    echo "<option value='$campus'>$camp[Campus]</option>";
                                                }
                                                ?>
                                            </select>
                                            <?php echo $campus_error; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label" for="sel1">Program Registered:</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="degree"
                                                    id="degree" <?php echo $state4; ?>>
                                                <?php
                                                if (!$degree) {
                                                    echo "<option value=''>[Select Programme]</option>";
                                                } else {
                                                    $take = mysqli_query($zalongwa, "select * from programme where ProgrammeCode='$degree'");
                                                    $t = mysqli_fetch_array($take);
                                                    echo "<option value='$degree'>$t[ProgrammeName]</option>";
                                                }
                                                ?>
                                            </select>
                                            <?php echo $degree_error; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="sel1">Class
                                            Stream:</label>
                                        <div class="col-sm-8">
                                            <select class="form-control vform" name="class"
                                                    id="class" <?php echo $state4; ?>>
                                                <?php
                                                if (!$class) {
                                                    echo "<option value=''>[Select Class]</option>";
                                                } else {
                                                    echo "<option value='$class'>$class</option>";
                                                }
                                                $nm = mysqli_query($zalongwa, "SELECT name FROM classstream where name!='$class' ORDER BY name ASC");
                                                while ($show = mysqli_fetch_array($nm)) {
                                                    echo "<option  value='$show[name]'>$show[name]</option>";
                                                }
                                                ?>
                                            </select>
                                            <?php echo $class_error;
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="sel1">Level of Study
                                            Registered:</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="studylevel"
                                                    id="studylevel" <?php echo $state4; ?>>
                                                <?php
                                                if (!$studylevel) {
                                                    echo "<option value=''>[Select Level of Study]</option>";
                                                } else {
                                                    $take = mysqli_query($zalongwa, "select * from studylevel where LevelCode='$studylevel'");
                                                    $t = mysqli_fetch_array($take);
                                                    echo "<option value='$studylevel'>$t[LevelName]</option>";
                                                }
                                                ?>
                                            </select>
                                            <?php echo $studylevel_error; ?>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="sel1">Manner of
                                            Entry:</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="manner"
                                                    id="manner" <?php echo $state4; ?>>
                                                <?php
                                                if (!$manner) {
                                                    echo "<option value=''>[Select Manner of Entry]</option>";
                                                } else {
                                                    $query_Manner = mysqli_query($zalongwa, "SELECT ID, MannerofEntry FROM mannerofentry where ID='$manner'");
                                                    $mana = mysqli_fetch_array($query_Manner);
                                                    echo "<option value='$manner'>$mana[MannerofEntry]</option>";
                                                }
                                                $query_MannerofEntry = "SELECT ID, MannerofEntry FROM mannerofentry ORDER BY MannerofEntry ASC";
                                                $nm = mysqli_query($zalongwa, $query_MannerofEntry);
                                                while ($show = mysqli_fetch_array($nm)) {
                                                    echo "<option  value='$show[ID]'>$show[MannerofEntry]</option>";
                                                }
                                                ?>
                                            </select>
                                            <?php
                                            echo $manner_error;
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="date">Graduation
                                            Date:</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="dtDate" class="form-control"
                                                   value="<?php echo $gyear; ?>" <?php echo $state3; ?> <?php echo $state4; ?>/>
                                            <!--<script type="text/javascript" src="./calendars.js"></script>-->
                                            <script language="JavaScript">
                                                new tcal({'formname': 'admission', 'controlname': 'dtDate'});
                                            </script>
                                            <?php echo $date_error; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label"
                                               for="sel1">Sponsorship:</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="txtSponsor"
                                                    id="txtSponsor" <?php echo $state4; ?>>
                                                <?php
                                                if (!$sponsor) {
                                                    echo "<option value=''>[Select Sponsor]</option>";
                                                } else {
                                                    echo "<option value='$sponsor'>$sponsor</option>";
                                                }
                                                $query_sponsor = "SELECT Name FROM sponsors ORDER BY SponsorID ASC";
                                                $nm = mysqli_query($zalongwa, $query_sponsor);
                                                while ($show = mysqli_fetch_array($nm)) {
                                                    echo "<option  value='$show[Name]'>$show[Name]</option>";
                                                }
                                                ?>
                                            </select>
                                            <?php
                                            echo $sponsor_error;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingTwo">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseTwo"
                                       aria-expanded="false" aria-controls="collapseTwo">
                                        Personal Information
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="card-block">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Surname:</label>
                                        <div class="col-sm-8">
                                            <input name="surname" type="text" class="form-control"
                                                   aria-describedby="textHelp" id="surname"
                                                   value="<?php echo $surname; ?>"
                                                   size="30" <?php echo $state3; ?> <?php echo $state4; ?>/>
                                            <?php echo $surname_error; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Middlename</label>
                                        <div class="col-sm-8">
                                            <input name="middlename" type="text" id="middlename" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $middlename; ?>" <?php echo $state3; ?> <?php echo $state4; ?>/>
                                            <?php echo $middlename_error; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Firstname:</label>
                                        <div class="col-sm-8">
                                            <input name="firstname" type="text" class="form-control"
                                                   aria-describedby="textHelp" value="<?php echo $firstname; ?>"
                                                   id="firstname" <?php echo $state3; ?> <?php echo $state4; ?>/>
                                            <?php echo $firstname_error; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="sel1">Sex:</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="sex" id="sex" <?php echo $state4; ?>>
                                                <?php
                                                if (!$sex) {
                                                    echo "<option value=''>[Select Gender]</option>";
                                                } else {
                                                    if ($sex == 'F') {
                                                        ?>
                                                        <option value="<?php echo $sex; ?>">Female</option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?php echo $sex; ?>">Male</option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            </select>
                                            <?php
                                            echo $sex_error;
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Date
                                            of Birth:</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="dtDOB" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $dtDOB; ?>" <?php echo $state4; ?>/>
                                            <script language="JavaScript">
                                                new tcal({'formname': 'admission', 'controlname': 'dtDOB'});
                                            </script>
                                            <?php echo $dtDOB_error; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">District
                                            of Birth:</label>
                                        <div class="col-sm-8">
                                            <input name="district" type="text" id="district" class="form-control"
                                                   aria-describedby="textHelp" value="<?php echo $district; ?>"
                                                   size="30" <?php echo $state4; ?>/>
                                            <?php echo $district_error; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Region
                                            of Birth:</label>
                                        <div class="col-sm-8">
                                            <input name="region" type="text" id="region" size="30" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $region; ?>" <?php echo $state4; ?>/>
                                            <?php echo $region_error; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="sel1">Country of
                                            Birth:</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="country" <?php echo $state4; ?>>
                                                <?php
                                                if ($country) {
                                                    echo "<option value='$country'>$country</option>";
                                                } else {
                                                    echo "<option value='Tanzania'>Tanzania</option>";
                                                }
                                                $query_country = "SELECT szCountry FROM country ORDER BY szCountry";
                                                $countrys = mysqli_query($zalongwa, $query_country);
                                                while ($row_country = mysqli_fetch_array($countrys)) {
                                                    ?>
                                                    <option value="<?php echo $row_country['szCountry'] ?>"> <?php echo $row_country['szCountry'] ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            <?php
                                            echo $country_error; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label"
                                               for="sel1">Nationality:</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="nationality" <?php echo $state4; ?>>
                                                <?php
                                                if ($nationality) {
                                                    echo "<option value='$nationality'>$nationality</option>";
                                                } else {
                                                    echo "<option value='Tanzanian'>Tanzanian</option>";
                                                }
                                                $query_nationality = "SELECT nationality FROM nationality";
                                                $nationalitys = mysqli_query($zalongwa, $query_nationality);
                                                while ($row_nationality = mysqli_fetch_array($nationalitys)) {
                                                    ?>
                                                    <option value="<?php echo $row_nationality['nationality'] ?>"> <?php echo $row_nationality['nationality'] ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            <?php
                                            echo $country_error; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="sel1">Student
                                            Status:</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="status"
                                                    id="status" <?php echo $state4; ?>>
                                                <?php
                                                if (!$status) {
                                                    echo "<option value=''>[Select Status]</option>";
                                                } else {
                                                    $query_studentStatus1 = mysqli_query($zalongwa, "SELECT StatusID,Status FROM studentstatus where StatusID='$status'");
                                                    $stat = mysqli_fetch_array($query_studentStatus1);
                                                    echo "<option value='$status'>$stat[Status]</option>";
                                                }
                                                ?>
                                            </select>
                                            <?php
                                            echo $status_error;
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label"
                                               for="sel1">Religion:</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name='denomination'
                                                    id='denomination' <?php echo $state4; ?>>
                                                <?php
                                                if (!$denomination) {
                                                    echo "<option value=''>[Select Sect of Religion ]</option>";
                                                } else {
                                                    ?>
                                                    <option value="<?php echo $denomination; ?>"><?php echo $denomination; ?></option>
                                                    <?php
                                                }

                                                $query_denomination2 = "SELECT * FROM religion";
                                                $nr = mysqli_query($zalongwa, $query_denomination2);
                                                while ($l = mysqli_fetch_array($nr)) {
                                                    //echo"<optgroup label='$l[Religion]'>";
                                                    //$query_denomination = "SELECT * FROM denomination where ReligionID='$l[ReligionID]' ORDER BY denomination ASC";
                                                    $query_denomination = "SELECT * FROM religion where ReligionID='$l[ReligionID]' ORDER BY Religion ASC";
                                                    $nm = mysqli_query($zalongwa, $query_denomination);
                                                    while ($show = mysqli_fetch_array($nm)) {
                                                        echo "<option  value='$show[Religion]'>$show[Religion]</option>";
                                                    }
                                                    //echo"</optgroup>";
                                                }
                                                ?>
                                            </select>
                                            <?php
                                            echo $denomination_error;
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="sel1">Marital
                                            Status:</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="maritalstatus"
                                                    id="maritalstatus" <?php echo $state4; ?>>
                                                <?php
                                                if ($maritalstatus) {
                                                    echo "<option value='$maritalstatus'>$maritalstatus</option>";
                                                } else {
                                                    echo "<option value=''>[Select Marital Status]</option>";
                                                }
                                                ?>
                                                <option value="Single">Single</option>
                                                <option value="Married">Married</option>
                                                <option value="Divorced">Divorced</option>
                                                <option value="Widowed">Widowed</option>
                                            </select>
                                            <?php
                                            echo $maritalstatus_error;
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label"
                                               for="sel1">Disability:</label>
                                        <div class="col-sm-8">
                                            <select class="form-control"
                                                    name='disabilityCategory' <?php echo $state4; ?>>
                                                <?php
                                                if ($disabilityCategory) {
                                                    ?>
                                                    <option value="<?php echo $disabilityCategory; ?>"> <?php echo $disabilityCategory; ?></option>
                                                    <?php
                                                } else {
                                                    echo "<option value='None'>[Select Disability]</option>";
                                                }
                                                $query_disability3 = "SELECT * FROM disability";
                                                $nm3 = mysqli_query($zalongwa, $query_disability3);
                                                while ($s = mysqli_fetch_array($nm3)) {
                                                    echo "<optgroup label='$s[disability]'>";
                                                    $query_disability2 = "SELECT * FROM disabilitycategory where DisabilityCode='$s[DisabilityCode]'";
                                                    $nm2 = mysqli_query($zalongwa, $query_disability2);
                                                    while ($show = mysqli_fetch_array($nm2)) {
                                                        echo "<option  value='$show[disabilityCategory]'>$show[disabilityCategory]</option>";
                                                    }
                                                    echo "<optgroup>";
                                                }
                                                ?>
                                            </select>
                                            <?php
                                            echo $disability_error;
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Permanent
                                            Address:</label>
                                        <div class="col-sm-8">
                                            <input name="paddress" type="text" id="paddress" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $paddress; ?>" <?php echo $state4; ?>/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Current
                                            Address:</label>
                                        <div class="col-sm-8">
                                            <input name="currentaddaress" type="text" id="currentaddaress" size="30"
                                                   class="form-control" aria-describedby="textHelp"
                                                   value="<?php echo $currentaddaress; ?>" <?php echo $state4; ?>/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Phone:</label>
                                        <div class="col-sm-8">
                                            <input name="phone" type="text" size="30"  class="form-control"
                                                   aria-describedby="textHelp" value="<?php echo $phone1; ?>" <?php echo $state4; ?>/>
                                            <?php
                                            echo $phone1_error;
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">E-mail:</label>
                                        <div class="col-sm-8">
                                            <input name="email" type="text" size="30" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $email1; ?>" <?php echo $state4; ?>/>
                                            <?php
                                            echo $email1_error;
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Name
                                            of Bank:</label>
                                        <div class="col-sm-8">
                                            <input name="bank_name" size="30" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $bank_name; ?>" <?php echo $state4; ?>/>
                                            <?php ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Name
                                            of Branch:</label>
                                        <div class="col-sm-8">
                                            <input name="bank_branch_name" size="30" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $bank_branch_name; ?>" <?php echo $state4; ?>/>
                                            <?php ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Account
                                            Number:</label>
                                        <div class="col-sm-8">
                                            <input name="account_number" size="30" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $account_number; ?>" <?php echo $state4; ?>/>
                                            <?php ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingThree">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseThree"
                                       aria-expanded="false" aria-controls="collapseThree">
                                        Sponsor Information
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="card-block">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Name
                                            of Sponsor:</label>
                                        <div class="col-sm-8">
                                            <input name="sponsor" size="30" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $relative; ?>" <?php echo $state4; ?> />
                                            <?php ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Sponsor
                                            Occupation:</label>
                                        <div class="col-sm-8">
                                            <input name="sponsor_job" size="30" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $relative_job; ?>" <?php echo $state4; ?>/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Sponsor
                                            Phone:</label>
                                        <div class="col-sm-8">
                                            <input name="sponsor_phone" size="30" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $relative_phone; ?>" <?php echo $state4; ?> />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Sponsor
                                            Address:</label>
                                        <div class="col-sm-8">
                                            <input name="sponsor_address" size="30" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $relative_address; ?>" <?php echo $state4; ?> />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingFour">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseFour"
                                       aria-expanded="false" aria-controls="collapseFour">
                                        Next of Kin Information
                                    </a>
                                </h5>
                            </div>

                            <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="card-block">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Name
                                            of Next of Kin:</label>
                                        <div class="col-sm-8">
                                            <input name="kin" size="30" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $kin; ?>" <?php echo $state4; ?> />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Next
                                            of Kin Phone:</label>
                                        <div class="col-sm-8">
                                            <input name="kin_phone" size="30" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $kin_phone; ?>" <?php echo $state4; ?> />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Next
                                            of Kin Occupation:</label>
                                        <div class="col-sm-8">
                                            <input name="kin_job" size="30" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $kin_job; ?>" <?php echo $state4; ?>/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Next
                                            of Kin Address:</label>
                                        <div class="col-sm-8">
                                            <input name="kin_address" size="30" class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $kin_address; ?>" <?php echo $state4; ?> />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingFive">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseFive"
                                       aria-expanded="false" aria-controls="collapseFive">
                                        Entry Qualifications Information
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingFive">
                                <div class="card-block">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Form
                                            IV School Name:</label>
                                        <div class="col-sm-8">
                                            <input name="form4name" type="text" id="form4name" size="30"
                                                   class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $form4name; ?>" <?php echo $state4; ?> />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="sel1">Form IV NECTA
                                            No:</label>
                                        <div class="col-sm-8">
                                            <div style="margin-top: 0; margin-bottom: 0" class="form-group row">
                                                <div class="col-sm-8">
                                                    <input name="form4no" type="text" id="formfour3"
                                                           class="form-control"
                                                           aria-describedby="textHelp"
                                                           value="<?php echo $form4no; ?>" <?php echo $state4; ?>/>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select class="form-control" name='f4year' <?php echo $state4; ?>>
                                                        <?php
                                                        if ($f4year) {
                                                            echo "<option value='$f4year'>$f4year</option>";
                                                        }
                                                        echo "<option value='None'>None</option>";
                                                        for ($k = date('Y'); $k >= 1960; $k--) {
                                                            echo "<option value='$k'>$k</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php echo $formfour_error; ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Form
                                            VI School Name:</label>
                                        <div class="col-sm-8">
                                            <input name="form6name" type="text" id="formfour4" size="30"
                                                   class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $form6name; ?>" <?php echo $state4; ?>/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="sel1">Form VI NECTA
                                            No:</label>
                                        <div class="col-sm-8">
                                            <div style="margin-top: 0; margin-bottom: 0" class="form-group row">
                                                <div class="col-sm-8">
                                                    <input name="form6no" type="text" id="formsix" class="form-control"
                                                           aria-describedby="textHelp"
                                                           value="<?php echo $form6no; ?>" <?php echo $state4; ?> />
                                                </div>
                                                <div class="col-sm-4">
                                                    <select class="form-control" name='f6year' <?php echo $state4; ?>>
                                                        <?php
                                                        if ($f6year) {
                                                            echo "<option value='$f6year'>$f6year</option>";
                                                        }
                                                        echo "<option value='None'>None</option>";
                                                        for ($k = date('Y'); $k >= 1960; $k--) {
                                                            echo "<option value='$k'>$k</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="example-text-input">Equivalent
                                            Institute Name:</label>
                                        <div class="col-sm-8">
                                            <input name="form7name" type="text" id="formfour" size="30"
                                                   class="form-control"
                                                   aria-describedby="textHelp"
                                                   value="<?php echo $form7name; ?>" <?php echo $state4; ?>/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 col-form-label" for="sel1">Equivalent
                                            Qualification:</label>
                                        <div class="col-sm-8">
                                            <div style="margin-top: 0; margin-bottom: 0" class="form-group row">
                                                <div class="col-sm-8">
                                                    <input name="form7no" type="text" id="diploma" class="form-control"
                                                           aria-describedby="textHelp"
                                                           value="<?php echo $form7no; ?>" <?php echo $state4; ?>/>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select class="form-control" name='f7year' <?php echo $state4; ?>>
                                                        <?php
                                                        if ($f7year) {

                                                            echo "<option value='$f7year'>$f7year</option>";
                                                        }
                                                        echo "<option value='None'>None</option>";
                                                        for ($k = date('Y'); $k >= 1960; $k--) {
                                                            echo "<option value='$k'>$k</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <?php echo $diploma_error; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="margin: 15px 0;">
                            <input name="<?php echo $inputname; ?>" type="<?php echo $inputtype; ?>"
                                   class="btn btn-success btn-block" value="<?php echo $inputvalue; ?>">
                        </div>
                    </div>
                </form>
                <?php
            }

            #Updating Records
            if (isset($_POST['actionupdate'])) {
                $disabilityCategory = $_POST['disabilityCategory'];
                $sponsorname = addslashes($_POST['sponsor']);
                $sponsor_phone = addslashes($_POST['sponsor_phone']);
                $sponsor_address = addslashes($_POST['sponsor_address']);
                $sponsor_job = addslashes($_POST['sponsor_job']);
                $kin = $_POST['kin'];
                $kin_phone = $_POST['kin_phone'];
                $kin_address = $_POST['kin_address'];
                $kin_job = $_POST['kin_job'];
                $regno = trim(addslashes($_POST['regno']));
                $AdmissionNo = trim(addslashes($_POST['AdmissionNo']));
                $stdid = $_POST['stdid'];
                $degree = $_POST['degree'];
                $faculty = $_POST['faculty'];
                $ayear = $_POST['ayear'];
                $combi = $_POST['combi'];
                $campus = $_POST['campus'];
                $class = $_POST['class'];
                $manner = $_POST['manner'];
                $byear = addslashes($_POST['txtYear']);
                $bmon = addslashes($_POST['txtMonth']);
                $bday = addslashes($_POST['txtDay']);
                $dtDOB = $bday . " - " . $bmon . " - " . $byear;
                $surname = strtoupper(addslashes($_POST['surname']));
                $firstname = addslashes($_POST['firstname']);
                $middlename = addslashes($_POST['middlename']);
                $dtDOB = $_POST['dtDOB'];
                $age = $_POST['age'];
                $sex = $_POST['sex'];
                $sponsor = $_POST['txtSponsor'];
                $nationality = $_POST['nationality']; //$chas
                $country = $_POST['country'];
                $district = addslashes($_POST['district']);
                $region = addslashes($_POST['region']);
                $maritalstatus = $_POST['maritalstatus'];
                $address = strtoupper($_POST['address']);
                $religion = $_POST['religion'];
                $denomination = $_POST['denomination'];
                $postaladdress = strtoupper(addslashes($_POST['postaladdress']));
                $residenceaddress = strtoupper(addslashes($_POST['residenceaddress']));
                $disability = $_POST['disability'];
                $status = $_POST['status'];
                $gyear = $_POST['dtDate'];
                $phone = $_POST['phone'];
                $email = $_POST['email'];
                $formsix = $_POST['formsix'];
                $formfour = $_POST['formfour'];
                $diploma = $_POST['diploma'];
                $studylevel = $_POST['studylevel'];
                $f4year = $_POST['f4year'];
                $f6year = $_POST['f6year'];
                $f7year = $_POST['f7year'];
                $denomination = $_POST['denomination'];
                $name = $surname . ", " . $firstname . " " . $middlename;

                //Added fields
                $account_number = $_POST['account_number'];
                $bank_branch_name = $_POST['bank_branch_name'];
                $bank_name = $_POST['bank_name'];
                $form4no = $_POST['form4no'];
                $form4name = $_POST['form4name'];
                $form6name = $_POST['form6name'];
                $form6no = $_POST['form6no'];
                $form7name = $_POST['form7name'];
                $form7no = $_POST['form7no'];
                $paddress = $_POST['paddress'];
                $currentaddaress = $_POST['currentaddaress'];

                $qRegNo = "SELECT RegNo FROM student WHERE RegNo = '$regno'";
                $dbRegNo = mysqli_query($zalongwa, $qRegNo);
                $total = mysqli_num_rows($dbRegNo);
                if ($total > 1) {
                    echo "ZALONGWA SARIS database system has detected that,<br> this Registration " . $regno . " is already in use";
                    echo "<br> Go Back and Insert Newone!<hr><br>";
                } else {
                    #update record
                    $sql = "update student set Name='$name',
	Sex='$sex',DBirth='$dtDOB',
	MannerofEntry='$manner',
	MaritalStatus='$maritalstatus',
	Campus='$campus',ProgrammeofStudy='$degree',
	Faculty='$faculty',Sponsor='$sponsor',
	GradYear='$gyear',EntryYear='$ayear',
	Status='$status',
	Address='$address',Nationality='$nationality',
	Region='$region',District='$district',
	Country='$country',
	Received=now(),user='$username',
	Denomination='$denomination',
	Religion='$religion',Disability='$disability',
	formfour='$formfour',formsix='$formsix',
	diploma='$diploma',f4year='$f4year',
	f6year='$f6year',f7year='$f7year',
	kin='$kin',kin_phone='$kin_phone',
	kin_address='$kin_address',kin_job='$kin_job',
	relative='$sponsorname',relative_phone='$sponsor_phone',relative_address='$sponsor_address',relative_job='$sponsor_job',
	disabilityCategory='$disabilityCategory',
	Subject='$combi',studylevel='$studylevel',
	account_number='$account_number',
	bank_branch_name='$bank_branch_name',
	bank_name='$bank_name',
	form4no='$form4no',
	form4name='$form4name',
	form6name='$form6name',
	form6no='$form6no',
	form7name='$form7name',
	form7no='$form7no',
	paddress='$paddress',
	Phone='$phone',
	Email='$email',
	AdmissionNo='$AdmissionNo',
	RegNo='$regno',
	Class='$class',
	currentaddaress='$currentaddaress'
	where Id='$stdid'";
                    $dbstudent = mysqli_query($zalongwa, $sql) or die(mysqli_error($zalongwa) . ' - mmeona wenyewe?');
                    if (!$dbstudent) {
                        echo "Admision Record Cannot be Updated - " . $dbstudent;
                    } else {
                        echo "<meta http-equiv='refresh' content='0'>";
                        echo "Admision Record Updated Successfuly";
                    }
                }
            }

            ?>
        </div>
    </div>
</div>
