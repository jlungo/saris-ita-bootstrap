<?php
require_once '../Connections/sessioncontrol.php';
require_once '../Connections/zalongwa.php';

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;

$szSection = 'Admission Process';

$szSubSection = 'Registration Form';

$szTitle = 'Student Registration Form';

$reg = $RegNo;

#get registration date of birth
$sqldate = "SELECT * FROM security WHERE RegNo='$RegNo'";
$getd = mysqli_query($zalongwa, $sqldate) or die(mysqli_error($zalongwa));
$getdate = mysqli_fetch_array($getd) or die(mysqli_error($zalongwa));
$securityDOB = $getdate['DBirth'];


#get current Academic Year
$qayr = "SELECT AYear FROM academicyear WHERE Status = '1'";
$aYearCurrent = mysqli_query($zalongwa, $qayr);
$getcryear = mysqli_fetch_array($aYearCurrent);
$cryear = $getcryear['AYear'];

//find if student already exist to update
$qRegNo1 = "SELECT Id FROM student WHERE RegNo = '$RegNo'";
$dbRegNo1 = mysqli_query($zalongwa, $qRegNo1);
$total1 = mysqli_num_rows($dbRegNo1);
$getID = mysqli_fetch_array($dbRegNo1);
$id = $getID['Id'];
$x = $total1;
#include calendar and other stylesheets
require_once('../includes/javascript.inc');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <style>
        body {
            /* background-color: #009688; */
        }

        .card {
            /* background-color: #324359; */
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /* color: white; */
            padding: 0px;
            border-radius: 0px !important;
        }

        .row {
            margin-top: 20px;
        }


    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'studentNavBar.php'; ?>

<?php
include 'edit_form.php';
?>

<br><br><br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>

