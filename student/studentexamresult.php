<?php

require_once('../Connections/sessioncontrol.php');

require_once('../Connections/zalongwa.php');

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;

$szSection = 'Academic Records';

$szTitle = 'Examination Results';

$szSubSection = 'Exam Result';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>

    <title>SARIS | <?php echo  $szSection ?> | <?php echo  $szSubSection ?></title>
    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>

</head>
<body>
<!-- navbar -->
<?php include 'studentNavBar.php';?>

<div class="container-flex">
    <br>
</div>
<div class="container ">
    <?php

    if ($examofficer <> 1) {

        require_once('../Connections/sessioncontrol.php');
        require_once('../Connections/zalongwa.php');


        global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;

        $szSection = 'Academic Records';

        $szTitle = 'Examination Results';

        $szSubSection = 'Exam Result';

    }


    $editFormAction = $_SERVER['PHP_SELF'];


    #check if has blocked

    $qstatus = "SELECT Status FROM student  WHERE (RegNo = '$RegNo')";

    $dbstatus = mysqli_query($zalongwa, $qstatus);

    $row_status = mysqli_fetch_array($dbstatus);

    $status = $row_status['Status'];


    if ($status == 'Blocked') {

        echo "Your Examination Results are Currently Blocked<br>";

        echo "Please Contact the Registrar Office to Resolve this Issue<br>";

        exit;

    }
    if (isset($_SERVER['QUERY_STRING'])) {

        $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);

    }

    $key = $RegNo;

    include '../academic/includes/showexamresults.php';

    if ($examofficer <> 1) {

        include('../footer/footer.php');

    }
    ?>

</div>
<br><br>
<!--footer-->
<?php //include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<!--script for datatable-->
<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal( {
                        header: function ( row ) {
                            var data = row.data();
                            return 'Details for '+data[0]+' '+data[1];
                        }
                    } ),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                        tableClass: 'table'
                    } )
                }
            }
        } );
    } );
</script>
</body>
</html>
