<?php
require_once '../Connections/sessioncontrol.php';
require_once '../Connections/zalongwa.php';

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Accommodation';
$szTitle = 'Room Application Form';
$szSubSection = 'Application Form';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /* background-color: #009688; */
        }

        .card {
            /* background-color: #324359; */
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /* color: white; */
            padding: 0px;
            border-radius: 0px !important;
        }

        @media (max-width: 34em) {
            .card {
                margin-top: 20px;
            }
        }

        @media (max-width: 48em) {
            .card {
                margin-top: 20px;
            }
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>

    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'studentNavBar.php'; ?>

<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <h3 class="card-header">
                    Room Application Form</h3>
                <div class="card-block">
                    <?php
                    //mysql_select_db($database_zalongwa);
                    $query_AllocationCriteria = "SELECT CriteriaID, ShortName FROM criteria";
                    $AllocationCriteria = mysqli_query($zalongwa, $query_AllocationCriteria) or die(mysqli_error($zalongwa));
                    $row_AllocationCriteria = mysqli_fetch_assoc($AllocationCriteria);
                    $totalRows_AllocationCriteria = mysqli_num_rows($AllocationCriteria);

                    //mysql_select_db($database_zalongwa);
                    $query_Hostel = "SELECT HID, HName FROM hostel";
                    $Hostel = mysqli_query($zalongwa, $query_Hostel) or die(mysqli_error($zalongwa));
                    $row_Hostel = mysqli_fetch_assoc($Hostel);
                    $totalRows_Hostel = mysqli_num_rows($Hostel);

                    //mysql_select_db($database_zalongwa);
                    $query_RoomApplication = "SELECT RegNo, AppYear, AllCriteria, Hall FROM roomapplication";
                    $RoomApplication = mysqli_query($zalongwa, $query_RoomApplication) or die(mysqli_error($zalongwa));
                    $row_RoomApplication = mysqli_fetch_assoc($RoomApplication);
                    $totalRows_RoomApplication = mysqli_num_rows($RoomApplication);

                    //mysql_select_db($database_zalongwa);
                    $query_AYear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
                    $AYear = mysqli_query($zalongwa, $query_AYear) or die(mysqli_error($zalongwa));
                    $row_AYear = mysqli_fetch_assoc($AYear);
                    $totalRows_AYear = mysqli_num_rows($AYear);

                    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
                    {
                        $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

                        switch ($theType) {
                            case "text":
                                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                                break;
                            case "long":
                            case "int":
                                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                                break;
                            case "double":
                                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                                break;
                            case "date":
                                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                                break;
                            case "defined":
                                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                                break;
                        }
                        return $theValue;
                    }

                    $editFormAction = $_SERVER['PHP_SELF'];
                    if (isset($_SERVER['QUERY_STRING'])) {
                        $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
                    }

                    if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "editstudentRoomApplication")) {

                        #get criteria
                        $criteria = addslashes($_POST['AllCriteria']);
                        $regno = addslashes($_POST['regno']);
                        $appyear = addslashes($_POST['AppYear']);

                        $qupdate = "UPDATE roomapplication SET AllCriteria = '$criteria' WHERE RegNo='$regno' AND AppYear ='$appyear'";
                        $dbupdate = mysqli_query($zalongwa, $qupdate);
                    }

                    if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "studentRoomApplication")) {
                        //get academic year
                        $year = $_POST['AppYear'];
                        //query current Year
                        $qyear = "SELECT AYear from academicyear WHERE Status = 1";
                        $dbyear = mysqli_query($zalongwa, $qyear);
                        $row_year = mysqli_fetch_assoc($dbyear);
                        $currentYear = $row_year['AYear'];
                        if ($currentYear <> $year) {
                            echo "You cannot Apply for This Year:" . $year . "<br> Application Rejected";
                            exit;
                        } else {
                            $regno = addslashes($_POST['regno']);
                            $appyear = addslashes($_POST['AppYear']);
                            //validate this regno
                            $qregno = "select Name, RegNo from student where RegNo='$regno'";
                            $dbregno = mysqli_query($zalongwa, $qregno);

                            if (mysqli_num_rows($dbregno) > 0) {
                                #check if is already applied
                                $qapp = "SELECT * FROM roomapplication WHERE RegNo='$regno' AND AppYear = '$appyear'";
                                $dbapp = mysqli_query($zalongwa, $qapp);
                                $app_total = mysqli_num_rows($dbapp);

                                if ($app_total > 0) {
                                    $app_rows = mysqli_fetch_assoc($dbapp);
                                    $criteria = $app_rows['AllCriteria'];
                                    $app_AllocationCriteria = "SELECT CriteriaID, ShortName FROM criteria WHERE CriteriaID = '$criteria'";
                                    $Criteria = mysqli_query($zalongwa, $app_AllocationCriteria) or die(mysqli_error($zalongwa));
                                    $row_Criteria = mysqli_fetch_assoc($Criteria);
                                    echo "<div style='color: red'>";
                                    echo 'OOPS! The RegNo ' . $app_rows['RegNo'] . ' is already applied !';
                                    echo "</div>";
                                    ?>
                                    <form action="<?php echo $editFormAction; ?>" method="POST"
                                          name="editstudentRoomApplication" id="editstudentRoomApplication">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Application Year:</label>
                                            <select class="form-control" name="AppYear" id="AppYear">
                                                <option value="<?php echo $app_rows['AppYear'] ?>"><?php echo $app_rows['AppYear'] ?></option>
                                                <?php
                                                do {
                                                    ?>
                                                    <option value="<?php echo $row_AYear['AYear'] ?>"><?php echo $row_AYear['AYear'] ?></option>
                                                    <?php
                                                } while ($row_AYear = mysqli_fetch_assoc($AYear));

                                                $rows = mysqli_num_rows($AYear);
                                                if ($rows > 0) {
                                                    mysqli_data_seek($AYear, 0);
                                                    $row_AYear = mysqli_fetch_assoc($AYear);
                                                }
                                                ?>
                                            </select>
                                            <input type="hidden" name="regno" value="<?php echo $RegNo; ?>"
                                                   style="width: 200px; padding: 2px">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Application Category:</label>
                                            <select class="form-control" name="AllCriteria" id="select2" required>
                                                <option value="<?php echo $row_Criteria['CriteriaID'] ?>"><?php echo $row_Criteria['ShortName'] ?></option>
                                                <?php
                                                do {
                                                    ?>
                                                    <option value="<?php echo $row_AllocationCriteria['CriteriaID'] ?>"><?php echo $row_AllocationCriteria['ShortName'] ?></option>
                                                    <?php
                                                } while ($row_AllocationCriteria = mysqli_fetch_assoc($AllocationCriteria));

                                                $rows = mysqli_num_rows($AllocationCriteria);
                                                if ($rows > 0) {
                                                    mysqli_data_seek($AllocationCriteria, 0);
                                                    $row_AllocationCriteria = mysqli_fetch_assoc($AllocationCriteria);
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Other Information</label>
                                            <textarea class="form-control" name="Hall" value="<?= $app_rows['Hall'] ?>"
                                                      id="Hall" required rows="3"></textarea>
                                        </div>

                                        <div class="form-group">
                                            Please Re-check Your Entries; Should Any Form of Cheating Found, Your
                                            Application Will be Rejected
                                        </div>
                                        <input class="btn btn-success btn-md btn-block" type="submit" name="Submit"
                                               value="Edit Form"></td>
                                    </form>
                                    <?php
                                    exit;
                                } else {
                                    $insertSQL = sprintf("INSERT INTO roomapplication (RegNo, AppYear, AllCriteria, Hall, Received, Processed) VALUES (%s, %s, %s, %s, now(), now())",
                                        GetSQLValueString($_POST['regno'], "text"),
                                        GetSQLValueString($_POST['AppYear'], "date"),
                                        GetSQLValueString($_POST['AllCriteria'], "text"),
                                        GetSQLValueString($_POST['Hall'], "text"));
                                    //mysql_select_db($database_zalongwa);
                                    $Result1 = mysqli_query($zalongwa, $insertSQL);
                                }
                            } else {
                                echo 'OOPS! The RegNo ' . $regno . ' Does not Exist !';
                            }
                        }

                        $insertGoTo = "roomApplicationform.php";
                        if (isset($_SERVER['QUERY_STRING'])) {
                            $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
                            $insertGoTo .= $_SERVER['QUERY_STRING'];
                        }
                    }

                    $leo = date("F");
                    if ($leo = 'January' || $leo = 'February' || $leo = 'March' || $leo = 'April' || $leo = 'May' || $leo = 'June') {
                        $today = date("Y") - 1;
                        $kesho = date("Y");
                    } else {
                        $today = date("Y");
                        $kesho = date("Y") + 1;
                    }
                    ?>

                    <form action="<?php echo $editFormAction; ?>" method="POST" name="studentRoomApplication"
                          id="studentRoomApplication">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Application Year:</label>
                            <select class="form-control" name="AppYear" id="AppYear" required>
                                <option value="" disabled="disabled" selected="selected"> -- select --</option>
                                <?php
                                do {
                                    ?>
                                    <option value="<?php echo $row_AYear['AYear'] ?>"><?php echo $row_AYear['AYear'] ?></option>
                                    <?php
                                } while ($row_AYear = mysqli_fetch_assoc($AYear));

                                $rows = mysqli_num_rows($AYear);
                                if ($rows > 0) {
                                    mysqli_data_seek($AYear, 0);
                                    $row_AYear = mysqli_fetch_assoc($AYear);
                                }
                                ?>
                            </select>
                        </div>
                        <input name="regno" id="regno" value="<?php echo $RegNo ?>" type="hidden" required>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Application Category :</label>
                            <select class="form-control" name="AllCriteria" id="select2" required>
                                <option value="" disabled="disabled" selected="selected"> -- select --</option>
                                <?php
                                do {
                                    ?>
                                    <option value="<?php echo $row_AllocationCriteria['CriteriaID'] ?>"><?php echo $row_AllocationCriteria['ShortName'] ?></option>
                                    <?php
                                } while ($row_AllocationCriteria = mysqli_fetch_assoc($AllocationCriteria));

                                $rows = mysqli_num_rows($AllocationCriteria);
                                if ($rows > 0) {
                                    mysqli_data_seek($AllocationCriteria, 0);
                                    $row_AllocationCriteria = mysqli_fetch_assoc($AllocationCriteria);
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Other Information</label>
                            <textarea class="form-control" name="Hall" id="Hall" rows="3"></textarea>
                        </div>
                        <input class="btn btn-success btn-md btn-block" type="submit" name="Submit" value="Submit  Form"
                               title='Please Re-check Your Entries. Should Any Form of Cheating Found, Your Application Will be Rejected'>
                        <p><input type="hidden" name="MM_insert" value="studentRoomApplication"></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>

