<?php
require_once '../Connections/sessioncontrol.php';
require_once '../Connections/zalongwa.php';

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;

$szSection = 'Academic Records';

$szTitle = 'Academic Records: Course Registered';

$szSubSection = 'Exam Registered';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
</head>
<body>
<!-- navbar -->
<?php include 'studentNavBar.php'; ?>

<div class="container-flex">
    <br>
</div>
<div class="container ">
    <h3 class="h3"><?php echo $szTitle; ?></h3>
    <?php
    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {

        $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

        switch ($theType) {

            case "text":

                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";

                break;

            case "long":

            case "int":

                $theValue = ($theValue != "") ? intval($theValue) : "NULL";

                break;

            case "double":

                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";

                break;

            case "date":

                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";

                break;

            case "defined":

                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;

                break;

        }

        return $theValue;

    }

    ?>

    <?php

    if ((isset($_GET['CourseCode'])) && ($_GET['CourseCode'] != "")) {

        $coursecode = addslashes($_GET['CourseCode']);

        $regno = addslashes($_GET['RegNo']);

        $deleteSQL = "DELETE FROM examregister WHERE CourseCode='$coursecode' AND checked=0 AND RegNo='$regno'";


        //mysql_select_db($database_zalongwa, $zalongwa);

        $Result1 = mysqli_query($zalongwa, $deleteSQL) or die('This Records is Locked by the Examination Officer');


        //$deleteGoTo = "studentAcademic.php";

        if (isset($_SERVER['QUERY_STRING'])) {

            $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";

            $deleteGoTo .= $_SERVER['QUERY_STRING'];

        }

        // header(sprintf("Location: %s", $deleteGoTo));

    }

    ?>



    <?php

    $currentPage = $_SERVER["PHP_SELF"];


    $maxRows_coursecandidate = 13;

    $pageNum_coursecandidate = 0;

    if (isset($_GET['pageNum_coursecandidate'])) {

        $pageNum_coursecandidate = $_GET['pageNum_coursecandidate'];

    }

    $startRow_coursecandidate = $pageNum_coursecandidate * $maxRows_coursecandidate;


    $colname_coursecandidate = "1";

    if (isset($_COOKIE['RegNo'])) {

        $colname_coursecandidate = (get_magic_quotes_gpc()) ? $_COOKIE['RegNo'] : addslashes($_COOKIE['RegNo']);

    }

    //mysql_select_db($database_zalongwa, $zalongwa);

    $query_coursecandidate = "SELECT 

								examregister.CourseCode, 

								course.Units, 

								course.CourseName, 

								examregister.RegNo,

								examregister.AYear

						FROM course INNER JOIN examregister ON course.CourseCode = examregister.CourseCode

							WHERE (((examregister.RegNo)='$RegNo')) ORDER BY AYear DESC, CourseCode ASC";

    $query_limit_coursecandidate = sprintf("%s LIMIT %d, %d", $query_coursecandidate, $startRow_coursecandidate, $maxRows_coursecandidate);

    $coursecandidate = mysqli_query($zalongwa, $query_limit_coursecandidate) or die(mysqli_error($zalongwa));

    $row_coursecandidate = mysqli_fetch_assoc($coursecandidate);


    if (isset($_GET['totalRows_coursecandidate'])) {

        $totalRows_coursecandidate = $_GET['totalRows_coursecandidate'];

    } else {

        $all_coursecandidate = mysqli_query($zalongwa, $query_coursecandidate);

        $totalRows_coursecandidate = mysqli_num_rows($all_coursecandidate);

    }

    $totalPages_coursecandidate = ceil($totalRows_coursecandidate / $maxRows_coursecandidate) - 1;


    $queryString_coursecandidate = "";

    if (!empty($_SERVER['QUERY_STRING'])) {

        $params = explode("&", $_SERVER['QUERY_STRING']);

        $newParams = array();

        foreach ($params as $param) {

            if (stristr($param, "pageNum_coursecandidate") == false &&

                stristr($param, "totalRows_coursecandidate") == false) {

                array_push($newParams, $param);

            }

        }

        if (count($newParams) != 0) {

            $queryString_coursecandidate = "&" . htmlentities(implode("&", $newParams));

        }

    }

    $queryString_coursecandidate = sprintf("&totalRows_coursecandidate=%d%s", $totalRows_coursecandidate, $queryString_coursecandidate);


    @$CourseCode = $_GET['CourseCode'];


    $editFormAction = $_SERVER['PHP_SELF'];

    if (isset($_SERVER['QUERY_STRING'])) {

        $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);

    }


    if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmCourseRegister")) {

        $insertSQL = sprintf("INSERT INTO coursecandidate (RegNo, CourseCode) VALUES (%s, %s)",

            GetSQLValueString($_POST['regno'], "text"),

            GetSQLValueString($_POST['coursecode'], "text"));


        //mysql_select_db($database_zalongwa, $zalongwa);

        $Result1 = mysqli_query($zalongwa, $insertSQL) or die("You have alrady registered for this course, <br>duplicate Records are not allowed");


        $insertGoTo = "studentindex.php";

        if (isset($_SERVER['QUERY_STRING'])) {

            $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";

            $insertGoTo .= $_SERVER['QUERY_STRING'];

        }

        header(sprintf("Location: %s", $insertGoTo));

    }

    ?>
    <?php echo "<div style='margin: 20px 0'><a class='btn btn-success' href=\"studentCourselist.php\"><span class=\"fa fa-plus-circle\" aria-hidden=\"true\"></span> Add New Course </a></div>"; ?>

    <table class="table table-striped table-bordered table-responsive" width="100%" cellspacing="0">
        <thead class="table-inverse">
        <tr>
            <th width="10%">Drop</th>
            <th width="10%">Year</th>
            <th width="20%">Course</th>
            <th width="10%">Units</th>
            <th width="50%">Course Title</th>
        </tr>
        </thead>
        <tbody>
        <?php do { ?>
            <tr>
                <td><?php $CourseCode = $row_coursecandidate['CourseCode'];
                    echo "<a class='btn btn-danger' href=\"studentAcademic.php?CourseCode=$CourseCode&RegNo=$RegNo\"> Drop </a>"; ?></td>

                <td><?php echo $row_coursecandidate['AYear']; ?></td>

                <td><?php echo $row_coursecandidate['CourseCode']; ?></td>

                <td>
                    <div align="center"><?php echo $row_coursecandidate['Units']; ?></div>
                </td>

                <td><?php echo $row_coursecandidate['CourseName']; ?></td>
            </tr>

        <?php } while ($row_coursecandidate = mysqli_fetch_assoc($coursecandidate)); ?>
        </tbody>
    </table>

    <p>
        <a href="<?php printf("%s?pageNum_coursecandidate=%d%s", $currentPage, max(0, $pageNum_coursecandidate - 1), $queryString_coursecandidate); ?>">Previous
            Page</a> <span class="style64"><span
                    class="style34">Records: <?php echo min($startRow_coursecandidate + $maxRows_coursecandidate, $totalRows_coursecandidate) ?>
                /<?php echo $totalRows_coursecandidate ?></span> <span class="style1">...</span></span><a
                href="<?php printf("%s?pageNum_coursecandidate=%d%s", $currentPage, min($totalPages_coursecandidate, $pageNum_coursecandidate + 1), $queryString_coursecandidate); ?>">Next
            Page</a> <span class="style64 style1"></span>

</div>
<br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<!--script for datatable-->
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details for ' + data[0] + ' ' + data[1];
                        }
                    }),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            }
        });
    });
</script>
</body>
</html>
