<!-- navbar -->
<nav class="navbar  navbar-default navbar-toggleable-md navbar-light bg-faded  sticky-top">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon "></span>
    </button>
    <a class="navbar-brand nav-link" href="studentindex.php"><img width="70" src="./img/logo.svg"/> SARIS SYSTEM  </a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav d-flex flex-row flex-nowrap ml-auto mr-sm-5 mr-md-5 mr-lg-0">
            <li class="nav-item ">
                <a class="navbar-brand nav-link " href="#"><h6> Welcome <b> <?php echo $name ?> </b></h6></a>
            </li>
            <li class="nav-item">
                <a class="navbar-brand nav-link " href="../signout.php"><img class="rounded-circle" width="40"
                                                                             src="./img/user.svg"/> Logout</a>
            </li>
            <li class="nav-item">
                <a class="navbar-brand nav-link" href="studentUserManual.php"> Help</a>
            </li>
        </ul>

    </div>
</nav>
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="studentindex.php">Home</a>
    <a class="breadcrumb-item" href="javascript:history.back();">Back</a>
    <span class="breadcrumb-item active"><?php echo $szSection; ?></span>
</nav>