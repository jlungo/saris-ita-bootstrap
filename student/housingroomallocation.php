<?php

#get connected to the database and verfy current session
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

# include the header
global $szSection, $szSubSection;
$szSection = 'Accommodation';
$szSubSection = 'Allocation Report';
$szTitle = 'Room Allocation';

?>

<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
    $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

    switch ($theType) {
        case "text":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "long":
        case "int":
            $theValue = ($theValue != "") ? intval($theValue) : "NULL";
            break;
        case "double":
            $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
            break;
        case "date":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "defined":
            $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
            break;
    }
    return $theValue;
}

//Print Room Allocation Report

$currentyear = mysqli_query($zalongwa, "SELECT AYear FROM academicyear WHERE Status=1");
list($year) = mysqli_fetch_array($currentyear);

//require_once('../Connections/zalongwa.php');
$sql = "SELECT student.Id, student.Name, student.Sex, student.ProgrammeofStudy, student.Faculty, student.Sponsor, 
				student.EntryYear, student.RegNo, hostel.HName, allocation.RNumber, allocation.AYear, hostel.HID
				FROM (allocation RIGHT JOIN student ON allocation.RegNo = student.RegNo) 
				LEFT JOIN hostel ON allocation.HID = hostel.HID
				WHERE (hostel.HName<>'' AND allocation.AYear='$year') 
				AND (student.RegNo = '$RegNo')";

$result = @mysqli_query($zalongwa, $sql) or die("Cannot query the database.<br>" . mysqli_error($zalongwa));
$query = @mysqli_query($zalongwa, $sql) or die("Cannot query the database.<br>" . mysqli_error($zalongwa));

$all_query = mysqli_query($zalongwa, $query);
$totalRows_query = mysqli_num_rows($query);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="https://cdn.datatables.net/responsive/2.2.0/css/responsive.bootstrap4.min.css">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>

    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>


</head>
<body>
<!-- navbar -->
<?php include 'studentNavBar.php'; ?>

<div class="container-flex">
</div>
<div class="container">

    <?php
    if (mysqli_num_rows($query) > 0){
    echo "<p>Congratulations! You have been allocated room.</p>";
    ?>
    <table id="example" class="table table-striped table-bordered table-responsive" width="100%" cellspacing="0">

        <thead class="table-inverse">
        <tr>
            <th> S/No</th>
            <th> Name</th>
            <th> Sex</th>
            <th> Degree</th>
            <th> Sponsor</th>
            <th> Registered</th>
            <th> Hostel</th>
            <th> Room No</th>
            <th> Academic Year</th>
        </tr>
        </thead>

        <?php
        $i = 1;
        while ($result = mysqli_fetch_array($query)) {
            $id = stripslashes($result["Id"]);
            $year = stripslashes($result["AYear"]);
            $Name = stripslashes($result["Name"]);
            $RegNo = stripslashes($result["RegNo"]);
            $sex = stripslashes($result["Sex"]);
            $degree = stripslashes($result["ProgrammeofStudy"]);
            $faculty = stripslashes($result["Faculty"]);
            $sponsor = stripslashes($result["Sponsor"]);
            $entryyear = stripslashes($result["EntryYear"]);
            $hall = stripslashes($result["HName"]);
            $citeria = stripslashes($result["RNumber"]);
            //search degree name
            $qdegree = "SELECT ProgrammeName from programme WHERE ProgrammeCode='$degree'";
            $dbdegree = mysqli_query($zalongwa, $qdegree);
            $row_degree = mysqli_fetch_array($dbdegree);
            $degreename = $row_degree['ProgrammeName'];

            $class = (fmod($i, 2) == '0') ? "bgcolor='#CCCCCC'" : "bgcolor='#ffffff'";

            echo "
        <tr $class>
            <td>$i</td>
            <td>$Name<br/><b>$RegNo</b></td>
            <td>$sex</td>
            <td>$degreename</td>
            <td>$sponsor</td>
            <td>$entryyear</td>
            <td>$hall</td>
            <td>$citeria</td>
            <td>$year</td>
            ";

            echo "
        </tr>
        ";

            $i = $i + 1;
        }
        echo "
    </table>
    ";
        }
        else {
            echo "Sorry, You have not been allocated<br>";
        }

        ?>
</div>
<br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<!--script for datatable-->
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details for ' + data[0] + ' ' + data[1];
                        }
                    }),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            }
        });
    });
</script>
</body>
</html>