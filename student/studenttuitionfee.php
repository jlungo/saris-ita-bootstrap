<?php

require_once('../Connections/zalongwa.php');

require_once('../Connections/sessioncontrol.php');

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;

$szSection = 'Financial Records';

$szTitle = 'Tuition Fee Payments Reports';

$szSubSection = 'Tuition Fee';

#Get all student in this cohot

$qstudent = "SELECT Name, RegNo from student WHERE RegNo = '$RegNo'";

$dbstudent = mysqli_query($zalongwa, $qstudent);

$totalstudent = mysqli_num_rows($dbstudent);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>

    <style></style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>


</head>
<body>
<!-- navbar -->
<?php include 'studentNavBar.php'; ?>

<div class="container-flex">
    <br>
</div>
<div class="container ">
    <h3 class="h3"><?php echo $szTitle; ?></h3>
    <?php
    if ($totalstudent > 0) {

        ?>

        <table id="example" class="table table-striped table-bordered nowrap" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>S/No</th>
                <th>Candidate Name</th>
                <th>RegNo</th>
                <th>Amount</th>
                <th>Receipt No</th>
                <th>Receipt Date</th>
                <th>Recorder</th>
                <th>Recorded On</th>
            </tr>
            </thead>
            <tbody>
            <?php

            $i = 1;

            $grandtotalpaid = 0;

            $totalpaid = 0;

            while ($rowstudent = mysqli_fetch_array($dbstudent)) {

                $name = $rowstudent['Name'];

                $regno = $rowstudent['RegNo'];


                //query caution fee paid

                $qcautionfee = "SELECT * FROM tblcautionfee WHERE RegNo = '$RegNo' AND Paytype = 4";

                $dbcautionfee = mysqli_query($zalongwa, $qcautionfee);

                $rowcautionfee = mysqli_fetch_array($dbcautionfee);

                //print student report

                if (intval($rowcautionfee['Amount']) > 0) {

                    ?>

                    <?php do { ?>

                        <tr>

                            <td> <?php echo $i ?> </td>

                            <td nowrap>  <?php echo $name ?> </td>

                            <td nowrap>  <?php echo $regno ?> </td>

                            <td nowrap>  <?php echo $rowcautionfee['Amount'];

                                $amount = $rowcautionfee['Amount'];

                                $totalpaid = $totalpaid + $amount;

                                //number_format($totalpaid,2,'.',',') ?> </td>

                            <td nowrap>  <?php echo $rowcautionfee['ReceiptNo'] ?> </td>

                            <td nowrap>  <?php echo $rowcautionfee['ReceiptDate'] ?> </td>

                            <td nowrap>  <?php echo $rowcautionfee['user'] ?> </td>

                            <td nowrap>  <?php echo $rowcautionfee['Received'] ?> </td>

                        </tr>
                        <?php $i = $i + 1;

                    } while ($rowcautionfee = mysqli_fetch_assoc($dbcautionfee)); ?>

                <?php }

            }

            ?>

            </tbody>
            <tfoot>
            <tr>
                <th colspan="3">Grand Total:</th>
                <th><?php echo number_format($totalpaid, 2, '.', ',') ?></th>
                <th colspan="4">End of Report</th>
            </tr>
            </tfoot>
        </table>
        <?php
    }
    ?>
</div>
<br><br>

<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<!--script for datatable-->
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details for ' + data[0] + ' ' + data[1];
                        }
                    }),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            }
        });
    });
</script>
</body>
</html>