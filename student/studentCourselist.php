<?php
require_once '../Connections/sessioncontrol.php';
require_once '../Connections/zalongwa.php';

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;

$szSection = 'Academic Records';

$szTitle = 'Exam Registration: Please Pick a Course';

$szSubSection = 'Course Roster';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>

</head>
<body>
<!-- navbar -->
<?php include 'studentNavBar.php'; ?>

<div class="container-flex">
    <br>
</div>
<div class="container ">
    <h3 class="h3">Exam Registration: Please Pick a Course</h3>
    <?php

    $currentPage = $_SERVER["PHP_SELF"];

    $maxRows_courselist = 13;

    $pageNum_courselist = 0;

    if (isset($_GET['pageNum_courselist'])) {

        $pageNum_courselist = $_GET['pageNum_courselist'];

    }

    $startRow_courselist = $pageNum_courselist * $maxRows_courselist;

    //mysql_select_db($database_zalongwa, $zalongwa);

    if (isset($_GET['course'])) {

        $key = $_GET['course'];

        $query_courselist = "SELECT CourseCode, CourseName, Units FROM course WHERE CourseCode Like '%$key%' ORDER BY CourseCode";

    } else {

        $query_courselist = "SELECT CourseCode, CourseName, Units FROM course ORDER BY CourseCode";

    }


    $query_limit_courselist = sprintf("%s LIMIT %d, %d", $query_courselist, $startRow_courselist, $maxRows_courselist);

    $courselist = mysqli_query($zalongwa, $query_limit_courselist) or die(mysqli_error($zalongwa));

    $row_courselist = mysqli_fetch_assoc($courselist);


    if (isset($_GET['totalRows_courselist'])) {

        $totalRows_courselist = $_GET['totalRows_courselist'];

    } else {

        $all_courselist = mysqli_query($zalongwa, $query_courselist);

        $totalRows_courselist = mysqli_num_rows($all_courselist);

    }

    $totalPages_courselist = ceil($totalRows_courselist / $maxRows_courselist) - 1;


    $queryString_courselist = "";

    if (!empty($_SERVER['QUERY_STRING'])) {

        $params = explode("&", $_SERVER['QUERY_STRING']);

        $newParams = array();

        foreach ($params as $param) {

            if (stristr($param, "pageNum_courselist") == false &&

                stristr($param, "totalRows_courselist") == false
            ) {

                array_push($newParams, $param);

            }

        }

        if (count($newParams) != 0) {

            $queryString_courselist = "&" . htmlentities(implode("&", $newParams));

        }

    }

    $queryString_courselist = sprintf("&totalRows_courselist=%d%s", $totalRows_courselist, $queryString_courselist);


    ?>
    <div class="row " style="margin: 20px 0;">
        <div class="col-md-5">
            <form name="form1" method="get" action="studentCourselist.php">
                <div class="input-group">
                    <input name="course" type="text" id="course" class="form-control" placeholder="Search Course Code"
                           maxlength="50">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" name="Submit">
                        <i class="fa fa-search"></i>Search
                    </button>
                </span>
                </div>
            </form>
        </div>
    </div>
    <table class="table table-striped table-bordered table-responsive" width="100%" cellspacing="0">
        <thead class="table-inverse">
        <tr>
            <th width="10%">Pick</th>
            <th width="20%">Course Code</th>
            <th width="10%">Units</th>
            <th width="40%">Course Description</th>
        </tr>
        </thead>
        <tbody>
        <?php do { ?>
            <tr>
                <td><?php $CourseCode = $row_courselist['CourseCode'];
                    echo "<a class='btn btn-primary' href=\"studentcourseregister.php?CourseCode=$CourseCode\"> Pick </a>"; ?></td>
                <td nowrap><?php echo $row_courselist['CourseCode']; ?></td>
                <td><?php echo $row_courselist['Units']; ?></td>
                <td><?php echo $row_courselist['CourseName']; ?></td>
            </tr>
        <?php } while ($row_courselist = mysqli_fetch_assoc($courselist)); ?>
        </tbody>
    </table>
    <p>
        <a href="<?php printf("%s?pageNum_courselist=%d%s", $currentPage, max(0, $pageNum_courselist - 1), $queryString_courselist); ?>">Previous
            Page</a> <span class="style66"><span class="style1">....</span><span class="style34">Record: <span
                        class="style67"><span
                            class="style34"><?php echo min($startRow_courselist + $maxRows_courselist, $totalRows_courselist) ?></span></span> of <?php echo $totalRows_courselist ?> </span><span
                    class="style1">......</span></span><a
                href="<?php printf("%s?pageNum_courselist=%d%s", $currentPage, min($totalPages_courselist, $pageNum_courselist + 1), $queryString_courselist); ?>">Next
            Page</a></p>

    <?php
    mysqli_free_result($courselist);
    ?>
</div>
<br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<!--script for datatable-->
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details for ' + data[0] + ' ' + data[1];
                        }
                    }),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            }
        });
    });
</script>
</body>
</html>
