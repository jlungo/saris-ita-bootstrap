
/**
 * @author Lackson David <lacksinho@gmail.com>
 * jquery function
 * */

$(document).ready(function() {
   $('#fc').hide();
$('#lect').hide();
$('#dept').hide();

$('#filter').change(function(){
   onchange_val = $(this).val();

   if(onchange_val == 3){
   $('#lect').show();
   $('#fc').hide();
   $('#pr').hide();
   $('#dept').hide();
           $('#prs').hide();
   }else if(onchange_val == 2){
	   $('#lect').hide();
	   $('#fc').show();
	   $('#pr').hide();
       $('#prs').hide();
       $('#dept').hide();
           
   }else if(onchange_val == 1){
	   $('#lect').hide();
	   $('#fc').hide();
	   $('#pr').show();
       $('#prs').show();
       $('#dept').hide();
   
   }else if(onchange_val == 4){
	   $('#lect').hide();
	   $('#fc').hide();
	   $('#pr').hide();
           $('#prs').hide();
           $('#dept').show();
   
   }
   });

// start arrange cell interval for timetable
    var single = 60;

//period with 1hr interval
    $('.view_timetable').find('div[class=one]').each(function() {
        single = $(this).width();

    });

    // period with 3hrs interval
    $('.view_timetable').find('div[class=three]').each(function() {
        $(this).width(3 * single); // overwrite the width to fix 3 td

        width = $(this).width();
        offset = $(this).position();  // get position of the div
        height = $(this).height(); // get height of the div
        var ll = 0;
        $($(this).parent().next()).find('div').each(function() {  // loop all div in the next td
            $(this).css('position', 'relative');
            $(this).css('z-index', '2000');

            off = $(this).position();  // get position of the div in the next td
            he = $(this).height();
            if (width > single) {
                if (off.top == offset.top) {  // if the div fall in the same possition append div aon top 

                    ass = height;

                    $('<div class="wiz" style="position:relative;z-index:2000;height:' + ass + 'px">ll</div>').insertBefore(this); // insert div before the overlaping div
                }
            }
        });

        //loop next td /// the 3rd td, the same procedure as above
        $($(this).parent().next().next()).find('div').each(function() {
            $(this).css('position', 'relative');
            $(this).css('z-index', '2000');
            // alert('sdsd');
            off = $(this).position();
            he = $(this).height();
            if (width > single) {
                if (off.top == offset.top) {

                    ass = height;

                    $('<div class="wiz" style="position:relative;z-index:3000;height:' + ass + 'px"></div>').insertBefore(this);
                }
            }
        });
    });



    // deal with period of 2hrs interval
    $('.view_timetable').find('div[class=two]').each(function() {

        $(this).width(2 * single);
        width = $(this).width();

        //$(this).append($(this).width());
        offset = $(this).position();
        height = $(this).height();
        var ll = 0;
        $($(this).parent().next()).find('div').each(function() {
            $(this).css('position', 'relative');
            $(this).css('z-index', '2000');

            off = $(this).position();
            he = $(this).height();
            if (width > single) {
                if (off.top == offset.top) {

                    ass = height;

                    $('<div class="wiz" style="position:relative;z-index:2000;height:' + ass + 'px"></div>').insertBefore(this);
                }
            }
        });



    });
});



