/**
 * function showManner()
 * ajax function to display drop menu on selected value
 * author Lackson David <lacksinho@gmail.com>
 * written on Saturday,April 13 2013
 * reuse it when applicable
 * */

function Ajax(){
	if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
   return xmlhttp;
	}
	
function showManner(str)
{
if (str=="")
  {
  document.getElementById("entry").innerHTML="";
  return;
  } 
   var xmlhttp= Ajax();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("entry").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","showManner.php?q="+str,true);
xmlhttp.send(null);
}

 
function showPosition(str,edit)
{
if (str=="")
  {
  document.getElementById("position").innerHTML="";
  return;
  } 
   var xmlhttp= Ajax();
   document.getElementById("position").innerHTML=''
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("position").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","showPosition.php?q="+str+"&edit="+edit,true);
xmlhttp.send(null);
}

function showFacDept(str,edit)
{
if (str=="")
  {
  document.getElementById("fac_dep").innerHTML="";
  return;
  } 
   var xmlhttp= Ajax();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("fac_dep").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","showFacDept.php?q="+str+"&edit="+edit,true);
xmlhttp.send(null);
}

/**
 * unblockrole function
 */
function unblockrole(username, role) {
	var xmlhttp = Ajax();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			window.location.reload();
			
		}
	};
	xmlhttp.open("GET", "processrole.php?user=" + username + "&block=0&role="
			+ role, true);
	xmlhttp.send(null);
	document.getElementById("sms").innerHTML = '<img src="../js/images/load2.gif" width="70" height="70"/>';
}
/**
 * blockrole function
 */
function blockrole(username, role) {
	var xmlhttp = Ajax();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			window.location.reload();
		}
	};
	xmlhttp.open("GET", "processrole.php?user=" + username + "&block=1&role="
			+ role, true);
	xmlhttp.send(null);
	document.getElementById("sms").innerHTML = '<img src="../js/images/load2.gif" width="70" height="70"/>';
}

function filterResult(str,key,user)
{
var url='';	
if(user==1){
	url= "admissionExamResult.php?year="+str+"&key="+key;
	}else{
url= "studentexamresult.php?year="+str+"&key="+key;		
		}	
window.location =url;
}




