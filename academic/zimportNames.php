<?php 
	#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    # initialise globals
	include('../academic/lecturerMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Examination';
	$szSubSection = 'Grade Book';
	$szTitle = 'Import MS Excel Database Data';
	include('../academic/lecturerheader.php');
?>
<?php
	#STEP 1.0 Upload data file
	# delete old files
	@unlink("./temp/zalongwa.xls");
	#constants
	$fileavailable=0;

	#validate the file
	$file_name = ($_POST['userfile']);
	$overwrite = addslashes($_POST['radiobutton']);
		
	$file = $_FILES['userfile']['name'];
	//The original name of the file on the client machine. 
	$filetype = $_FILES['userfile']['type'];
	//The mime type of the file, if the browser provided this information. An example would be "image/gif". 
	$filesize = $_FILES['userfile']['size'];
	//The size, in bytes, of the uploaded file. 
	$filetmp = $_FILES['userfile']['tmp_name'];
	//The temporary filename of the file in which the uploaded file was stored on the server. 
	$filetype_error = $_FILES['userfile']['error'];
	$filename=time().$_FILES['userfile']['name'];
?>
<?php 
	// In PHP earlier then 4.1.0, $HTTP_POST_FILES  should be used instead of $_FILES.
	if (is_uploaded_file($filetmp)) {
		$filename=time().$file;
	    copy($filetmp, "$filename");
	} 
	else {
	   echo "File not uploaded, see the error: <br>".$filetype_error;
	}
	move_uploaded_file($filetmp, "./temp/$filename");

	#check file extension
   	$str = $filename;
	$i = strrpos($str,".");
	if (!$i) { return ""; }
	
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);	
    $pext = strtolower($ext);
   if ($pext != "xls")
    {
	print "<h2>ERROR</h2>File Extension Unknown.<br>";
        print "<p>Please Upload a File with the Extension .csv ONLY<br>";
	print "To convert your Excel File to csv, go to File -> Save As, then Select Save as Type CSV (Comma delimeded) (*.csv)</p>\n";
        print "The file you uploaded have this extension: $pext</p>\n";
	echo '<meta http-equiv = "refresh" content ="10; url = zalongwaimport.php">';		
	include('../footer/footer.php');
		unlink($filename);
        exit();
    }

?>
<?php 
	if ($pext == "xls") {
		$fileavailable=1;
	}
	#STEP 2.0 exceute sql scripts
	if ($fileavailable==1){
		echo "<strong>Data Import in Process</strong><br /><br>"; 
		echo "Progress Status.............<br>"; 
		$fcontents = file ("./temp/$filename"); 
	  # expects the csv file to be in the same dir as this script
	  #get impo year
		  /*
	      $line = trim($fcontents[0], ',');
		  $arr = explode(",", $line); 
		  $impayear = trim($arr[1]);
		  */
		  $impayear = addslashes($_POST['ayear']);
		  
	  #get impo semester
	 	 /*
		$line = trim($fcontents[1], ',');
		 $arr = explode(",", $line); 
		 $impsem = trim($arr[1]);
		  */
		  $impsem = addslashes($_POST['sem']);
	  #get impo coursecode
	  	/*
		$line = trim($fcontents[2], ',');
		$arr = explode(",", $line); 
		$impcourse = trim($arr[1]);
		  */
		  $impcourse = addslashes($_POST['coursecode']);
		  	 
	  #get impo examcategory
	  /*
	      $line = trim($fcontents[3], ',');
		  $arr = explode(",", $line); 
		  $impexamcat = trim($arr[1]);
		  */
		  $impexamcat = addslashes($_POST['examcat']);
		  /*
		  $impexamcat = strtolower($impexamcat);
		  if ($impexamcat == 'cwk'){
		  		$impexamcat = 4;
		  }elseif($impexamcat=='exam'){
		   		$impexamcat = 5;
		  }else{
		 		$impexamcat = $impexamcat;
		  }
		  */
		  $today = date('Y-m-d');
	/** PHPExcel */	  
	require_once '../Classes/PHPExcel/IOFactory.php';	
	$objPHPExcel = PHPExcel_IOFactory::load("./temp/$filename");
  	foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) 
  	{
  	//echo 'umeona hii - '.$impcourse;	
    		$worksheetTitle     = $worksheet->getTitle();
    		$highestRow         = $worksheet->getHighestRow(); // e.g. 10
    		$highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
    		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
    		$nrColumns = ord($highestColumn) - 64;
    		echo "<br>The worksheet ".$worksheetTitle." has ";
    		echo $nrColumns . ' columns (A-' . $highestColumn . ') ';
    		echo ' and ' . $highestRow . ' row.';
    		
    		echo '<br>Data: <table border="1"><tr>';
    		for ($row = 1; $row <= $highestRow; ++ $row) {
        		echo '<tr>';
        		for ($col = 0; $col < $highestColumnIndex; ++ $col) {
            			$cell = $worksheet->getCellByColumnAndRow($col, $row);
           			$val = $cell->getValue();
            			//$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
            			
            			if(($col==0) and ($row >= 3)){
            			         #check if regno exists
           				$qregno="SELECT Name, RegNo FROM student WHERE RegNo='$val'";
           				$dbregno=mysql_query($qregno);
           				$regct=mysql_num_rows($dbregno);
			   		if ($regct>0){
			    			echo '<td>' . $val . '</td>';
			    		}
			    		else{
			    			echo '<td> RegNo '.$val.' Doesnot exists! </td>';
			    		}
				}
				else{
					echo '<td>' . $val . '</td>';
				}
			}
        		echo '</tr>';
    		}
    		echo '</table>';		
    		
    		for($i=1; $i<=$highestRow; ++$i) { 
    			//for ($j=0; $j<$highestColumnIndex; ++$j) {
    			echo '<br>';
    		
    		        $cell = $worksheet->getCellByColumnAndRow(0, $i);
           			$val0 = $cell->getValue();
           			//echo 'A'.$i.' = '.$val0.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(1, $i);
           			$val1 = $cell->getValue();
           			//echo 'B'.$i.' = '.$val1.'<br>';

           			
           			$cell = $worksheet->getCellByColumnAndRow(2, $i);
           			$val2 = $cell->getValue();
           			//echo 'C'.$i.' = '.$val2.'<br>';           			
           			
           			
           			$cell = $worksheet->getCellByColumnAndRow(3, $i);
           			$val3 = $cell->getValue();
           			//echo 'D'.$i.' = '.$val3.'<br>';           			

           			
           			$cell = $worksheet->getCellByColumnAndRow(4, $i);
           			$val4 = $cell->getValue();
           			//echo 'E'.$i.' = '.$val4.'<br>';

           			
           			$cell = $worksheet->getCellByColumnAndRow(5, $i);
           			$val5 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(6, $i);
           			$val6 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(7, $i);
           			$val7 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(8, $i);
           			$val8 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(9, $i);
           			$val9 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(10, $i);
           			$val10 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(11, $i);
           			$val11 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(12, $i);
           			$val12 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(13, $i);
           			$val13 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(14, $i);
           			$val14 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(15, $i);
           			$val15 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(16, $i);
           			$val16 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(17, $i);
           			$val17 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(18, $i);
           			$val18 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(19, $i);
           			$val19 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(20, $i);
           			$val20 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(21, $i);
           			$val21 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(22, $i);
           			$val22 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(23, $i);
           			$val23 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(24, $i);
           			$val24 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(25, $i);
           			$val25 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(26, $i);
           			$val26 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(27, $i);
           			$val27 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(28, $i);
           			$val28 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(29, $i);
           			$val29 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(30, $i);
           			$val30 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(31, $i);
           			$val31 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			$cell = $worksheet->getCellByColumnAndRow(32, $i);
           			$val32 = $cell->getValue();
           			//echo $j.$i.' = '.$val5.'<br>';
           			
           			          			
           		//}
	               #check if regno exists
			$qregno="SELECT Name, RegNo FROM chas_student WHERE RegNo='$val0'";
			$dbregno=mysql_query($qregno);
			$regct=mysql_num_rows($dbregno);
			
	   		if ($regct>0){
           		#imports data
           		//OVERWRITING STARTS HERE
           		
				$sqlcwk ="REPLACE INTO student SET
							Id='$val0', Name='$val1', RegNo='$val2', Sex='$val3', DBirth='$val4', MannerofEntry='$val5', 
							MaritalStatus='$val6', Campus='$val7', ProgrammeofStudy='$val8', Subject='$val9', Faculty='$val10',
							Department='$val11', Sponsor='$val12', GradYear='$val13', EntryYear='$val14', Status='$val15',
							YearofStudy='$val16', Address='$val17', Comment='$val18', Photo='$val19', IDProcess='$val20',
							Nationality='$val21', Region='$val22', District='$val23', Country='$val24', ParentOccupation='$val25',
							Received='$val26', User='$val27', Display='$val28', Denomination='$val29', Religion='$val30',
							Disability='$val31', AdmissionNo='$val32'";
							
						mysql_query($sqlcwk);
						if(mysql_error()) {
							 echo "CA Record of ".$val0." - ".$val1."- is a Duplicate Entry - Not Imported!<br>\n"; 
						  }
									
			}
			else{
				$sqlfe ="INSERT INTO student SET
							Id='$val0', Name='$val1', RegNo='$val2', Sex='$val3', DBirth='$val4', MannerofEntry='$val5', 
							MaritalStatus='$val6', Campus='$val7', ProgrammeofStudy='$val8', Subject='$val9', Faculty='$val10',
							Department='$val11', Sponsor='$val12', GradYear='$val13', EntryYear='$val14', Status='$val15',
							YearofStudy='$val16', Address='$val17', Comment='$val18', Photo='$val19', IDProcess='$val20',
							Nationality='$val21', Region='$val22', District='$val23', Country='$val24', ParentOccupation='$val25',
							Received='$val26', User='$val27', Display='$val28', Denomination='$val29', Religion='$val30',
							Disability='$val31', AdmissionNo='$val32'";
						
						mysql_query($sqlfe);
						if(mysql_error()) {
							 echo " FE Record ".$val0." - ".$val2." - is a Duplicate Entry - Not Imported!<br>\n"; 
						  }		
				}
	}   
}
		echo "<br /><br /><strong>Zalongwa Data Import Process Completed</strong>"; 
		unlink("./temp/$filename");
		unlink("$filename");
}
	include('../footer/footer.php');
	exit;
?> 
