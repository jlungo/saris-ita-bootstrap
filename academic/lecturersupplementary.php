<?php
if (isset($_POST['EXCEL'])) {
    #get connected to the database and verfy current session
    require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
    $ayear = $_POST['ayear'];
    $cohort = $_POST['cohort'];
    $cohot = $_POST['cohort'];
    $prog = $_POST['programme'];
// check if there is any supp for that year under that programme
    $sql = "SELECT DISTINCT ex.RegNo FROM examresult as ex,student as s WHERE ex.RegNo=s.RegNo AND ex.AYear='$ayear' AND s.EntryYear='$cohort' AND s.ProgrammeofStudy='$prog' AND ex.ExamCategory=7";
    $result_query = mysqli_query($zalongwa, $sql);
    $num_row = mysqli_num_rows($result_query);
    if ($num_row > 0) {
        //hold array of key registration number and value is array of all supp for that student

        $total_student_supp = array();
        while ($row1 = mysqli_fetch_array($result_query)) {
            $total_student_supp[$row1['RegNo']] = $row1['RegNo'];
        }
        include 'includes/supplemetaryreport_excel.php';
        exit;
    } else {
        $error = urlencode("No supplementary report found");
        $location = "lecturersupplementary.php?error=$error";
        echo '<meta http-equiv="refresh" content="0; url=' . $location . '">';
        exit;
    }

}
#get connected to the database and verfy current session
require_once('../Connections/sessioncontrol.php');

require_once('../Connections/zalongwa.php');

# include the header

global $szSection, $szSubSection;

$szSection = 'Examination';

$szSubSection = 'Supp Report';

$szTitle = 'Supplementary Report';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
    <!--Font Awesome (added because you use icons in your prepend/append)-->
    <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css"/>
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="./css/breadcrumb.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /*background-color: #009688;*/
        }

        .card {
            /*background-color: #324359;*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /*color: white;*/
            padding: 0px;
            border-radius: 0px !important;
        }

        @media (max-width: 34em) {
            .card {
                margin-top: 20px;
            }
        }

        @media (max-width: 48em) {
            .card {
                margin-top: 20px;
            }
        }

    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">
            <?php
            if (isset($_GET['error'])) {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <?php echo $_GET['error']; ?>
                </div>
                <?php
            }
            ?>
            <div class="card">
                <h3 class="card-header">
                    Select Correct Criteria</h3>
                <div class="card-block">
                    <form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
                        <div class="form-group">
                            <label>Programme:</label>
                            <select class="form-control" name="programme">
                                <?php
                                $sql = "SELECT * FROM programme";
                                $data = mysqli_query($zalongwa, $sql);
                                while ($row = mysqli_fetch_array($data)) {
                                    echo '<option value="' . $row['ProgrammeCode'] . '">' . $row['ProgrammeName'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Cohort of the Year:</label>
                            <select class="form-control" name="cohort">
                                <?php
                                $sql = "SELECT * FROM academicyear";
                                $data = mysqli_query($zalongwa, $sql);
                                while ($row = mysqli_fetch_array($data)) {
                                    echo '<option value="' . $row['AYear'] . '">' . $row['AYear'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Academic Year:</label>
                            <select class="form-control" name="ayear">
                                <?php
                                $sql = "SELECT * FROM academicyear";
                                $data = mysqli_query($zalongwa, $sql);
                                while ($row = mysqli_fetch_array($data)) {
                                    echo '<option value="' . $row['AYear'] . '">' . $row['AYear'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <input class="btn btn-success btn-md btn-block" type="submit" name="EXCEL" id="EXCEL"
                               value="Generate Report">
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<script>
    $(document).ready(function () {
        var date_input = $('input[name="date"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        var options = {
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
</script>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
</body>
</html>
