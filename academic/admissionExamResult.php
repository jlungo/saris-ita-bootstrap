<?php

#get connected to the database and verfy current session

require_once('../Connections/sessioncontrol.php');

require_once('../Connections/zalongwa.php');

# include the header

global $szSection, $szSubSection;

$szSection = 'Examination';

$szSubSection = 'Search';

$szTitle = 'Examination Result';


function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")

{

    $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;


    switch ($theType) {

        case "text":

            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";

            break;

        case "long":

        case "int":

            $theValue = ($theValue != "") ? intval($theValue) : "NULL";

            break;

        case "double":

            $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";

            break;

        case "date":

            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";

            break;

        case "defined":

            $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;

            break;

    }

    return $theValue;

}


$editFormAction = $_SERVER['PHP_SELF'];

if (isset($_SERVER['QUERY_STRING'])) {

    $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);

}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="./css/breadcrumb.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>

    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>

    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container text-muted">

    <!--    <div class="container-search" align="center">-->
    <!--        <img class="card-img-top img-fluid "-->
    <!--             style="width: 200px; height: inherit; padding-top: 15px; "-->
    <!--             src="./img/search.svg">-->
    <!--        <form class="navbar-form" role="search">-->
    <!--            <div class="input-group add-on">-->
    <!--                <input class="form-control" placeholder="Enter Student Registration Number" name="srch-term" id="srch-term" type="text">-->
    <!--                <div class="input-group-btn">-->
    <!--                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </form>-->
    <!--    </div>-->

    <?php
    if (isset($_POST['search']) && ($_POST['search'] == "Search")) {

    #get post variables

    $rawkey = trim(addslashes($_POST['key']));

    $key = preg_replace("[[:space:]]+", " ", $rawkey);

    #get student info

    $qstudent = "SELECT Name, RegNo, ProgrammeofStudy from student WHERE regno = '$key'";

    $dbstudent = mysqli_query($zalongwa, $qstudent) or die("This student has no exam results!" . mysqli_error($zalongwa));

    $row_result = mysqli_fetch_array($dbstudent);

    $name = $row_result['Name'];

    $regno = $row_result['RegNo'];

    $degree = $row_result['ProgrammeofStudy'];

    /*

    # get all courses for this candidate

    $qcourse="SELECT DISTINCT course.Units,

    course.Department,

    course.StudyLevel,

    examresult.AYear,

    examresult.CourseCode

    FROM

    course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)

    WHERE

    (course.Programme = '$degree') AND

    (RegNo='$key')

    ORDER BY examresult.AYear";

    */


    # get all courses for this candidate

    $qcourse = "SELECT DISTINCT course.Units,

    course.Department,

    course.StudyLevel,

    examresult.AYear,

    examresult.CourseCode

    FROM

    course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)

    WHERE

    (RegNo='$key')

    ORDER BY examresult.AYear";

    $dbcourse = mysqli_query($zalongwa, $qcourse) or die("No Exam Results for the Candidate - $key ");

    $total_rows = mysqli_num_rows($dbcourse);


    if ($total_rows > 0) {

    #initialise s/no

    $sn = 0;


    //get degree name

    $qdegree = "Select Title from programme where ProgrammeCode = '$degree'";

    $dbdegree = mysqli_query($zalongwa, $qdegree);

    $row_degree = mysqli_fetch_array($dbdegree);

    $programme = $row_degree['Title'];

    echo "$name - $regno <br> $programme";

    ?>


    <table class="table table-striped table-bordered table-responsive" width="100%" cellspacing="0">
        <thead class="table-light">
        <tr style="background:#ceceff;">
            <th width="5%">S/No</th>
            <th width="10%">Year</th>
            <th width="10%">Course Code</th>
            <th width="5%">Cwk</th>
            <th width="5%">Exam</th>
            <th width="5%">Sup</th>
            <th width="5%">Spec</th>
            <th width="5%">TP</th>
            <th width="5%">PT</th>
            <th width="5%">Proj</th>
            <th width="10%">Total</th>
            <th width="5%">Grade</th>
            <th width="10%">Remarks</th>
        </tr>
        </thead>

        <?php

        while ($row_course = mysqli_fetch_array($dbcourse)) {

        $course = $row_course['CourseCode'];

        $units = $row_course['Units'];

        $ayear = $row_course['AYear'];

        $coursefaculty = $row_course['Department'];

        $sn = $sn + 1;

        $remarks = 'remarks';

        $RegNo = $key;
        #include grading system

        include 'includes/choose_studylevel.php';


        #display results

        ?>

        <tr>

            <td>

                <?php if ($privilege == 2) {

                    echo "<a href=\"lecturerEditsingleresult.php?Candidate=$regno&Course=$course&AYear=$ayear\">$sn</a>";

                } else {

                    echo $sn;

                } ?>

            </td>

            <td>
                <?php echo $ayear ?>
            </td>

            <td>
                <?php echo $course ?>
            </td>

            <td>
                <?php echo $test2score ?>
            </td>

            <td>
                <?php echo $aescore ?>
            </td>

            <td>
                <?php echo $supscore ?>
            </td>

            <td>
                <?php echo $spscore ?>
            </td>

            <td>
    <?php echo $tpscore ?></div>
</td>

<td>
    <?php echo $ptscore ?></div>
</td>

<td>
    <?php echo $proscore ?></div>
</td>

<td>
    <?php echo $marks ?>
</td>

<td>
    <?php echo $grade ?>
</td>

<td>
    <?php echo $remark ?></div>
</td>

</tr>

<?php

//close while loop

}

?>

</table>

<div style="margin: 20px;" align="center">
    ==========================<br>

    Student Exam Results View<br>

    ==========================<br>
</div>
<?php

$RegNo = $regno;

$examofficer = 1;


if ($examofficer <> 1) {

    require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');


    global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;

    $szSection = 'Academic Records';

    $szTitle = 'Examination Results';

    $szSubSection = 'Exam Result';

}


$editFormAction = $_SERVER['PHP_SELF'];


#check if has blocked

$qstatus = "SELECT Status FROM student  WHERE (RegNo = '$RegNo')";

$dbstatus = mysqli_query($zalongwa, $qstatus);

$row_status = mysqli_fetch_array($dbstatus);

$status = $row_status['Status'];


if ($status == 'Blocked') {

    echo "Your Examination Results are Currently Blocked<br>";

    echo "Please Contact the Registrar Office to Resolve this Issue<br>";

    exit;

}
if (isset($_SERVER['QUERY_STRING'])) {

    $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);

}

$key = $RegNo;

include '../academic/includes/showexamresults.php';

if ($examofficer <> 1) {

    include('../footer/footer.php');

}


//close if total statement

}

} elseif (isset($_GET['search'])) {

#get post variables

$key = $_SESSION['search'];

#get student info

$qstudent = "SELECT Name, RegNo, ProgrammeofStudy from student WHERE regno = '$key'";

$dbstudent = mysqli_query($zalongwa, $qstudent) or die("No Exam Results for the Candidate" . mysqli_error($zalongwa));

$row_result = mysqli_fetch_array($dbstudent);

$name = $row_result['Name'];

$regno = $row_result['RegNo'];

$degree = $row_result['ProgrammeofStudy'];

/*

# get all courses for this candidate

$qcourse="SELECT DISTINCT course.Units,

                          course.Department,

                          course.StudyLevel,

                          examresult.AYear,

                          examresult.CourseCode

          FROM

            course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)

          WHERE

                (course.Programme = '$degree') AND

                (RegNo='$key')

          ORDER BY examresult.AYear";

*/


# get all courses for this candidate

$qcourse = "SELECT DISTINCT course.Units, 

						  course.Department, 

						  course.StudyLevel, 

						  examresult.AYear, 

						  examresult.CourseCode 

          FROM 

			course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)

          WHERE 

				(RegNo='$key')

		  ORDER BY examresult.AYear";

$dbcourse = mysqli_query($zalongwa, $qcourse) or die("No Exam Results for the Candidate - $key ");

$total_rows = mysqli_num_rows($dbcourse);


if ($total_rows > 0) {

#initialise s/no

$sn = 0;


//get degree name

$qdegree = "Select Title from programme where ProgrammeCode = '$degree'";

$dbdegree = mysqli_query($zalongwa, $qdegree);

$row_degree = mysqli_fetch_array($dbdegree);

$programme = $row_degree['Title'];

echo "$name - $regno <br> $programme";

?>

<table class="table table-striped table-bordered table-responsive" width="100%" cellspacing="0">
    <thead class="table-light">
    <tr>
        <th>S/No</th>
        <th>Year</th>
        <th>Course Code</th>
        <th>Cwk</th>
        <th>Exam</th>
        <th>Sup</th>
        <th>Spec</th>
        <th>TP</th>
        <th>PT</th>
        <th>Proj</th>
        <th>Total</th>
        <th>Grade</th>
        <th>Remarks</th>
    </tr>
    </thead>
    <tbody>
    <?php

    while ($row_course = mysqli_fetch_array($dbcourse)) {

        $course = $row_course['CourseCode'];

        $units = $row_course['Units'];

        $ayear = $row_course['AYear'];

        $coursefaculty = $row_course['Department'];

        $sn = $sn + 1;

        $remarks = 'remarks';

        $RegNo = $key;

        #include grading system

        include 'includes/choose_studylevel.php';


        #display results

        ?>

        <tr>

            <td>

                <?php if ($privilege == 2) {

                    echo "<a href=\"lecturerEditsingleresult.php?Candidate=$regno&Course=$course&AYear=$ayear\">$sn</a>";

                } else {

                    echo $sn;

                } ?>

            </td>

            <td>
                <?php echo $ayear ?>
            </td>

            <td>
                <?php echo $course ?>
            </td>

            <td>
                <?php echo $test2score ?>
            </td>

            <td>
                <?php echo $aescore ?>
            </td>

            <td>
                <?php echo $supscore ?>
            </td>

            <td>
                <?php echo $spscore ?>
            </td>

            <td>
                <?php echo $tpscore ?></div>
            </td>

            <td>
                <?php echo $ptscore ?></div>
            </td>

            <td>
                <?php echo $proscore ?></div>
            </td>

            <td>
                <?php echo $marks ?>
            </td>

            <td>
                <?php echo $grade ?>
            </td>

            <td>
                <?php echo $remark ?></div>
            </td>

        </tr>

        <?php

        //close while loop

    }

    ?>
    </tbody>
</table>

<?php

}

} else {


?>


<div class="container-search" align="center">
    <img class="card-img-top img-fluid "
         style="width: 150px; height: inherit; padding-top: 15px; "
         src="./img/search.svg">
    <form class="navbar-form" role="search" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST"
          name="studentRoomApplication" id="studentRoomApplication">
        <div class="input-group add-on">
            <input name="key" type="text" id="key" class="form-control"
                   placeholder="Search Registration Number">
            <div class="input-group-btn">
                <button type="submit" name="search" value="Search" class="btn btn-default"><i
                            class="fa fa-search"></i></button>
            </div>
        </div>
    </form>
</div>
<?php

}
?>
</div>

</div>
<!-- end .container -->
<br><br><br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</div>
</body>
</html>