<?php

#get connected to the database and verfy current session

require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

$today = date("Y-m-d");

# include the header
global $szSection, $szSubSection;
$szSection = 'Administration';
$szSubSection = 'Limit Upload';
$szTitle = 'Setting of Course Results Upload Deadline';

?>


<?php
$currentPage = $_SERVER["PHP_SELF"];

//populate academic year combo box
//mysql_select_db($database_zalongwa, $zalongwa);

$query_AYear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$AYear = mysqli_query($zalongwa, $query_AYear) or die(mysqli_error($zalongwa));

$row_AYear = mysqli_fetch_assoc($AYear);
$totalRows_AYear = mysqli_num_rows($AYear);


//populate semester combo box
//mysql_select_db($database_zalongwa, $zalongwa);
$query_sem = "SELECT Semester FROM terms ORDER BY Semester ASC";

$sem = mysqli_query($zalongwa, $query_sem) or die(mysqli_error($zalongwa));
$row_sem = mysqli_fetch_assoc($sem);
$totalRows_sem = mysqli_num_rows($sem);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="./css/breadcrumb.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /*background-color: #009688;*/
        }

        .card {
            /*background-color: #324359;*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /*color: white;*/
            padding: 0px;
            border-radius: 0px !important;
        }

        .row {
            margin-top: 20px;
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">

            <?php

            if (isset($_POST['Confirm']) && ($_POST['Confirm'] == 'Confirm')) {

                $currentPage = $_SERVER["PHP_SELF"];

                $key = stripslashes($_POST['deadline']);
                $ayear = stripslashes($_POST['ayear']);
                $sem = stripslashes($_POST['sem']);
                $act = stripslashes($_POST['action']);

                $date = explode("-", $key);
                //$maxRows_ExamOfficerGradeBook = 10000;
                //$pageNum_ExamOfficerGradeBook = 0;

                echo "<div class=\"alert alert-danger\">";

                if (strlen($key) == 0 || strlen($ayear) == 0 || strlen($sem) == 0 || strlen($act) == 0) {
                    echo "Please make sure you fill all the fields before Confirming!";
                    exit;
                } elseif ((count($date) != 3) || (strlen($date[0]) != 4) || (strlen($date[1]) != 2) || (strlen($date[2]) != 2)) {
                    echo "Please enter the date in the format specified!";
                    exit;
                } elseif ($key < date('Y-m-d')) {
                    echo "Please enter the date that is greater than or equals to today's date!";
                    exit;
                }

                //check whether to limit or Allow
                if (intval($act) == 1) {

                    //limit uploading
                    $checktb = mysqli_query($zalongwa, "SELECT * FROM uploadlimit");
                    if ($rows = mysqli_num_rows($checktb) >= 5) {
                        //set upload limit
                        mysqli_query($zalongwa, "DELETE FROM uploadlimit WHERE AYear<>'$ayear'");
                        $query = mysqli_query($zalongwa, "INSERT INTO uploadlimit VALUES ('$ayear','$sem',1,'$key')");
                        if ($query) {
                            echo "The Deadline has been successfuly set";
                            exit;
                        } else {
                            echo "The setting wasn't successful";
                            exit;
                        }
                    } else {
                        //update upload limit
                        $less = mysqli_query($zalongwa, "SELECT * FROM uploadlimit WHERE AYear='$ayear' AND Semester='$sem'");
                        if ($row = mysqli_num_rows($less) == 1) {
                            $query = mysqli_query($zalongwa, "UPDATE uploadlimit SET Checked=1, Date='$key' WHERE
            Semester='$sem' AND AYear = '$ayear'");
                            if ($query) {
                                echo "The Deadline has been successfuly Updated";
                                exit;
                            } else {
                                echo "The setting wasn't successful";
                                exit;
                            }
                        } else {
                            //set upload limit
                            $query = mysqli_query($zalongwa, "INSERT INTO uploadlimit VALUES ('$ayear','$sem',1,'$key')");
                            if ($query) {
                                echo "The Deadline has been successfuly set";
                                exit;
                            } else {
                                echo "The setting wasn't successful";
                                exit;
                            }
                        }
                    }
                } elseif (intval($act) == 0) {

                    //release the upload limit
                    $check = mysqli_query($zalongwa, "SELECT * FROM uploadlimit WHERE AYear='$ayear' AND Semester='$sem'");
                    if ($rows = mysqli_num_rows($check) == 0) {
                        echo "Academic Year ($ayear) has no Deadline for $sem results uploading";
                    } else {
                        $query = mysqli_query($zalongwa, "UPDATE uploadlimit SET checked = 0 WHERE Semester='$sem' AND AYear = '$ayear'");
                        echo "The Deadline has been released!";
                    }
                }
                echo " </div>";
            } else {
                ?>

                <div class="card">
                    <h3 class="card-header">
                        <?php echo $szTitle; ?></h3>
                    <div class="card-block">
                        <form name="form1" method="post" action="<?php echo $currentPage; ?> ">

                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Academic Year:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="ayear" id="ayear">

                                            <option value="">-----------------</option>
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_AYear['AYear'] ?>"><?php echo $row_AYear['AYear'] ?></option>
                                                <?php
                                            } while ($row_AYear = mysqli_fetch_assoc($AYear));
                                            $rows = mysqli_num_rows($AYear);
                                            if ($rows > 0) {
                                                mysqli_data_seek($AYear, 0);
                                                $row_AYear = mysqli_fetch_assoc($AYear);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Semester:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="sem" id="sem">

                                            <option value="">-----------------</option>

                                            <?php
                                            do {
                                                ?>

                                                <option value="<?php echo $row_sem['Semester'] ?>"><?php echo $row_sem['Semester'] ?></option>

                                                <?php
                                            } while ($row_sem = mysqli_fetch_assoc($sem));
                                            $rows = mysqli_num_rows($sem);
                                            if ($rows > 0) {
                                                mysqli_data_seek($sem, 0);
                                                $row_sem = mysqli_fetch_assoc($sem);
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Deadline Date:</label>
                                    <div class="col-sm-8">
                                        <script language="JavaScript" src="datepicker/Calendar1-901.js"
                                                type="text/javascript"></script>
                                        <input class="form-control" name="deadline" type="text" id="deadline"
                                               value=<?php echo $today; ?> size="12">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Choose Action:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="action" id="select3">

                                            <option value="">-----------------</option>

                                            <option value="1">Set Deadline</option>

                                            <option value="0">Unset Deadline</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"></label>
                                    <div class="col-sm-8">
                                        <input class="btn btn-primary btn-md btn-block" onclick="return show_confirm()"
                                               name="Confirm" type="submit" id="Confirm" value="Confirm">
                                    </div>

                                </div>
                            </div>

                        </form>

                    </div>
                </div>
                <?php
            }
            ?>

        </div>
    </div>
</div>
<br>
<br>
<br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<script type="text/javascript">
    function show_confirm() {
        var r = confirm("Are you sure you want to save this changes ?");
        if (r == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
</body>
</html>
