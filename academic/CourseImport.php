<?php
#get connected to the database and verify current session
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Import Module from Excel';
$szSubSection = 'Import Module';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            background-color: #009688;
        }

        .card {
            background-color: #324359;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            color: white;
            padding: 0px;
            border-radius: 0px !important;
        }

        .row {
            margin-top: 20px;
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include '../student/studentNavBar.php'; ?>

<div class="container-flex">
    <!--    <div class="container">-->
    <!--        Change Password-->
    <!--    </div>-->

</div>
<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <h3 class="card-header">
                    Change Password</h3>
                <div class="card-block">
                    <?php
                    if (isset($_POST['Import']) && $_POST['Import'] == 'Import') {
                        $department = $_POST['department'];
                        $nta = $_POST['nta'];
                        include 'includes/courseimport.php';


                    } else {
                        #populate department
                        $query_acyear = "SELECT DeptName FROM department";
                        $department = mysqli_query($zalongwa, $query_acyear) or die(mysqli_error($zalongwa));

                        #populate nta
                        $query_nta = "SELECT Code,StudyLevel FROM  programmelevel";
                        $nta = mysqli_query($zalongwa, $query_nta) or die(mysqli_error($zalongwa));

                        ###############################################
                        $editFormAction = $_SERVER['PHP_SELF'];
                        if (isset($_SERVER['QUERY_STRING'])) {
                            $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
                        }
                        if (isset($_GET['error'])) {
                            $sms = ($_GET['error'] == 1) ? 'Year must be selected' : "You Cannot Add Exam Remarks This Year:" . $_GET['year'] . " (This is not current year)<br> Database Update Rejected !!";
                            echo "<p style='color:red;font-family: tahoma, verdana;font-size: 12px;'>$sms</p>";
                        }
                        ?>
                        <form action="<?php echo $editFormAction; ?>" method=POST id=fmAdd name=fmAdd
                              LANGUAGE=javascript
                              onsubmit="return fmAdd_onsubmit()" enctype="multipart/form-data">
                                
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Old Password</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="txtoldPWD" type="password" id="txtoldPWD">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">New Password</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="txtnewPWD" type="password" id="txtnewPWD">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Retype Password</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="txtrenewPWD" type="password" id="txtrenewPWD">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"></label>
                                    <div class="col-sm-8">
                                        <input type="submit" value="Submit" name="Submit"
                                               class="btn btn-success btn-md btn-block">
                                    </div>

                                </div>
                            </div>
                            <input type="hidden" name="MM_insert" value="true">
                            <input type="hidden" name="MM_update" value="fmAdd">
                        </form>
                        <?php
                    }
                    echo "<br><br>";
                    include('../footer/footer.php');
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>
