<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header
include('lecturerMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Programme Information';
$szSubSection = 'Policy Setup';
include("lecturerheader.php");

if (isset($_GET['content'])) {
    if ($_POST['update']) {
        $ProgrammeCode = addslashes($_POST["ProgrammeCode"]);
        $ProgrammeName = addslashes($_POST["ProgrammeName"]);
        $Title = addslashes($_POST["Title"]);
        $Ntalevel = addslashes($_POST["Ntalevel"]);
        $Faculty = addslashes($_POST["Faculty"]);
        $CampusID = addslashes($_POST["CampusID"]);

        $sql = "UPDATE programme SET ProgrammeCode = '$ProgrammeCode', ProgrammeName='$ProgrammeName', Title = '$Title', Ntalevel = '$Ntalevel',
			Faculty='$Faculty', CampusID='$CampusID' WHERE ProgrammeID=" . $_GET['content'];
        if ($zalongwa->query($sql)) {
            echo "<p>Updated successfully</p>";
            header("Location: admissionProgramme.php");
        } else {
            echo "<p>Failed to update..</p>";
        }
    }
    $result = $zalongwa->query("SELECT * FROM programme WHERE ProgrammeID=" . $_GET['content']);
    $institution = $result->fetch_assoc();
    ?>
    <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
        <table class='table_view'>
            <tr class='header'>
                <td> Field</td>
                <td> Field Value</td>
            </tr>
            <tr class='list'>
                <td> Institution: *</td>
                <td>
                    <select name="CampusID" required>
                        <option value="" disabled="disabled"> --select--
                        </option>
                        <?php
                        $query_campus = "SELECT CampusID, Campus FROM campus";
                        $result_campus = $zalongwa->query($query_campus) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
                        while ($campus_row = $result_campus->fetch_array()) {
                            ?>
                            <option <?php if($institution['CampusID'] == $campus_row['CampusID']){?> selected="selected" <?php }?> value="<?php echo $campus_row['CampusID']; ?>"><?php echo $campus_row['Campus']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr class='list'>
                <td> Faculty: *</td>
                <td>
                    <select name="Faculty" required>
                        <option value="" disabled="disabled"> --select--
                        </option>
                        <?php
                        $query_faculty = "SELECT FacultyID, FacultyName FROM faculty";
                        $result_faculty = $zalongwa->query($query_faculty) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
                        while ($faculty_row = $result_faculty->fetch_array()) {
                            ?>
                            <option <?php if($institution['Faculty'] == $faculty_row['FacultyID']){?> selected="selected" <?php }?> value="<?php echo $faculty_row['FacultyName']; ?>"><?php echo $faculty_row['FacultyName']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr class='list'>
                <td> Award: *</td><td><select name="Ntalevel" required>
                        <option value="" disabled="disabled"> --select--
                        </option>
                        <?php
                        $query_studylevel = "SELECT LevelCode, LevelName FROM studylevel";
                        $result_studylevel = $zalongwa->query($query_studylevel) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
                        while ($studylevel_row = $result_studylevel->fetch_array()) {
                            ?>
                            <option <?php if($institution['Ntalevel'] == $studylevel_row['LevelCode']){?> selected="selected" <?php }?> value="<?php echo $studylevel_row['LevelCode']; ?>"><?php echo $studylevel_row['LevelName']; ?></option>
                            <?php
                        }
                        ?>
                    </select></td>
            </tr>
            <tr class='list'>
                <td> Programme Code: *</td>
                <td><input type="text" id="ProgrammeCode" name="ProgrammeCode" value="<?php echo $institution['ProgrammeCode']; ?>" size="40" required></td>
            </tr>
            <tr class='list'>
                <td> Programme Name *</td>
                <td><input type="text" id="ProgrammeName" name="ProgrammeName" value="<?php echo $institution['ProgrammeName']; ?>"
                           size="40" required></td>
            </tr>
            <tr class='list'>
                <td> Title: *</td>
                <td><input type="text" id="Title" name="Title" value="<?php echo $institution['Title']; ?>"
                           size="40" required></td>
            </tr>
        </table>
        <br>
        <table class='table_view'>
            <tr style='float: right' class='list'>
                <td>
                    <button formaction="admissionProgramme.php">Back</button>
                </td>
                <td><input type="submit" id="submit" name="update" value="update"></td>
            </tr>
        </table>

    </form>

    <?php

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
        $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}
# include the footer
include("../footer/footer.php");
?>