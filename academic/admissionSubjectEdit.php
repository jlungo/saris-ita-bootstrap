<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header
include('lecturerMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Course Information';
$szSubSection = 'Policy Setup';
include("lecturerheader.php");

if (isset($_GET['content'])) {
    if ($_POST['update']) {
        $CourseCode = addslashes($_POST["CourseCode"]);
        $CourseName = addslashes($_POST["CourseName"]);
        $Capacity = addslashes($_POST["Capacity"]);
        $Units = addslashes($_POST["Units"]);
        $Department = addslashes($_POST["Department"]);
        $StudyLevel = addslashes($_POST["StudyLevel"]);

        $sql = "UPDATE course SET CourseCode = '$CourseCode', CourseName='$CourseName', Capacity = '$Capacity',
			Units='$Units', Department='$Department', StudyLevel='$StudyLevel' WHERE Id=" . $_GET['content'];
        if ($zalongwa->query($sql)) {
            echo "<p>Updated successfully</p>";
            //header("Location: admissioncourse.php");
        } else {
            echo "<p>Failed to update..</p>";
        }
    }
    $result = $zalongwa->query("SELECT * FROM course WHERE Id=" . $_GET['content']);
    $institution = $result->fetch_assoc();
    ?>
    <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
        <table class='table_view'>
            <tr class='header'>
                <td> Field</td>
                <td> Field Value</td>
            </tr>
            <tr class='list'>
                <td> Department: *</td>
                <td>
                    <select name="Department" required>
                        <option value="" disabled="disabled"> --select--
                        </option>
                        <?php
                        $query_dept = "SELECT DeptID, DeptName FROM department";
                        $result_dept = $zalongwa->query($query_dept) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
                        while ($dept_row = $result_dept->fetch_array()) {
                            ?>
                            <option <?php if($institution['Department'] == $dept_row['DeptName']){?> selected="selected" <?php }?> value="<?php echo $dept_row['DeptName']; ?>"><?php echo $dept_row['DeptName']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr class='list'>
                <td> Module Code: *</td>
                <td><input type="text" id="CourseCode" name="CourseCode" value="<?php echo $institution['CourseCode']; ?>" size="40" required></td>
            </tr>
            <tr class='list'>
                <td> Module Name: *</td>
                <td><input type="text" id="CourseName" name="CourseName" value="<?php echo $institution['CourseName']; ?>"
                           size="40" required></td>
            </tr>
            <tr class='list'>
                <td> Capacity: *</td>
                <td><input type="text" id="Capacity" name="Capacity" value="<?php echo $institution['Capacity']; ?>"
                           size="40" required></td>
            </tr>
            <tr class='list'>
                <td> Units: *</td>
                <td><input type="text" id="Units" name="Units" value="<?php echo $institution['Units']; ?>" size="40" required>
                </td>
            </tr>
            <tr class='list'>
                <td> Exam Regulation: *</td>
                <td>
                    <select name="StudyLevel" required>
                        <option value="" disabled="disabled"> --select--
                        </option>
                        <?php
                        $query_level = "SELECT Code, StudyLevel FROM programmelevel";
                        $result_level = $zalongwa->query($query_level) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
                        while ($level_row = $result_level->fetch_array()) {
                            ?>
                            <option <?php if($institution['StudyLevel'] == $level_row['Code']){?> selected="selected" <?php }?> value="<?php echo $level_row['Code']; ?>"><?php echo $level_row['StudyLevel']; ?></option>

                            <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
        </table>
        <br>
        <table class='table_view'>
            <tr style='float: right' class='list'>
                <td>
                    <button formaction="admissionSubject.php">Back</button>
                </td>
                <td><input type="submit" id="submit" name="update" value="update"></td>
            </tr>
        </table>

    </form>

    <?php

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
        $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}
# include the footer
include("../footer/footer.php");
?>