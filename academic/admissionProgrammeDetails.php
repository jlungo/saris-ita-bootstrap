<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header
include('lecturerMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Programme Information';
$szSubSection = 'Policy Setup';
include("lecturerheader.php");

if (isset($_GET['details'])) {
    $result = $zalongwa->query("SELECT * FROM programme INNER JOIN campus ON programme.CampusID=campus.CampusID INNER JOIN faculty ON programme.Faculty=faculty.FacultyName INNER JOIN studylevel ON LevelCode=Ntalevel WHERE ProgrammeID=" . $_GET['details']);
    $institution = $result->fetch_assoc();
    if ($result->num_rows > 0) {
        ?>
        <table class='table_view'>
            <tr class='header'>
                <td> Field</td>
                <td> Field Value</td>
            </tr>
            <tr class='list'>
                <td> Institution:</td>
                <td><?php echo $institution['Campus']; ?></td>
            </tr>
            <tr class='list'>
                <td> Faculty:</td>
                <td><?php echo $institution['FacultyName']; ?></td>
            </tr>
            <tr class='list'>
                <td> Programme Code:</td>
                <td><?php echo $institution['ProgrammeCode']; ?></td>
            </tr>
            <tr class='list'>
                <td> Programme Name</td>
                <td><?php echo $institution['ProgrammeName']; ?></td>
            </tr>
            <tr class='list'>
                <td> Title:</td>
                <td><?php echo $institution['Title']; ?></td>
            </tr>
            <tr class='list'>
                <td> Award:</td>
                <td><?php echo $institution['LevelName']; ?></td>
            </tr>
            <tr class='list'>
                <td> Faculty:</td>
                <td><?php echo $institution['Faculty']; ?></td>
            </tr>
        </table>
        <br>
        <table class='table_view'>
            <tr style='float: right' class='list'>
                <td><a href="admissionProgramme.php">Back</a></td>
            </tr>
        </table>
        <?php
    } else {
        echo "Sorry, No Records Found <br>";
    }
}
# include the footer
include("../footer/footer.php");
?>