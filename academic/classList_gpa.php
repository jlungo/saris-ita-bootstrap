<?php
$regno = $regnumber;
$ayos = $yearofstudy;
$gpa = 0;
//query academeic year
$qayear = "SELECT DISTINCT AYear FROM examresult WHERE RegNo='$regno' AND AYear='$year'";
$dbayear = $zalongwa->query($qayear);
$row_year = $dbayear->fetch_array();
$academicYear = $row_year['AYear'];


if ($academicYear <> '') {
    $dbsemester = $zalongwa->query("SELECT DISTINCT Semester FROM examresult WHERE RegNo = '$regno' AND AYear = '$academicYear' ORDER BY Semester ASC");
    $numsem = $dbsemester->num_rows;

    if ($numsem > 0) {
        while ($rowsemester = $dbsemester->fetch_object()) {
            $currentsemester = $rowsemester->Semester;
            $ayear = $academicYear;

            $qstudent = "SELECT Name, RegNo, ProgrammeofStudy from student WHERE RegNo = '$regno'";
            $dbstudent = $zalongwa->query($qstudent);
            $row_result = $dbstudent->fetch_array();
            $name = $row_result['Name'];
            $regno = $row_result['RegNo'];
            $degree = $row_result['ProgrammeofStudy'];

            //get all courses for this candidate
            $qcourse = "SELECT DISTINCT course.CourseCode, course.CourseName, courseprogramme.credits, course.StudyLevel, course.Department
			FROM course
			INNER JOIN courseprogramme ON courseprogramme.CourseCode = course.CourseCode
			WHERE courseprogramme.ProgrammeID =  '$degree'
			AND courseprogramme.AYear =  '$year'
			AND courseprogramme.YearOfStudy = '$ayos'
			AND course.CourseCode
			IN (
			SELECT DISTINCT CourseCode
			FROM examresult
			WHERE RegNo = '$regno'
			AND AYear = '$year'
			AND Checked = '1'
			)";

            $dbcourse = $zalongwa->query($qcourse) or die("No Exam Results for the Candidate - $key ");
            $total_rows = $dbcourse->num_rows;

            if ($total_rows > 0) {
                $sn = 0;

                //get degree name
                $qdegree = "Select Title from programme where ProgrammeCode = '$degree'";
                $dbdegree = $zalongwa->query($qdegree);
                $row_degree = $dbdegree->fetch_array();
                $programme = $row_degree['Title'];

                //initialise
                $totalunit = 0;
                $unittaken = 0;
                $sgp = 0;
                $totalsgp = 0;
                $gpa = 0;
                ?>

                <?php
                $totalinc = 0;
                while ($row_course = $dbcourse->fetch_array()) {
                    $course = $row_course['CourseCode'];
                    $unit = $row_course['credits'];
                    $coursename = $row_course['CourseName'];
                    $coursefaculty = $row_course['Department'];

                    if ($row_course['Status'] == 1) {
                        $status = 'Core';
                    } else {
                        $status = 'Elective';
                    }
                    $sn = $sn + 1;
                    $remarks = 'remarks';
                    $RegNo = $regno;
                    include '../academic/includes/choose_studylevel.php';
                }

                $gpa = @substr($totalsgp / $unittaken, 0, 4);
                if (strlen($gpa) == 1) {
                    $gpa = $gpa . '.00';
                } elseif (strlen($gpa) == 2) {
                    $gpa = $gpa . '00';
                } elseif (strlen($gpa) == 3) {
                    $gpa = $gpa . '0';
                }

            } else {
                #say something
            }
        }//ends while rowsemester
    } else {
        #say something
    }
}//ends while rowayear
	

