<?php

#get connected to the database and verfy current session

require_once('../Connections/sessioncontrol.php');

require_once('../Connections/zalongwa.php');

global $szSection, $szSubSection;

$szSection = 'Administration';

$szSubSection = 'Change Semester';

$szTitle = 'Change Exam Semester';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
    <!--Font Awesome (added because you use icons in your prepend/append)-->
    <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css"/>
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="./css/breadcrumb.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /*background-color: #009688;*/
        }

        .card {
            /*background-color: #324359;*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /*color: white;*/
            padding: 0px;
            border-radius: 0px !important;
        }

        @media (max-width: 34em) {
            .card {
                margin-top: 20px;
            }
        }

        @media (max-width: 48em) {
            .card {
                margin-top: 20px;
            }
        }

    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <h3 class="card-header">
                    Change Exam Semester</h3>
                <div class="card-block">
                    <?php

                    $currentPage = $_SERVER["PHP_SELF"];


                    //populate academic year combo box


                    $query_AYear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";

                    $AYear = $zalongwa->query($query_AYear) or die($zalongwa->connect_error);

                    $row_AYear = $AYear->fetch_assoc();

                    $totalRows_AYear = $AYear->num_rows;


                    //populate semester combo box


                    $query_sem = "SELECT Semester FROM terms ORDER BY Semester ASC";

                    $sem = $zalongwa->query($query_sem) or die($zalongwa->connect_error);

                    $row_sem = $sem->fetch_assoc();

                    $totalRows_sem = $sem->num_rows;


                    //populate coursecode combo box


                    $query_course = "SELECT CourseCode FROM course ORDER BY CourseCode ASC";

                    $course = $zalongwa->query($query_course) or die($zalongwa->connect_error);

                    $row_course = $course->fetch_assoc();

                    $totalRows_course = $course->num_rows;


                    if (isset($_POST['confirm']) && ($_POST['confirm'] == 'Confirm')) {


                        $currentPage = $_SERVER["PHP_SELF"];

                        @$key = $_POST['course'];

                        @$ayear = $_POST['ayear'];

                        @$sem = $_POST['sem'];


                        if (empty($key) || empty($ayear) || empty($sem)) {

                            $error = urlencode("Make sure you fill all the fields..");
                            $location = "lecturerexamofficerchangesemester.php?error=$error";
                            echo '<meta http-equiv="refresh" content="0; url=' . $location . '">';
                            exit;
                        }


                        $maxRows_ExamOfficerGradeBook = 10000;

                        $pageNum_ExamOfficerGradeBook = 0;

//change semester registration

                        $query = "UPDATE examregister SET Semester='$sem' WHERE CourseCode ='$key' AND AYear = '$ayear'";

                        $qupdadateexamresult = "UPDATE examresult SET Semester='$sem' WHERE CourseCode ='$key' AND AYear = '$ayear'";


                        $result = $zalongwa->query($query);

                        $result1 = $zalongwa->query($qupdadateexamresult);


                        if ($result) {


                            $error = urlencode("Database Updated Successful!");
                            $location = "lecturerexamofficerchangesemester.php?error=$error";
                            echo '<meta http-equiv="refresh" content="0; url=' . $location . '">';
                            exit;

                        }

                    }

                    if (isset($_GET['error'])) {
                        $error = urldecode($_GET['error']);
                        echo "<p style='color:red;font-family: tahoma, verdana;font-size: 12px;'>$error</p>";
                    }
                    //openup a form

                    ?>

                    <form name="form1" method="post" action="<?php echo $currentPage; ?> ">
                        <div class="form-group">
                            <label for="exampleFormControlSelect">Academic Year:</label>
                            <select name="ayear" id="ayear" class="form-control">
                                <option value="">-----------------</option>

                                <?php

                                do {

                                    ?>

                                    <option value="<?php echo $row_AYear['AYear'] ?>"><?php echo $row_AYear['AYear'] ?></option>

                                    <?php

                                } while ($row_AYear = $AYear->fetch_assoc());

                                $rows = $AYear->num_rows;

                                if ($rows > 0) {

                                    mysqli_data_seek($AYear, 0);

                                    $row_AYear = $AYear->fetch_assoc();

                                }

                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect">Semester:</label>
                            <select name="sem" id="sem" class="form-control">
                                <option value="">-----------------</option>

                                <?php

                                do {

                                    ?>

                                    <option value="<?php echo $row_sem['Semester'] ?>"><?php echo $row_sem['Semester'] ?></option>

                                    <?php

                                } while ($row_sem = $sem->fetch_assoc());

                                $rows = $sem->num_rows;

                                if ($rows > 0) {

                                    mysqli_data_seek($sem, 0);

                                    $row_sem = $sem->fetch_assoc();

                                }

                                ?>

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect">Course Code:</label>
                            <select name="course" id="select2" class="form-control">
                                <option value="">-----------------</option>

                                <?php

                                do {

                                    ?>

                                    <option value="<?php echo $row_course['CourseCode'] ?>"><?php echo $row_course['CourseCode'] ?></option>

                                    <?php

                                } while ($row_course = $course->fetch_assoc());

                                $rows = $course->num_rows;

                                if ($rows > 0) {

                                    mysqli_data_seek($course, 0);

                                    $row_course = $course->fetch_assoc();

                                }

                                ?>
                            </select>
                        </div>
                        <input class="btn btn-success btn-md btn-block" onclick="return show_confirm()"
                               name="confirm" type="submit"
                               id="confirm" value="Confirm">
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<script>
    $(document).ready(function () {
        var date_input = $('input[name="date"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        var options = {
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
</script>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    function show_confirm() {
        var r = confirm("Are you sure you want to change Semester ?");
        if (r == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
</body>
</html>
