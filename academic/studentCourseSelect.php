<?php
#start pdf
if (isset($_POST['PDF']) && ($_POST['PDF'] == "Registered List-PDF")) {

    $year = trim(addslashes($_POST['ayear']));
    $coursecode = trim(addslashes($_POST['Hall']));

    #get connected to the database and verfy current session
    require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');

    #Get Organisation Name
    $qorg = "SELECT * FROM organisation";
    $dborg = mysqli_query($zalongwa, $qorg);
    $row_org = mysqli_fetch_assoc($dborg);
    $org = $row_org['Name'];
    $address = $row_org['Address'];
    $phone = $row_org['tel'];
    $fax = $row_org['fax'];
    $email = $row_org['email'];
    $website = $row_org['website'];
    $city = $row_org['city'];

    if ($year == '' || $coursecode == '') {
        $error = urlencode("Please fill all field....");
        $location = "studentCourseSelect.php?error=$error";
        echo '<meta http-equiv="refresh" content="0; url=' . $location . '">';
        exit;


    }


    # get all students' annual result for this course
    $whereclause = ($privilege <> 2) ? " AND Recorder='$username' " : "";
    $qregno = "SELECT DISTINCT RegNo FROM 
				  examregister
					 WHERE AYear='$year' AND RegNo IN (SELECT RegNo FROM student) AND CourseCode = '$coursecode' $whereclause ORDER BY RegNo";
    $dbregno = mysqli_query($zalongwa, $qregno);// or die("No Exam Results for the course - $coursecode - in the year - $year ");
    $total_rows = mysqli_num_rows($dbregno);

    if ($total_rows == 0) {
        $error = urlencode("No student register for the course - $coursecode - in the year - $year ");
        $location = "studentCourseSelect.php?error=$error";
        echo '<meta http-equiv="refresh" content="0; url=' . $location . '">';
        exit;


    }

    if ($total_rows > 0) {
        #getcourse information
        $qcourseinfo = "SELECT * FROM course WHERE coursecode = '$coursecode'";
        $dbcourseinfo = mysqli_query($zalongwa, $qcourseinfo);
        $row_courseinfo = mysqli_fetch_assoc($dbcourseinfo);
        $coursename = $row_courseinfo['CourseName'];
        $coursedept = $row_courseinfo['Department'];
        $courseunit = $row_courseinfo['Units'];
        $courseyear = $row_courseinfo['YearOffered'];

        #start pdf
        include('includes/PDF.php');
        $pdf = &PDF::factory('p', 'a4');      // Set up the pdf object.
        $pdf->open();                         // Start the document.
        $pdf->setCompression(true);           // Activate compression.
        $pdf->addPage();
        $pdf->setFont('Arial', 'I', 8);
        $pdf->text(530.28, 825.89, 'Page ' . $pg);
        $pdf->text(50, 825.89, 'Printed On ' . $today = date("d-m-Y H:i:s"));

        #put page header

        $x = 60;
        $y = 74;
        $i = 1;
        $pg = 1;
        $pdf->text(530.28, 825.89, 'Page ' . $pg);

        //$i=1;
        #count unregistered
        $j = 0;
        #count sex
        $fmcount = 0;
        $mcount = 0;
        $fcount = 0;

        #print header for landscape paper layout
        include '../includes/orgname.php';

        $pdf->setFillColor('rgb', 0, 0, 0);
        $pdf->setFont('Arial', '', 13);
        $pdf->text($x + 170, $y + 14, 'COURSE REGISTRATION SHEET');
        $y = $y + 14;
        $pdf->text($x + 170, $y + 14, 'ACADEMIC YEAR: ' . $year);
        $pdf->text($x + 3, $y + 34, strtoupper('DEPARTMENT:' . $coursedept));
        #reset values of x,y
        $x = 50;
        $y = $y + 40;
        #table course details
        $pdf->line($x, $y, 570.28, $y);
        $pdf->line($x, $y + 15, 570.28, $y + 15);
        $pdf->line($x, $y + 30, 570.28, $y + 30);
        $pdf->line($x, $y, $x, $y + 30);
        $pdf->line($x + 68, $y, $x + 68, $y + 30);
        $pdf->line($x + 468, $y, $x + 468, $y + 30);
        $pdf->line(570.28, $y, 570.28, $y + 30);
        $pdf->setFont('Arial', 'B', 13);
        $pdf->text($x, $y + 12, 'Code');
        $pdf->text($x + 70, $y + 12, 'Course Title');
        $pdf->text($x + 470, $y + 12, 'Credits');
        $pdf->setFont('Arial', '', 13);
        $pdf->text($x, $y + 27, $coursecode);
        $pdf->text($x + 70, $y + 27, $coursename);
        $pdf->text($x + 485, $y + 27, $courseunit);

        #reset the value of y
        $y = $y + 40;
        $pdf->setFont('Arial', 'B', 11);
        $pdf->line($x, $y, 570.28, $y);
        $pdf->line($x, $y + 15, 570.28, $y + 15);
        $pdf->line($x, $y, $x, $y + 15);
        $pdf->text($x + 2, $y + 12, 'S/No');
        $pdf->line($x + 35, $y, $x + 35, $y + 15);
        $pdf->text($x + 40, $y + 12, 'Name');
        $pdf->line($x + 196, $y, $x + 196, $y + 15);
        $pdf->text($x + 200, $y + 12, 'Sex');
        $pdf->line($x + 231, $y, $x + 231, $y + 15);
        $pdf->text($x + 235, $y + 12, 'RegNo');
        $pdf->line($x + 340, $y, $x + 340, $y + 15);
        $pdf->text($x + 342, $y + 12, 'Programme');
        $pdf->line(570.28, $y, 570.28, $y + 15);
        $pdf->setFont('Arial', '', 10);

        #get coursename
        $qcourse = "Select CourseName, Department, StudyLevel from course where CourseCode = '$coursecode'";
        $dbcourse = mysqli_query($zalongwa, $qcourse);
        $row_course = mysqli_fetch_array($dbcourse);
        $coursename = $row_course['CourseName'];
        $coursefaculty = $row_course['Department'];

        #initiate grade counter
        $countm = 0;
        $countf = 0;

        #print title
        $sn = 0;
        while ($row_regno = mysqli_fetch_array($dbregno)) {
            $key = $row_regno['RegNo'];
            $course = $coursecode;
            $ayear = $year;
            $units = $row_course['Units'];
            $sn = $sn + 1;
            $remarks = 'remarks';
            $grade = '';

            #get name and sex of the candidate
            $qstudent = "SELECT Name, Sex,ProgrammeofStudy from student WHERE RegNo = '$key'";
            $dbstudent = mysqli_query($zalongwa, $qstudent) or die("Mwanafunzi huyu hayupo");
            $row_result = mysqli_fetch_array($dbstudent);
            $name = $row_result['Name'];
            $progr = $row_result['ProgrammeofStudy'];
            $sex = strtoupper($row_result['Sex']);

            if ($sex == 'M') {
                $countm = $countm + 1;
            } else {
                $countf = $countf + 1;
            }
            #get student progrmme name
            $qstudent1 = "SELECT * from programme WHERE ProgrammeCode = '$progr'";
            $dbstudent1 = mysqli_query($zalongwa, $qstudent1) or die("Mwanafunzi huyu hayupo");
            $row_result1 = mysqli_fetch_array($dbstudent1);
            $progname = $row_result1['ProgrammeName'];
            # grade marks
            $RegNo = $key;
            include 'includes/choose_studylevel.php';

            #update grade counter
            if ($grade == 'A') {
                $countgradeA = $countgradeA + 1;
                if ($sex == 'M') {
                    $countgradeAm = $countgradeAm + 1;
                } else {
                    $countgradeAf = $countgradeAf + 1;
                }
            } elseif ($grade == 'B+') {
                $countgradeBplus = $countgradeBplus + 1;
                if ($sex == 'M') {
                    $countgradeBplusm = $countgradeBplusm + 1;
                } else {
                    $countgradeBplusf = $countgradeBplusf + 1;
                }
            } elseif ($grade == 'B') {
                $countgradeB = $countgradeB + 1;
                if ($sex == 'M') {
                    $countgradeBm = $countgradeBm + 1;
                } else {
                    $countgradeBf = $countgradeBf + 1;
                }
            } elseif ($grade == 'C') {
                $countgradeC = $countgradeC + 1;
                if ($sex == 'M') {
                    $countgradeCm = $countgradeCm + 1;
                } else {
                    $countgradeCf = $countgradeCf + 1;
                }
            } elseif ($grade == 'D') {
                $countgradeD = $countgradeD + 1;
                if ($sex == 'M') {
                    $countgradeDm = $countgradeDm + 1;
                } else {
                    $countgradeDf = $countgradeDf + 1;
                }
            } elseif ($grade == 'E') {
                $countgradeE = $countgradeE + 1;
                if ($sex == 'M') {
                    $countgradeEm = $countgradeEm + 1;
                } else {
                    $countgradeEf = $countgradeEf + 1;
                }
            } else {
                $countgradeI = $countgradeI + 1;
                if ($sex == 'M') {
                    $countgradeIm = $countgradeIm + 1;
                } else {
                    $countgradeIf = $countgradeIf + 1;
                }
            }
            // }


            #display results

            #calculate summary areas
            $yind = $y + 15;
            $dataarea = 820.89 - $yind;
            if ($dataarea < 20) {
                $pdf->addPage();

                $x = 50;
                $y = 80;
                $pg = $pg + 1;
                $tpg = $pg;
                $pdf->setFont('Arial', 'I', 8);
                $pdf->text(530.28, 820.89, 'Page ' . $pg);
                $pdf->text(300, 820.89, $copycount);
                $pdf->text(50, 825.89, 'Printed On ' . $today = date("d-m-Y H:i:s"));
                $yind = $y;
                $pdf->setFont('Arial', '', 10);
            }
            if ($test2score == -1) {
                $test2score = 'PASS';
            }
            if ($aescore == -1) {
                $aescore = 'PASS';
            }
            if ($marks == -2) {
                $marks = 'PASS';
            }
            $y = $y + 15;
            $pdf->setFont('Arial', '', 8.7);
            $pdf->line($x, $y, 570.28, $y);
            $pdf->line($x, $y + 15, 570.28, $y + 15);
            $pdf->line($x, $y, $x, $y + 15);
            $pdf->text($x + 2, $y + 12, $sn);
            $pdf->line($x + 35, $y, $x + 35, $y + 15);
            $pdf->text($x + 40, $y + 12, $name);
            $pdf->line($x + 196, $y, $x + 196, $y + 15);
            $pdf->text($x + 200, $y + 12, strtoupper($sex));
            $pdf->line($x + 231, $y, $x + 231, $y + 15);
            $pdf->text($x + 232, $y + 12, strtoupper($key));
            $pdf->line($x + 340, $y, $x + 340, $y + 15);
            $pdf->text($x + 342, $y + 12, $progname);
            $pdf->line(570.28, $y, 570.28, $y + 15);
            $pdf->setFont('Arial', '', 10);
        }
    }
    #calculate summary areas
    $yind = $y + 25;
    $summaryarea = 820.89 - $yind;
    if ($summaryarea < 90) {
        $pdf->addPage();

        $x = 50;
        $y = 80;
        $pg = $pg + 1;
        $tpg = $pg;
        $pdf->setFont('Arial', 'I', 8);
        $pdf->text(530.28, 820.89, 'Page ' . $pg);
        $pdf->text(300, 820.89, $copycount);
        $pdf->text(50, 825.89, 'Printed On ' . $today = date("d-m-Y H:i:s"));
        $yind = $y;
        $pdf->setFont('Arial', 'I', 10);
    }
    $pdf->setFont('Arial', '', 10);
    $b = $y + 25;
    if ($b < 820.89) {
        # results summary table
        $y = $b;

        $pdf->text($x, $y, 'TOTAL CANDIDATES:  ');
        $pdf->text($x + 115, $y, $total_rows);
        $pdf->text($x + 50, $y + 14, '  Male (M):   ');
        $pdf->text($x + 115, $y + 14, $countm);
        $pdf->text($x + 50, $y + 24, '  Female (F):  ');
        $pdf->text($x + 115, $y + 24, $countf);
    }
    #calculate signature areas
    $yind = $y + 120;
    $indarea = 820.89 - $yind;
    if ($indarea < 203) {
        $pdf->addPage();

        $x = 50;
        $y = 80;
        $pg = $pg + 1;
        $tpg = $pg;
        $pdf->setFont('Arial', 'I', 8);
        $pdf->text(530.28, 820.89, 'Page ' . $pg);
        $pdf->text(300, 820.89, $copycount);
        $pdf->text(50, 825.89, 'Printed On ' . $today = date("d-m-Y H:i:s"));
        $yind = $y;
    }


    #output file
    $filename = preg_replace("[[:space:]]+", "", $coursecode);
    $pdf->output($filename . '.pdf');
}#end if isset pdf

#get connected to the database and verfy current session
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

# include the header
$szSection = 'Administration';
$szSubSection = 'Registered Students';
$szTitle = 'Registered Students in Course';

$query_AcademicYear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$AcademicYear = mysqli_query($zalongwa, $query_AcademicYear) or die(mysqli_error($zalongwa));
$row_AcademicYear = mysqli_fetch_assoc($AcademicYear);
$totalRows_AcademicYear = mysqli_num_rows($AcademicYear);


//$query_Hostel = "SELECT CourseCode FROM course ORDER BY CourseCode";

#get current year
$qcurrentyear = 'SELECT AYear FROM academicyear where Status = 1';
$dbcurrentyear = mysqli_query($zalongwa, $qcurrentyear);
$row_current = mysqli_fetch_array($dbcurrentyear);
$ayear = $row_current['AYear'];

if ($privilege == 3) {
    $query_Hostel = "
		SELECT DISTINCT course.CourseCode 
		FROM examregister 
			INNER JOIN course ON (examregister.CourseCode = course.CourseCode)
		WHERE  (examregister.RegNo='$username')  ORDER BY course.CourseCode DESC";
} else {
    $query_Hostel = " SELECT CourseCode FROM course ORDER BY CourseCode";
}

$Hostel = mysqli_query($zalongwa, $query_Hostel) or die('query ,$query_Hostel, not executed');
$row_Hostel = mysqli_fetch_assoc($Hostel);
$totalRows_Hostel = mysqli_num_rows($Hostel);


function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
    $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

    switch ($theType) {
        case "text":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "long":
        case "int":
            $theValue = ($theValue != "") ? intval($theValue) : "NULL";
            break;
        case "double":
            $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
            break;
        case "date":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "defined":
            $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
            break;
    }
    return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
    $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
    <!--Font Awesome (added because you use icons in your prepend/append)-->
    <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css"/>
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="./css/breadcrumb.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /*background-color: #009688;*/
        }

        .card {
            /*background-color: #324359;*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /*color: white;*/
            padding: 0px;
            border-radius: 0px !important;
        }

        @media (max-width: 34em) {
            .card {
                margin-top: 20px;
            }
        }

        @media (max-width: 48em) {
            .card {
                margin-top: 20px;
            }
        }

    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">
            <?php
            if (isset($_POST['print']) && ($_POST['print'] == "PreView") || ($_GET['page']) || ($_GET['search'])) {
            #get post variables
            $year = trim(addslashes($_POST['ayear']));
            $coursecode = trim(addslashes($_POST['Hall']));

            //Control pagination
            if (isset($_GET['page'])) {
                $pageNum = $_GET['page'];
                $year = $_GET['ayear'];
                $coursecode = $_GET['coursecode'];

                # get all students for this course
                $qregno = "SELECT DISTINCT RegNo FROM
            examregisterstudent
            WHERE (AYear='$year' AND CourseCode = '$coursecode') ORDER BY RegNo";
                $dbregno = mysqli_query($zalongwa, $qregno) or die("No Student Registered for the course - $coursecode - in the year - $year ");
                $total_rows = mysqli_num_rows($dbregno);
                //echo $total_rows;
            } //Control Search
            else if (isset($_GET['search'])) {
                $key = $_GET['key'];
                $year = $_GET['ayear'];
                $coursecode = $_GET['coursecode'];
                $pageNum = 1;

                # get all students for this course
                $qregno = "SELECT DISTINCT RegNo FROM
            examregisterstudent
            WHERE (AYear='$year' AND CourseCode = '$coursecode'AND RegNo='$key')";
                $dbregno = mysqli_query($zalongwa, $qregno) or die("No Student" . $key . "for the course - $coursecode - in the year - $year ");
                $total_rows = mysqli_num_rows($dbregno);

            } else {

                $pageNum = 1;

                # get all students for this course
                $qregno = "SELECT DISTINCT RegNo FROM
            examregisterstudent
            WHERE (AYear='$year' AND CourseCode = '$coursecode') ORDER BY RegNo";
                $dbregno = mysqli_query($zalongwa, $qregno) or die("<br><br><p>No Student Registered for the course - $coursecode - in the year - $year </p>");
                $total_rows = mysqli_num_rows($dbregno);
            }
            //echo 'This'.$total_rows;
            if ($total_rows > 0) {
            echo "<form action='$_SERVER[PHP_SELF]' method='GET'>
                <table class='resView'>
                    <tr>
                        <td class='resViewhd'nowrap><em>Enter RegNo</em></td>
                        <td class='resViewtd'><input type='text' name='key'size='30'></td>";
            ?>
            <td class='resViewtd'>
                <input type='hidden' name='ayear' value="<?php echo $year; ?>">
                <input type='hidden' name='coursecode' value="<?php echo $coursecode; ?>">
                <input type='submit' name='search' value='Search' onmouseover="this.style.background='#DEFEDE'"
                       onmouseout="this.style.background='lightblue'"
                       style='background-color:lightblue;color:black;font-size:9pt;font-weight:bold'
                       title="Click to Search the List">
                <?php
                echo "</td>
</tr>
</table></form>";

                $rowPerPage = 12;
                $offset = ($pageNum - 1) * $rowPerPage;
                $k = $offset + 1;

                $query = $qregno . " LIMIT $offset,$rowPerPage";
                //echo $query;
                $result = mysqli_query($zalongwa, $query) or die(mysqli_error($zalongwa));


                ?>
                <table class="resView">
                    <tr>
                        <td class="resViewhd">
                            <div align="center">S/No</div>
                        </td>
                        <td class="resViewhd" nowrap>
                            <div align="center">Name</div>
                        </td>
                        <td class="resViewhd" nowrap>
                            <div align="center">Sex</div>
                        </td>
                        <td class="resViewhd" nowrap>
                            <div align="center">RegNo</div>
                        </td>
                        <td class="resViewhd" nowrap>
                            <div align="center">Programme</div>
                        </td>
                        <!-- <td class='resViewhd' width="4%"><strong>Drop</strong></td> -->

                    </tr>
                    <?php
                    #get coursename
                    $qcourse = "Select CourseName, Department, StudyLevel from course where CourseCode = '$coursecode'";
                    $dbcourse = mysqli_query($zalongwa, $qcourse);
                    $row_course = mysqli_fetch_array($dbcourse);
                    $coursename = $row_course['CourseName'];
                    $coursefaculty = $row_course['Department'];


                    #initiate grade counter
                    $countm = 0;
                    $countf = 0;

                    #print title
                    echo '<p><br><br>Year: ' . $year . '<br>';
                    echo 'Course: ' . $coursecode . ' - ' . $coursename;
                    echo '<br>TOTAL CANDIDATES: <strong>' . $total_rows . '</strong>';

                    echo "</p>";
                    #initialise s/no

                    $sn = $k - 1;

                    while ($row_regno = mysqli_fetch_array($result)) {
                        $key = $row_regno['RegNo'];
                        $course = $coursecode;
                        $ayear = $year;
                        $units = $row_course['Units'];
                        $sn = $sn + 1;
                        $remarks = 'remarks';
                        $grade = '';

                        #get year exam done
                        $examyear = $year; //result of the year
                        $examyear = substr($examyear, 0, 4);

                        #get name and sex of the candidate
                        $qstudent = "SELECT Name, Sex,ProgrammeofStudy from student WHERE RegNo = '$key'";
                        $dbstudent = mysqli_query($zalongwa, $qstudent) or die("Mwanafunzi huyu hayupo");
                        $row_result = mysqli_fetch_array($dbstudent);
                        $name = $row_result['Name'];
                        $progr = $row_result['ProgrammeofStudy'];
                        $sex = strtoupper($row_result['Sex']);

                        if ($sex == 'M') {
                            $countm = $countm + 1;
                        } else {
                            $countf = $countf + 1;
                        }
                        #get student progrmme name
                        $qstudent1 = "SELECT * from programme WHERE ProgrammeCode = '$progr'";
                        $dbstudent1 = mysqli_query($zalongwa, $qstudent1) or die("Mwanafunzi huyu hayupo");
                        $row_result1 = mysqli_fetch_array($dbstudent1);
                        $progname = $row_result1['ProgrammeName'];

                        $RegNo = $key;

                        include 'includes/choose_studylevel.php';


                        #display results
                        ?>
                        <tr>
                            <td class="resViewhd">
                                <?php if ($privilege == 2) {
                                    //echo "<a href=\"lecturerEditsingleresult.php?Candidate=$key&Course=$course\">$sn</a>" ;
                                    echo $sn;
                                } else {
                                    echo $sn;
                                } ?>
                            </td>
                            <td class="resViewtd" nowrap>
                                <div align="left"><?php echo $name ?> </div>
                            </td>
                            <td class="resViewtd" nowrap>
                                <div align="center"><?php echo strtoupper($sex) ?> </div>
                            </td>
                            <td class="resViewtd" nowrap>
                                <div align="center"><?php echo strtoupper($key) ?> </div>
                            </td>
                            <td class="resViewtd" nowrap>
                                <div align="center"><?php echo strtoupper($progname) ?> </div>
                            </td>
                            <!-- <td class='resViewhd'><?php print "<a href=\"lecturerexamresultdelete.php?RegNo=$key&ayear=$ayear&key=$coursecode\">Drop</a>"; ?></td> -->

                        </tr>
                        <?php

                        #update grade counter
                        if ($grade == 'A')
                            $countgradeA = $countgradeA + 1;
                        elseif ($grade == 'B+')
                            $countgradeBplus = $countgradeBplus + 1;
                        elseif ($grade == 'B')
                            $countgradeB = $countgradeB + 1;
                        elseif ($grade == 'C')
                            $countgradeC = $countgradeC + 1;
                        elseif ($grade == 'D')
                            $countgradeD = $countgradeD + 1;
                        elseif ($grade == 'E')
                            $countgradeE = $countgradeE + 1;
                        else
                            $countgradeI = $countgradeI + 1;

                    }

                    echo "</table>";
                    //echo '<br>Male: <strong>'. $countm .'</strong>';
                    //echo '&nbsp;&nbsp;Female: <strong>'.$countf.'</strong>';

                    $data = mysqli_query($zalongwa, $qregno);
                    $numrows = mysqli_num_rows($data);
                    $result = mysqli_query($zalongwa, $data);
                    $row = mysqli_fetch_array($data);


                    $maxPage = ceil($numrows / $rowPerPage);

                    $self = $_SERVER['PHP_SELF'];
                    $nav = '';
                    for ($page = 1; $page <= $maxPage; $page++) {
                        if ($page == $pageNum) {
                            $nav .= " $page";
                            $nm = $page;
                        } else {
                            $nav .= "<a href=\"$self?page=$page&ayear=$year&coursecode=$coursecode\">$page</a>";
                        }
                    }
                    if ($pageNum > 1) {
                        $page = $pageNum - 1;
                        $prev = "<a href=\"$self?page=$page&ayear=$year&coursecode=$coursecode\">Previous</a>";
                        $first = "<a href=\"$self?page=1\">[First]</a>";
                    } else {
                        $prev = '&nbsp;';
                        $first = '&nbsp;';
                    }

                    if ($pageNum < $maxPage) {
                        $page = $pageNum + 1;
                        $next = "<a href=\"$self?page=$page&ayear=$year&coursecode=$coursecode\">Next</a>";
                        $last = "<a href=\"$self?page=$maxPage\" class='mymenu'>[Last Page]</a>";
                    } else {
                        $next = '&nbsp;';
                        $last = '&nbsp;';
                    }
                    echo "<table>
<tr>
<td width='200'>&nbsp;&nbsp;&nbsp;&nbsp;$prev&nbsp;&nbsp;</td>
<td width='200'>&nbsp;&nbsp;Page $nm of $maxPage&nbsp;&nbsp;</td>
<td width='200'>&nbsp;&nbsp;$next&nbsp;&nbsp;</td>
<td>&nbsp;&nbsp;&nbsp;<font color='#CCCCCC'></font></td>
</tr></table></center>";

                    //End of Pagination
                    ?>
                    <p></p>
                    <!--
TOTAL CANDIDATES: <strong>
      <?php


                    echo $total_rows ?>
    </strong> -->
                    <br>
                    <?php
                    //close if total statement
                    } else {
                        echo '<font color="#C11B17">No Student Found, Try Again</font> <br><br>';
                        # redisplay the form incase results werenot found
                        ?>

                        <div class="card">
                            <h3 class="card-header">
                                <?php echo $szTitle; ?></h3>
                            <div class="card-block">
                                <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST" name="courseresults"
                                      id="courseresults">
                                    <div class="form-group">
                                        <label>Academic Year:</label>
                                        <select class="form-control" name="ayear" id="select2">
                                            <option value="">SelectAcademicYear</option>
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_AcademicYear['AYear'] ?>"><?php echo $row_AcademicYear['AYear'] ?></option>
                                                <?php
                                            } while ($row_AcademicYear = mysqli_fetch_assoc($AcademicYear));
                                            $rows = mysqli_num_rows($AcademicYear);
                                            if ($rows > 0) {
                                                mysqli_data_seek($AcademicYear, 0);
                                                $row_AcademicYear = mysqli_fetch_assoc($AcademicYear);
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlSelect">Course Code:</label>
                                        <select class="form-control" name="Hall" id="select">
                                            <option value="">Select Course Code</option>
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_Hostel['CourseCode'] ?>"><?php echo $row_Hostel['CourseCode'] ?></option>
                                                <?php
                                            } while ($row_Hostel = mysqli_fetch_assoc($Hostel));
                                            $rows = mysqli_num_rows($Hostel);
                                            if ($rows > 0) {
                                                mysqli_data_seek($Hostel, 0);
                                                $row_Hostel = mysqli_fetch_assoc($Hostel);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <input class="btn btn-success btn-md btn-block" type="submit" name="print"
                                           id="print"
                                           value="PreView"
                                           title="Click to Preview the List">
                                    <input type="submit" name="PDF" id="PDF" value="Registered List-PDF"
                                           title="Click to Create a Course Registration List-PDF Report">
                                </form>

                            </div>
                        </div>

                        <?php
                    }
                    } else {
                        if (isset($_GET['error'])) {
                            $error = urldecode($_GET['error']);
                            echo "<p style='color:red;font-family: tahoma, verdana;font-size: 12px;'>$error</p>";
                        }
                        ?>
                        <div class="card">
                            <h3 class="card-header">
                                <?php echo $szTitle; ?></h3>
                            <div class="card-block">
                                <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST" name="courseresults"
                                      id="courseresults">
                                    <div class="form-group">
                                        <label>Academic Year:</label>
                                        <select class="form-control" name="ayear" id="select2">
                                            <option value="">SelectAcademicYear</option>
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_AcademicYear['AYear'] ?>"><?php echo $row_AcademicYear['AYear'] ?></option>
                                                <?php
                                            } while ($row_AcademicYear = mysqli_fetch_assoc($AcademicYear));
                                            $rows = mysqli_num_rows($AcademicYear);
                                            if ($rows > 0) {
                                                mysqli_data_seek($AcademicYear, 0);
                                                $row_AcademicYear = mysqli_fetch_assoc($AcademicYear);
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlSelect">Course Code:</label>
                                        <select class="form-control" name="Hall" id="select">
                                            <option value="">Select Course Code</option>
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_Hostel['CourseCode'] ?>"><?php echo $row_Hostel['CourseCode'] ?></option>
                                                <?php
                                            } while ($row_Hostel = mysqli_fetch_assoc($Hostel));
                                            $rows = mysqli_num_rows($Hostel);
                                            if ($rows > 0) {
                                                mysqli_data_seek($Hostel, 0);
                                                $row_Hostel = mysqli_fetch_assoc($Hostel);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <input onclick="return show_confirm()" name="PDF" type="submit"
                                           class="btn btn-success btn-md btn-block" id="Confirm"
                                           value="Registered List-PDF">
                                </form>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

        </div>
    </div>
</div>
<br><br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<script>
    $(document).ready(function () {
        var date_input = $('input[name="date"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        var options = {
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
</script>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
</body>
</html>

