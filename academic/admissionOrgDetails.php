<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header
include('lecturerMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Policy Setup';
$szSubSection = 'Policy Setup';
include("lecturerheader.php");

if (isset($_GET['details'])) {
    $result = $zalongwa->query("SELECT * FROM organisation WHERE Id=" . $_GET['details']);
    $institution = $result->fetch_assoc();
    if ($result->num_rows > 0) {
        ?>
        <table class='table_view'>
            <tr class='header'>
                <td> Field</td>
                <td> Field Value</td>
            </tr>
            <tr class='list'>
                <td> Name</td>
                <td><?php echo $institution['Name']; ?></td>
            </tr>
            <tr class='list'>
                <td> Physical Address:</td>
                <td><?php echo $institution['Address']; ?></td>
            </tr>
            <tr class='list'>
                <td> City:</td>
                <td><?php echo $institution['city']; ?></td>
            </tr>
            <tr class='list'>
                <td> Website URL:</td>
                <td><?php echo $institution['website']; ?></td>
            </tr>
            <tr class='list'>
                <td> Telephone:</td>
                <td><?php echo $institution['tel']; ?></td>
            </tr>
            <tr class='list'>
                <td> Fax:</td>
                <td><?php echo $institution['fax']; ?></td>
            </tr>
            <tr class='list'>
                <td> Email:</td>
                <td><?php echo $institution['email']; ?></td>
            </tr>
        </table>
        <br>
        <table class='table_view'>
            <tr style='float: right' class='list'>
                <td><a href="admissionOrg.php">Back</a></td>
            </tr>
        </table>
        <?php
    } else {
        echo "Sorry, No Records Found <br>";
    }
}
# include the footer
include("../footer/footer.php");
?>