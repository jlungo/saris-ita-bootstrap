<?php
#STEP 1.0 Upload data file
# delete old files
@unlink("./temp/zalongwa.xls");
#constants
$fileavailable = 0;

#validate the file
$file_name = ($_POST['userfile']);
$overwrite = addslashes($_POST['radiobutton']);

$file = $_FILES['userfile']['name'];
//The original name of the file on the client machine. 
$filetype = $_FILES['userfile']['type'];
//The mime type of the file, if the browser provided this information. An example would be "image/gif". 
$filesize = $_FILES['userfile']['size'];
//The size, in bytes, of the uploaded file. 
$filetmp = $_FILES['userfile']['tmp_name'];
//The temporary filename of the file in which the uploaded file was stored on the server. 
$filetype_error = $_FILES['userfile']['error'];
$filename = time() . $_FILES['userfile']['name'];
?>
<?php

// In PHP earlier then 4.1.0, $HTTP_POST_FILES  should be used instead of $_FILES.
if (is_uploaded_file($filetmp)) {
    $filename = time() . $file;
    copy($filetmp, "$filename");
} else {
    echo "File not uploaded,try again..<br/>";
}
move_uploaded_file($filetmp, "./temp/$filename");

#check file extension
$str = $filename;
$i = strrpos($str, ".");
if (!$i) {
    return "";
}

$l = strlen($str) - $i;
$ext = substr($str, $i + 1, $l);
$pext = strtolower($ext);
if ($pext != "xls") {
    print "<h2>ERROR</h2>File Extension Unknown.<br>";
    print "<p>Please Upload a File with the Extension .xls ONLY<br>";
    print "The file you uploaded have this extension: $pext</p>\n";
    echo '<meta http-equiv = "refresh" content ="10; url = zimport.php">';
    include('../footer/footer.php');
    unlink($filename);
    exit();
}
?>
<?php

if ($pext == "xls") {
    $fileavailable = 1;
}
#STEP 2.0 exceute sql scripts
if ($fileavailable == 1) {
	
    echo "<strong>Data Import in Process</strong><br /><br>";
    echo "Progress Status.............<br>";
    $fcontents = file("./temp/$filename");
   
    $today = date('Y-m-d');
    
    /** PHPExcel */
    require_once '../Classes/PHPExcel/IOFactory.php';
    $objPHPExcel = PHPExcel_IOFactory::load("./temp/$filename");
    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
        //echo 'umeona hii - '.$impcourse;	
        $worksheetTitle = $worksheet->getTitle();
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        $nrColumns = ord($highestColumn) - 64;
        $num=0;
        for ($i = 3; $i <= $highestRow; ++$i) {
            //for ($j=0; $j<$highestColumnIndex; ++$j) {
            echo '<br>';

            $cell = $worksheet->getCellByColumnAndRow(0, $i);
            $CourseCode = trim($cell->getValue());
            
            $cell = $worksheet->getCellByColumnAndRow(1, $i);
           $CourseName = trim($cell->getValue());
            
            $cell = $worksheet->getCellByColumnAndRow(2, $i);
            $Units = trim($cell->getValue());
            
            
            #check if regno exists
            $qcourse = "SELECT * FROM course WHERE TRIM(CourseCode)='$CourseCode'";
            $dbcourse = mysqli_query($zalongwa, $qcourse);
            $exist = mysqli_num_rows($dbcourse);
				
            if ($exist==0) {
				#imports data
	           $sqlcwk = "INSERT INTO course SET
									    CourseCode ='$CourseCode',
										CourseName = '$CourseName',
										Capacity = '1000',
										Units='$Units',
										Department='$department',
										StudyLevel='$nta'	
										";
                      $insert =   mysqli_query($zalongwa, $sqlcwk);
                        if (mysqli_error($zalongwa)) {
                         echo  " This course <b>" . $CourseCode ."</b> - Not Imported!<br>\n";		
                        }else{
							 echo  " This course <b>" . $CourseCode . "</b> - Imported Successfuly!<br>\n";
							}
                
            } else {
                 echo  " This course <b>" . $CourseCode ."</b> does exist-  - Not Imported!<br>\n";
            }
				

        }
    }
    unlink("temp/$filename");
    unlink("$filename");
}
?> 
