<?php
set_time_limit(0);
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */

	/** Error reporting */
	error_reporting(E_ALL);

	date_default_timezone_set('Europe/London');

	/** PHPExcel */
	require_once '../Classes/PHPExcel.php';

	$papersize='PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4';
	$fontstyle='Arial';
	$font=10.5;
	$prog=$_POST['degree'];
	$qprog= "SELECT ProgrammeCode, Title, Faculty FROM programme WHERE ProgrammeCode='$prog'";
	$dbprog = mysqli_query($zalongwa, $qprog);
	$row_prog = mysqli_fetch_array($dbprog);
	$programme = $row_prog['Title'];

	// hold statistical data
	$get_statistical_data =array(); // hold general 
	$config_data =array(); // hold coursecode and its cell coordinate in worksheet one
	$get_statistical_data_gender =array(); // hold statistical data by gender

	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();


	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	
	// Rename sheet
	//$objPHPExcel->getActiveSheet()->setTitle('Semester I');
	$objPHPExcel->getActiveSheet()->setTitle('End of Year Results');


	// Set properties
	$objPHPExcel->getProperties()->setCreator("Zalongwa")
							 ->setLastModifiedBy("Juma Lungo")
							 ->setTitle($programme)
							 ->setSubject("Semester Exam Results")
							 ->setDescription("Semester Exam Results.")
							 ->setKeywords("zalongwa saris software")
							 ->setCategory("Exam result file");
	
	$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);
	
	# Set protected sheets to 'true' kama hutaki waandike waziedit sheets zako. Kama unataka wazi-edit weka 'false'
	$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
	
	#set worksheet orientation and size
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize($papersize);
	
	#Set page fit width to true
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
	
	#Set footer page numbers
	$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
	#Show or hide grid lines
	$objPHPExcel->getActiveSheet()->setShowGridlines(false);
	
	#Set sheet style (fonts and font size)
	$objPHPExcel->getDefaultStyle()->getFont()->setName($fontstyle);
	$objPHPExcel->getDefaultStyle()->getFont()->setSize($font); 
	
	#Set page margins
	$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(1);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(1);
	
	# Set Rows to repeate in each page
	$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 5);
	
	# Set Report Logo

	
	$styleArray = array(
		'borders' => array(
			'outline' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			),
		),
	);


	

	for($col='A';$col<'ZZ';$col++) { 
		$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
	}
	#Get Organisation Name
	$qorg = "SELECT * FROM organisation";
	$dborg = mysqli_query($zalongwa, $qorg);
	$row_org = mysqli_fetch_assoc($dborg);
	$org = $row_org['Name'];
	
	$prog=$_POST['degree'];
	$cohotyear = $_POST['cohot'];
	$ayear = $_POST['ayear'];
	$nameofcurrentyear = $ayear;
	$qprog= "SELECT * FROM programme WHERE ProgrammeCode='$prog'";
	$dbprog = mysqli_query($zalongwa, $qprog);
	$row_prog = mysqli_fetch_array($dbprog);
	$programme = $row_prog['Title'];
	$faculty = $row_prog['Faculty'];
	$nta = $row_prog['Ntalevel'];
	
	//calculate year of study
	$entry = intval(substr($cohotyear,0,4));
	$current = intval(substr($ayear,0,4));
	$yearofstudy=$current-$entry;
	
	$deg=addslashes($_POST['degree']);
	$year = addslashes($_POST['ayear']);
	$cohot = addslashes($_POST['cohot']);
	
	$sem = addslashes($_POST['sem']);
	if ($sem =='Semester I' || $sem =='Semester III' || $sem =='Semester V' || $sem =='Semester VII' || $sem =='Semester IX'){
			$semval = 1;
		}
	elseif ($sem=='Semester II' || $sem=='Semester IV' || $sem=='Semester VI' || $sem=='Semester VIII' || $sem=='Semester X'){
			$semval = 2;
		}
	$seme_ster= $semval;
	

	#calculate year of study
    require_once('../includes/calculateyearofstudy.php');
    
	#cohort number
	$yearofstudy = $yearofstudy +1;
	$totalcolms =0;
	
	#determine total number of columns
	
	$qcolmns = "SELECT DISTINCT CourseCode, Status FROM courseprogramme 
				WHERE  (ProgrammeID='$deg') AND (YearofStudy='$yearofstudy') AND (AYear='$year') AND (Semester='$semval')
				ORDER BY Semester, CourseCode";
			
	$dbcolmns = mysqli_query($zalongwa, $qcolmns);
	$dbcolmnscredits = mysqli_query($zalongwa, $qcolmns);
	$dbexamcat = mysqli_query($zalongwa, $qcolmns);
	$totalcolms = mysqli_num_rows($dbcolmns);
	
 	$degree=$deg;
 	
 
	#getting the number of credits to be studied	
	
	$mysql = mysqli_query($zalongwa, "SELECT * FROM coursecountprogramme WHERE ProgrammeID='$deg' AND YearofStudy='$yearofstudy' 
						AND AYear='$year' AND Semester='$semval'");
							
	$mysq = mysqli_fetch_array($mysql);
	$semx = $mysq['CourseCount']; 
		
	# Print Report header	
	$rpttitle='OVERALL SUMMARY OF EXAMINATIONS RESULTS -'.$ayear;
	$objPHPExcel->getActiveSheet()->mergeCells('A1:Z1');
	$objPHPExcel->getActiveSheet()->mergeCells('A2:Z2');
	$objPHPExcel->getActiveSheet()->mergeCells('A3:Z3');

	$objPHPExcel->getActiveSheet()->mergeCells('A5:Z5');


	$objPHPExcel->getActiveSheet()->mergeCells('A4:Z4');
	
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', strtoupper($org))->setCellValue('A2', strtoupper($faculty))->setCellValue('A3', strtoupper($rpttitle))
				->setCellValue('A4', "NTA LEVEL :  " .strtoupper($nta). "                                          FIELD OF STUDY  :  " . strtoupper($programme))
				->setCellValue('A5', "YEAR OF STUDY :  " . $class . "         SEMESTER : " . strtoupper($sem) . "           DATE :.....................................           WEIGHT : CA 40%            WEIGHT : SE60%");

	
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20); 
	$objPHPExcel->getActiveSheet()->getStyle('A2:A3')->getFont()->setSize(16); 
	$objPHPExcel->getActiveSheet()->getStyle('A2:A3')->getFont()->setSize(13);
	$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getStyle('A1:A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
	$colm = "A";
	$rows = 6;
	$rows1 = $rows +1;
	
	$objPHPExcel->getActiveSheet()->getStyle('A6:BE6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
	#Print Serial Number
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, '#');
		    
	$colm++; 
		    
	#Print Sex
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, 'Name'); 
	$colm++;
	
	#Print RegNo
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, 'RegNo'); 
	$colm++; 	

	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, 'Sex'); 
	$colm++; 
	

	$progcredits=0;
	$totalunits = 0; 
	$coursecount = 0;
	$rows=6;
	   
	#Print Course Headers
	while($rowcourseheader = mysqli_fetch_array($dbcolmnscredits)) { 
		#get course units
		$course= $rowcourseheader['CourseCode'] ;
		
		#get semester 
		$qsem= "SELECT DISTINCT Semester FROM examresult WHERE CourseCode='$course' and AYear='$year'";
		$dbsem = mysqli_query($zalongwa, $qsem);
		$row_sem = mysqli_fetch_array($dbsem);
		$sem=$row_sem['Semester'];
		
		if ($sem =='Semester I'){
			$semval = 1;
			}
		elseif ($sem=='Semester II'){
			$semval = 2;
			}
	   	
	   	#get course unit
		$qunits = "SELECT Units FROM course WHERE CourseCode='$course'";
		$dbunits = mysqli_query($zalongwa, $qunits);
		$row_units = mysqli_fetch_array($dbunits);
		$unit=$row_units['Units'];
		$progcredits = $progcredits+$unit;	 

		$colmc = $colm;
	
	
		$colmc++; $colmc++; $colmc++;
		$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colmc.$rows)->getStyle($colm.$rows.':'.$colmc.$rows)->applyFromArray($styleArray);		    
		
		#write down the course code.			     			      
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $course.' ('.$unit.')');  
		
		#rotate text to 90 degree
		$config_data[$course]=$colm.'-'.$rows;
		$totalunits = $totalunits + $unit;		
	
	
		$rows++;
		$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm++.$rows, ' CA ');

		$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm++.$rows, ' FE ');
		
		$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm++.$rows, 'Total');
		
		$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm++.$rows, ' GD ');
		$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);
			
		$rows = $rows -1;
		//$sem='';
		}
 
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)->getStyle($colm.$rows.':'.$colm.$rows1);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, '#Courses');
	$colm++;
	
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, 'POINTS');
	$ptscell=$colm.$rows.':'.$colm.$rows1;
	$colm++;
	
	
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, 'GPA');
	$ptscell=$colm.$rows.':'.$colm.$rows1;
	$colm++;
		
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, 'REMARK');
	$colm++;
	
	# print candidate results
	$overallpasscount = 0;
	$overallsuppcount = 0;
	$overallinccount = 0;
	$overalldiscocount = 0;
	
	//remark variables
	$fepass = '0';	$mapass = '0';
	$fesupp = '0';	$masupp = '0';
	$fefail = '0';	$mafail = '0';
	$feabsc = '0';	$maabsc = '0';
	$feothr = '0';	$maothr = '0';
	
	//array to hold statistical report
	$statistical_grade=array();
	
	$qstudent = "SELECT Name, RegNo, Sex, DBirth, ProgrammeofStudy, Faculty, Sponsor, EntryYear, Status, Class 
				 FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY Name";
		
				 
	$dbstudent = mysqli_query($zalongwa, $qstudent);
	$totalstudent = mysqli_num_rows($dbstudent);
	$z=1;
	$rows = 8;
	

	#freez
	$objPHPExcel->getActiveSheet()->freezePane($colm.$rows);
		
	while($rowstudent = mysqli_fetch_array($dbstudent)) {
		// check if student disco or deceaded
		$regno = $rowstudent['RegNo'];
		$check_disco ="SELECT * FROM studentremark WHERE (Remark ='DISCO' OR Remark ='DECEASED') AND RegNo='$regno' AND AYear < '$nameofcurrentyear'";
		$result_disco =mysqli_query($zalongwa, $check_disco);
		$num_disco_st = mysqli_num_rows($result_disco);
		
		if($num_disco_st == 0) {
			$colms = "A";		
			$regno = $rowstudent['RegNo'];
			
			$name = stripslashes($rowstudent['Name']);			
			$sex = $rowstudent['Sex'];
			$bdate = $rowstudent['DBirth'];
			$comb = $rowstudent['Class'];
			$degree = stripslashes($rowstudent["ProgrammeofStudy"]);
			$faculty = stripslashes($rowstudent["Faculty"]);
			$sponsor = stripslashes($rowstudent["Sponsor"]);
			$entryyear = stripslashes($result['EntryYear']);
			$ststatus = stripslashes($rowstudent['Status']);
			
		
		
			#initialise
			$totalunit=0;
			$gmarks=0;
			$totalfailed=0;
			$totalinccount=0;
			$unittaken=0;
			$creditslost=0;
			$requiredcredits=0;
			$creditstaken=0;
			$inco_count=0;
			$supp_count=0;
			$optcredits=0;
			$extracreditstaken=0;
			$sgp=0;
			$totalsgp=0;
			$gpa=0;
			$pct=0;
			$chas=0;
			$failed = 0;
			$marks=0;
			
			# new values
			$totalfailed=0;
			$totalinccount=0;
			$halfsubjects=0;
			$ovremark='';
			$gmarks=0;
			$avg =0;
			$gmarks=0;	
			$decrement = 0;
			$key = $regno;
			$compute_gpa = false;			
		
			# Print results
			
			#Print Serial Number
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $z);
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
			$colms++; 
			
		
			#Print Serial Number
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $name);
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
			$colms++; 
					
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $regno);
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
			$colms++;
			
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $sex);
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
			$colms++;
		
	
			//search for course results and prints	
			//get all courses
			
			$qcourselist = "SELECT DISTINCT CourseCode, Status FROM courseprogramme 
							WHERE  (ProgrammeID='$deg') AND (YearofStudy='$yearofstudy') AND (AYear='$year') AND (Semester='$semval')
							ORDER BY Semester, CourseCode";
				
			$dbcourselist = mysqli_query($zalongwa, $qcolmns);
			
			$niku1 = 0;
			while($row_courselist = mysqli_fetch_array($dbcourselist)) { 
				$course= $row_courselist['CourseCode'];
				$coption = $row_courselist['Status']; 
				
				$qcoursestd="SELECT Units, Department, StudyLevel FROM course WHERE CourseCode = '$course'";	
				$dbcoursestd = mysqli_query($zalongwa, $qcoursestd);
				$row_coursestd = mysqli_fetch_array($dbcoursestd);
				$unit = $row_coursestd['Units'];					
				
				$remarks = 'remarks';
				$RegNo = $key;
				$currentyear=$year;
				$projscore="";
				
				include 'choose_studylevel.php';
				
				//find course's examninationregulation
				$qexamreg = "SELECT StudyLevel FROM course  WHERE  CourseCode = '$course'";
				$dbexamreg = mysqli_query($zalongwa, $qexamreg);
				$row_result_sheet = mysqli_fetch_array($dbexamreg);
				$studylevel= $row_result_sheet['StudyLevel'];				

				
				//control shading of failed results
				$levels_of_study = array(4,5,6,7,8,10,11,12,13); //study levels array
				$levels_of_pass = array(4=>array(20,30),5=>array(20,30),6=>array(18,27),7=>array(16,24),8=>array(16,24),
										10=>array('n/a',50),11=>array('n/a',50),12=>array('n/a',45),13=>array('n/a',40)); //pass margin array
				
				foreach($levels_of_study as $study_value){
					if($study_value == $studylevel){
						if($test2score<>'n/a' && $test2score<>'' && $test2score<$levels_of_pass[$studylevel][0]){
							$test2score='';
							$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
							$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->getFill()->getStartColor()->setARGB('CCCCCC');
							$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $test2score);							
							}
						else{
							$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $test2score);
							}
						$colms++;
						
						if($aescore<>'' && $aescore<$levels_of_pass[$studylevel][1]){
							$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
							$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->getFill()->getStartColor()->setARGB('CCCCCC');
							$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $aescore);							
							}
						else{
							$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $aescore);
							}
						$colms++;
						}
					}

				$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, ($marks));
				$colms++;
			
				
				
				#shade failed grades
				if($remark<>'PASS' && $grade>'C'){
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->getStartColor()->setARGB('CCCCCC');
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->applyFromArray($styleArray);
					
					$decrement =$decrement + $unit;
					
					if($unit > 0){
						$failed = $failed + 1;
						}				
					}
				else{
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->applyFromArray($styleArray);
					}
				if($grade > 'C'){
					$supp_count++;
					}
                 
				if($grade == 'I'){
					$inco_count++;
					$compute_gpa = true;
					}
                 		
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms++.$rows, $grade);
				
				// gets statistical report
				if($grade == "A"){
		          	if(array_key_exists($course, $statistical_grade)){
		          		  if(array_key_exists('A', $statistical_grade[$course])){
		          		  	$statistical_grade[$course]['A']=$statistical_grade[$course]['A']+1;
		          		  }else{
		          		  	$statistical_grade[$course]['A']=1;
		          		  }
		          	}else{
		          	$statistical_grade[$course]['A']=1;	
		          	}
		          	
		          }
		        else if($grade == "B+"){
		           	if(array_key_exists($course, $statistical_grade)){
		          		  if(array_key_exists('B+', $statistical_grade[$course])){
		          		  	$statistical_grade[$course]['B+']=$statistical_grade[$course]['B+']+1;
		          		  }else{
		          		  	$statistical_grade[$course]['B+']=1;
		          		  }
		          	}else{
		          	$statistical_grade[$course]['B+']=1;	
		          	}
		          }
		        else if($grade=="B"){
		           	if(array_key_exists($course, $statistical_grade)){
		          		  if(array_key_exists('B', $statistical_grade[$course])){
		          		  	$statistical_grade[$course]['B']=$statistical_grade[$course]['B']+1;
		          		  }else{
		          		  	$statistical_grade[$course]['B']=1;
		          		  }
		          	}else{
		          	$statistical_grade[$course]['B']=1;	
		          	}
		          }
		        else if($grade == "C"){
		           	if(array_key_exists($course, $statistical_grade)){
		          		  if(array_key_exists('C', $statistical_grade[$course])){
		          		  	$statistical_grade[$course]['C']=$statistical_grade[$course]['C']+1;
		          		  }else{
		          		  	$statistical_grade[$course]['C']=1;
		          		  }
		          	}else{
		          	$statistical_grade[$course]['C']=1;	
		          	}
		          }
		        else if($grade == "D"){
		           	if(array_key_exists($course, $statistical_grade)){
		          		  if(array_key_exists('D', $statistical_grade[$course])){
		          		  	$statistical_grade[$course]['D']=$statistical_grade[$course]['D']+1;
		          		  }else{
		          		  	$statistical_grade[$course]['D']=1;
		          		  }
		          	}else{
		          	$statistical_grade[$course]['D']=1;	
		          	}
		          }
		        else if($grade == "F"){
		           	if(array_key_exists($course, $statistical_grade)){
		          		  if(array_key_exists('F', $statistical_grade[$course])){
		          		  	$statistical_grade[$course]['F']=$statistical_grade[$course]['F']+1;
		          		  }else{
		          		  	$statistical_grade[$course]['F']=1;
		          		  }
		          	}else{
		          	$statistical_grade[$course]['F']=1;	
		          	}
		          }
		        else if($grade == "I"){
		           	if(array_key_exists($course, $statistical_grade)){
		          		  if(array_key_exists('I', $statistical_grade[$course])){
		          		  	$statistical_grade[$course]['I']=$statistical_grade[$course]['I']+1;
		          		  }else{
		          		  	$statistical_grade[$course]['I']=1;
		          		  }
		          	}else{
		          	$statistical_grade[$course]['I']=1;	
		          	}
		          }
		    				
				}
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows,' '.' '.' '. $totalcolms.' '.' '.' ');
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
			$colms++;
			
			# Assign total credits
			$requiredcredits = $progcredits-$optcredits-$extracreditstaken;
			
			$curr_semester=$semval;
 			
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, number_format($totalsgp,2));
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
			$colms++;
			
			//compute gpa
			$gpa= substr($totalsgp/$unittaken,0,3);
			if($gpa == 1){
				$gpa = '1.0';
				}
			elseif($gpa == 2){
				$gpa = '2.0';
				}
			elseif($gpa == 3){
				$gpa = '3.0';
				}
			elseif($gpa == 4){
				$gpa = '4.0';
				}
			elseif($gpa == 5){
				$gpa = '5.0';
				}				
		    
		    if($supp_count >0){
				$remark = "SUPP";                 
				}
			elseif ($suppfe==1){
				$remark = "SUPP";
				}
			else{
				$remark = "PASS";
				}
			
			if($compute_gpa){
				$remark = "INCO";
				$gpa ='---';
				}
				
				//get total required unit per year
			$sql = "SELECT * FROM coursecountprogramme WHERE ProgrammeID='$prog' AND AYear='$ayear' AND YearofStudy='$yearofstudy' AND Semester='$seme_ster'";
			$mysq_result = mysqli_query($zalongwa, $sql);
			$num_row = mysqli_num_rows($mysq_result);
			
			$objPHPExcel->getActiveSheet()->getCell($colms.$rows)->setValueExplicit($gpa, PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
			$colms++;
			
			
			if($remark == 'DISCO'){
				$remark = 'SUPP';
				}
				
			elseif($remark == 'PASS'){
				//if pass then clean sheet					
				}
			elseif($remark == 'ABSC'){
				$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);					
				$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->getStartColor()->setARGB('CCCCCC');
				$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->applyFromArray($styleArray);					
				}
							
			//$objPHPExcel->setActiveSheetIndex(1)->setCellValue($colms.$rows, $gpa);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $remark);
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);			
			# prints overall remarks
			$colms++;
			
			//track remark statistics
			if($remark == 'PASS'){
				if($sex=='F'){
					$fepass = $fepass + 1;
					}
				else{
					$mapass = $mapass + 1;
					}
				}
			elseif($remark == 'SUPP'){
				if($sex=='F'){
					$fesupp = $fesupp + 1;
					}
				else{
					$masupp = $masupp + 1;
					}
				}
			elseif($remark == 'FAIL'){
				if($sex=='F'){
					$fefail = $fefail + 1;
					}
				else{
					$mafail = $mafail + 1;
					}
				}
			elseif($remark == 'INCO'){
				if($sex=='F'){
					$feabsc = $feabsc + 1;
					}
				else{
					$maabsc = $maabsc + 1;
					}
				}
			else{
				if($sex=='F'){
					$feothr = $feothr + 1;
					}
				else{
					$maothr = $maothr + 1;
					}
				}	
			
			// get end of the course column
			$end_subject = "E";
			for ($x =0; $x < $totalcolms; $x++){
				$end_subject++;
				}
				
			$rows++;
			$z=$z+1;
			$counter = $rows;
			//$supp='';
			$fsup='';
			$remark = '';
			$suppfe='';
			
			}
		}
 	
 	
	/*******************************
	 * PRINT PERFORMANCE STATISTICS
	 *******************************/
	$colm = "C";		
	$y = $colm;
	
	$track = 55 - $counter;
	$rows = $counter;
	
	for($v=1; $v<3; $v++){
		$rows++;
		}
	$counter = $counter + 5;	
		
	$vw = 10;
	for($v=1; $v<$vw; $v++){
		$y++;
		}
	
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$y.$rows)->getStyle($colm.$rows.':'.$y.$rows)->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$y.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	//$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$y.$rows)->getFill()->getStartColor()->setARGB('000FFFFF');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, ' SUMMARY OF PERFOMANCE STATISTICS '); 				
	$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);
	$rows++;
	
	$colrow = $rows;
	$studentstat = array('REMARKS','FEMALE','MALE','SUBTOTAL');
	$oldstat = array('PASS','SUPP','FAIL','INCOMPLETE','OTHER REMARKS');
	$newstat = array('PASS','REPEAT','OTHER REMARKS');
	
	$sumcounter=1;
	$looparray = $oldstat;
	
	foreach($studentstat as $colhead){
		$colm='C';
		$objPHPExcel->getActiveSheet()->mergeCells($colm.$colrow.':'.$colm.$colrow)->getStyle($colm.$colrow.':'.$colm.$colrow)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$colrow, $colhead); 
		$objPHPExcel->getActiveSheet()->getStyle($colm.$colrow)->applyFromArray($styleArray);		
		$colrow++;
		}
	$colm++;
	$colx=$colm;
	
	foreach($looparray as $arrayvalue){
		$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colx.$rows)->getStyle($colm.$rows.':'.$colx.$rows)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $arrayvalue); 
		$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);
		
		if($sumcounter<2){
			$colm++;
			$colx=$colm;
			$colx++;	
			}
		else{
			$colm=$colx;
			$colm++;
			$colx++;
			$colx++;
			}
		$sumcounter++;
		}
	$rows++;
		
		
	$fearray = array($fepass,$fesupp,$fefail,$feabsc,$feothr);
	$maarray = array($mapass,$masupp,$mafail,$maabsc,$maothr);
	$totalarray = array($fepass+$mapass,$fesupp+$masupp,$fefail+$mafail,$feabsc+$maabsc,$feothr+$maothr);
	
	$colm = 'C';
	$colm++;
	$colx=$colm;
	$sumcounter=1;
	foreach($fearray as $femalevalue){
		$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colx.$rows)->getStyle($colm.$rows.':'.$colx.$rows)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $femalevalue); 
		$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);
		
		if($sumcounter<2){
			$colm++;
			$colx=$colm;
			$colx++;	
			}
		else{
			$colm=$colx;
			$colm++;
			$colx++;
			$colx++;
			}
		$sumcounter++;
		}
	$rows++;
	
	$colm = 'C';
	$colm++;
	$colx=$colm;
	$sumcounter=1;
	foreach($maarray as $malevalue){
		$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colx.$rows)->getStyle($colm.$rows.':'.$colx.$rows)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $malevalue); 
		$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);
		
		if($sumcounter<2){
			$colm++;
			$colx=$colm;
			$colx++;	
			}
		else{
			$colm=$colx;
			$colm++;
			$colx++;
			$colx++;
			}
		$sumcounter++;
		}
	$rows++;
	
	$colm = 'C';
	$colm++;
	$colx=$colm;
	$sumcounter=1;
	foreach($totalarray as $totalvalue){
		$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colx.$rows)->getStyle($colm.$rows.':'.$colx.$rows)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $totalvalue); 
		$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);
		
		if($sumcounter<2){
			$colm++;
			$colx=$colm;
			$colx++;	
			}
		else{
			$colm=$colx;
			$colm++;
			$colx++;
			$colx++;
			}
		$sumcounter++;
		}
	$rows++;
	
	
	
	$colms="C";
	$counter = $rows + 2;
	$cc=$counter;
	$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	//$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->getFill()->getStartColor()->setARGB('000FFFFF');
	$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$counter, ' GRADE SUMMARY ');
	
	$y = $colms;
	for($x=1; $x<=15; $x++){		
		$y++;			
		$colms2 = $y;
		}
	$colms1 = $colms;
	$colms1++;
	
	if(count($statistical_grade) > 0){
		$array=array('A','B+','B','C','D','F','I');
		foreach ($array as $gr=>$grv){
			$counter+=1;
			$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->applyFromArray($styleArray);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$counter, ' '.$grv);				
			}
		
		$count = $cc;
		$colms="D";
		
		foreach ($statistical_grade as $w=>$v){			
			$objPHPExcel->getActiveSheet()->getStyle($colms.$count)->applyFromArray($styleArray);
			$colmp=$colms;
			$colmp++;
			$objPHPExcel->getActiveSheet()->getStyle($colms.$count.':'.$colmp.$count)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			//$objPHPExcel->getActiveSheet()->getStyle($colms.$count.':'.$colmp.$count)->getFill()->getStartColor()->setARGB('000FFFFF');
			$objPHPExcel->getActiveSheet()->mergeCells($colms.$count.':'.$colmp.$count);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$count, ' '.$w);
			$count++;
			$objPHPExcel->getActiveSheet()->getStyle($colms.$count)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->mergeCells($colms.$count.':'.$colmp.$count);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$count, ' '.($v['A'] > 0 ? $v['A']:0));	
			$count++;
			$objPHPExcel->getActiveSheet()->getStyle($colms.$count)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->mergeCells($colms.$count.':'.$colmp.$count);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$count, ' '.($v['B+'] > 0 ? $v['B+']:0));
			$count++;
			$objPHPExcel->getActiveSheet()->getStyle($colms.$count)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->mergeCells($colms.$count.':'.$colmp.$count);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$count, ' '.($v['B']>0 ? $v['B']:0));	
			$count++;
			$objPHPExcel->getActiveSheet()->getStyle($colms.$count)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->mergeCells($colms.$count.':'.$colmp.$count);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$count, ' '.($v['C']>0 ? $v['C']:0));	
			$count++;
			$objPHPExcel->getActiveSheet()->getStyle($colms.$count)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->mergeCells($colms.$count.':'.$colmp.$count);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$count, ' '.($v['D']>0 ?$v['D'] :0));	
			$count++;
			$objPHPExcel->getActiveSheet()->getStyle($colms.$count)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->mergeCells($colms.$count.':'.$colmp.$count);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$count, ' '.($v['F'] > 0 ? $v['F']:0));	
			$count++;
			$objPHPExcel->getActiveSheet()->getStyle($colms.$count)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->mergeCells($colms.$count.':'.$colmp.$count);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$count, ' '.($v['I'] > 0 ? $v['I']:0));	
			$count++;
			
			$count=$cc;
			$colms=$colmp;
			$colms++;
			}
		}

	
	$colms = 'C';
 	$counter = $count + 10;
 	$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	//$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->getFill()->getStartColor()->setARGB('000FFFFF');
	$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->applyFromArray($styleArray);
 	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$counter, ' KEY FOR THE COURSE CODES  ');
	//$colms++;
	
	//get the course details
	$qcolmn = mysqli_query($zalongwa, "SELECT DISTINCT cp.CourseCode, c.CourseName FROM courseprogramme cp, course c 
							WHERE (cp.ProgrammeID='$prog') AND (cp.YearofStudy='$yearofstudy') AND (AYear='$ayear') AND (Semester='$semval')
							AND (cp.CourseCode=c.CourseCode) ORDER BY cp.CourseCode");
		
		
	$y = $colms;
	for($x=1; $x<=15; $x++){		
		$y++;			
		$colms2 = $y;
		}
	$colms1 = $colms;
	$colms1++;
	
	while($course = mysqli_fetch_array($qcolmn)){
		
		//print coursecode
		$counter += 1;
		$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$counter, ' '.$course['CourseCode']);				
		
		//print course title
		$objPHPExcel->getActiveSheet()->mergeCells($colms1.$counter.':'.$colms2.$counter)
					->getStyle($colms1.$counter.':'.$colms2.$counter)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms1.$counter, ' '.$course['CourseName']);
		//$colms++;	
		}
	
	
	// Rename sheet
	$objPHPExcel->getActiveSheet()->setTitle('Semester Report');


	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="SARIS_Exam_Results.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	
	/*make pdf
	
	header('Content-Type: application/pdf');
	header('Content-Disposition: attachment;filename="SARIS_Exam_Results.pdf"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
	*/
	
	$objWriter->save('php://output');
	exit;
