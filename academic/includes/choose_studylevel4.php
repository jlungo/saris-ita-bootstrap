<?php
	$studylevel= $row_course['StudyLevel'];
	if($studylevel == 1){
		include'ntalevel1_gradscale.php';
	}elseif($studylevel == 2){
		include'ntalevel2_gradscale.php';
	}elseif($studylevel == 3){
		include'ntalevel3_gradscale.php';
	}elseif($studylevel == 4){
		include'ntalevel4_gradscale.php'; 
	}elseif($studylevel == 5){
		include'ntalevel5_gradscale.php'; 
	}elseif($studylevel == 6){
		include'ntalevel6_gradscale.php'; 
	}elseif($studylevel == 7){
		include'ntalevel7_gradscale.php'; 
	}elseif($studylevel == 8){
		include'ntalevel8_gradscale.php'; 
	}elseif($studylevel == 9){
		include'ntalevel9_gradscale.php'; 
	}elseif($studylevel == 10){
		include'traditional_gradscale.php'; // Traditional Grades
	}elseif($studylevel == 11){
		include'traditional_gradscale.php'; // Traditional Grades for Logistic Dept added
	}elseif($studylevel == 12){
		include'occasional_gradscale.php';
	}elseif($studylevel == 13){
		include'phd_gradscale.php';
	}elseif($studylevel == 14){
		include'pgd_doctor_gradscale.php';
	}elseif($studylevel == 15){
		include'shortterm_gradscale.php';
	}elseif($studylevel == 16){
		include'occasional_gradscale.php';
	}elseif($studylevel == 17){
		include'exchange_gradscale.php';
	}else{ 
		include'ntalevel9_gradscale.php';
	}
	$studylevel= '';

?>
