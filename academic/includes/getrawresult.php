<?php
	#reset gpa calculation values
	$point = '';
	$grade = '';
	$remark = '';
	$gradesupp='';
    	$marks='';
    	$aescore='';
	$nullae=0;
	$projscore='';
	$supscore='';
	$ptscore='';
	$spscore='';
	$test2score='';
if($module==3){

		$checked =" AND Checked=1";

		}

    else{

		$checked='';

		} 
    
    if($studylevel<=8){
		#query Homework One
	$qhw1 = "SELECT ExamCategory, Examdate, ExamScore FROM examresult WHERE CourseCode='$course' AND RegNo='$RegNo' AND ExamCategory=1 $checked";
	$dbhw1=mysqli_query($zalongwa, $qhw1);
	$total_hw1 = mysqli_num_rows($dbhw1);
	$row_hw1=mysqli_fetch_array($dbhw1);
	$value_hw1score=$row_hw1['ExamScore'];
	if(($total_hw1>0)&&($value_hw1score<>'')){
		$hw1date=$row_hw1['ExamDate'];
		$hw1score=number_format($value_hw1score,2);
	}else{
		$hw1score='';
	}

	#query Homework Two
	$qhw2 = "SELECT ExamCategory, Examdate, ExamScore FROM examresult WHERE CourseCode='$course' AND RegNo='$RegNo' AND ExamCategory=2 $checked";
	$dbhw2=mysqli_query($zalongwa, $qhw2);
	$total_hw2 = mysqli_num_rows($dbhw2);
	$row_hw2=mysqli_fetch_array($dbhw2);
	$value_hw2score=$row_hw2['ExamScore'];
	if(($total_hw2>0)&&($value_hw2score<>'')){
		$hw2date=$row_hw2['ExamDate'];
		$hw2score=number_format($value_hw2score,2);
	}else{
		$hw2score='';
	}

	#query Quiz One
	$qqz1 = "SELECT ExamCategory, Examdate, ExamScore FROM examresult WHERE CourseCode='$course' AND RegNo='$RegNo' AND ExamCategory=3 $checked";
	$dbqz1=mysqli_query($zalongwa, $qqz1);
	$total_qz1 = mysqli_num_rows($dbqz1);
	$row_qz1=mysqli_fetch_array($dbqz1);
	$value_qz1score=$row_qz1['ExamScore'];
	if(($total_qz1>0)&&($value_qz1score<>'')){
		$qz1date=$row_qz1['ExamDate'];
		$qz1score=number_format($value_qz1score,2);
	}else{
		$qz1score='';
	}

	#query Quiz Two
	$qqz2 = "SELECT ExamCategory, Examdate, ExamScore FROM examresult WHERE CourseCode='$course' AND RegNo='$RegNo' AND ExamCategory=4 $checked";
	$dbqz2=mysqli_query($zalongwa, $qqz2);
	$total_qz2 = mysqli_num_rows($dbqz2);
	$row_qz2=mysqli_fetch_array($dbqz2);
	$value_qz2score=$row_qz2['ExamScore'];
	if(($total_qz2>0)&&($value_qz2score<>'')){
		$qz2date=$row_qz2['ExamDate'];
		$qz2score=number_format($value_qz2score,0);
	}else{
		$qz2score='';
		$nullca=1;
	}
	
	#query Semester Examination
	$qae = "SELECT ExamCategory, Examdate, ExamScore, AYear, Semester FROM examresult WHERE CourseCode='$course' AND RegNo='$RegNo' AND ExamCategory=5 $checked";
	$dbae=mysqli_query($zalongwa, $qae);
	$total_ae = mysqli_num_rows($dbae);
	$row_ae=mysqli_fetch_array($dbae);
	$value_aescore=$row_ae['ExamScore'];
	if(($total_ae>0)&&($value_aescore<>'')){
		$aedate=$row_ae['ExamDate'];
		$aescore=number_format($value_aescore,0);
	}else{
		$remarks = "Inc";
		$aescore='';
		$nullae=1;
	}
	
	#query Group Assignment
	$qga = "SELECT ExamCategory, Examdate, ExamScore FROM examresult WHERE CourseCode='$course' AND RegNo='$RegNo' AND ExamCategory=6 $checked";
	$dbga=mysqli_query($zalongwa, $qga);
	$total_ga = mysqli_num_rows($dbga);
	$row_ga=mysqli_fetch_array($dbga);
	$value_gascore=$row_ga['ExamScore'];
	if(($total_ga>0)&&($value_gascore<>'')){
		$gadate=$row_ga['ExamDate'];
		$gascore=number_format($value_gascore,2);
	}else{
		$gascore='';
	}

	#query Supplimentatary Exam
	$qsup = "SELECT ExamCategory, Examdate, ExamScore FROM examresult WHERE CourseCode='$course' AND RegNo='$RegNo' AND ExamCategory=7 $checked";
	$dbsup=mysqli_query($zalongwa, $qsup);
	$row_sup=mysqli_fetch_array($dbsup);
	$row_sup_total=mysqli_num_rows($dbsup);
	$supdate=$row_sup['ExamDate'];
	$supscore=$row_sup['ExamScore'];
	if(($row_sup_total>0)&&($supscore<>'')){
		$remarks = '';
		$supscore = number_format($supscore,0);
		#empty coursework
		$test2score ='n/a';
	}
	
	#query Classroom Test One (1)
	$qct1 = "SELECT ExamCategory, Examdate, ExamScore FROM examresult WHERE CourseCode='$course' AND RegNo='$RegNo' AND ExamCategory=9 $checked";
	$dbct1=mysqli_query($zalongwa, $qct1);
	$row_ct1=mysqli_fetch_array($dbct1);
	$row_ct1_total=mysqli_num_rows($dbct1);
	$ct1date=$row_ct1['ExamDate'];
	$ct1score=$row_ct1['ExamScore'];
	if(($row_ct1_total>0)&&($ct1score<>'')){
		$remark = '';
		$ct1score = number_format($ct1score,2);
	}else{
		$ct1score='';
	}
	
	#query Classroom Test Two (2)
	$qct2 = "SELECT ExamCategory, Examdate, ExamScore FROM examresult WHERE CourseCode='$course' AND RegNo='$RegNo' AND ExamCategory=10 $checked";
	$dbct2=mysqli_query($zalongwa, $qct2);
	$row_ct2=mysqli_fetch_array($dbct2);
	$row_ct2_total=mysqli_num_rows($dbct2);
	$ct2date=$row_ct2['ExamDate'];
	$ct2score=$row_ct2['ExamScore'];
	if(($row_ct2_total>0)&&($ct2score<>'')){
		$remarks = '';
		$ct2score = number_format($ct2score,2);
	}else{
		$ct2score='';
	}

	
	#query Special Exam
	$qsp = "SELECT ExamCategory, Examdate, ExamScore FROM examresult WHERE CourseCode='$course' AND RegNo='$RegNo' AND ExamCategory=11 $checked";
	$dbsp=mysqli_query($zalongwa, $qsp);
	$total_sp = mysqli_num_rows($dbsp);
	$row_sp=mysqli_fetch_array($dbsp);
	$value_spscore=$row_sp['ExamScore'];
	if(($total_sp>0)&&($value_spscore<>'')){
		$spdate=$row_sp['ExamDate'];
		$remarks='';
		$spscore=number_format($value_spscore);
		
	}else{
		$spscore='';
	}
	
	
	#Check if the course is option
	$qcoption = "SELECT Status FROM courseprogramme 
				WHERE  (ProgrammeID='$deg') AND (CourseCode='$course')";
	$dbcoption = mysqli_query($zalongwa, $qcoption);
	$row_coption = mysqli_fetch_array($dbcoption);
	$coption = $row_coption ['Status']; 
	
	#check if ommited
	$qcount = "SELECT DISTINCT Count FROM examresult WHERE CourseCode='$course' AND RegNo='$RegNo'";
	$dbcount=mysqli_query($zalongwa, $qcount);
	$row_count=mysqli_fetch_array($dbcount);
	$count =$row_count['Count'];
	
	if ($count==1){
		$unit=0;
		$coursename ='*'.$coursename;
	}
	
	if(($coption==2) and ($marks<1)){
		$unit=0;
	}	
	$projscore='';$ptscore='';		
	#get Continuous Assessment (C.A)
	$test2score = $hw1score + $hw2score + $qz1score + $qz2score + $gascore + $proscore + $ct1score + $ct2score;
        $test2score = number_format($test2score,1);

		}else{
			
	#query Project Exam
	$qproj = "SELECT ExamCategory, Examdate, ExamScore FROM examresult WHERE CourseCode='$course' AND RegNo='$RegNo' AND ExamCategory=12 $checked";
	$dbproj=mysqli_query($zalongwa, $qproj);
	$row_proj=mysqli_fetch_array($dbproj);
	$row_proj_total=mysqli_num_rows($dbproj);
	$projdate=$row_proj['ExamDate'];
	$projscore=$row_proj['ExamScore'];
	if(($row_proj_total>0)&&($projscore<>'')){
		$remarks = '';
		$projscore = number_format($projscore,0);
		$aescore = $projscore;
		#empty coursework
		$test2score ='n/a';
		$supscore='';
	}else{
			$projscore='';
	}
	#query Practical Training Exam
	$qpt = "SELECT ExamCategory, Examdate, ExamScore FROM examresult WHERE CourseCode='$course' AND RegNo='$RegNo' AND ExamCategory=8";
	$dbpt=mysqli_query($zalongwa, $qpt);
	$row_pt=mysqli_fetch_array($dbpt);
	$row_pt_total=mysqli_num_rows($dbpt);
	$ptdate=$row_pt['ExamDate'];
	$ptscore=$row_pt['ExamScore'];
	if(($row_pt_total>0)&&($ptscore<>'')){
		$remarks = '';
		$ptscore = number_format($ptscore,0);
		$aescore = $ptscore;
		#empty coursework
		$test2score ='n/a';
	}else{
			$ptscore = '';
			$tmarks='';
			$remarks = "Inc";
		$remarks = "Inc";

		$nullae=1;
			}    
	
}

		
	#get total marks
	if (($row_sup_total>0)&&($supscore<>'')){
				$tmarks = $supscore;
					$gradesupp='C';
	}elseif(($row_proj_total>0)&&($projscore<>'')){
		$tmarks = $projscore;$ptscore='';
	}elseif(($row_tp_total>0)&&($tpscore<>'')){
		$tmarks = $tpscore;
	}elseif(($row_pt_total>0)&&($ptscore<>'')){
		//if(($row_sup_total=='')){$supscore='';}
		//if(($row_proj_total=='')){$projscore='';}
		$tmarks = $ptscore;
	}elseif(($total_sp>0)&&($spscore<>'')){
		$aescore=$spscore;
		$tmarks = $test2score + $spscore;
	}else{
		$tmarks = $test2score + $aescore;
	}
	#round marks
	$marks = number_format($tmarks,2);

?>
