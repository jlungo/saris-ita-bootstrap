<?php

//select student
$qstudent = "SELECT * from student WHERE RegNo = '$key'";
$dbstudent = mysqli_query($zalongwa, $qstudent) or die("No Exam Results for the Candidate" . mysqli_error($zalongwa));
$row_result = mysqli_fetch_array($dbstudent);
$name = $row_result['Name'];
$eregno = $row_result['RegNo'];
$degree = $row_result['ProgrammeofStudy'];
$eregNo = $eregno;

//get degree name
$qdegree = "Select Title from programme where ProgrammeCode = '$degree'";
$dbdegree = mysqli_query($zalongwa, $qdegree);
$row_degree = mysqli_fetch_array($dbdegree);
$programme = $row_degree['Title'];

echo "$name - $eregno <br> $programme";


//query academeic year
$qayear = "SELECT DISTINCT AYear FROM examresult WHERE RegNo = '$eregNo' ORDER BY AYear ASC";
$dbayear = mysqli_query($zalongwa, $qayear);
?>
    <table class="table table-striped table-bordered table-responsive" width="100%" cellspacing="0">
        <?php
        //query exam results sorted per years
        while ($rowayear = mysqli_fetch_object($dbayear)) {
            $currentyear = $rowayear->AYear;

            $qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$eregno' AND  AYear='$currentyear'";
            $dbremarks = mysqli_query($zalongwa, $qremarks);
            $row_remarks = mysqli_fetch_assoc($dbremarks);
            $totalremarks = mysqli_num_rows($dbremarks);
            $studentremarks = $row_remarks['Remark'];


            if ($studentremarks == 'RWAR' || $studentremarks == 'RWDR' || $studentremarks == 'WITHLD') {
                $reason = ($studentremarks == 'RWAR') ? "Results Withheld for Academic Reasons" : "Results Withheld for Debt Reasons";
                echo "<tr><td colspan='11' align='center' style='color:red'>$reason</td></tr>";
            }


            # get all courses for this candidate
            $qcourse = "SELECT DISTINCT course.CourseName, 
									  course.Units, 
									  course.StudyLevel, 
									  course.Department, 
									  examresult.CourseCode, 
									  examresult.Semester, 
									  examresult.Status 
					  FROM 
							course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
					  WHERE (examresult.RegNo='$eregno') AND 
							(examresult.AYear = '$currentyear') AND 
							(examresult.Checked='1') 
				      ORDER BY examresult.AYear DESC, examresult.Semester ASC";
            $dbcourse = mysqli_query($zalongwa, $qcourse) or die("No Exam Results for the Candidate - $key ");
            $total_rows = mysqli_num_rows($dbcourse);

            if ($total_rows > 0) {

                $qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$eregno' AND  AYear='$currentyear'";
                $dbremarks = mysqli_query($zalongwa, $qremarks);
                $row_remarks = mysqli_fetch_assoc($dbremarks);
                $totalremarks = mysqli_num_rows($dbremarks);
                $studentremarks = $row_remarks['Remark'];

                $query_course = "SELECT Remark, Description FROM examremark WHERE Remark='$studremark'";
                $course = mysqli_query($zalongwa, $query_course) or die(mysqli_error($zalongwa));
                $row_course = mysqli_fetch_assoc($course);
                $totalRows_course = mysqli_num_rows($course);
                $remarkdesc = $row_course['Description'];

                if ($totalremarks == 0) {
                    #initialise s/no
                    $sn = 0;
                    #print name and degree
                    //select student
                    $qstudent = "SELECT Name, RegNo, ProgrammeofStudy from student WHERE RegNo = '$eregNo'";
                    $dbstudent = mysqli_query($zalongwa, $qstudent) or die("No Exam Results for the Candidate" . mysqli_error($zalongwa));
                    $row_result = mysqli_fetch_array($dbstudent);
                    $name = $row_result['Name'];
                    $eregno = $row_result['RegNo'];
                    $degree = $row_result['ProgrammeofStudy'];

                    //get degree name
                    $qdegree = "Select Title from programme where ProgrammeCode = '$degree'";
                    $dbdegree = mysqli_query($zalongwa, $qdegree);
                    $row_degree = mysqli_fetch_array($dbdegree);
                    $programme = $row_degree['Title'];

                    #initialise
                    $totalunit = 0;
                    $unittaken = 0;
                    $sgp = 0;
                    $totalsgp = 0;
                    $gpa = 0;
                    ?>
                    <thead class="table-inverse">
                    <tr>
                        <th scope="col" colspan='8'>
                            <div align="center"><?php echo '<b>' . $rowayear->AYear;
                                echo '</b>' ?></div>
                        </th>
                    </tr>
                    </thead>
                    <tr style="background:#ceceff; font-weight:bold">
                        <th width="10%">Semester</th>
                        <th width="10%">Code</th>
                        <th width="40%">Name</th>
                        <th width="5%">Credits</th>
                        <th width="5%">Grade</th>
                        <th width="10%">Remark</th>
                        <th width="5%">Points</th>
                        <th width="5%">GPA</th>
                    </tr>
                    <?php
                    while ($row_course = mysqli_fetch_array($dbcourse)) {
                        $course = $row_course['CourseCode'];
                        $unit = $row_course['Units'];
                        $semester = $row_course['Semester'];
                        $coursename = $row_course['CourseName'];
                        $coursefaculty = $row_course['Department'];
                        if ($row_course['Status'] == 1) {
                            $status = 'Core';
                        } else {
                            $status = 'Elective';
                        }
                        $sn = $sn + 1;
                        $remarks = 'remarks';

                        include 'choose_studylevel.php';

                        #display results

                        ?>
                        <tr>
                            <td>
                                <div align="left"><?php echo $semester ?></div>
                            </td>
                            <td>
                                <div align="left"><?php echo $course ?></div>
                            </td>
                            <td>
                                <div align="left"><?php echo $coursename; ?></div>
                            </td>
                            <td>
                                <div align="center"><?php echo $row_course['Units'] ?></div>
                            </td>
                            <td>
                                <div align="center"><?php echo $grade; ?></div>
                            </td>
                            <td>
                                <div align="center"><?php echo $remark; ?></div>
                            </td>
                            <td>
                                <div align="center"><?php echo $sgp; ?></div>
                            </td>
                            <td></td>
                        </tr>
                    <?php } ?>
                    <tr style="background:#ceceff; font-weight:bold">
                        <td scope="col"></td>
                        <td scope="col"></td>
                        <td width="488"></td>

                        <td width="30">
                            <div align="center"><?php echo $unittaken; ?></div>
                        </td>
                        <td width="38"></td>
                        <td width="38"></td>
                        <td width="31">
                            <div align="center"><?php echo $totalsgp; ?></div>
                        </td>
                        <td width="31">
                            <div align="center"><?php echo @substr($totalsgp / $unittaken, 0, 3); ?></div>
                        </td>
                    </tr>

                <?php } elseif ($studentremarks == 'DISCO') {
                    #initialise s/no
                    $sn = 0;
                    #print name and degree
                    //select student
                    $qstudent = "SELECT Name, RegNo, ProgrammeofStudy from student WHERE RegNo = '$eregNo'";
                    $dbstudent = mysqli_query($zalongwa, $qstudent) or die("No Exam Results for the Candidate" . mysqli_error($zalongwa));
                    $row_result = mysqli_fetch_array($dbstudent);
                    $name = $row_result['Name'];
                    $eregno = $row_result['RegNo'];
                    $degree = $row_result['ProgrammeofStudy'];

                    //get degree name
                    $qdegree = "Select Title from programme where ProgrammeCode = '$degree'";
                    $dbdegree = mysqli_query($zalongwa, $qdegree);
                    $row_degree = mysqli_fetch_array($dbdegree);
                    $programme = $row_degree['Title'];

                    #initialise
                    $totalunit = 0;
                    $unittaken = 0;
                    $sgp = 0;
                    $totalsgp = 0;
                    $gpa = 0;
                    ?>

                    <tr>
                        <td scope="col" colspan='8'>
                            <div align="center"><?php echo '<b>' . $rowayear->AYear;
                                echo '</b>' ?></div>
                        </td>
                    </tr>
                    <tr style="background:#ceceff; font-weight:bold">
                        <td scope="col">Semester</td>
                        <td>Code</td>
                        <td>Name</td>
                        <td>Credits</td>
                        <td>Grade</td>
                        <td>Remark</td>
                        <td>Points</td>
                        <td>GPA</td>
                    </tr>
                    <?php
                    while ($row_course = mysqli_fetch_array($dbcourse)) {
                        $course = $row_course['CourseCode'];
                        $unit = $row_course['Units'];
                        $semester = $row_course['Semester'];
                        $coursename = $row_course['CourseName'];
                        $coursefaculty = $row_course['Department'];
                        if ($row_course['Status'] == 1) {
                            $status = 'Core';
                        } else {
                            $status = 'Elective';
                        }
                        $sn = $sn + 1;
                        $remarks = 'remarks';

                        include 'choose_studylevel.php';

                        #display results

                        ?>
                        <tr>
                            <td>
                                <div align="left"><?php echo $semester ?></div>
                            </td>
                            <td>
                                <div align="left"><?php echo $course ?></div>
                            </td>
                            <td width="450">
                                <div align="left"><?php echo $coursename; ?></div>
                            </td>
                            <td width="30">
                                <div align="center"><?php echo $row_course['Units'] ?></div>
                            </td>
                            <td width="38">
                                <div align="center"><?php echo $grade; ?></div>
                            </td>
                            <td width="31">
                                <div align="center"><?php echo $remark; ?></div>
                            </td>
                            <td width="31">
                                <div align="center"><?php echo $sgp; ?></div>
                            </td>
                            <td width="31"></td>
                        </tr>
                    <?php } ?>
                    <tr style="background:#ceceff; font-weight:bold">
                        <td scope="col"></td>
                        <td scope="col"></td>
                        <td width="488">
                            <div align="center" style="color:red">DISCONTINUED</div>
                        </td>

                        <td width="30">
                            <div align="center"><?php echo $unittaken; ?></div>
                        </td>
                        <td width="38"></td>
                        <td width="38"></td>
                        <td width="31">
                            <div align="center"><?php echo $totalsgp; ?></div>
                        </td>
                        <td width="31">
                            <div align="center"><?php echo @substr($totalsgp / $unittaken, 0, 3); ?></div>
                        </td>
                    </tr>

                <?php } else {
                    if (!$preg_match[$c]) {
                    } else {
                        echo "$c" . ".Sorry, No Records Found for '$preg_match[$c]'<br><hr>";
                    }
                }
            } else {
                echo '<tr><td><div style="color:red"> Exams results for this Academic Year not yet available !</div></td></tr>';
            }
        }//ends while rowayear
        ?>
    </table>
<?php
mysqli_close($zalongwa);
