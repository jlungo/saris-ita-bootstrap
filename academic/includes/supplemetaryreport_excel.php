<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */

/** Error reporting */
error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

    /** PHPExcel */
    require_once '../Classes/PHPExcel.php';
	
	
 $ayear = $_POST['ayear'];    
 $cohort = $_POST['cohort'];
 $cohot = $_POST['cohort'];    
 $prog = $_POST['programme'];

$papersize='PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4';
$fontstyle='Arial';
$font=10.5;
$qprog= "SELECT ProgrammeCode, Title, Faculty FROM programme WHERE ProgrammeCode='$prog'";
$dbprog = mysqli_query($zalongwa, $qprog);
$row_prog = mysqli_fetch_array($dbprog);
$programme = $row_prog['Title'];

// hold statistical data
$get_statistical_data =array(); // hold general 
$config_data =array(); // hold coursecode and its cell coordinate in worksheet one
$get_statistical_data_gender =array(); // hold statistical data by gender

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();


	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	
	// Rename sheet
	//$objPHPExcel->getActiveSheet()->setTitle('Semester I');
	$objPHPExcel->getActiveSheet()->setTitle('End of Year Results');


// Set properties
$objPHPExcel->getProperties()->setCreator("Zalongwa")
							 ->setLastModifiedBy("Juma Lungo")
							 ->setTitle($programme)
							 ->setSubject("Semester Exam Results")
							 ->setDescription("Semester Exam Results.")
							 ->setKeywords("zalongwa saris software")
							 ->setCategory("Exam result file");
	
	$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);
	
	# Set protected sheets to 'true' kama hutaki waandike waziedit sheets zako. Kama unataka wazi-edit weka 'false'
	$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
	
	#set worksheet orientation and size
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize($papersize);
	
	#Set page fit width to true
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
	
	#Set footer page numbers
	$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
	#Show or hide grid lines
	$objPHPExcel->getActiveSheet()->setShowGridlines(false);
	
	#Set sheet style (fonts and font size)
	$objPHPExcel->getDefaultStyle()->getFont()->setName($fontstyle);
	$objPHPExcel->getDefaultStyle()->getFont()->setSize($font); 
	
	#Set page margins
	$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(1);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(1);
	
	# Set Rows to repeate in each page
	$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 5);
	
	
	$styleArray = array(
		'borders' => array(
			'outline' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			),
		),
	);



	for($col='A';$col<'ZZ';$col++) { 
		$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setWidth(4);
	}
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	#Get Organisation Name
	$qorg = "SELECT * FROM organisation";
	$dborg = mysqli_query($zalongwa, $qorg);
	$row_org = mysqli_fetch_assoc($dborg);
	$org = $row_org['Name'];
	
	/*$prog=$_POST['degree'];
	$cohotyear = $_POST['cohot'];
	$ayear = $_POST['ayear'];*/
	$qprog= "SELECT * FROM programme WHERE ProgrammeCode='$prog'";
	$dbprog = mysqli_query($zalongwa, $qprog);
	$row_prog = mysqli_fetch_array($dbprog);
	$programme = $row_prog['Title'];
	$faculty = $row_prog['Faculty'];
	$nta = $row_prog['Ntalevel'];
	
	//calculate year of study
	$entry = intval(substr($cohotyear,0,4));
	$current = intval(substr($ayear,0,4));
	$yearofstudy=$current-$entry;
	
	
		$deg=$prog;//addslashes($_POST['degree']);
		/*$year = addslashes($_POST['ayear']);
		$cohot = addslashes($_POST['cohot']);
		$dept = addslashes($_POST['dept']);
		*/
	//$sem='Semester I';
	#force semvalue to be semester 1
	//$semval = 1;
	
	#calculate year of study
    require_once('../includes/calculateyearofstudy.php');
    
	#cohort number
	$yearofstudy = $yearofstudy +1;
	$totalcolms =0;
	
	#determine total number of columns
	
		$qcolmns = "SELECT DISTINCT CourseCode, Status FROM courseprogramme 
				WHERE  (ProgrammeID='$deg') AND (YearofStudy='$yearofstudy') AND (AYear='$ayear')
				ORDER BY Semester, CourseCode";
			

		
	$dbcolmns = mysqli_query($zalongwa, $qcolmns);
	$dbcolmnscredits = mysqli_query($zalongwa, $qcolmns);
	$dbexamcat = mysqli_query($zalongwa, $qcolmns);
	$totalcolms = mysqli_num_rows($dbcolmns);
	
 	$degree=$deg;
 	
 
 	
	#getting the number of credits to be studied	
	
		$mysql = mysqli_query($zalongwa, "SELECT * FROM coursecountprogramme WHERE ProgrammeID='$deg' AND YearofStudy='$yearofstudy' 
						AND AYear='$ayear'");
		
						
	$mysq = mysqli_fetch_array($mysql);
	$semx = $mysq['CourseCount']; 
		
	# Print Report header	
	# Print Report header	
	$rpttitle='SUPPLEMENTARY EXAMINATIONS RESULTS';
	$objPHPExcel->getActiveSheet()->mergeCells('A1:Z1');
	$objPHPExcel->getActiveSheet()->mergeCells('A2:Z2');
	$objPHPExcel->getActiveSheet()->mergeCells('A3:Z3');

	$objPHPExcel->getActiveSheet()->mergeCells('A5:Z5');


	$objPHPExcel->getActiveSheet()->mergeCells('A4:Z4');
	
		$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('A1', strtoupper($org))
->setCellValue('A2', strtoupper($faculty))
		    ->setCellValue('A3', strtoupper($rpttitle))
 ->setCellValue('A4', "NTA LEVEL :  " .strtoupper($nta) . "                                          FIELD OF STUDY  :  " . strtoupper($programme))
		    ->setCellValue('A5', "YEAR OF STUDY :  " . $class . "                     DATE :.....................................           WEIGHT : CA 40%            WEIGHT : SE60%");

	
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20); 
	$objPHPExcel->getActiveSheet()->getStyle('A2:A3')->getFont()->setSize(16); 
	$objPHPExcel->getActiveSheet()->getStyle('A2:A3')->getFont()->setSize(13);
	$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getStyle('A1:A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
	$colm = "A";
	$rows = 7;
	$rows1 = $rows +1;
	
	//$objPHPExcel->getActiveSheet()->getStyle('A6:BE6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		


	
	
	#Print Serial Number
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
				      ->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue($colm.$rows, '#');
		    
	$colm++; 
		    
	#Print Sex
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
				      ->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue($colm.$rows, 'Name'); 
	$colm++;
	
	#Print RegNo
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
				      ->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue($colm.$rows, 'RegNo'); 
	$colm++; 	

	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
				      ->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue($colm.$rows, 'Sex'); 
	$colm++; 
	
	

	$progcredits=0;
	$totalunits = 0; 
	$coursecount = 0;
	   
	#Print Course Headers
	while($rowcourseheader = mysqli_fetch_array($dbcolmnscredits)) 
	{ 
		#get course units
		$course= $rowcourseheader['CourseCode'] ;
		
		#get semester 
		$qsem= "SELECT DISTINCT Semester FROM examresult WHERE CourseCode='$course'";
		$dbsem = mysqli_query($zalongwa, $qsem);
		$row_sem = mysqli_fetch_array($dbsem);
		$sem=$row_sem['Semester'];
		if ($sem =='Semester I' || $sem =='Semester III' || $sem =='Semester V' || $sem =='Semester VII' || $sem =='Semester IX'){
			$semval = 1;
		}elseif ($sem=='Semester II' ||  $sem =='Semester IV' || $sem =='Semester VI' || $sem =='Semester VIII' || $sem =='Semester X'){
			$semval = 2;
		}
	   	#get course unit
		$qunits = "SELECT Units FROM course WHERE CourseCode='$course'";
		$dbunits = mysqli_query($zalongwa, $qunits);
		$row_units = mysqli_fetch_array($dbunits);
		$unit=$row_units['Units'];
		$progcredits = $progcredits+$unit;	 
	
		$colmc = $colm;
	
		
			$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colmc.$rows)
						      ->getStyle($colm.$rows.':'.$colmc.$rows)->applyFromArray($styleArray);		    
			#write down the course code.			     			      
			$objPHPExcel->setActiveSheetIndex(0)
				    ->setCellValue($colm.$rows, $course.' ('.$unit.')');  
			#rotate text to 90 degree
			$config_data[$course]=$colm.'-'.$rows;
			$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colmc.$rows)
						      ->getStyle($colm.$rows.':'.$colmc.$rows)->getAlignment()->setTextRotation(90);
			#set row height to 74 points
			$objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(74);
			
			$totalunits = $totalunits + $unit;		
		
		$rows++;
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm++.$rows, $semval);
			$objPHPExcel->getActiveSheet()->getStyle($colm--.$rows)->applyFromArray($styleArray);
	
		$rows = $rows -1;
		$sem='';
	}

	
	
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
					->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, 'POINTS');
	$ptscell=$colm.$rows.':'.$colm.$rows1;
	$colm++;
	
	
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
					->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, 'GPA');
	$ptscell=$colm.$rows.':'.$colm.$rows1;
	$colm++;
		
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
					->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, 'REMARK');
	$colm++;
	
	


		$qstudent = "SELECT s.Name, s.RegNo, s.Sex, s.DBirth, s.ProgrammeofStudy, s.Faculty, s.Sponsor, s.EntryYear, s.Status, s.Class 
				 FROM student as s, examresult as ex WHERE (s.ProgrammeofStudy = '$deg') AND (s.EntryYear = '$cohot') AND (s.RegNo=ex.RegNo) AND (ex.ExamCategory=7) AND (ex.AYear='$ayear') ORDER BY Name";
		
				
	$dbstudent = mysqli_query($zalongwa, $qstudent);
	$totalstudent = mysqli_num_rows($dbstudent);
	$z=1;
	$rows = 9;
	//echo $qstudent;exit;
	//echo $totalstudent;exit;
	

	#freez
	$objPHPExcel->getActiveSheet()->freezePane($colm.$rows);
		
	while ($rowstudent = mysqli_fetch_array($dbstudent)) 
	{
		$colms = "A";		
		$regno = $rowstudent['RegNo'];
		
		
			$name = stripslashes($rowstudent['Name']);			
			$sex = $rowstudent['Sex'];
			$bdate = $rowstudent['DBirth'];
			$comb = $rowstudent['Class'];
			$degree = stripslashes($rowstudent["ProgrammeofStudy"]);
			$faculty = stripslashes($rowstudent["Faculty"]);
			$sponsor = stripslashes($rowstudent["Sponsor"]);
			$entryyear = stripslashes($result['EntryYear']);
			$ststatus = stripslashes($rowstudent['Status']);
			
		
		
		#initialise
			$totalunit=0;
			$gmarks=0;
			$totalfailed=0;
			$totalinccount=0;
			$unittaken=0;
			$creditslost=0;
			$requiredcredits=0;
			$creditstaken=0;
			$optcredits=0;
			$extracreditstaken=0;
			$sgp=0;
			$totalsgp=0;
			$gpa=0;
			$pct=0;
			$chas=0;
			$failed = 0;
			
		# new values
			$totalfailed=0;
			$totalinccount=0;
			$halfsubjects=0;
		
			$ovremark='';
			$gmarks=0;
			$avg =0;
			$gmarks=0;	
			$decrement = 0;
			
			$key = $regno; 			
		
		# Print results
		
		#Print Serial Number
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $z);
		$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
		$colms++; 
		
		
		#Print Serial Number
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $name);
		$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
		$colms++; 
				
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $regno);
		$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
		$colms++;
		
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $sex);
		$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
		$colms++;
		
		
				
				
			$dbcourselist = mysqli_query($zalongwa, $qcolmns);
			
			while($row_courselist = mysqli_fetch_array($dbcourselist)) 
			{ 
				$course= $row_courselist['CourseCode'];
				$coption = $row_courselist['Status']; 
				$qcoursestd="SELECT Units, Department, StudyLevel FROM course WHERE CourseCode = '$course'";	
				$dbcoursestd = mysqli_query($zalongwa, $qcoursestd);
				$row_coursestd = mysqli_fetch_array($dbcoursestd);
				$unit = $row_coursestd['Units'];					
				$remarks = 'remarks';
				$RegNo = $key;
				$currentyear=$year;
				include 'choose_studylevel.php';
				
				
				# find course's examninationregulation
				$qexamreg = "SELECT StudyLevel FROM course  WHERE  CourseCode = '$course'";
				$dbexamreg = mysqli_query($zalongwa, $qexamreg);
				$row_result_sheet = mysqli_fetch_array($dbexamreg);
				$studylevel= $row_result_sheet['StudyLevel'];				
				
							#shade failed grades
				if($remark<>'PASS'){
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					//$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->getStartColor()->setARGB('FFFF0000');
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->getStartColor()->setARGB('CCCCCC');
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->applyFromArray($styleArray);
					
					#mark candidate for supp
					if($fsup=='!'){
						$supp=1;
						}
					
					$decrement =$decrement + $unit;
					
					if($unit > 0){
						$failed = $failed + 1;
						}					
				}
				else{
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->applyFromArray($styleArray);
					}
					
				$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms++.$rows, $grade);				
				
								
			}
			
		
			# Assign total credits
			$requiredcredits = $progcredits-$optcredits-$extracreditstaken;

			
			
			
			$curr_semester=$semval;
			//include 'compute_overall_remark.php';
 
 			$creditstaken=$requiredcredits-$creditslost;
			
	$objPHPExcel->getActiveSheet()->getColumnDimension($colms)->setAutoSize(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, number_format($totalsgp,2));
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
			$colms++;
			
			
					
			# compute gpa
			$gpa= substr($totalsgp/$unittaken,0,3);
			if($gpa == 1){
				$gpa = '1.0';
				}
			elseif($gpa == 2){
				$gpa = '2.0';
				}
			elseif($gpa == 3){
				$gpa = '3.0';
				}
			elseif($gpa == 4){
				$gpa = '4.0';
				}
			elseif($gpa == 5){
				$gpa = '5.0';
				}
				
			#compute percentage points
			$creditsacquired=$creditstaken-$creditslost;
			
				//$objPHPExcel->getActiveSheet()->getColumnDimension($colms)->setWidth(4);
	$objPHPExcel->getActiveSheet()->getColumnDimension($colms)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getCell($colms.$rows)->setValueExplicit($gpa, PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
			$colms++;
			
			if($gpa<1.8){
				$remark = "DISCO";
				}
			elseif($supp==1){
				$remark = "SUPP";
				}
			else{
				$remark = "PASS";
				}
			//$objPHPExcel->getActiveSheet()->getColumnDimension($colms)->setWidth(5);
	$objPHPExcel->getActiveSheet()->getColumnDimension($colms)->setAutoSize(true);
			if($remark == 'DISCO'){
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->getStartColor()->setARGB('FFFF0000');					
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->applyFromArray($styleArray);					
				}
				
			elseif($remark == 'PASS'){
				#if pass then clean sheet					
				}
			elseif($remark == 'ABSC'){
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);					
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->getStartColor()->setARGB('CCCCCC');
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->applyFromArray($styleArray);					
				}
							
			//$objPHPExcel->setActiveSheetIndex(1)->setCellValue($colms.$rows, $gpa);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, ' '.' '.$remark.' '.' ');
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);			
			# prints overall remarks
			$colms++;
			
			
			// get end of the course column
			$end_subject = "E";
			for ($x =0; $x < $totalcolms; $x++){
				$end_subject++;
			}
		
			
				
		$rows++;
		$z=$z+1;
		$counter = $rows;
		$supp='';
		$fsup='';
		}
 	
 	
 	$colms = 'C';
 	$counter = $counter + 2;
 	$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->applyFromArray($styleArray);
 	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$counter, ' KEY FOR THE COURSE CODES  ');
	//$colms++;
	
	#get the course details
	
		$qcolmn = mysqli_query($zalongwa, "SELECT DISTINCT cp.CourseCode, c.CourseName FROM courseprogramme cp, course c 
							WHERE (cp.ProgrammeID='$prog') AND (cp.YearofStudy='$yearofstudy') AND (AYear='$ayear')
							AND (cp.CourseCode=c.CourseCode) ORDER BY cp.CourseCode");
		
		
	$y = $colms;
	for($x=1; $x<=15; $x++){		
		$y++;			
		$colms2 = $y;
		}
	$colms1 = $colms;
	$colms1++;
	
	while($course = mysqli_fetch_array($qcolmn)){
		
		#print coursecode
		$counter += 1;
		$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$counter, ' '.$course['CourseCode']);				
		//$counter2 = $counter + 1;
		#print course title
		$objPHPExcel->getActiveSheet()->mergeCells($colms1.$counter.':'.$colms2.$counter)
					->getStyle($colms1.$counter.':'.$colms2.$counter)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms1.$counter, ' '.$course['CourseName']);
		//$colms++;	

	}

/*
if(isset($_POST['st_report'])){
//======================================================================================================================
// initialize data to be used in the second worksheet for statistical
if(count($config_data) > 0){

     mysql_data_seek($dbcolmns,0);
     $gender_colm ="D";
       $Am1 = 0; $B1m1 = 0; $B2m1 = 0; $Cm1=0; $Dm1 = 0; $Em1 = 0; $Im1 = 0; // track the total grades for all course.. male
	   $Af2 = 0; $B1f2 = 0; $B2f2 = 0; $Cf2=0; $Df2 = 0; $Ef2 = 0; $If2 = 0; // track total grades for all courses.. female
	 
	 foreach ($config_data as $key=>$value){
	 	$ex= explode("-",$value); // explode to get coordinate of the cell where coursecode is located in sheet 1
	   $ex[1] = $ex[1]+2;
	   $colm = $ex[0];
	   
	   $rows = $ex[1];
	   $A = 0; $B1 = 0; $B2 = 0; $C=0; $D = 0; $E = 0; $I = 0; // track total number of grades ignore gender
	   $Am = 0; $B1m = 0; $B2m = 0; $Cm=0; $Dm = 0; $Em = 0; $Im = 0; // track number of grades for male
	   $Af = 0; $B1f = 0; $B2f = 0; $Cf=0; $Df = 0; $Ef = 0; $If = 0; // track number of grades for female
	   
	   for ($j = 0; $j < $totalstudent; $j++) { // how many rows we have based on the number of student
	   	
	   	// hold data for male
	   	if($objPHPExcel->getActiveSheet()->getCell($gender_colm.$rows)->getValue() == "M"){ // only grades for male
	   	if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() == "A"){
	   		$Am++;  $Am1++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="B+"){
	   		$B1m++; $B1m1++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="B"){
	   		$B2m++; $B2m1++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="C"){
	   		$Cm++; $Cm1++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="D"){
	   		$Dm++; $Dm1++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="F"){
	   		$Em++; $Em1++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="I"){
	   		$Im++; $Im1++;
	   	}
	   	}
	   		
	   	// hold data for female
	   	if( $objPHPExcel->getActiveSheet()->getCell($gender_colm.$rows)->getValue() == "F" ){ //only grades for female
	   	if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() == "A"){
	   		$Af++; $Af2++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="B+"){
	   		$B1f++; $B1f2++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="B"){
	   		$B2f++; $B2f2++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="C"){
	   		$Cf++; $Cf2++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="D"){
	   		$Df++; $Df2++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="F"){
	   		$Ef++;$Ef2++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="I"){
	   		$If++;$If2++;
	   	}
	   	}
	   	
	   	// hold general data
	   	if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() == "A"){ // all grades ignore gender
	   		$A++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="B+"){
	   		$B1++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="B"){
	   		$B2++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="C"){
	   		$C++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="D"){
	   		$D++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="F"){
	   		$E++;
	   	}else if($objPHPExcel->getActiveSheet()->getCell($colm.$rows)->getValue() =="I"){
	   		$I++;
	   	}
	   	$rows++;
	   	
	   }
	   
	   // put grades ignore gender in this array
	   $get_statistical_data[$key]=array(
	   'A'=>$A,'B+'=>$B1,'B'=>$B2,'C'=>$C,'D'=>$D,'F'=>$E,'I'=>$I
	   );
	   // only grades for male
	   $get_statistical_data_gender[$key]['M']=array(
	   'A'=>$Am,'B+'=>$B1m,'B'=>$B2m,'C'=>$Cm,'D'=>$Dm,'F'=>$Em,'I'=>$Im
	   );
	   //only grades for female
	    $get_statistical_data_gender[$key]['F']=array(
	   'A'=>$Af,'B+'=>$B1f,'B'=>$B2f,'C'=>$Cf,'D'=>$Df,'F'=>$Ef,'I'=>$If
	   );
	    
	 }
	 //hold total grades in  all courses for male
	  $get_statistical_total_grades['M']=array(
	   'A'=>$Am1,'B+'=>$B1m1,'B'=>$B2m1,'C'=>$Cm1,'D'=>$Dm1,'F'=>$Em1,'I'=>$Im1
	   );
	   
	   // hold total grades in all courses for female
	   $get_statistical_total_grades['F']=array(
	   'A'=>$Af2,'B+'=>$B1f2,'B'=>$B2f2,'C'=>$Cf2,'D'=>$Df2,'F'=>$Ef2,'I'=>$If2
	   );
	  
	 
	 
	 

     //Open the second sheet (ststistic sheet)

     $objPHPExcel->createSheet();
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(1);
	
	// Rename sheet
	//$objPHPExcel->getActiveSheet()->setTitle('Semester I');
	  $objPHPExcel->getActiveSheet()->setTitle('End of Year Results Statistic');
	  
	  
// Set properties
$objPHPExcel->getProperties()->setCreator("Zalongwa")
							 ->setLastModifiedBy("Juma Lungo")
							 ->setTitle($programme)
							 ->setSubject("Semester Exam Results")
							 ->setDescription("Semester Exam Results.")
							 ->setKeywords("zalongwa saris software")
							 ->setCategory("Exam result file");
	
	$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);
	
	# Set protected sheets to 'true' kama hutaki waandike waziedit sheets zako. Kama unataka wazi-edit weka 'false'
	$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
	
	#set worksheet orientation and size
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize($papersize);
	
	#Set page fit width to true
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
	
	#Set footer page numbers
	$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
	#Show or hide grid lines
	$objPHPExcel->getActiveSheet()->setShowGridlines(true);
	
	#Set sheet style (fonts and font size)
	$objPHPExcel->getDefaultStyle()->getFont()->setName($fontstyle);
	$objPHPExcel->getDefaultStyle()->getFont()->setSize($font); 
	
	#Set page margins
	$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(1);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(1);
	
for($col=A;$col<ZZ;$col++) { 
	  if($col != "K"){
		$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
	}
}
	$objPHPExcel->getActiveSheet()->getColumnDimension("K")->setWidth(70);
	
	$coll ="A";
	for($col=1;$col<100;$col++) { 
		$objPHPExcel->getActiveSheet()->getColumnDimension($coll)->setWidth(10);
	$coll++;
	}
	
	// style used in formating border of the cell
	$default_border = array(
	         'style' => PHPExcel_Style_Border::BORDER_THIN,
	          'color' => array('rgb'=>'1006A3') );
	
	$set_borders = array(
	 'borders' => array( 
	               'bottom' => $default_border, 
	               'left' => $default_border,
	               'top' => $default_border,
	               'right' => $default_border, ),
	         
	);
	
	$style_header = array(
	       'borders' => array( 
	               'bottom' => $default_border, 
	               'left' => $default_border,
	               'top' => $default_border,
	               'right' => $default_border, ), 
	         
	       'fill' => array( 
	             'type' => PHPExcel_Style_Fill::FILL_SOLID, 
	             'color' => array(
	                    'rgb'=>'E1E0F7'),
	                   ),
	         'font' => array( 
	               'bold' => true, ) );
	                   
	
	#Set Rows to repeate in each page
	$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 5);
	$colm="A";
	$colm2 = "B";
	
//======================================================================================================
	// print title
	$objPHPExcel->getActiveSheet()->setCellValue("B2","ANNUAL EXAMINATION RESULTS: ".'   '. $year);
	$objPHPExcel->getActiveSheet()->mergeCells('B2:J2');
	
	$objPHPExcel->getActiveSheet()->setCellValue("B4",$class.' - '.strtoupper($programme));
	$objPHPExcel->getActiveSheet()->mergeCells('B4:J4');
	
	$objPHPExcel->getActiveSheet()->setCellValue("B6","STATISTICAL SUMMARY OF EXAMINATION RESULTS");
	$objPHPExcel->getActiveSheet()->mergeCells('B6:J6');
	
	
	$objPHPExcel->getActiveSheet()->setCellValue("N2","ANNUAL EXAMINATION RESULTS: ".'   '. $year);
	$objPHPExcel->getActiveSheet()->mergeCells('N2:V2');
	
	$objPHPExcel->getActiveSheet()->setCellValue("N4",$class.' - '.strtoupper($programme));
	$objPHPExcel->getActiveSheet()->mergeCells('N4:V4');
	
	$objPHPExcel->getActiveSheet()->setCellValue("N6","DISTRIBUTION OF GRADES OF EXAMINATION RESULTS");
	$objPHPExcel->getActiveSheet()->mergeCells('N6:V6');
	//===============================================================================================================================
	
	$rows=9;
	
	//=====================================================================================================================================
    //Start print the general data, Grades	
    
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'CODE');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'A');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'B+');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'B');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'C');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'D');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'F');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'I');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'SUM');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$rows++;
	
	$row_before_loop = $rows;
	
	$get_coordinate_sum = array(); // hold coordinate of the total Grades per course
	
	foreach ($get_statistical_data as $k=>$v){
		$colm2="B";
		$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,$k);
		$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
		$colm2++;
		foreach ($v as $grd=> $total){
			$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,$total);
			$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
			$colm2++;
		}
		$end_col=$colm2;
		--$end_col;
		
		$get_coordinate_sum[$k]=$colm2.$rows;
		$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'=SUM(B'.$rows.':I'.$rows.')');
		$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
		$rows++;
	}
	$colm2="B";
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'SUM');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
	$colm2++;
	$r=$rows;
	$row_end= --$r;
	
		    $objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'=SUM('.$colm2.$row_before_loop.':'.$colm2.$row_end.')');
			$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
		    $colm2++;
			$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'=SUM('.$colm2.$row_before_loop.':'.$colm2.$row_end.')');
			$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
			$colm2++;
			$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'=SUM('.$colm2.$row_before_loop.':'.$colm2.$row_end.')');
			$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
			$colm2++;
			$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'=SUM('.$colm2.$row_before_loop.':'.$colm2.$row_end.')');
			$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
			$colm2++;
			$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'=SUM('.$colm2.$row_before_loop.':'.$colm2.$row_end.')');
			$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
			$colm2++;
			$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'=SUM('.$colm2.$row_before_loop.':'.$colm2.$row_end.')');
			$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
			$colm2++;
			$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'=SUM('.$colm2.$row_before_loop.':'.$colm2.$row_end.')');
			$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
			$colm2++;
			$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'=SUM('.$colm2.$row_before_loop.':'.$colm2.$row_end.')');
			$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
			$colm2++;

			// end of printing general data
			
			//=========================================================================================================================
			
			
			
			
			
//======================================================================================================================
			
			// start printing statistical data for the specific course information in gender wise
			
	$colm = "L";
	$rows = 9;
    
	$p = 1;
	foreach ($get_statistical_data_gender as $key=>$value){
	
		
		$objPHPExcel->getActiveSheet()->setCellValue($colm.$rows,$key);
		$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($style_header);
		$colm2  =$colm;
		$colm++;
		$objPHPExcel->getActiveSheet()->setCellValue($colm.$rows,'F');
		$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($style_header);
		$colm++;
		$objPHPExcel->getActiveSheet()->setCellValue($colm.$rows,'M');
		$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($style_header);
		$colm++;
		$objPHPExcel->getActiveSheet()->setCellValue($colm.$rows,'SUM');
		$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($style_header);
		$colm++;
		$colm++;
		$p++;
	    $rpr=$rows;
		foreach ($value['M'] as $k=>$v){
		$rows++;
		$colm1= $colm2;
		$objPHPExcel->getActiveSheet()->setCellValue($colm1.$rows,$k);
		$objPHPExcel->getActiveSheet()->getStyle($colm1.$rows)->applyFromArray($set_borders);
		$colm1++;
		$objPHPExcel->getActiveSheet()->setCellValue($colm1.$rows,$value['M'][$k]);
		$objPHPExcel->getActiveSheet()->getStyle($colm1.$rows)->applyFromArray($set_borders);
		$c1 = $colm1.$rows;
		$colm1++;
		$objPHPExcel->getActiveSheet()->setCellValue($colm1.$rows,$value['F'][$k]);
		$objPHPExcel->getActiveSheet()->getStyle($colm1.$rows)->applyFromArray($set_borders);	
		$c2=$colm1.$rows;
		$colm1++;
		$objPHPExcel->getActiveSheet()->setCellValue($colm1.$rows,'=SUM('.$c1.':'.$c2.')');
		$objPHPExcel->getActiveSheet()->getStyle($colm1.$rows)->applyFromArray($set_borders);	
		$colm1++;
		
		}
		$end_r = $rows;
		$rows++;
		
		$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'SUM');
		$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
		$colm2++;
		$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'=SUM('.$colm2.$rpr.':'.$colm2.$end_r.')');
		$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
		$colm2++;
		$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'=SUM('.$colm2.$rpr.':'.$colm2.$end_r.')');
		$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
		$colm2++;
		$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'=SUM('.$colm2.$rpr.':'.$colm2.$end_r.')');
		$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
		$colm2++;
		
		$rows = $rpr;
		
		if($p > 5){
			$colm = "L";
			
			for ($j2 = 0; $j2 < 11; $j2++) {
				$rows++;
			}
			$p=1;
		}
	}
//=====================================================================================================================
	
	//==================================================================================================================
// print total summary for all grades

  $co = "N";
  $rows = $end_r+5;
  $objPHPExcel->getActiveSheet()->setCellValue($co.$rows,'Total');
  $objPHPExcel->getActiveSheet()->getStyle($co.$rows)->applyFromArray($style_header);
  $co++;

  $objPHPExcel->getActiveSheet()->setCellValue($co.$rows,'F');
  $objPHPExcel->getActiveSheet()->getStyle($co.$rows)->applyFromArray($style_header);
  $co++;

  $objPHPExcel->getActiveSheet()->setCellValue($co.$rows,'M');
  $objPHPExcel->getActiveSheet()->getStyle($co.$rows)->applyFromArray($style_header);
  $co++;
  
  $objPHPExcel->getActiveSheet()->setCellValue($co.$rows,'SUM');
  $objPHPExcel->getActiveSheet()->getStyle($co.$rows)->applyFromArray($style_header);
  $co++;
  $pp="N";
  $rows++;
  $sstart_row=$rows;
	foreach ($get_statistical_total_grades['M'] as $xx=>$vv){
		$co=$pp;
		$objPHPExcel->getActiveSheet()->setCellValue($co.$rows,$xx);
		$objPHPExcel->getActiveSheet()->getStyle($co.$rows)->applyFromArray($set_borders);
		$co++;
		$objPHPExcel->getActiveSheet()->setCellValue($co.$rows,$get_statistical_total_grades['F'][$xx]);
		$objPHPExcel->getActiveSheet()->getStyle($co.$rows)->applyFromArray($set_borders);
		$cc= $co.$rows;
		$co++;
		$objPHPExcel->getActiveSheet()->setCellValue($co.$rows,$get_statistical_total_grades['M'][$xx]);
		$objPHPExcel->getActiveSheet()->getStyle($co.$rows)->applyFromArray($set_borders);
		$ll=$co.$rows;
		$co++;
		$objPHPExcel->getActiveSheet()->setCellValue($co.$rows,'=SUM('.$cc.':'.$ll.')');
		$objPHPExcel->getActiveSheet()->getStyle($co.$rows)->applyFromArray($set_borders);
		$co++;
		$rows++;
	}
	$co = $pp;
	$end_row=$rows;
	--$end_row;
	
	$objPHPExcel->getActiveSheet()->setCellValue($co.$rows,'SUM');
	$objPHPExcel->getActiveSheet()->getStyle($co.$rows)->applyFromArray($set_borders);
	$co++;
	$objPHPExcel->getActiveSheet()->setCellValue($co.$rows,'=SUM('.$co.$sstart_row.':'.$co.$end_row.')');
	$objPHPExcel->getActiveSheet()->getStyle($co.$rows)->applyFromArray($set_borders);
	$co++;
	$objPHPExcel->getActiveSheet()->setCellValue($co.$rows,'=SUM('.$co.$sstart_row.':'.$co.$end_row.')');
	$objPHPExcel->getActiveSheet()->getStyle($co.$rows)->applyFromArray($set_borders);
	$co++;
	$objPHPExcel->getActiveSheet()->setCellValue($co.$rows,'=SUM('.$co.$sstart_row.':'.$co.$end_row.')');
	$objPHPExcel->getActiveSheet()->getStyle($co.$rows)->applyFromArray($set_borders);
	$co++;
	
	//==================================================================================================================
	
  //====================================================================================================================
	// insert bar chart here
	$rows2=0;
	for ($k2 = ($row_end+4); $k2 < ($row_end+20); $k2++) {
		$objPHPExcel->getActiveSheet()->mergeCells('B'.$k2.':I'.$k2);
		$row2=$k2;
	}
	
	$yy =$row_end+4;
	
	require_once 'create_bar_chart_result_annual.php';
	
	 $data_for_male = array_values($get_statistical_total_grades['M']);
	 $data_for_female = array_values($get_statistical_total_grades['F']);
	 
	 $xdata = array_keys($get_statistical_total_grades['F']);
	 // if x label is character need to have single quote 'A'
	/* $xdata_temp =array();
	 foreach ($xdata as $k1 =>$v1){
	 	$xdata_temp[]= "'".$v1."'";
	 }
	 $xdata = $xdata_temp;
	 */
	
	 //exit;
/*     $bar = new Create_bar();
     
	 $bar->create_image($data_for_male,$data_for_female, $xdata);
	
	
	$objDrawing = new PHPExcel_Worksheet_Drawing();
	$objDrawing->setName('Logo');
	$objDrawing->setDescription('Logo');
	$objDrawing->setPath('images/graph/bar.png');
	//$objDrawing->setHeight(200);
	
	$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
	
	$objDrawing->setName('Logo');
	$objDrawing->setDescription('Logo');
	$objDrawing->setPath('images/graph/bar.png');
	$objDrawing->setCoordinates('B'.$yy);
	$objDrawing->setOffsetX(110);
	$objDrawing->setRotation(25);
	$objDrawing->getShadow()->setVisible(true);
	$objDrawing->getShadow()->setDirection(45);
	
	//====================================================================================================================
	
	
	//====================================================================================================================
	// Percentage distribution of the grades
	
	$objPHPExcel->getActiveSheet()->setCellValue("D".($row2+3),"PERCENTAGE DISTRIBUTION OF EXAMINATION GRADES ");
	$objPHPExcel->getActiveSheet()->mergeCells('D'.($row2+3).':I'.($row2+3));
	
	$row2 = $row2 +5;
	$rows = $row2;
	$colm2 = "B";
	
	//Start print the general data	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'CODE');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'A');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'B+');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'B');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'C');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'D');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'F');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'I');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'SUM');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	
	$rows++;
	foreach ($get_statistical_data as $k=>$v){
		$colm2="B";
		$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,$k);
		$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
		$colm2++;
		foreach ($v as $grd=> $total){
			$cell_value = $objPHPExcel->getActiveSheet()->getCell($get_coordinate_sum[$k])->getFormattedValue();
		    $cell_value= number_format(($total/$cell_value)*100,1);
		    
			$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,$cell_value);
			$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
			$colm2++;
		}
		$end_col=$colm2;
		--$end_col;
		
		$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'=SUM(B'.$rows.':I'.$rows.')');
		$getp = $objPHPExcel->getActiveSheet()->getCell($colm2.$rows)->getFormattedValue();
		$sum = round($getp);
		$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,$sum);
		$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
		$rows++;
	}
	
	
	//==============================================================================================================
	
	
	//
	//
	
	$rows= $rows+5;
	$colm2="B";
	
	
	
	
	//================================================================================================================
	//Summary of grades gender wise to all courses
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'CODE');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$cc=$colm2;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'A');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$objPHPExcel->getActiveSheet()->mergeCells($colm2.$rows.':'.(++$colm2).$rows);
	$rows++;
	$colm2=$cc;
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'F');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$objPHPExcel->getActiveSheet()-> setCellValue($colm2.$rows,'M');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$cc=$colm2;
	$rows--;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'B+');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$objPHPExcel->getActiveSheet()->mergeCells($colm2.$rows.':'.(++$colm2).$rows);
	$rows++;
	$colm2=$cc;
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'F');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$objPHPExcel->getActiveSheet()-> setCellValue($colm2.$rows,'M');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$cc=$colm2;
	$rows--;
	
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'B');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$objPHPExcel->getActiveSheet()->mergeCells($colm2.$rows.':'.(++$colm2).$rows);
	$rows++;
	$colm2=$cc;
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'F');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$objPHPExcel->getActiveSheet()-> setCellValue($colm2.$rows,'M');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$cc=$colm2;
	$rows--;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'C');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$objPHPExcel->getActiveSheet()->mergeCells($colm2.$rows.':'.(++$colm2).$rows);
	$rows++;
	$colm2=$cc;
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'F');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$objPHPExcel->getActiveSheet()-> setCellValue($colm2.$rows,'M');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$cc=$colm2;
	$rows--;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'D');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$objPHPExcel->getActiveSheet()->mergeCells($colm2.$rows.':'.(++$colm2).$rows);
	$rows++;
	$colm2=$cc;
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'F');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$objPHPExcel->getActiveSheet()-> setCellValue($colm2.$rows,'M');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$cc=$colm2;
	$rows--;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'E');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$objPHPExcel->getActiveSheet()->mergeCells($colm2.$rows.':'.(++$colm2).$rows);
	$rows++;
	$colm2=$cc;
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'F');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$objPHPExcel->getActiveSheet()-> setCellValue($colm2.$rows,'M');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$cc=$colm2;
	$rows--;
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'I');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$objPHPExcel->getActiveSheet()->mergeCells($colm2.$rows.':'.(++$colm2).$rows);
	$rows++;
	$colm2=$cc;
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'F');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$objPHPExcel->getActiveSheet()-> setCellValue($colm2.$rows,'M');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$cc=$colm2;
	$rows--;
	
	
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'SUM');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$objPHPExcel->getActiveSheet()->mergeCells($colm2.$rows.':'.(++$colm2).$rows);
	$rows++;
	$colm2=$cc;
	$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,'F');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$objPHPExcel->getActiveSheet()-> setCellValue($colm2.$rows,'M');
	$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($style_header);
	$colm2++;
	$cc=$colm2;
	$rows--;
	
	
	
	$rows++;
	$rows++;
	$b4_loop = $rows;
	foreach ($get_statistical_data as $k=>$v){
		$colm2="B";
		$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,$k);
		$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
		$colm2++;
		foreach ($v as $grd=> $total){
			
			$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,$get_statistical_data_gender[$k]['F'][$grd]);
			$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
			$colm2++;
			$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,$get_statistical_data_gender[$k]['M'][$grd]);
			$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
			$colm2++;
			
		}
		$end_col=$colm2;
		--$end_col;
		
		$tF =0;
		$tM =0;
		$start ="C";
		
		for ($l = 1; $l < 15; $l++) {
			if($l % 2 == 0){
			$tM += $objPHPExcel->getActiveSheet()->getCell($start.$rows)->getFormattedValue();	
			}
			if($l % 2 != 0){
				$tF += $objPHPExcel->getActiveSheet()->getCell($start.$rows)->getFormattedValue();
			}
			$start++;
		}
		$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,$tF);
		$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
		$colm2++;
		$objPHPExcel->getActiveSheet()->setCellValue($colm2.$rows,$tM);
		$objPHPExcel->getActiveSheet()->getStyle($colm2.$rows)->applyFromArray($set_borders);
		$rows++;
	}
	$start ="B";
	$objPHPExcel->getActiveSheet()->setCellValue($start.$rows,'Total');
	$objPHPExcel->getActiveSheet()->getStyle($start.$rows)->applyFromArray($set_borders);
	$colm2++;
	$start++;
	$end_r = $rows;
	$end_r--;
	for ($l = 1; $l < 17; $l++) {
	$objPHPExcel->getActiveSheet()->setCellValue($start.$rows,'=SUM('.$start.$b4_loop.':'.$start.$end_r.')');
	$objPHPExcel->getActiveSheet()->getStyle($start.$rows)->applyFromArray($set_borders);	
		$start++;
	}
	
	
	//================================================================================================================================
	
	
	//================================================================================================================================
	// course key
	
	$colms = 'C';
 	$counter = $rows + 6;
 	$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->applyFromArray($style_header);
 	$objPHPExcel->getActiveSheet()->setCellValue($colms.$counter, ' KEY FOR THE COURSE CODES  ');
 	$objPHPExcel->getActiveSheet()->mergeCells($colms.$counter.':M'.$counter);
	//$colms++;
	
	#get the course details
	if ($checkcombin=='on'){
		$qcolmn = mysqli_query($zalongwa, "SELECT DISTINCT cp.CourseCode, c.CourseName FROM courseprogramme cp, course c 
							WHERE (cp.ProgrammeID='$prog') AND (cp.Combination = '$combin') AND (cp.YearofStudy='$yearofstudy')
							AND (AYear='$ayear') AND (cp.CourseCode=c.CourseCode) ORDER BY cp.CourseCode");
		}
	else{
		$qcolmn = mysqli_query($zalongwa, "SELECT DISTINCT cp.CourseCode, c.CourseName FROM courseprogramme cp, course c 
							WHERE (cp.ProgrammeID='$prog') AND (cp.YearofStudy='$yearofstudy') AND (AYear='$ayear')
							AND (cp.CourseCode=c.CourseCode) ORDER BY cp.CourseCode");
		}
		
	$y = $colms;
	for($x=1; $x<=10; $x++){		
		$y++;			
		$colms2 = $y;
		}
	$colms1 = $colms;
	$colms1++;
	
	while($course = mysqli_fetch_array($qcolmn)){
		
		#print coursecode
		$counter += 1;
		
		$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->applyFromArray($set_borders);
		$objPHPExcel->getActiveSheet()->setCellValue($colms.$counter, ' '.$course['CourseCode']);
		//$counter2 = $counter + 1;
		#print course title
		
		$objPHPExcel->getActiveSheet()->mergeCells($colms1.$counter.':'.$colms2.$counter)
					->getStyle($colms1.$counter.':'.$colms2.$counter)->applyFromArray($set_borders);
		$objPHPExcel->getActiveSheet()->setCellValue($colms1.$counter, ' '.$course['CourseName']);
		//$colms++;	
	}
	
}
//========================================================================================================================

}
*/
$objPHPExcel->setActiveSheetIndex(0);









	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="SARIS_Exam_Results.xls"');
	header('Cache-Control: max-age=0');
	//header("Content-Transfer-Encoding: binary ");
	#make pdf
	/*
	header('Content-Type: application/pdf');
	header('Content-Disposition: attachment;filename="SARIS_Exam_Results.pdf"');
	header('Cache-Control: max-age=0');
	*/
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	
	#make pdf
	// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
	 
$objWriter->save('php://output');
exit;
