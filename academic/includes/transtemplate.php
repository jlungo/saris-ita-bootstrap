
<?php
#signatory
$signatory = '                              Date                                                                REGISTRAR';
$pdf->image('../academic/images/logo.jpg', 50, 30);
$pdf->setFont('Arial', 'I', 8);
$pdf->text(530.28, 820.89, 'Page '.$pg);
$pdf->setFont('Arial', 'B', 8);
$pdf->text(51, 96, strtoupper("Tanzania revenue authority"));
$pdf->setFont('Arial', 'B', 18);
include('../includes/orgname.php');
$pdf->text(150, 125, strtoupper($org));
$pdf->setFont('Arial', 'I', 8);
$pdf->text(50, 820.89, 'Dar es Salaam, '.$today = date("d-m-Y H:i:s"));
$pdf->text(300, 820.89, $copycount);

$yadd=162;
$my_x = 89;
#University Addresses
$post = 'P.o. Box 9321 |';
$pdf->setFont('Arial', '', 11.3);
$pdf->text($my_x - 39, $yadd, $post);
$pdf->text($my_x + 45, $yadd, 'Tel: 022-2780160 | ');
$pdf->text($my_x + 145 , $yadd, 'Fax: 022-2780161 | ');
$pdf->text($my_x + 245, $yadd, 'Email: ita@tra.go.tz | ');
$pdf->text($my_x + 350, $yadd, 'Dar es Salaam, Tanzania');
$yadd=143;

#candidate photo box
$pdf->line(490, 39, 490, 119);       // leftside.
$pdf->line(490, 39, 570, 39);        // upperside.
$pdf->line(570, 39, 570, 119);       // rightside.
$pdf->line(490, 119, 570, 119);       // bottom side.
if ($nophoto == 1){
    //$pdf->image('images/default.jpg', 490, 58);
}else{
//
    if($cat != '1'){
        $pdf->image($imgfile, 488, 35);

    }

}
?>
