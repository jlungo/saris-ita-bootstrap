<?php
function formatgpa($gpa){
if ($gpa !=''){
$explo = explode('.',$gpa);
$dec = '';

if(array_key_exists(1,$explo)){
	
	if(strlen($explo[1]) >= 3 ){
		$dec=$explo[1];
	}else if(strlen($explo[1]) == 2 ){
	 $dec = $explo[1].'0';
	}else if(strlen($explo[1]) == 1 ){
	 $dec = $explo[1].'00';
	}else if(strlen($explo[1]) == 0 ){
	 $dec = $explo[1].'000';
	}
	
}else{
	
	 $dec ='000';
		
}



return $explo[0].'.'.$dec;

}
return $gpa;
}
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */

/** Error reporting */
error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

/** PHPExcel */
require_once '../Classes/PHPExcel.php';

$papersize='PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4';
$fontstyle='Arial';
$font=10.5;
$prog=$_POST['degree'];
$qprog= "SELECT ProgrammeCode, Title, Faculty FROM programme WHERE ProgrammeCode='$prog'";
$dbprog = mysql_query($qprog);
$row_prog = mysql_fetch_array($dbprog);
$programme = $row_prog['Title'];

// hold statistical data
$get_statistical_data =array(); // hold general 
$config_data =array(); // hold coursecode and its cell coordinate in worksheet one
$get_statistical_data_gender =array(); // hold statistical data by gender

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();


	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	
	// Rename sheet
	//$objPHPExcel->getActiveSheet()->setTitle('Semester I');
	$objPHPExcel->getActiveSheet()->setTitle('End of Year Results');


// Set properties
$objPHPExcel->getProperties()->setCreator("Zalongwa")
							 ->setLastModifiedBy("Juma Lungo")
							 ->setTitle($programme)
							 ->setSubject("Semester Exam Results")
							 ->setDescription("Semester Exam Results.")
							 ->setKeywords("zalongwa saris software")
							 ->setCategory("Exam result file");
	
	$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);
	
	# Set protected sheets to 'true' kama hutaki waandike waziedit sheets zako. Kama unataka wazi-edit weka 'false'
	$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
	
	#set worksheet orientation and size
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize($papersize);
	
	#Set page fit width to true
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
	
	#Set footer page numbers
	$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
	#Show or hide grid lines
	$objPHPExcel->getActiveSheet()->setShowGridlines(false);
	
	#Set sheet style (fonts and font size)
	$objPHPExcel->getDefaultStyle()->getFont()->setName($fontstyle);
	$objPHPExcel->getDefaultStyle()->getFont()->setSize($font); 
	
	#Set page margins
	$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(1);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(1);
	
	# Set Rows to repeate in each page
	$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 5);
	
	# Set Report Logo

	
	$styleArray = array(
		'borders' => array(
			'outline' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			),
		),
	);


	

	for($col='A';$col<'ZZ';$col++) { 
		$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
	}
	#Get Organisation Name
	$qorg = "SELECT * FROM organisation";
	$dborg = mysql_query($qorg);
	$row_org = mysql_fetch_assoc($dborg);
	$org = $row_org['Name'];
	
	$prog=$_POST['degree'];
	$cohotyear = $_POST['cohot'];
	$ayear = $_POST['ayear'];
	$nameofcurrentyear = $ayear;
	$qprog= "SELECT * FROM programme WHERE ProgrammeCode='$prog'";
	$dbprog = mysql_query($qprog);
	$row_prog = mysql_fetch_array($dbprog);
	$programme = $row_prog['Title'];
	$faculty = $row_prog['Faculty'];
	$nta = $row_prog['Ntalevel'];
	
	//calculate year of study
	$entry = intval(substr($cohotyear,0,4));
	$current = intval(substr($ayear,0,4));
	$yearofstudy=$current-$entry;
	
		$deg=addslashes($_POST['degree']);
		$year = addslashes($_POST['ayear']);
		$cohot = addslashes($_POST['cohot']);
		
		 $sem = addslashes($_POST['sem']);
		if ($sem =='Semester I' || $sem =='Semester III' || $sem =='Semester V' || $sem =='Semester VII' || $sem =='Semester IX'){
			$semval = 1;
		}elseif ($sem=='Semester II' || $sem=='Semester IV' || $sem=='Semester VI' || $sem=='Semester VIII' || $sem=='Semester X'){
			$semval = 2;
		}
$seme_ster= $semval;
	
	#calculate year of study
	$entry = intval(substr($cohot,0,4));
	$current = intval(substr($ayear,0,4));
	$yearofstudy=$current-$entry;
	
	if($yearofstudy==0){
		$class="FIRST YEAR";
		}elseif($yearofstudy==1){
		$class="SECOND YEAR";
		}elseif($yearofstudy==2){
		$class="THIRD YEAR";
		}elseif($yearofstudy==3){
		$class="FOURTH YEAR";
		}elseif($yearofstudy==4){
		$class="FIFTH YEAR";
		}elseif($yearofstudy==5){
		$class="SIXTH YEAR";
		}elseif($yearofstudy==6){
		$class="SEVENTH YEAR";
		}else{
		$class="";
	}
	#cohort number
	$yearofstudy = $yearofstudy +1;
	$totalcolms =0;
	
	#determine total number of columns
	
		$qcolmns = "SELECT DISTINCT CourseCode, Status FROM courseprogramme 
				WHERE  (ProgrammeID='$deg') AND (YearofStudy='$yearofstudy') AND (AYear='$year') AND (Semester='$semval')
				ORDER BY Semester, CourseCode";
			

		
	$dbcolmns = mysql_query($qcolmns);
	$dbcolmnscredits = mysql_query($qcolmns);
	$dbexamcat = mysql_query($qcolmns);
	$totalcolms = mysql_num_rows($dbcolmns);
	
 	$degree=$deg;
 	
 
 	
	#getting the number of credits to be studied	
	
		$mysql = mysql_query("SELECT * FROM coursecountprogramme WHERE ProgrammeID='$deg' AND YearofStudy='$yearofstudy' 
						AND AYear='$year' AND Semester='$semval'");
		
						
	$mysq = mysql_fetch_array($mysql);
	$semx = $mysq['CourseCount']; 
		
	# Print Report header	
	$rpttitle='OVERALL SUMMARY OF EXAMINATIONS RESULTS -'.$ayear;
	$objPHPExcel->getActiveSheet()->mergeCells('A1:Z1');
	$objPHPExcel->getActiveSheet()->mergeCells('A2:Z2');
	$objPHPExcel->getActiveSheet()->mergeCells('A3:Z3');

	$objPHPExcel->getActiveSheet()->mergeCells('A5:Z5');


	$objPHPExcel->getActiveSheet()->mergeCells('A4:Z4');
	
		$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('A1', strtoupper($org))
->setCellValue('A2', strtoupper($faculty))
		    ->setCellValue('A3', strtoupper($rpttitle))
 ->setCellValue('A4', "NTA LEVEL :  " .strtoupper($nta) . "                                          FIELD OF STUDY  :  " . strtoupper($programme))
		    ->setCellValue('A5', "YEAR OF STUDY :  " . $class . "         SEMESTER : " . strtoupper($sem) . "           DATE :.....................................           WEIGHT : CA 40%            WEIGHT : SE60%");

	
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20); 
	$objPHPExcel->getActiveSheet()->getStyle('A2:A3')->getFont()->setSize(16); 
	$objPHPExcel->getActiveSheet()->getStyle('A2:A3')->getFont()->setSize(13);
	$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getStyle('A1:A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
	$colm = "A";
	$rows = 6;
	$rows1 = $rows +1;
	
	$objPHPExcel->getActiveSheet()->getStyle('A6:BE6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		

	
	
	#Print Serial Number
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
				      ->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue($colm.$rows, '#');
		    
	$colm++; 
		    
	#Print Sex
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
				      ->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue($colm.$rows, 'Name'); 
	$colm++;
	
	#Print RegNo
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
				      ->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue($colm.$rows, 'RegNo'); 
	$colm++; 	

	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
				      ->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue($colm.$rows, 'Sex'); 
	$colm++; 
	
	
	

	$progcredits=0;
	$totalunits = 0; 
	$coursecount = 0;
	   $rows=6;
	#Print Course Headers
	while($rowcourseheader = mysql_fetch_array($dbcolmnscredits)) 
	{ 
		#get course units
		$course= $rowcourseheader['CourseCode'] ;
		
		#get semester 
		$qsem= "SELECT DISTINCT Semester FROM examresult WHERE CourseCode='$course'";
		$dbsem = mysql_query($qsem);
		$row_sem = mysql_fetch_array($dbsem);
		$sem=$row_sem['Semester'];
		if ($sem =='Semester I'){
			$semval = 1;
		}elseif ($sem=='Semester II'){
			$semval = 2;
		}
	   	#get course unit
		$qunits = "SELECT Units FROM course WHERE CourseCode='$course'";
		$dbunits = mysql_query($qunits);
		$row_units = mysql_fetch_array($dbunits);
		$unit=$row_units['Units'];
		$progcredits = $progcredits+$unit;	 

		$colmc = $colm;
	
	
			$colmc++; $colmc++; $colmc++;
			$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colmc.$rows)
						      ->getStyle($colm.$rows.':'.$colmc.$rows)->applyFromArray($styleArray);		    
			#write down the course code.			     			      
			$objPHPExcel->setActiveSheetIndex(0)
				    ->setCellValue($colm.$rows, $course.' ('.$unit.')');  
			#rotate text to 90 degree
			$config_data[$course]=$colm.'-'.$rows;
			//$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colmc.$rows)
				//		      ->getStyle($colm.$rows.':'.$colmc.$rows)->getAlignment()->setTextRotation(90);
			#set row height to 74 points
			//$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(74);
			
			$totalunits = $totalunits + $unit;		
		
		
		$rows++;
			$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);
	
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm++.$rows, ' CA ');

			$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);	
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm++.$rows, ' FE ');
			
			$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm++.$rows, 'Total');
			
			$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm++.$rows, ' GD ');
			$objPHPExcel->getActiveSheet()->getStyle($colm.$rows)->applyFromArray($styleArray);
			
		$rows = $rows -1;
		//$sem='';
	}
 
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
					->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
					->getStyle($colm.$rows.':'.$colm.$rows1);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, '#Courses');
	$colm++;

	
	
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
					->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, 'POINTS');
	$ptscell=$colm.$rows.':'.$colm.$rows1;
	$colm++;
	
	
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
					->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, 'GPA');
	$ptscell=$colm.$rows.':'.$colm.$rows1;
	$colm++;
		
	$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm.$rows1)
					->getStyle($colm.$rows.':'.$colm.$rows1)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, 'REMARK');
	$colm++;
	
	
	

# print candidate results

	$overallpasscount = 0;
	$overallsuppcount = 0;
	$overallinccount = 0;
	$overalldiscocount = 0;
	
	
	
	
		$qstudent = "SELECT Name, RegNo, Sex, DBirth, ProgrammeofStudy, Faculty, Sponsor, EntryYear, Status, Class 
				 FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY RegNo";
		
				 
	$dbstudent = mysql_query($qstudent);
	$totalstudent = mysql_num_rows($dbstudent);
	$z=1;
	$rows = 8;
	

	#freez
	$objPHPExcel->getActiveSheet()->freezePane($colm.$rows);
		
	while($rowstudent = mysql_fetch_array($dbstudent)) 
	{
		// check if student disco or deceaded
		$regno = $rowstudent['RegNo'];
		$check_disco ="SELECT * FROM studentremark WHERE (Remark ='DISCO' OR Remark ='DECEASED') AND RegNo='$regno' AND AYear < '$nameofcurrentyear'";
		$result_disco =mysql_query($check_disco);
		$num_disco_st = mysql_num_rows($result_disco);
		if($num_disco_st == 0) {
		$colms = "A";		
		$regno = $rowstudent['RegNo'];
			
			$name = stripslashes($rowstudent['Name']);			
			$sex = $rowstudent['Sex'];
			$bdate = $rowstudent['DBirth'];
			$comb = $rowstudent['Class'];
			$degree = stripslashes($rowstudent["ProgrammeofStudy"]);
			$faculty = stripslashes($rowstudent["Faculty"]);
			$sponsor = stripslashes($rowstudent["Sponsor"]);
			$entryyear = stripslashes($result['EntryYear']);
			$ststatus = stripslashes($rowstudent['Status']);
			
		
		
		#initialise
			$totalunit=0;
			$gmarks=0;
			$totalfailed=0;
			$totalinccount=0;
			$unittaken=0;
			$creditslost=0;
			$requiredcredits=0;
			$creditstaken=0;
                        $inco_count=0;
                        $supp_count=0;
			$optcredits=0;
			$extracreditstaken=0;
			$sgp=0;
			$totalsgp=0;
			$gpa=0;
			$pct=0;
			$chas=0;
			$failed = 0;
			
		# new values
			$totalfailed=0;
			$totalinccount=0;
			$halfsubjects=0;
			$ovremark='';
			$gmarks=0;
			$avg =0;
			$gmarks=0;	
			$decrement = 0;
			
			$key = $regno; 			
		
		# Print results
		
		#Print Serial Number
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $z);
		$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
		$colms++; 
		
		
		#Print Serial Number
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $name);
		$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
		$colms++; 
				
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $regno);
		$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
		$colms++;
		
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $sex);
		$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
		$colms++;
		
	
				
		# search for course results and prints	
		
			#get all courses
			
				$qcourselist = "SELECT DISTINCT CourseCode, Status FROM courseprogramme 
				WHERE  (ProgrammeID='$deg') AND (YearofStudy='$yearofstudy') AND (AYear='$year') AND (Semester='$semval')
				ORDER BY Semester, CourseCode";
				
			$dbcourselist = mysql_query($qcolmns);
			
			$niku1 = 0;
			while($row_courselist = mysql_fetch_array($dbcourselist)) 
			{ 
				$course= $row_courselist['CourseCode'];
				$coption = $row_courselist['Status']; 
				$qcoursestd="SELECT Units, Department, StudyLevel FROM course WHERE CourseCode = '$course'";	
				$dbcoursestd = mysql_query($qcoursestd);
				$row_coursestd = mysql_fetch_array($dbcoursestd);
				$unit = $row_coursestd['Units'];					
				$remarks = 'remarks';
				$RegNo = $key;
				$currentyear=$year;
                                $projscore="";
				  include 'choose_studylevel.php';
				
				
				# find course's examninationregulation
				$qexamreg = "SELECT StudyLevel FROM course  WHERE  CourseCode = '$course'";
				$dbexamreg = mysql_query($qexamreg);
				$row_result_sheet = mysql_fetch_array($dbexamreg);
				$studylevel= $row_result_sheet['StudyLevel'];				

				
					
				$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms++.$rows, $test2score);
				$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms++.$rows, $aescore);
				$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms++.$rows, ($marks));
				$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
			
				
				
				#shade failed grades
				if($remark<>'PASS'){
					//$colms++;// $colms++; $colms++;
					
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					//$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->getStartColor()->setARGB('FFFF0000');
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->getStartColor()->setARGB('CCCCCC');
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->applyFromArray($styleArray);
					
					$decrement =$decrement + $unit;
					
					if($unit > 0){
						$failed = $failed + 1;
						}
						 // $colms--;					
				}
				else{
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->applyFromArray($styleArray);
					}
                           if($grade > 'C'){
                             $supp_count++;
                              }
                 
          if($grade == 'I'){
                             $inco_count++;
                              }
                 
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms++.$rows, $grade);				
				
			         					
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows,' '.' '.' '. $totalcolms.' '.' '.' ');
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
			$colms++;
			
			# Assign total credits
			$requiredcredits = $progcredits-$optcredits-$extracreditstaken;
			
			$curr_semester=$semval;
			//include '../academic/includes/compute_overall_remark.php';
 
 			
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, number_format($totalsgp,2));
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
			$colms++;
			
				
			
			
					
			# compute gpa
			$gpa= substr(formatgpa($totalsgp/$unittaken),0,5);
			/*if($gpa == 1){
				$gpa = '1.0';
				}
			elseif($gpa == 2){
				$gpa = '2.0';
				}
			elseif($gpa == 3){
				$gpa = '3.0';
				}
			elseif($gpa == 4){
				$gpa = '4.0';
				}
			elseif($gpa == 5){
				$gpa = '5.0';
				}*/				
		   
		    if($supp==1){
				$remark = "SUPP";
                                $gpa ='---';
				}else if($inco_count >0){
				$remark = "INCO";
                                $gpa ='---';
				}elseif ($suppfe==1){
					if($gpa < 2){
					$remark = "DISCO";	
					}else{
					$remark = "SUPP";
				}
				}else{
				$remark = "PASS";
			}
				
				//get total required unit per year
			$sql = "SELECT * FROM coursecountprogramme WHERE ProgrammeID='$prog' AND AYear='$ayear' AND YearofStudy='$yearofstudy' AND Semester='$seme_ster'";
			$mysq_result = mysql_query($sql);
			$num_row = mysql_num_rows($mysq_result);
			if($num_row == 0){
				$gpa = '---';
				$remark = "INCO (LESS UNIT)";
			}else{
				$unit_required = 0;
				while ($r = mysql_fetch_array($mysq_result)){
					$unit_required+=$r['CourseCount'];
				}
				
				if($unittaken < $unit_required){
					$gpa ='---';
					$remark ='INCO (LESS UNIT)';
				}
			}
			
$objPHPExcel->getActiveSheet()->getCell($colms.$rows)->setValueExplicit($gpa, PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);
			$colms++;	
		
			
			
			if($remark == 'DISCO'){
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->getStartColor()->setARGB('FFFF0000');					
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->applyFromArray($styleArray);					
				}
				
			elseif($remark == 'PASS'){
				#if pass then clean sheet					
				}
			elseif($remark == 'ABSC'){
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);					
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->getFill()->getStartColor()->setARGB('CCCCCC');
					$objPHPExcel->getActiveSheet()->getStyle($colms--.$rows)->applyFromArray($styleArray);					
				}
							
			//$objPHPExcel->setActiveSheetIndex(1)->setCellValue($colms.$rows, $gpa);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$rows, $remark);
			$objPHPExcel->getActiveSheet()->getStyle($colms.$rows)->applyFromArray($styleArray);			
			# prints overall remarks
			$colms++;
			
			
			// get end of the course column
			$end_subject = "E";
			for ($x =0; $x < $totalcolms; $x++){
				$end_subject++;
			}
				
		$rows++;
		$z=$z+1;
		$counter = $rows;
		$supp='';
		$fsup='';
		$remark = '';
		$suppfe='';
		
 	}
}
 	
 	$colms = 'C';
 	$counter = $counter + 2;
 	$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->applyFromArray($styleArray);
 	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$counter, ' KEY FOR THE COURSE CODES  ');
	//$colms++;
	
	#get the course details
	
		$qcolmn = mysql_query("SELECT DISTINCT cp.CourseCode, c.CourseName FROM courseprogramme cp, course c 
							WHERE (cp.ProgrammeID='$prog') AND (cp.YearofStudy='$yearofstudy') AND (AYear='$ayear') AND (cp.Semester='$semval')
							AND (cp.CourseCode=c.CourseCode) ORDER BY cp.CourseCode");
		
		
	$y = $colms;
	for($x=1; $x<=15; $x++){		
		$y++;			
		$colms2 = $y;
		}
	$colms1 = $colms;
	$colms1++;
	
	while($course = mysql_fetch_array($qcolmn)){
		
		#print coursecode
		$counter += 1;
		$objPHPExcel->getActiveSheet()->getStyle($colms.$counter)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms.$counter, ' '.$course['CourseCode']);				
		//$counter2 = $counter + 1;
		#print course title
		$objPHPExcel->getActiveSheet()->mergeCells($colms1.$counter.':'.$colms2.$counter)
					->getStyle($colms1.$counter.':'.$colms2.$counter)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colms1.$counter, ' '.$course['CourseName']);
		//$colms++;	
	}
	
 	//}


// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Semester Report');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="SARIS_Exam_Results.xls"');
header('Cache-Control: max-age=0');
#make pdf
/*
header('Content-Type: application/pdf');
header('Content-Disposition: attachment;filename="SARIS_Exam_Results.pdf"');
header('Cache-Control: max-age=0');
*/
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
#make pdf
// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
$objWriter->save('php://output');
exit;

