<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header
include('lecturerMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Department Information';
$szSubSection = 'Policy Setup';
include("lecturerheader.php");

if (isset($_GET['details'])) {
    $result = $zalongwa->query("SELECT * FROM department INNER JOIN faculty ON Faculty=facultyID INNER JOIN campus ON department.CampusID=campus.CampusID WHERE DeptID=" . $_GET['details']);
    $institution = $result->fetch_assoc();
    if ($result->num_rows > 0) {
        ?>
        <table class='table_view'>
            <tr class='header'>
                <td> Field</td>
                <td> Field Value</td>
            </tr>
            <tr class='list'>
                <td> Campus Name</td>
                <td><?php echo $institution['Campus']; ?></td>
            </tr>
            <tr class='list'>
                <td> Faculty Name</td>
                <td><?php echo $institution['FacultyName']; ?></td>
            </tr>
            <tr class='list'>
                <td> Department Name</td>
                <td><?php echo $institution['DeptName']; ?></td>
            </tr>
            <tr class='list'>
                <td> Physical Address:</td>
                <td><?php echo $institution['DeptPhysAdd']; ?></td>
            </tr>
            <tr class='list'>
                <td> Address:</td>
                <td><?php echo $institution['DeptAddress']; ?></td>
            </tr>
            <tr class='list'>
                <td> Telephone:</td>
                <td><?php echo $institution['DeptTel']; ?></td>
            </tr>
            <tr class='list'>
                <td> Email:</td>
                <td><?php echo $institution['DeptEmail']; ?></td>
            </tr>
            <tr class='list'>
                <td> Dept Head:</td>
                <td><?php echo $institution['DeptHead']; ?></td>
            </tr>
        </table>
        <br>
        <table class='table_view'>
            <tr style='float: right' class='list'>
                <td><a href="admissionDepartment.php">Back</a></td>
            </tr>
        </table>
        <?php
    } else {
        echo "Sorry, No Records Found <br>";
    }
}
# include the footer
include("../footer/footer.php");
?>