<?php
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Security';
$szTitle = 'Change Password';
$szSubSection = 'Change Password';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /*background-color: #009688;*/
        }
        .card {
            /*background-color: #324359;*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /*color: white;*/
            padding: 0px;
            border-radius: 0px !important;
        }
        .row {
            margin-top: 20px;
        }
    </style>
    <title>SARIS | <?php echo  $szSection ?> | <?php echo  $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php';?>

<div class="container-flex">
    <!--    <div class="container">-->
    <!--        Change Password-->
    <!--    </div>-->

</div>
<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <h3 class="card-header">
                    Change Password</h3>
                <div class="card-block">
                    <?php
                    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
                    {
                        $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

                        switch ($theType) {
                            case "text":
                                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                                break;
                            case "long":
                            case "int":
                                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                                break;
                            case "double":
                                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                                break;
                            case "date":
                                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                                break;
                            case "defined":
                                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                                break;
                        }
                        return $theValue;
                    }

                    $editFormAction = $_SERVER['PHP_SELF'];
                    if (isset($_SERVER['QUERY_STRING'])) {
                        $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
                    }

                    if (isset($_POST["MM_update"]) && ($_POST["MM_update"] == "fmAdd")) {


                        $txtusername = addslashes($_POST['username']);
                        $pass = addslashes($_POST['txtoldPWD']);

                        $newpass = addslashes($_POST['txtnewPWD']);
                        $confirm = addslashes($_POST['txtrenewPWD']);
                        // Generate jlungo hash for old password
                        $hashold = "{jlungo-hash}" . base64_encode(pack("H*", sha1($pass)));
                        // Generate jlungo hash for new password
                        $hashnew = "{jlungo-hash}" . base64_encode(pack("H*", sha1($newpass)));

                        $getlastuser = "SELECT * FROM security WHERE UserName='$txtusername'";
                        $get_old = mysqli_query($zalongwa, $getlastuser);
                        $fetch_old = mysqli_fetch_array($get_old);
                        $passolddata = $fetch_old['password'];
                        if ($passolddata != $hashold) {
                            $updateGoTo = 'admissionChangepassword.php?error=' . urlencode("Invalid Old Password");
                            echo '<meta http-equiv = "refresh" content ="0; url =' . $updateGoTo . '">';
                            exit;
                        } else if ($newpass != $confirm) {
                            $updateGoTo = 'admissionChangepassword.php?error=' . urlencode("New password should Match Confirm Password");
                            echo '<meta http-equiv = "refresh" content ="0; url =' . $updateGoTo . '">';
                            exit;
                        } else {
                            $updateSQL = "UPDATE security SET password='$hashnew' WHERE UserName='$txtusername' AND password ='$hashold'";
                            //mysql_select_db($database_zalongwa, $zalongwa);
                            $Result1 = mysqli_query($zalongwa, $updateSQL);
                            if ($Result1) {
                                $updateGoTo = "../index.php";

                                echo '<meta http-equiv = "refresh" content ="0; url =' . $updateGoTo . '">';
                                exit;
                            }
                        }
                    }
                    $colname_changepassword = "1";
                    if (isset($_COOKIE['UserName'])) {
                        $colname_changepassword = (get_magic_quotes_gpc()) ? $_COOKIE['UserName'] : addslashes($_COOKIE['UserName']);
                    }
                    $query_changepassword = sprintf("SELECT * FROM security WHERE UserName = '$username'", $colname_changepassword);
                    $changepassword = mysqli_query($zalongwa, $query_changepassword) or die(mysqli_error($zalongwa));
                    $row_changepassword = mysqli_fetch_assoc($changepassword);
                    $totalRows_changepassword = mysqli_num_rows($changepassword);
                    ?>
                    <SCRIPT ID=clientEventHandlersJS LANGUAGE=javascript>
                        <!--

                        function fmAdd_onsubmit() {
                            if (fmAdd.txtoldPWD.value == "") {
                                window.alert("OUT Student Information System Asks You to Fill in the Blank Text Fields");
                                document.getElementById('txtoldPWD').focus();
                                return false;
                            }
                            if (fmAdd.txtnewPWD.value == "") {
                                window.alert("OUT Student Information System Asks You to Fill in the Blank Text Fields");
                                document.getElementById('txtnewPWD').focus();
                                return false;
                            }
                            if (fmAdd.txtrenewPWD.value == "") {
                                window.alert("OUT Student Information System Asks You to Fill in the Blank Text Fields");
                                document.getElementById('txtrenewPWD').focus();
                                return false;
                            }
                            if (fmAdd.txtnewPWD.value != fmAdd.txtrenewPWD.value) {
                                window.alert("Password Texts donot Match, Enter them again, SARIS");
                                document.getElementById('txtnewPWD').focus();
                                return false;
                            }
                        }

                        //-->
                    </SCRIPT>
                    <SCRIPT LANGUAGE="javascript1.2">
                        //form object
                        //var fmAdd=document.forms(0);
                        //Boolean to track if error found
                        var foundErr;
                        //Form element index number which the first error occured
                        var focusOn;

                        function check_form() {
                            foundErr = false;
                            focusOn = -1;
                            //Username field must be at least 5 char.
                            if (fmAdd.txtoldPWD.value.length < 5) {
                                alert("RegNo too short! RegNo must be at least 4 Charaters");
                                foundErr = true;
                                focusOn = 0;
                            }

                            //Passowrd field must be at least 5 char.
                            if (fmAdd.txtnewPWD.value.length < 5) {
                                alert("Birth Date too short! Birth Date must be at least 5 Charaters");
                                foundErr = true;
                                if (focusOn == -1) focusOn = 1;
                            }

                            //Hass any error occured?
                            if (foundErr) {
                                //Yes. Focus on Which the first occured
                                fmAdd.elements.focus(focusOn);
                            } else {
                                // No. Submit the form
                                fmAdd.submit()
                            }
                        }
                    </SCRIPT>
                    <?php
                    if (isset($_GET['error'])) {
                        echo '<div style="color:red;">' . $_GET['error'] . '</div>';
                    }
                    include("../includes/changepass_form.php");
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>

