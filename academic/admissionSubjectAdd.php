<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header
//include('lecturerMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Course Information';
$szSubSection = 'Policy Setup';
//include("lecturerheader.php");

if (isset($_GET['content'])) {
    if ($_POST['add-btn']) {
        $CourseCode = addslashes($_POST["CourseCode"]);
        $CourseName = addslashes($_POST["CourseName"]);
        $Capacity = addslashes($_POST["Capacity"]);
        $Units = addslashes($_POST["Units"]);
        $Department = addslashes($_POST["Department"]);
        $StudyLevel = addslashes($_POST["StudyLevel"]);

        $sql = "INSERT INTO course (CourseCode, CourseName, Capacity, Units, Department, StudyLevel)
	  													VALUES (%s, %s, %d, %s, %s, %s)";

        if($zalongwa->query($sql)){
            echo "<p style='color: #008000'>Module created successfully...</p>";
        }else{
            die("Cannot query the database.<br>" . $zalongwa->connect_error);
        }

    }
//    $result = $zalongwa->query("SELECT * FROM course WHERE Id=" . $_GET['content']);
//    $institution = $result->fetch_assoc();
    ?>

    <!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <style>
        body {
            background-color: #eff0f1;
        }

        a:hover {
            text-decoration: none;
            color: #0056b3;
        }

        .navbar {
            width: 100%;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
        }

        .navbar-toggler {
            cursor: pointer;
            outline: 0;
            padding-top: inherit;
        }

        @media (max-width: 34em) {
            .navbar {
                padding-top: 5px;
                padding-bottom: 0px;
                overflow: hidden;
            }

            .navbar-toggler {
                cursor: pointer;
                outline: 0;
                padding-top: inherit;
            }

            .nav-link {
                color: whitesmoke;
            }

            .nav-item {
                padding-top: 30px;
                padding-bottom: 0px;
                margin-bottom: -3px;
            }

            .row {
                margin: -7%;
            }

            .card {
                margin: 2%;
            }
        }

        @media (max-width: 48em) {
            .card h5 {
                font-size: 14px;
            }
        }

        .row {
            margin-top: 20px;
        }

        .card {
            border-top: 7px solid #263238;
            padding-top: 5%;
        }

        .card-block a {
            color: #263238;
        }

        footer {
            bottom: 0;
            width: 100%;
            margin-top: 20px;
            padding: 2px;
        }

        footer p {
            margin-top: 0;
            margin-bottom: 1rem;
            font-size: small;
            margin: 0px;
        }

        a {
            -webkit-transition: .25s all;
            transition: .25s all;
        }

        .card:focus, .card:hover {
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            /*box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.20);*/
        }

        .card-inverse .card-img-overlay {
            background-color: rgba(51, 51, 51, 0.85);
            border-color: rgba(51, 51, 51, 0.85);
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>

</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
        <div class="card">
            <h3 class="card-header">
                <?echo $szTitle; ?></h3>
            <div class="card-block">
                      <form action="admissionSubject.php?content=AddSubject" method="POST">
                          <div class="container">
                              <div class="form-group row">
                                  <label for="Department" class="col-sm-4 col-form-label">Department</label>
                                  <div class="col-sm-8">
                                      <select name="Department" class="form-control" required>
                                          <option value="" disabled="disabled"> --select--
                                          </option>
                                          <?php
                                          $query_dept = "SELECT DeptID, DeptName FROM department";
                                          $result_dept = $zalongwa->query($query_dept) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
                                          while ($dept_row = $result_dept->fetch_array()) {
                                              ?>
                                              <option <?php if($institution['Department'] == $dept_row['DeptName']){?> selected="selected" <?php }?> value="<?php echo $dept_row['DeptName']; ?>"><?php echo $dept_row['DeptName']; ?></option>
                                              <?php
                                          }
                                          ?>
                                      </select>

                                  </div>
                              </div>
                          </div>
                          <div class="container">
                              <div class="form-group row">
                                  <label for="CourseCode" class="col-sm-4 col-form-label">Module Code</label>
                                  <div class="col-sm-8">
                                    <input class="form-control" type="text" id="CourseCode" name="CourseCode" value="<?php echo $institution['CourseCode']; ?>" required>
                                  </div>
                              </div>
                          </div>
                          <div class="container">
                              <div class="form-group row">
                                  <label for="CourseCode" class="col-sm-4 col-form-label">Module Code</label>
                                  <div class="col-sm-8">
                                      <input class="form-control" type="text" id="CourseName" name="CourseName" value="<?php echo $institution['CourseName']; ?>" required>

                                  </div>
                              </div>
                          </div>
                          <div class="container">
                              <div class="form-group row">
                                  <label for="Capacity" class="col-sm-4 col-form-label">Capacity</label>
                                  <div class="col-sm-8">
                                      <input class="form-control" type="text" id="Capacity" name="Capacity" value="<?php echo $institution['Capacity']; ?>" required>

                                  </div>
                              </div>
                          </div>
                          <div class="container">
                              <div class="form-group row">
                                  <label for="Units" class="col-sm-4 col-form-label">Units</label>
                                  <div class="col-sm-8">
                                      <input class="form-control" type="text" id="Units" name="Units" value="<?php echo $institution['Units']; ?>" required>

                                  </div>
                              </div>
                          </div>
                          <div class="container">
                              <div class="form-group row">
                                  <label for="Units" class="col-sm-4 col-form-label">Exam Regulation</label>
                                  <div class="col-sm-8">
                                      <select name="StudyLevel" class="form-control" required>
                                          <option value="" disabled="disabled"> --select--
                                          </option>
                                          <?php
                                          $query_level = "SELECT Code, StudyLevel FROM programmelevel";
                                          $result_level = $zalongwa->query($query_level) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
                                          while ($level_row = $result_level->fetch_array()) {
                                              ?>
                                              <option <?php if($institution['StudyLevel'] == $level_row['Code']){?> selected="selected" <?php }?> value="<?php echo $level_row['Code']; ?>"><?php echo $level_row['StudyLevel']; ?></option>

                                              <?php
                                          }
                                          ?>
                                      </select>
                                  </div>
                              </div>
                          </div>
                          <div class="container">
                              <div class="form-group row">
                                  <label class="col-sm-4 col-form-label"></label>
                                  <div class="col-sm-8">
                                      <input class="btn btn-primary btn-md btn-block" onclick="return show_confirm()"
                                             name="add-btn" type="submit" id="Confirm" value="Confirm">
                                  </div>

                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>



    <?php

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
        $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}

# include the footer
include("../footer/footer.php");
?>
</body>
</html>
