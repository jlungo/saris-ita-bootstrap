<?php
if (isset($_POST['EXCEL'])) {
    #get connected to the database and verfy current session
    require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
    #get post variables
    $prog = addslashes($_POST['degree']);
    $year = addslashes($_POST['ayear']);
    $cohot = addslashes($_POST['cohot']);
    $sem = addslashes($_POST['sem']);
    //$st_report =$_POST['st_report'];


    //$st_report =$_POST['st_report'];
    $check_stud = "SELECT * FROM student WHERE EntryYear='$cohot' AND ProgrammeofStudy='$prog'";
    $sql_prog = "SELECT ProgrammeName FROM programme WHERE ProgrammeCode='$prog'";
    $result_prog = mysqli_query($zalongwa, $sql_prog);
    $row_prog = mysqli_fetch_array($result_prog);
    $prog_name = $row_prog['ProgrammeName'];

    if (mysqli_num_rows(mysqli_query($zalongwa, $check_stud)) > 0) {
        $check_res = "SELECT *FROM examresult WHERE RegNo IN (SELECT RegNo FROM student WHERE EntryYear='$cohot' 
							AND ProgrammeofStudy='$prog') AND AYear='$year'";

        if (mysqli_num_rows(mysqli_query($zalongwa, $check_res)) > 0) {
            require_once('includes/print_excel_semester.php');
        } else {
            $error = urlencode("Sorry!, There are no results for the programme $prog_name in the Academic Year $year");
            $location = "lecturerSemesterresultsheet.php?error=$error";
            echo '<meta http-equiv="refresh" content="0; url=' . $location . '">';
        }
    } else {
        $error = urlencode("Sorry!, There are no student for the programme $prog_name in the Entry Year $cohot");
        $location = "lecturerSemesterresultsheet.php?error=$error";
        echo '<meta http-equiv="refresh" content="0; url=' . $location . '">';
    }

    exit;
}

#get connected to the database and verfy current session
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

# include the header
global $szSection, $szSubSection;
$szSection = 'Examination';
$szSubSection = 'Semester Results';
$szTitle = 'Printing Semester Examinations Results Report';

$editFormAction = $_SERVER['PHP_SELF'];


$query_studentlist = "SELECT RegNo, Name, ProgrammeofStudy FROM student ORDER BY ProgrammeofStudy  ASC";
$studentlist = mysqli_query($zalongwa, $query_studentlist) or die(mysqli_error($zalongwa));
$row_studentlist = mysqli_fetch_assoc($studentlist);
$totalRows_studentlist = mysqli_num_rows($studentlist);


$query_degree = "SELECT ProgrammeCode, ProgrammeName FROM programme ORDER BY ProgrammeName ASC";
$degree = mysqli_query($zalongwa, $query_degree) or die(mysqli_error($zalongwa));
$row_degree = mysqli_fetch_assoc($degree);
$totalRows_degree = mysqli_num_rows($degree);


$query_ayear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$ayear = mysqli_query($zalongwa, $query_ayear) or die(mysqli_error($zalongwa));
$row_ayear = mysqli_fetch_assoc($ayear);
$totalRows_ayear = mysqli_num_rows($ayear);


$query_sem = "SELECT Semester FROM terms ORDER BY Semester ";
$sem = mysqli_query($zalongwa, $query_sem) or die(mysqli_error($zalongwa));
$row_sem = mysqli_fetch_assoc($sem);
$totalRows_sem = mysqli_num_rows($sem);


$query_dept = "SELECT Faculty, DeptName FROM department ORDER BY DeptName, Faculty ASC";
$dept = mysqli_query($zalongwa, $query_dept) or die(mysqli_error($zalongwa));
$row_dept = mysqli_fetch_assoc($dept);
$totalRows_dept = mysqli_num_rows($dept);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="./css/breadcrumb.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /*background-color: #009688;*/
        }

        .card {
            /*background-color: #324359;*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /*color: white;*/
            padding: 0px;
            border-radius: 0px !important;
        }

        .row {
            margin-top: 20px;
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>

    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">
            <?php
            if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
                echo "<h4 align='center'>";

                $prog = $_POST['degree'];
                $cohotyear = $_POST['cohot'];
                $ayear = $_POST['ayear'];
                $qprog = "SELECT ProgrammeCode, Title FROM programme WHERE ProgrammeCode='$prog'";
                $dbprog = mysqli_query($zalongwa, $qprog);
                $row_prog = mysqli_fetch_array($dbprog);
                $progname = $row_prog['Title'];
                $qyear = "SELECT AYear FROM academicyear WHERE AYear='$cohotyear'";
                $dbyear = mysqli_query($zalongwa, $qyear);
                $row_year = mysqli_fetch_array($dbyear);
                $year = $row_year['AYear'];
                echo $progname;
                echo " - " . $year . "<br></h4>";

                @$checkdegree = addslashes($_POST['checkdegree']);
                @$checkyear = addslashes($_POST['checkyear']);
                @$checksem = addslashes($_POST['checksem']);
                $checkcohot = addslashes($_POST['checkcohot']);

                $c = 0;

                if (($checkdegree == 'on') && ($checkyear == 'on') && ($checksem == 'on') && ($checkcohot == 'on')) {
                } elseif (($checkdegree == 'on') && ($checkcohot == 'on')) {
                } elseif ($checkcohot == 'on') {
                }
            } else {

                if (isset($_GET['error'])) {
                    $error = urldecode($_GET['error']);
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $error; ?>
                    </div>

                <?php } ?>

                <div class="card">
                    <h3 class="card-header">
                        Tick the corresponding check box first then select appropriately
                    </h3>
                    <div class="card-block">
                        <form name="form1" method="post" action="<?php echo $editFormAction ?>">
                            <div class="container">
                                <div class="form-group row">

                                    <label class="col-sm-4 col-form-label">
                                        <input name="checkdegree" type="checkbox" id="checkdegree" value="on" checked>
                                        &nbsp;&nbsp;Degree Programme:
                                    </label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="degree" id="degree">
                                            <?php
                                            do {
                                                echo "<option value='" . $row_degree['ProgrammeCode'] . "'>$row_degree[ProgrammeName]</option>";
                                            } while ($row_degree = mysqli_fetch_assoc($degree));

                                            $rows = mysqli_num_rows($degree);
                                            if ($rows > 0) {
                                                mysqli_data_seek($degree, 0);
                                                $row_degree = mysqli_fetch_assoc($degree);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><input name="checkcohot" type="checkbox"
                                                                                  id="checkcohot" value="on" checked>&nbsp;&nbsp;Intake
                                        of the Year:
                                    </label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="cohot" id="cohot">
                                            <?php
                                            do {
                                                echo "<option value='" . $row_ayear['AYear'] . "'>$row_ayear[AYear]</option>";
                                            } while ($row_ayear = mysqli_fetch_assoc($ayear));

                                            $rows = mysqli_num_rows($ayear);
                                            if ($rows > 0) {
                                                mysqli_data_seek($ayear, 0);
                                                $row_ayear = mysqli_fetch_assoc($ayear);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><input name="checkyear" type="checkbox"
                                                                                  id="checkyear" value="on" checked>&nbsp;&nbsp;Results
                                        of the Year:
                                    </label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="ayear" id="ayear">
                                            <?php
                                            do {
                                                echo "<option value='" . $row_ayear['AYear'] . "'>$row_ayear[AYear]</option>";

                                            } while ($row_ayear = mysqli_fetch_assoc($ayear));

                                            $rows = mysqli_num_rows($ayear);
                                            if ($rows > 0) {
                                                mysqli_data_seek($ayear, 0);
                                                $row_ayear = mysqli_fetch_assoc($ayear);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><input name="checksem"
                                                                                  type="checkbox"
                                                                                  id="checksem"
                                                                                  value="on"
                                                                                  checked>&nbsp;&nbsp;Semester:
                                    </label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="sem" id="sem">
                                            <?php
                                            do {
                                                echo "<option value='" . $row_sem['Semester'] . "'>$row_sem[Semester]</option>";

                                            } while ($row_sem = mysqli_fetch_assoc($sem));

                                            $rows = mysqli_num_rows($sem);
                                            if ($rows > 0) {
                                                mysqli_data_seek($sem, 0);
                                                $row_sem = mysqli_fetch_assoc($sem);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"></label>
                                    <div class="col-sm-8">
                                        <input type="submit" name="EXCEL" id="EXCEL" value="Generate Report"
                                               class="btn btn-primary btn-md btn-block">
                                    </div>
                                </div>
                            </div>
                            <input name="MM_update" type="hidden" id="MM_update" value="form1">
                        </form>
                    </div>
                </div>

                <?php
            }
            ?>
        </div>
    </div>
</div>
<br>
<br>
<br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>
