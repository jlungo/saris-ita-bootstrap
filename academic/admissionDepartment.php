<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Department Information';
$szSubSection = 'Policy Setup';


if (isset($_GET['delete'])) {
    $zalongwa->query("DELETE FROM department WHERE DeptID=" . $_GET['delete']);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #eff0f1;
        }

        a:hover {
            text-decoration: none;
            color: #0056b3;
        }

        .navbar {
            width: 100%;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
        }

        .navbar-toggler {
            cursor: pointer;
            outline: 0;
            padding-top: inherit;
        }

        @media (max-width: 34em) {
            .navbar {
                padding-top: 5px;
                padding-bottom: 0px;
                background-color: #FAFAFA;
                overflow: hidden;
            }

            .navbar-toggler {
                cursor: pointer;
                outline: 0;
                padding-top: inherit;
            }

            .nav-link {
                color: whitesmoke;
            }

            .nav-item {
                padding-top: 30px;
                padding-bottom: 0px;
                margin-bottom: -3px;
            }

            .row {
                margin: -7%;
            }

            .card {
                margin: 2%;
            }
        }

        @media (max-width: 48em) {
            .card h5 {
                font-size: 14px;
            }
        }

        .row {
            margin-top: 20px;
        }

        .card {
            border-top: 7px solid #263238;
            padding-top: 5%;
        }

        .card-block a {
            color: #263238;
        }

        footer {
            bottom: 0;
            width: 100%;
            margin-top: 20px;
            padding: 2px;
        }

        footer p {
            margin-top: 0;
            margin-bottom: 1rem;
            font-size: small;
            margin: 0px;
        }

        a {
            -webkit-transition: .25s all;
            transition: .25s all;
        }

        .card:focus, .card:hover {
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            /*box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.20);*/
        }

        .card-inverse .card-img-overlay {
            background-color: rgba(51, 51, 51, 0.85);
            border-color: rgba(51, 51, 51, 0.85);
        }
    </style>

    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>

    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>

</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<nav class="breadcrumb">
    <a class="breadcrumb-item" href="lecturerindex.php">Home</a>
    <a class="breadcrumb-item" href="javascript:history.back();">Back</a>
    <span class="breadcrumb-item active">Check Message</span>
</nav>
<div class="container-flex">
    <br>
</div>
<div class="container">
    <h3 class="h3">Inbox</h3>
    <?php
    switch ($_GET['content']) {
        default:
            $query = "SELECT * FROM department";
            $result_sql = $zalongwa->query($query) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
            ?>
            <table class="table table-striped table-bordered nowrap" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th><a href="admissionDepartment.php?content=AddDepartment">+Add</a></th>
                    <th>Department</th>
                    <th>Physical Address</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Tel</th>
                    <th>Location</th>
                    <th>View</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($result = $result_sql->fetch_array()) {
                    $DeptID = $result['DeptID'];
                    $DeptName = stripslashes($result["DeptName"]);
                    $DeptPhysAdd = stripslashes($result["DeptPhysAdd"]);
                    $DeptAddress = stripslashes($result["DeptAddress"]);
                    $DeptTel = stripslashes($result["DeptTel"]);
                    $DeptEmail = stripslashes($result["DeptEmail"]);
                    $DeptHead = stripslashes($result["DeptHead"]);
                    ?>
                    <tr>

                        <td><a class="remove-blue-link"
                               href="admissionDepartmentedit.php?content=<?php echo $DeptID; ?>">Edit</a></td>
                        <td><?php echo $DeptName; ?></td>
                        <td><?php echo $DeptPhysAdd; ?></td>
                        <td><?php echo $DeptAddress; ?></td>
                        <td><?php echo $DeptEmail; ?></td>
                        <td><?php echo $DeptTel; ?></td>
                        <td><?php echo $DeptHead; ?></td>
                        <td><a href="admissionDepartmentDetails.php?details=<?php echo $DeptID; ?>">Details</a></td>
                        <td class="center"><a href="admissionDepartment.php?delete=<?php echo $DeptID; ?>"
                                              onClick="return confirm('Are you sure you want to delete <?php echo $DeptName; ?>')"><i
                                        class="fa fa-trash"></i></a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <?php
            break;

        case "AddDepartment":
            function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
            {
                $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

                switch ($theType) {
                    case "text":
                        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                        break;
                    case "long":
                    case "int":
                        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                        break;
                    case "double":
                        $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                        break;
                    case "date":
                        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                        break;
                    case "defined":
                        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                        break;
                }
                return $theValue;
            }

            if (isset($_POST["submit"])) {
                $sql_insert = sprintf("INSERT INTO department (CampusID, Faculty, DeptName, DeptPhysAdd, DeptAddress, DeptTel, DeptEmail, DeptHead)
	  													VALUES (%d, %s, %s, %s, %s, %s, %s, %s)",
                    GetSQLValueString($_POST['CampusID'], "int"),
                    GetSQLValueString($_POST['Faculty'], "text"),
                    GetSQLValueString($_POST['DeptName'], "text"),
                    GetSQLValueString($_POST['DeptPhysAdd'], "text"),
                    GetSQLValueString($_POST['DeptAddress'], "text"),
                    GetSQLValueString($_POST['DeptTel'], "text"),
                    GetSQLValueString($_POST['DeptEmail'], "text"),
                    GetSQLValueString($_POST['DeptHead'], "text"));

                if ($zalongwa->query($sql_insert)) {
                    echo "<p style='color: #008000'>Department created successfully...</p>";
                } else {
                    die("Cannot query the database.<br>" . $zalongwa->connect_error);
                }

            }
            ?>
            <form action="admissionDepartment.php?content=AddDepartment" method="POST">
                <p>* Items marked with an asterisk are required</p>
                <?php
                echo "<table class='table_view'>";
                echo "<tr class='header'><td> Field </td><td> Field Value </td></tr>";
                ?>

                <tr class='list'>
                    <td> Institution: *</td>
                    <td><select name="CampusID" required>
                            <option value="" disabled="disabled" selected="selected"> --select--
                            </option>
                            <?php
                            $query_campus = "SELECT CampusID, Campus FROM campus";
                            $result_campus = $zalongwa->query($query_campus) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
                            while ($campus_row = $result_campus->fetch_array()) {
                                ?>
                                <option value="<?php echo $campus_row['CampusID']; ?>"><?php echo $campus_row['Campus']; ?></option>
                                <?php
                            }
                            ?>
                        </select></td>
                </tr>
                <tr class='list'>
                    <td> Faculty: *</td>
                    <td><select name="Faculty" required>
                            <option value="" disabled="disabled" selected="selected"> --select--
                            </option>
                            <?php
                            $query_faculty = "SELECT FacultyID, FacultyName FROM faculty";
                            $result_faculty = $zalongwa->query($query_faculty) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
                            while ($faculty_row = $result_faculty->fetch_array()) {
                                ?>
                                <option value="<?php echo $faculty_row['FacultyID']; ?>"><?php echo $faculty_row['FacultyName']; ?></option>
                                <?php
                            }
                            ?>
                        </select></td>
                </tr>

                <?php
                echo "<tr class='list'><td> Department Name: *</td><td> <input type=\"text\" id=\"DeptName\" name=\"DeptName\" size=\"40\" required> </td></tr>";
                echo "<tr class='list'><td> Dept Head: *</td><td> <input type=\"text\" id=\"DeptHead\" name=\"DeptHead\" size=\"40\" required> </td></tr>";
                echo "<tr class='list'><td> Physical Address: *</td><td> <input type=\"text\" id=\"DeptPhysAdd\" name=\"DeptPhysAdd\" size=\"40\" required> </td></tr>";
                echo "<tr class='list'><td> Address: *</td><td> <input type=\"text\" id=\"DeptAddress\" name=\"DeptAddress\" size=\"40\" required> </td></tr>";
                echo "<tr class='list'><td> Telephone: </td><td> <input type=\"text\" id=\"DeptTel\" name=\"DeptTel\" size=\"40\"> </td></tr>";
                echo "<tr class='list'><td> Email: </td><td> <input type=\"text\" id=\"DeptEmail\" name=\"DeptEmail\" size=\"40\"> </td></tr>";
                echo "</table>";
                echo "<table class=\" table_view table_form\">";
                echo "<tr class=\"submit\">
                    <td>
                        <input onclick=\"location.href = 'admissionDepartment.php';\" type=\"button\"  value=\"Back\">
                        <input type=\"submit\" id=\"submit\" name=\"submit\" value=\"create\"> </td>
                  </tr>";
                echo "</table>";
                ?>
            </form>
            <?php
            break;
    }
    ?>

</div>
<br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<!--script for datatable-->
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details for ' + data[0] + ' ' + data[1];
                        }
                    }),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            }
        });
    });
</script>
</body>
</html>
