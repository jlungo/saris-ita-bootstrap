<?php

#start excel
if (isset($_POST['Excel']) && ($_POST['Excel'] == "Print Excel")) {
    #get connected to the database and verfy current session
    require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');


    include '../academic/reports/print_excel_results.php';
    exit;
}

#start pdf
if (isset($_POST['PDF']) && ($_POST['PDF'] == "Print PDF")) {
    #get connected to the database and verfy current session
    require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');

    #Get Organisation Name
    $qorg = "SELECT * FROM organisation";
    $dborg = mysqli_query($zalongwa, $qorg);
    $row_org = mysqli_fetch_assoc($dborg);
    $org = $row_org['Name'];

    @$checkdegree = addslashes($_POST['checkdegree']);
    @$checkyear = addslashes($_POST['checkyear']);
    @$checkdept = addslashes($_POST['checkdept']);
    @$checkcohot = addslashes($_POST['checkcohot']);
    @$paper = 'a3'; //addslashes($_POST['paper']);
    @$layout = 'L'; //addslashes($_POST['layout']);
    if ($paper == 'a3') {
        $xpoint = 1050.00;
        $ypoint = 800.89;
    } else {
        $xpoint = 800.89;
        $ypoint = 580.28;
    }
    $show_name = $_POST['show_name'];
    $prog = $_POST['degree'];
    $cohotyear = $_POST['cohot'];
    $ayear = $_POST['ayear'];
    // if($ayear < '2011/2012' ){
    $qprog = "SELECT ProgrammeCode, Title, Faculty, Ntalevel FROM programme WHERE ProgrammeCode='$prog'";
//}else{
//	$qprog= "SELECT ProgrammeCode, Title, Faculty, Ntalevel FROM programme WHERE ProgrammeCode='$prog' AND AYear='2011/2012'";
//}
    $dbprog = mysqli_query($zalongwa, $qprog);
    $row_prog = mysqli_fetch_array($dbprog);
    $progname = $row_prog['Title'];
    $faculty = $row_prog['Faculty'];
    $class = $row_prog['Ntalevel'];

    //calculate year of study
    $entry = intval(substr($cohotyear, 0, 4));
    $current = intval(substr($ayear, 0, 4));
    $yearofstudy = $current - $entry;

    global $ctrlstudent;

    if (($checkdegree == 'on') && ($checkyear == 'on') && ($checkcohot == 'on')) {
        $deg = addslashes($_POST['degree']);
        $year = addslashes($_POST['ayear']);
        $cohot = addslashes($_POST['cohot']);
        $dept = addslashes($_POST['dept']);
        $sem = addslashes($_POST['sem']);

        #determine total number of columns
        $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')";
        $dbstd = mysqli_query($zalongwa, $qstd);
        $stud_num = mysqli_num_rows($dbstd);
        $whereclause = " ";
        $stud = 1;
        $totalcolms = 0;
        while ($rowstd = mysqli_fetch_array($dbstd)) {
            $whereclause .= ($stud < $stud_num) ? "RegNo='$stdregno' OR " : "RegNo='$stdregno'";
            $stdregno = $rowstd['RegNo'];
            $qstdcourse = "SELECT DISTINCT coursecode FROM examresult WHERE (RegNo='$stdregno') AND (AYear='$year') AND (Semester='$sem') ORDER BY coursecode";

            $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
            $totalstdcourse = mysqli_num_rows($dbstdcourse);
            #count total courses for all students
            if ($totalstdcourse > $totalcolms) {
                $totalcolms = $totalstdcourse;
                $ctrlstudent = $stdregno;
            }
            $stud++;
        }

        if ($totalcolms > 4) {
            @$paper = 'a2'; //addslashes($_POST['paper']);
            @$layout = 'L'; //addslashes($_POST['layout']);
            if ($paper == 'a2') {
                $xpoint = 1600.78;
                $ypoint = 1150.55;
            } else {
                $xpoint = 800.89;
                $ypoint = 580.28;
            }
        } else {
            @$paper = 'a3'; //addslashes($_POST['paper']);
            @$layout = 'L'; //addslashes($_POST['layout']);
            if ($paper == 'a3') {
                $xpoint = 1050.00;
                $ypoint = 800.89;
            } else {
                $xpoint = 800.89;
                $ypoint = 580.28;
            }
        }
        #start pdf
        include('includes/PDF.php');
        $pdf = &PDF::factory($layout, $paper);
        $pdf->open();
        $pdf->setCompression(true);
        $pdf->addPage();
        $pdf->setFont('Arial', 'I', 8);
        $pdf->text(50, $ypoint, 'Printed On ' . $today = date("d-m-Y H:i:s"));

        #put page header
        $x = 60;
        $y = 74;
        $i = 1;
        $pg = 1;
        $pdf->text($xpoint, $ypoint, 'Page ' . $pg);

        #count unregistered
        $j = 0;
        #count sex
        $fmcount = 0;
        $mcount = 0;
        $fcount = 0;
        #print row 1
        #print NACTE FORM EXAM 0.3
        $pdf->setFont('Arial', 'B', 12);
        $pdf->text(50, $y, 'NACTE FORM EXAM 03');

        #print header for landscape paper layout
        $playout = 'l';
        include '../includes/orgname.php';
        #print row 2
        $pdf->setFillColor('rgb', 0, 0, 0);
        $pdf->setFont('Arial', 'B', 20);
        $pdf->text($x + 90, $y + 20, strtoupper($faculty));
        $pdf->text(50, $y + 40, strtoupper($class));
        #print row 3
        $pdf->text($x + 120, $y + 40, 'PROVISIONAL OVERALL SUMMARY RESULTS');
        #print row 4
        $pdf->setFont('Arial', 'B', 12);
        //$pdf->text(50, $y+60, 'NTA LEVEL 4:');
        $pdf->text($x + 90, $y + 60, strtoupper($progname) . '  Weight: CA - 40%  Weight: SE - 60%');
        #print row 5
        $pdf->text(50, $y + 60, 'Year of study: ' . $year . ' - ' . $sem);
        //$pdf->text($x+300, $y+74, 'Semester: '.$sem);
        //$pdf->text($x+660, $y+60, 'Weight: CA - 40%');
        //$pdf->text($x+778, $y+60, 'Weight: SE - 60%');
        #reset values of x,y
        $x = 50;
        $y = $y + 88;
        #set table header
        #print row 1
        //$pdf->line($x, $y, $xpoint+25, $y); //first horizontal line
        //$pdf->line($x, $y+14, $xpoint+25, $y+14); //second horizontal line
        //$pdf->line($x, $y, $x, $y+14); //first vertical line
        //$pdf->line($xpoint+25, $y, $xpoint+25, $y+14); //last vertical line
        #print row 2
        $pdf->line($x, $y + 28, $xpoint + 25, $y + 28); //second horizontal line
        //$pdf->line($x, $y+14, $x, $y+28); //first vertical line
        //$pdf->line($xpoint+25, $y+14, $xpoint+25, $y+28); //last vertical line
        #print row 3
        //$pdf->line($x+150, $y+42, $xpoint+25, $y+42); //second horizontal line
        $pdf->line($x, $y + 28, $x, $y + 42); //first vertical line
        $pdf->line($xpoint + 25, $y + 28, $xpoint + 25, $y + 42); //last vertical line
        #print row 4
        $pdf->line($x, $y + 56, $xpoint + 25, $y + 56); //second horizontal line
        $pdf->line($x, $y + 42, $x, $y + 56); //first vertical line
        $pdf->line($xpoint + 25, $y + 42, $xpoint + 25, $y + 56); //last vertical line
        #print row 5
        $pdf->line($x, $y + 70, $xpoint + 25, $y + 70); //second horizontal line
        $pdf->line($x, $y + 56, $x, $y + 70); //first vertical line
        $pdf->line($xpoint + 25, $y + 56, $xpoint + 25, $y + 70); //last vertical line

        #print module column
        //$y = $y + 24;
        $pdf->line($x + 480, $y + 28, $x + 480, $y + 70);
        $pdf->line($x + 25, $y + 56, $x + 25, $y + 70);
        $pdf->line($x + 205, $y + 56, $x + 205, $y + 70);
        $pdf->line($x + 235, $y + 56, $x + 235, $y + 70);
        $pdf->line($x + 315, $y + 56, $x + 315, $y + 70);
        $pdf->line($x + 405, $y + 56, $x + 405, $y + 70);
        #print text
        $pdf->setFont('Arial', '', 10);
        $pdf->text($x + 2, $y + 48, 'Exam Components:');
        $pdf->text($x + 2, $y + 68, 'S/No');
        $pdf->text($x + 26, $y + 68, 'NAME');
        $pdf->text($x + 207, $y + 68, 'SEX');
        $pdf->text($x + 237, $y + 68, 'DATE OF BIRTH');
        $pdf->text($x + 327, $y + 68, 'REG. No');
        $pdf->text($x + 407, $y + 68, 'ENTRY YEAR');
        $pdf->text($x + 660, $y + 68, 'Module Code(Module Credits)');
        #reset values of x,y
        $x = 50;
        $y = $y + 56;
        #set colm width
        $clmw = ($totalcolms > 8) ? 85 : 100;
        //control overall space for more than 8 course so as to avoid overlap
        $shiftoverall = ($totalcolms > 8) ? 16 : 0;
        #get column width factor
        $cwf = 200;
        #calculate course clumns widths
        $cw = $clmw * $totalcolms + 100;
        $x = $x + 65;
        $x = $x - 85;
        // control gpa,att,remarks colmn for course more than 8
        $shift2 = ($totalcolms > 8) ? 15 : 0;
        $attshift = ($totalcolms > 8) ? 27 : 0;
        $pdf->line(500 + $x + $cw - $shift2, $y, 500 + $x + $cw - $shift2, $y + 14);
        $pdf->text(500 + $x + $cw - $shift2 + 1, $y + 12, 'GPA');
        $pdf->line(500 + $x + $cw + 30 - $shift2, $y, 500 + $x + $cw + 30 - $shift2, $y + 14);
        $pdf->text(500 + $x + $cw + 32 - $shift2, $y + 12, 'Attendance');
        $pdf->text(500 + $x + $cw + 22 - $shiftoverall, $y - 8, 'OVERALL PERFORMANCE');
        $pdf->line(500 + $x + $cw + 95 - $attshift, $y, 500 + $x + $cw + 95 - $attshift, $y + 14);
        $pdf->text(500 + $x + $cw + 97 - $attshift, $y + 12, 'Remark');
        $pdf->line($xpoint + 25, $y, $xpoint + 25, $y + 14);
        $y = $y + 15;

        #query student list
        $overallpasscount = 0;
        $overallsuppcount = 0;
        $overallinccount = 0;
        $overalldiscocount = 0;
        $overallothcount = 0;
        $ending = 0;
        $qstudent = "SELECT Name, RegNo, Sex, DBirth, EntryYear, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY RegNo";
        $dbstudent = mysqli_query($zalongwa, $qstudent);
        $totalstudent = mysqli_num_rows($dbstudent);
        $i = 1;
        $jheader = 0;
        while ($rowstudent = mysqli_fetch_array($dbstudent)) {
            $name = $rowstudent['Name'];
            $regno = $rowstudent['RegNo'];
            $sex = $rowstudent['Sex'];
            $bdate = $rowstudent['DBirth'];
            $entryyr = "OCTOBER, " . substr($rowstudent['EntryYear'], 0, 4);
            # get all courses for this candidate
            $qcourse = "SELECT DISTINCT course.Units, course.Department, course.StudyLevel, examresult.CourseCode FROM 
								course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
									 WHERE (examresult.RegNo='$regno') AND (examresult.AYear='$year') AND (examresult.Semester='$sem') ORDER BY examresult.CourseCode";
            $dbcourse = mysqli_query($zalongwa, $qcourse);
            $dbcourseUnit = mysqli_query($zalongwa, $qcourse);
            $total_rows = mysqli_num_rows($dbcourse);

            if ($total_rows > 0) {

                #initialise
                $totalunit = 0;
                $unittaken = 0;
                $sgp = 0;
                $totalsgp = 0;
                $gpa = 0;
                $totatattend = 0;
                $key = $regno;
                $x = 50;


                #print student info
                $pdf->setFont('Arial', '', 8);
                $pdf->line($x, $y, $xpoint + 25, $y);
                $pdf->line($x, $y + 28, $xpoint + 25, $y + 28);
                $pdf->line($x, $y, $x, $y + 28);
                $pdf->text($x + 2, $y + 14, $i);
                if ($show_name == 2) {
                    $name = '';
                }
                $pdf->line($x + 25, $y, $x + 25, $y + 28);
                $pdf->text($x + 27, $y + 14, strtoupper($name));
                $pdf->line($x + 205, $y, $x + 205, $y + 28);
                $pdf->text($x + 215, $y + 14, strtoupper($sex));
                $pdf->line($x + 235, $y, $x + 235, $y + 28);
                $pdf->text($x + 240, $y + 14, strtoupper($bdate));
                $pdf->line($x + 315, $y, $x + 315, $y + 28);
                $pdf->text($x + 317, $y + 14, strtoupper($regno));
                $pdf->line($x + 405, $y, $x + 405, $y + 28);
                $pdf->text($x + 407, $y + 14, strtoupper($entryyr));

                #calculate course clumns widths
                $clnspace = $x + 480;
                // control the colm width for more than 8 courses
                $clnspaceextension = ($totalcolms > 8) ? 15 : 0;
                $header = ($totalcolms > 8) ? $x + 465 : $x + 480;
                $shift = ($totalcolms > 8) ? 4 : 0;
                while ($rowcourse = mysqli_fetch_array($dbcourse)) {
                    $stdcourse = $rowcourse['CourseCode'];
                    $unit = $rowcourse['Units'];
                    $pdf->text($clnspace + 20, $y + 12, $stdcourse . ' (' . $unit . ')');
                    $pdf->line($clnspace, $y + 15, $clnspace + $clmw, $y + 15);

                    #partition the course
                    $pdf->line($clnspace, $y, $clnspace, $y + 28);
                    $pdf->line($clnspace + 26 + $shift - $clnspaceextension, $y + 15, $clnspace + 26 + $shift - $clnspaceextension, $y + 28);
                    $pdf->line($clnspace + 50 + $shift - $clnspaceextension, $y + 15, $clnspace + 50 + $shift - $clnspaceextension, $y + 28);
                    $pdf->line($clnspace + 80 - $clnspaceextension, $y + 15, $clnspace + 80 - $clnspaceextension, $y + 28);
                    $pdf->line($clnspace + $clmw, $y, $clnspace + $clmw, $y + 28);

                    #print exam categories headers
                    if ($jheader == 0) {

                        for ($jheader = 0; $jheader < $totalcolms; $jheader++) {
                            #partition the header
                            $startpoint = ($totalcolms > 8) ? 15 : 9;
                            $pdf->text($header + $startpoint, $y - 25, 'CA');
                            $pdf->line($header + 26 + $shift, $y - 42, $header + 26 + $shift, $y - 15);
                            $pdf->text($header + 32, $y - 25, 'SE');
                            $pdf->line($header + 48 + $shift, $y - 42, $header + 48 + $shift, $y - 15);
                            $pdf->text($header + 51, $y - 25, 'TOTAL');
                            $pdf->line($header + 80, $y - 42, $header + 80, $y - 15);
                            $pdf->text($header + 82, $y - 25, 'GRD');
                            $pdf->line($header + 100, $y - 42, $header + 100, $y - 15);
                            $header = $header + $clmw;
                        }

                    }
                    $clnspace = $clnspace + $clmw;
                }
                $jheader = $jheader + 1;
                #reset colm space
                $clmspace = $x + 480;
                $extscore = ($totalcolms > 8) ? 10 : 0;
                while ($row_course = mysqli_fetch_array($dbcourseUnit)) {
                    $course = $row_course['CourseCode'];
                    $unit = $row_course['Units'];
                    $name = $row_course['CourseName'];
                    $coursefaculty = $row_course['Department'];
                    $sn = $sn + 1;
                    $remarks = 'remarks';
                    $grade = '';
                    $RegNo = $key;

                    #include grading scale
                    include 'includes/choose_studylevel.php';

                    if ($supp == '!') {
                        $pdf->setFont('Arial', 'B', 8);
                        $pdf->text($clmspace, $y + 24, $test2score);
                        $pdf->text($clmspace + 31 - $extscore, $y + 24, $aescore);
                        $pdf->text($clmspace + 54, $y + 24, $marks);
                        $pdf->text($clmspace + 85 - $extscore, $y + 24, $grade);
                        if ($grade == 'I') {
                            $totatattend++;
                        }
                        $save[] = $course;
                        //$pdf->text($clmspace+83, $y+24, number_format($sgp/$unit,1,'.',','));						$suptotal
                        $supp = '';
                        $fsup = '!';
                        $pdf->setFont('Arial', '', 8);
                        $suptotal++;
                    } else {
                        $pdf->text($clmspace, $y + 24, $test2score);
                        $pdf->text($clmspace + 31 - $extscore, $y + 24, $aescore);
                        $pdf->text($clmspace + 54, $y + 24, $marks);
                        $pdf->text($clmspace + 85 - $extscore, $y + 24, $grade);
                        if ($grade == 'I') {
                            $totatattend++;
                        }
                        //$pdf->text($clmspace+83, $y+24, number_format($sgp/$unit,1,'.',','));
                    }
                    $pdf->line($clmspace + $clmw, $y, $clmspace + $clmw, $y + 28);
                    $clmspace = $clmspace + $clmw;
                    $ending = ($clmspace > $ending) ? $clmspace : $ending;
                }
                #fill blank space
                $emptycolm = $totalcolms - $total_rows;
                while ($emptycolm > 0) {
                    $pdf->line($clmspace + $clmw, $y, $clmspace + $clmw, $y + 28);
                    $clmspace = $clmspace + $clmw;
                    $emptycolm = $emptycolm - 1;
                    $pdf->line($clnspace, $y + 28, $clnspace + $clmw, $y + 28);
                    $clnspace = $clnspace + $clmw;
                }

                $gunits = $unittaken + $gunits;
                $gpoints = $totalsgp + $gpoints;
                $gpa = @substr($totalsgp / $unittaken, 0, 3);
                #calculate courses column width
                $cw = $clmw * ($totalcolms + 1);
                $x = $x - 60;
                //$pdf->line($x+$cw, $y, $x+$cw, $y+28);	if ($igrade<>'I'){$pdf->text($x+$cw+1, $y+24, $unittaken); }
                //$pdf->line($x+$cw+30, $y, $x+$cw+30, $y+28);	if ($igrade<>'I'){$pdf->text($x+$cw+32, $y+24, ''); }
                //$pdf->line($x+$cw+30, $y, $x+$cw+30, $y+28);	if ($igrade<>'I'){$pdf->text($x+$cw+32, $y+24, $totalsgp); }
                $pdf->line(500 + $x + $cw + 40, $y, 500 + $x + $cw + 40, $y + 28);
                if ($igrade <> 'I') {
                    $pdf->text(500 + $x + $cw + 47, $y + 24, $gpa);
                }

                #get student remarks
                $qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$regno'";
                $dbremarks = mysqli_query($zalongwa, $qremarks);
                $row_remarks = mysqli_fetch_assoc($dbremarks);
                $totalremarks = mysqli_num_rows($dbremarks);
                $studentremarks = $row_remarks['Remark'];
                if (($totalremarks > 0) && ($studentremarks <> '')) {
                    $remark = $studentremarks;
                    if ($remark <> '') {
                        $fsup = '!';
                    }
                } else {
                    if ($year < '2011/2012') {

                        if ($gpa >= 4.4) {
                            $remark = 'PASS';
                            $gpagrade = 'A';
                        } elseif ($gpa >= 3.5) {
                            $remark = 'PASS';
                            $gpagrade = 'B+';
                        } elseif ($gpa >= 2.7) {
                            $remark = 'PASS';
                            $gpagrade = 'B';
                        } elseif ($gpa >= 2.0) {
                            $remark = 'PASS';
                            $gpagrade = 'C';
                        } elseif ($gpa >= 1.8) {
                            $remark = 'PASS';
                            $gpagrade = 'D';
                        } elseif ($gpa >= 1.0) {
                            //$remark = 'DISCO';
                            $remark = 'SUPP';
                            $gpagrade = 'D';
                        } else {
                            $remark = 'SUPP';
                            $gpagrade = 'E';
                        }

                    } else {


                        if ($gpa >= 4.4) {
                            $remark = 'PASS';
                            $gpagrade = 'A';
                        } elseif ($gpa >= 3.5) {
                            $remark = 'PASS';
                            $gpagrade = 'B+';
                        } elseif ($gpa >= 2.7) {
                            $remark = 'PASS';
                            $gpagrade = 'B';
                        } elseif ($gpa >= 2.0) {
                            $remark = 'PASS';
                            $gpagrade = 'C';
                        } /*	elseif ($gpa >= 1.8){
                            $remark = 'PASS';
                            $gpagrade= 'D';
                            }*/
                        elseif ($gpa >= 1.0) {
                            //$remark = 'DISCO';
                            $remark = 'SUPP';
                            $gpagrade = 'D';
                        } else {
                            $remark = 'SUPP';
                            //$remark = 'DISCO';
                            $gpagrade = 'E';
                        }

                    }
                }
                $pdf->line(500 + $x + $cw + 70, $y, 500 + $x + $cw + 70, $y + 28);
                $pdf->line($xpoint + 25, $y, $xpoint + 25, $y + 28);

                $pdf->line(500 + $x + $cw + 70, $y + 28, 500 + $x + $cw + 135, $y + 28);
                $shift3 = ($totalcolms > 8) ? 12 : 0;
                $pdf->line(500 + $x + $cw + 135 - $shift3, $y, 500 + $x + $cw + 135 - $shift3, $y + 28);

                #check attendance
                if ($remark == 'FAIL') {
                    $attend = '';
                } elseif ($remark == 'ABSC') {
                    $attend = 20;
                } else {
                    if ($totatattend > 0 && $totatattend < 2) {
                        $attend = 70;
                    } else if ($totatattend > 0 && $totatattend < 3) {
                        $attend = 60;

                    } else if ($totatattend > 0 && $totatattend < 4) {
                        $attend = 50;

                    } else if ($totatattend == 0) {
                        $attend = 80;
                    } else {
                        $attend = 80;
                    }
                }
                $pdf->text(500 + $x + $cw + 102, $y + 23, $attend);

                #check failed exam
                /*if ($fexm=='#'){
                $pdf->text($x+$cw+137, $y+26, $fexm.' = Fail Exam'); $fexm = ''; $remark ='';
                }*/
                #check failed exam
                if (($totalremarks > 0) && ($studentremarks <> '')) {
                    $pdf->text(500 + $x + $cw + 137, $y + 14, $remark);
                    $remark = '';
                    $fexm = '';
                    $fcwk = '';
                    $fsup = '';
                    $igrade = '';
                    $egrade = '';
                    $overallothcount = $overallothcount + 1;
                    $save = '';
                } else {
                    #check for supp and inco
                    if ($igrade <> 'I') {
                        //$pdf->text($x+$cw+77, $y+24, '  '.$gpagrade);
                    }
                    if ($fsup == '!') {

                        if ($remark == 'DISCO') {
                            $pdf->text(500 + $x + $cw + 137, $y + 12, $remark);
                            $save = '';
                            $overalldiscocount = $overalldiscocount + 1;
                        } else {
                            $pdf->text(500 + $x + $cw + 137, $y + 12, 'SUPP');
                            $fsup = '';
                            $overallsuppcount = $overallsuppcount + 1;

                            $ong = 0;
                            $zero = 0;
                            if ($totalcolms > 7) {
                                $savesize = sizeof($save);

                                #control the display of supp courses
                                if ($savesize > 3) {
                                    $counter = 0;
                                    foreach ($save as $val) {
                                        if ($counter < 3) {
                                            #same height as the keyword SUPP
                                            $pdf->text(500 + $x + $cw + $ong + 180, $y + 12, $val . ', ');
                                            $ong += 42;
                                        } else {
                                            #below the keyword SUPP
                                            $pdf->text(500 + $x + $cw + $zero + 137, $y + 24, $val . ', ');
                                            $zero += 42;
                                        }
                                        $counter = $counter + 1;
                                    }
                                } else {
                                    foreach ($save as $val) {
                                        $pdf->text(500 + $x + $cw + $ong + 137, $y + 24, $val . ', ');
                                        $ong += 42;
                                    }
                                }
                                $save = '';
                            } #display of supp courses
                            else {
                                foreach ($save as $val) {
                                    $pdf->text(500 + $x + $cw + $ong + 137, $y + 24, $val . ', ');
                                    $ong += 45;
                                }
                            }
                            $save = '';
                        }
                        /*
                        $k =0;
                        $chas=mysqli_query($zalongwa, "SELECT DISTINCT course.Units, course.Department, course.StudyLevel,
                                    examresult.CourseCode FROM course INNER JOIN examresult
                                    ON (course.CourseCode = examresult.CourseCode)
                                    WHERE (examresult.RegNo='$regno') AND (examresult.AYear='$year')
                                    AND (examresult.Semester='$sem') ORDER BY examresult.CourseCode");

                        while($chas = mysqli_fetch_array($chas)){
                            $course= $chas['CourseCode'];
                            $unit = $chas['Units'];
                            $name = $chas['CourseName'];
                            $coursefaculty = $chas['Department'];
                            $remarks = 'remarks';
                            $grade='';
                            $RegNo = $key;

                            #include grading scale
                            include 'includes/choose_studylevel.php';

                            if(($supp=='!')&&($marks>0)){
                                    $pdf->text($x+$k+$cw+137, $y+24, ','.$course);
                                    $k =$k+45;
                                }
                            #empty option value
                            $coption='';
                        }*/

                    } elseif (($igrade <> 'I') || ($fsup <> '!')) {
                        $pdf->text(500 + $x + $cw + 137, $y + 24, $remark);
                        $overallpasscount = $overallpasscount + 1;
                        $save = '';
                    }

                    if ($igrade == 'I') {
                        $pdf->text(500 + $x + $cw, $y + 24, 'INC.');
                        $igrade = '';
                        $overallinccount = $overallinccount + 1;
                        $save = '';
                    }


                    if ($fcwk == '*') {
                        $pdf->text($x + $cw + 137, $y + 38, $fcwk . ' = Fail CWK');
                        $fcwk = '';
                        $remark = '';
                    }

                    if ($fsup == '!') {
                        $pdf->text($x + $cw + 137, $y + 42, $fsup . ' = Supp');
                        $fsup = '';
                        $remark = '';
                    }
                    if ($igrade == 'I') {
                        $pdf->text($x + $cw + 177, $y + 42, $igrade . ' = Inc.');
                        $igrade = '';
                        $remark = '';
                    }
                    if ($egrade == '*') {
                        $pdf->text($x + $cw + 177, $y + 26, $egrade . ' = C/Repeat.');
                        $egrade = '';
                        $remark = '';
                    }
                }
                $i = $i + 1;
                $y = $y + 28;
                if ($y >= $ypoint - 50) {
                    #start new page
                    $pdf->addPage();
                    $pdf->setFont('Arial', 'I', 8);
                    $pdf->text(50, $ypoint, 'Printed On ' . $today = date("d-m-Y H:i:s"));

                    $x = 50;
                    $y = 50;
                    $pg = $pg + 1;
                    $pdf->text($xpoint, $ypoint, 'Page ' . $pg);

                    #count unregistered
                    $j = 0;
                    #count sex
                    $fmcount = 0;
                    $mcount = 0;
                    $fcount = 0;
                    #set table header
                    #print row 1
                    //$pdf->line($x, $y, $xpoint+25, $y); //first horizontal line
                    //$pdf->line($x, $y+14, $xpoint+25, $y+14); //second horizontal line
                    //$pdf->line($x, $y, $x, $y+14); //first vertical line
                    //$pdf->line($xpoint+25, $y, $xpoint+25, $y+14); //last vertical line
                    #print row 2
                    $pdf->line($x, $y + 28, $xpoint + 25, $y + 28); //second horizontal line
                    //$pdf->line($x, $y+14, $x, $y+28); //first vertical line
                    //$pdf->line($xpoint+25, $y+14, $xpoint+25, $y+28); //last vertical line
                    #print row 3
                    //$pdf->line($x+150, $y+42, $xpoint+25, $y+42); //second horizontal line
                    $pdf->line($x, $y + 28, $x, $y + 42); //first vertical line
                    $pdf->line($xpoint + 25, $y + 28, $xpoint + 25, $y + 42); //last vertical line
                    #print row 4
                    $pdf->line($x, $y + 56, $xpoint + 25, $y + 56); //second horizontal line
                    $pdf->line($x, $y + 42, $x, $y + 56); //first vertical line
                    $pdf->line($xpoint + 25, $y + 42, $xpoint + 25, $y + 56); //last vertical line
                    #print row 5
                    $pdf->line($x, $y + 70, $xpoint + 25, $y + 70); //second horizontal line
                    $pdf->line($x, $y + 56, $x, $y + 70); //first vertical line
                    $pdf->line($xpoint + 25, $y + 56, $xpoint + 25, $y + 70); //last vertical line

                    #print module column
                    //$y = $y + 24;
                    $pdf->line($x + 480, $y + 28, $x + 480, $y + 70);
                    $pdf->line($x + 25, $y + 56, $x + 25, $y + 70);
                    $pdf->line($x + 205, $y + 56, $x + 205, $y + 70);
                    $pdf->line($x + 235, $y + 56, $x + 235, $y + 70);
                    $pdf->line($x + 315, $y + 56, $x + 315, $y + 70);
                    $pdf->line($x + 405, $y + 56, $x + 405, $y + 70);
                    #print text
                    $pdf->setFont('Arial', '', 10);
                    //$pdf->text($x+2, $y+12, 'Module Credits:');
                    //$pdf->text($x+2, $y+24, 'Module Code:');
                    $pdf->text($x + 2, $y + 48, 'Exam Components:');
                    $pdf->text($x + 2, $y + 68, 'S/No');
                    $pdf->text($x + 26, $y + 68, 'NAME');
                    $pdf->text($x + 207, $y + 68, 'SEX');
                    $pdf->text($x + 237, $y + 68, 'DATE OF BIRTH');
                    $pdf->text($x + 327, $y + 68, 'REG. No');
                    $pdf->text($x + 407, $y + 68, 'ENTRY YEAR');
                    $pdf->text($x + 660, $y + 68, 'Module Code(Module Credits)');

                    #reset values of x,y
                    $x = 50;
                    $y = $y + 56;
                    #set colm width
                    $clmw = ($totalcolms > 8) ? 85 : 100;
                    $shiftoverall = ($totalcolms > 8) ? 16 : 0;
                    #get column width factor
                    $cwf = 200;
                    #calculate course clumns widths
                    $cw = $clmw * $totalcolms + 100;
                    $x = $x + 65;
                    $x = $x - 85;
                    // control gpa,att,remarks colmn for course more than 8
                    $shift2 = ($totalcolms > 8) ? 15 : 0;
                    $attshift = ($totalcolms > 8) ? 27 : 0;
                    $pdf->line(500 + $x + $cw - $shift2, $y - 28, 500 + $x + $cw - $shift2, $y + 14);
                    $pdf->text(500 + $x + $cw + 1 - $shift2, $y + 12, 'GPA');
                    $pdf->line(500 + $x + $cw + 30 - $shift2, $y, 500 + $x + $cw + 30 - $shift2, $y + 14);
                    $pdf->text(500 + $x + $cw + 32 - $shift2, $y + 12, 'Attendance');
                    $pdf->text(500 + $x + $cw + 22 - $shiftoverall, $y - 8, 'OVERALL PERFORMANCE');
                    //$pdf->line($x+$cw+60, $y, $x+$cw+60, $y+14);	$pdf->text($x+$cw+67, $y+12, 'Remark');
                    $pdf->line(500 + $x + $cw + 95 - $attshift, $y, 500 + $x + $cw + 95 - $attshift, $y + 14);
                    $pdf->text(500 + $x + $cw + 97 - $attshift, $y + 12, 'Remark');
                    $pdf->line($xpoint + 25, $y, $xpoint + 25, $y + 14);
                    $y = $y + 15;

                    #print exam categories headers
                    /*
                    if ($jheader==0){
                        for ($jheader =0; $jheader < $totalcolms; $jheader++){
                             #partition the header
                             $pdf->setFont('Arial', '', 8);
                             $pdf->text($header+9, $y-25, 'CA');
                             //$pdf->text($header+9, $y-17, 'X/40');
                             $pdf->line($header+26, $y-42, $header+26, $y-15);
                             $pdf->text($header+30, $y-25, 'SE');
                             //$pdf->line($header+6, $y-30, $header+60, $y-30);
                             //$pdf->text($header+28, $y-17, 'X/60');
                             $pdf->line($header+46, $y-42, $header+46, $y-15); $pdf->text($header+49, $y-25, 'TOTAL');
                             //$pdf->text($header+42, $y-17, 'X/100');
                             //$pdf->line($header+60, $y-42, $header+60, $y-15); $pdf->text($header+62, $y-17, 'GRD');
                             $pdf->line($header+80, $y-42, $header+80, $y-15); $pdf->text($header+82, $y-25, 'GRD');
                             $pdf->line($header+100, $y-42, $header+100, $y-15);
                             $header = $header+$clmw;
                            $pdf->setFont('Arial', '', 10);
                        }
                  }
                  $jheader+=1;
                  */

                    #print exam categories headers
                    if ($jheader == 0) {

                        for ($jheader = 0; $jheader < $totalcolms; $jheader++) {
                            #partition the header
                            $startpoint = ($totalcolms > 8) ? 15 : 9;
                            $pdf->text($header + $startpoint, $y - 25, 'CA');
                            $pdf->line($header + 26, $y - 42, $header + 26, $y - 15);
                            $pdf->text($header + 32, $y - 25, 'SE');
                            $pdf->line($header + 48, $y - 42, $header + 48, $y - 15);
                            $pdf->text($header + 51, $y - 25, 'TOTAL');
                            $pdf->line($header + 80, $y - 42, $header + 80, $y - 15);
                            $pdf->text($header + 82, $y - 25, 'GRD');
                            $pdf->line($header + 100, $y - 42, $header + 100, $y - 15);
                            $header = $header + $clmw;
                        }

                    }

                    $jheader = $jheader + 1;

                }
            } //ends if $total_rows
        }//ends $rowstudent loop

#start new page for the keys
        $space = $ypoint - 50 - $y;
        $yind = $y + 20;
        if ($space < 70) {
            $pdf->addPage();

            $x = 50;
            $y = 50;
            $pg = $pg + 1;
            $tpg = $pg;
            $pdf->setFont('Arial', 'I', 8);
            $pdf->text(50, $ypoint - 50, 'Printed On ' . $today = date("d-m-Y H:i:s"));
            $pg = $pg + 1;
            $pdf->text($xpoint, $ypoint - 50, 'Page ' . $pg);
            $yind = $y;
        }
        $pdf->text(50, $yind, 'PASS count = ' . $overallpasscount . ' SUPP Count = ' . $overallsuppcount . ' DISCO Count = ' . $overalldiscocount . ' INC Count =' . $overallinccount . ' OTHER REMARKS Count =' . $overallothcount);

        $y = $yind;
        $x = 55;
        $pdf->setFont('Arial', 'B', 12);
        $pdf->text($x, $y + 50, 'KEY FOR MODULE CODES');
        $pdf->setFont('Arial', '', 12);

        $chass = mysqli_query($zalongwa, "SELECT DISTINCT CourseCode FROM examresult WHERE ($whereclause) AND (AYear='$year') 
						AND (Semester='$sem') ORDER BY CourseCode");
        $y = $yind + 70;
        $keyno = 1;

        while ($chas = mysqli_fetch_array($chass)) {
            $code = $chas['CourseCode'];
            $qcourse2 = mysqli_query($zalongwa, "SELECT CourseName FROM course WHERE CourseCode='$code'");
            $fqname = mysqli_fetch_assoc($qcourse2);
            $pdf->text($x, $y, $keyno . '. ' . $code . ' - ' . $fqname['CourseName']);
            $y += 20;
            $keyno = $keyno + 1;
        }
        $pdf->setFont('Arial', 'B', 10);
        $y = $y + 5;
        $pdf->text($x + 200, $y, 'NAME OF HEAD OF DEPARTMENT ..................................................................');
        $pdf->text($x + 200, $y + 20, 'SIGNATURE OF HEAD OF DEPARTMENT ........................................................');
        $pdf->text($x + 200, $y + 35, 'DATE ...............................................');
        $pdf->text(800, $y, 'NAME OF INSTITUTE\'S EXAMINATIONS OFFICER ..................................................................');
        $pdf->text(800, $y + 20, 'SIGNATURE OF INSTITUTE\'S EXAMINATIONS OFFICER ...............................................');
        $pdf->text(800, $y + 35, 'DATE ...............................................');

        /*
       $pdf->text(450, $yind, 'Signature of The Dean:  ���������     ');
       $pdf->text(450, $yind+12, 'Date: ���������������������       ');
        $pdf->text(450, $yind+24, 'Signature of the Chairperson of the Senate:  ����������');
       $pdf->text(450, $yind+36, 'Date: ����������������������');

       $pdf->setFont('Arial', 'I', 9);
       $pdf->text(190.28, $yind, '          ######## END OF EXAM RESULTS ########');

       #table 1
        include 'includes/pointskey.php';
       $x=50;
       $y= $yind + 44;
       #table 1
        include 'includes/gradescale.php';
       */

        #output file
        $filename = preg_replace("[[:space:]]+", "", $progname);
        $pdf->output($filename . '.pdf');
    }
}//end of print pdf
?>
<?php
#get connected to the database and verfy current session
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

# include the header
global $szSection, $szSubSection;
$szSection = 'Examination';
$szSubSection = 'NTA Semester Rpt';
$szTitle = 'Printing Examination Results Report';

$editFormAction = $_SERVER['PHP_SELF'];


$query_studentlist = "SELECT RegNo, Name, ProgrammeofStudy FROM student ORDER BY ProgrammeofStudy  ASC";
$studentlist = mysqli_query($zalongwa, $query_studentlist) or die(mysqli_error($zalongwa));
$row_studentlist = mysqli_fetch_assoc($studentlist);
$totalRows_studentlist = mysqli_num_rows($studentlist);


$query_degree = "SELECT ProgrammeCode, ProgrammeName FROM programme ORDER BY ProgrammeName ASC";
$degree = mysqli_query($zalongwa, $query_degree) or die(mysqli_error($zalongwa));
$row_degree = mysqli_fetch_assoc($degree);
$totalRows_degree = mysqli_num_rows($degree);


$query_ayear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$ayear = mysqli_query($zalongwa, $query_ayear) or die(mysqli_error($zalongwa));
$row_ayear = mysqli_fetch_assoc($ayear);
$totalRows_ayear = mysqli_num_rows($ayear);


$query_sem = "SELECT Semester FROM terms ORDER BY Semester LIMIT 6";
$sem = mysqli_query($zalongwa, $query_sem) or die(mysqli_error($zalongwa));
$row_sem = mysqli_fetch_assoc($sem);
$totalRows_sem = mysqli_num_rows($sem);


$query_dept = "SELECT Faculty, DeptName FROM department ORDER BY DeptName, Faculty ASC";
$dept = mysqli_query($zalongwa, $query_dept) or die(mysqli_error($zalongwa));
$row_dept = mysqli_fetch_assoc($dept);
$totalRows_dept = mysqli_num_rows($dept);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="./css/breadcrumb.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /*background-color: #009688;*/
        }

        .card {
            /*background-color: #324359;*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /*color: white;*/
            padding: 0px;
            border-radius: 0px !important;
        }

        .row {
            margin-top: 20px;
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">

            <?php

            if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
                ?>
                <style type="text/css">
                    <!--
                    .style1 {
                        color: #FFFFFF
                    }

                    -->
                </style>

                <h4 align="center">

                    <?php
                    $prog = $_POST['degree'];
                    $cohotyear = $_POST['cohot'];
                    $ayear = $_POST['ayear'];
                    $qprog = "SELECT ProgrammeCode, Title FROM programme WHERE ProgrammeCode='$prog'";
                    $dbprog = mysqli_query($zalongwa, $qprog);
                    $row_prog = mysqli_fetch_array($dbprog);
                    $progname = $row_prog['Title'];
                    $qyear = "SELECT AYear FROM academicyear WHERE AYear='$cohotyear'";
                    $dbyear = mysqli_query($zalongwa, $qyear);
                    $row_year = mysqli_fetch_array($dbyear);
                    $year = $row_year['AYear'];
                    echo $progname;
                    echo " - " . $year;
                    ?>

                    <br>
                </h4>
                <?php

                @$checkdegree = addslashes($_POST['checkdegree']);
                @$checkyear = addslashes($_POST['checkyear']);
                @$checksem = addslashes($_POST['checksem']);
                $checkcohot = addslashes($_POST['checkcohot']);

                $c = 0;

                if (($checkdegree == 'on') && ($checkyear == 'on') && ($checksem == 'on') && ($checkcohot == 'on')) {
                    echo "<br><br>Examination Results for Academic Year - " . $ayear;

                    $deg = addslashes($_POST['degree']);
                    $year = addslashes($_POST['ayear']);
                    $cohot = addslashes($_POST['cohot']);
                    $sem = addslashes($_POST['sem']);
//query student list
                    $qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY RegNo";
                    $dbstudent = mysqli_query($zalongwa, $qstudent);
                    $totalstudent = mysqli_num_rows($dbstudent);
                    $i = 1;
                    while ($rowstudent = mysqli_fetch_array($dbstudent)) {
                        $name = $rowstudent['Name'];
                        $regno = $rowstudent['RegNo'];
                        $sex = $rowstudent['Sex'];

                        # get all courses for this candidate
                        $qcourse = "SELECT DISTINCT course.Units, course.Department, course.StudyLevel, examresult.CourseCode FROM 
						course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
							 WHERE 
									(RegNo='$regno') AND 
									(AYear='$year') AND 
									(Semester = '$sem')";
                        $dbcourse = mysqli_query($zalongwa, $qcourse);
                        $dbcourseUnit = mysqli_query($zalongwa, $qcourse);
                        $total_rows = mysqli_num_rows($dbcourse);

                        if ($total_rows > 0) {

                            #initialise
                            $totalunit = 0;
                            $unittaken = 0;
                            $sgp = 0;
                            $totalsgp = 0;
                            $gpa = 0;
                            $key = $regno;
                            ?>

                            <table width="100%" height="100%" border="1" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="20" rowspan="2" nowrap scope="col">
                                        <div align="left"></div> <?php echo $i ?></td>
                                    <td width="160" rowspan="2" nowrap
                                        scope="col"><?php echo $name . ": " . $regno; ?> </td>
                                    <td width="13" rowspan="2" nowrap>
                                        <div align="center"><?php echo $sex ?></div>
                                    </td>
                                    <?php while ($rowcourse = mysqli_fetch_array($dbcourse)) { ?>
                                        <td>
                                            <div align="center"><?php echo $rowcourse['CourseCode']; ?></div>
                                        </td>
                                    <?php } ?>
                                    <td>
                                        <div align="center">Units</div>
                                    </td>
                                    <td>
                                        <div align="center">Points</div>
                                    </td>
                                    <td>
                                        <div align="center">GPA</div>
                                    </td>
                                    <td>
                                        <div align="center">Grade</div>
                                    </td>
                                    <td scope="col">Remarks</td>
                                </tr>
                                <tr>
                                    <?php while ($row_course = mysqli_fetch_array($dbcourseUnit)) {
                                        $course = $row_course['CourseCode'];
                                        $unit = $row_course['Units'];
                                        $name = $row_course['CourseName'];
                                        $coursefaculty = $row_course['Department'];
                                        $sn = $sn + 1;
                                        $remarks = 'remarks';

                                        $RegNo = $key;

                                        #include grading scale
                                        include 'includes/choose_studylevel.php';
                                        ?>

                                        <td>
                                            <div align="center"><?php echo $grade; ?></div>
                                        </td>
                                    <?php } //ends while row_course loop

                                    ?>
                                    <td>
                                        <div align="center"><?php $gunits = $unittaken + $gunits;
                                            echo $unittaken; ?></div>
                                    </td>
                                    <td>
                                        <div align="center"><?php $gpoints = $totalsgp + $gpoints;
                                            echo $totalsgp; ?></div>
                                    </td>
                                    <td>
                                        <div align="center"><?php $gpa = @substr($totalsgp / $unittaken, 0, 3);
                                            echo $gpa ?></div>
                                    </td>
                                    <td>
                                        <div align="center"><?php #get student remarks
                                            $qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$regno'";
                                            $dbremarks = mysqli_query($zalongwa, $qremarks);
                                            $row_remarks = mysqli_fetch_assoc($dbremarks);
                                            $totalremarks = mysqli_num_rows($dbremarks);
                                            $studentremarks = $row_remarks['Remark'];
                                            if (($totalremarks > 0) && ($studentremarks <> '')) {
                                                $remark = $studentremarks;
                                            } else {

                                                if ($gpa >= 4.4) {
                                                    $remark = 'PASS';
                                                    echo 'A';
                                                } elseif ($gpa >= 3.5) {
                                                    $remark = 'PASS';
                                                    echo 'B+';
                                                } elseif ($gpa >= 2.7) {
                                                    $remark = 'PASS';
                                                    echo 'B';
                                                } elseif ($gpa >= 2.0) {
                                                    $remark = 'PASS';
                                                    echo 'C';
                                                } elseif ($gpa >= 1.0) {
                                                    $remark = 'FAIL';
                                                    echo 'D';
                                                } else {
                                                    $remark = 'FAIL';
                                                    echo 'E';
                                                }
                                            }
                                            ?></div>
                                    </td>
                                    <td>
                                        <div align="left">
                                            <?php echo $remark;
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <?php $i = $i + 1;
                        } //ends if $total_rows
                    }//ends $rowstudent loop

                } elseif (($checkdegree == 'on') && ($checkcohot == 'on')) {
//query student list

                    $deg = $_POST['degree'];
                    $year = $_POST['ayear'];
                    $cohot = $_POST['cohot'];
                    $dept = $_POST['dept'];
                    echo "<br><br>Examination Results for Academic Year - " . $year;
//query student list
                    $qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY RegNo";
                    $dbstudent = mysqli_query($zalongwa, $qstudent);
                    $totalstudent = mysqli_num_rows($dbstudent);
                    $i = 1;
                    while ($rowstudent = mysqli_fetch_array($dbstudent)) {
                        $name = $rowstudent['Name'];
                        $regno = $rowstudent['RegNo'];
                        $sex = $rowstudent['Sex'];

                        # get all courses for this candidate
                        $qcourse = "SELECT DISTINCT course.Units, course.Department, course.StudyLevel, examresult.CourseCode FROM 
						course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
							 WHERE 
									(RegNo='$regno') AND 
									(AYear='$year') ";
                        $dbcourse = mysqli_query($zalongwa, $qcourse) or die("No Exam Results for the Candidate - $key ");
                        $dbcourseUnit = mysqli_query($zalongwa, $qcourse);
                        $total_rows = mysqli_num_rows($dbcourse);

                        if ($total_rows > 0) {

                            #initialise
                            $totalunit = 0;
                            $unittaken = 0;
                            $sgp = 0;
                            $totalsgp = 0;
                            $gpa = 0;
                            $key = $regno;
                            ?>

                            <table width="100%" height="100%" border="1" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="20" rowspan="2" nowrap scope="col">
                                        <div align="left"></div> <?php echo $i ?></td>
                                    <td width="160" rowspan="2" nowrap
                                        scope="col"><?php echo $name . ": " . $regno; ?> </td>
                                    <td width="13" rowspan="2" nowrap>
                                        <div align="center"><?php echo $sex ?></div>
                                    </td>
                                    <?php while ($rowcourse = mysqli_fetch_array($dbcourse)) { ?>
                                        <td>
                                            <div align="center"><?php echo $rowcourse['CourseCode']; ?></div>
                                        </td>
                                    <?php } ?>
                                    <td>
                                        <div align="center">Units</div>
                                    </td>
                                    <td>
                                        <div align="center">Points</div>
                                    </td>
                                    <td>
                                        <div align="center">GPA</div>
                                    </td>
                                    <td>
                                        <div align="center">Grade</div>
                                    </td>
                                    <td scope="col">Remarks</td>
                                </tr>
                                <tr>
                                    <?php while ($row_course = mysqli_fetch_array($dbcourseUnit)) {
                                        $course = $row_course['CourseCode'];
                                        $unit = $row_course['Units'];
                                        $name = $row_course['CourseName'];
                                        $coursefaculty = $row_course['Department'];
                                        $sn = $sn + 1;
                                        $remarks = 'remarks';

                                        $RegNo = $key;
                                        #insert grading results
                                        include 'includes/choose_studylevel.php';
                                        ?>

                                        <td>
                                            <div align="center"><?php echo $grade; ?></div>
                                        </td>
                                    <?php } //ends while row_course loop

                                    ?>
                                    <td>
                                        <div align="center"><?php $gunits = $unittaken + $gunits;
                                            echo $unittaken; ?></div>
                                    </td>
                                    <td>
                                        <div align="center"><?php $gpoints = $totalsgp + $gpoints;
                                            echo $totalsgp; ?></div>
                                    </td>
                                    <td>
                                        <div align="center"><?php $gpa = @substr($totalsgp / $unittaken, 0, 3);
                                            echo $gpa; ?></div>
                                    </td>
                                    <td>
                                        <div align="center"><?php #get student remarks
                                            $qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$regno'";
                                            $dbremarks = mysqli_query($zalongwa, $qremarks);
                                            $row_remarks = mysqli_fetch_assoc($dbremarks);
                                            $totalremarks = mysqli_num_rows($dbremarks);
                                            $studentremarks = $row_remarks['Remark'];
                                            if (($totalremarks > 0) && ($studentremarks <> '')) {
                                                $remark = $studentremarks;
                                            } else {

                                                if ($gpa >= 4.4) {
                                                    $remark = 'PASS';
                                                    echo 'A';
                                                } elseif ($gpa >= 3.5) {
                                                    $remark = 'PASS';
                                                    echo 'B+';
                                                } elseif ($gpa >= 2.7) {
                                                    $remark = 'PASS';
                                                    echo 'B';
                                                } elseif ($gpa >= 2.0) {
                                                    $remark = 'PASS';
                                                    echo 'C';
                                                } elseif ($gpa >= 1.0) {
                                                    $remark = 'FAIL';
                                                    echo 'D';
                                                } else {
                                                    $remark = 'FAIL';
                                                    echo 'E';
                                                }
                                            }
                                            ?></div>
                                    </td>
                                    <td>
                                        <div align="left">
                                            <?php echo $remark;
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <?php $i = $i + 1;
                        } //ends if $total_rows
                    }//ends $rowstudent loop

                } elseif ($checkcohot == 'on') {

                    $deg = addslashes($_POST['degree']);
                    $year = addslashes($_POST['ayear']);
                    $cohot = addslashes($_POST['cohot']);
                    echo "<br><br>Examination Results for Academic Year - " . $year;

//query student list
                    $qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE EntryYear = '$cohot' ORDER BY RegNo";
                    $dbstudent = mysqli_query($zalongwa, $qstudent);
                    $totalstudent = mysqli_num_rows($dbstudent);
                    $i = 1;
                    while ($rowstudent = mysqli_fetch_array($dbstudent)) {
                        $name = $rowstudent['Name'];
                        $regno = $rowstudent['RegNo'];
                        $sex = $rowstudent['Sex'];

                        # get all courses for this candidate
                        $qcourse = "SELECT DISTINCT course.Units, course.Department, course.StudyLevel, examresult.CourseCode FROM 
						course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
							 WHERE 
									(RegNo='$regno') AND
									(course.Programme = '$deg') 
							";
                        $dbcourse = mysqli_query($zalongwa, $qcourse) or die("No Exam Results for the Candidate - $key ");
                        $dbcourseUnit = mysqli_query($zalongwa, $qcourse);
                        $total_rows = mysqli_num_rows($dbcourse);

                        if ($total_rows > 0) {

                            #initialise
                            $totalunit = 0;
                            $unittaken = 0;
                            $sgp = 0;
                            $totalsgp = 0;
                            $gpa = 0;
                            $key = $regno;
                            ?>

                            <table width="100%" height="100%" border="1" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="20" rowspan="2" nowrap scope="col">
                                        <div align="left"></div> <?php echo $i ?></td>
                                    <td width="160" rowspan="2" nowrap
                                        scope="col"><?php echo $name . ": " . $regno; ?> </td>
                                    <td width="13" rowspan="2" nowrap>
                                        <div align="center"><?php echo $sex ?></div>
                                    </td>
                                    <?php while ($rowcourse = mysqli_fetch_array($dbcourse)) { ?>
                                        <td>
                                            <div align="center"><?php echo $rowcourse['CourseCode']; ?></div>
                                        </td>
                                    <?php } ?>
                                    <td>
                                        <div align="center">Units</div>
                                    </td>
                                    <td>
                                        <div align="center">Points</div>
                                    </td>
                                    <td>
                                        <div align="center">GPA</div>
                                    </td>
                                    <td>
                                        <div align="center">Grade</div>
                                    </td>
                                    <td scope="col">Remarks</td>
                                </tr>
                                <tr>
                                    <?php while ($row_course = mysqli_fetch_array($dbcourseUnit)) {
                                        $course = $row_course['CourseCode'];
                                        $unit = $row_course['Units'];
                                        $name = $row_course['CourseName'];
                                        $coursefaculty = $row_course['Department'];
                                        $sn = $sn + 1;
                                        $remarks = 'remarks';

                                        $RegNo = $key;
                                        #insert grading results
                                        include 'includes/choose_studylevel.php';
                                        ?>

                                        <td>
                                            <div align="center"><?php echo $grade; ?></div>
                                        </td>
                                    <?php } //ends while row_course loop

                                    ?>
                                    <td>
                                        <div align="center"><?php $gunits = $unittaken + $gunits;
                                            echo $unittaken; ?></div>
                                    </td>
                                    <td>
                                        <div align="center"><?php $gpoints = $totalsgp + $gpoints;
                                            echo $totalsgp; ?></div>
                                    </td>
                                    <td>
                                        <div align="center"><?php $gpa = @substr($totalsgp / $unittaken, 0, 3);
                                            echo $gpa; ?></div>
                                    </td>
                                    <td>
                                        <div align="center"><?php #get student remarks
                                            $qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$regno'";
                                            $dbremarks = mysqli_query($zalongwa, $qremarks);
                                            $row_remarks = mysqli_fetch_assoc($dbremarks);
                                            $totalremarks = mysqli_num_rows($dbremarks);
                                            $studentremarks = $row_remarks['Remark'];
                                            if (($totalremarks > 0) && ($studentremarks <> '')) {
                                                $remark = $studentremarks;
                                            } else {

                                                if ($gpa >= 4.4) {
                                                    $remark = 'PASS';
                                                    echo 'A';
                                                } elseif ($gpa >= 3.5) {
                                                    $remark = 'PASS';
                                                    echo 'B+';
                                                } elseif ($gpa >= 2.7) {
                                                    $remark = 'PASS';
                                                    echo 'B';
                                                } elseif ($gpa >= 2.0) {
                                                    $remark = 'PASS';
                                                    echo 'C';
                                                } elseif ($gpa >= 1.0) {
                                                    $remark = 'FAIL';
                                                    echo 'D';
                                                } else {
                                                    $remark = 'FAIL';
                                                    echo 'E';
                                                }
                                            }
                                            ?></div>
                                    </td>
                                    <td>
                                        <div align="left">
                                            <?php echo $remark;
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <?php $i = $i + 1;
                        } //ends if $total_rows
                    }//ends $rowstudent loop
                }
            } else {
                ?>
                <p>
                    If you want to filter the results by criteria, tick the corresponding check box first
                    then select appropriately.
                </p>
                <div class="card">
                    <h3 class="card-header">
                        <?php echo $szTitle; ?></h3>
                    <div class="card-block">
                        <form name="form1" method="post" action="<?php echo $editFormAction ?>">

                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><input name="checkdegree"
                                                                                  type="checkbox"
                                                                                  id="checkdegree"
                                                                                  value="on"
                                                                                  checked>&nbsp;&nbsp;Degree Programme:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="degree" id="degree">
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_degree['ProgrammeCode'] ?>"><?php echo $row_degree['ProgrammeName'] ?></option>
                                                <?php
                                            } while ($row_degree = mysqli_fetch_assoc($degree));
                                            $rows = mysqli_num_rows($degree);
                                            if ($rows > 0) {
                                                mysqli_data_seek($degree, 0);
                                                $row_degree = mysqli_fetch_assoc($degree);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><input name="checkcohot" type="checkbox"
                                                                                  id="checkcohot" value="on" checked>&nbsp;&nbsp;Cohort
                                        of the Year:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="cohot" id="cohot">
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_ayear['AYear'] ?>"><?php echo $row_ayear['AYear'] ?></option>
                                                <?php
                                            } while ($row_ayear = mysqli_fetch_assoc($ayear));
                                            $rows = mysqli_num_rows($ayear);
                                            if ($rows > 0) {
                                                mysqli_data_seek($ayear, 0);
                                                $row_ayear = mysqli_fetch_assoc($ayear);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><input name="checkyear" type="checkbox"
                                                                                  id="checkyear" value="on" checked>&nbsp;&nbsp;Results
                                        of the Year:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="ayear" id="ayear">
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_ayear['AYear'] ?>"><?php echo $row_ayear['AYear'] ?></option>
                                                <?php
                                            } while ($row_ayear = mysqli_fetch_assoc($ayear));
                                            $rows = mysqli_num_rows($ayear);
                                            if ($rows > 0) {
                                                mysqli_data_seek($ayear, 0);
                                                $row_ayear = mysqli_fetch_assoc($ayear);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><input name="checksem" type="checkbox"
                                                                                  id="checksem" value="on" checked>&nbsp;&nbsp;Semester:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="sem" id="sem">
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_sem['Semester'] ?>"><?php echo $row_sem['Semester'] ?></option>
                                                <?php
                                            } while ($row_sem = mysqli_fetch_assoc($sem));
                                            $rows = mysqli_num_rows($sem);
                                            if ($rows > 0) {
                                                mysqli_data_seek($sem, 0);
                                                $row_sem = mysqli_fetch_assoc($sem);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Show names:</label>
                                    <div class="col-sm-8">
                                        <input type="radio" value="1" name="show_name" checked="checked"/>Yes<br/><input
                                                name="show_name" type="radio" value="2"/>No
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"></label>
                                    <div class="col-sm-8">
                                        <input class="btn btn-primary btn-md btn-block" type="submit" name="PDF"
                                               id="PDF" value="Print PDF">
                                        <input class="btn btn-primary btn-md btn-block" type="submit" name="Excel"
                                               id="Excel" value="Print Excel">
                                    </div>
                                </div>
                            </div>
                            <input name="MM_update" type="hidden" id="MM_update" value="form1">
                        </form>
                    </div>
                </div>

                <?php
            }
            ?>

        </div>
    </div>
</div>
<br>
<br>
<br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>
