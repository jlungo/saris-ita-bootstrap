<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header
include('lecturerMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Faculty Information';
$szSubSection = 'Policy Setup';
include("lecturerheader.php");

if (isset($_GET['content'])) {
    if ($_POST['update']) {
        $FacultyName = addslashes($_POST["FacultyName"]);
        $Address = addslashes($_POST["Address"]);
        $Email = addslashes($_POST["Email"]);
        $Tel = addslashes($_POST["Tel"]);
        $Location = addslashes($_POST["Location"]);
        $CampusID = addslashes($_POST["CampusID"]);

        $sql = "UPDATE faculty SET CampusID='$CampusID', FacultyName='$FacultyName', Address = '$Address',
			Email='$Email', Tel = '$Tel', Location='$Location' WHERE FacultyID=" . $_GET['content'];
        if ($zalongwa->query($sql)) {
            echo "<p style='color: #008000'>Updated successfully</p>";
        } else {
            echo "<p>Failed to update..</p>";
        }
    }
    $result = $zalongwa->query("SELECT * FROM faculty WHERE FacultyID=" . $_GET['content']);
    $institution = $result->fetch_assoc();
    ?>
    <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
        <table class='table_view'>
            <tr class='header'>
                <td> Field</td>
                <td> Field Value</td>
            </tr>
            <tr class='list'>
                <td> Institution: *</td>
                <td>
                    <select name="CampusID" required>
                        <option value="" disabled="disabled" selected="selected">Please select an institution
                        </option>
                        <?php
                        $query_campus = "SELECT CampusID, Campus FROM campus";
                        $result_campus = $zalongwa->query($query_campus) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
                        while ($campus_row = $result_campus->fetch_array()) {
                            ?>
                            <option <?php if($institution['CampusID'] == $campus_row['CampusID']){?> selected="selected" <?php }?> value="<?php echo $campus_row['CampusID']; ?>"><?php echo $campus_row['Campus']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr class='list'>
                <td> Faculty Name: *</td>
                <td><input type="text" id="FacultyName" name="FacultyName" value="<?php echo $institution['FacultyName']; ?>"
                           size="40" required></td>
            </tr>
            <tr class='list'>
                <td> Physical Address: *</td>
                <td><input type="text" id="Address" name="Address" value="<?php echo $institution['Address']; ?>"
                           size="40" required></td>
            </tr>
            <tr class='list'>
                <td> Telephone:</td>
                <td><input type="text" id="Tel" name="Tel" value="<?php echo $institution['Tel']; ?>" size="40"></td>
            </tr>
            <tr class='list'>
                <td> Email:</td>
                <td><input type="text" id="Email" name="Email" value="<?php echo $institution['Email']; ?>" size="40">
                </td>
            </tr>
            <tr class='list'>
                <td> Location: *</td>
                <td><input type="text" id="Location" name="Location" value="<?php echo $institution['Location']; ?>"
                           size="40" required></td>
            </tr>
        </table>
        <br>
        <table class='table_view'>
            <tr style='float: right' class='list'>
                <td>
                    <button formaction="admissionFaculty.php">Back</button>
                </td>
                <td><input type="submit" id="submit" name="update" value="update"></td>
            </tr>
        </table>

    </form>

    <?php

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
        $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}
# include the footer
include("../footer/footer.php");
?>