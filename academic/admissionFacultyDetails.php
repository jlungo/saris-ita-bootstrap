<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header

include('lecturerMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Faculty Information';
$szSubSection = 'Policy Setup';
include("lecturerheader.php");

if (isset($_GET['details'])) {
    $result = $zalongwa->query("SELECT Campus, FacultyName, faculty.Address AS FacultyAddress, faculty.Email AS FacultyEmail, faculty.Tel AS FacultyTel, faculty.Location AS FacultyLocation FROM faculty INNER JOIN campus ON faculty.CampusID=campus.CampusID WHERE FacultyID=" . $_GET['details']);
    $institution = $result->fetch_assoc();
    if ($result->num_rows > 0) {
        ?>
        <table class='table_view'>
            <tr class='header'>
                <td> Field</td>
                <td> Field Value</td>
            </tr>
            <tr class='list'>
                <td> Campus Name</td>
                <td><?php echo $institution['Campus']; ?></td>
            </tr>
            <tr class='list'>
                <td> Faculty Name</td>
                <td><?php echo $institution['FacultyName']; ?></td>
            </tr>
            <tr class='list'>
                <td> Physical Address:</td>
                <td><?php echo $institution['FacultyAddress']; ?></td>
            </tr>
            <tr class='list'>
                <td> Email:</td>
                <td><?php echo $institution['FacultyEmail']; ?></td>
            </tr>
            <tr class='list'>
                <td> Telephone:</td>
                <td><?php echo $institution['FacultyTel']; ?></td>
            </tr>
            <tr class='list'>
                <td> Location:</td>
                <td><?php echo $institution['FacultyLocation']; ?></td>
            </tr>
        </table>
        <br>
        <table class='table_view'>
            <tr style='float: right' class='list'>
                <td><a href="admissionFaculty.php">Back</a></td>
            </tr>
        </table>
        <?php
    } else {
        echo "Sorry, No Records Found <br>";
    }
}
# include the footer
include("../footer/footer.php");
?>