<?php
#get connected to the database and verfy current session
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

# include the header
global $szSection, $szSubSection;
$szSection = 'Profile';
$szSubSection = 'Profile';
$szTitle = 'Academic Module';

#Store Login History
$browser = $_SERVER["HTTP_USER_AGENT"];
$ip = $_SERVER["REMOTE_ADDR"];
$name = $username;
//$username = $username." "."Visited ".$szTitle;
$sql = "INSERT INTO stats(ip,browser,received,page) VALUES('$ip','$browser',now(),'$name')";
$result = mysqli_query($zalongwa, $sql) or die("Siwezi kuingiza data.<br>" . mysqli_error($zalongwa));

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        .row {
            margin-top: 20px;
        }
        .card {
            border-top: 7px solid #263238;
            padding-top: 5%;
        }
        .card:focus, .card:hover {
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            /*box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.20);*/
        }
        .card-inverse .card-img-overlay {
            background-color: rgba(51, 51, 51, 0.85);
            border-color: rgba(51, 51, 51, 0.85);
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container text-muted">
    <!-- cards -->
    <div class="row">
        <div class="col-sm-6 col-md-6">
            <div class="card " class="z-depth-5" align="center">

                <a class="nav-link" href="admissionpolicy.php"> <img class="card-img-top img-fluid "
                                                                     style="width: 112px; height: inherit; padding-top: 15px; "
                                                                     src="./img/administration.svg"></a>
                <div class="card-block">
                    <a class="bootstrap-link" href="admissionpolicy.php"><h5 class="card-title">Policy Setup</h5>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-6">
            <div class="card " class="z-depth-5" align="center">

                <a class="nav-link" href="lecturerAdministration.php"> <img class="card-img-top img-fluid "
                                                                            style="width: 112px; height: inherit; padding-top: 15px; "
                                                                            src="./img/transaction.svg"></a>
                <div class="card-block">
                    <a class="bootstrap-link" href="lecturerAdministration.php"><h5 class="card-title">
                            Administration</h5>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-6 ">
            <div class="card" align="center">

                <a href="lecturerResults.php"> <img class="card-img-top img-fluid "
                                                    style="width: 112px; height: inherit; padding-top: 15px; "
                                                    src="./img/examination.svg"></a>

                <div class="card-block">
                    <a href="lecturerResults.php"><h5 class="card-title">Examination</h5></a>

                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-6 ">
            <div class="card" align="center">

                <a href="gettimetable.php"> <img class="card-img-top img-fluid "
                                                 style="width: 112px; height: inherit; padding-top: 15px; "
                                                 src="./img/timetable.svg"></a>

                <div class="card-block">
                    <a href="gettimetable.php"><h5 class="card-title">Time Table</h5></a>

                </div>
            </div>
        </div>

    </div>
    <div class="row">

        <div class="col-sm-6 col-md-6">
            <div class="card " align="center">

                <a href="admissionComm.php"> <img class="card-img-top img-fluid "
                                                  style="width: 112px; height: inherit; padding-top: 15px; "
                                                  src="./img/communication.svg"></a>
                <div class="card-block">
                    <a class="bootstrap-link" href="admissionComm.php"><h5 class="card-title">Communication</h5></a>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-6 ">
            <div class="card" align="center">

                <a href="admissionSecurity.php"> <img class="card-img-top img-fluid "
                                                      style="width: 112px; height: inherit; padding-top: 15px; "
                                                      src="./img/security.svg"></a>

                <div class="card-block">
                    <a href="admissionSecurity.php"><h5 class="card-title">Security</h5></a>

                </div>
            </div>
        </div>
    </div>
    <!-- end .container -->
    <br><br><br><br>
    <!--footer-->
    <?php include '../footer/footer.php'; ?>

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
            integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
            integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
            integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
            crossorigin="anonymous"></script>
    <!--adding tooltip-->
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <!--Modernaizer here check if not svg supported replace with png-->
    <script>
        if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
        for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
    </script>
</div>
</body>
</html>
