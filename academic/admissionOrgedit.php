<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header
include('lecturerMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Policy Setup';
$szSubSection = 'Policy Setup';
include("lecturerheader.php");

if (isset($_GET['content'])) {
    if ($_POST['update']) {
        $Name = addslashes($_POST["Name"]);
        $Address = addslashes($_POST["Address"]);
        $tel = addslashes($_POST["tel"]);
        $fax = addslashes($_POST["fax"]);
        $email = addslashes($_POST["email"]);
        $website = addslashes($_POST["website"]);
        $city = addslashes($_POST["city"]);

        $sql = "UPDATE organisation SET Name='$Name', Address = '$Address', tel = '$tel', fax = '$fax',
			email='$email', website='$website',city='$city' WHERE Id=" . $_GET['content'];
        if ($zalongwa->query($sql)) {
            echo "<p>Updated successfully</p>";
            header("Location: admissionOrg.php");
        } else {
            echo "<p>Failed to update..</p>";
        }
    }

    $result = $zalongwa->query("SELECT * FROM organisation WHERE Id=" . $_GET['content']);
    $institution = $result->fetch_assoc();
    ?>
    <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
        <table class='table_view'>
            <tr class='header'>
                <td> Field</td>
                <td> Field Value</td>
            </tr>
            <tr class='list'>
                <td> Name</td>
                <td><input type="text" id="Name" name="Name" value="<?php echo $institution['Name']; ?>" size="40"></td>
            </tr>
            <tr class='list'>
                <td> Physical Address:</td>
                <td><input type="text" id="Address" name="Address" value="<?php echo $institution['Address']; ?>"
                           size="40"></td>
            </tr>
            <tr class='list'>
                <td> City:</td>
                <td><input type="text" id="city" name="city" value="<?php echo $institution['city']; ?>" size="40"></td>
            </tr>
            <tr class='list'>
                <td> Website URL:</td>
                <td><input type="text" id="website" name="website" value="<?php echo $institution['website']; ?>"
                           size="40"></td>
            </tr>
            <tr class='list'>
                <td> Telephone:</td>
                <td><input type="text" id="tel" name="tel" value="<?php echo $institution['tel']; ?>" size="40"></td>
            </tr>
            <tr class='list'>
                <td> Fax:</td>
                <td><input type="text" id="fax" name="fax" value="<?php echo $institution['fax']; ?>" size="40"></td>
            </tr>
            <tr class='list'>
                <td> Email:</td>
                <td><input type="text" id="email" name="email" value="<?php echo $institution['email']; ?>" size="40">
                </td>
            </tr>
        </table>
        <br>
        <table class='table_view'>
            <tr style='float: right' class='list'>
                <td>
                    <button formaction="admissionOrg.php">Back</button>
                </td>
                <td><input type="submit" id="submit" name="update" value="update"></td>
            </tr>
        </table>

    </form>

    <?php

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
        $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }

}
# include the footer
include("../footer/footer.php");
?>