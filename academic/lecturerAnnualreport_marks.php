<?php
require('include/rotation.php');

class PDF extends PDF_Rotate
{
    function RotatedText($x, $y, $txt, $angle)
    {
        //Text rotated around its origin
        $this->Rotate($angle, $x, $y);
        $this->Text($x, $y, $txt);
        $this->Rotate(0);
    }

    function RotatedImage($file, $x, $y, $w, $h, $angle)
    {
        //Image rotated around its upper-left corner
        $this->Rotate($angle, $x, $y);
        $this->Image($file, $x, $y, $w, $h);
        $this->Rotate(0);
    }
}

if (isset($_POST['excel'])) {
    #get post variables
    $deg = addslashes($_POST['degree']);
    $year = addslashes($_POST['ayear']);
    $cohot = addslashes($_POST['cohot']);
    //$st_report =$_POST['st_report'];


    #get connected to the database and verfy current session
    require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
    //require_once('includes/print_excel_results.php');
    #prints annual report

    require_once('includes/print_excel_results_annual_marks.php');

    exit;
} #start pdf
elseif (isset($_POST['PDF']) && ($_POST['PDF'] == "Print PDF")) {
    #get connected to the database and verfy current session
    require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');


//create table temp for used courses
    $temp = mysqli_query($zalongwa, "DROP TABLE temp");
    $temp1 = mysqli_query($zalongwa, "CREATE TABLE temp (
  Code varchar(9) NOT NULL,
  Name varchar(200) NOT NULL,
  PRIMARY KEY (Code)
)");

    @$paper = addslashes($_POST['paper']);
    @$layout = addslashes($_POST['layout']);

//$_POST['size']='a3';
    if ($paper == 'a3') {
        @$hsize = '1125.00';
        @$vsize = '835.89';
    } else if ($paper == 'a4') {
        @$hsize = '800.89';
        @$vsize = '580.28';
    }

    #Get Organisation Name
    $qorg = "SELECT * FROM organisation";
    $dborg = mysqli_query($zalongwa, $qorg);
    $row_org = mysqli_fetch_assoc($dborg);
    $org = $row_org['Name'];

    @$checkdegree = addslashes($_POST['checkdegree']);
    @$checkyear = addslashes($_POST['checkyear']);
    @$checkdept = addslashes($_POST['checkdept']);
    @$checkcohot = addslashes($_POST['checkcohot']);

    $prog = $_POST['degree'];
    $cohotyear = $_POST['cohot'];
    $ayear = $_POST['ayear'];
    $qprog = "SELECT ProgrammeCode, Title FROM programme WHERE ProgrammeCode='$prog'";
    $dbprog = mysqli_query($zalongwa, $qprog);
    $row_prog = mysqli_fetch_array($dbprog);
    $progname = $row_prog['Title'];

    //calculate year of study
    $entry = intval(substr($cohotyear, 0, 4));
    $current = intval(substr($ayear, 0, 4));
    $yearofstudy = $current - $entry;

    if ($yearofstudy == 0) {
        $class = "FIRST YEAR";
        $ryr = 1;
    } elseif ($yearofstudy == 1) {
        $class = "SECOND YEAR";
        $ryr = 2;
    } elseif ($yearofstudy == 2) {
        $class = "THIRD YEAR";
        $ryr = 3;
    } elseif ($yearofstudy == 3) {
        $class = "FOURTH YEAR";
        $ryr = 4;
    } elseif ($yearofstudy == 4) {
        $class = "FIFTH YEAR";
        $ryr = 5;
    } elseif ($yearofstudy == 5) {
        $class = "SIXTH YEAR";
        $ryr = 6;
    } elseif ($yearofstudy == 6) {
        $class = "SEVENTHND YEAR";
        $ryr = 7;
    } else {
        $class = "";
    }


    if (($checkdegree == 'on') && ($checkyear == 'on') && ($checkcohot == 'on')) {
        $deg = addslashes($_POST['degree']);
        $year = addslashes($_POST['ayear']);
        $cohot = addslashes($_POST['cohot']);
        $dept = addslashes($_POST['dept']);
        $sem = addslashes($_POST['sem']);
        $cat = $_POST['cat'];
        $checkcombin = addslashes($_POST['checkcombin']);
        if ($checkcombin == 'on') {
            $combin = addslashes($_POST['combin']);
        }

        if ($cat == 4) {
            ############ GET TOTAL REQUIRED CREDITS PER ANNUM ################
            $yCredt = 0;
            for ($s = 1; $s <= 2; $s++) {
                $qcredt = "SELECT * FROM coursecountprogramme WHERE ProgrammeID='$deg' AND YearofStudy='$ryr' AND Semester='$s'";
                $dcredt = mysqli_query($zalongwa, $qcredt);
                $row_credt = mysqli_fetch_array($dcredt);
                $gCredt = $row_credt['CourseCount'];
                $yCredt = $yCredt + $gCredt;
            }


            ############# COUNT CORE COURSES DONE #########
            if ($deg == '1002') {
                if ($checkcombin == 'on') {
                    $qcountcoz = "SELECT * FROM courseprogramme WHERE YearofStudy='$ryr' AND ProgrammeID='$deg' AND Subject='$combin'";
                } else {
                    $qcountcoz = "SELECT * FROM courseprogramme WHERE YearofStudy='$ryr' AND ProgrammeID='$deg'";
                }
                $dcountcoz = mysqli_query($zalongwa, $qcountcoz);
                $row_countcoz = mysqli_num_rows($dcountcoz);
            } else {
                $qcountcoz = "SELECT * FROM courseprogramme WHERE YearofStudy='$ryr' AND ProgrammeID='$deg'";
                $dcountcoz = mysqli_query($zalongwa, $qcountcoz);
                $row_countcoz = mysqli_num_rows($dcountcoz);

            }

            #determine total number of columns
            if ($deg == '1002') {
                if ($checkcombin == 'on') {
                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND Subject='$combin'";
                } else {
                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')";
                }
                $dbstd = mysqli_query($zalongwa, $qstd);
            } else {
                $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')";
                $dbstd = mysqli_query($zalongwa, $qstd);
            }
            $totalcolms = 0;
            while ($rowstd = mysqli_fetch_array($dbstd)) {
                $stdregno = $rowstd['RegNo'];
                $qstdcourse = "SELECT DISTINCT coursecode FROM examscore WHERE RegNo='$stdregno' and AYear='$year' AND ExamSitting=4";
                $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
                $totalstdcourse = mysqli_num_rows($dbstdcourse);
                if ($totalstdcourse > $totalcolms) {
                    $totalcolms = $totalstdcourse;
                }

            }

            #Start PDF
            $pdf = new PDF('L', 'pt', $paper);
            $pdf->open();
            $pdf->setCompression(true);
            $pdf->addPage();
            $pdf->setFont('Arial', 'I', 8);
            $pdf->text(50, $vsize, 'Printed On ' . $today = date("d-m-Y H:i:s"));

            if ($deg == '1002') {
                if ($checkcombin == 'on') {
                    $grpu = $combinD;
                } else {
                    $grpu = "All";
                }
                $pdf->text(300, $vsize, $class . '   ' . $progname . '    --(' . $grpu . ')        ' . $year . '    - Repeaters Annual Results ');
            } else {
                $pdf->text(300, $vsize, $class . '   ' . $progname . '        ' . $year . '  - Repeaters Annual Results');
            }

            #put page header
            $x = 60;
            $y = 64;
            $i = 1;
            $pg = 1;
            $pdf->text($hsize, $vsize, 'Page ' . $pg);
            $cscorewidth = 16;

            #count unregistered
            $j = 0;
            #count sex
            $fmcount = 0;
            $mcount = 0;
            $fcount = 0;

            #print header for landscape paper layout
            $playout = 'l';
            include '../includes/orgname.php';

            $pdf->setFillColor('rgb', 0, 0, 0);
            $pdf->setFont('Arial', '', 13);
            $pdf->text($x + 185, $y + 14, 'ACADEMIC YEAR: ' . $year);
            $pdf->text($x + 200, $y + 28, 'PROGRAMME RECORD SHEET - COURSE REPEATERS');
            $pdf->text($x - 100, $y + 48, $class . ' - ' . strtoupper($progname));

            if ($checkcombin == 'on') {
                $qco = mysqli_query($zalongwa, "SELECT Description FROM subjectcombination WHERE subjectCode='$combin'");
                $dbco = mysqli_fetch_array($qco);
                $combinD = $dbco['Description'];
                $pdf->setFont('Arial', 'I', 9);
                $pdf->text($x - 100 + 320, $y + 48, '   -- (' . strtoupper($combinD) . ')');
            }
            $pdf->setFont('Arial', 'I', 9);
            $pdf->setFont('Arial', '', 13);
            #reset values of x,y
            $x = 50;
            $y = $y + 54;
            #Set Semesters
            $qstdcourse = "SELECT DISTINCT RegNo, coursecode, Semester FROM examscore WHERE AYear='$year' AND ExamSitting=4 ORDER BY Semester, CourseCode ASC";
            $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
            $course = mysqli_fetch_array($dbstdcourse);
            $stno = $course['RegNo'];

            if ($deg == '1002') {
                if ($checkcombin == 'on') {
                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND 
			RegNo='$stno' AND Subject='$combin'";
                } else {
                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                }
                $dbstd = mysqli_query($zalongwa, $qstd);
                $totalstd = mysqli_num_rows($dbstd);
                //$coz=$course['coursecode'];
                $sem = $course['Semester'];
            } else {
                $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                $dbstd = mysqli_query($zalongwa, $qstd);
                $totalstd = mysqli_num_rows($dbstd);
                //$coz=$course['coursecode'];
                $sem = $course['Semester'];
            }
            #set table header
            $pdf->setFont('Arial', 'B', 7);
            $pdf->line($x, $y, $hsize, $y);
            $pdf->line($x, $y + 15, $hsize, $y + 15);
            $pdf->line($x, $y, $x, $y + 15);
            if ($sem == "Semester II" || $sem == "Semester IV" || $sem == "Semester VI" || $sem == "Semester VIII" || $sem == "Semester X") {
                $sem3 = "SEMESTER II";
            }
            if ($sem == "Semester I" || $sem == "Semester III" || $sem == "Semester V" || $sem == "Semester VII" || $sem == "Semester IX") {
                $sem3 = "SEMESTER I";
            }
            $pdf->line($x + 165, $y, $x + 165, $y + 15);
            $pdf->text($x + 180, $y + 12, strtoupper($sem3));
            //$pdf->line($x+118, $y, $x+118, $y+15);
            $sem1 = $sem;
            $sem4 = $sem3;
            #adjust x vlaue
            $x = $x - 85;

            $x = $x + 250;
            $totalcolms = 0;
            $qstdcourse = "SELECT DISTINCT RegNo, coursecode, Semester FROM examscore WHERE AYear='$year'AND ExamSitting=4 
	ORDER BY Semester, CourseCode ASC";
            $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
            while ($course = mysqli_fetch_array($dbstdcourse)) {
                $stno = $course['RegNo'];
                if ($deg == '1002') {
                    if ($checkcombin == 'on') {
                        $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno' AND 
		Subject='$combin'";
                    } else {
                        $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                    }
                    $dbstd = mysqli_query($zalongwa, $qstd);
                    $totalstd = mysqli_num_rows($dbstd);
                    $coz = $course['coursecode'];
                    $sem = $course['Semester'];
                } else {
                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                    $dbstd = mysqli_query($zalongwa, $qstd);
                    $totalstd = mysqli_num_rows($dbstd);
                    $coz = $course['coursecode'];
                    $sem = $course['Semester'];
                }

                if (($totalstd > 0) && ($coz != $stCoz)) {
                    $totalcolms = $totalcolms + 1;
                    $cozp[$totalcolms] = $coz;//create array kuweka these courses
                    $pdf->setFont('Arial', 'B', 7);
                    if (($sem != $sem1)) {
                        if ($sem == "Semester II" || $sem == "Semester IV" || $sem == "Semester VI" || $sem == "Semester VIII" || $sem == "Semester X") {
                            $sem3 = "SEMESTER II";
                        }
                        if ($sem == "Semester I" || $sem == "Semester III" || $sem == "Semester V" || $sem == "Semester VII" || $sem == "Semester IX") {
                            $sem3 = "SEMESTER I";
                        }
                        if (($sem4 != $sem3)) {
                            $pdf->line($x, $y, $x, $y + 15);
                            $pdf->text($x + 25, $y + 12, strtoupper($sem3));
                            $sem4 = $sem3;
                        }
                        $sem1 = $sem;
                    }
                    $x = $x + $cscorewidth;
                    $stCoz = $coz;
                }

            }

            #set colm width
            $clmw = $cscorewidth;
            #get column width factor
            $cwf = $cscorewidth;
            $cw = $x;

            $pdf->line($hsize, $y, $hsize, $y + 15);
            $y = $y + 15;

            #set table Column header
#reset x value
            $x = 50;
            $pdf->setFont('Arial', 'B', 7);
            $pdf->line($x, $y, $hsize, $y);
            $pdf->line($x, $y + 45, $hsize, $y + 45);
            $pdf->line($x, $y, $x, $y + 45);
            $pdf->text($x + 2, $y + 12, 'S/No');
            $pdf->line($x + 25, $y, $x + 25, $y + 45);
            $pdf->text($x + 27, $y + 12, 'Student');

#adjust x vlaue
            $x = $x - 85;
            $pdf->line($x + 226, $y, $x + 226, $y + 45);
            $pdf->text($x + 230, $y + 12, 'Sex');
            $x = $x + 250;
            $totalcolms = 0;
            $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
            while ($course = mysqli_fetch_array($dbstdcourse)) {
                $stno = $course['RegNo'];
                if ($deg == '1002') {
                    if ($checkcombin == 'on') {
                        $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'AND Subject='$combin'";
                    } else {

                        $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                    }
                    $dbstd = mysqli_query($zalongwa, $qstd);
                    $totalstd = mysqli_num_rows($dbstd);
                    $coz = $course['coursecode'];
                } else {
                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                    $dbstd = mysqli_query($zalongwa, $qstd);
                    $totalstd = mysqli_num_rows($dbstd);
                    $coz = $course['coursecode'];
                }

                if (($totalstd > 0) && ($coz != $stCoz)) {
                    $totalcolms = $totalcolms + 1;
                    $cozp[$totalcolms] = $coz;//tafuta array kuweka hizi
                    $pdf->setFont('Arial', 'B', 7);
                    $pdf->line($x, $y, $x, $y + 45);
                    $pdf->RotatedText($x + 7, $y + 34, $coz, 90);
//GET UNITS
                    $qunits = mysqli_query($zalongwa, "SELECT Units FROM course WHERE CourseCode='$coz'");
                    $gunits = mysqli_fetch_array($qunits);
                    $units = $gunits['Units'];
                    $pdf->Text($x + 9, $y + 43, $units, 90);
//$pdf->RotatedText($x,$y,'Hello!',90);

                    $x = $x + $cscorewidth;
                    $stCoz = $coz;
                }

            }

            #set colm width
            $clmw = $cscorewidth;
            #get column width factor
            $cwf = $cscorewidth;
            $cw = $x;

            $pdf->line($x, $y - 15, $x, $y + 45);    /*$pdf->text($x+2, $y+12, 'Credit');
		$pdf->line($x+25, $y, $x+25, $y+45);	$pdf->text($x+27, $y+12, 'Point');
		$pdf->line($x+60, $y, $x+60, $y+45);	$pdf->text($x+67, $y+12, 'GPA');
		$pdf->line($x+95, $y, $x+95, $y+45);	$pdf->text($x+97, $y+12, 'Remark');*/
            $pdf->text($x + 2, $y + 12, 'Remark');
            $pdf->line($hsize, $y, $hsize, $y + 45);
            $y = $y + 45;

            $pdf->setFont('Arial', '', 9);
            #query student list
            if ($deg == '1002') {
                if ($checkcombin == 'on') {
                    $qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') AND Subject='$combin' ORDER BY RegNo";
                } else {
                    $qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY RegNo";

                }
                $dbstudent = mysqli_query($zalongwa, $qstudent);
                $totalstudent = mysqli_num_rows($dbstudent);
            } else {
                $qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY RegNo";
                $dbstudent = mysqli_query($zalongwa, $qstudent);
                $totalstudent = mysqli_num_rows($dbstudent);
            }
            $i = 1;
//for($rsem=1; $rsem<=2; $rsem++)
            while ($rowstudent = mysqli_fetch_array($dbstudent)) {
                $name = $rowstudent['Name'];
                $regno = $rowstudent['RegNo'];
                $sex = $rowstudent['Sex'];

                # get all courses for this candidate
                $qcourse = "SELECT DISTINCT course.CourseName, course.Units, course.Department, course.StudyLevel, examscore.CourseCode, examscore.Semester FROM 

								course INNER JOIN examscore ON (course.CourseCode = examscore.CourseCode)

									 WHERE 

										(examscore.RegNo='$regno') AND 

										(examscore.AYear='$year')AND (examscore.ExamSitting=4) ORDER BY examscore.Semester,examscore.CourseCode ASC";
                $dbcourse = mysqli_query($zalongwa, $qcourse);
                $dbcourseUnit = mysqli_query($zalongwa, $qcourse);
                $total_rows = mysqli_num_rows($dbcourse);

                if ($total_rows > 0) {

                    #initialise
                    $unit = 0;
                    $totalunit = 0;
                    $unittaken = 0;
                    $sgp = 0;
                    $totalsgp = 0;
                    $gpa = 0;
                    $totalCcount = 0;
                    $cntD = 0;
                    $cntE = 0;
                    $key = $regno;
                    $x = 50;

                    #print student info
                    $pdf->setFont('Arial', '', 7);
                    $pdf->line($x, $y, $hsize, $y);
                    $pdf->line($x, $y + 20, $hsize, $y + 20);
                    $pdf->line($x, $y, $x, $y + 20);
                    $pdf->text($x + 2, $y + 16, $i);
                    $pdf->line($x + 25, $y, $x + 25, $y + 20);
                    $pdf->text($x + 27, $y + 17, strtoupper($regno));
                    $pdf->text($x + 25, $y + 9, strtoupper($name));
                    $x = $x - 85;
                    $pdf->line($x + 226, $y, $x + 226, $y + 20);
                    $pdf->text($x + 233, $y + 17, strtoupper($sex));
                    $pdf->line($x + 250, $y, $x + 250, $y + 20);

                    #calculate course clumns widths
                    $clnspace = $x + 250;
                    while ($rowcourse = mysqli_fetch_array($dbcourse)) {
                        $stdcourse = $rowcourse['CourseCode'];
                        $coursefull = explode(" ", $stdcourse);
                        $courseleft = $coursefull[0];
                        $courseright = $coursefull[1];
                        //$pdf->text($clnspace+2, $y+12, $courseleft);
                        //$pdf->text($clnspace+2, $y+24, $courseright);
                        for ($c = 1; $c <= $totalcolms; $c++) {

                            if ($cozp[$c] == $stdcourse)//soma from array tafuta pa kuandika course
                                $add = $c * $cscorewidth;
                        }


                        $clnspace = $clnspace + $add - $cscorewidth;
                        //$pdf->text($clnspace+2, $y+12, $stdcourse );
                        //$pdf->line($clnspace, $y+28, $clnspace+$clmw, $y+28);
                        //$pdf->line($clnspace+$clmw, $y, $clnspace+$clmw, $y+45);
                        $clnspace = $x + 250;
                    }
                    #reset colm space
                    $clmspace = $x + 250;
                    while ($row_course = mysqli_fetch_array($dbcourseUnit)) {
                        $course = $row_course['CourseCode'];
                        $unit = $row_course['Units'];
                        $name = $row_course['CourseName'];
                        $coursefaculty = $row_course['Department'];

//Check if core course
                        $qCore = mysqli_query($zalongwa, "SELECT Status FROM course WHERE CourseCode='$course'");
                        $gCore = mysqli_fetch_array($qCore);
                        $czstatus = $gCore['Status'];
                        if ($czstatus == '1') {
                            $totalCcount = $totalCcount + 1;
                        }
//Save in temp course table
                        $weka = mysqli_query($zalongwa, "INSERT INTO temp(Code, Name)VALUES('$course','$name')");

                        $sn = $sn + 1;
                        $remarks = 'remarks';
                        $grade = '';

                        $RegNo = $key;
                        #insert grading results
                        include 'includes/choose_studylevel.php';
                        for ($c = 1; $c <= $totalcolms; $c++) {

                            if ($cozp[$c] == $course)//soma from array, find pa kuandika course
                                $add = $c * $cscorewidth;
                            if ($cozp[$c] != $course) {
                                $add1 = $c * $cscorewidth;
                                $clmspace = $clmspace + $add1 - $cscorewidth;
                                $pdf->line($clmspace, $y + 10, $clmspace + $clmw, $y + 10);
                                $pdf->line($clmspace + $clmw, $y, $clmspace + $clmw, $y + 20);

                                $clmspace = $x + 250;
                            }
                        }

                        if ($supp == '!') {
                            $clmspace = $clmspace + $add - $cscorewidth;
                            $kpsup = '!';
                            $pdf->setFont('Arial', 'B', 8);

                            $pdf->text($clmspace + 1, $y + 8, $marks);
                            $pdf->text($clmspace + 3, $y + $cscorewidth + 1, $grade . '' . $supp);
                            $supp = '';
                            $fsup = '!';
                            $cntD = $cntD + 1;
                            $pdf->setFont('Arial', '', 7);
                        } elseif ($csup == '*') {
                            $clmspace = $clmspace + $add - $cscorewidth;
                            $pdf->setFont('Arial', 'B', 8);
                            $pdf->text($clmspace + 1, $y + 8, $marks);
                            $pdf->text($clmspace + 3, $y + $cscorewidth + 1, $grade . '' . $supsup . $csup);
                            $kprept = '*';
                            $csup = '';
                            $cntE = $cntE + 1;
                            $pdf->setFont('Arial', '', 7);

                        } else {
                            $clmspace = $clmspace + $add - $cscorewidth;
                            $pdf->text($clmspace + 1, $y + 8, $marks);
                            $pdf->setFont('Arial', '', 9);
                            $pdf->text($clmspace + 3, $y + 18, $grade);
                            $pdf->setFont('Arial', '', 7);
                        }
                        $pdf->line($clmspace + $clmw, $y, $clmspace + $clmw, $y + 20);
                        $clmspace = $x + 250;
                    }

                    $pdf->setFont('Arial', '', 8);
                    $gpa = @substr($totalsgp / $unittaken, 0, 3);
                    #calculate courses column width
                    //$cw = $cwf*$totalcolms;
                    $x = $cw;
//GET REMARKS FROM D AND E COUNTS
                    if (($cntD > 3)) {
                        $cntdisco = 1;
                    } elseif (($cntD >= 2) && ($cntE >= 1)) {
                        $cntdisco = 1;
                    } elseif ($cntE > 2) {
                        $cntdisco = 1;
                    }

//CHECK IF DONE ALL CORE COURSES
                    if ($totalCcount < $row_countcoz) {
                        $skipcoz = 1;
                    }
//CHECK IF COMPLETED THE REQUIRED CREDITS
                    if ($unittaken < $yCredt) {
                        $belwCredt = 1;
                    }

                    $pdf->line($x, $y, $x, $y + 20);
                    /*//$pdf->text($x+5, $y+10, $unittaken);

                $pdf->line($x+25, $y, $x+25, $y+20);
                    $pdf->text($x+30, $y+10, $totalsgp);

                $pdf->line($x+60, $y, $x+60, $y+20);
                if (($igrade<>'I')&& ($skipcoz<>'1')){
                    $pdf->text($x+67, $y+8, $gpa);
                    }
                */

                    #get student remarks
                    $qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$regno'";
                    $dbremarks = mysqli_query($zalongwa, $qremarks);
                    $row_remarks = mysqli_fetch_assoc($dbremarks);
                    $totalremarks = mysqli_num_rows($dbremarks);
                    $studentremarks = $row_remarks['Remark'];
                    if (($totalremarks > 0) && ($studentremarks <> '')) {
                        $remark = $studentremarks;
                    } else {

                        if ($gpa >= 4.4) {
                            $remark = 'PASS';
                            $gpagrade = 'A';
                        } elseif ($gpa >= 3.5) {
                            $remark = 'PASS';
                            $gpagrade = 'B+';
                        } elseif ($gpa >= 2.7) {
                            $remark = 'PASS';
                            $gpagrade = 'B';
                        } elseif ($gpa >= 2.0) {
                            $remark = 'PASS';
                            $gpagrade = 'C';
                        } else {
                            $remark = 'DISCO';
                            $gpagrade = 'E';
                            $disco = 1;
                        }
                    }
                    //$pdf->line($x+60, $y, $x+60, $y+13);

                    if (($igrade <> 'I') && ($skipcoz <> '1')) {
                        $pdf->setFont('Arial', '', 9);
                        //$pdf->text($x+67, $y+18, $gpagrade);
                    }
                    $pdf->setFont('Arial', '', 8);
                    //$pdf->line($x+60, $y+10, $x+95, $y+10);
                    //$pdf->line($x+95, $y, $x+95, $y+20);
                    if ($fsup == '!') {
                        $pdf->setFont('Arial', 'B', 7);
                        $pdf->text($x + 5, $y + 6, 'SUPP');
                        if ($csupf == '*') {
                            $pdf->text($x + 5, $y + 12, 'RPT.Co');
                        }
                        if ($igrade == 'I') {
                            $pdf->text($x + 5, $y + 18, 'INC.Co');
                        }


                    } elseif ($igrade == 'I') {
                        $pdf->setFont('Arial', 'B', 7);
                        $pdf->text($x + 5, $y + 18, 'INC.Co');

                        if ($csupf == '*') {
                            $pdf->text($x + 5, $y + 12, 'RPT.Co');
                        }
                        if ($fsup == '!') {
                            $pdf->text($x + 5, $y + 6, 'SUPP');
                        }


                    } elseif ($csupf == '*') {
                        $pdf->setFont('Arial', 'B', 7);
                        $pdf->text($x + 5, $y + 12, 'RPT.Co');
                        if ($igrade == 'I') {
                            $pdf->text($x + 5, $y + 18, 'INC.Co');
                        }
                        if ($fsup == '!') {
                            $pdf->text($x + 5, $y + 6, 'SUPP');
                        }


                    } elseif (($igrade <> 'I') || ($fsup <> '!') || ($csupf <> '*')) {
                        $pdf->setFont('Arial', '', 8);
                        $pdf->text($x + 5, $y + 12, $remark);
                    }
                    $pdf->line($hsize, $y, $hsize, $y + 20);
                    $y = $y - 25;

                    #check failed exam
                    if ($fexm == '#') {
                        //$pdf->text($x+97, $y+26, $fexm.' = Fail Exam');
                        $fexm = '';
                        $remark = '';
                    }
                    #check failed exam
                    if ($fcwk == '*') {
                        //$pdf->text($x+97, $y+38, $fcwk.' = Fail CWK');
                        $fcwk = '';
                        $remark = '';
                    }
                    if ($fsup == '!') {
                        //$pdf->text($x+97, $y+42, $fsup.' = Supp');
                        $kpsup = '!';
                        $fsup = '';
                        $remark = '';
                    }
                    if ($igrade == 'I') {
                        //$pdf->text($x+137, $y+42, $igrade.' = Inc.');
                        $kpinc = 'I';
                        $igrade = '';
                        $remark = '';
                    }
                    if ($csupf == '*') {
                        //$pdf->text($x+137, $y+26, $egrade.' = C/Repeat.');
                        $csupf = '';
                        $kprept = '*';
                        $egrade = '';
                        $remark = '';
                    }
                    $i = $i + 1;
                    $y = $y + 45;
                    if ($y >= $vsize - 50) {
                        $pdf->setFont('Arial', 'B', 8);
                        if ($kpsup == '!' || $kpinc == 'I' || $kprept == '*') {
                            $pdf->text(50, $vsize - 20, 'KEY:');
                            if ($kpsup == '!') {
                                $pdf->text(80, $vsize - 20, $kpsup . ' = Supp ,');
                            }
                            if ($kpinc == 'I') {
                                $pdf->text(125, $vsize - 20, $kpinc . ' = Inco. ,');
                            }
                            if ($kprept == '*') {
                                $pdf->text(170, $vsize - 20, $kprept . ' = Repeat Course');
                            }
                        }
                        #start new page
                        $pdf->addPage();
                        $pdf->setFont('Arial', 'I', 8);
                        $pdf->text(50, $vsize, 'Printed On ' . $today = date("d-m-Y H:i:s"));

                        $x = 50;
                        $y = 50;
                        $pg = $pg + 1;
                        $pdf->text($hsize, $vsize, 'Page ' . $pg);
                        if ($deg == '1002') {
                            if ($checkcombin == 'on') {
                                $grpu = $combinD;
                            } else {
                                $grpu = "All";
                            }
                            $pdf->text(300, $vsize, $class . '   ' . $progname . '    --(' . $grpu . ')        ' . $year . '    - Repeaters Annual Results ');
                        } else {
                            $pdf->text(300, $vsize, $class . '   ' . $progname . '        ' . $year . '  - Repeaters Annual Results');
                        }


                        #count unregistered
                        $j = 0;
                        #count sex
                        $fmcount = 0;
                        $mcount = 0;
                        $fcount = 0;
#Set Semesters
                        $qstdcourse = "SELECT DISTINCT RegNo, coursecode, Semester FROM examscore WHERE AYear='$year' AND ExamSitting=4 ORDER BY Semester, CourseCode ASC";
                        $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
                        $course = mysqli_fetch_array($dbstdcourse);
                        $stno = $course['RegNo'];
                        if ($deg == '1002') {
                            if ($checkcombin == 'on') {
                                $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno' AND Combination='$combin'";
                            } else {

                                $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                            }
                            $dbstd = mysqli_query($zalongwa, $qstd);
                            $totalstd = mysqli_num_rows($dbstd);
                            //$coz=$course['coursecode'];
                            $sem = $course['Semester'];
                        } else {

                            $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                            $dbstd = mysqli_query($zalongwa, $qstd);
                            $totalstd = mysqli_num_rows($dbstd);
                            //$coz=$course['coursecode'];
                            $sem = $course['Semester'];
                        }
#set table header
                        $pdf->setFont('Arial', 'B', 7);
                        $pdf->line($x, $y, $hsize, $y);
                        $pdf->line($x, $y + 15, $hsize, $y + 15);
                        $pdf->line($x, $y, $x, $y + 15);
                        if ($sem == "Semester II" || $sem == "Semester IV" || $sem == "Semester VI" || $sem == "Semester VIII" || $sem == "Semester X") {
                            $sem3 = "SEMESTER II";
                        }
                        if ($sem == "Semester I" || $sem == "Semester III" || $sem == "Semester V" || $sem == "Semester VII" || $sem == "Semester IX") {
                            $sem3 = "SEMESTER I";
                        }
                        $pdf->line($x + 165, $y, $x + 165, $y + 15);
                        $pdf->text($x + 180, $y + 12, strtoupper($sem3));
                        //$pdf->line($x+118, $y, $x+118, $y+15);
                        $sem1 = $sem;
                        $sem4 = $sem3;
                        #adjust x vlaue
                        $x = $x - 85;

                        $x = $x + 250;
                        $totalcolms = 0;
                        $qstdcourse = "SELECT DISTINCT RegNo, coursecode, Semester FROM examscore WHERE AYear='$year' AND ExamSitting=4 ORDER BY Semester, CourseCode ASC";
                        $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
                        while ($course = mysqli_fetch_array($dbstdcourse)) {
                            $stno = $course['RegNo'];
                            if ($deg == '1002') {
                                if ($checkcombin == 'on') {
                                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno' AND Combination='$combin'";
                                } else {
                                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                                }
                                $dbstd = mysqli_query($zalongwa, $qstd);
                                $totalstd = mysqli_num_rows($dbstd);
                                $coz = $course['coursecode'];
                                $sem = $course['Semester'];
                            } else {
                                $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                                $dbstd = mysqli_query($zalongwa, $qstd);
                                $totalstd = mysqli_num_rows($dbstd);
                                $coz = $course['coursecode'];
                                $sem = $course['Semester'];

                            }

                            if (($totalstd > 0) && ($coz != $stCoz)) {
                                $totalcolms = $totalcolms + 1;
                                $cozp[$totalcolms] = $coz;//tafuta array kuweka hizi
                                $pdf->setFont('Arial', 'B', 7);
                                if ($sem != $sem1) {
                                    if ($sem == "Semester II" || $sem == "Semester IV" || $sem == "Semester VI" || $sem == "Semester VIII" || $sem == "Semester X") {
                                        $sem3 = "SEMESTER II";
                                    }
                                    if ($sem == "Semester I" || $sem == "Semester III" || $sem == "Semester V" || $sem == "Semester VII" || $sem == "Semester IX") {
                                        $sem3 = "SEMESTER I";
                                    }
                                    if (($sem4 != $sem3)) {
                                        $pdf->line($x, $y, $x, $y + 15);
                                        $pdf->text($x + 25, $y + 12, strtoupper($sem3));
                                        $sem4 = $sem3;
                                    }
                                    $sem1 = $sem;
                                }
                                $x = $x + $cscorewidth;
                                $stCoz = $coz;
                            }

                        }

                        #set colm width
                        $clmw = $cscorewidth;
                        #get column width factor
                        $cwf = $cscorewidth;
                        $cw = $x;

                        $pdf->line($hsize, $y, $hsize, $y + 15);
                        $y = $y + 15;

                        #set table Column header
#reset x value
                        $x = 50;
                        $pdf->setFont('Arial', 'B', 7);
                        $pdf->line($x, $y, $hsize, $y);
                        $pdf->line($x, $y + 45, $hsize, $y + 45);
                        $pdf->line($x, $y, $x, $y + 45);
                        $pdf->text($x + 2, $y + 12, 'S/No');
                        $pdf->line($x + 25, $y, $x + 25, $y + 45);
                        $pdf->text($x + 27, $y + 12, 'Student');
                        #adjust x vlaue
                        $x = $x - 85;
                        $pdf->line($x + 226, $y, $x + 226, $y + 45);
                        $pdf->text($x + 230, $y + 12, 'Sex');
                        $x = $x + 250;
                        $totalcolms = 0;
                        $qstdcourse = "SELECT DISTINCT RegNo, coursecode, Semester FROM examscore WHERE AYear='$year'AND ExamSitting=4 ORDER BY Semester, CourseCode ASC";
                        $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
                        while ($course = mysqli_fetch_array($dbstdcourse)) {
                            $stno = $course['RegNo'];
                            if ($deg == '1002') {
                                if ($checkcombin == 'on') {
                                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno' AND Combination='$combin'";
                                } else {

                                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                                }
                                $dbstd = mysqli_query($zalongwa, $qstd);
                                $totalstd = mysqli_num_rows($dbstd);
                                $coz = $course['coursecode'];
                                $sem = $course['Semester'];
                            } else {
                                $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                                $dbstd = mysqli_query($zalongwa, $qstd);
                                $totalstd = mysqli_num_rows($dbstd);
                                $coz = $course['coursecode'];
                                $sem = $course['Semester'];
                            }

                            if (($totalstd > 0) && ($coz != $stCoz)) {
                                $totalcolms = $totalcolms + 1;
                                $cozp[$totalcolms] = $coz;//tafuta array kuweka hizi
                                $pdf->setFont('Arial', 'B', 7);
                                $pdf->line($x, $y, $x, $y + 45);
                                $pdf->RotatedText($x + 7, $y + 34, $coz, 90);
//GET UNITS
                                $qunits = mysqli_query($zalongwa, "SELECT Units FROM course WHERE CourseCode='$coz'");
                                $gunits = mysqli_fetch_array($qunits);
                                $units = $gunits['Units'];
                                $pdf->Text($x + 9, $y + 43, $units, 90);
                                $x = $x + $cscorewidth;
                                $stCoz = $coz;
                            }

                        }

                        #set colm width
                        $clmw = $cscorewidth;
                        #get column width factor
                        $cwf = $cscorewidth;
                        $cw = $x;
                        $pdf->line($x, $y - 15, $x, $y + 45);    /*$pdf->text($x+2, $y+12, 'Credit');
		$pdf->line($x+25, $y, $x+25, $y+45);	$pdf->text($x+27, $y+12, 'Point');
		$pdf->line($x+60, $y, $x+60, $y+45);	$pdf->text($x+67, $y+12, 'GPA');
		$pdf->line($x+95, $y, $x+95, $y+45);	$pdf->text($x+97, $y+12, 'Remark'); */
                        $pdf->text($x + 2, $y + 12, 'Remark');
                        $pdf->line($hsize, $y, $hsize, $y + 45);
                        $y = $y + 45;

                    }
                } //ends if $total_rows
            }//ends $rowstudent loop

#start new page for the keys
            $space = $vsize - $y;
            $yind = $y + 25;
            if ($space < 70) {
                $pdf->addPage();

                $x = 50;
                $y = 50;
                $pg = $pg + 1;
                $tpg = $pg;
                $pdf->setFont('Arial', 'I', 8);
                $pdf->text(50, $vsize, 'Printed On ' . $today = date("d-m-Y H:i:s"));
                $pg = $pg + 1;
                $pdf->text($hsize, $vsize, 'Page ' . $pg);
                if ($deg == '1002') {
                    if ($checkcombin == 'on') {
                        $grpu = $combinD;
                    } else {
                        $grpu = "All";
                    }
                    $pdf->text(300, $vsize, $class . '   ' . $progname . '    --(' . $grpu . ')        ' . $year . '    - Repeaters Annual Results ');
                } else {
                    $pdf->text(300, $vsize, $class . '   ' . $progname . '        ' . $year . '  - Repeaters Annual Results');
                }
                $yind = $y;
            }

            if (($deg == 2200) || ($deg == 1200) || ($deg == 1300)) {
                $pdf->text(450, $yind, 'Deputy Provost for Academic Affairs(Name): ............................. Signature: ...............');
                $pdf->text(450, $yind + 12, 'Date: .................................... ');
                $pdf->text(450, $yind + 28, 'Director for Postgraduate Studies(Name): ......................  Signature: ................. ');
                $pdf->text(450, $yind + 38, 'Date: ................................');
            } else {
                $pdf->text(450, $yind, 'Signature of The Dean: ....................................');
                $pdf->text(450, $yind + 12, 'Date: ...............................       ');
                $pdf->text(450, $yind + 28, 'Signature of the Chairperson of the Senate: ..............................');
                $pdf->text(450, $yind + 38, 'Date: ......................................');

            }
            $pdf->setFont('Arial', 'B', 8);
            if ($kpsup == '!' || $kpinc == 'I' || $kprept == '*') {
                $pdf->text(50, $yind - 12, 'KEY:');
                if ($kpsup == '!') {
                    $pdf->text(80, $yind - 12, $kpsup . ' = Supp ,');
                }
                if ($kpinc == 'I') {
                    $pdf->text(125, $yind - 12, $kpinc . ' = Inco. ,');
                }
                if ($kprept == '*') {
                    $pdf->text(170, $yind - 12, $kprept . ' = Repeat Course');
                }
            }
            $pdf->setFont('Arial', 'I', 9);
            $pdf->text(190.28, $yind, '          ######## END OF EXAM RESULTS ########');

            #include points calculation keys
            include 'includes/pointskey.php';
            $x = 50;
            $y = $yind + 44;
            #table 1
            include 'includes/gradescale.php';

// KEY COURSES
#start new page for the keys
            $space = $vsize - 50 - $y;
            $yind = $y + 100;
            if ($space < 70) {
                $pdf->addPage();

                $x = 50;
                $y = 50;
                $pg = $pg + 1;
                $tpg = $pg;
                $pdf->setFont('Arial', 'I', 8);
                $pdf->text(50, $vsize, 'Printed On ' . $today = date("d-m-Y H:i:s"));
                $pg = $pg + 1;
                $pdf->text($hsize, $vsize, 'Page ' . $pg);
                if ($deg == '1002') {
                    if ($checkcombin == 'on') {
                        $grpu = $combinD;
                    } else {
                        $grpu = "All";
                    }
                    $pdf->text(300, $vsize, $class . '   ' . $progname . '    --(' . $grpu . ')        ' . $year . '    - Repeaters Annual Results ');
                } else {
                    $pdf->text(300, $vsize, $class . '   ' . $progname . '        ' . $year . '  - Repeaters Annual Results');
                }
                $yind = $y;
            }
            $pdf->text(70, $yind, 'KEY TO COURSES:');

//get course list
            $qcourseget = "select * from temp";
//$yind=$yind+125;
            $xn = 160;
            $qcourseDesc = mysqli_query($zalongwa, $qcourseget);
            $add = 0;
            $cnt = 0;
            while ($getDesc = mysqli_fetch_array($qcourseDesc)) {
                $cozcode = $getDesc['Code'];
                $cozname = $getDesc['Name'];
                $pdf->text($xn, $yind, $cozcode);
                $pdf->text($xn + 50, $yind, $cozname);
                $yind = $yind + 10;
                $add = $add + 1;

                $space = $vsize - 50 - $yind;

########################HAPAAAAAA################
                if ($space < 0 && $cnt != 1) {
                    $cnt = 1;
                    $xn = $xn + 300;
                    $st = $add * 10;
                    $yind = $yind - $st;
                    $add = 0;


                    $dev = $cnt;
                } elseif ($space < 0 && $cnt == 1) {
                    $xn = 160;
                    $add = 0;
                    $cnt = 0;
                    $yind = $y + 100;
                    $pdf->addPage();

                    $x = 50;
                    $y = 50;
                    $pg = $pg + 1;
                    $tpg = $pg;
                    $pdf->setFont('Arial', 'I', 8);
                    $pdf->text(50, $vsize, 'Printed On ' . $today = date("d-m-Y H:i:s"));
                    $pdf->text($hsize, $vsize, 'Page ' . $pg);
                    if ($deg == '1002') {
                        if ($checkcombin == 'on') {
                            $grpu = $combinD;
                            echo $grpu;
                            exit;
                        } else {
                            $grpu = "All";
                        }
                        $pdf->text(300, $vsize, $class . '   ' . $progname . '    --(' . $grpu . ')        ' . $year . '    - Repeaters Annual Results ');
                    } else {
                        $pdf->text(300, $vsize, $class . '   ' . $progname . '        ' . $year . '  - Repeaters Annual Results ');
                    }
                    $yind = $y;

                    $pdf->text(70, $yind, 'KEY TO COURSES:');
                }


############################################


            }
//drop temporary course table
            $temp = mysqli_query($zalongwa, "DROP TABLE temp");

        } else {

###########End Repeaters########################
############ GET TOTAL REQUIRED CREDITS PER ANNUM ################
            $yCredt = 0;
            for ($s = 1; $s <= 2; $s++) {
                $qcredt = "SELECT * FROM coursecountprogramme WHERE ProgrammeID='$deg' AND YearofStudy='$ryr' AND Semester='$s'";
                $dcredt = mysqli_query($zalongwa, $qcredt);
                $row_credt = mysqli_fetch_array($dcredt);
                $gCredt = $row_credt['CourseCount'];
                $yCredt = $yCredt + $gCredt;
            }


            #############COUNT CORE COURSES DONE #########
            if ($deg == '1002') {
                if ($checkcombin == 'on') {
                    $qcountcoz = "SELECT * FROM courseprogramme WHERE YearofStudy='$ryr' AND ProgrammeID='$deg' AND Combination='$combin'";
                } else {
                    $qcountcoz = "SELECT * FROM courseprogramme WHERE YearofStudy='$ryr' AND ProgrammeID='$deg'";
                }
                $dcountcoz = mysqli_query($zalongwa, $qcountcoz);
                $row_countcoz = mysqli_num_rows($dcountcoz);
            } else {
                $qcountcoz = "SELECT * FROM courseprogramme WHERE YearofStudy='$ryr' AND ProgrammeID='$deg'";
                $dcountcoz = mysqli_query($zalongwa, $qcountcoz);
                $row_countcoz = mysqli_num_rows($dcountcoz);

            }

############


            #determine total number of columns
            if ($deg == '2100' || $deg == '2101' || $deg == '2103') {
                if ($checkcombin == 'on') {
                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND Combination='$combin'";
                } else {
                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')";
                }
                $dbstd = mysqli_query($zalongwa, $qstd);
            } else {
                $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')";
                $dbstd = mysqli_query($zalongwa, $qstd);
            }
            $totalcolms = 0;
            while ($rowstd = mysqli_fetch_array($dbstd)) {
                $stdregno = $rowstd['RegNo'];
                $qstdcourse = "SELECT DISTINCT coursecode FROM examresult WHERE RegNo='$stdregno' and AYear='$year'";
                $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
                $totalstdcourse = mysqli_num_rows($dbstdcourse);
                if ($totalstdcourse > $totalcolms) {
                    $totalcolms = $totalstdcourse;
                }

            }

            #Start PDF
            $pdf = new PDF('L', 'pt', $paper);
            $pdf->open();
            $pdf->setCompression(true);
            $pdf->addPage();
            $pdf->setFont('Arial', 'I', 8);
            $pdf->text(50, $vsize, 'Printed On ' . $today = date("d-m-Y H:i:s"));
            if ($deg == '1002') {
                if ($checkcombin == 'on') {
                    $grpu = $combinD;
                } else {
                    $grpu = "All";
                }
                $pdf->text(300, $vsize, $class . '   ' . $progname . '    --(' . $grpu . ')        ' . $year . '    Annual Results ');
            } else {
                $pdf->text(300, $vsize, $class . '   ' . $progname . '        ' . $year . '  Annual Results');
            }

            #put page header
            $x = 60;
            $y = 64;
            $i = 1;
            $pg = 1;
            $pdf->text($hsize, $vsize, 'Page ' . $pg);
            $cscorewidth = 16;

            #count unregistered
            $j = 0;
            #count sex
            $fmcount = 0;
            $mcount = 0;
            $fcount = 0;

            #print header for landscape paper layout
            $playout = 'l';
            include '../includes/orgname.php';

            $pdf->setFillColor('rgb', 0, 0, 0);
            $pdf->setFont('Arial', '', 13);
            $pdf->text($x + 185, $y + 14, 'ACADEMIC YEAR: ' . $year);
            $pdf->text($x + 200, $y + 28, 'PROGRAMME RECORD SHEET');
            $pdf->text($x - 100, $y + 48, $class . ' - ' . strtoupper($progname));
            if ($checkcombin == 'on') {
                $qco = mysqli_query($zalongwa, "SELECT Description FROM subjectcombination WHERE subjectCode='$combin'");
                $dbco = mysqli_fetch_array($qco);
                $combinD = $dbco['Description'];
                $pdf->setFont('Arial', 'I', 9);
                $pdf->text($x - 100 + 350, $y + 48, ' --- (' . strtoupper($combinD) . ')');
            }
            $pdf->setFont('Arial', 'I', 9);
            $pdf->setFont('Arial', '', 13);
            #reset values of x,y
            $x = 50;
            $y = $y + 54;
#Set Semesters
            $qstdcourse = "SELECT DISTINCT RegNo, coursecode, Semester FROM examresult WHERE AYear='$year' ORDER BY Semester, CourseCode ASC";
            $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
            $course = mysqli_fetch_array($dbstdcourse);
            $stno = $course['RegNo'];

            if ($deg == '1002') {
                if ($checkcombin == 'on') {
                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno' AND 
		subject='$combin'";
                } else {
                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                }
                $dbstd = mysqli_query($zalongwa, $qstd);
                $totalstd = mysqli_num_rows($dbstd);
                //$coz=$course['coursecode'];
                $sem = $course['Semester'];
            } else {
                $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                $dbstd = mysqli_query($zalongwa, $qstd);
                $totalstd = mysqli_num_rows($dbstd);
                //$coz=$course['coursecode'];
                $sem = $course['Semester'];

            }
#set table header
            $pdf->setFont('Arial', 'B', 7);
            $pdf->line($x, $y, $hsize, $y);
            $pdf->line($x, $y + 15, $hsize, $y + 15);
            $pdf->line($x, $y, $x, $y + 15);
            if ($sem == "Semester II" || $sem == "Semester IV" || $sem == "Semester VI" || $sem == "Semester VIII" || $sem == "Semester X") {
                $sem3 = "SEMESTER II";
            }
            if ($sem == "Semester I" || $sem == "Semester III" || $sem == "Semester V" || $sem == "Semester VII" || $sem == "Semester IX") {
                $sem3 = "SEMESTER I";
            }
            $pdf->line($x + 165, $y, $x + 165, $y + 15);
            $pdf->text($x + 180, $y + 12, strtoupper($sem3));
            //$pdf->line($x+118, $y, $x+118, $y+15);
            $sem1 = $sem;
            $sem4 = $sem3;
            #adjust x vlaue
            $x = $x - 85;

            $x = $x + 250;
            $totalcolms = 0;
            $qstdcourse = "SELECT DISTINCT RegNo, coursecode, Semester FROM examresult WHERE AYear='$year' ORDER BY Semester, CourseCode ASC";
            $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
            while ($course = mysqli_fetch_array($dbstdcourse)) {
                $stno = $course['RegNo'];
                if ($deg == '1002') {
                    if ($checkcombin == 'on') {
                        $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno' AND Combination='$combin'";
                    } else {
                        $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                    }
                    $dbstd = mysqli_query($zalongwa, $qstd);
                    $totalstd = mysqli_num_rows($dbstd);
                    $coz = $course['coursecode'];
                    $sem = $course['Semester'];
                } else {
                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                    $dbstd = mysqli_query($zalongwa, $qstd);
                    $totalstd = mysqli_num_rows($dbstd);
                    $coz = $course['coursecode'];
                    $sem = $course['Semester'];
                }

                if (($totalstd > 0) && ($coz != $stCoz)) {
                    $totalcolms = $totalcolms + 1;
                    $cozp[$totalcolms] = $coz;//create array kuweka these courses
                    $pdf->setFont('Arial', 'B', 7);
                    if (($sem != $sem1)) {
                        if ($sem == "Semester II" || $sem == "Semester IV" || $sem == "Semester VI" || $sem == "Semester VIII" || $sem == "Semester X") {
                            $sem3 = "SEMESTER II";
                        }
                        if ($sem == "Semester I" || $sem == "Semester III" || $sem == "Semester V" || $sem == "Semester VII" || $sem == "Semester IX") {
                            $sem3 = "SEMESTER I";
                        }
                        if (($sem4 != $sem3)) {
                            $pdf->line($x, $y, $x, $y + 15);
                            $pdf->text($x + 25, $y + 12, strtoupper($sem3));
                            $sem4 = $sem3;
                        }
                        $sem1 = $sem;
                    }
                    $x = $x + $cscorewidth;
                    $stCoz = $coz;
                }

            }

            #set colm width
            $clmw = $cscorewidth;
            #get column width factor
            $cwf = $cscorewidth;
            $cw = $x;

            $pdf->line($hsize, $y, $hsize, $y + 15);
            $y = $y + 15;

            #set table Column header
#reset x value
            $x = 50;
            $pdf->setFont('Arial', 'B', 7);
            $pdf->line($x, $y, $hsize, $y);
            $pdf->line($x, $y + 45, $hsize, $y + 45);
            $pdf->line($x, $y, $x, $y + 45);
            $pdf->text($x + 2, $y + 12, 'S/No');
            $pdf->line($x + 25, $y, $x + 25, $y + 45);
            $pdf->text($x + 27, $y + 12, 'Student');

#adjust x vlaue
            $x = $x - 85;
            $pdf->line($x + 226, $y, $x + 226, $y + 45);
            $pdf->text($x + 230, $y + 12, 'Sex');
            $x = $x + 250;
            $totalcolms = 0;
            $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
            while ($course = mysqli_fetch_array($dbstdcourse)) {
                $stno = $course['RegNo'];
                if ($deg == '1002') {
                    if ($checkcombin == 'on') {
                        $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'AND Combination='$combin'";
                    } else {

                        $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                    }
                    $dbstd = mysqli_query($zalongwa, $qstd);
                    $totalstd = mysqli_num_rows($dbstd);
                    $coz = $course['coursecode'];
                } else {
                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";

                    $dbstd = mysqli_query($zalongwa, $qstd);
                    $totalstd = mysqli_num_rows($dbstd);
                    $coz = $course['coursecode'];
                }

                if (($totalstd > 0) && ($coz != $stCoz)) {
                    $totalcolms = $totalcolms + 1;
                    $cozp[$totalcolms] = $coz;//tafuta array kuweka hizi
                    $pdf->setFont('Arial', 'B', 7);
                    $pdf->line($x, $y, $x, $y + 45);
                    $pdf->RotatedText($x + 7, $y + 34, $coz, 90);
//GET UNITS
                    $qunits = mysqli_query($zalongwa, "SELECT Units FROM course WHERE CourseCode='$coz'");
                    $gunits = mysqli_fetch_array($qunits);
                    $units = $gunits['Units'];
                    $pdf->Text($x + 9, $y + 43, $units, 90);
//$pdf->RotatedText($x,$y,'Hello!',90);

                    $x = $x + $cscorewidth;
                    $stCoz = $coz;
                }

            }

            #set colm width
            $clmw = $cscorewidth;
            #get column width factor
            $cwf = $cscorewidth;
            $cw = $x;

            $pdf->line($x, $y - 15, $x, $y + 45);
            $pdf->text($x + 2, $y + 12, 'Credit');
            $pdf->line($x + 25, $y, $x + 25, $y + 45);
            $pdf->text($x + 27, $y + 12, 'Point');
            $pdf->line($x + 60, $y, $x + 60, $y + 45);
            $pdf->text($x + 67, $y + 12, 'GPA');
            $pdf->line($x + 95, $y, $x + 95, $y + 45);
            $pdf->text($x + 97, $y + 12, 'Remark');
            $pdf->line($hsize, $y, $hsize, $y + 45);
            $y = $y + 45;

            $pdf->setFont('Arial', '', 9);
            #query student list
            if ($deg == '1002') {
                if ($checkcombin == 'on') {
                    $qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') AND Combination='$combin' ORDER BY RegNo";
                } else {
                    $qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY RegNo";

                }
                $dbstudent = mysqli_query($zalongwa, $qstudent);
                $totalstudent = mysqli_num_rows($dbstudent);
            } else {
                $qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY RegNo";
                $dbstudent = mysqli_query($zalongwa, $qstudent);
                $totalstudent = mysqli_num_rows($dbstudent);
            }
            $i = 1;
//for($rsem=1; $rsem<=2; $rsem++)
            while ($rowstudent = mysqli_fetch_array($dbstudent)) {
                $name = $rowstudent['Name'];
                $regno = $rowstudent['RegNo'];
                $sex = $rowstudent['Sex'];

                # get all courses for this candidate
                $qcourse = "SELECT DISTINCT course.CourseName, course.Units, course.Department, course.StudyLevel, examresult.CourseCode, examresult.Semester FROM 
								course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
									 WHERE 
										(examresult.RegNo='$regno') AND 
										(examresult.AYear='$year') ORDER BY examresult.Semester,examresult.CourseCode ASC";
                $dbcourse = mysqli_query($zalongwa, $qcourse);
                $dbcourseUnit = mysqli_query($zalongwa, $qcourse);
                $total_rows = mysqli_num_rows($dbcourse);

                if ($total_rows > 0) {

                    #initialise
                    $unit = 0;
                    $totalunit = 0;
                    $unittaken = 0;
                    $sgp = 0;
                    $totalsgp = 0;
                    $gpa = 0;
                    $totalCcount = 0;
                    $cntD = 0;
                    $cntE = 0;
                    $key = $regno;
                    $x = 50;

                    #print student info
                    $pdf->setFont('Arial', '', 7);
                    $pdf->line($x, $y, $hsize, $y);
                    $pdf->line($x, $y + 20, $hsize, $y + 20);
                    $pdf->line($x, $y, $x, $y + 20);
                    $pdf->text($x + 2, $y + 16, $i);
                    $pdf->line($x + 25, $y, $x + 25, $y + 20);
                    $pdf->text($x + 27, $y + 17, strtoupper($regno));
                    $pdf->text($x + 25, $y + 9, strtoupper($name));
                    $x = $x - 85;
                    $pdf->line($x + 226, $y, $x + 226, $y + 20);
                    $pdf->text($x + 233, $y + 17, strtoupper($sex));
                    $pdf->line($x + 250, $y, $x + 250, $y + 20);

                    #calculate course clumns widths
                    $clnspace = $x + 250;
                    while ($rowcourse = mysqli_fetch_array($dbcourse)) {
                        $stdcourse = $rowcourse['CourseCode'];
                        $coursefull = explode(" ", $stdcourse);
                        $courseleft = $coursefull[0];
                        $courseright = $coursefull[1];
                        //$pdf->text($clnspace+2, $y+12, $courseleft);
                        //$pdf->text($clnspace+2, $y+24, $courseright);
                        for ($c = 1; $c <= $totalcolms; $c++) {

                            if ($cozp[$c] == $stdcourse)//soma from array tafuta pa kuandika course
                                $add = $c * $cscorewidth;
                        }


                        $clnspace = $clnspace + $add - $cscorewidth;
                        //$pdf->text($clnspace+2, $y+12, $stdcourse );
                        //$pdf->line($clnspace, $y+28, $clnspace+$clmw, $y+28);
                        //$pdf->line($clnspace+$clmw, $y, $clnspace+$clmw, $y+45);
                        $clnspace = $x + 250;
                    }
                    #reset colm space
                    $clmspace = $x + 250;
                    while ($row_course = mysqli_fetch_array($dbcourseUnit)) {
                        $course = $row_course['CourseCode'];
                        $unit = $row_course['Units'];
                        $name = $row_course['CourseName'];
                        $coursefaculty = $row_course['Department'];

//Check if core course
                        $qCore = mysqli_query($zalongwa, "SELECT Status FROM course WHERE CourseCode='$course'");
                        $gCore = mysqli_fetch_array($qCore);
                        $czstatus = $gCore['Status'];
                        if ($czstatus == '1') {
                            $totalCcount = $totalCcount + 1;
                        }
//Save in temp course table
                        $weka = mysqli_query($zalongwa, "INSERT INTO temp(Code, Name)VALUES('$course','$name')");

                        $sn = $sn + 1;
                        $remarks = 'remarks';
                        $grade = '';

                        $RegNo = $key;
                        #insert grading results
                        include 'includes/choose_studylevel.php';
                        for ($c = 1; $c <= $totalcolms; $c++) {

                            if ($cozp[$c] == $course)//soma from array, find pa kuandika course
                                $add = $c * $cscorewidth;
                            if ($cozp[$c] != $course) {
                                $add1 = $c * $cscorewidth;
                                $clmspace = $clmspace + $add1 - $cscorewidth;
                                $pdf->line($clmspace, $y + 10, $clmspace + $clmw, $y + 10);
                                $pdf->line($clmspace + $clmw, $y, $clmspace + $clmw, $y + 20);

                                $clmspace = $x + 250;
                            }
                        }

                        if ($supp == '!') {
                            $clmspace = $clmspace + $add - $cscorewidth;
                            $kpsup = '!';
                            $pdf->setFont('Arial', 'B', 8);

                            $pdf->text($clmspace + 1, $y + 8, $marks);
                            $pdf->text($clmspace + 3, $y + $cscorewidth + 1, $grade . '' . $supp);
                            $supp = '';
                            $fsup = '!';
                            $cntD = $cntD + 1;
                            $pdf->setFont('Arial', '', 7);
                        } elseif ($csup == '*') {
                            $clmspace = $clmspace + $add - $cscorewidth;
                            $pdf->setFont('Arial', 'B', 8);
                            $pdf->text($clmspace + 1, $y + 8, $marks);
                            $pdf->text($clmspace + 3, $y + $cscorewidth + 1, $grade . '' . $supsup . $csup);
                            $kprept = '*';
                            $csup = '';
                            $cntE = $cntE + 1;
                            $pdf->setFont('Arial', '', 7);

                        } else {
                            $clmspace = $clmspace + $add - $cscorewidth;
                            $pdf->text($clmspace + 1, $y + 8, $marks);
                            $pdf->setFont('Arial', '', 9);
                            $pdf->text($clmspace + 3, $y + 18, $grade);
                            $pdf->setFont('Arial', '', 7);
                        }
                        $pdf->line($clmspace + $clmw, $y, $clmspace + $clmw, $y + 20);
                        $clmspace = $x + 250;
                    }

                    $pdf->setFont('Arial', '', 8);
                    $gpa = @substr($totalsgp / $unittaken, 0, 3);
                    #calculate courses column width
                    //$cw = $cwf*$totalcolms;
                    $x = $cw;
//GET REMARKS FROM D AND E COUNTS
                    if (($cntD > 3)) {
                        $cntdisco = 1;
                    } elseif (($cntD >= 2) && ($cntE >= 1)) {
                        $cntdisco = 1;
                    } elseif ($cntE > 2) {
                        $cntdisco = 1;
                    }

//CHECK IF DONE ALL CORE COURSES
                    if ($totalCcount < $row_countcoz) {
                        $skipcoz = 1;
                    }
//CHECK IF COMPLETED THE REQUIRED CREDITS
                    if ($unittaken < $yCredt) {
                        $belwCredt = 1;
                    }

                    $pdf->line($x, $y, $x, $y + 20);
                    $pdf->text($x + 5, $y + 10, $unittaken);

                    $pdf->line($x + 25, $y, $x + 25, $y + 20);
                    $pdf->text($x + 30, $y + 10, $totalsgp);

                    $pdf->line($x + 60, $y, $x + 60, $y + 20);
                    if (($igrade <> 'I') && ($skipcoz <> '1')) {
                        $pdf->text($x + 67, $y + 8, $gpa);
                    }

                    #get student remarks
                    $qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$regno'";
                    $dbremarks = mysqli_query($zalongwa, $qremarks);
                    $row_remarks = mysqli_fetch_assoc($dbremarks);
                    $totalremarks = mysqli_num_rows($dbremarks);
                    $studentremarks = $row_remarks['Remark'];

                    if (($totalremarks > 0) && ($studentremarks <> '')) {
                        $remark = $studentremarks;
                    } else {

                        if ($gpa >= 4.4) {
                            $remark = 'PASS';
                            $gpagrade = 'A';
                        } elseif ($gpa >= 3.5) {
                            $remark = 'PASS';
                            $gpagrade = 'B+';
                        } elseif ($gpa >= 2.7) {
                            $remark = 'PASS';
                            $gpagrade = 'B';
                        } elseif ($gpa >= 2.0) {
                            $remark = 'PASS';
                            $gpagrade = 'C';
                        } else {
                            $remark = 'DISCO';
                            $gpagrade = 'E';
                            $disco = 1;
                        }
                    }
                    $pdf->line($x + 60, $y, $x + 60, $y + 13);

                    if (($igrade <> 'I') && ($skipcoz <> '1')) {
                        $pdf->setFont('Arial', '', 9);
                        $pdf->text($x + 67, $y + 18, $gpagrade);
                    }
                    $pdf->setFont('Arial', '', 8);
                    $pdf->line($x + 60, $y + 10, $x + 95, $y + 10);
                    $pdf->line($x + 95, $y, $x + 95, $y + 20);
                    if ($belwCredt == '1') {
                        $pdf->setFont('Arial', 'B', 7);
                        $pdf->text($x + 130, $y + 6, 'BELW.CRDT');
                        $belwCredt = '';
                        if ($csupf == '*') {
                            $pdf->text($x + 97, $y + 12, 'RPT.Co');
                        }
                        if ($igrade == 'I') {
                            $pdf->text($x + 97, $y + 18, 'INC.Co');
                        }
                        if ($disco == '1' || $cntdisco == '1') {
                            $pdf->text($x + 130, $y + 18, 'DISCO');
                            $disco = '';
                            $cntdisco = '';
                        }
                        if ($skipcoz == '1') {
                            $pdf->text($x + 130, $y + 12, 'INC.(SKIP Co)');
                            $skipcoz = '';
                        }
                    } elseif ($fsup == '!') {
                        $pdf->setFont('Arial', 'B', 7);
                        $pdf->text($x + 97, $y + 6, 'SUPP');
                        if ($csupf == '*') {
                            $pdf->text($x + 97, $y + 12, 'RPT.Co');
                        }
                        if ($igrade == 'I') {
                            $pdf->text($x + 97, $y + 18, 'INC.Co');
                        }
                        if ($disco == '1' || $cntdisco == '1') {
                            $pdf->text($x + 130, $y + 18, 'DISCO');
                            $disco = '';
                            $cntdisco = '';
                        }
                        if ($skipcoz == '1') {
                            $pdf->text($x + 130, $y + 12, 'INC.(SKIP Co)');
                            $skipcoz = '';
                            $belwCredt = '';
                        }

                    } elseif ($igrade == 'I') {
                        $pdf->setFont('Arial', 'B', 7);
                        $pdf->text($x + 97, $y + 18, 'INC.Co');
                        if ($skipcoz == '1') {
                            $pdf->text($x + 130, $y + 12, 'INC.(SKIP Co)');
                            $skipcoz = '';
                        }
                        if ($csupf == '*') {
                            $pdf->text($x + 97, $y + 12, 'RPT.Co');
                        }
                        if ($fsup == '!') {
                            $pdf->text($x + 97, $y + 6, 'SUPP');
                        }
                        if ($disco == '1' || $cntdisco == '1') {
                            $pdf->text($x + 130, $y + 18, 'DISCO');
                            $disco = '';
                            $cntdisco = '';
                        }

                    } elseif ($skipcoz == '1') {
                        $pdf->setFont('Arial', 'B', 7);
                        $pdf->text($x + 130, $y + 12, 'INC.(SKIP Co)');
                        $skipcoz = '';
                        if ($igrade == 'I') {
                            $pdf->text($x + 97, $y + 18, 'INC.Co');
                        }
                        if ($csupf == '*') {
                            $pdf->text($x + 97, $y + 12, 'RPT.Co');
                        }
                        if ($fsup == '!') {
                            $pdf->text($x + 97, $y + 6, 'SUPP');
                        }
                        if ($disco == '1' || $cntdisco == '1') {
                            $pdf->text($x + 130, $y + 18, 'DISCO');
                            $disco = '';
                            $cntdisco = '';
                        }

                    } elseif ($csupf == '*') {
                        $pdf->setFont('Arial', 'B', 7);
                        $pdf->text($x + 97, $y + 12, 'RPT.Co');
                        if ($igrade == 'I') {
                            $pdf->text($x + 97, $y + 18, 'INC.Co');
                        }
                        if ($fsup == '!') {
                            $pdf->text($x + 97, $y + 6, 'SUPP');
                        }
                        if ($disco == '1' || $cntdisco == '1') {
                            $pdf->text($x + 130, $y + 18, 'DISCO');
                            $disco = '';
                            $cntdisco = '';
                        }
                        if ($skipcoz == '1') {
                            $pdf->text($x + 130, $y + 12, 'INC.(SKIP Co)');
                            $skipcoz = '';
                        }
                    } elseif (($igrade <> 'I') || ($fsup <> '!') || ($csupf <> '*')) {
                        $pdf->setFont('Arial', '', 8);
                        $pdf->text($x + 97, $y + 12, $remark);
                    }
                    $pdf->line($hsize, $y, $hsize, $y + 20);
                    $y = $y - 25;

                    #check failed exam
                    if ($fexm == '#') {
                        //$pdf->text($x+97, $y+26, $fexm.' = Fail Exam');
                        $fexm = '';
                        $remark = '';
                    }
                    #check failed exam
                    if ($fcwk == '*') {
                        //$pdf->text($x+97, $y+38, $fcwk.' = Fail CWK');
                        $fcwk = '';
                        $remark = '';
                    }
                    if ($fsup == '!') {
                        //$pdf->text($x+97, $y+42, $fsup.' = Supp');
                        $kpsup = '!';
                        $fsup = '';
                        $remark = '';
                    }
                    if ($igrade == 'I') {
                        //$pdf->text($x+137, $y+42, $igrade.' = Inc.');
                        $kpinc = 'I';
                        $igrade = '';
                        $remark = '';
                    }
                    if ($csupf == '*') {
                        //$pdf->text($x+137, $y+26, $egrade.' = C/Repeat.');
                        $csupf = '';
                        $kprept = '*';
                        $egrade = '';
                        $remark = '';
                    }
                    $i = $i + 1;
                    $y = $y + 45;
                    if ($y >= $vsize - 50) {
                        $pdf->setFont('Arial', 'B', 8);
                        if ($kpsup == '!' || $kpinc == 'I' || $kprept == '*') {
                            $pdf->text(50, $vsize - 20, 'KEY:');
                            if ($kpsup == '!') {
                                $pdf->text(80, $vsize - 20, $kpsup . ' = Supp ,');
                            }
                            if ($kpinc == 'I') {
                                $pdf->text(125, $vsize - 20, $kpinc . ' = Inco. ,');
                            }
                            if ($kprept == '*') {
                                $pdf->text(170, $vsize - 20, $kprept . ' = Repeat Course');
                            }
                        }
                        #start new page
                        $pdf->addPage();
                        $pdf->setFont('Arial', 'I', 8);
                        $pdf->text(50, $vsize, 'Printed On ' . $today = date("d-m-Y H:i:s"));

                        $x = 50;
                        $y = 50;
                        $pg = $pg + 1;
                        $pdf->text($hsize, $vsize, 'Page ' . $pg);
                        if ($deg == '1002') {
                            if ($checkcombin == 'on') {
                                $grpu = $combinD;
                            } else {
                                $grpu = "All";
                            }
                            $pdf->text(300, $vsize, $class . '   ' . $progname . '    --(' . $grpu . ')        ' . $year . '    Annual Results ');
                        } else {
                            $pdf->text(300, $vsize, $class . '   ' . $progname . '        ' . $year . '  Annual Results');
                        }


                        #count unregistered
                        $j = 0;
                        #count sex
                        $fmcount = 0;
                        $mcount = 0;
                        $fcount = 0;
#Set Semesters
                        $qstdcourse = "SELECT DISTINCT RegNo, coursecode, Semester FROM examresult WHERE AYear='$year' ORDER BY Semester, CourseCode ASC";
                        $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
                        $course = mysqli_fetch_array($dbstdcourse);
                        $stno = $course['RegNo'];
                        if ($deg == '1002') {
                            if ($checkcombin == 'on') {
                                $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno' AND Subject='$combin'";
                            } else {

                                $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                            }
                            $dbstd = mysqli_query($zalongwa, $qstd);
                            $totalstd = mysqli_num_rows($dbstd);
                            //$coz=$course['coursecode'];
                            $sem = $course['Semester'];
                        } else {

                            $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                            $dbstd = mysqli_query($zalongwa, $qstd);
                            $totalstd = mysqli_num_rows($dbstd);
                            //$coz=$course['coursecode'];
                            $sem = $course['Semester'];
                        }
#set table header
                        $pdf->setFont('Arial', 'B', 7);
                        $pdf->line($x, $y, $hsize, $y);
                        $pdf->line($x, $y + 15, $hsize, $y + 15);
                        $pdf->line($x, $y, $x, $y + 15);
                        if ($sem == "Semester II" || $sem == "Semester IV" || $sem == "Semester VI" || $sem == "Semester VIII" || $sem == "Semester X") {
                            $sem3 = "SEMESTER II";
                        }
                        if ($sem == "Semester I" || $sem == "Semester III" || $sem == "Semester V" || $sem == "Semester VII" || $sem == "Semester IX") {
                            $sem3 = "SEMESTER I";
                        }
                        $pdf->line($x + 165, $y, $x + 165, $y + 15);
                        $pdf->text($x + 180, $y + 12, strtoupper($sem3));
                        //$pdf->line($x+118, $y, $x+118, $y+15);
                        $sem1 = $sem;
                        $sem4 = $sem3;
                        #adjust x vlaue
                        $x = $x - 85;

                        $x = $x + 250;
                        $totalcolms = 0;
                        $qstdcourse = "SELECT DISTINCT RegNo, coursecode, Semester FROM examresult WHERE AYear='$year' ORDER BY Semester, CourseCode ASC";
                        $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
                        while ($course = mysqli_fetch_array($dbstdcourse)) {
                            $stno = $course['RegNo'];
                            if ($deg == '1002') {
                                if ($checkcombin == 'on') {
                                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno' AND Subject='$combin'";
                                } else {
                                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                                }
                                $dbstd = mysqli_query($zalongwa, $qstd);
                                $totalstd = mysqli_num_rows($dbstd);
                                $coz = $course['coursecode'];
                                $sem = $course['Semester'];
                            } else {
                                $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                                $dbstd = mysqli_query($zalongwa, $qstd);
                                $totalstd = mysqli_num_rows($dbstd);
                                $coz = $course['coursecode'];
                                $sem = $course['Semester'];

                            }

                            if (($totalstd > 0) && ($coz != $stCoz)) {
                                $totalcolms = $totalcolms + 1;
                                $cozp[$totalcolms] = $coz;//tafuta array kuweka hizi
                                $pdf->setFont('Arial', 'B', 7);
                                if ($sem != $sem1) {
                                    if ($sem == "Semester II" || $sem == "Semester IV" || $sem == "Semester VI" || $sem == "Semester VIII" || $sem == "Semester X") {
                                        $sem3 = "SEMESTER II";
                                    }
                                    if ($sem == "Semester I" || $sem == "Semester III" || $sem == "Semester V" || $sem == "Semester VII" || $sem == "Semester IX") {
                                        $sem3 = "SEMESTER I";
                                    }
                                    if (($sem4 != $sem3)) {
                                        $pdf->line($x, $y, $x, $y + 15);
                                        $pdf->text($x + 25, $y + 12, strtoupper($sem3));
                                        $sem4 = $sem3;
                                    }
                                    $sem1 = $sem;
                                }
                                $x = $x + $cscorewidth;
                                $stCoz = $coz;
                            }

                        }

                        #set colm width
                        $clmw = $cscorewidth;
                        #get column width factor
                        $cwf = $cscorewidth;
                        $cw = $x;

                        $pdf->line($hsize, $y, $hsize, $y + 15);
                        $y = $y + 15;

                        #set table Column header
#reset x value
                        $x = 50;
                        $pdf->setFont('Arial', 'B', 7);
                        $pdf->line($x, $y, $hsize, $y);
                        $pdf->line($x, $y + 45, $hsize, $y + 45);
                        $pdf->line($x, $y, $x, $y + 45);
                        $pdf->text($x + 2, $y + 12, 'S/No');
                        $pdf->line($x + 25, $y, $x + 25, $y + 45);
                        $pdf->text($x + 27, $y + 12, 'Student');
                        #adjust x vlaue
                        $x = $x - 85;
                        $pdf->line($x + 226, $y, $x + 226, $y + 45);
                        $pdf->text($x + 230, $y + 12, 'Sex');
                        $x = $x + 250;
                        $totalcolms = 0;
                        $qstdcourse = "SELECT DISTINCT RegNo, coursecode, Semester FROM examresult WHERE AYear='$year' ORDER BY Semester, CourseCode ASC";
                        $dbstdcourse = mysqli_query($zalongwa, $qstdcourse);
                        while ($course = mysqli_fetch_array($dbstdcourse)) {
                            $stno = $course['RegNo'];
                            if ($deg == '1002') {
                                if ($checkcombin == 'on') {
                                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno' AND Subject='$combin'";
                                } else {

                                    $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                                }
                                $dbstd = mysqli_query($zalongwa, $qstd);
                                $totalstd = mysqli_num_rows($dbstd);
                                $coz = $course['coursecode'];
                                $sem = $course['Semester'];
                            } else {
                                $qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')AND RegNo='$stno'";
                                $dbstd = mysqli_query($zalongwa, $qstd);
                                $totalstd = mysqli_num_rows($dbstd);
                                $coz = $course['coursecode'];
                                $sem = $course['Semester'];
                            }

                            if (($totalstd > 0) && ($coz != $stCoz)) {
                                $totalcolms = $totalcolms + 1;
                                $cozp[$totalcolms] = $coz;//tafuta array kuweka hizi
                                $pdf->setFont('Arial', 'B', 7);
                                $pdf->line($x, $y, $x, $y + 45);
                                $pdf->RotatedText($x + 7, $y + 34, $coz, 90);
//GET UNITS
                                $qunits = mysqli_query($zalongwa, "SELECT Units FROM course WHERE CourseCode='$coz'");
                                $gunits = mysqli_fetch_array($qunits);
                                $units = $gunits['Units'];
                                $pdf->Text($x + 9, $y + 43, $units, 90);
                                $x = $x + $cscorewidth;
                                $stCoz = $coz;
                            }

                        }

                        #set colm width
                        $clmw = $cscorewidth;
                        #get column width factor
                        $cwf = $cscorewidth;
                        $cw = $x;
                        $pdf->line($x, $y - 15, $x, $y + 45);
                        $pdf->text($x + 2, $y + 12, 'Credit');
                        $pdf->line($x + 25, $y, $x + 25, $y + 45);
                        $pdf->text($x + 27, $y + 12, 'Point');
                        $pdf->line($x + 60, $y, $x + 60, $y + 45);
                        $pdf->text($x + 67, $y + 12, 'GPA');
                        $pdf->line($x + 95, $y, $x + 95, $y + 45);
                        $pdf->text($x + 97, $y + 12, 'Remark');
                        $pdf->line($hsize, $y, $hsize, $y + 45);
                        $y = $y + 45;

                    }
                } //ends if $total_rows
            }//ends $rowstudent loop

#start new page for the keys
            $space = $vsize - $y;
            $yind = $y + 25;
            if ($space < 70) {
                $pdf->addPage();

                $x = 50;
                $y = 50;
                $pg = $pg + 1;
                $tpg = $pg;
                $pdf->setFont('Arial', 'I', 8);
                $pdf->text(50, $vsize, 'Printed On ' . $today = date("d-m-Y H:i:s"));
                $pg = $pg + 1;
                $pdf->text($hsize, $vsize, 'Page ' . $pg);
                if ($deg == '1002') {
                    if ($checkcombin == 'on') {
                        $grpu = $combinD;
                    } else {
                        $grpu = "All";
                    }
                    $pdf->text(300, $vsize, $class . '   ' . $progname . '    --(' . $grpu . ')        ' . $year . '    Annual Results ');
                } else {
                    $pdf->text(300, $vsize, $class . '   ' . $progname . '        ' . $year . '  Annual Results');
                }
                $yind = $y;
            }

            if (($deg == 2200) || ($deg == 1200) || ($deg == 1300)) {
                $pdf->text(450, $yind, 'Deputy Provost for Academic Affairs(Name): ............................. Signature: ...............');
                $pdf->text(450, $yind + 12, 'Date: .................................... ');
                $pdf->text(450, $yind + 28, 'Director for Postgraduate Studies(Name): ......................  Signature: ................. ');
                $pdf->text(450, $yind + 38, 'Date: ................................');
            } else {
                $pdf->text(450, $yind, 'Signature of The Dean: ....................................');
                $pdf->text(450, $yind + 12, 'Date: ...............................       ');
                $pdf->text(450, $yind + 28, 'Signature of the Chairperson of the Senate: ..............................');
                $pdf->text(450, $yind + 38, 'Date: ......................................');

            }
            $pdf->setFont('Arial', 'B', 8);
            if ($kpsup == '!' || $kpinc == 'I' || $kprept == '*') {
                $pdf->text(50, $yind - 12, 'KEY:');
                if ($kpsup == '!') {
                    $pdf->text(80, $yind - 12, $kpsup . ' = Supp ,');
                }
                if ($kpinc == 'I') {
                    $pdf->text(125, $yind - 12, $kpinc . ' = Inco. ,');
                }
                if ($kprept == '*') {
                    $pdf->text(170, $yind - 12, $kprept . ' = Repeat Course');
                }
            }
            $pdf->setFont('Arial', 'I', 9);
            $pdf->text(190.28, $yind, '          ######## END OF EXAM RESULTS ########');

            #include points calculation keys
            include 'includes/pointskey.php';
            $x = 50;
            $y = $yind + 44;
            #table 1
            include 'includes/gradescale.php';

// KEY COURSES
#start new page for the keys
            $space = $vsize - 50 - $y;
            $yind = $y + 100;
            if ($space < 70) {
                $pdf->addPage();

                $x = 50;
                $y = 50;
                $pg = $pg + 1;
                $tpg = $pg;
                $pdf->setFont('Arial', 'I', 8);
                $pdf->text(50, $vsize, 'Printed On ' . $today = date("d-m-Y H:i:s"));
                $pg = $pg + 1;
                $pdf->text($hsize, $vsize, 'Page ' . $pg);
                if ($deg == '1002') {
                    if ($checkcombin == 'on') {
                        $grpu = $combinD;
                    } else {
                        $grpu = "All";
                    }
                    $pdf->text(300, $vsize, $class . '   ' . $progname . '    --(' . $grpu . ')        ' . $year . '    Annual Results ');
                } else {
                    $pdf->text(300, $vsize, $class . '   ' . $progname . '        ' . $year . '  Annual Results');
                }
                $yind = $y;
            }
            $pdf->text(70, $yind, 'KEY TO COURSES:');

//get course list
            $qcourseget = "select * from temp";
//$yind=$yind+125;
            $xn = 160;
            $qcourseDesc = mysqli_query($zalongwa, $qcourseget);
            $add = 0;
            $cnt = 0;
            while ($getDesc = mysqli_fetch_array($qcourseDesc)) {
                $cozcode = $getDesc['Code'];
                $cozname = $getDesc['Name'];
                $pdf->text($xn, $yind, $cozcode);
                $pdf->text($xn + 50, $yind, $cozname);
                $yind = $yind + 10;
                $add = $add + 1;

                $space = $vsize - 50 - $yind;

########################HAPAAAAAA################
                if ($space < 0 && $cnt != 1) {
                    $cnt = 1;
                    $xn = $xn + 300;
                    $st = $add * 10;
                    $yind = $yind - $st;
                    $add = 0;


                    $dev = $cnt;
                } elseif ($space < 0 && $cnt == 1) {
                    $xn = 160;
                    $add = 0;
                    $cnt = 0;
                    $yind = $y + 100;
                    $pdf->addPage();

                    $x = 50;
                    $y = 50;
                    $pg = $pg + 1;
                    $tpg = $pg;
                    $pdf->setFont('Arial', 'I', 8);
                    $pdf->text(50, $vsize, 'Printed On ' . $today = date("d-m-Y H:i:s"));
                    $pdf->text($hsize, $vsize, 'Page ' . $pg);
                    if ($deg == '1002') {
                        if ($checkcombin == 'on') {
                            $grpu = $combinD;
                        } else {
                            $grpu = "All";
                        }
                        $pdf->text(300, $vsize, $class . '   ' . $progname . '    --(' . $grpu . ')        ' . $year . '    Annual Results ');
                    } else {
                        $pdf->text(300, $vsize, $class . '   ' . $progname . '        ' . $year . '  Annual Results');
                    }
                    $yind = $y;

                    $pdf->text(70, $yind, 'KEY TO COURSES:');
                }


############################################


            }
//drop temporary course table
            $temp = mysqli_query($zalongwa, "DROP TABLE temp");
        }


        #output file
        $filename = ereg_replace("[[:space:]]+", "", $progname);

        $pdf->Output();

    }
}//end of print pdf
?>
<?php
#get connected to the database and verfy current session
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

# include the header
global $szSection, $szSubSection;
$szSection = 'Examination';
//$szSubSection = 'Annual Report';
$szSubSection = 'Annual Results (Marks)';
$szTitle = ' Annual Report Examination Results';

$editFormAction = $_SERVER['PHP_SELF'];


$query_studentlist = "SELECT RegNo, Name, ProgrammeofStudy FROM student ORDER BY ProgrammeofStudy  ASC";
$studentlist = mysqli_query($zalongwa, $query_studentlist) or die(mysqli_error($zalongwa));
$row_studentlist = mysqli_fetch_assoc($studentlist);
$totalRows_studentlist = mysqli_num_rows($studentlist);


$query_degree = "SELECT ProgrammeCode, ProgrammeName FROM programme ORDER BY ProgrammeName ASC";
$degree = mysqli_query($zalongwa, $query_degree) or die(mysqli_error($zalongwa));
$row_degree = mysqli_fetch_assoc($degree);
$totalRows_degree = mysqli_num_rows($degree);


$query_ayear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$ayear = mysqli_query($zalongwa, $query_ayear) or die(mysqli_error($zalongwa));
$row_ayear = mysqli_fetch_assoc($ayear);
$totalRows_ayear = mysqli_num_rows($ayear);


$query_sem = "SELECT Semester FROM terms ORDER BY Semester";
$sem = mysqli_query($zalongwa, $query_sem) or die(mysqli_error($zalongwa));
$row_sem = mysqli_fetch_assoc($sem);
$totalRows_sem = mysqli_num_rows($sem);


$query_dept = "SELECT Faculty, DeptName FROM department ORDER BY DeptName, Faculty ASC";
$dept = mysqli_query($zalongwa, $query_dept) or die(mysqli_error($zalongwa));
$row_dept = mysqli_fetch_assoc($dept);
$totalRows_dept = mysqli_num_rows($dept);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="./css/breadcrumb.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /*background-color: #009688;*/
        }

        .card {
            /*background-color: #324359;*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /*color: white;*/
            padding: 0px;
            border-radius: 0px !important;
        }

        .row {
            margin-top: 20px;
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">

            <?php

            if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
                ?>
                <style type="text/css">
                    <!--
                    .style1 {
                        color: #FFFFFF
                    }

                    -->
                </style>

                <h4 align="center">

                    <?php
                    $prog = $_POST['degree'];
                    $cohotyear = $_POST['cohot'];
                    $ayear = $_POST['ayear'];
                    $qprog = "SELECT ProgrammeCode, Title FROM programme WHERE ProgrammeCode='$prog'";
                    $dbprog = mysqli_query($zalongwa, $qprog);
                    $row_prog = mysqli_fetch_array($dbprog);
                    $progname = $row_prog['Title'];
                    $qyear = "SELECT AYear FROM academicyear WHERE AYear='$cohotyear'";
                    $dbyear = mysqli_query($zalongwa, $qyear);
                    $row_year = mysqli_fetch_array($dbyear);
                    $year = $row_year['AYear'];
                    echo $progname;
                    echo " - " . $year;
                    ?>

                    <br>
                </h4>
                <?php

                @$checkdegree = addslashes($_POST['checkdegree']);
                @$checkyear = addslashes($_POST['checkyear']);
                @$checksem = addslashes($_POST['checksem']);
                $checkcohot = addslashes($_POST['checkcohot']);

                $c = 0;

                if (($checkdegree == 'on') && ($checkyear == 'on') && ($checksem == 'on') && ($checkcohot == 'on')) {
//echo "<br>Examination Results for Academic Year - ".$ayear.'-'.$sem;

                    $deg = addslashes($_POST['degree']);
                    $year = addslashes($_POST['ayear']);
                    $cohot = addslashes($_POST['cohot']);
                    $sem = addslashes($_POST['sem']);
                    echo "<p><b>Examination Results for Academic Year - " . $ayear . '&nbsp;-&nbsp;' . $sem;
                    echo "</b></p>";
//query student list
                    $qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY RegNo";
                    $dbstudent = mysqli_query($zalongwa, $qstudent);
                    $totalstudent = mysqli_num_rows($dbstudent);
                    $i = 1;
                    while ($rowstudent = mysqli_fetch_array($dbstudent)) {
                        $name = $rowstudent['Name'];
                        $regno = $rowstudent['RegNo'];
                        $sex = $rowstudent['Sex'];

                        # get all courses for this candidate
                        $qcourse = "SELECT DISTINCT course.Units, course.Department, course.StudyLevel, examresult.CourseCode FROM 
						course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
							 WHERE 
									(RegNo='$regno') AND 
									(AYear='$year') AND 
									(course.Programme = '$deg') AND 
									(Semester = '$sem')";
                        $dbcourse = mysqli_query($zalongwa, $qcourse);
                        $dbcourseUnit = mysqli_query($zalongwa, $qcourse);
                        $total_rows = mysqli_num_rows($dbcourse);

                        if ($total_rows > 0) {

                            #initialise
                            $totalunit = 0;
                            $unittaken = 0;
                            $sgp = 0;
                            $totalsgp = 0;
                            $gpa = 0;
                            $key = $regno;
                            ?>

                            <table class='resView' width="100%" height="100%">
                                <tr>
                                    <td class='resViewhd' width="20" rowspan="2" nowrap scope="col">
                                        <div align="left"></div> <?php echo $i ?></td>
                                    <td class='resViewhd' width="160" rowspan="2" nowrap
                                        scope="col"><?php echo $name . ": " . $regno; ?> </td>
                                    <td class='resViewhd' width="13" rowspan="2" nowrap>
                                        <div align="center"><?php echo $sex ?></div>
                                    </td>
                                    <?php while ($rowcourse = mysqli_fetch_array($dbcourse)) { ?>
                                        <td class='resViewhd'>
                                            <div align="center"><?php echo $rowcourse['CourseCode']; ?></div>
                                        </td>
                                    <?php } ?>
                                    <td class='resViewhd'>
                                        <div align="center">Units</div>
                                    </td>
                                    <td class='resViewhd'>
                                        <div align="center">Points</div>
                                    </td>
                                    <td class='resViewhd'>
                                        <div align="center">GPA</div>
                                    </td>
                                    <td class='resViewhd'>
                                        <div align="center">Grade</div>
                                    </td>
                                    <td class='resViewhd' scope="col">Remarks</td>
                                </tr>
                                <tr>
                                    <?php while ($row_course = mysqli_fetch_array($dbcourseUnit)) {
                                        $course = $row_course['CourseCode'];
                                        $unit = $row_course['Units'];
                                        $name = $row_course['CourseName'];
                                        $coursefaculty = $row_course['Department'];
                                        $sn = $sn + 1;
                                        $remarks = 'remarks';

                                        $RegNo = $key;
                                        #insert grading results
                                        include 'includes/choose_studylevel.php';
                                        ?>

                                        <td class='resViewtd'>
                                            <div align="center"><?php echo $grade; ?></div>
                                        </td>
                                    <?php } //ends while row_course loop

                                    ?>
                                    <td class='resViewtd'>
                                        <div align="center"><?php $gunits = $unittaken + $gunits;
                                            echo $unittaken; ?></div>
                                    </td>
                                    <td class='resViewtd'>
                                        <div align="center"><?php $gpoints = $totalsgp + $gpoints;
                                            echo $totalsgp; ?></div>
                                    </td>
                                    <td class='resViewtd'>
                                        <div align="center"><?php $gpa = @substr($totalsgp / $unittaken, 0, 3);
                                            echo $gpa ?></div>
                                    </td>
                                    <td class='resViewtd'>
                                        <div align="center"><?php #get student remarks
                                            $qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$regno'";
                                            $dbremarks = mysqli_query($zalongwa, $qremarks);
                                            $row_remarks = mysqli_fetch_assoc($dbremarks);
                                            $totalremarks = mysqli_num_rows($dbremarks);
                                            $studentremarks = $row_remarks['Remark'];
                                            if (($totalremarks > 0) && ($studentremarks <> '')) {
                                                $remark = $studentremarks;
                                            } else {

                                                if ($gpa >= 4.4) {
                                                    $remark = 'PASS';
                                                    echo 'A';
                                                } elseif ($gpa >= 3.5) {
                                                    $remark = 'PASS';
                                                    echo 'B+';
                                                } elseif ($gpa >= 2.7) {
                                                    $remark = 'PASS';
                                                    echo 'B';
                                                } elseif ($gpa >= 2.0) {
                                                    $remark = 'PASS';
                                                    echo 'C';
                                                } elseif ($gpa >= 1.0) {
                                                    $remark = 'FAIL';
                                                    echo 'D';
                                                } else {
                                                    $remark = 'FAIL';
                                                    echo 'E';
                                                }
                                            }
                                            ?></div>
                                    </td>
                                    <td class='resViewtd'>
                                        <div align="left">
                                            <?php echo $remark;
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <?php $i = $i + 1;
                        } //ends if $total_rows
                    }//ends $rowstudent loop

                } elseif (($checkdegree == 'on') && ($checkcohot == 'on')) {
//query student list
                    $deg = $_POST['degree'];
                    $year = $_POST['ayear'];
                    $cohot = $_POST['cohot'];
                    $dept = $_POST['dept'];
                    echo "<p><b>Examination Results for Academic Year - " . $year . '&nbsp;-&nbsp;' . $sem;
                    echo "</b></p>";
//query student list
                    $qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY RegNo";
                    $dbstudent = mysqli_query($zalongwa, $qstudent);
                    $totalstudent = mysqli_num_rows($dbstudent);
                    $i = 1;
                    while ($rowstudent = mysqli_fetch_array($dbstudent)) {
                        $name = $rowstudent['Name'];
                        $regno = $rowstudent['RegNo'];
                        $sex = $rowstudent['Sex'];

                        # get all courses for this candidate
                        $qcourse = "SELECT DISTINCT course.Units, course.Department, course.StudyLevel, examresult.CourseCode FROM 
						course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
							 WHERE 
									(RegNo='$regno') AND 
									(course.Programme = '$deg') AND 
									(AYear='$year') ";
                        $dbcourse = mysqli_query($zalongwa, $qcourse) or die("No Exam Results for the Candidate - $key ");
                        $dbcourseUnit = mysqli_query($zalongwa, $qcourse);
                        $total_rows = mysqli_num_rows($dbcourse);

                        if ($total_rows > 0) {

                            #initialise
                            $totalunit = 0;
                            $unittaken = 0;

                            $sgp = 0;
                            $totalsgp = 0;
                            $gpa = 0;
                            $key = $regno;
                            ?>

                            <table class='table_view'>
                                <tr class="list">
                                    <td rowspan="2" nowrap scope="col">
                                        <div align="left"></div> <?php echo $i ?></td>
                                    <td width="13" rowspan="2" nowrap>
                                        <div align="center"><?php echo $name ?></div>
                                    </td>
                                    <td width="13" rowspan="2" nowrap>
                                        <div align="center"><?php echo $regno ?></div>
                                    </td>
                                    <td width="13" rowspan="2" nowrap>
                                        <div align="center"><?php echo $sex ?></div>
                                    </td>
                                    <?php while ($rowcourse = mysqli_fetch_array($dbcourse)) { ?>
                                        <td class='resViewhd'>
                                            <div align="center"><?php echo $rowcourse['CourseCode']; ?></div>
                                        </td>
                                    <?php } ?>
                                    <td class='resViewhd'>
                                        <div align="center">Units</div>
                                    </td>
                                    <td class='resViewhd'>
                                        <div align="center">Points</div>
                                    </td>
                                    <td class='resViewhd'>
                                        <div align="center">GPA</div>
                                    </td>
                                    <td class='resViewhd'>
                                        <div align="center">Grade</div>
                                    </td>
                                    <td class='resViewhd' scope="col">Remarks</td>
                                </tr>
                                <tr>
                                    <?php while ($row_course = mysqli_fetch_array($dbcourseUnit)) {
                                        $course = $row_course['CourseCode'];
                                        $unit = $row_course['Units'];
                                        $name = $row_course['CourseName'];
                                        $coursefaculty = $row_course['Department'];
                                        $sn = $sn + 1;
                                        $remarks = 'remarks';
                                        $RegNo = $key;
                                        #insert grading results
                                        include 'includes/choose_studylevel.php';
                                        ?>

                                        <td class='resViewtd'>
                                            <div align="center"><?php echo $grade; ?></div>
                                        </td>
                                    <?php } //ends while row_course loop

                                    ?>
                                    <td class='resViewtd'>
                                        <div align="center"><?php $gunits = $unittaken + $gunits;
                                            echo $unittaken; ?></div>
                                    </td>
                                    <td class='resViewtd'>
                                        <div align="center"><?php $gpoints = $totalsgp + $gpoints;
                                            echo $totalsgp; ?></div>
                                    </td>
                                    <td class='resViewtd'>
                                        <div align="center"><?php $gpa = @substr($totalsgp / $unittaken, 0, 3);
                                            echo $gpa; ?></div>
                                    </td>
                                    <td class='resViewtd'>
                                        <div align="center"><?php #get student remarks
                                            $qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$regno'";
                                            $dbremarks = mysqli_query($zalongwa, $qremarks);
                                            $row_remarks = mysqli_fetch_assoc($dbremarks);
                                            $totalremarks = mysqli_num_rows($dbremarks);
                                            $studentremarks = $row_remarks['Remark'];
                                            if (($totalremarks > 0) && ($studentremarks <> '')) {
                                                $remark = $studentremarks;
                                            } else {

                                                if ($gpa >= 4.4) {
                                                    $remark = 'PASS';
                                                    echo 'A';
                                                } elseif ($gpa >= 3.5) {
                                                    $remark = 'PASS';
                                                    echo 'B+';
                                                } elseif ($gpa >= 2.7) {
                                                    $remark = 'PASS';
                                                    echo 'B';
                                                } elseif ($gpa >= 2.0) {
                                                    $remark = 'PASS';
                                                    echo 'C';
                                                } elseif ($gpa >= 1.0) {
                                                    $remark = 'FAIL';
                                                    echo 'D';
                                                } else {
                                                    $remark = 'FAIL';
                                                    echo 'E';
                                                }
                                            }
                                            ?></div>
                                    </td>
                                    <td class='resViewtd'>
                                        <div align="left">
                                            <?php echo $remark;
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <?php $i = $i + 1;
                        } //ends if $total_rows
                    }//ends $rowstudent loop

                } elseif ($checkcohot == 'on') {

                    $deg = addslashes($_POST['degree']);
                    $year = addslashes($_POST['ayear']);
                    $cohot = addslashes($_POST['cohot']);
                    echo "<p></b>Examination Results for Academic Year - " . $year . '&nbsp;-&nbsp;' . $sem;
                    echo "</b></p>";
//query student list
                    $qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE EntryYear = '$cohot' ORDER BY RegNo";
                    $dbstudent = mysqli_query($zalongwa, $qstudent);
                    $totalstudent = mysqli_num_rows($dbstudent);
                    $i = 1;
                    while ($rowstudent = mysqli_fetch_array($dbstudent)) {
                        $name = $rowstudent['Name'];
                        $regno = $rowstudent['RegNo'];
                        $sex = $rowstudent['Sex'];

                        # get all courses for this candidate
                        $qcourse = "SELECT DISTINCT course.Units, course.Department, course.StudyLevel, examresult.CourseCode FROM 
						course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
							 WHERE 
									(RegNo='$regno') AND
									(course.Programme = '$deg') 
						";
                        $dbcourse = mysqli_query($zalongwa, $qcourse) or die("No Exam Results for the Candidate - $key ");
                        $dbcourseUnit = mysqli_query($zalongwa, $qcourse);
                        $total_rows = mysqli_num_rows($dbcourse);

                        if ($total_rows > 0) {

                            #initialise
                            $totalunit = 0;
                            $unittaken = 0;
                            $sgp = 0;
                            $totalsgp = 0;
                            $gpa = 0;
                            $key = $regno;
                            ?>

                            <table height="100%">
                                <tr>
                                    <td class='resViewhd' width="20" rowspan="2" nowrap scope="col">
                                        <div align="left"></div> <?php echo $i ?></td>
                                    <td class='resViewhd' width="160" rowspan="2" nowrap
                                        scope="col"><?php echo $name . ": " . $regno; ?> </td>
                                    <td class='resViewhd' width="13" rowspan="2" nowrap>
                                        <div align="center"><?php echo $sex ?></div>
                                    </td>
                                    <?php while ($rowcourse = mysqli_fetch_array($dbcourse)) { ?>
                                        <td class='resViewhd'>
                                            <div align="center"><?php echo $rowcourse['CourseCode']; ?></div>
                                        </td>
                                    <?php } ?>
                                    <td class='resViewhd'>
                                        <div align="center">Units</div>
                                    </td>
                                    <td class='resViewhd'>
                                        <div align="center">Points</div>
                                    </td>
                                    <td class='resViewhd'>
                                        <div align="center">GPA</div>
                                    </td>
                                    <td class='resViewhd'>
                                        <div align="center">Grade</div>
                                    </td>
                                    <td class='resViewhd' scope="col">Remarks</td>
                                </tr>
                                <tr>
                                    <?php while ($row_course = mysqli_fetch_array($dbcourseUnit)) {
                                        $course = $row_course['CourseCode'];
                                        $unit = $row_course['Units'];
                                        $name = $row_course['CourseName'];
                                        $coursefaculty = $row_course['Department'];
                                        $sn = $sn + 1;
                                        $remarks = 'remarks';

                                        $RegNo = $key;
                                        #insert grading results
                                        include 'includes/choose_studylevel.php';
                                        ?>

                                        <td class='resViewtd'>
                                            <div align="center"><?php echo $grade; ?></div>
                                        </td>
                                    <?php } //ends while row_course loop

                                    ?>
                                    <td class='resViewtd'>
                                        <div align="center"><?php $gunits = $unittaken + $gunits;
                                            echo $unittaken; ?></div>
                                    </td>
                                    <td class='resViewtd'>
                                        <div align="center"><?php $gpoints = $totalsgp + $gpoints;
                                            echo $totalsgp; ?></div>
                                    </td>
                                    <td class='resViewtd'>
                                        <div align="center"><?php $gpa = @substr($totalsgp / $unittaken, 0, 3);
                                            echo $gpa; ?></div>
                                    </td>
                                    <td class='resViewtd'>
                                        <div align="center"><?php #get student remarks
                                            $qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$regno'";
                                            $dbremarks = mysqli_query($zalongwa, $qremarks);
                                            $row_remarks = mysqli_fetch_assoc($dbremarks);
                                            $totalremarks = mysqli_num_rows($dbremarks);
                                            $studentremarks = $row_remarks['Remark'];
                                            if (($totalremarks > 0) && ($studentremarks <> '')) {
                                                $remark = $studentremarks;
                                            } else {

                                                if ($gpa >= 4.4) {
                                                    $remark = 'PASS';
                                                    echo 'A';
                                                } elseif ($gpa >= 3.5) {
                                                    $remark = 'PASS';
                                                    echo 'B+';
                                                } elseif ($gpa >= 2.7) {
                                                    $remark = 'PASS';
                                                    echo 'B';
                                                } elseif ($gpa >= 2.0) {
                                                    $remark = 'PASS';
                                                    echo 'C';
                                                } elseif ($gpa >= 1.0) {
                                                    $remark = 'FAIL';
                                                    echo 'D';
                                                } else {
                                                    $remark = 'FAIL';
                                                    echo 'E';
                                                }
                                            }
                                            ?></div>
                                    </td>
                                    <td class='resViewtd'>
                                        <div align="left">
                                            <?php echo $remark;
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <?php $i = $i + 1;
                        } //ends if $total_rows
                    }//ends $rowstudent loop
                }
            } elseif (isset($_POST['programme'])) {

                $prgcomb = $_POST['degree'];
                $cat = $_POST['cat'];

                $qprogn = "SELECT ProgrammeCode, Title FROM programme WHERE ProgrammeCode='$prgcomb'";
                $dbprogn = mysqli_query($zalongwa, $qprogn);
                $row_progn = mysqli_fetch_array($dbprogn);
                $progname = $row_progn['Title'];
                if ($cat == 4) {
                    $repeaters = '- REPEATERS';
                }

                ?>
                <?php echo '<div style="font-weight:bold;"> PROGRAMME : ' . strtoupper($progname) . ' ANNUAL REPORT &nbsp;&nbsp;' . $repeaters . '</div>'; ?>

                <div class="top-card" style="margin:10px 0 5px 0px; font-style:italic;">If you want to filter the
                    results by
                    criteria,<br>Tick the corresponding check box first then select appropriately
                </div>

                <div class="card">
                    <h3 class="card-header">
                        Change Password</h3>
                    <div class="card-block">
                        <form name="form1" method="post" action="<?php echo $editFormAction ?>">
                            <input name="checkdegree" type="hidden" id="checkdegree" value="on" checked>
                            <input name="degree" type="hidden" id="degree" value="<?php echo $prgcomb; ?>">
                            <input name="cat" type="hidden" id="cat" value="<?php echo $cat; ?>">


                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><input name="checkcohot"
                                                                                  type="checkbox"
                                                                                  id="checkcohot"
                                                                                  value="on"
                                                                                  checked>&nbsp;&nbsp;Cohort
                                        of the Year:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="cohot" id="cohot">
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_ayear['AYear'] ?>"><?php echo $row_ayear['AYear'] ?></option>
                                                <?php
                                            } while ($row_ayear = mysqli_fetch_assoc($ayear));
                                            $rows = mysqli_num_rows($ayear);
                                            if ($rows > 0) {
                                                mysqli_data_seek($ayear, 0);
                                                $row_ayear = mysqli_fetch_assoc($ayear);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><input name="checkyear" type="checkbox"
                                                                                  id="checkyear" value="on" checked>&nbsp;&nbsp;Results
                                        of the Year:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="ayear" id="ayear">
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_ayear['AYear'] ?>"><?php echo $row_ayear['AYear'] ?></option>
                                                <?php
                                            } while ($row_ayear = mysqli_fetch_assoc($ayear));
                                            $rows = mysqli_num_rows($ayear);
                                            if ($rows > 0) {
                                                mysqli_data_seek($ayear, 0);
                                                $row_ayear = mysqli_fetch_assoc($ayear);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Include Statistical Report? :</label>
                                    <div class="col-sm-8">
                                        <input checked="checked" type="checkbox" value="1" name="st_report"/>&nbsp;&nbsp;YES
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"></label>
                                    <div class="col-sm-8">
                                        <input style="display:none;" type="submit" name="PDF" id="PDF" value="Print PDF"
                                               title="Click to Create a PDF File">
                                        <input class="btn btn-success btn-md btn-block" name="excel" type="submit"
                                               id="excel" value="Generate Report"
                                               title="Click to View the Semester Results">
                                    </div>

                                </div>
                            </div>
                            <input name="MM_update" type="hidden" id="MM_update" value="form1">
                        </form>

                    </div>
                </div>

                <?php
            } else {

                ?>

                <div class="card">
                    <h3 class="card-header">
                        Select a Degree Programme
                    </h3>
                    <div class="card-block">
                        <form name="form2" method="post" action="<?php echo $editFormAction ?>">

                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Category:</label>
                                    <div class="col-sm-8">
                                        <input type="radio" value="1" id="cat" name="cat" checked>&nbsp;&nbsp;First
                                        Sitting
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Degree Programme:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="degree" id="degree">
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_degree['ProgrammeCode'] ?>"><?php echo $row_degree['ProgrammeName'] ?></option>
                                                <?php
                                            } while ($row_degree = mysqli_fetch_assoc($degree));
                                            $rows = mysqli_num_rows($degree);
                                            if ($rows > 0) {
                                                mysqli_data_seek($degree, 0);
                                                $row_degree = mysqli_fetch_assoc($degree);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"></label>
                                    <div class="col-sm-8">
                                        <input type="submit" name="programme" id="programme" value="Load"
                                               class="btn btn-success btn-md btn-block">
                                    </div>
                                </div>
                            </div>
                            <input name="MM_updatep" type="hidden" id="MM_update" value="form2">
                        </form>

                    </div>
                </div>

                <?php

            }
            ?>

        </div>
    </div>
</div>
<br>
<br>
<br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>
