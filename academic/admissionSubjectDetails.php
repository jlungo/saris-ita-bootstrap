<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header
include('lecturerMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Course Information';
$szSubSection = 'Policy Setup';
include("lecturerheader.php");

if (isset($_GET['details'])) {
    $result = $zalongwa->query("SELECT * FROM course WHERE Id=" . $_GET['details']);
    $institution = $result->fetch_assoc();
    if ($result->num_rows > 0) {
        ?>
        <table class='table_view'>
            <tr class='header'>
                <td> Field</td>
                <td> Field Value</td>
            </tr>
            <tr class='list'>
                <td> CourseCode:</td>
                <td><?php echo $institution['CourseCode']; ?></td>
            </tr>
            <tr class='list'>
                <td> Course Name</td>
                <td><?php echo $institution['CourseName']; ?></td>
            </tr>
            <tr class='list'>
                <td> YearOffered:</td>
                <td><?php echo $institution['YearOffered']; ?></td>
            </tr>
            <tr class='list'>
                <td> Capacity:</td>
                <td><?php echo $institution['Capacity']; ?></td>
            </tr>
            <tr class='list'>
                <td> Units:</td>
<!--                <td>--><?php //echo $institution['Units']; ?><!--</td>-->
            </tr>
            <tr class='list'>
                <td> Department:</td>
                <td><?php echo $institution['Department']; ?></td>
            </tr>
            <tr class='list'>
                <td> Exam Regulation:</td>
                <td><?php echo $institution['StudyLevel']; ?></td>
            </tr>
        </table>
        <br>
        <table class='table_view'>
            <tr style='float: right' class='list'>
                <td><a href="admissionSubject.php">Back</a></td>
            </tr>
        </table>
        <?php
    } else {
        echo "Sorry, No Records Found <br>";
    }
}
# include the footer
include("../footer/footer.php");
?>