<?php
require_once('../Connections/sessioncontrol.php');
# include the header

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Policy Setup';
$szSubSection = 'Policy Setup';

require_once('../Connections/zalongwa.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="./css/breadcrumb.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>

        .row {
            margin-top: 20px;
        }
        .card {
            border-top: 7px solid #263238;
            padding-top: 5%;
        }
        .card:focus, .card:hover {
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            /*box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.20);*/
        }
        .card-inverse .card-img-overlay {
            background-color: rgba(51, 51, 51, 0.85);
            border-color: rgba(51, 51, 51, 0.85);
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container text-muted">
    <!-- cards -->
    <div class="row">
        <div class="col-sm-3 col-md-3">
            <div class="card " class="z-depth-5" align="center">

                <a class="nav-link" href="admissionOrg.php"> <img class="card-img-top img-fluid "
                                                                  style="width: 112px; height: inherit; padding-top: 15px; "
                                                                  src="./img/studentregister.svg"></a>
                <div class="card-block">
                    <a class="bootstrap-link" href="admissionOrg.php">Institution
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="card" class="z-depth-5" align="center">

                <a class="nav-link" href="admissionInst.php"> <img class="card-img-top img-fluid "
                                                                   style="width: 112px; height: inherit; padding-top: 15px; "
                                                                   src="./img/studentremark.svg"></a>
                <div class="card-block">
                    <a class="bootstrap-link" href="admissionInst.php">Campus
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="card " class="z-depth-5" align="center">

                <a class="nav-link" href="admissionFaculty.php"> <img class="card-img-top img-fluid "
                                                                      style="width: 112px; height: inherit; padding-top: 15px; "
                                                                      src="./img/limitupload.svg"></a>
                <div class="card-block">
                    <a class="bootstrap-link" href="admissionFaculty.php">Faculty</a>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="card " class="z-depth-5" align="center">

                <a class="nav-link" href="admissionDepartment.php"> <img class="card-img-top img-fluid "
                                                                         style="width: 112px; height: inherit; padding-top: 15px; "
                                                                         src="./img/publishexams.svg"></a>
                <div class="card-block">
                    <a class="bootstrap-link" href="admissionDepartment.php">Department
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3 col-md-3">
            <div class="card " class="z-depth-5" align="center">

                <a class="nav-link" href="admissionProgramme.php"> <img class="card-img-top img-fluid "
                                                                        style="width: 112px; height: inherit; padding-top: 15px; "
                                                                        src="./img/changesemister.svg"></a>
                <div class="card-block">
                    <a class="bootstrap-link" href="admissionProgramme.php">Programme
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="card " class="z-depth-5" align="center">

                <a class="nav-link" href="admissionSubject.php"> <img class="card-img-top img-fluid "
                                                                style="width: 112px; height: inherit; padding-top: 15px; "
                                                                src="./img/classlist2.svg"></a>
                <div class="card-block">
                    <a class="bootstrap-link" href="admissionSubject.php">Module
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="card " class="z-depth-5" align="center">

                <a class="nav-link" href="CourseImport.php"> <img class="card-img-top img-fluid "
                                                                      style="width: 112px; height: inherit; padding-top: 15px; "
                                                                      src="./img/updateclasslist.svg"></a>
                <div class="card-block">
                    <a class="bootstrap-link" href="CourseImport.php">Import Module
                    </a>
                </div>
            </div>
        </div>

    </div>

    <!-- end .container -->
    <br><br><br><br>
    <!--footer-->
    <?php include '../footer/footer.php'; ?>

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
            integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
            integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
            integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
            crossorigin="anonymous"></script>
    <!--adding tooltip-->
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <!--Modernaizer here check if not svg supported replace with png-->
    <script>
        if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
        for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
    </script>
</div>
</body>
</html>
