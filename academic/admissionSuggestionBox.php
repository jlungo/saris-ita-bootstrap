<?php

#get connected to the database and verfy current session

require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

# include the header

global $szSection, $szSubSection;
$szSection = 'Communication';
$szSubSection = 'Suggestion Box';
$szTitle = 'Suggestion Box';

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{

    $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;


    switch ($theType) {

        case "text":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;

        case "long":

        case "int":
            $theValue = ($theValue != "") ? intval($theValue) : "NULL";
            break;

        case "double":
            $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
            break;

        case "date":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;

        case "defined":
            $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
            break;

    }

    return $theValue;

}


$editFormAction = $_SERVER['PHP_SELF'];

if (isset($_SERVER['QUERY_STRING'])) {

    $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);

}


if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmsuggestion")) {

    $insertSQL = sprintf("INSERT INTO suggestion (received, fromid, toid, message) VALUES (now(), %s, %s, %s)",

        GetSQLValueString($_POST['regno'], "text"),
        GetSQLValueString($_POST['toid'], "text"),
        GetSQLValueString($_POST['message'], "text"));


    //mysql_select_db($database_zalongwa, $zalongwa);
    $Result1 = mysqli_query($zalongwa, $insertSQL) or die(mysqli_error($zalongwa));

//show replied

    $id = addslashes($_GET['id']);
    $t = date('l dS \of F Y h:i:s A');

    $qreplied = "UPDATE suggestion SET replied='The message was replied by \'$username\', on: $t' WHERE id='$id'";
    mysqli_query($zalongwa, $qreplied) or die(mysqli_error($zalongwa));


    //$insertGoTo = "housingcheckmessage.php";
    if (isset($_SERVER['QUERY_STRING'])) {

        $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
        $insertGoTo .= $_SERVER['QUERY_STRING'];

    }

    echo '<meta http-equiv = "refresh" content ="0; url = admissionCheckMessage.php">';

}


//mysql_select_db($database_zalongwa, $zalongwa);

$query_suggestionbox = "SELECT suggestion.received, suggestion.fromid, suggestion.toid, suggestion.message FROM suggestion";
$suggestionbox = mysqli_query($zalongwa, $query_suggestionbox) or die(mysqli_error($zalongwa));
$row_suggestionbox = mysqli_fetch_assoc($suggestionbox);
$totalRows_suggestionbox = mysqli_num_rows($suggestionbox);

$RegNo = $_GET['from'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /* background-color: #009688; */
        }

        .card {
            /* background-color: #324359; */
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /* color: white; */
            padding: 0px;
            border-radius: 0px !important;
        }

        @media (max-width: 34em) {
            .card {
                margin-top: 20px;
            }
        }

        @media (max-width: 48em) {
            .card {
                margin-top: 20px;
            }
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <h3 class="card-header">
                    <?php echo $szTitle ?></h3>
                <div class="card-block">
                    <form action="<?php echo $editFormAction; ?>" method="POST" name="frmsuggestion" id="frmsuggestion">

                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Send To</label>
                            <?php
                            $user = $_SESSION['username'];
                            $sql = mysqli_query($zalongwa, "SELECT RegNo FROM security WHERE UserName='$user'");
                            $sql = mysqli_fetch_array($sql);
                            $sender = $sql['RegNo'];
                            ?>
                            <input class='form-control' type='text' name='toid' value='<?php echo $RegNo; ?>' readonly>
                            <input type='hidden' name='regno' value='<?php echo $sender; ?>'>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Message</label>
                            <textarea class="form-control normaltext" name="message" rows="8" id="message"></textarea>
                        </div>
                        <input class="btn btn-success btn-md btn-block" name="Send" type="submit" value="Post Message">
                        <input type="hidden" name="MM_insert" value="frmsuggestion">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>
