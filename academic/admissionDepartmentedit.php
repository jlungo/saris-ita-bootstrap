<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header
include('lecturerMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Department Information';
$szSubSection = 'Policy Setup';
include("lecturerheader.php");

if (isset($_GET['content'])) {
    if ($_POST['update']) {
        $DeptName = addslashes($_POST["DeptName"]);
        $DeptPhysAdd = addslashes($_POST["DeptPhysAdd"]);
        $DeptAddress = addslashes($_POST["DeptAddress"]);
        $DeptEmail = addslashes($_POST["DeptEmail"]);
        $DeptTel = addslashes($_POST["DeptTel"]);
        $DeptHead = addslashes($_POST["DeptHead"]);
        $CampusID = addslashes($_POST["CampusID"]);
        $Faculty = addslashes($_POST["Faculty"]);

        $sql = "UPDATE department SET CampusID='$CampusID', Faculty='$Faculty', DeptName='$DeptName', DeptPhysAdd = '$DeptPhysAdd', DeptAddress = '$DeptAddress', DeptTel = '$DeptTel',
			DeptEmail='$DeptEmail', DeptHead='$DeptHead' WHERE DeptID=" . $_GET['content'];
        if ($zalongwa->query($sql)) {
            echo "<p>Updated successfully</p>";
            //header("Location: admissionDepartment.php");
        } else {
            echo "<p>Failed to update..</p>";
        }
    }
    $result = $zalongwa->query("SELECT * FROM department WHERE DeptID=" . $_GET['content']);
    $institution = $result->fetch_assoc();
    ?>
    <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
        <table class='table_view'>
            <tr class='header'>
                <td> Field</td>
                <td> Field Value</td>
            </tr>
            <tr class='list'>
                <td> Institution: *</td>
                <td>
                    <select name="CampusID" required>
                        <option value="" disabled="disabled"> --select--
                        </option>
                        <?php
                        $query_campus = "SELECT CampusID, Campus FROM campus";
                        $result_campus = $zalongwa->query($query_campus) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
                        while ($campus_row = $result_campus->fetch_array()) {
                            ?>
                            <option <?php if($institution['CampusID'] == $campus_row['CampusID']){?> selected="selected" <?php }?> value="<?php echo $campus_row['CampusID']; ?>"><?php echo $campus_row['Campus']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr class='list'>
                <td> Faculty: *</td>
                <td>
                    <select name="Faculty" required>
                        <option value="" disabled="disabled"> --select--
                        </option>
                        <?php
                        $query_faculty = "SELECT FacultyID, FacultyName FROM faculty";
                        $result_faculty = $zalongwa->query($query_faculty) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
                        while ($faculty_row = $result_faculty->fetch_array()) {
                            ?>
                            <option <?php if($institution['Faculty'] == $faculty_row['FacultyID']){?> selected="selected" <?php }?> value="<?php echo $faculty_row['FacultyID']; ?>"><?php echo $faculty_row['FacultyName']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr class='list'>
                <td> Department Name</td>
                <td><input type="text" id="DeptName" name="DeptName" value="<?php echo $institution['DeptName']; ?>"
                           size="40"></td>
            </tr>
            <tr class='list'>
                <td> Physical Address:</td>
                <td><input type="text" id="DeptPhysAdd" name="DeptPhysAdd" value="<?php echo $institution['DeptPhysAdd']; ?>"
                           size="40"></td>
            </tr>
            <tr class='list'>
                <td> Address:</td>
                <td><input type="text" id="DeptAddress" name="DeptAddress" value="<?php echo $institution['DeptAddress']; ?>"
                           size="40"></td>
            </tr>
            <tr class='list'>
                <td> Telephone:</td>
                <td><input type="text" id="DeptTel" name="DeptTel" value="<?php echo $institution['DeptTel']; ?>" size="40"></td>
            </tr>
            <tr class='list'>
                <td> Email:</td>
                <td><input type="text" id="DeptEmail" name="DeptEmail" value="<?php echo $institution['DeptEmail']; ?>" size="40">
                </td>
            </tr>
            <tr class='list'>
                <td> Dept Head:</td>
                <td><input type="text" id="DeptHead" name="DeptHead" value="<?php echo $institution['DeptHead']; ?>"
                           size="40"></td>
            </tr>
        </table>
        <br>
        <table class='table_view'>
            <tr style='float: right' class='list'>
                <td>
                    <button formaction="admissionDepartment.php">Back</button>
                </td>
                <td><input type="submit" id="submit" name="update" value="update"></td>
            </tr>
        </table>

    </form>

    <?php

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
        $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}
# include the footer
include("../footer/footer.php");
?>