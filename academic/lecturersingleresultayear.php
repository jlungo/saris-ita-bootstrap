<?php
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('lecturerMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Examination';
	$szSubSection = 'Search';
	$szTitle = 'Change Module Academic Year and Semester';
	include('lecturerheader.php');

	$regno=$_GET['RegNo'];
	$key=$_GET['key'];
	$editFormAction = $_SERVER['PHP_SELF'];
	
	
	$query_ayear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
	$ayear = mysqli_query($zalongwa, $query_ayear) or die(mysqli_error($zalongwa));
	$row_ayear = mysqli_fetch_assoc($ayear);
	$totalRows_ayear = mysqli_num_rows($ayear);
	
	
	$query_sem = "SELECT Semester FROM terms ORDER BY Semester LIMIT 2";
	$sem = mysqli_query($zalongwa, $query_sem) or die(mysqli_error($zalongwa));
	$row_sem = mysqli_fetch_assoc($sem);
	$totalRows_sem = mysqli_num_rows($sem);

if (isset($_POST['PDF']) && ($_POST['PDF'] == "Confirm")){
	$year = addslashes($_POST['ayear']);
	$sem = addslashes($_POST['sem']);
	$regno = addslashes($_POST['regno']);
	$oldayear = addslashes($_POST['oldayear']);
	$coursecode = addslashes($_POST['coursecode']);
	
	#update examresult
	$qstatus = "UPDATE examresult SET AYear = '$year', Semester= '$sem' WHERE CourseCode='$coursecode' AND RegNo='$regno'";
	$dbstatus=mysqli_query($zalongwa, $qstatus);
	
	#Refresh exam result page
	echo "Database Updated Successfuly!";
	$_SESSION['search'] = $regno;
	echo '<meta http-equiv = "refresh" content ="0; url = admissionExamResult.php?search=search">';
	
}else{
?>
<form name="form1" method="post" action="<?php echo $editFormAction ?>">
<div align="center">
<table width="200" border="0" bgcolor="#CCCCCC">
<tr>
<td nowrap> Candidate:</td>
<td nowrap><?php echo $regno ?></td>
</tr>
<tr>
<td nowrap> Module Code:</td>
<td nowrap><?php echo  $key ?></td>
</tr>

<tr>
<td nowrap><div align="left">Academic Year: </div></td>
<td><div align="left">
<select name="ayear" id="ayear">
<?php
do {
?>
<option value="<?php echo $row_ayear['AYear']?>"><?php echo $row_ayear['AYear']?></option>
<?php
} while ($row_ayear = mysqli_fetch_assoc($ayear));
$rows = mysqli_num_rows($ayear);
if($rows > 0) {
mysqli_data_seek($ayear, 0);
$row_ayear = mysqli_fetch_assoc($ayear);
}
?>
</select>
</div></td>
</tr>
<tr>
<td nowrap><div align="left">Semester: </div></td>
<td><div align="left">
<select name="sem" id="sem">
<?php
do {
?>
<option value="<?php echo $row_sem['Semester']?>"><?php echo $row_sem['Semester']?></option>
<?php
} while ($row_sem = mysqli_fetch_assoc($sem));
$rows = mysqli_num_rows($sem);
if($rows > 0) {
mysqli_data_seek($sem, 0);
$row_sem = mysqli_fetch_assoc($sem);
}
?>
</select>
</div></td>
</tr>
<tr>
<td colspan="3"><div align="center">
<input type="submit" name="PDF"  id="PDF" value="Confirm">
<input type="hidden" name="regno"  id="regno" value="<?php echo $regno ?>">
<input type="hidden" name="coursecode"  id="coursecode" value="<?php echo $key ?>">

</div></td>
</tr>
</table>
</div>
</form>
<?php }?>