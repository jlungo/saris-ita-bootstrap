<?php 

#start pdf

if (isset($_POST['PrintPDF']) && ($_POST['PrintPDF'] == "Print PDF")) {

	#get post variables

	$rawkey = addslashes(trim($_POST['key']));

	$key = preg_replace("[[:space:]]+", " ",$rawkey);

	#get content table raw height

	$rh= addslashes(trim($_POST['sex']));

	$temp= addslashes(trim($_POST['temp']));

	$award= addslashes(trim($_POST['award']));

	$realcopy= addslashes(trim($_POST['real']));

	

	#CONVERT NTA LEVEL

	if($award==1){

		$ntalevel='NTA LEVEL 8';

	}elseif($award==6){

		$ntalevel='NTA LEVEL 7';

		$award=1;

	}elseif($award==2){

		$ntalevel='NTA LEVEL 6';

	}elseif($award==3){

		$ntalevel='NTA LEVEL 5';

	}elseif($award==4){

		$ntalevel='NTA LEVEL 4';

	}elseif($award==5){

		$ntalevel='Short Course';

	}

	#get connected to the database and verfy current session

	require_once('../Connections/sessioncontrol.php');

	require_once('../Connections/zalongwa.php');

	# check if is a trial print

	if($realcopy==1){

		$copycount = 'TRIAL COPY';

	}

	#check if is a reprint

	$qtranscounter = "SELECT RegNo, received FROM transcriptcount where RegNo='$key'";

	$dbtranscounter = mysqli_query($zalongwa, $qtranscounter);

	@$transcounter = mysqli_num_rows($dbtranscounter);

	

	if ($transcounter>0){

		$row_transcounter = mysqli_fetch_array($dbtranscounter);

		$lastprinted = $row_result['received'];

	}

	#Get Organisation Name

	$qorg = "SELECT * FROM organisation";

	$dborg = mysqli_query($zalongwa, $qorg);

	$row_org = mysqli_fetch_assoc($dborg);

	$org = $row_org['Name'];

	$post = $row_org['Address'];

	$phone = $row_org['tel'];

	$fax = $row_org['fax'];

	$email = $row_org['email'];

	$website = $row_org['website'];

	$city = $row_org['city'];



	include('includes/PDF.php');



	$i=0;

	$pg=1;

	$tpg =$pg;



	$qstudent = "SELECT * from student WHERE regno = '$key'";

	$dbstudent = mysqli_query($zalongwa, $qstudent); 

	$row_result = mysqli_fetch_array($dbstudent);

		$sname = $row_result['Name'];

		$regno = $row_result['RegNo'];

		$degree = $row_result['ProgrammeofStudy'];

		$sex = $row_result['Sex'];

		$dbirth = $row_result['DBirth'];

		$entry = $row_result['EntryYear'];

		$citizen = $row_result['Nationality'];

		$address = $row_result['Address'];

		$gradyear = $row_result['GradYear'];

		$admincriteria = $row_result['MannerofEntry'];

		$campusid = $row_result['Campus'];

		$subjectid = $row_result['Subject'];

		$photo = $row_result['Photo'];

		$checkit = strlen($photo);

		#get campus name

		$qcampus = "SELECT * FROM campus where CampusID='$campusid'";

		$dbcampus = mysqli_query($zalongwa, $qcampus);

		$row_campus= mysqli_fetch_assoc($dbcampus);

		$campus = $row_campus['Campus'];

		

		if ($checkit > 8){

			include '../includes/photoformat.php';

		}else{

			$nophoto = 1;

		}

		#get degree name

		$qdegree = "Select Title, Faculty FROM programme WHERE ProgrammeCode = '$degree'";

		$dbdegree = mysqli_query($zalongwa, $qdegree);

		$row_degree = mysqli_fetch_array($dbdegree);

		$programme = $row_degree['Title'];

		$faculty = $row_degree['Faculty'];

		

		#get subject combination

		$qsubjectcomb = "SELECT SubjectName FROM subjectcombination WHERE SubjectID='$subjectid'";

		$dbsubjectcom = mysqli_query($zalongwa, $qsubjectcomb);

		$row_subjectcom = mysqli_fetch_assoc($dbsubjectcom);

		$counter = mysqli_num_rows($dbsubjectcom );

		if ($counter>0){

		$subject = $row_subjectcom['SubjectName'];

		}



	//require 'PDF.php';                    // Require the lib. 

	$pdf = &PDF::factory('p', 'a4');      // Set up the pdf object. 

	$pdf->open();                         // Start the document. 

	$pdf->setCompression(true);           // Activate compression. 

	$pdf->addPage();  

	

	#include transcript address

	include 'includes/transtemplate.php';

	

	$ytitle = $yadd+72;

	$pdf->setFillColor('rgb', 1, 0, 0);   

	$pdf->setFont('Arial', '', 10); 

  

	$pdf->text(170, $ytitle, 'TRANSCRIPT OF EXAMINATIONS RESULTS'); 

	$pdf->setFillColor('rgb', 0, 0, 0);    



	#title line

	$pdf->line(50, $ytitle+3, 570, $ytitle+3);



	//$pdf->setFont('Arial', 'B', 10.3);     

	#set page header content fonts

	#line1
   $pdf->setFont('Arial', '', 7.3);
	$pdf->line(50, $ytitle+3, 50, $ytitle+15);       

	$pdf->line(130, $ytitle+3,130, $ytitle+15);       

	$pdf->line(230, $ytitle+3, 230, $ytitle+15);

	$pdf->line(260, $ytitle+3, 260, $ytitle+15);       

	$pdf->line(350, $ytitle+3, 350, $ytitle+15); 
	
	$pdf->line(480, $ytitle+3, 480, $ytitle+15); 
	$pdf->line(570, $ytitle+3, 570, $ytitle+15); 
	
	
	//$pdf->setFont('Arial', '', 9.3);
	  
	$pdf->text(52, $ytitle+13, 'Surname');
	$pdf->text(132, $ytitle+13, 'Other Names');
	$pdf->text(232, $ytitle+13, 'Sex');
	$pdf->text(262, $ytitle+13, 'Birthdate');
	$pdf->text(352, $ytitle+13, 'Registration Number');
	$pdf->text(482, $ytitle+13, 'Nationality');
	
    #line 1	
	$pdf->line(50, $ytitle+15, 570, $ytitle+15);
	
	$pdf->line(50, $ytitle+15, 50, $ytitle+30);       
	$pdf->line(130, $ytitle+15,130, $ytitle+30);       
	$pdf->line(230, $ytitle+15, 230, $ytitle+30);
	$pdf->line(260, $ytitle+15, 260, $ytitle+30);       
	$pdf->line(350, $ytitle+15, 350, $ytitle+30); 
	$pdf->line(480, $ytitle+15, 480, $ytitle+30); 
	$pdf->line(570, $ytitle+15, 570, $ytitle+30);
	 
	#line 2
	$pdf->line(50, $ytitle+30, 570, $ytitle+30);
	
	#format name

	$candname = explode(",",$sname);

	$surname = $candname[0];

	$othername = $candname[1];

    $pdf->text(52, $ytitle+28, strtoupper($surname));
    $pdf->text(132, $ytitle+28,strtoupper($othername));
	$pdf->text(232, $ytitle+28,strtoupper($sex));
	$pdf->text(262, $ytitle+28,strtoupper($dbirth));
	$pdf->text(352, $ytitle+28,strtoupper($regno));
	$pdf->text(482, $ytitle+28,strtoupper($citizen));

	
	$pdf->line(50, $ytitle+30, 50, $ytitle+45);       
	$pdf->line(260, $ytitle+30,260, $ytitle+45);       
	$pdf->line(410, $ytitle+30,  410, $ytitle+45);
	$pdf->line(570, $ytitle+30, 570, $ytitle+45);       
	
	$pdf->line(50, $ytitle+45, 570, $ytitle+45);
	
	$pdf->text(52, $ytitle+43,"Programme :");  $pdf->text(100, $ytitle+43,strtoupper($programme));
	
	 $pdf->text(262, $ytitle+43,"Admission Date :"); $pdf->text(320, $ytitle+43,$entry);
	$graddate = explode("-",$gradyear);

	$gradday = $graddate[2];

	$gradmon = $graddate[1];

	$grady = $graddate[0];
    $pdf->text(412, $ytitle+43,"Copmletion Date :");	$pdf->text(490, $ytitle+43, $gradday.' - '.$gradmon.' - '.$grady);
	
	
	
	$pdf->line(50, $ytitle+45, 50, $ytitle+60);
	$pdf->line(260, $ytitle+45, 260, $ytitle+60);
	$pdf->line(570, $ytitle+45, 570, $ytitle+60);
	$pdf->line(50, $ytitle+60, 570, $ytitle+60);
	
	$pdf->text(52, $ytitle+58,"OVERALL GPA :"); 
	
	$pdf->text(262, $ytitle+58,"CLASSFICATION AWARD :"); 
	


	//$pdf->setFont('Arial', 'B', 9.3);  $pdf->text(52, $ytitle+25, 'CITIZENSHIP:'); $pdf->setFont('Arial', 'I', 9.3); $pdf->text(118, $ytitle+25, $citizen); 

	//$pdf->setFont('Arial', 'B', 9.3); 	$pdf->text(190, $ytitle+25, 'ADDRESS:'); $pdf->setFont('Arial', 'I', 9.3); $pdf->text(250, $ytitle+25, $address); 

	#line3

	/*$pdf->line(50, $ytitle+27, 50, $ytitle+39);       

	$pdf->line(188, $ytitle+27, 188, $ytitle+39);       

	$pdf->line(383, $ytitle+27, 383, $ytitle+39);       

	$pdf->line(570, $ytitle+27, 570, $ytitle+39);       

	$pdf->line(50, $ytitle+39, 570, $ytitle+39); 

	

	#Format grad year
/*
	$graddate = explode("-",$gradyear);

	$gradday = $graddate[2];

	$gradmon = $graddate[1];

	$grady = $graddate[0];



	$pdf->setFont('Arial', 'B', 9.3);  $pdf->text(52, $ytitle+37, 'BIRTH DATE:'); $pdf->setFont('Arial', 'I', 9.3); $pdf->text(120, $ytitle+37, $dbirth); 

	$pdf->setFont('Arial', 'B', 9.3); 	$pdf->text(190, $ytitle+37, 'ADMITTED:'); $pdf->setFont('Arial', 'I', 9.3); $pdf->text(250, $ytitle+37, $entry); 

	$pdf->setFont('Arial', 'B', 9.3);  $pdf->text(385, $ytitle+37, 'COMPLETED:'); $pdf->setFont('Arial', 'I', 9.3); $pdf->text(456, $ytitle+37, $gradday.' - '.$gradmon.' - '.$grady); 

/*

	#line4

	$pdf->line(50, $ytitle+39, 50, $ytitle+51);       

	$pdf->line(570, $ytitle+39, 570, $ytitle+51);       

	$pdf->line(50, $ytitle+51, 570, $ytitle+51); 

	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(50, $ytitle+49, 'ADMITTED ON THE BASIS OF:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(205, $ytitle+49, $admincriteria); 

	*/

	#line5
/*
	$pdf->line(50, $ytitle+39, 50, $ytitle+51);       

	$pdf->line(310, $ytitle+39, 310, $ytitle+51);       

	$pdf->line(570, $ytitle+39, 570, $ytitle+51);       

	$pdf->line(50, $ytitle+51, 570, $ytitle+51); 



	$pdf->setFont('Arial', 'B', 9.3);  $pdf->text(52, $ytitle+49, 'CAMPUS:'); $pdf->setFont('Arial', 'I', 9.3); $pdf->text(100, $ytitle+49, $campus); 

	$pdf->setFont('Arial', 'B', 9.3); 	$pdf->text(312, $ytitle+49, 'DEPARTMENT:'); $pdf->setFont('Arial', 'I', 9.3); $pdf->text(387, $ytitle+49, $faculty); 



	#line6

	$pdf->line(50, $ytitle+51, 50, $ytitle+63);       

	$pdf->line(570, $ytitle+51, 570, $ytitle+63);       

	$pdf->line(50, $ytitle+63, 570, $ytitle+63); 

	$pdf->setFont('Arial', 'B', 9.3);  $pdf->text(52, $ytitle+61, 'NAME OF PROGRAMME:'); $pdf->setFont('Arial', 'I', 9.3); $pdf->text(175, $ytitle+61, $programme); 



	#line7

	$pdf->line(50, $ytitle+63, 50, $ytitle+75);       

	$pdf->line(570, $ytitle+63, 570, $ytitle+75);       

	$pdf->line(50, $ytitle+75, 570, $ytitle+75); 

	$pdf->setFont('Arial', 'B', 9.3);  $pdf->text(52, $ytitle+73, 'AWARDS LEVEL:'); $pdf->setFont('Arial', 'I', 9.3); $pdf->text(135, $ytitle+73, $ntalevel.' (Programme Accredited by the National Council for Technical Education)'); 
*/


	$sub =$subjectid;

	if($sub<>0){

		#line7

		$pdf->line(50, $ytitle+75, 50, $ytitle+87);       

		$pdf->line(570, $ytitle+75, 570, $ytitle+87);       

		$pdf->line(50, $ytitle+87, 570, $ytitle+87); 

		$pdf->setFont('Arial', 'B', 9.3);  $pdf->text(50, $ytitle+85, 'MAJOR STUDY AREA:'); $pdf->text(175, $ytitle+85,$subject); 

	}

	#initialize x and y

	$x=50;

	$y=$ytitle+83;

	#initialise total units and total points

	$annualUnits=0;

	$annualPoints=0;

	

	$yval=$y+33;

	$y=$y+33;



	#set page body content fonts

	$pdf->setFont('Arial', '', 9.3);     

	

	//query academeic year

	$qayear = "SELECT DISTINCT AYear FROM examresult WHERE RegNo = '$regno' and checked=1 ORDER BY AYear ASC";

	$dbayear = mysqli_query($zalongwa, $qayear);

	

	#query project

	/*

	$qproject = "SELECT ayear, thesis FROM thesis WHERE RegNo = '$key'";

	$dbproject = mysqli_query($zalongwa, $qproject);

	$row_project = mysqli_fetch_assoc($dbproject);

	$thesisresult = mysqli_num_rows($dbproject);

	$thesis = $row_project['thesis'];

	$thesisyear = $row_project['ayear'];

	*/

	#initialise ayear

	$acyear = 0;

	

	//query exam results sorted per years

	while($rowayear = mysqli_fetch_object($dbayear)){

	$acyear = $acyear +1;

	$currentyear = $rowayear->AYear;

   $pdf->setFont('Arial', '', 7.3);
	

	//query semester

	$qsemester = "SELECT DISTINCT Semester FROM examresult WHERE RegNo = '$regno' AND AYear = '$currentyear' ORDER BY Semester ASC";

	$dbsemester = mysqli_query($zalongwa, $qsemester);

		

		//query exam results sorted per semester

		while($rowsemester = mysqli_fetch_object($dbsemester)){

		$currentsemester = $rowsemester->Semester;

			

		# get all courses for this candidate

		$query_examresult="SELECT DISTINCT course.CourseName, 

						  course.Units, 

						  course.StudyLevel, 

						  course.Department, 

						  examresult.CourseCode, 

						  examresult.Semester, 

						  examresult.Status 

					  FROM 

							course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)

					  WHERE (examresult.RegNo='$regno') AND 

							(examresult.AYear = '$currentyear') AND

							(examresult.Semester = '$currentsemester') AND  

							(examresult.Checked='1') 

				      ORDER BY examresult.AYear DESC, examresult.Semester ASC";	



		$result = mysqli_query($zalongwa, $query_examresult); 

		$query = @mysqli_query($zalongwa, $query_examresult);

		$dbcourseUnit = mysqli_query($zalongwa, $query_examresult);

				

				$totalunit=0;

				$unittaken=0;

				$sgp=0;

				$totalsgp=0;

				$gpa=0;

				#check if u need to sart a new page

				$blank=$y-12;

				$space = 820.89 - $blank;

				if ($space<150){

				#start new page

				$pdf->addPage();  

				

					$x=50;

					$yadd=50;

	

					$y=80;

					$pg=$pg+1;

					$tpg =$pg;

					#insert transcript footer

					include 'includes/transcriptfooter.php';

				}

				#create table header

				if($acyear==1){

					if ($award==3){

						$pdf->text($x, $y-$rh, 'EXAMINATIONS RESULTS: ');

					}else{

						if($temp==2){

						$pdf->text($x, $y-$rh, 'FIRST YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear.' - '.$currentsemester); 

						}else{

						$pdf->text($x, $y-$rh, 'FIRST YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear.' - '.$currentsemester); 

						}

					}

				}elseif($acyear==2){

					if($temp==2){

					$pdf->text($x, $y-$rh, 'SECOND YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear.' - '.$currentsemester); 

					}else{

					$pdf->text($x, $y-$rh, 'SECOND YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear.' - '.$currentsemester); 

					}

				}elseif($acyear==3){

					$pdf->text($x, $y-$rh, 'THIRD YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear.' - '.$currentsemester); 

				}elseif($acyear==4){

					$pdf->text($x, $y-$rh, 'FOURTH YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear.' - '.$currentsemester); 

				}elseif($acyear==5){

					$pdf->text($x, $y-$rh, 'FIFTH YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear.' - '.$currentsemester); 

				}elseif($acyear==6){

					$pdf->text($x, $y-$rh, 'SIXTH YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear.' - '.$currentsemester); 

				}elseif($acyear==7){

					$pdf->text($x, $y-$rh, 'SEVENTH YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear.' - '.$currentsemester); 

				}

				#check result tables to use

				if ($temp ==2)

				{

					#use muchs format

					include 'includes/muchs_result_tables.php';

				}else

				{

					#use general format

					$pdf->text($x+10, $y, 'Code'); 

					$pdf->text($x+70, $y, 'Module Name'); 

					$pdf->text($x+402, $y, 'Credits'); 

					$pdf->text($x+436, $y, 'Grade'); 

					$pdf->text($x+471, $y, 'Points'); 

					$pdf->text($x+499, $y, 'GPA'); 
					

					

					#calculate results

					$i=1;

					while($row_course = mysqli_fetch_array($dbcourseUnit)){

						$course= $row_course['CourseCode'];

						$unit = $row_course['Units'];

						$cname = $row_course['CourseName'];

						$coursefaculty = $row_course['Department'];

						$sn=$sn+1;

						$remarks = 'remarks';

						$grade='';

						/*

						#get specific ourse units

						$qcunits = "select Units from course where (course.Programme = '$degree') AND coursecode = '$course'";

						$dbcunits = mysqli_query($zalongwa, $qcunits);

						$count = mysqli_num_rows($dbcunits);

						if ($count > 0) 

						{

							$unit = $row_cunits['Units'];

						}

						*/

						# grade marks

						$RegNo = $regno;

						include'includes/choose_studylevel.php';

							

							$coursecode = $course;

							

							#print results

							$pdf->text($x+3, $y+$rh, substr($coursecode,0,10)); 

							$pdf->text($x+55, $y+$rh, substr($cname,0,73)); 

							$pdf->text($x+413, $y+$rh, $unit); 

							$pdf->text($x+445, $y+$rh, $grade); 

							$pdf->text($x+477, $y+$rh, $sgp); 

							if ($grade=='I'){

								$gpacomp = 1;

							}elseif($grade=='F'){

								$gpacomp = 1;

							}

							#check if the page is full

							$x=$x;

							#draw a line

							$pdf->line($x, $y-$rh+2, 300, $y-$rh+2);        

							$pdf->line($x, $y-$rh+2, $x, $y);       

							$pdf->line(570.28, $y-$rh+2, 570.28, $y);      

							$pdf->line($x, $y-$rh+2, $x, $y+$rh+4);              

							$pdf->line(570.28, $y-$rh+2, 570.28, $y+$rh+4);      

							$pdf->line($x+498, $y-$rh+2, $x+498, $y+$rh+4);       

							$pdf->line($x+468, $y-$rh+2, $x+468, $y+$rh+4);     

							$pdf->line($x+434, $y-$rh+2, $x+434, $y+$rh+4);       

							$pdf->line($x+400, $y-$rh+2, $x+400, $y+$rh+4);       

							$pdf->line($x+53, $y-$rh+2, $x+53, $y+$rh+2); 

							#get space for next year

							$y=$y+$rh;

	

							if ($y>800){

								#put page header

								//include('PDFTranscriptPageHeader.inc');

								$pdf->addPage();  

	

								$x=50;

								$y=100;

								$pg=$pg+1;

								$tpg =$pg;

							#insert transcript footer

							include 'includes/transcriptfooter.php';

							}

							#draw a line

							$pdf->line($x, $y+$rh+2, 570.28, $y+$rh+2);       

							$pdf->line($x, $y-$rh+2, $x, $y+$rh+2); 

							$pdf->line(570.28, $y-$rh+2, 570.28, $y+$rh+2);      

							$pdf->line($x+498, $y-$rh+2, $x+498, $y+$rh+2);       

							$pdf->line($x+468, $y-$rh+2, $x+468, $y+$rh+2);       

							$pdf->line($x+434, $y-$rh+2, $x+434, $y+$rh+2);      

							$pdf->line($x+400, $y-$rh+2, $x+400, $y+$rh+2); 

							$pdf->line($x+53, $y-$rh+2, $x+53, $y+$rh+2);      

					  }//ends while loop

					  #check degree

					  //if(($degree==632)||($degree==633)||($degree==635)){

					  if ($gpacomp<>1){

							$pdf->setFont('Arial', 'BI', 9.5);     

							$pdf->text($x+2, $y+$rh+1, 'Sub-total');

							$pdf->text($x+413, $y+$rh+1, $unittaken); 

							$pdf->text($x+470, $y+$rh+1, $totalsgp); 

							$pdf->text($x+504, $y+$rh+1,@substr($totalsgp/$unittaken, 0,3)); 

							$pdf->setFont('Arial', '', 9.5); 

					  }

				   } //ends while rowsemester

						#check x,y values

						$y=$y+3.5*$rh;

						//$x=$y+22;

						if ($y==800){

							$pdf->addPage();  



							#put page header

							$x=50;

							$y=80;

							$pg=$pg+1;

							$tpg =$pg;

							#insert transcript content header

							include 'includes/transciptheader';						}

						

	 } //ends while rowayear

						#get annual units and Points

						$annualUnits = $annualUnits+$unittaken;

						$annualPoints = $annualPoints+$totalsgp;



  }

	$avgGPA=@substr($annualPoints/$annualUnits, 0,3);

	#specify degree classification

	if ($award==1){

		if($avgGPA>=4.4){



				$degreeclass = 'First Class';

			}elseif($avgGPA>=3.5){

				$degreeclass = 'Upper Second Class';

			}elseif($avgGPA>=2.7){

				$degreeclass = 'Lower Second Class';

			}elseif($avgGPA>=2.0){

				$degreeclass = 'Pass';

			}else{

				$degreeclass = 'FAIL';

			}

	}elseif($award==2){

	if($avgGPA>=4.4){

				$degreeclass = 'First Class';

			}elseif($avgGPA>=3.5){

				$degreeclass = 'Upper Second Class';

			}elseif($avgGPA>=2.7){

				$degreeclass = 'Lower Second Class';

			}elseif($avgGPA>=2.0){

				$degreeclass = 'Pass';

			}else{

				$degreeclass = 'FAIL';

			}

	}elseif($award==3){

		if($avgGPA>=3.5){

				$degreeclass = 'First Class';

			}elseif($avgGPA>=3.0){

				$degreeclass = 'Second Class';

			}elseif($avgGPA>=2.0){

				$degreeclass = 'Pass';

			}else{

				$degreeclass = 'FAIL';

			}

	}

	$sblank=$y-20;

	$sspace = 820.89 - $sblank;

	if ($sspace<80){

			#start new page

			#put page header

			$pdf->addPage();  



			$x=50;

			$y=80;

			$pg=$pg+1;

			$tpg =$pg;

			#insert transcript footer

			include 'includes/transcriptfooter.php';

	}

	$sub =$subject;

	if($thesisresult>0){

		#print final year project title

		$pdf->line($x, $y-20, 570, $y-20); 

		$pdf->line($x, $y-20, $x, $y-8);       

		$pdf->line(570, $y-20, 570, $y-8);       

		$pdf->line($x, $y-8, 570, $y-8); 

		$pdf->setFont('Arial', 'B', 10.3);  $pdf->text($x+70, $y-10, 'Title of the Final Year Project/Independent Study/Thesis of '.$thesisyear);  

		

		$pdf->line($x, $y-8, $x, $y+4);       

		$pdf->line(570, $y-8, 570, $y+4);       

		$pdf->line($x, $y+4, 570, $y+4); 

		$pdf->setFont('Arial', 'I', 10.3); $pdf->text($x, $y+2, substr($thesis,0,107)); 

	}

	#print gpa

	

	//if(($degree==632)||($degree==633)||($degree==635)){

	if ($gpacomp<>1){

		$pdf->setFont('Arial', 'B', 10.3);  $pdf->text($x, $y+24, 'OVERALL G.P.A.:'); $pdf->text($x+95, $y+24, @substr($annualPoints/$annualUnits, 0,3));

		$pdf->setFont('Arial', 'B', 10.3); 	$pdf->text($x+220, $y+24, 'CLASSIFICATION:'); $pdf->text($x+320, $y+24, $degreeclass);

		$pdf->line($x, $y+27, 570.28, $y+27); 

	}

	/*	

	}else{

	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text($x, $y+24, 'OVERALL PERFORMANCE:'); $pdf->text($x+145, $y+24, 'PASS');

	$pdf->line($x, $y+27, 570.28, $y+27); 

	}

	*/

	$b=$y+27;

	if ($b<820.89){

	#print signature lines

	$pdf->text(59.28, $y+57, '.........................                        .........................................                            ................................');    						

	$pdf->text(60.28, $y+67, $signatory);    	

	}					

	$pdf->setFont('Arial', 'I', 8);      

	$pdf->text(50, 820.89, $city.', '.$today = date("d-m-Y H:i:s"));    

	#print the key index

	$pdf->setFont('Arial', 'I', 9); 

	$yind = $y+87;

	

	#check if there is enough printing area

	$indarea = 820.89-$yind;

	if ($indarea< 203){

			$pdf->addPage();  



			$x=50;

			$y=80;

			$pg=$pg+1;

			$tpg =$pg;

			$pdf->setFont('Arial', 'I', 8);     

			$pdf->text(530.28, 820.89, 'Page '.$pg);  

			$pdf->text(300, 820.89, $copycount);    

			$pdf->text(50, 820.89, $city.', '.$today = date("d-m-Y H:i:s")); 

			$yind = $y; 

    }

	

	include 'includes/transcriptkeys.php';

	#delete imgfile

	@unlink($imgfile); 

	#print the file

	$pdf->output($key.'.pdf');              // Output the 

}/*ends is isset*/

#ends pdf

#get connected to the database and verfy current session

require_once('../Connections/sessioncontrol.php');

require_once('../Connections/zalongwa.php');

# initialise globals

require_once('lecturerMenu.php');



# include the header

global $szSection, $szSubSection;

$szSection = 'Examination';

$szSubSection = 'Cand. Transcript';

$szTitle = 'Transcript of Examination Results';

require_once('lecturerheader.php');



function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 

{

  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;



  switch ($theType) {

    case "text":

      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";

      break;    

    case "long":

    case "int":

      $theValue = ($theValue != "") ? intval($theValue) : "NULL";

      break;

    case "double":

      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";

      break;

    case "date":

      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";

      break;

    case "defined":

      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;

      break;

  }

  return $theValue;

}



$editFormAction = $_SERVER['PHP_SELF'];

if (isset($_SERVER['QUERY_STRING'])) {

  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);

}

if (isset($_POST['search']) && ($_POST['search'] == "PreView")) {



#get post variables

$rawkey = $_POST['key'];

$key = preg_replace("[[:space:]]+", " ",$rawkey);

include 'includes/showexamresults.php';



}else{



?>

<a href="lecturerTranscriptcount.php">Transcript Report</a>

<form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" name="studentRoomApplication" id="studentRoomApplication">

<table width="284" border="1" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">

        <tr>

          <td colspan="9" nowrap><div align="left"></div></td>

        </tr>

        <tr>

          <td nowrap><div align="right"><strong>Award:

            </strong></div>            <div align="center"></div></td>

          <td colspan="8" nowrap><div align="left">

          <input type="hidden" value="1" id="temp" name="temp">

            <select name="award" id="award">

              <option value="1" selected>NTA Level 8</option>

              <option value="6">NTA Level 7</option>

              <option value="2">NTA Level 6 </option>

              <option value="3">NTA Level 5</option>

              <option value="4">NTA Level 4</option>

              <option value="5">Short Course</option>

            </select>

            </div>            <div align="right"></div>            <div align="right"></div></td>

        </tr>

        <tr>

          <td align="right" nowrap><strong> RegNo:</strong></td>

          <td colspan="8" bordercolor="#ECE9D8" bgcolor="#CCCCCC"><span class="style67">

          <input name="key" type="text" id="key" size="40" maxlength="40">

          </span></td>

        </tr>

		<tr> 

			<td align="right" nowrap><strong>Table:</strong></td> 

			<td width="35"><div align="center">11<input type="radio" value="11" id="sex" name="sex"></div></td> 

			<td width="35"><div align="center">12<input type="radio" value="12" id="sex" name="sex" checked></div></td> 

			<td width="35"><div align="center">13<input type="radio" value="13" id="sex" name="sex" ></div></td> 

			<td width="35"><div align="center">14<input type="radio" value="14" id="sex" name="sex" ></div></td> 

			<td width="35"><div align="center">15<input type="radio" value="15" id="sex" name="sex" ></div></td> 

			<td width="35"><div align="center">16<input type="radio" value="16" id="sex" name="sex" ></div></td> 

			<td width="35"><div align="center">-</div></td> 

			<td><div align="left">17<input type="radio" value="17" id="sex" name="sex" ></div></td>

		</tr>

        <tr>

          <td nowrap><div align="right"><strong>Confirmed:

            </strong></div>            

            <div align="center"></div></td>

          <td colspan="3" nowrap><div align="right">No</div></td>

          <td nowrap><input type="radio" value="1" id="real" name="real" checked></td>

          <td colspan="2" nowrap><div align="right"></div></td>

          <td nowrap><div align="right">Yes</div></td>

          <td nowrap><div align="left">

            <input type="radio" value="2" id="real" name="real" >

          </div></td>

        </tr>

        <tr>

          <td nowrap><div align="right"> </div></td>

          <td colspan="4" bgcolor="#CCCCCC">

            <div align="left">

              <input type="submit" name="search" value="PreView">

            </div>            <div align="right">

            </div></td>

          <td colspan="4" nowrap bgcolor="#CCCCCC">

            <div align="right">

              <input name="PrintPDF" type="submit" id="PrintPDF" value="Print PDF">

            </div></td>

        </tr>

  </table>

</form>

<p>&nbsp;</p>

<?php

}

include('../footer/footer.php');

?>

