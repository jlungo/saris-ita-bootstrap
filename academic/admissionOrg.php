<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Institution Information';
$szSubSection = 'Policy Setup';

if (isset($_GET['delete'])) {
    $zalongwa->query("DELETE FROM organisation WHERE Id=" . $_GET['delete']);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <style>
        body {
            background-color: #eff0f1;
        }

        a:hover {
            text-decoration: none;
            color: #0056b3;
        }

        .navbar {
            width: 100%;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
        }

        .navbar-toggler {
            cursor: pointer;
            outline: 0;
            padding-top: inherit;
        }

        @media (max-width: 34em) {
            .navbar {
                padding-top: 5px;
                padding-bottom: 0px;
                background-color: #FAFAFA;
                overflow: hidden;
            }

            .navbar-toggler {
                cursor: pointer;
                outline: 0;
                padding-top: inherit;
            }

            .nav-link {
                color: whitesmoke;
            }

            .nav-item {
                padding-top: 30px;
                padding-bottom: 0px;
                margin-bottom: -3px;
            }

            .row {
                margin: -7%;
            }

            .card {
                margin: 2%;
            }
        }

        @media (max-width: 48em) {
            .card h5 {
                font-size: 14px;
            }
        }

        .row {
            margin-top: 20px;
        }

        .card {
            border-top: 7px solid #263238;
            padding-top: 5%;
        }

        .card-block a {
            color: #263238;
        }

        footer {
            bottom: 0;
            width: 100%;
            margin-top: 20px;
            padding: 2px;
        }

        footer p {
            margin-top: 0;
            margin-bottom: 1rem;
            font-size: small;
            margin: 0px;
        }

        a {
            -webkit-transition: .25s all;
            transition: .25s all;
        }

        .card:focus, .card:hover {
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            /*box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.20);*/
        }

        .card-inverse .card-img-overlay {
            background-color: rgba(51, 51, 51, 0.85);
            border-color: rgba(51, 51, 51, 0.85);
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>

</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container-flex">
</div>
<div class="container">
    <h3 class="h3"><?php echo $szTitle ?></h3>
    <?php
    switch ($_GET['content']) {
        default:
            $query = "SELECT Id, Name, Address, tel, fax, email, website, city FROM organisation";
            $result_sql = $zalongwa->query($query) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
            ?>
            <table class="table table-striped table-bordered table-responsive" width="100%" cellspacing="0">
                <thead class="table-inverse">
                <tr>
                    <th><a href="admissionOrg.php?content=AddInstitution">+Add</a></th>
                    <th> Name</th>
                    <th> Address</th>
                    <th> Website</th>
                    <th> Tel</th>
                    <th> Email</th>
                    <th> View</th>
                    <th> Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($result = $result_sql->fetch_array()) {
                    $Id = $result['Id'];
                    $Name = stripslashes($result["Name"]);
                    $Address = stripslashes($result["Address"]);
                    $tel = stripslashes($result["tel"]);
                    $fax = stripslashes($result["fax"]);
                    $email = stripslashes($result["email"]);
                    $website = stripslashes($result["website"]);
                    $city = stripslashes($result["city"]);
                    ?>
                    <tr>
                        <td><a class="remove-blue-link" href="admissionOrgedit.php?content=<?php echo $Id; ?>">Edit</a>
                        </td>
                        <td><?php echo $Name; ?></td>
                        <td><?php echo $Address; ?></td>
                        <td><?php echo $website; ?></td>
                        <td><?php echo $tel; ?></td>
                        <td><?php echo $email; ?></td>
                        <td><a href="admissionOrgDetails.php?details=<?php echo $Id; ?>">Details</a></td>
                        <td class="center"><a href="admissionOrg.php?delete=<?php echo $Id; ?>"
                                              onClick="return confirm('Are you sure you want to delete <?php echo $Name; ?>')"><i
                                        class="fa fa-trash"></i></a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <?php
            break;

        case
        "AddInstitution":
            function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
            {
                $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

                switch ($theType) {
                    case "text":
                        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                        break;
                    case "long":
                    case "int":
                        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                        break;
                    case "double":
                        $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                        break;
                    case "date":
                        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                        break;
                    case "defined":
                        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                        break;
                }
                return $theValue;
            }

            if (isset($_POST["submit"])) {
                $sql_insert = sprintf("INSERT INTO organisation (Name, Address, tel, fax, email, website, city)
	  													VALUES (%s, %s, %s, %s, %s, %s, %s)",
                    GetSQLValueString($_POST['Name'], "text"),
                    GetSQLValueString($_POST['Address'], "text"),
                    GetSQLValueString($_POST['tel'], "text"),
                    GetSQLValueString($_POST['fax'], "text"),
                    GetSQLValueString($_POST['email'], "text"),
                    GetSQLValueString($_POST['website'], "text"),
                    GetSQLValueString($_POST['city'], "text"));
                if ($zalongwa->query($sql_insert)) {
                    echo "<p style='color: green'> Institution created successfully...</p>";
                } else {
                    die("Cannot query the database.<br>" . $zalongwa->connect_error);
                }
            }
            ?>
            <div class="container ">
                <div class="row ">
                    <div class="col-sm-8 offset-sm-2">
                        <div class="card">
                            <div class="card-block">
                                <form action="admissionOrg.php?content=AddInstitution" method="POST">
                                    <p>* Items marked with an asterisk are required</p>
                                    <div class="container">
                                        <div class="form-group row">
                                            <label for="Name" class="col-sm-4 col-form-label">Name *</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="Name" name="Name" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="form-group row">
                                            <label for="PhysicalAddress" class="col-sm-4 col-form-label">Physical
                                                Address *</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="Address" name="Address"
                                                       required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="form-group row">
                                            <label for="City" class="col-sm-4 col-form-label">City</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="city" name="city">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="form-group row">
                                            <label for="City" class="col-sm-4 col-form-label">Website URL </label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="website" name="website">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="form-group row">
                                            <label for="City" class="col-sm-4 col-form-label">Telephone</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="tel" name="tel" size="40">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="form-group row">
                                            <label for="City" class="col-sm-4 col-form-label">Fax</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="fax" name="fax">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="form-group row">
                                            <label for="City" class="col-sm-4 col-form-label">Email</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="email" name="email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="form-group row">
                                            <label for="Button" class="col-sm-4 col-form-label"></label>
                                            <div class="col-sm-8">
                                                <input class="btn btn-success btn-md btn-block" type="submit"
                                                       id="submit" name="submit" value="create">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
    }
    ?>
</div>
<br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<!--script for datatable-->
</body>
</html>