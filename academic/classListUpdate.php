<?php

#get connected to the database and verfy current session

require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

$szSection = 'Administration';
$szTitle = 'Update Class List';
$szSubSection = 'Update Class List';

$query_AcademicYear = "SELECT DISTINCT AYear FROM academicyear ORDER BY AYear DESC";
$AcademicYear = $zalongwa->query($query_AcademicYear) or die($zalongwa->connect_error);
$row_AcademicYear = $AcademicYear->fetch_assoc();
$totalRows_AcademicYear = $AcademicYear->num_rows;


$query_Hostel = "SELECT ProgrammeCode, ProgrammeName FROM programme ORDER BY ProgrammeName ASC";
$Hostel = $zalongwa->query($query_Hostel) or die($zalongwa->connect_error);
$row_Hostel = $Hostel->fetch_assoc();
$totalRows_Hostel = $Hostel->num_rows;


$query_sessions = "SELECT id, name FROM classstream";
$sessions = $zalongwa->query($query_sessions) or die($zalongwa->connect_error);
$row_sesssions = $sessions->fetch_assoc();
$totalRows_Sessions = $sessions->num_rows;


?>
<script type="text/javascript" language="javascript">// <![CDATA[
    function checkAll(formname, checktoggle) {
        var checkboxes = new Array();
        checkboxes = document[formname].getElementsByTagName('input');

        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type == 'checkbox') {
                checkboxes[i].checked = checktoggle;
            }
        }
    }

    // ]]></script>


<script type='text/javascript'>

    function confirmupdate(data) {

        if (confirm("Are you sure you want to update the record      " + data + '\n Please review before submit')) {

            return true;

        } else {

            return false;

        }

    }

</script>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="./css/breadcrumb.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
        body {
            /*background-color: #009688;*/
        }

        .card {
            /*background-color: #324359;*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /*color: white;*/
            padding: 0px;
            border-radius: 0px !important;
        }

        .row {
            margin-top: 20px;
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<div class="container ">

    <?php

    if (isset($_POST['update'])) {

        $Eq = $_POST[Eq];

        $count = count($Eq);

        if ($count <= 0) {

            echo "<table>

<tr><td>

<font color='red'>Choose Student please.</font>

</td></tr>

</table>";

        } else {

            $e = 1;
            $c = 0;
            for ($j = 0; $j < $count; $j++) {

#Fetch Records

                $sql = "SELECT * FROM student WHERE Id ='$Eq[$j]'";

                $update = $zalongwa->query($sql) or die($zalongwa->connect_error);

                $update_row = $update->fetch_array() or die($zalongwa->connect_error);

                $regno = addslashes($update_row['RegNo']);
                $yearofstudy = addslashes($_POST['yearofstudy']);
                $ayear = addslashes($_POST['year']);
                $ActUser = $_SESSION['username'];


# check wrong yearofstudy for specific year
                $sqlcheck = "SELECT YearofStudy FROM class WHERE AYear='$ayear' AND RegNo='$regno'";
                $resultcheck = $zalongwa->query($sqlcheck);
                $numcheck = $resultcheck->num_rows;
                $rowcheck = $resultcheck->fetch_array();
                if ($numcheck > 0 && $rowcheck['YearofStudy'] <> $yearofstudy) {
                    #update record
                    $sql = "UPDATE class SET YearofStudy='$yearofstudy',Recorder='$ActUser' WHERE AYear='$ayear' AND RegNo='$regno'";
                } else {
                    #insert record
                    $sql = "INSERT INTO class SET AYear='$ayear',RegNo='$regno',YearofStudy='$yearofstudy',Recorder='$ActUser'";
                }

//echo $sql;

                $plg = $zalongwa->query($sql);

                if ($plg) {
                    $c++;

                } else {
                    echo "Student with Reg # $regno exist in this class list->Fail to update<br/>";
                }

            }

        }
        if ($c > 0) {
            echo '<meta http-equiv = "refresh" content ="0; 
							url = classListUpdate.php?success=1&size=' . $c . '">';
            exit;
        }

    }
    ?>

    <!--    Begin of Filter Panel-->

    <div class="row ">
        <div class="col-sm-8">
            <div class="card">
                <h3 class="card-header"><?php echo $szTitle; ?></h3>
                <div class="card-block">

                    <form action='<?php echo $_SERVER['PHP_SELF']; ?>' method='POST'>

                        <div class="container">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Programme:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="programme" id="programme">
                                        <option value="">Select programme</option>
                                        <?php
                                        do {
                                            if (isset($_GET['programme']) && $_GET['programme'] == $row_Hostel['ProgrammeCode']) {
                                                $progselected = ' selected="selected" ';
                                            } else {
                                                $progselected = '';
                                            }

                                            ?>
                                            <option value="<?php echo $row_Hostel['ProgrammeCode'] ?>" <?php echo $progselected; ?>><?php echo $row_Hostel['ProgrammeName'] ?></option>
                                            <?php
                                        } while ($row_Hostel = $Hostel->fetch_assoc());
                                        $rows = $Hostel->num_rows;
                                        if ($rows > 0) {
                                            mysqli_data_seek($Hostel, 0);
                                            $row_Hostel = $Hostel->fetch_assoc();
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Class:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="yearofstudy">

                                        <option value="">Select year of study</option>
                                        <option value="1" <?php echo (isset($_GET['yearofstudy']) && $_GET['yearofstudy'] == 1) ? ' selected="selected" ' : ''; ?>>
                                            First Year
                                        </option>
                                        <option value="2" <?php echo (isset($_GET['yearofstudy']) && $_GET['yearofstudy'] == 2) ? ' selected="selected" ' : ''; ?>>
                                            Second Year
                                        </option>
                                        <option value="3" <?php echo (isset($_GET['yearofstudy']) && $_GET['yearofstudy'] == 3) ? ' selected="selected" ' : ''; ?>>
                                            Third Year
                                        </option>
                                        <option value="4" <?php echo (isset($_GET['yearofstudy']) && $_GET['yearofstudy'] == 4) ? ' selected="selected" ' : ''; ?>>
                                            Fourth Year
                                        </option>
                                        <option value="5" <?php echo (isset($_GET['yearofstudy']) && $_GET['yearofstudy'] == 5) ? ' selected="selected" ' : ''; ?>>
                                            Fifth Year
                                        </option>
                                        <option value="6" <?php echo (isset($_GET['yearofstudy']) && $_GET['yearofstudy'] == 6) ? ' selected="selected" ' : ''; ?>>
                                            Sixth Year
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Year:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="year" id="year">
                                        <option value="">Select academic year</option>
                                        <?php
                                        do {
                                            if (isset($_GET['year']) && $_GET['year'] == $row_AcademicYear['AYear']) {
                                                $yearselected = ' selected="selected" ';
                                            } else {
                                                $yearselected = '';
                                            }
                                            ?>
                                            <option value="<?php echo $row_AcademicYear['AYear'] ?>" <?php echo $yearselected; ?>><?php echo $row_AcademicYear['AYear'] ?></option>
                                            <?php
                                        } while ($row_AcademicYear = $AcademicYear->fetch_assoc());
                                        $rows = $AcademicYear->num_rows;
                                        if ($rows > 0) {
                                            mysqli_data_seek($AcademicYear, 0);
                                            $row_AcademicYear = $AcademicYear->fetch_assoc();
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"></label>
                                <div class="col-sm-8">
                                    <input type='submit' name='search' value='Go'
                                           class="btn btn-success btn-md btn-block">
                                </div>
                            </div>
                        </div>

                    </form>
                    <?php

                    echo isset($_GET['success']) ? '<div style="color:#00FF00">' . $_GET['size'] . ' records updated successfully</div>' : '';

                    if (isset($_POST['search']) || $_GET['key']) {
                    $programme = $_POST['programme'];
                    $yearofstudy = $_POST['yearofstudy'];
                    $year = $_POST['year'];
                    $key = $_POST['key'];
                    $sessid = $_POST['sess'];
                    if ($sessid == 3) {
                        $query = "SELECT * FROM  student where ProgrammeOfStudy='$programme' AND RegNo IN (SELECT RegNo FROM class WHERE AYear='$year' AND YearofStudy='$yearofstudy')";
                    } else {
                        $query = "SELECT * FROM  student where 
				ProgrammeOfStudy='$programme' AND 
				RegNo IN (SELECT RegNo FROM class WHERE AYear='$year' AND YearofStudy='$yearofstudy')";
                    }


                    $result = $zalongwa->query($query) or die($zalongwa->connect_error);

                    echo "<form action='$_SERVER[PHP_SELF]' method='POST' name='frm1'>";
                    ?>

                    <div class="container">
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-8">
                                <p style="text-align: center; margin-top: 20px;"><strong>TO </strong> <span
                                            class="fa fa-arrow-down"></span></p>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-4 col-form-label">Class:</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="yearofstudy">
                                    <?php include '../includes/year_of_study.php' ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-4 col-form-label">Year:</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="year" id="year">
                                    <option value="">Select academic year</option>
                                    <?php
                                    do {
                                        ?>
                                        <option value="<?php echo $row_AcademicYear['AYear'] ?>"><?php echo $row_AcademicYear['AYear'] ?></option>
                                        <?php
                                    } while ($row_AcademicYear = $AcademicYear->fetch_assoc());
                                    $rows = $AcademicYear->num_rows;
                                    if ($rows > 0) {
                                        mysqli_data_seek($AcademicYear, 0);
                                        $row_AcademicYear = $AcademicYear->fetch_assoc();
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-8">
                                <input class="btn btn-success btn-md btn-block" type='submit' name='update'
                                       value='Update' id="All selected Students ?"
                                       onClick="return confirmupdate(this.id)">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<br/>

    <div class="container row">
        <div class="col col-sm-6">
            <p><strong>Select Student(s) to Update</strong></p>
        </div>
        <div class="col col-sm-6">
            <a onclick="javascript:checkAll('frm1', true);" href="javascript:void();"><strong>check all</strong></a>
            |
            <a onclick="javascript:checkAll('frm1', false);" href="javascript:void();"><strong>uncheck all</strong></a>
        </div>
    </div>

<?php

echo "<table class=\"table table-bordered table-responsive\" width=\"100%\" cellspacing=\"0\">

<thead class='table-inverse'>
<tr>

<th>SN</th>

<th>Student Name</th>
<th>RegNo</th>
<th>Programme</th>
<th>GPA</th>

<th>Select
</th>

</tr>
</thead>";

while ($r = $result->fetch_array()) {
    ################## CALL GPA CALCULATOR #########################
    $regnumber = $r['RegNo'];
    include 'classList_gpa.php';
    {
        $result_prog = $zalongwa->query("SELECT Title FROM programme WHERE ProgrammeCode='$r[ProgrammeofStudy]'");
        $row_prog = $result_prog->fetch_array();
        $prog = $row_prog['Title'];
        if ($gpa > 2.0) {
            echo "<tr>

<td>&nbsp;$k</td>
<td>&nbsp;$r[Name]</td>
<td>&nbsp;$r[RegNo]</td>
<td>&nbsp;$prog</td>
<td>&nbsp;$gpa</td>
<td>
<input type='checkbox' name='Eq[]' value='$r[Id]' class='cb-element'>
</td>
</tr>";
            $k++;

        } else {
            echo "<tr bgcolor='#FF0000'>
<td>&nbsp;$k</td>
<td>&nbsp;$r[Name]</td>
<td>&nbsp;$r[RegNo]</td>
<td>&nbsp;$prog</td>
<td>&nbsp;$gpa</td>
<td>
<input type='checkbox' name='Eq[]' value='$r[Id]' class='cb-element'>
</td>
</tr>";
            $k++;

        }
    }
}
echo "</table>";

//$data=$zalongwa->query($look);
//$numrows=mysql_num_rows($data);
//$result=$zalongwa->query($data);
//$row=mysql_fetch_array($data);

$self = $_SERVER['PHP_SELF'];

echo "<table>

<tr>

<td>&nbsp;&nbsp;&nbsp;<font color='#CCCCCC'></font></td>

</tr></table></center>";

//End of Pagination
?>
<?php
}
echo "</form>";
?>

</div>
<br>
<br>
<br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>
