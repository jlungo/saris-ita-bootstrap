<?php
#get connected to the database and verify current session
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Administration';
$szTitle = 'Student Register';
$szSubSection = 'Student Register';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="./css/breadcrumb.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>

    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'academicNavBar.php'; ?>

<?php
#populate academic year Combo Box
$query_paytype = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$paytype = mysqli_query($zalongwa, $query_paytype) or die(mysqli_error($zalongwa));
$row_paytype = mysqli_fetch_assoc($paytype);
$totalRows_paytype = mysqli_num_rows($paytype);

#populate semester Box
$query_semester = "SELECT Description FROM terms ORDER BY Semester ASC";
$semester = mysqli_query($zalongwa, $query_semester) or die(mysqli_error($zalongwa));
$row_semester = mysqli_fetch_assoc($semester);
$totalRows_semester = mysqli_num_rows($semester);

#populate course list combo box
#get current year
$qcurrentyear = mysqli_query($zalongwa, "SELECT AYear FROM academicyear where Status = 1");
$row_current = mysqli_fetch_array($qcurrentyear);
$curyear = $row_current['AYear'];

if ($privilege == 3) {
    $query_course = "SELECT DISTINCT course.CourseCode
FROM examregister
INNER JOIN course ON (examregister.CourseCode = course.CourseCode)
WHERE (examregister.AYear ='$curyear')
AND (examregister.RegNo='$username')  ORDER BY course.CourseCode DESC";
} else {
    $whereclause = ($userDeptHead == 1) ? " WHERE Department IN (SELECT DeptName FROM department WHERE DeptID='$userDept' ) " : "";
    $query_course = "SELECT CourseCode FROM course $whereclause ORDER BY CourseCode";
}
#$query_course = "SELECT CourseCode, CourseName FROM course ORDER BY CourseCode ASC";
$course = mysqli_query($zalongwa, $query_course) or die(mysqli_error($zalongwa));
$row_course = mysqli_fetch_assoc($course);
$totalRows_course = mysqli_num_rows($course);
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
    $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

    switch ($theType) {
        case "text":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "long":
        case "int":
            $theValue = ($theValue != "") ? intval($theValue) : "NULL";
            break;
        case "double":
            $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
            break;
        case "date":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "defined":
            $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
            break;
    }
    return $theValue;
}

function add($f)
{
    global $errorindicator, $errorclass, $Javascript;
    $tocheck = explode(',', ',' . $_POST['required']);
    preg_match('/id="(.*?)"/i', $f, $i);
    preg_match('/name="(.*?)"/i', $f, $n);
    preg_match('/type="(.*?)"/i', $f, $t);
    preg_match('/value="(.*?)"/i', $f, $iv);
    $n = $n[1];
    $iv = $iv[1];
    $i = str_replace('_', ' ', $i[1]);
    if (preg_match('/<textarea/', $f)) {
        $v = $_POST[$n] == '' ? $i : $_POST[$n];
        $f = preg_replace('/<textarea(.*?)>(.*?)<\/textarea>/',
            '<textarea\\1>' . stripslashes(htmlentities($v)) . '</textarea>', $f);
        if ($Javascript) {
            $f = preg_replace('/<textarea/',
                '<textarea onfocus="this.value=this.value==\'' .
                $i . '\'?\'\':this.value"', $f);
        }
    }
    if (@preg_match('/<select/', $f)) {
        @preg_match('/<select.*?>
<style type="text/css">
    <!--
    .style4 {color: #CCCCCC}
    -->
</style>
/', $f, $st);
        @preg_match_all('/<option value="(.*?)">(.*?)<\/option>/', $f, $allopt);
        foreach ($allopt[0] as $k => $a) {
            if ($_POST[$n] == $allopt[1][$k] ||
                ($_POST[$n] == '' && $k == 0)) {
                $preg = '/<option value="';
                $preg .= $allopt[1][$k] . '">' . $allopt[2][$k] .
                    '<\/option>/si';
                $rep = '<option selected="selected" value="';
                $rep .= $allopt[1][$k] . '">' . $allopt[2][$k] .
                    '</option>';
                $f = preg_replace($preg, $rep, $f);
            }
        }
    } else {
        switch ($t[1]) {
            case 'text':
                $v = $_POST[$n] == '' ? 'value="' . $i . '"' : 'value="' .
                    stripslashes(htmlentities($_POST[$n])) . '"';
                $f = preg_replace('/<input/', '<input ' . $v, $f);
                if ($Javascript) {
                    $f = preg_replace('/<input/',
                        '<input onfocus="this.value=this.value==\''
                        . $i . '\'?\'\':this.value"', $f);
                }
                break;
        }
    }
    $f .= '<input type="hidden" name="' . $n . 'initvalue" value="' . $i . '" />';
    if (array_search($n, $tocheck) and ($_POST[$n] == '' or $_POST[$n] == $i)) {
        if ($errorindicator != '') {
            $f = $errorindicator . $f;
        }
        if ($errorclass != '') {
            $f = preg_replace('/name=/i', 'class="' .
                $errorclass . '" name=', $f);
        }
    }
    return $f;
}

// check the form
function check()
{
    if ($_POST != '') {
        $sentarray = array();
        $tocheck = explode(',', $_POST['required']);
        $error[0] = "Errors:";
        foreach ($tocheck as $t) {
            if (!array_key_exists($t, $_POST)) {
                $error[] = $t;
            }
        }
        foreach (array_keys($_POST) as $p) {
            if (!preg_match('/initvalue/', $p) and
                !preg_match('/required/', $p)) {
                $sentarray[$p] = $_POST[$p] ==
                $_POST[$p . 'initvalue'] ? '' : $_POST[$p];
            }
            foreach ($tocheck as $c) {
                if ($p == $c and $sentarray[$p] == '') {
                    $error[] = $_POST[$p . 'initvalue'];
                }
            }
        }
        return $error[1] == '' ? $sentarray : $error;
    }
}

?>

<?php
//use the function
$errorindicator = '<img src="images/delete.gif" width="14" height="14" 
  alt="Alert" title="Indicator for missing form element" border="0" />';
$errorclass = "error";
$Javascript = true;
$results = check();
if ($results[0] == 'Errors:') {

    ?>
    <h3>There has been an error:</h3>
    <p>You forgot to enter the following field(s)</p>
    <ul>
        <?php foreach ($results as $i => $e) {
            if ($i > 0) {
                echo "<li>$e</li>";
            }
        }
        @$errored = 1;
        $regnos = $_POST['regno'];
        ?>
    </ul>
    <?php
}

###############################################
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
    $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["save"])) && ($_POST["save"] == "Save")) {

    //get submitted vaules
    $course = addslashes($_POST['course']);
    $ayear = addslashes($_POST['Ayear']);
    $sregno = addslashes($_POST['regno']);
    $status = addslashes($_POST['core']);

#update examresult
    $qstatus = "UPDATE examresult SET Status = '$status' WHERE RegNo ='$sregno' AND CourseCode ='$course'";
    $dbstatus = mysqli_query($zalongwa, $qstatus);

//query current Year
    $qyear = "SELECT AYear from academicyear WHERE Status = 1";
    $dbyear = mysqli_query($zalongwa, $qyear);
    $row_year = mysqli_fetch_assoc($dbyear);
    $currentYear = $row_year['AYear'];
    if ($privilege <> 2) {
        if ($currentYear <> $ayear) {
            echo "You Cannot Register For This Year:" . $ayear . "<br> Registration Rejected !!";
            exit;
        }
    }
//get total registered student
    $qregistered = "
		SELECT DISTINCT COUNT(course.CourseCode) as Total, 
								course.CourseCode,
									examregister.AYear 						 
		FROM examregister 
			INNER JOIN course ON (examregister.CourseCode = course.CourseCode)
		WHERE (examregister.AYear ='$ayear') AND examregister.CourseCode = '$course'
		GROUP BY course.CourseCode";
    $dbregistered = mysqli_query($zalongwa, $qregistered);
    $row_registered = mysqli_fetch_assoc($dbregistered);
    $totalRegistered = $row_registered['Total'];

//get course capacity
    $qcapacity = "SELECT Capacity from course where CourseCode = '$course'";
    $dbcapacity = mysqli_query($zalongwa, $qcapacity);
    $row_capacity = mysqli_fetch_assoc($dbcapacity);
    $capacity = $row_capacity['Capacity'];

    if ($totalRegistered < $capacity + 1) {
        /*
          $insertSQL = sprintf("INSERT INTO examresult (AYear, Semester, RegNo, CourseCode, Recorder, RecordDate, Status, ExamCategory, Checked)
                                                              VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                               GetSQLValueString($_POST['Ayear'], "text"),
                               GetSQLValueString($_POST['semester'], "text"),
                               GetSQLValueString($_POST['regno'], "text"),
                               GetSQLValueString($_POST['course'], "text"),
                               GetSQLValueString($_POST['user'], "text"),
                               GetSQLValueString($_POST['rdate'], "text"),
                               GetSQLValueString($_POST['core'], "text"),
                               GetSQLValueString($_POST['examcat'], "text"),
                               GetSQLValueString($_POST['checked'], "text"));
            mysql_select_db($database_zalongwa, $zalongwa);
            $Result1 = mysql_query($insertSQL, $zalongwa) or die(mysqli_error($zalongwa));
        */
        $insertSQL2 = sprintf("INSERT INTO examregister (AYear, Semester, RegNo, CourseCode, Recorder, RecordDate, Status, Checked) 
	  													VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
            GetSQLValueString($_POST['Ayear'], "text"),
            GetSQLValueString($_POST['semester'], "text"),
            GetSQLValueString($_POST['regno'], "text"),
            GetSQLValueString($_POST['course'], "text"),
            GetSQLValueString($_POST['user'], "text"),
            GetSQLValueString($_POST['rdate'], "text"),
            GetSQLValueString($_POST['core'], "text"),
            //GetSQLValueString($_POST['examcat'], "text"),
            GetSQLValueString($_POST['checked'], "text"));


        $Result2 = mysqli_query($zalongwa, $insertSQL2) or die(mysqli_error($zalongwa));

        echo '<meta http-equiv = "refresh" content ="0; 
							url = lecturerStudentRegister.php">';
    } else {
        echo "Registration Not Possible, Reached Course Capacity Limit <br>";
        echo "Choose another Course";
    }
}
###############################################

//Search Candidate
if (isset($_POST["candidate"])) {
    $key = trim($_POST["candidate"]);
    $query_candidate = "SELECT student.Name, student.RegNo
						  FROM student 
						  WHERE (student.RegNo ='$key')";
    $candidate = mysqli_query($zalongwa, $query_candidate) or die(mysqli_error($zalongwa));
    $row_candidate = mysqli_fetch_assoc($candidate);
    $totalRows_candidate = mysqli_num_rows($candidate);

}
//display the form if candidate is found
if ((@$totalRows_candidate > 0) || ($errored == 1)) {
    ?>
    <form name="addpayment" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
        <input type="hidden" name="required" value="regno"/>
        <input name="user" type="hidden" id="user" value="<?php echo $username; ?>">
        <input name="checked" type="hidden" id="checked" value="0">
        <?php $today = date("Y-m-d") ?>
        <input name="rdate" type="hidden" id="rdate" value="<?php echo $today ?>">

        <div class="container ">
            <div class="row ">
                <div class="col-sm-8 offset-sm-2">
                    <div class="card">
                        <h3 class="card-header">
                            Exam Registration Form</h3>
                        <div class="card-block">
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Candidate</label>
                                    <div class="col-sm-8">
                                        <input type="hidden" name="regno" id="regno"
                                               value="<?php echo $row_candidate['RegNo'] ?>"/>
                                        <p class="form-control-static"><?php echo $row_candidate['Name'] . ": " ?><?php echo $row_candidate['RegNo'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"
                                           for="exampleFormControlSelect">Semester</label>
                                    <div class="col-sm-8">
                                        <select name="semester" id="semester" class="form-control">
                                            <option value="0">Select Semester</option>
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_semester['Description'] ?>"><?php echo $row_semester['Description'] ?></option>
                                                <?php
                                            } while ($row_semester = mysqli_fetch_assoc($semester));
                                            $rows = mysqli_num_rows($semester);
                                            if ($rows > 0) {
                                                mysqli_data_seek($semester, 0);
                                                $row_semester = mysqli_fetch_assoc($semester);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" for="exampleFormControlSelect">Course</label>
                                    <div class="col-sm-8">
                                        <select name="course" id="course" class="form-control">
                                            <option value="0">Select CourseCode</option>
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_course['CourseCode'] ?>"><?php echo $row_course['CourseCode'] ?></option>
                                                <?php
                                            } while ($row_course = mysqli_fetch_assoc($course));
                                            $rows = mysqli_num_rows($course);
                                            if ($rows > 0) {
                                                mysqli_data_seek($course, 0);
                                                $row_course = mysqli_fetch_assoc($course);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" for="exampleFormControlSelect">Year</label>
                                    <div class="col-sm-8">
                                        <select name="Ayear" id="Ayear" class="form-control">
                                            <option value="0">Select Academic Year</option>
                                            <?php
                                            do {
                                                ?>
                                                <option value="<?php echo $row_paytype['AYear'] ?>"><?php echo $row_paytype['AYear'] ?></option>
                                                <?php
                                            } while ($row_paytype = mysqli_fetch_assoc($paytype));
                                            $rows = mysqli_num_rows($paytype);
                                            if ($rows > 0) {
                                                mysqli_data_seek($paytype, 0);
                                                $row_paytype = mysqli_fetch_assoc($paytype);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" for="exampleFormControlSelect">Status</label>
                                    <div class="col-sm-8">
                                        <select name="core" id="core" class="form-control">
                                            <option value="1">Core</option>
                                            <option value="2">Elective</option>
                                            <option value="3">Fundamental</option>
                                        </select>
                                        <input type="hidden" name="examcat" id="examcat" value="4"/>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"></label>
                                    <div class="col-sm-8">
                                        <input class="btn btn-success btn-md btn-block" name="save" type="submit"
                                               id="save"
                                               value="Save"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <?php
} else {
    ?>
    <div class="container text-muted">
        <!-- cards -->
        <div class="container-search" align="center">
            <img class="card-img-top img-fluid "
                 style="width: 200px; height: inherit; padding-top: 15px; "
                 src="./img/search.svg">
            <form class="navbar-form" role="search" action="<?php $_SERVER['PHP_SELF'] ?>" method="post" name="search"
                  id="search">
                <div class="input-group add-on">
                    <input class="form-control" placeholder="Enter Student Registration Number" name="candidate"
                           id="candidate" type="text" value="">
                    <div class="input-group-btn">
                        <input class="btn btn-default" type="submit" name="Submit" value="GO">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php
}
?>

<!-- end .container -->
<br><br><br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</div>
</body>
</html>
