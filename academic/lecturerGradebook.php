<?php 
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('lecturerMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Examination';
	$szSubSection = 'Grade Book';
	$szTitle = 'Examination GradeBook';
	include('lecturerheader.php');

#save user statistics
$browser  = $_SERVER["HTTP_USER_AGENT"];   
$ip  =  $_SERVER["REMOTE_ADDR"];   
$sql="INSERT INTO stats(ip,browser,received,page) VALUES('$ip','$browser',now(),'$username')";   
$result = mysqli_query($zalongwa, $sql) or die("Can't insert data!!.<br>" . mysqli_error($zalongwa));

#Control Refreshing the page
#if not refreshed set refresh = 0
@$refresh = 0;
#------------
#populate academic year combo box

$query_AYear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$AYear = mysqli_query($zalongwa, $query_AYear) or die(mysqli_error($zalongwa));
$row_AYear = mysqli_fetch_assoc($AYear);
$totalRows_AYear = mysqli_num_rows($AYear);

//check if is a Departmental examination officer
$query_userdept = "SELECT Dept FROM security where UserName = '$username' AND Dept<>0";
$userdept = mysqli_query($zalongwa, $query_userdept) or die(mysqli_error($zalongwa));
$row_userdept = mysqli_fetch_assoc($userdept);
$totalRows_userdept = mysqli_num_rows($userdept);


//check if is Faculty examination officer
if($privilege == 2){
	$query_userfac = "SELECT Faculty FROM security where UserName = '$username' AND Dept=0";
	}
else{
	$query_userfac = "SELECT Faculty FROM security where UserName = '$username'";
	}
$userfac = mysqli_query($zalongwa, $query_userfac) or die(mysqli_error($zalongwa));
$row_userfac = mysqli_fetch_assoc($userfac);
$totalRows_userfac = mysqli_num_rows($userfac);
$fac = $row_userfac["Faculty"];

if($totalRows_userdept>0){
	$query_dept = "SELECT department.DeptName	FROM department
					INNER JOIN security ON (department.DeptID = security.Dept)
					WHERE (UserName = '$username') ORDER BY department.DeptName";
	}
elseif($privilege == 2){
	$query_dept = "SELECT FacultyID, FacultyName FROM faculty WHERE (FacultyID = '$fac')";
	}
else{
	$query_dept = "SELECT DeptID, DeptName	FROM department ORDER BY DeptName ASC";
	}
								
$dept = mysqli_query($zalongwa, $query_dept) or die(mysqli_error($zalongwa));
$row_dept = mysqli_fetch_assoc($dept);
$totalRows_dept = mysqli_num_rows($dept);

#process form submission
$editFormAction = $_SERVER['PHP_SELF'];
if ((isset($_POST["frmSubmit"])) && ($_POST["frmSubmit"] == "yes")) {
#set refresh = 1
$refresh = 1;

#..............
@$ayear = addslashes($_POST['ayear']);
@$faculty = addslashes($_POST['faculty']);
@$sem = $_POST['sem'];

if($sem=="Choice"){
	echo "<div align='center'><p style='color : red'>Choose Semester of Study First<p>";
	echo "<input onclick='history.back(-1)' type='button' value='Back'></div>";
	exit;
}

$upload = mysqli_query($zalongwa, "SELECT * FROM uploadlimit WHERE AYear='$ayear' AND Semester='$sem' AND DeptID='$fac'");
while($values = mysqli_fetch_array($upload)){
		$limc = $values['Checked'];
		$limd = $values['Date'];
	}

$limd2 = date('Y-m-d');
if ($privilege !='2' && $limc == 1 && $limd != "" && ( $limd < $limd2)){
	echo "<p style='color:maroon'>Results uploading season has passed, please consult your Department Examinations officer<p>";
	exit;
	}

#populate examcayegory combo box


if($fac==1){
$query_examcategory = "SELECT Id,Description FROM examcategory WHERE (Id > 2) ORDER BY Id";
}else{
$query_examcategory = "SELECT Id,Description FROM examcategory ORDER BY Id";
}
$examcategory = mysqli_query($zalongwa, $query_examcategory) or die(mysqli_error($zalongwa));
$row_examcategory = mysqli_fetch_assoc($examcategory);
$totalRows_examcategory = mysqli_num_rows($examcategory);

#populate Exam Marker combo box

$query_exammarker = "SELECT Id, Name FROM exammarker ORDER BY Name";
$exammarker = mysqli_query($zalongwa, $query_exammarker) or die(mysqli_error($zalongwa));
$row_exammarker = mysqli_fetch_assoc($exammarker);
$totalRows_exammarker = mysqli_num_rows($exammarker);

if ($privilege ==3){
$query_coursecode = "SELECT DISTINCT course.CourseCode, examregister.AYear
		FROM examregister INNER JOIN course ON (examregister.CourseCode = course.CourseCode)
		WHERE (examregister.AYear ='$ayear') AND (examregister.RegNo='$username') 
		ORDER BY examregister.CourseCode ASC";
}else{
$whereclause = ($userDeptHead==1) ?  " WHERE Department IN (SELECT DeptName FROM department WHERE DeptID='$userDept' ) " : "";
$query_coursecode = "SELECT DISTINCT CourseCode FROM course $whereclause ORDER BY CourseCode ASC";
}

$coursecode = mysqli_query($zalongwa, $query_coursecode) or die(mysqli_error($zalongwa));

?>
 Select Appropriate Entries  For : 
	<?php 
	echo $_POST['sem'].' - '.$_POST['ayear'];
	?>
		<script language="JavaScript" src="fuct.js" type="text/javascript"></script>
		<form action="lecturerGradebookAdd.php" method="post" name="frm2" enctype="multipart/form-data" name="frmCourse" target="_self" onsubmit="return lackOnsubmit()">
						
		<table class="table_form" cellspacing="0" cellpadding="0">
          <tr class="table_form_header">
            <th nowrap="nowrap" class="td_label"  scope="col">Module Code 
				<input name="ayear" type="hidden" value="<?php echo $ayear ?>">
				<input name="sem" type="hidden" value="<?php echo $_POST['sem'] ?>">
			</th>
            <th nowrap="nowrap" class="td_label" scope="col">Exam Category </th>
            <th nowrap="nowrap" class="td_label" scope="col">Exam Date </th>
           
          </tr>
          <tr>
            <td ><select style="width:auto;" name="course" size="1">
              <option value="0">[Select Course Code]</option>
              <?php
				if($privilege == 2){
					while($row_coursecode2 = mysqli_fetch_array($coursecode2)){
					
						echo "<option value='".$row_coursecode2['CourseCode']."'>".$row_coursecode2['CourseCode']."</option>";
						}
					}
				do {
					if($row_coursecode['CourseCode']<>'') {
						?>
              <option value="<?php echo $row_coursecode['CourseCode']?>"><?php echo $row_coursecode['CourseCode']?></option>
              <?php
		        }
							} while ($row_coursecode = mysqli_fetch_assoc($coursecode));
									$rows = mysqli_num_rows($coursecode);
									if($rows > 0) {
						mysqli_data_seek($coursecode, 0);
						$row_coursecode = mysqli_fetch_assoc($coursecode);
  					}
               ?>
            </select></td>
            <td ><select style="width:auto;" name="examcat" size="1">
              <option value="0">[Select Examcategory]</option>
              <?php
				do {  
						?>
              <option value="<?php echo $row_examcategory['Id']?>"><?php echo $row_examcategory['Description']?></option>
              <?php
							} while ($row_examcategory = mysqli_fetch_assoc($examcategory));
									$rows = mysqli_num_rows($examcategory);
									if($rows > 0) {
						mysqli_data_seek($examcategory, 0);
						$row_examcategory = mysqli_fetch_assoc($examcategory);
  					}
               ?>
            </select></td>
            <td >			<!-- A Separate Layer for the Calendar -->
					<script language="JavaScript" src="datepicker/Calendar1-901.js" type="text/javascript"></script>
					 <table border="0">
									<tr>
										<td><input style="width:150px;" name="examdate" type="text" size="10" maxlength="10"></td>
										<td><input type="button" class="button" name="rpDate_button" value="Pick Date" onClick="show_calendar('frmCourse.examdate', '','','YYYY-MM-DD', 'POPUP','AllowWeekends=Yes;Nav=No;SmartNav=Yes;PopupX=325;PopupY=325;')"></td>
									</tr>
		    </table></td>
            
          </tr>
        </table>
        <table class="table_form">
        <tr class="submit">
        <td>
        <input onclick="history.back(-1)" type="button"  value="Back">
        <input name="view" type="submit" value="Edit Records" />
        </td>
        </tr>
        </table>
		</form>	
<?php
//end of the form display
}

#display the form when refresh is zero
if ($refresh == 0) {
?> 

<form action="<?php echo $editFormAction ?>" method="post" enctype="multipart/form-data" name="form1">
<div >Select Appropriate Academic Year and Semester</div>
              <table class="table_form" cellpadding="0" cellspacing="0">
              <tr class="table_form_header">
              <td class="td_label">Field</td><td>Field Value</td>
              </tr>
                <tr>
                  <td class="td_label" nowrap><div align="right">Academic Year: </div></td>
                  <td><select name="ayear" id="ayear">
                      <?php
do {  
?>
                      <option value="<?php echo $row_AYear['AYear']?>"><?php echo $row_AYear['AYear']?></option>
                      <?php
} while ($row_AYear = mysqli_fetch_assoc($AYear));
  $rows = mysqli_num_rows($AYear);
  if($rows > 0) {
      mysqli_data_seek($AYear, 0);
	  $row_AYear = mysqli_fetch_assoc($AYear);
  }
?>
                  </select></td>
                </tr>
                <tr>
                  <td class="td_label" nowrap><div align="right">Semester: </div></td>
                  <td><select name="sem" id="sem">
			   <option value="Choice">Choose Semester</option>
                        <?php

$query_sem = "SELECT Semester FROM terms ORDER BY Semester ";
$sem = mysqli_query($zalongwa, $query_sem);
$row_sem = mysqli_fetch_assoc($sem);
$totalRows_sem = mysqli_num_rows($sem);
do {  
?>
                        <option value="<?php echo $row_sem['Semester']?>"><?php echo $row_sem['Semester']?></option>
                        <?php
} while ($row_sem = mysqli_fetch_assoc($sem));
  $rows = mysqli_num_rows($sem);
  if($rows > 0) {
      mysqli_data_seek($sem, 0);
	  $row_sem = mysqli_fetch_assoc($sem);
  }
?>
                    </select>
                  </td>
                </tr>
              
  </table>
  <table class="table_form">
  <tr class="submit">
  <td>
  <input onclick="history.back(-1)" type="button"  value="Back">
  <input name="frmSubmit" type="hidden" id="frmSubmit" value="yes">
  <input type="submit" name="action" value="View Courses">
  </td>
  </tr>
  </table>
</form>
<?php

}
include('../footer/footer.php');
?>
