<?php

#get connected to the database and verfy current session
require_once('../Connections/sessioncontrol.php');
# initialise globals
include('../academic/lecturerMenu.php');

# include the header
global $szSection, $szSubSection;
$szSection = 'Examination';
$szSubSection = 'Grade Book';
$szTitle = 'Import MS Excel Database Data';
include('../academic/lecturerheader.php');
?>
<?php

#STEP 1.0 Upload data file
# delete old files
@unlink("./temp/zalongwa.xls");
#constants
$fileavailable = 0;

#validate the file
$file_name = ($_POST['userfile']);
$overwrite = addslashes($_POST['radiobutton']);

$file = $_FILES['userfile']['name'];
//The original name of the file on the client machine. 
$filetype = $_FILES['userfile']['type'];
//The mime type of the file, if the browser provided this information. An example would be "image/gif". 
$filesize = $_FILES['userfile']['size'];
//The size, in bytes, of the uploaded file. 
$filetmp = $_FILES['userfile']['tmp_name'];
//The temporary filename of the file in which the uploaded file was stored on the server. 
$filetype_error = $_FILES['userfile']['error'];
$filename = time() . $_FILES['userfile']['name'];
?>
<?php

// In PHP earlier then 4.1.0, $HTTP_POST_FILES  should be used instead of $_FILES.
if (is_uploaded_file($filetmp)) {
    $filename = time() . $file;
    copy($filetmp, "$filename");
} else {
    echo "File not uploaded, Sorry start again <input onclick='javascript: history.go(-3)' type='button' value='Back'>";
}
move_uploaded_file($filetmp,    "/var/lib/saris-ita/academic/temp/$filename");

#check file extension
$str = $filename;
$i = strrpos($str, ".");
if (!$i) {
    return "";
}

$l = strlen($str) - $i;
$ext = substr($str, $i + 1, $l);
$pext = strtolower($ext);
if ($pext != "xls") {
    print "<h2>ERROR</h2>File Extension Unknown.<br>";
    print "<p>Please Upload a File with the Extension .csv ONLY<br>";
    print "To convert your Excel File to csv, go to File -> Save As, then Select Save as Type CSV (Comma delimeded) (*.csv)</p>\n";
    print "The file you uploaded have this extension: $pext</p>\n";
    echo '<meta http-equiv = "refresh" content ="10; url = zalongwaimport.php">';
    include('../footer/footer.php');
    unlink($filename);
    exit();
}
?>
<?php

if ($pext == "xls") {
    $fileavailable = 1;
}
#STEP 2.0 exceute sql scripts
if ($fileavailable == 1) {
    echo "<strong>Data Import in Process</strong><br /><br>";
    echo "Progress Status.............<br>";
    $fcontents = file("/var/lib/saris-ita/academic/temp/$filename");
    # expects the csv file to be in the same dir as this script
    #get impo year
    /*
      $line = trim($fcontents[0], ',');
      $arr = explode(",", $line);
      $impayear = trim($arr[1]);
     */
    $impayear = addslashes($_POST['ayear']);

    #get impo semester
    /*
      $line = trim($fcontents[1], ',');
      $arr = explode(",", $line);
      $impsem = trim($arr[1]);
     */
    $impsem = addslashes($_POST['sem']);
    #get impo coursecode
    /*
      $line = trim($fcontents[2], ',');
      $arr = explode(",", $line);
      $impcourse = trim($arr[1]);
     */
    $impcourse = addslashes($_POST['coursecode']);

    #get impo examcategory
    /*
      $line = trim($fcontents[3], ',');
      $arr = explode(",", $line);
      $impexamcat = trim($arr[1]);
     */
    $impexamcat = addslashes($_POST['examcat']);
    /*
      $impexamcat = strtolower($impexamcat);
      if ($impexamcat == 'cwk'){
      $impexamcat = 4;
      }elseif($impexamcat=='exam'){
      $impexamcat = 5;
      }else{
      $impexamcat = $impexamcat;
      }
     */
    $today = date('Y-m-d');
    /** PHPExcel */
    require_once '../Classes/PHPExcel/IOFactory.php';
    $objPHPExcel = PHPExcel_IOFactory::load("./temp/$filename");
    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
		 $courselabel=$worksheet->getCellByColumnAndRow(0,1);
		 $courselabel=$courselabel->getValue();
         $regnolabel=$worksheet->getCellByColumnAndRow(0, 2);
         $regnolabel=strtolower(trim($regnolabel->getValue()));
         $scorelabel=$worksheet->getCellByColumnAndRow(1, 2);
         $scorelabel=strtolower(trim($scorelabel->getValue()));
		}
	 if($impcourse==$courselabel && $regnolabel=='regno' && $scorelabel=='score'){
		 foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
			
        $worksheetTitle = $worksheet->getTitle();
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        $nrColumns = ord($highestColumn) - 64;
        echo "<br>The worksheet " . $worksheetTitle . " has ";
        echo $nrColumns . ' columns (A-' . $highestColumn . ') ';
        echo ' and ' . $highestRow . ' row.';
        
        echo '<br>Data: <table border="1"><tr>';
        for ($row = 1; $row <= $highestRow; ++$row) {
            echo '<tr>';
            for ($col = 0; $col < $highestColumnIndex; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, $row);
                $val = $cell->getValue();
                //$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);

                if (($col == 0) and ($row >= 3)) {
                    #check if regno exists
                    $qregno = "SELECT Name, RegNo FROM student WHERE RegNo='$val'";
                    $dbregno = mysqli_query($zalongwa, $qregno);
                    $regct = mysqli_num_rows($dbregno);
                    if ($regct > 0) {
                        echo '<td>' . $val . '</td>';
                    } else {
                        echo '<td> RegNo ' . $val . ' Doesnot exists! </td>';
                    }
                } else {
                    echo '<td>' . $val . '</td>';
                }
            }
            echo '</tr>';
        }
        echo '</table>';
		
			
        for ($i = 3; $i <= $highestRow; ++$i) {
            //for ($j=0; $j<$highestColumnIndex; ++$j) {
            echo '<br>';

            $cell = $worksheet->getCellByColumnAndRow(0, $i);
            $val0 = $cell->getValue();
            $val0 = trim ($val0);
            //echo 'A'.$i.' = '.$val0.'<br>';

            $cell = $worksheet->getCellByColumnAndRow(1, $i);
            $val1 = $cell->getValue();
            $val1 = trim ($val1);
            //echo 'B'.$i.' = '.$val1.'<br>';


            $cell = $worksheet->getCellByColumnAndRow(2, $i);
            $val2 = $cell->getValue();
            //echo 'C'.$i.' = '.$val2.'<br>';           			


            $cell = $worksheet->getCellByColumnAndRow(3, $i);
            $val3 = $cell->getValue();
            //echo 'D'.$i.' = '.$val3.'<br>';           			


            $cell = $worksheet->getCellByColumnAndRow(4, $i);
            $val4 = $cell->getValue();
            //echo 'E'.$i.' = '.$val4.'<br>';


            $cell = $worksheet->getCellByColumnAndRow(5, $i);
            $val5 = $cell->getValue();
            //echo $j.$i.' = '.$val5.'<br>';
            //}
            #check if regno exists
            $qregno = "SELECT Name, RegNo FROM student WHERE RegNo='$val0'";
            $dbregno = mysqli_query($zalongwa, $qregno);
            $regct = mysqli_num_rows($dbregno);

            if ($regct > 0) {
                #imports data
                //OVERWRITING STARTS HERE
                if ($overwrite == 1) {
					$sqlcwk = "REPLACE INTO examresult SET
										AYear ='$impayear',
										Semester = '$impsem',
										CourseCode = '$impcourse',
										ExamCategory = '$impexamcat',
										Recorder = '$username',
										RecordDate = '$today',
										RegNo = '$val0',
										ExamScore = '$val1',
										Checked = '0',
										Status = 1,
										Count = 0,
										Comment =''
										";
										$insert =   mysqli_query($zalongwa, $sqlcwk);
                        if (mysqli_error($zalongwa)) {
                            
                            echo  " Record of " . $val0 . " - " . $val1 . "-  - is a Duplicate Entry - Not Imported!<br>\n";
                        }   
                }
                //REPLACEMENT(OVERWRITING) ENDS HERE
                //INSERTION OF NEW RECORDS STARTS HERE
                else {
					
					// check in case result exist
		         $numexist= mysqli_num_rows(mysqli_query($zalongwa, "SELECT * FROM examresult WHERE AYear ='$impayear' AND Semester = '$impsem' AND 
		         CourseCode = '$impcourse' AND ExamCategory = '$impexamcat' AND RegNo = '$val0'"));
		         if($numexist > 0){
					 echo  " Record of " . $val0 . " - " . $val1 . "- Duplicate  - Not Imported!<br>\n";
					 } else{
						  $sqlcwk = "INSERT INTO examresult SET
										AYear ='$impayear',
										Semester = '$impsem',
										CourseCode = '$impcourse',
										ExamCategory = '$impexamcat',
										Recorder = '$username',
										RecordDate = '$today',
										RegNo = '$val0',
										ExamScore = '$val1',
										Checked = '0',
										Status = 1,
										Count = 0,
										Comment =''
										";
                      $insert =   mysqli_query($zalongwa, $sqlcwk);
                        if (mysqli_error($zalongwa)) {
                            
                            echo  " Record of " . $val0 . " - " . $val1 . "-  - Not Imported!<br>\n";
                        }
						 }
				
                }
                //INSERTION OF NEW RECORDS ENDS HERE
            } else {
                 echo  " Student with Reg #" . $val0 ."does not exist-  - Not Imported!<br>\n";
            }
        }		
    }
    echo "<br /><br /><strong>Zalongwa Data Import Process Completed</strong>";
		 
		 }else{
	   echo  " Wrong Excel format, check first two rows of your excel files";
         echo "You Metadata are: <br> CourseCode = ".$impcourse."<br> RegNo = ".$regnolabel."<br> Score = ".$scorelabel;
	       echo "<br /><br /><strong>Zalongwa Data Import Process not Completed</strong>";		
			}	
    
    
    unlink("./temp/$filename");
    unlink("$filename");
}
include('../footer/footer.php');
exit;
?> 

