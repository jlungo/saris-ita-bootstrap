<?php
#start html
if (isset($_POST['HTML']) && ($_POST['HTML'] == "Print HTML")){
	
	#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
	require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('lecturerMenu.php');
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Examination';
	$szSubSection = 'Semester Results';
	$szTitle = 'Printing Semester Examinations Results Report';
	include('lecturerheader.php');
	include '../academic/includes/print_html_semester_results.php';
	include('../footer/footer.php');
	/*
	# file name for download 
	$filename = "SARIS_Results_" . date('Ymd') . ".xls"; 
	header("Content-Disposition: attachment; filename=\"$filename\"");
	header("Content-Type: application/vnd.ms-excel"); 
   */
	exit;
}elseif (isset($_POST['PDF']) && ($_POST['PDF'] == "Print PDF")){
	#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
	require_once('../Connections/zalongwa.php');

#start pdf
	#Get Organisation Name
	$qorg = "SELECT * FROM organisation";
	$dborg = mysql_query($qorg);
	$row_org = mysql_fetch_assoc($dborg);
	$org = $row_org['Name'];

	@$checkdegree = addslashes($_POST['checkdegree']);
	@$checkyear = addslashes($_POST['checkyear']);
	@$checkdept = addslashes($_POST['checkdept']);
	@$checkcohot = addslashes($_POST['checkcohot']);
	
	@$paper = 'a3'; //addslashes($_POST['paper']);
	@$layout = 'L'; //addslashes($_POST['layout']);
	if ($paper=='a3')
	{
		$xpoint = 1050.00;
		$ypoint = 800.89;
	}else
	{
		$xpoint = 800.89;
		$ypoint = 580.28;
	}

	$prog=$_POST['degree'];
	$cohotyear = $_POST['cohot'];
	$ayear = $_POST['ayear'];
	$qprog= "SELECT ProgrammeCode, Title, Faculty, Ntalevel FROM programme WHERE ProgrammeCode='$prog'";
	$dbprog = mysql_query($qprog);
	$row_prog = mysql_fetch_array($dbprog);
	$progname = $row_prog['Title'];
	$faculty = $row_prog['Faculty'];
	$class = $row_prog['Ntalevel'];
	
	//calculate year of study
	$entry = intval(substr($cohotyear,0,4));
	$current = intval(substr($ayear,0,4));
	$yearofstudy=$current-$entry;
	
	if (($checkdegree=='on') && ($checkyear == 'on') && ($checkcohot == 'on')){
		$deg=addslashes($_POST['degree']);
		$year = addslashes($_POST['ayear']);
		$cohot = addslashes($_POST['cohot']);
		$dept = addslashes($_POST['dept']);
		$sem = addslashes($_POST['sem']);
		if ($sem =='Semester I'){
			$semval = 1;
		}elseif ($sem=='Semester II'){
			$semval = 2;
		}
	
	#calculate year of study
	$entry = intval(substr($cohot,0,4));
	$current = intval(substr($ayear,0,4));
	$yearofstudy=$current-$entry;
	
	if($yearofstudy==0){
		$class="FIRST YEAR";
		}elseif($yearofstudy==1){
		$class="SECOND YEAR";
		}elseif($yearofstudy==2){
		$class="THIRD YEAR";
		}elseif($yearofstudy==3){
		$class="FOURTH YEAR";
		}elseif($yearofstudy==4){
		$class="FIFTH YEAR";
		}elseif($yearofstudy==5){
		$class="SIXTH YEAR";
		}elseif($yearofstudy==6){
		$class="SEVENTH YEAR";
		}else{
		$class="";
	}
	#cohort number
	$yearofstudy = $yearofstudy +1;
	$totalcolms =0;
		#determine total number of columns
		$qcolmns = "SELECT DISTINCT CourseCode FROM courseprogramme WHERE  (ProgrammeID='$deg') AND (YearofStudy='$yearofstudy') 
						AND (Semester = '$semval') ORDER BY CourseCode";
		$dbcolmns = mysql_query($qcolmns);
		#query for supp
		$dbsupp = mysql_query($qcolmns);
		
		$totalcolms = mysql_num_rows($dbcolmns);

		#start pdf
		include('includes/PDF.php');
		$pdf = &PDF::factory($layout, $paper);      
		$pdf->open();                         
		$pdf->setCompression(true);           
		$pdf->addPage();  
		$pdf->setFont('Arial', 'I', 8);     
		$pdf->text(50, $ypoint, 'Printed On '.$today = date("d-m-Y H:i:s"));  		
 
		#put page header
		$x=60;
		$y=74;
		$i=1;
		$pg=1;
		$pdf->text($xpoint,$ypoint, 'Page '.$pg);   
		
		#count unregistered
		$j=0;
		#count sex
		$fmcount = 0;
		$mcount = 0;
		$fcount = 0;
		#print row 1
		#print NACTE FORM EXAM 0.3
		$pdf->setFont('Arial', 'B', 12);   
		//$pdf->text(50,$y, 'NACTE FORM EXAM 03');
		  
		#print header for landscape paper layout 
		$playout ='l'; 
		include '../includes/orgname.php';
		#print row 2
		$pdf->setFillColor('rgb', 0, 0, 0);    
		$pdf->setFont('Arial', 'B', 20);      
		//$pdf->text($x+190, $y+20, strtoupper($faculty)); 
		//$pdf->text(50, $y+40, strtoupper($class)); 
		#print row 3
		$pdf->text($x+190, $y+40, 'PROVISIONAL RESULTS');
		#print row 4
			$pdf->setFont('Arial', 'B', 13);   
			//$pdf->text(50, $y+60, 'NTA LEVEL 4:');
			//$pdf->text(50, $y+60, strtoupper($class)); 
			$pdf->text($x+110, $y+70, '['.strtoupper($class).'] '.strtoupper($progname));
		#print row 5
			$pdf->text(50, $y+70, 'Academic Year: '.$year.' - '.$sem);
			//$pdf->text($x+300, $y+74, 'Semester: '.$sem);
			//$pdf->text($x+650, $y+60, 'Weight: CA - 40%');
			//$pdf->text($x+768, $y+60, 'Weight: SE - 60%');
		#reset values of x,y
		$x=50; $y=$y+88;
		#set table header
		#print row 1
		$pdf->line($x, $y, $xpoint+25, $y); //first horizontal line
		$pdf->line($x, $y+14, $xpoint+25, $y+14); //second horizontal line
		$pdf->line($x, $y, $x, $y+14); //first vertical line
		$pdf->line($xpoint+25, $y, $xpoint+25, $y+14); //last vertical line
		#print row 2
		$pdf->line($x, $y+28, $xpoint+25, $y+28); //second horizontal line
		$pdf->line($x, $y+14, $x, $y+28); //first vertical line
		$pdf->line($xpoint+25, $y+14, $xpoint+25, $y+28); //last vertical line
		#print row 3
		//$pdf->line($x+150, $y+42, $xpoint+25, $y+42); //second horizontal line
		$pdf->line($x, $y+28, $x, $y+42); //first vertical line
		$pdf->line($xpoint+25, $y+28, $xpoint+25, $y+42); //last vertical line
		#print row 4
		$pdf->line($x, $y+56, $xpoint+25, $y+56); //second horizontal line
		$pdf->line($x, $y+42, $x, $y+56); //first vertical line
		$pdf->line($xpoint+25, $y+42, $xpoint+25, $y+56); //last vertical line
		#print row 5
		$pdf->line($x, $y+70, $xpoint+25, $y+70); //second horizontal line
		$pdf->line($x, $y+56, $x, $y+70); //first vertical line
		$pdf->line($xpoint+25, $y+56, $xpoint+25, $y+70); //last vertical line
		
		#print module column
		$pdf->line($x+150, $y, $x+150, $y+70); 
		$pdf->line($x+25, $y+56, $x+25, $y+70); 
		$pdf->line($x+120, $y+56, $x+120, $y+70); 
		#print text
			$pdf->setFont('Arial', '', 10); 
			$pdf->text($x+2, $y+12, 'Module Credits:');
			$pdf->text($x+2, $y+24, 'Module Code:');
			$pdf->text($x+2, $y+48, 'Exam Components:');
			$pdf->text($x+2, $y+68, 'S/No');
			$pdf->text($x+26, $y+68, 'Reg. No.');
			$pdf->text($x+122, $y+68, 'Sex');
			$pdf->text($x+275, $y+68, 'Examinations Results'); 
		#reset values of x,y
		$x=50; $y=$y+56;
		#set colm width
		$clmw = 80;
		#get column width factor
		$cwf = 80;
		#calculate course clumns widths
		$cw = $cwf*$totalcolms;
		$x=$x+235;
		$x=$x-85;
		$pdf->line($x+$cw, $y, $x+$cw, $y+14);	$pdf->text($x+$cw+1, $y+12, 'CRDT');
		$pdf->line($x+$cw+30, $y, $x+$cw+30, $y+14);	$pdf->text($x+$cw+32, $y+12, 'PNTS'); $pdf->text($x+$cw+22, $y-8, 'OVERALL PERFORMANCE'); 
		$pdf->line($x+$cw+60, $y, $x+$cw+60, $y+14);	$pdf->text($x+$cw+67, $y+12, 'GPA'); 
		$pdf->line($x+$cw+95, $y, $x+$cw+95, $y+14);	$pdf->text($x+$cw+97, $y+12, 'Remark');
		$pdf->line($xpoint+25, $y, $xpoint+25, $y+14);  
		$y=$y+15;
		
		#query student list
		$overallpasscount = 0;
		$overallsuppcount = 0;
		$overallinccount = 0;
		$overalldiscocount = 0;
		$qstudent = "SELECT Name, RegNo, Sex, DBirth, ProgrammeofStudy, Faculty, Sponsor, EntryYear, Status FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY RegNo";
		$dbstudent = mysql_query($qstudent);
		$totalstudent = mysql_num_rows($dbstudent);
		$i=1;
		$jheader = 0;
		while($rowstudent = mysql_fetch_array($dbstudent)) 
		{
				$name = stripslashes($rowstudent['Name']);
				$regno = $rowstudent['RegNo'];
				$sex = $rowstudent['Sex'];
				$bdate = $rowstudent['DBirth'];
				
				if ($checkhide=='on'){
					$degree = stripslashes($rowstudent["ProgrammeofStudy"]);
					$faculty = stripslashes($rowstudent["Faculty"]);
					$sponsor = stripslashes($rowstudent["Sponsor"]);
					$entryyear = stripslashes($result['EntryYear']);
					$ststatus = stripslashes($rowstudent['Status']);
				}
				# get all courses for this candidate

				$qcourse = "SELECT DISTINCT CourseCode, Status FROM courseprogramme 
								WHERE  (ProgrammeID='$deg') AND (YearofStudy='$yearofstudy') AND 
									(Semester='$semval') ORDER BY CourseCode";
				$dbcourse = mysql_query($qcourse);
				$dbcourseUnit = mysql_query($qcourse);
				$dbcourseovremark = mysql_query($qcourse);
				$dbcourseheader = mysql_query($qcourse);
				$total_rows = mysql_num_rows($dbcourse);
					
					#initialise
					$totalunit=0;
					$gmarks=0;
					$totalfailed=0;
					$totalinccount=0;
					$unittaken=0;
					$sgp=0;
					$totalsgp=0;
					$gpa=0;
					
					# new values
					$subjecttaken=0;
					$curr_semester='';
					$totalfailed=0;
					$totalinccount=0;
					$halfsubjects=0;
					$ovremark='';
					$gmarks=0;
					$avg =0;
					$gmarks=0;	
					
					$key = $regno; 
					$x=50;
					
     					#print student info
						$pdf->setFont('Arial', '', 8); 
						$pdf->line($x, $y, $xpoint+25, $y); 
						$pdf->line($x, $y+15, $xpoint+25, $y+15); 
						$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, $i);
						$pdf->line($x+25, $y, $x+25, $y+15);	 $pdf->text($x+27, $y+12, strtoupper($regno)); //$pdf->text($x+27, $y+25, 'B.Date: '.strtoupper($bdate));
						$x=$x-85;
						$pdf->line($x+205, $y, $x+205, $y+15);	$pdf->text($x+210, $y+12, strtoupper($sex));
						$pdf->line($x+235, $y, $x+235, $y+15);	
						
						#calculate course clumns widths
						$clnspace = $x+235; 
						$header = $x+235;
					    
						#print header
						while($rowcourse = mysql_fetch_array($dbcourse)) 
						{ 
							$stdcourse = $rowcourse['CourseCode']; 
							$qcoursestd="SELECT Units, Department, StudyLevel FROM course WHERE CourseCode = '$stdcourse'";	
							$dbcoursestd = mysql_query($qcoursestd);
							$row_coursestd = mysql_fetch_array($dbcoursestd);
							$unit = $row_coursestd['Units'];
							$pdf->line($clnspace, $y+15, $clnspace+$clmw, $y+15);
						
							#partition the course
							$pdf->line($clnspace+20, $y, $clnspace+20, $y+15);
							$pdf->line($clnspace+40, $y, $clnspace+40, $y+15);
							$pdf->line($clnspace+60, $y, $clnspace+60, $y+15);
							$pdf->line($clnspace, $y+15, $clnspace+$clmw, $y+15);
								
								#print exam categories headers
								if ($jheader==0){
									$cheader = $header;
									while ($rownheader = mysql_fetch_array($dbcourseheader)) {
											$courseheader = $rownheader['CourseCode']; 
											#get course unit
											$qunits = "SELECT Units FROM course WHERE CourseCode='$courseheader'";
											$dbunits = mysql_query($qunits);
											$row_units = mysql_fetch_array($dbunits);
											$unit=$row_units['Units'];
												#print courses as headers
												$unitheader = $unit;
												$courseheader = $rownheader['CourseCode']; 
												$pdf->text($cheader+26, $y-60, $unitheader);
												$pdf->text($cheader+22, $y-48, $courseheader);
												$cheader = $cheader+$clmw; 
									}
									
										for ($jheader =0; $jheader < $totalcolms; $jheader++)
										{										
											 #partition the header
											 $pdf->text($header+3, $y-35, 'CW'); 	
											 $pdf->text($header+3, $y-17, 'X/40');
											 $pdf->line($header+20, $y-42, $header+20, $y-15); 
											 $pdf->text($header+22, $y-35, 'FE'); 
											 $pdf->line($header, $y-30, $header+60, $y-30); 
											 $pdf->text($header+22, $y-17, 'X/60');
											 $pdf->line($header+40, $y-42, $header+40, $y-15); $pdf->text($header+41, $y-35, 'TOTL'); 
											 $pdf->text($header+42, $y-17, 'X/100');
											 $pdf->line($header+60, $y-42, $header+60, $y-15); $pdf->text($header+65, $y-35, 'GD');
											 $pdf->line($header+80, $y-70, $header+80, $y-15); 
											 $header = $header+$clmw; 
										 }
								}
								$clnspace = $clnspace+$clmw;
							}
					$jheader = $jheader+1;	 
					#reset colm space
					$clmspace = $x+235; 
					
 					while($row_course = mysql_fetch_array($dbcourseUnit))
 					{
						$course= $row_course['CourseCode'];
						$coption = $row_course['Status']; 
						
						#get course unit
						$qunits = "SELECT Units FROM course WHERE CourseCode='$course'";
						$dbunits = mysql_query($qunits);
						$row_units = mysql_fetch_array($dbunits);
						$unit=$row_units['Units'];
						
						$sn=$sn+1;
						$remarks = 'remarks';
						$RegNo = $key;
						
						#mark this report as report group 2
						$reportgroup = 'sheet';

						$currentyear=$year;
						include '../academic/includes/compute_student_remark.php';
					}
					$curr_semester=$semval;
					include '../academic/includes/compute_overall_remark.php';

					#check failed exam
					if ($fexm=='#'){
						$pdf->text($x+$cw+97, $y+26, $fexm.' = Fail Exam'); $fexm = ''; $remark ='';
					}
					#check failed exam
					if(($totalremarks>0)&&($studentremarks<>'')){
						$pdf->text($x+$cw+97, $y+12, $ovremark);
						$remark ='';
						$fexm = '';
						$fcwk = '';
						$fsup = ''; 
						$igrade = ''; 
						$egrade = ''; 
					}else{
							#check for supp and inco
									$k =0;
									if ($ovremark=='SUPP:'){
										$pdf->text($x+$cw+97, $y+12, $ovremark);
											#print supplementaries
											$k =25;
											#determine total number of columns
											$qsupp = "SELECT DISTINCT CourseCode, Status FROM courseprogramme WHERE  (ProgrammeID='$deg') 
														AND (YearofStudy='$yearofstudy') 
															AND (Semester = '$semval') ORDER BY CourseCode";
											$dbsupp = mysql_query($qsupp);
											while($row_supp = mysql_fetch_array($dbsupp)){
												$course= $row_supp['CourseCode'];
												$coption = $row_supp['Status']; 
												$grade='';
												$supp='';
												$RegNo = $regno;
													#include grading scale
													include 'includes/choose_studylevel.php';	
													if(($supp=='!')&&($marks>0)){
														$pdf->text($x+$k+$cw+97, $y+12, ','.$course);
														$k =$k+35;
													}
												#empty option value
												$coption='';
											}
									}else{
										$pdf->text($x+$cw+97, $y+12, $ovremark);
									}

									if ($igrade<>'I'){
									   }
										if($fsup=='!'){
											$fsup = '';
											//$overallsuppcount = $overallsuppcount+1;
											
										}elseif (($igrade<>'I') || ($fsup<>'!')){
											//$overallpasscount= $overallpasscount+1;
										}

										if($totalinccount>0){
											$igrade = '';
											//$overallinccount = $overallinccount+1;
										}
						}
				
					  $i=$i+1;
					  $y=$y+15;
					  if($y>=$ypoint-50){
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 8);     
						$pdf->text(50, $ypoint, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text($xpoint,$ypoint, 'Page '.$pg);   
						
						#count unregistered
						$j=0;
						#count sex
						$fmcount = 0;
						$mcount = 0;
						$fcount = 0;
						#set table header
						#print row 1
						$pdf->line($x, $y, $xpoint+25, $y); //first horizontal line
						$pdf->line($x, $y+14, $xpoint+25, $y+14); //second horizontal line
						$pdf->line($x, $y, $x, $y+14); //first vertical line
						$pdf->line($xpoint+25, $y, $xpoint+25, $y+14); //last vertical line
						#print row 2
						$pdf->line($x, $y+28, $xpoint+25, $y+28); //second horizontal line
						$pdf->line($x, $y+14, $x, $y+28); //first vertical line
						$pdf->line($xpoint+25, $y+14, $xpoint+25, $y+28); //last vertical line
						#print row 3
						//$pdf->line($x+150, $y+42, $xpoint+25, $y+42); //second horizontal line
						$pdf->line($x, $y+28, $x, $y+42); //first vertical line
						$pdf->line($xpoint+25, $y+28, $xpoint+25, $y+42); //last vertical line
						#print row 4
						$pdf->line($x, $y+56, $xpoint+25, $y+56); //second horizontal line
						$pdf->line($x, $y+42, $x, $y+56); //first vertical line
						$pdf->line($xpoint+25, $y+42, $xpoint+25, $y+56); //last vertical line
						#print row 5
						$pdf->line($x, $y+70, $xpoint+25, $y+70); //second horizontal line
						$pdf->line($x, $y+56, $x, $y+70); //first vertical line
						$pdf->line($xpoint+25, $y+56, $xpoint+25, $y+70); //last vertical line
						
						#print module column
						$pdf->line($x+150, $y, $x+150, $y+70); 
						$pdf->line($x+25, $y+56, $x+25, $y+70); 
						$pdf->line($x+120, $y+56, $x+120, $y+70); 
						#print text
							$pdf->setFont('Arial', '', 10); 
							$pdf->text($x+2, $y+12, 'Module Credits:');
							$pdf->text($x+2, $y+24, 'Module Code:');
							$pdf->text($x+2, $y+48, 'Exam Components:');
							$pdf->text($x+2, $y+68, 'S/No');
							$pdf->text($x+26, $y+68, 'Reg. No.');
							$pdf->text($x+122, $y+68, 'Sex');
							$pdf->text($x+275, $y+68, 'Examinations Results'); 
						
						
						#reset values of x,y
						$x=50; $y=$y+56;
						#set colm width
						$clmw = 80;
						#get column width factor
						$cwf = 80;
						#calculate course clumns widths
						$cw = $cwf*$totalcolms;
						$header=$x+150;
						$x=$x+235;
						$x=$x-85;
						$jheader=0;
						$pdf->line($x+$cw, $y, $x+$cw, $y+14);	$pdf->text($x+$cw+1, $y+12, 'CRDT');
						$pdf->line($x+$cw+30, $y, $x+$cw+30, $y+14);	$pdf->text($x+$cw+32, $y+12, 'PNTS'); $pdf->text($x+$cw+22, $y-8, 'OVERALL PERFORMANCE'); 
						$pdf->line($x+$cw+60, $y, $x+$cw+60, $y+14);	$pdf->text($x+$cw+67, $y+12, 'GPA'); 
						$pdf->line($x+$cw+95, $y, $x+$cw+95, $y+14);	$pdf->text($x+$cw+97, $y+12, 'Remark');
						$pdf->line($xpoint+25, $y, $xpoint+25, $y+14);  
						$y=$y+15;
							#print exam categories headers
							if ($jheader==0){
							$cheader = $header;
							while ($rownheader = mysql_fetch_array($dbcourseheader)) {
								#get course unit
								$qunits = "SELECT Units FROM course WHERE CourseCode='$courseheader'";
								$dbunits = mysql_query($qunits);
								$row_units = mysql_fetch_array($dbunits);
								$unit=$row_units['Units'];
								#print courses as headers
								$unitheader = $unit;
								$courseheader = $rownheader['CourseCode']; 
								$pdf->text($cheader+26, $y-60, $unitheader);
								$pdf->text($cheader+22, $y-48, $courseheader);
								$cheader = $cheader+$clmw; 
							}
								for ($jheader =0; $jheader < $totalcolms; $jheader++){
									 #partition the header
									 $pdf->setFont('Arial', '', 8); 
									 $pdf->text($header+3, $y-35, 'CW'); 	
									 $pdf->text($header+3, $y-17, 'X/40');
									 $pdf->line($header+20, $y-42, $header+20, $y-15); 
									 $pdf->text($header+22, $y-35, 'FE'); 
									 $pdf->line($header, $y-30, $header+60, $y-30); 
									 $pdf->text($header+22, $y-17, 'X/60');
									 $pdf->line($header+40, $y-42, $header+40, $y-15); $pdf->text($header+41, $y-35, 'TOTL'); 
									 $pdf->text($header+42, $y-17, 'X/100');
									 $pdf->line($header+60, $y-42, $header+60, $y-15); $pdf->text($header+65, $y-35, 'GD');
									 $pdf->line($header+80, $y-42, $header+80, $y-15);
									 $pdf->line($header+80, $y-70, $header+80, $y-15); 
									 $header = $header+$clmw; 
									 $pdf->setFont('Arial', '', 10); 
								}
						  }
						  $jheader++;
					  }
				  } //ends if $total_rows
			}//ends $rowstudent loop

#start new page for the keys
$space = $ypoint-50 - $y;
$yind = $y+80; 
//if($space<70){
	$pdf->addPage();  

			$x=50;
			$y=50;
			$pg=$pg+1;
			$tpg =$pg;
			$pdf->setFont('Arial', 'I', 8);     
			$pdf->text(50, $ypoint-50, 'Printed On '.$today = date("d-m-Y H:i:s"));   
			$pg=$pg+1;
			$pdf->text($xpoint,$ypoint-50, 'Page '.$pg);   
			$yind = $y; 
			$i =$i-1;
			 #print statistics
			$pdf->setFont('Arial', 'B', 12);     
			 $pdf->text(105, $yind-15, 'OVERALL PERFORMANCE'); 
			$pdf->setFont('Arial', '', 8);     
			 $pdf->line(50, $yind, 550, $yind);
			 $pdf->line(550, $yind, 550, $yind+93);
			 $pdf->line(50, $yind, 50, $yind+93); 
			 $pdf->line(50, $yind+93, 550, $yind+93); 
			 
			 $pdf->line(250, $yind, 250, $yind+93); 
			 $pdf->line(350, $yind, 350, $yind+93); 
			 $other = $i - ($overallpasscount+$overallsuppcount+$overallinccount+$overalldiscocount);

			 $pdf->text(55, $yind+15, 'PASS'); 
			 $pdf->text(255, $yind+15, $overallpasscount);
			 $pdf->text(355, $yind+15, number_format($overallpasscount/$i*100,0).'%'); 
			 $pdf->line(50, $yind+18, 550, $yind+18);
			
			 $pdf->text(55, $yind+30, 'SUPPLEMENT'); 
			 $pdf->text(255, $yind+30, $overallsuppcount);
			 $pdf->text(355, $yind+30, number_format($overallsuppcount/$i*100,0).'%'); 
			 $pdf->line(50, $yind+33, 550, $yind+33);
			 
			 $pdf->text(55, $yind+45, 'ABSCONDED'); 
			 $pdf->text(255, $yind+45, $other );
			 $pdf->text(355, $yind+45, number_format($other/$i*100,0).'%'); 
			 $pdf->line(50, $yind+48, 550, $yind+48);
			 
			 $pdf->text(55, $yind+60, 'FAIL & DISCONT\'D'); 
			 $pdf->text(255, $yind+60, $overalldiscocount);
			 $pdf->text(355, $yind+60, number_format($overalldiscocount/$i*100,0).'%'); 
			 $pdf->line(50, $yind+63, 550, $yind+63);
			
			 $pdf->text(55, $yind+75, 'OTHER REMARKS'); 
			 $pdf->text(255, $yind+75, $overallinccount);
			 $pdf->text(355, $yind+75, number_format($overallinccount/$i*100,0).'%'); 
			 $pdf->line(50, $yind+78, 550, $yind+78);
			 
			 $pdf->text(55, $yind+90, 'TOTAL CANDIDATES'); 
			 $pdf->text(255, $yind+90, $i);
			 $pdf->text(355, $yind+90, number_format($i/$i*100,0).'%'); 
			 $pdf->line(50, $yind+93, 550, $yind+93);
	//	}
	

 	 #output file
	 $filename = ereg_replace("[[:space:]]+", "",$progname);
	 $pdf->output($filename.'.pdf');
	// }
}//end of print pdf
?>
<?php 
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('lecturerMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Examination';
	$szSubSection = 'Semester Results';
	$szTitle = 'Printing Semester Examinations Results Report';
	include('lecturerheader.php');
$editFormAction = $_SERVER['PHP_SELF'];

mysql_select_db($database_zalongwa, $zalongwa);
$query_studentlist = "SELECT RegNo, Name, ProgrammeofStudy FROM student ORDER BY ProgrammeofStudy  ASC";
$studentlist = mysql_query($query_studentlist, $zalongwa) or die(mysql_error());
$row_studentlist = mysql_fetch_assoc($studentlist);
$totalRows_studentlist = mysql_num_rows($studentlist);

mysql_select_db($database_zalongwa, $zalongwa);
$query_degree = "SELECT ProgrammeCode, ProgrammeName FROM programme ORDER BY ProgrammeName ASC";
$degree = mysql_query($query_degree, $zalongwa) or die(mysql_error());
$row_degree = mysql_fetch_assoc($degree);
$totalRows_degree = mysql_num_rows($degree);

mysql_select_db($database_zalongwa, $zalongwa);
$query_ayear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$ayear = mysql_query($query_ayear, $zalongwa) or die(mysql_error());
$row_ayear = mysql_fetch_assoc($ayear);
$totalRows_ayear = mysql_num_rows($ayear);

mysql_select_db($database_zalongwa, $zalongwa);
$query_sem = "SELECT Semester FROM terms ORDER BY Semester Limit 2";
$sem = mysql_query($query_sem, $zalongwa) or die(mysql_error());
$row_sem = mysql_fetch_assoc($sem);
$totalRows_sem = mysql_num_rows($sem);

mysql_select_db($database_zalongwa, $zalongwa);
$query_dept = "SELECT Faculty, DeptName FROM department ORDER BY DeptName, Faculty ASC";
$dept = mysql_query($query_dept, $zalongwa) or die(mysql_error());
$row_dept = mysql_fetch_assoc($dept);
$totalRows_dept = mysql_num_rows($dept);
?>
<?php
			
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
	?>
	<style type="text/css">
	<!--
	.style1 {color: #FFFFFF}
	-->
	</style>
	
	<h4 align="center">
	
	<?php 
	$prog=$_POST['degree'];
	$cohotyear = $_POST['cohot'];
	$ayear = $_POST['ayear'];
	$qprog= "SELECT ProgrammeCode, Title FROM programme WHERE ProgrammeCode='$prog'";
	$dbprog = mysql_query($qprog);
	$row_prog = mysql_fetch_array($dbprog);
	$progname = $row_prog['Title'];
	$qyear= "SELECT AYear FROM academicyear WHERE AYear='$cohotyear'";
	$dbyear = mysql_query($qyear);
	$row_year = mysql_fetch_array($dbyear);
	$year = $row_year['AYear'];
	echo $progname;
	echo " - ".$year;
	?>
	
	<br>
	</h4>
	<?php
	
	@$checkdegree = addslashes($_POST['checkdegree']);
	@$checkyear = addslashes($_POST['checkyear']);
	@$checksem = addslashes($_POST['checksem']);
	$checkcohot = addslashes($_POST['checkcohot']);
	
	$c=0;
	
	if (($checkdegree=='on') && ($checkyear == 'on') && ($checksem == 'on')&& ($checkcohot == 'on')){
	}elseif (($checkdegree=='on') && ($checkcohot == 'on')){
	}elseif ($checkcohot == 'on'){
	}
	}else{
	?>
	
	<form name="form1" method="post" action="<?php echo $editFormAction ?>">
	            <div align="center">
				<table width="200" border="0" bgcolor="#CCCCCC">
	            <tr>
	                  <td colspan="3"><span class="style61">if you want to filter the results by  criteria <span class="style34">Tick the corresponding check box first</span> then select appropriately </span></td>
	                </tr>
	                <tr>
	                  <td nowrap><input name="checkdegree" type="checkbox" id="checkdegree" value="on" checked></td>
	                  <td nowrap><div align="left">Degree Programme:</div></td>
	                  <td>
	                      <div align="left">
	                        <select name="degree" id="degree">
	                          <?php
	do {  
	?>
	                          <option value="<?php echo $row_degree['ProgrammeCode']?>"><?php echo $row_degree['ProgrammeName']?></option>
	                          <?php
	} while ($row_degree = mysql_fetch_assoc($degree));
	  $rows = mysql_num_rows($degree);
	  if($rows > 0) {
	      mysql_data_seek($degree, 0);
		  $row_degree = mysql_fetch_assoc($degree);
	  }
	?>
	                        </select>
	                    </div></td></tr>
	                <tr>
	                  <td><input name="checkcohot" type="checkbox" id="checkcohot" value="on" checked></td>
	                  <td nowrap><div align="left">Cohort of the  Year: </div></td>
	                  <td><div align="left">
	                    <select name="cohot" id="cohot">
	                        <?php
	do {  
	?>
	                        <option value="<?php echo $row_ayear['AYear']?>"><?php echo $row_ayear['AYear']?></option>
	                        <?php
	} while ($row_ayear = mysql_fetch_assoc($ayear));
	  $rows = mysql_num_rows($ayear);
	  if($rows > 0) {
	      mysql_data_seek($ayear, 0);
		  $row_ayear = mysql_fetch_assoc($ayear);
	  }
	?>
	                    </select>
	                  </div></td>
	                </tr>
	            	<tr>
	                  <td><input name="checkyear" type="checkbox" id="checkyear" value="on" checked></td>
	                  <td nowrap><div align="left">Results of the  Year: </div></td>
	                  <td><div align="left">
	                    <select name="ayear" id="ayear">
	                        <?php
	do {  
	?>
	                        <option value="<?php echo $row_ayear['AYear']?>"><?php echo $row_ayear['AYear']?></option>
	                        <?php
	} while ($row_ayear = mysql_fetch_assoc($ayear));
	  $rows = mysql_num_rows($ayear);
	  if($rows > 0) {
	      mysql_data_seek($ayear, 0);
		  $row_ayear = mysql_fetch_assoc($ayear);
	  }
	?>
	                    </select>
	                  </div></td>
	                </tr>
	            	<tr>
	                  <td><input name="checksem" type="checkbox" id="checksem" value="on" checked></td>
	                  <td nowrap><div align="left">Semester: </div></td>
	                  <td><div align="left">
	                    <select name="sem" id="sem">
	                        <?php
	do {  
	?>
	                        <option value="<?php echo $row_sem['Semester']?>"><?php echo $row_sem['Semester']?></option>
	                        <?php
	} while ($row_sem = mysql_fetch_assoc($sem));
	  $rows = mysql_num_rows($sem);
	  if($rows > 0) {
	      mysql_data_seek($sem, 0);
		  $row_sem = mysql_fetch_assoc($sem);
	  }
	?>
	                    </select>
	                  </div></td>
	                </tr>
	                <?php 
	                #check if paid
if($userFaculty==31){
?>
	<input name="checkwh" type="hidden" id="checkwh" value="on" checked>
	<?php     
		}else{
		?>	
		<tr>
	                  <td><input name="checkwh" type="checkbox" id="checkwh" value="on" checked></td>
	                  <td colspan="3" nowrap><div align="left">Hide withheld results in HTML Report </div></td>
	                  <td><div align="left">
	                    
	                  </div></td>
	                </tr>
	  <?php 
	}
?>                
	                <tr>
	                  <td colspan="2"><div align="center">
	                  <?php if($userFaculty<>31){
?>
	                    <input type="submit" name="PDF"  id="PDF" value="Print PDF">
	                    <?php }?>
	                  </div></td>
	                  <td colspan="1"><div align="center">
	                    <input type="submit" name="HTML"  id="HTML" value="Print HTML">
	                  </div></td>
	              </tr>
	              </table>
	              <input name="MM_update" type="hidden" id="MM_update" value="form1">       
	  </div>
	</form>
	<?php
}
include('../footer/footer.php');
?>