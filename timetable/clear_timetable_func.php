<?php
	/**
	* @file clear_timetable_func.php
	* @ created on Sun,March 10 2013
	* @ author lackson david
	* */
	
	function clearTimetable($ayear,$cat){
	 /**
	  *  validate input data
	  * */
	  
	 // reset value to no error occured
	 $err=FALSE;
	 $error="";	
		
	// check academic year format
	$year = explode('/',$ayear);
	$yrdiff = $year[1]-$year[0];	 
	if($yrdiff != 1){
		 $err= TRUE;
		 $error= "Sorry, System can not copy timetable due to wrong academic year format";
		 echo '<meta http-equiv = "refresh" content ="0; url = copytimetable.php?error='.urlencode($error).'">';
		 exit;
		}	
		
	  // check timetable categoty exist
	  $sql_timetablecategory= "SELECT name FROM timetableCategory WHERE Id=".$cat;
	  $result_timetablecategory = mysql_query($sql_timetablecategory);
	  $row_timetablecategory = mysql_fetch_array($result_timetablecategory);
	  $catname = $row_timetablecategory['name'];
	  $numrow_cat = mysql_num_rows($result_timetablecategory);
	  
	  // row required to be deleted
	  $sql_reqdel = "SELECT * FROM timetable WHERE AYear = '$ayear' AND timetable_category = '$cat'";
	  $rowreq = mysql_num_rows(mysql_query($sql_reqdel));

	if($numrow_cat < 1)	 {
		 $err= TRUE;
		 $error= "The timetable category you pick does not exis,please pick correct one";
		 echo '<meta http-equiv = "refresh" content ="0; url = copytimetable.php?error='.urlencode($error).'">';
		 exit;
		}
	/**
	 * delete timetable
	 * */	
		 	
	$sql_del = "DELETE FROM timetable WHERE AYear = '$ayear' AND timetable_category = '$cat'";
	$result_del = mysql_query($sql_del);
	$numrow_del=mysql_affected_rows();
	$rowdiff = $rowreq-$numrow_del;
	if($numrow_del > 0){
		 $success= "Successfully done</br>";
		 $summary="Timetable for $ayear - $catname is deleted<br/>";
		 $summary.= "Row found : $rowreq<br/>Rows deleted : $numrow_del<br/>Rows not deleted: $rowdiff";
		 echo '<meta http-equiv = "refresh" content ="0; url = copytimetable.php?success='.urlencode($success).'&det='.urldecode($summary).'">';
		 exit;
		
		}	 	
		}
?>
