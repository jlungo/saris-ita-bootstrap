<?php 
#start pdf
if (isset($_POST['PrintPDF']) && ($_POST['PrintPDF'] == "Print PDF")) {
	#get post variables
	$rawkey = addslashes(trim($_POST['key']));
	$inst= addslashes(trim($_POST['cmbInst']));
	$cat= addslashes(trim($_POST['cat']));
	$award= addslashes(trim($_POST['award']));
	$key = ereg_replace("[[:space:]]+", " ",$rawkey);
	#get content table raw height
	$rh= addslashes(trim($_POST['sex']));

	#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
	require_once('../Connections/zalongwa.php');
	
	#process report title
	if($cat=='1'){
	$rtitle='STATEMENT OF EXAMINATIONS RESULTS';
	$xtitle = 180;
	}elseif($cat=='2'){
	$rtitle='PROGRESS REPORT ON STUDENT ACADEMIC PERFORMANCE';
	$xtitle = 130;
	}
	$titlelenth = strlen($inst);
	if ($titlelenth<30){
	$xinst = 190;
	}else{
	$xinst = 161;
	}
	
	#check if u belongs to this faculty
	$qFacultyID = "SELECT FacultyID from faculty WHERE FacultyName = '$inst'";
	$dbFacultyID = mysql_query($qFacultyID);
	$rowFacultyID = mysql_fetch_array($dbFacultyID);
	$studentFacultyID = $rowFacultyID['FacultyID'];

	include('includes/PDF.php');
	$i=0;
	$pg=1;
	$tpg =$pg;
	//select student
	$qstudent = "SELECT * from student WHERE RegNo = '$key'";
	$dbstudent = mysql_query($qstudent) or die("Mwanafunzi huyu hana matokeo".  mysql_error()); 
	$row_result = mysql_fetch_array($dbstudent);
		$sname = $row_result['Name'];
		$regno = $row_result['RegNo'];
		$degree = $row_result['ProgrammeofStudy'];
		$sex = $row_result['Sex'];
		$dbirth = $row_result['DBirth'];
		$entry = $row_result['EntryYear'];
		$faculty = $row_result['Faculty'];
		$citizen = $row_result['Nationality'];
		$address = $row_result['Address'];
		$gradyear = $row_result['GradYear'];
		$admincriteria = $row_result['MannerofEntry'];
		$campus = $row_result['Campus'];
		$faculty = $row_result['Faculty'];
		$subject = $row_result['Subject'];

		//get degree name
		$qdegree = "Select Title from programme where ProgrammeCode = '$degree'";
		$dbdegree = mysql_query($qdegree);
		$row_degree = mysql_fetch_array($dbdegree);
		$programme = $row_degree['Title'];

	$pdf = &PDF::factory('p', 'a4');      // Set up the pdf object. 
	$pdf->open();                         // Start the document. 
	$pdf->setCompression(true);           // Activate compression. 
	$pdf->addPage();  
	#put page header
	
	#print header
	$pdf->image('../images/slogo.jpg', 50, 46);   
	$pdf->setFont('Arial', 'I', 8);      
	$pdf->text(530.28, 820.89, 'Page '.$pg);   
	#print header for landscape paper layout 
	$pdf->setFont('Arial', 'B', 24.5); 
	$pdf->text(50, 50, strtoupper($org));   
	
	#University Addresses
	$pdf->setFont('Arial', '', 13.3);    
	$pdf->text($xinst, 90, strtoupper($inst));
	$pdf->setFont('Arial', '', 13);    
	$pdf->text($xtitle, 129, $rtitle); 
	$pdf->setFillColor('rgb', 0, 0, 0);   

	#title line
	$pdf->line(50, 135, 570, 135);

	$pdf->setFont('Arial', 'B', 10.3);     
	#set page header content fonts
	#line1
	$pdf->line(50, 135, 50, 147);       
	$pdf->line(383, 135, 383, 147);       
	$pdf->line(432, 135, 432, 147);
	$pdf->line(570, 135, 570, 147);       
	$pdf->line(50, 147, 570, 147); 

	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(50, 145, 'NAME:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(90, 145, $sname); 
	$pdf->setFont('Arial', 'B', 10.3); 	$pdf->text(385, 145, 'SEX:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(415, 145, $sex); 
	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(435, 145, 'RegNo.:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(480, 145, $regno); 
	
	
	#line2
	$pdf->line(50, 147, 50, 159);       
	$pdf->line(188, 147, 188, 159);       
	$pdf->line(570, 147, 570, 159);       
	$pdf->line(50, 159, 570, 159); 
	
	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(50, 157, 'CITIZENSHIP:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(118, 157, $citizen); 
	$pdf->setFont('Arial', 'B', 10.3); 	$pdf->text(190, 157, 'ADDRESS:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(250, 157, $address); 
	
	#line3
	$pdf->line(50, 159, 50, 171);       
	$pdf->line(188, 159, 188, 171);       
	//$pdf->line(383, 159, 383, 171);       
	$pdf->line(570, 159, 570, 171);       
	$pdf->line(50, 171, 570, 171); 

	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(50, 169, 'BIRTH DATE:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(120, 169, $dbirth); 
	$pdf->setFont('Arial', 'B', 10.3); 	$pdf->text(190, 169, 'ADMITTED:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(250, 169, $entry); 
	#line5
	$pdf->line(50, 171, 50, 183);       
	//$pdf->line(268, 183, 268, 194);       
	$pdf->line(570, 171, 570, 183);       
	$pdf->line(50, 183, 570, 183); 

	$pdf->setFont('Arial', 'B', 10.3); 	$pdf->text(50, 182, 'INSTITUTE:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(160, 182, $faculty); 
    
	#line6
	$pdf->line(50, 183, 50, 195);       
	$pdf->line(570, 183, 570, 195);       
	$pdf->line(50, 195, 570, 195); 
	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(50, 194, 'NAME OF PROGRAMME:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(175, 194, $programme); 
    
	#initialize x and y
	$x=50;
	$y=199;
	#initialise total units and total points
	$annualUnits=0;
	$annualPoints=0;
	
	$yval=$y+33;
	$y=$y+33;

	#set page body content fonts
	$pdf->setFont('Arial', '', 9.5);    

	//query academeic year
	$qayear = "SELECT DISTINCT AYear from examresult WHERE RegNo = '$regno' and checked=1 ORDER BY AYear ASC";
	$dbayear = mysql_query($qayear);
	#initialise ayear
	$examyear = 0;
	//query exam results sorted per years
	while($rowayear = mysql_fetch_object($dbayear)){
		$examyear = $examyear +1;
		$currentyear = $rowayear->AYear;
		$query_examresult = "
							  SELECT DISTINCT course.CourseName, 
											  course.Units, 
											  course.Department,
											  course.StudyLevel, 
											  examresult.CourseCode, 
											  examresult.Status 
							  FROM 
									course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
							  WHERE (examresult.RegNo='$regno') AND 
									(examresult.AYear = '$currentyear') AND 
									(course.Programme = '$degree') AND 
									(examresult.Checked='1') 
							  ORDER BY examresult.AYear DESC, course.YearOffered";	
		$result = mysql_query($query_examresult); 
		$query = @mysql_query($query_examresult);
		$dbcourseUnit = mysql_query($query_examresult);
						
		if (mysql_num_rows($query) > 0){
		
				$totalunit=0;
				$unittaken=0;
				$sgp=0;
				$totalsgp=0;
				$gpa=0;
		#check if u need to sart a new page
		$blank=$y-12;
		$space = 820.89 - $blank;
		if ($space<150){
		#start new page
		$pdf->addPage();  
		
			$x=50;
			$yadd=50;

			$y=80;
			$pg=$pg+1;
			$tpg =$pg;
			include 'includes/transcriptfooter.php';
		}
		#create table header
		if($examyear==1){
			$pdf->text($x, $y-$rh, 'FIRST YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear); 
		}elseif($examyear==2){
			$pdf->text($x, $y-$rh, 'SECOND YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear); 
		}elseif($examyear==3){
			$pdf->text($x, $y-$rh, 'THIRD YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear); 
		}elseif($examyear==4){
			$pdf->text($x, $y-$rh, 'FOURTH YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear); 
		}elseif($examyear==5){
			$pdf->text($x, $y-$rh, 'FIFTH YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear); 
		}elseif($examyear==6){
			$pdf->text($x, $y-$rh, 'SIXTH YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear); 
		}elseif($examyear==7){
			$pdf->text($x, $y-$rh, 'SEVENTH YEAR EXAMINATIONS RESULTS: '.$rowayear->AYear); 
		}
		$pdf->text($x+1, $y, 'Semester'); 
		$pdf->text($x+45, $y, 'Code'); 
		$pdf->text($x+84, $y, 'Course Title'); 
		$pdf->text($x+412, $y, 'Unit'); 
		$pdf->text($x+437, $y, 'Grade'); 
		$pdf->text($x+472, $y, 'Point'); 
		$pdf->text($x+500, $y, 'GPA'); 
		
		#calculate results
		$i=1;
		while($row_course = mysql_fetch_array($dbcourseUnit)){
			$course= $row_course['CourseCode'];
			$unit = $row_course['Units'];
			$cname = $row_course['CourseName'];
			$coursefaculty = $row_course['Department'];
			$sn=$sn+1;
			$remarks = 'remarks';
			$grade='';
		
		$RegNo = $regno;
		
		#include grading system 					
		include 'includes/choose_studylevel.php';
				
				$coursecode = $course;
				
				#print results
				$pdf->text($x+16, $y+$rh, $semid); 
				$pdf->text($x+45, $y+$rh, substr($coursecode,0,6)); 
				$pdf->text($x+84, $y+$rh, substr($cname,0,73)); 
				$pdf->text($x+413, $y+$rh, $unit); 
				$pdf->text($x+445, $y+$rh, $grade); 
				$pdf->text($x+477, $y+$rh, $sgp); 
				#check if the page is full
				$x=$x;
				#draw a line
				$pdf->line($x, $y-$rh+2, 570.28, $y-$rh+2);        
				$pdf->line($x, $y-$rh+2, $x, $y);       
				$pdf->line(570.28, $y-$rh+2, 570.28, $y);      
				$pdf->line($x, $y-$rh+2, $x, $y+$rh+4);              
				$pdf->line(570.28, $y-$rh+2, 570.28, $y+$rh+4);      
				$pdf->line($x+498, $y-$rh+2, $x+498, $y+$rh+4);       
				$pdf->line($x+468, $y-$rh+2, $x+468, $y+$rh+4);     
				$pdf->line($x+434, $y-$rh+2, $x+434, $y+$rh+4);       
				$pdf->line($x+410, $y-$rh+2, $x+410, $y+$rh+4);  
				$pdf->line($x+82, $y-$rh+2, $x+82, $y+$rh+2); 
				$pdf->line($x+43, $y-$rh+2, $x+43, $y+$rh+2); 
				#get space for next year
				$y=$y+$rh;

				if ($y>800){
					#put page header
					//include('PDFTranscriptPageHeader.inc');
					$pdf->addPage();  
		
						$x=50;
						$yadd=50;
		
						$y=80;
						$pg=$pg+1;
						$tpg =$pg;
						include 'includes/transcriptfooter.php';
					#title line
					$pdf->line(50, 135, 570, 135);
					#set page body content fonts
					$pdf->setFont('Arial', '', 9.5);    
				}
				#draw a line
				$pdf->line($x, $y+$rh+2, 570.28, $y+$rh+2);       
				$pdf->line($x, $y-$rh+2, $x, $y+$rh+2); 
				$pdf->line(570.28, $y-$rh+2, 570.28, $y+$rh+2);      
				$pdf->line($x+498, $y-$rh+2, $x+498, $y+$rh+2);       
				$pdf->line($x+468, $y-$rh+2, $x+468, $y+$rh+2);       
				$pdf->line($x+434, $y-$rh+2, $x+434, $y+$rh+2);      
				$pdf->line($x+410, $y-$rh+2, $x+410, $y+$rh+2); 
				$pdf->line($x+82, $y-$rh+2, $x+82, $y+$rh+2); 
				$pdf->line($x+43, $y-$rh+2, $x+43, $y+$rh+2);      
			  }//ends while loop
			  #check degree
			  //if(($degree==632)||($degree==633)||($degree==635)){
				$pdf->setFont('Arial', 'BI', 9.5);     
				$pdf->text($x+84, $y+$rh+1, 'Sub-total');
				$pdf->text($x+413, $y+$rh+1, $unittaken); 
				$pdf->text($x+470, $y+$rh+1, $totalsgp); 
				$pdf->text($x+504, $y+$rh+1,@substr($totalsgp/$unittaken, 0,3)); 
				$pdf->setFont('Arial', '', 9.5); 
			//  }#end check degree    
				#check x,y values
				$y=$y+3.5*$rh;
				//$x=$y+22;
				if ($y==800){
					$pdf->addPage();  
						$x=50;
						$yadd=50;
		
						$y=80;
						$pg=$pg+1;
						$tpg =$pg;
						include 'includes/transcriptfooter.php';
						#title line
						$pdf->line(50, 135, 570, 135);
						#set page body content fonts
						$pdf->setFont('Arial', '', 9.5);    
				}
				
			 }
				#get annual units and Points
				$annualUnits = $annualUnits+$unittaken;
				$annualPoints = $annualPoints+$totalsgp;

			 }
				$avgGPA=@substr($annualPoints/$annualUnits, 0,3);
				#specify degree classification
				if ($award==1){
					if($avgGPA>=4.4){
							if ($checksupp ==1){
								$degree = 'First Class';
							}else{
								$degree = 'First Class (Honours)';
							}
						}elseif($avgGPA>=3.5){
							if ($checksupp ==1){
								$degree = 'Uppersecond Class';
							}else{
								$degree = 'Uppersecond Class (Honours)';
							}
						}elseif($avgGPA>=2.7){
							if ($checksupp ==1){
								$degree = 'Lowersecond Class';
							}else{
								$degree = 'Lowersecond Class (Honours)';
							}
						}elseif($avgGPA>=2.0){
							$degree = 'Pass';
						}else{
							$degree = 'FAIL';
						}
				}elseif($award==2){
					if($avgGPA>=4.4){
							if ($checksupp ==1){
								$degree = 'First Class';
							}else{
								$degree = 'First Class (Honours)';
							}
						}elseif($avgGPA>=3.5){
							if ($checksupp ==1){
								$degree = 'Uppersecond Class';
							}else{
								$degree = 'Uppersecond Class (Honours)';
							}
						}elseif($avgGPA>=2.7){
							if ($checksupp ==1){
								$degree = 'Lowersecond Class';
							}else{
								$degree = 'Lowersecond Class (Honours)';
							}
						}elseif($avgGPA>=2.0){
							$degree = 'Pass';
						}else{
							$degree = 'FAIL';
						}
				}elseif($award==3){
					if($avgGPA>=4.4){
							if ($checksupp ==1){
								$degree = 'First Division';
							}else{
								$degree = 'First Division  (Honours)';
							}
						}elseif($avgGPA>=3.5){
							if ($checksupp ==1){
								$degree = 'Second Division';
							}else{
								$degree = 'Second Division (Honours)';
							}
						}elseif($avgGPA>=2.7){
							if ($checksupp ==1){
								$degree = 'Third Division';
							}else{
								$degree = 'Third Division (Honours)';
							}
						}elseif($avgGPA>=2.0){
							$degree = 'Pass';
						}else{
							$degree = 'FAIL';
						}
				}

				if($cat=='1'){
					//if(($degree==632)||($degree==633)||($degree==635)){
					$pdf->setFont('Arial', 'B', 10.3);  $pdf->text($x, $y+24, 'OVERALL G.P.A.:'); $pdf->text($x+95, $y+24, @substr($annualPoints/$annualUnits, 0,3));
					//$pdf->setFont('Arial', 'B', 10.3); 	$pdf->text($x+220, $y+24, 'CLASSIFICATION:'); $pdf->text($x+320, $y+24, $degreeclass);
					$pdf->line($x, $y+27, 570.28, $y+27); 
					}else{
					$pdf->setFont('Arial', 'B', 10.3);  $pdf->text($x, $y+24, 'OVERALL PERFORMANCE:'); $pdf->text($x+145, $y+24, 'PASS');
					$pdf->line($x, $y+27, 570.28, $y+27); 
					//}
				}
				
				#get officer fullname
				$qname="SELECT FullName FROM security WHERE UserName='$username'";
				$dbname=mysql_query($qname);
				$row_name=mysql_fetch_assoc($dbname);
				$ofname=$row_name['FullName'];
				#see if we have a space to rint signature
				$b=$y+17;
				if ($b<820.89){
				#print signature lines
				$pdf->text($x+190, $y+47, '.................................................');    						
				$pdf->text($x+200, $y+57, strtoupper($ofname));  
				$pdf->text($x+205, $y+67, 'REGISTRAR');   						
				} 						
				$pdf->setFont('Arial', 'I', 8);     
				$pdf->text(50, 820.89, $city.', '.$today = date("d-m-Y H:i:s"));   

	#print the key index
	$pdf->setFont('Arial', 'I', 9); 
	$yind = $y+87;
	
	#check if there is enough printing area
	$indarea = 820.89-$yind;
	if ($indarea< 203){
			$pdf->addPage();  

			$x=50;
			$y=80;
			$pg=$pg+1;
			$tpg =$pg;
			$pdf->setFont('Arial', 'I', 8);     
			$pdf->text(530.28, 820.89, 'Page '.$pg);  
			$pdf->text(50, 820.89, $city.', '.$today = date("d-m-Y H:i:s")); 
			$yind = $y; 
    }
	include 'includes/transcriptkeys.php';
	#print the file
	$pdf->output($key.'.pdf');              // Output the 
}/*ends is isset*/
#ends pdf
#get connected to the database and verfy current session
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');
# initialise globals
include('lecturerMenu.php');

# include the header
global $szSection, $szSubSection;
$szSection = 'Examination';
$szSubSection = 'Cand. Statement';
$szTitle = 'Student\'s Statement of Examination Results';
include('lecturerheader.php');

mysql_select_db($database_zalongwa, $zalongwa);
$query_campus = "SELECT FacultyName FROM faculty WHERE FacultyID='$userFaculty' ORDER BY FacultyName ASC";
$campus = mysql_query($query_campus, $zalongwa) or die(mysql_error());
$row_campus = mysql_fetch_assoc($campus);
$totalRows_campus = mysql_num_rows($campus);


function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
if (isset($_POST['search']) && ($_POST['search'] == "PreView")) {
#get post variables
$rawkey = $_POST['key'];
$key = ereg_replace("[[:space:]]+", " ",$rawkey);
//select student
$qstudent = "SELECT * from student WHERE RegNo = '$key'";
$dbstudent = mysql_query($qstudent) or die("Mwanafunzi huyu hana matokeo".  mysql_error()); 
$row_result = mysql_fetch_array($dbstudent);
			$name = $row_result['Name'];
			$regno = $row_result['RegNo'];
			$degree = $row_result['ProgrammeofStudy'];
			$RegNo = $regno;
			
			//get degree name
			$qdegree = "Select Title from programme where ProgrammeCode = '$degree'";
			$dbdegree = mysql_query($qdegree);
			$row_degree = mysql_fetch_array($dbdegree);
			$programme = $row_degree['Title'];
			
			echo  "$name - $regno <br> $programme";	

				
//query academeic year
$qayear = "SELECT DISTINCT AYear FROM examresult WHERE RegNo = '$RegNo' ORDER BY AYear ASC";
$dbayear = mysql_query($qayear);

//query exam results sorted per years
while($rowayear = mysql_fetch_object($dbayear)){
$currentyear = $rowayear->AYear;

			# get all courses for this candidate
			$qcourse="SELECT DISTINCT course.Units, course.Department, course.CourseName, course.StudyLevel, examresult.CourseCode FROM 
						course INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
							 WHERE 
									(RegNo='$RegNo') AND 
									(course.Programme = '$degree') AND 
									(AYear='$currentyear')";	
			$dbcourse = mysql_query($qcourse) or die("No Exam Results for the Candidate - $key ");
			$total_rows = mysql_num_rows($dbcourse);
			
			if($total_rows>0){
			#initialise s/no
			$sn=0;
			#print name and degree
			//select student
				$qstudent = "SELECT Name, RegNo, ProgrammeofStudy from student WHERE RegNo = '$RegNo'";
				$dbstudent = mysql_query($qstudent) or die("Mwanafunzi huyu hana matokeo".  mysql_error()); 
				$row_result = mysql_fetch_array($dbstudent);
				$name = $row_result['Name'];
				$regno = $row_result['RegNo'];
				$degree = $row_result['ProgrammeofStudy'];
				
				//get degree name
				$qdegree = "Select Title from programme where ProgrammeCode = '$degree'";
				$dbdegree = mysql_query($qdegree);
				$row_degree = mysql_fetch_array($dbdegree);
				$programme = $row_degree['Title'];
							
							#initialise
							$totalunit=0;
							$unittaken=0;
							$sgp=0;
							$totalsgp=0;
							$gpa=0;
							?>
<table width="100%" height="100%" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td width="63" scope="col"><?php echo $rowayear->AYear;?></td>
	<td width="350" nowrap scope="col">Course</td>
    <td width="30" nowrap scope="col">Unit</td>
    <td width="38" nowrap scope="col">Grade</td>
    <td width="31" nowrap scope="col">Point</td>
    <td width="31" nowrap scope="col">GPA</td>
  </tr>
  <?php
		while($row_course = mysql_fetch_array($dbcourse)){
				$course= $row_course['CourseCode'];
				$unit= $row_course['Units'];
				$coursename= $row_course['CourseName'];
				$coursefaculty = $row_course['Department'];
				if($row_course['Status']==1){
				$status ='Core';
				}else{
					$status = 'Elective';
				}
					$sn=$sn+1;
					$remarks = 'remarks';
				
				include 'includes/choose_studylevel.php';

				#display results

				?>
	<tr>
     <td nowrap scope="col"><div align="left"><?php echo $course?></div></td>
     <td width="350" nowrap scope="col"><div align="left"><?php echo $coursename;?></div></td>
     <td width="30" nowrap scope="col"><div align="center"><?php echo $row_course['Units']?></div></td>
     <td width="38" nowrap scope="col"><div align="center"><?php echo $grade;?></div></td>
	 <td width="31" nowrap scope="col"><div align="center"><?php echo $sgp;?></div></td>
    <td width="31" nowrap scope="col"></td>
  </tr>
  <?php }?>
  	<tr>
     <td scope="col"></td>
     <td width="350" nowrap scope="col"></td>
     <td width="30" nowrap scope="col"><div align="center"><?php echo $unittaken;?></div></td>
     <td width="38" nowrap scope="col"></td>
	 <td width="31" nowrap scope="col"><div align="center"><?php echo $totalsgp;?></div></td>
    <td width="31" nowrap scope="col"><div align="center"><?php echo @substr($totalsgp/$unittaken, 0,3);?></div></td>
  </tr>
</table>
<?php }else{ 
					if(!@$reg[$c]){}else{
					echo "$c". ".Sorry, No Records Found for '$reg[$c]'<br><hr>";
							}
						}
				}//ends while rowayear	
mysql_close($zalongwa);

}else{

?>

<form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" name="studentRoomApplication" id="studentRoomApplication">
            <table width="284" border="1" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
        <tr>
          <td colspan="9" nowrap><div align="center"></div>
          </td>
        </tr>
        <tr>
          <td width="110"><div align="right"><strong></strong></div></td>
          <td colspan="8" bordercolor="#ECE9D8" bgcolor="#CCCCCC"><span class="style67">
            <input name="cmbInst" type="hidden" id="cmbInst" value="<?php echo $row_campus['FacultyName']?>">
          </span></td>
        </tr>
        <tr>
          <td nowrap><div align="right"><strong>Programme:
            </strong></div>            <div align="center"></div></td>
          <td colspan="2" nowrap><div align="right">Degree</div></td>
          <td width="31" nowrap><input type="radio" value="1" id="award1" name="award" checked></td>
          <td colspan="2" nowrap><div align="right">Diploma</div></td>
          <td width="31" nowrap><input type="radio" value="2" id="award2" name="award" ></td>
          <td width="89" nowrap><div align="right">Certificate</div></td>
		  <td width="30" nowrap><input type="radio" value="3" id="award2" name="award" ></td>
        </tr>
		<tr> 
		<td align="right"><strong>Category:</strong></td> 
		<td colspan="4">Finalist:
		  <input type="radio" value="1" id="cat" name="cat" checked>  </td> 
		<td colspan="4">Continuing:
		  <input type="radio" value="2" id="cat" name="cat">
		</td>
		</tr> 
		<tr>
          <td><div align="right"><strong><span class="style67">RegNo:</span></strong></div></td>
          <td colspan="8" bordercolor="#ECE9D8" bgcolor="#CCCCCC"><span class="style67">
          <input name="key" type="text" id="key" size="40" maxlength="40">
          </span></td>
        </tr>
		<tr> 
			<td align="right" nowrap><strong>Table:</strong></td> 
			<td width="35"><div align="center">11<input type="radio" value="11" id="sex" name="sex"></div></td> 
			<td width="35"><div align="center">12<input type="radio" value="12" id="sex" name="sex" checked></div></td> 
			<td width="35"><div align="center">13<input type="radio" value="13" id="sex" name="sex" ></div></td> 
			<td width="35"><div align="center">14<input type="radio" value="14" id="sex" name="sex" ></div></td> 
			<td width="35"><div align="center">15<input type="radio" value="15" id="sex" name="sex" ></div></td> 
			<td width="35"><div align="center">16<input type="radio" value="16" id="sex" name="sex" ></div></td> 
			<td width="35"> <div align="center">-</div></td> 
			<td><div align="left">17
		      <input type="radio" value="17" id="sex" name="sex" >
			</div></td>
		</tr>
        <tr>
          <td nowrap><div align="right"><strong> </strong></div></td>
          <td bgcolor="#CCCCCC" colspan="4">
            <div align="left">
              <input type="submit" name="search" value="PreView">
            </div></td>
          <td bgcolor="#CCCCCC" colspan="4"><div align="right">
            <input name="PrintPDF" type="submit" id="PrintPDF" value="Print PDF">
          </div></td>
        </tr>
  </table>
</form>
<?php
}
include('../footer/footer.php');
?>