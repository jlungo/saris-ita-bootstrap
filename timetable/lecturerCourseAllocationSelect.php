<?php 
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('lecturerMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Administration';
	$szSubSection = 'Course Allocation';
	$szTitle = 'Lecturer Course Allocation';
	include('lecturerheader.php');
?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
?>

<fieldset>
<legend>Select the Appropriate Activity to Perform</legend>
<table align="center" cellspacing='5' >
<tr>
<td >
<form action="lecturerCourseAllocation.php" method="POST" name='academic'>
<input name="result" type="submit" value="View Allocated Courses"  onmouseover="this.style.background='#DEFEDE'"
onmouseout="this.style.background='lightblue'" style='background-color:lightblue;color:black;font-size:9pt;font-weight:bold' title="Click to View All Allocated Courses"> 
</form>
</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td> 
<form action="lecturerCourseAllocate.php" method="POST" name='academic'>
<input name="coursework" type="submit" value="Allocate Course"
 onmouseover="this.style.background='#DEFEDE'"
onmouseout="this.style.background='lightblue'" style='background-color:lightblue;color:black;font-size:9pt;font-weight:bold' title="Click to Allocate Course to a Lecturer">

</td>
</tr>
</table>
</fieldset>

<?php

echo "<br>";
include('../footer/footer.php');
?>
