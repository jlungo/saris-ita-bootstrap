<?php
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	# include menu
	include('lecturerMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Policy Setup';
	$szTitle = 'Programme Information';
	$szSubSection = 'Programme';
	include("lecturerheader.php");

$currentPage = $_SERVER["PHP_SELF"];
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmInstEdit")) {
	$rawcode = $_POST['txtCode'];
	$programme = ereg_replace("[[:space:]]+", " ",$rawcode);
	$rawcount = $_POST['txtCount'];
	$code = ereg_replace("[[:space:]]+", " ",$rawcount);
	$rawyearofstudy = $_POST['YearofStudy'];
	$yearofstudy = ereg_replace("[[:space:]]+", " ",$rawyearofstudy);
	$rawsemester = $_POST['semester'];
	$semester = ereg_replace("[[:space:]]+", " ",$rawsemester);

#check if coursecode exist
$coursecodeFound = 1;
if ($coursecodeFound) {
	#insert records	 
	$insSQL = "INSERT INTO coursecountprogramme (ProgrammeID, YearofStudy, Semester, CourseCount) VALUES ('$programme', '$yearofstudy', '$semester', '$code')";  				   
	  $Result1 = mysql_query($insSQL);
	}

 }
 
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
//control the display table
@$new=2;

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$maxRows_inst = 10000;
$pageNum_inst = 0;
if (isset($_GET['pageNum_inst'])) {
  $pageNum_inst = $_GET['pageNum_inst'];
}
$startRow_inst = $pageNum_inst * $maxRows_inst;

mysql_select_db($database_zalongwa, $zalongwa);
if (isset($_GET['edit'])) {
  $rawkey=$_GET['edit'];
  $key = addslashes($rawkey);
  $query_inst = "SELECT * FROM coursecountprogramme WHERE ProgrammeID='$key' ORDER BY ProgrammeID ASC";
}else{
$query_inst = "SELECT * FROM coursecountprogramme ORDER BY ProgrammeID ASC";
}
#get combination name
$qcomb = "select ProgrammeName from programme where ProgrammeCode='$key'";
$dbcomb = mysql_query($qcomb);
$row_comb =mysql_fetch_assoc($dbcomb);
$comb = $row_comb['ProgrammeName'];

$query_limit_inst = sprintf("%s LIMIT %d, %d", $query_inst, $startRow_inst, $maxRows_inst);
$inst = mysql_query($query_limit_inst, $zalongwa) or die(mysql_error());
$row_inst = mysql_fetch_assoc($inst);

if (isset($_GET['totalRows_inst'])) {
  $totalRows_inst = $_GET['totalRows_inst'];
} else {
  $all_inst = mysql_query($query_inst);
  $totalRows_inst = mysql_num_rows($all_inst);
}
$totalPages_inst = ceil($totalRows_inst/$maxRows_inst)-1;
?>
<style type="text/css">
<!--
.style1 {color: #FFFFFF}
.style2 {color: #000000}
-->
</style>


<?php @$new=$_GET['new'];
echo "</a>";
if (@$new<>1){
?>
<form name="form1" method="get" action="<?php echo $editFormAction; ?>">
              Search by CourseCode: 
                <input name="course" type="text" id="course" maxlength="50">
              <input type="submit" name="Submit" value="Search">
</form>

<?php
if (isset($_GET['edit'])){
#get post variables
$rawkey = addslashes($_GET['edit']);
$key = ereg_replace("[[:space:]]+", " ",$rawkey);

mysql_select_db($database_zalongwa, $zalongwa);
$query_instEdit = "SELECT * FROM coursecountprogramme WHERE ProgrammeID ='$key'";
$instEdit = mysql_query($query_instEdit, $zalongwa) or die(mysql_error());
$row_instEdit = mysql_fetch_assoc($instEdit);
$totalRows_instEdit = mysql_num_rows($instEdit);

$queryString_inst = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_inst") == false && 
        stristr($param, "totalRows_inst") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_inst = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_inst = sprintf("&totalRows_inst=%d%s", $totalRows_inst, $queryString_inst);

?>
<form action="<?php echo $editFormAction; ?>" method="POST" name="frmInstEdit" id="frmInstEdit">
  <table width="200" border="1" cellpadding="0" cellspacing="0" bordercolor="#006600">
    
    <tr bgcolor="#CCCCCC">
      <th nowrap scope="row"><div align="right">Study Programme:</div></th>
      <td><input name="txtCode" type="hidden" id="txtCode" value="<?php echo $key?>" size="20" />
          <?php echo $comb?></td>
      <td>YearofStudy</td>
      <td>Semester</td>
    </tr>
    <tr bgcolor="#CCCCCC">
      <th nowrap scope="row"><div align="right">Total Credits Count:</div></th>
      <td><input name="txtCount" type="text" id="txtCount" value="" size="6" /></td>
       <td><select name="YearofStudy" id="YearofStudy">
        <option value="1">First Year</option>
        <option value="2">Second Year</option>
        <option value="3">Third Year</option>
        <option value="4">Fourth Year</option>
        <option value="5">Fifth Year</option>
      </select>      </td>
      <td><select name="semester" id="semester">
        <option value="1">Semester I </option>
        <option value="2">Semester II</option>
      </select> </td>
    </tr>
    
    <tr bgcolor="#CCCCCC">
      <th scope="row"><input name="id" type="hidden" id="id" value="<?php echo $key ?>" /></th>
      <td colspan="3"><div align="center">
          <input type="submit" name="Submit2" value="Add Record" />
      </div></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="frmInstEdit">
</form>
<?php
}
?>
<table border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td><strong>S/No</strong></td>
	<td><strong>Programme</strong></td>
	<td><strong>YearofStudy</strong></td>
	<td><strong>Semester</strong></td>
	<td><strong>CourseCount</strong></td>
	<td><strong>Delete</strong></td>
  </tr>
  <?php 
  $sn =1;
  do { ?>
  <tr>
     <td nowrap><?php echo $sn?></td>
	 <td><?php echo $comb?></td>
	  <td nowrap><?php $yrs=$row_inst['YearofStudy']; echo $row_inst['YearofStudy']?></td>
     <td nowrap><?php $sem=$row_inst['Semester']; echo $row_inst['Semester']?></td>
 	 <td nowrap><?php $count=$row_inst['CourseCount']; echo $row_inst['CourseCount']?></td>
      <td nowrap><?php echo "<a href=\"lecturerProgrammecoursecountdelete.php?prog=$key&sem=$sem&year=$yrs\">Delete</a>"?></td>
    </tr>
  <?php 
  $sn=$sn+1;
  
  } while ($row_inst = mysql_fetch_assoc($inst)); ?>
</table>
<a href="<?php printf("%s?pageNum_inst=%d%s", $currentPage, max(0, $pageNum_inst - 1), $queryString_inst); ?>">Previous</a><span class="style1">......<span class="style2"><?php echo min($startRow_inst + $maxRows_inst, $totalRows_inst) ?>/<?php echo $totalRows_inst ?> </span>..........</span><a href="<?php printf("%s?pageNum_inst=%d%s", $currentPage, min($totalPages_inst, $pageNum_inst + 1), $queryString_inst); ?>">Next</a><br>
<?php }

	# include the footer
	include("../footer/footer.php");

@mysql_free_result($inst);

@mysql_free_result($instEdit);

@mysql_free_result($faculty);

@mysql_free_result($campus);