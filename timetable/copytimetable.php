
<?php 
/**
 * @file copytimetable.php
 * created on Sun,March 10 2013
 * @ author lackson david
 * */

require_once('../Connections/zalongwa.php'); 
require_once('../Connections/sessioncontrol.php');

# include the header
include('lecturerMenu.php');
	global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
	$szSection = 'Timetable';
	$szTitle = 'Copy timetable';
	$szSubSection = 'Copy Timetable';
	include("lecturerheader.php");
?>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/drag.js"></script>
<script type="text/javascript" src="js/val.js"></script>
<style>
select{
  width:200px;
  padding:3px;
}

#table_timetable tr td{
padding:5px 0px 0px 0px;
}
</style>

<?php
//find lowest academic year in timetable
$sql_lowyr= "SELECT DISTINCT AYear FROM timetable ORDER BY AYear ASC LIMIT 1";
$result_lowyr=mysql_query($sql_lowyr) ;
$row_lowyr = mysql_fetch_array($result_lowyr);
$lowyr = $row_lowyr['AYear'];

//select all academic year from timetable
$sql_ayear1= "SELECT DISTINCT AYear FROM timetable ORDER BY AYear DESC";
$result_ayear1=mysql_query($sql_ayear1) ;

// select all timetable type/category
$sql_timetablecategory1= "SELECT * FROM timetableCategory";
$result_timetablecategory1=mysql_query($sql_timetablecategory1);


//select all academic year
$sql_ayear2= "SELECT * FROM academicyear WHERE AYear > '$lowyr' ORDER BY AYear DESC";
$result_ayear2=mysql_query($sql_ayear2);

// select all timetable type/category
$sql_timetablecategory2= "SELECT * FROM timetableCategory";
$result_timetablecategory2=mysql_query($sql_timetablecategory2);




if(isset($_POST['copy'])){
	$ayearfrom=$_POST['ayearfrom'];
	$ayearto=$_POST['ayearto'];
	$programme = $_POST['programme'];
	$typefrom =$_POST['tcategoryfrom'];
	$typeto =$_POST['tcategoryto'];
 
	echo '<meta http-equiv = "refresh" content ="0; url = copytimetable.php?copy=1&ayearfrom='.$ayearfrom.'&ayearto='.$ayearto.'&cat1='.$typefrom.'&cat2='.$typeto.'">';
	exit;
}
if(isset($_GET['success'])){
	echo '<div style="margin:5px"><img src="images/yes.png"  height="18" width="18"/><font color="#0000FF" size="+1">&nbsp;&nbsp;' . urldecode($_GET['success']). '</font>'.urldecode($_GET['det']).'</div>';
	}

if(isset($_GET['error'])){
	echo '<div style="margin:5px"><img src="images/xx.png"  height="15" width="15"/><font color="#FF0000" size="+1">&nbsp;&nbsp;' . urldecode($_GET['error']). '</font><br/></div><div align="center">'.urldecode($_GET['det']).'</div>';
	}
	
if(isset($_SESSION['clear']) && ($_SESSION['cleartxt']!='')){
	$cleartxt=$_SESSION['cleartxt'];
	echo '<br/><div align="center">'.$cleartxt.'</div>';
	$_SESSION['cleartxt']='';
	}	
if(!isset($_GET['copy'])){
?>
<form action="<?php echo $_SERVER['PHP_SELF']?>" method="post" name="frmCopy" onsubmit="return onsubConfirm('Click Ok to start copying or Cancel to stop copying');">
<table class="resView">
	<tr><td class="resViewhd" colspan="3">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	From
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 To</td></tr>
	<tr><td colspan="3"></td></tr>
<tr>
<td class="resViewhd">Academic Year:</td>
<td class="resViewtd">
<select name="ayearfrom">
<?php 
echo '<option value="">Select Year</option>';
while($row = mysql_fetch_array($result_ayear1)){
	echo '<option value="'.$row['AYear'].'">'.$row['AYear'].'</option>';
}
?>
</select>
</td>

<td class="resViewtd">
<select name="ayearto">
<?php 
echo '<option value="">Select Year</option>';
while($row = mysql_fetch_array($result_ayear2)){
	echo '<option value="'.$row['AYear'].'">'.$row['AYear'].'</option>';
}
?>
</select>
</td>

</tr>
<tr><td class="resViewhd">Time table Category:</td>
<td class="resViewtd">
<select name="tcategoryfrom">
<?php 
echo '<option value="">Select Category</option>';
while($row = mysql_fetch_array($result_timetablecategory1)){
	echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
}
?>
</select>
</td>

<td class="resViewtd">
<select name="tcategoryto">
<?php 
echo '<option value="">Select Category</option>';
while($row = mysql_fetch_array($result_timetablecategory2)){
	echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
}
?>
</select>
</td>

</tr>

<tr>
<td class="resViewhd" colspan="3" align="center"><input type="submit" name="copy" value="COPY"/></td>
</tr>
</table>
</form>

<?php } else {
	 $ayearfrom = trim($_GET['ayearfrom']);
	 $ayearto= trim($_GET['ayearto']);
	 $cat1 = trim($_GET['cat1']);
	 $cat2= trim($_GET['cat2']);
	include 'copy_timetable_func.php';
	copyTimetable($ayearfrom,$ayearto,$cat1,$cat2);
}

if(isset($_GET['clear'])){
	$ayear= trim($_GET['ayear']);
	$cat= trim($_GET['cat']);
	include 'clear_timetable_func.php';
	clearTimetable($ayear,$cat);
	
	}
	?>



