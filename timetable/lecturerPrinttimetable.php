<?php
#start pdf
if (isset($_POST['PDF']) && ($_POST['PDF'] == "Print PDF")){
	#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
	require_once('../Connections/zalongwa.php');

	#Get Organisation Name
	$qorg = "SELECT * FROM organisation";
	$dborg = mysql_query($qorg);
	$row_org = mysql_fetch_assoc($dborg);
	$org = $row_org['Name'];

	@$paper = addslashes($_POST['paper']);
	@$layout = addslashes($_POST['layout']);
	@$checkdegree = "on";
	@$checksem = "on";
	@$checkyear = "on";
	if ($paper=='a3')
	{
		$xpoint = 1050.00;
		$ypoint = 800.89;
	}else
	{
		$xpoint = 800.89;
		$ypoint = 580.28;
	}

	$prog=$_POST['degree'];
	$cohotyear = $_POST['cohot'];
	$ayear = $_POST['ayear'];
	$sem=$_POST['sem'];
	$qprog= "SELECT ProgrammeCode, Title FROM programme WHERE ProgrammeCode='$prog'";
	$dbprog = mysql_query($qprog);
	$row_prog = mysql_fetch_array($dbprog);
	$progname = $row_prog['Title'];
		
	//calculate year of study
	$entry = intval(substr($cohotyear,0,4));
	$current = intval(substr($ayear,0,4));
	$yearofstudy=$current-$entry;
	
	 if($sem=="Semester I" || $sem == "Semester II")
   $class="1ST YEAR";
else if($sem=="Semester III" || $sem == "Semester IV")
   $class="2ND YEAR";
else if($sem=="Semester V" || $sem == "Semester VI")
   $class=" 3RD YEAR";
else if($sem=="Semester VII" || $sem == "Semester VIII")
   $class="4TH YEAR";
else if($sem=="Semester IX" || $sem == "Semester X")
   $class="  5TH YEAR";
else 
   $class=" ";
if($sem=="Semester I"||$sem=="Semester III"||$sem=="Semester V"||$sem=="Semester VII"||$sem=="Semester IX")
$ysem="Semester I";
elseif($sem=="Semester II"||$sem=="Semester IV"||$sem=="Semester VI"||$sem=="Semester VIII"||$sem=="Semester X")
$ysem="Semester II";
else
$ysem=" ";

	
	if (($checkdegree=='on') && ($checkyear == 'on') && ($checksem == 'on')){
		$deg=addslashes($_POST['degree']);
		$degree=addslashes($_POST['degree']);
		$year = addslashes($_POST['ayear']);
		$cohot = addslashes($_POST['cohot']);
		$dept = addslashes($_POST['dept']);
		$sem = addslashes($_POST['sem']);
		
		#determine total number of columns
		$qstd = "SELECT PCode FROM examregisterlecturer ;";
		$dbstd = mysql_query($qstd);
		$totalcolms = mysql_num_rows($dbstd);
		while($rowstd = mysql_fetch_array($dbstd)) {
			$pcode = $rowstd['PCode'];
			/*$qstdcourse = "SELECT DISTINCT coursecode FROM examresult WHERE RegNo='$stdregno' and AYear='$year'";
			$dbstdcourse = mysql_query($qstdcourse);
			$totalstdcourse = mysql_num_rows($dbstdcourse);
			if ($totalstdcourse>$totalcolms){
				$totalcolms = $totalstdcourse;
			}
*/		}
		#start pdf
		include('includes/PDF.php');
		$pdf = &PDF::factory($layout, $paper);      
		$pdf->open();                         
		$pdf->setCompression(true);           
		$pdf->addPage();  
		$pdf->setFont('Arial', 'I', 8);     
		$pdf->text(50, $ypoint, 'Printed On '.$today = date("d-m-Y H:i:s"));  		
 
		#put page header
		$x=60;
		$y=54;
		$i=1;
		$pg=1;
		$pdf->text($xpoint,$ypoint, 'Page '.$pg);   
		
		#count unregistered
		$j=0;
		#count sex
		$fmcount = 0;
		$mcount = 0;
		$fcount = 0;
		
		#print header for landscape paper layout 
		$playout ='l'; 
		include '../includes/orgname.php';
		
		$pdf->setFillColor('rgb', 0, 0, 0);    
		$pdf->setFont('Arial', '', 13);      
		$pdf->text($x+195, $y+14, 'TIMETABLE SHEET'); 
		$pdf->text($x+205, $y+28, 'ACADEMIC YEAR: '.$year.'  '.strtoupper($ysem)); 
		$pdf->text($x-100, $y+48, $class.' - '.strtoupper($prog)); 
		#reset values of x,y
		$x=50; $y=$y+54;
		
		#set table header
		$pdf->setFont('Arial', '', 10); 
		$pdf->line($x, $y, $xpoint+1, $y); 
		$pdf->line($x, $y+15, $xpoint+1, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'DAY/START');
		//$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, '8:00-9:00');
		$pdf->line($x+70, $y, $x+70, $y+15);	$pdf->text($x+80, $y+12, '8:00');
		$pdf->line($x+120, $y, $x+120, $y+15);	$pdf->text($x+130, $y+12, '9:00');
		$pdf->line($x+175, $y, $x+175, $y+15);	$pdf->text($x+180, $y+12, '10:00');
		$pdf->line($x+230, $y, $x+230, $y+15);	$pdf->text($x+240, $y+12, '11:00');
		$pdf->line($x+287, $y, $x+287, $y+15);	$pdf->text($x+295, $y+12, '12:00');
		$pdf->line($x+345, $y, $x+345, $y+15);	$pdf->text($x+355, $y+12, '13:00');
		$pdf->line($x+405, $y, $x+405, $y+15);	$pdf->text($x+410, $y+12, '14:00');
		$pdf->line($x+465, $y, $x+465, $y+15);	$pdf->text($x+475, $y+12, '15:00');
		$pdf->line($x+525, $y, $x+525, $y+15);	$pdf->text($x+535, $y+12, '16:00');
		$pdf->line($x+585, $y, $x+585, $y+15);	$pdf->text($x+595, $y+12, '17:00');
		$pdf->line($x+644, $y, $x+644, $y+15);	$pdf->text($x+655, $y+12, '18:00');
		$pdf->line($x+698, $y, $x+698, $y+15);	$pdf->text($x+705, $y+12, '19:00');
		$pdf->line($xpoint+1, $y+15, $xpoint+1, $y+$rspan); 
		$y=$y+15;
		

for ($i=1; $i<=5; $i++){
 switch ($i){
case 1: {$day="Monday"; break; }
case 2: {$day="Tuesday"; break; }
case 3: {$day="Wednesday"; break; }
case 4: {$day="Thursday"; break; }
case 5: {$day="Friday"; break; }
//default: "";
} 

$rows=0;

	$query_day8 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='8' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz8 = mysql_query($query_day8, $zalongwa) or die(mysql_error());
$rspan8 = mysql_num_rows($daycoz8); //Rows Ngapi?

	while($row_course8 = mysql_fetch_array($daycoz8)){
	$course8 = $row_course8['CourseCode'];
	$endcourse8 = $row_course8['EndTime'];
	$room8 = $row_course8['Room'];
$span8=$endcourse8-8;//Box Ngapi?
}
if($rspan8>$rows)
$rows=$rspan8;				

	$query_day9 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='9' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz9 = mysql_query($query_day9, $zalongwa) or die(mysql_error());
$rspan9 = mysql_num_rows($daycoz9); //Rows Ngapi?
	while($row_course9 = mysql_fetch_array($daycoz9)){
	$course9= $row_course9['CourseCode'];
	$endcourse9= $row_course9['EndTime'];
	$room9= $row_course9['Room'];
$span9=$endcourse9-9;//Box Ngapi?
}
if($rspan9>$rows)
$rows=$rspan9;
						
	$query_day10 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='10' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz10 = mysql_query($query_day10, $zalongwa) or die(mysql_error());
$rspan10 = mysql_num_rows($daycoz10); //Rows Ngapi?
	while($row_course10 = mysql_fetch_array($daycoz10)){
	$course10= $row_course10['CourseCode'];
	$endcourse10= $row_course10['EndTime'];
	$room10= $row_course10['Room'];
$span10=$endcourse10-10;//Box Ngapi?
}
if($rspan10>$rows)
$rows=$rspan10;
						
	$query_day11 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='11' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz11 = mysql_query($query_day11, $zalongwa) or die(mysql_error());
$rspan11 = mysql_num_rows($daycoz11); //Rows Ngapi?
	while($row_course11 = mysql_fetch_array($daycoz11)){
	$course11= $row_course11['CourseCode'];
	$endcourse11= $row_course11['EndTime'];
	$room11= $row_course11['Room'];
$span11=$endcourse11-11;//Box Ngapi?
}
if($rspan11>$rows)
$rows=$rspan11;
						
	$query_day12 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='12' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz12 = mysql_query($query_day12, $zalongwa) or die(mysql_error());
$rspan12 = mysql_num_rows($daycoz12); //Rows Ngapi?
	while($row_course12 = mysql_fetch_array($daycoz12)){
	$course12= $row_course12['CourseCode'];
	$endcourse12= $row_course12['EndTime'];
	$room12= $row_course12['Room'];
$span12=$endcourse12-12;//Box Ngapi?
}

if($rspan12>$rows)
$rows=$rspan12;
						
	$query_day13 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='13' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz13 = mysql_query($query_day13, $zalongwa) or die(mysql_error());
$rspan13 = mysql_num_rows($daycoz13); //Rows Ngapi?
	while($row_course13 = mysql_fetch_array($daycoz13)){
	$course13= $row_course13['CourseCode'];
	$endcourse13= $row_course13['EndTime'];
	$room13= $row_course13['Room'];
$span13=$endcourse13-13;//Box Ngapi?
}
if($rspan13>$rows)
$rows=$rspan13;
						
	$query_day14 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='14' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz14 = mysql_query($query_day14, $zalongwa) or die(mysql_error());
$rspan14 = mysql_num_rows($daycoz14); //Rows Ngapi?
	while($row_course14 = mysql_fetch_array($daycoz14)){
	$course14= $row_course14['CourseCode'];
	$endcourse14= $row_course14['EndTime'];
	$room14= $row_course14['Room'];
$span14=$endcourse14-14;//Box Ngapi?
}
if($rspan14>$rows)
$rows=$rspan14;
						
	$query_day15 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='15' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz15 = mysql_query($query_day15, $zalongwa) or die(mysql_error());
$rspan15 = mysql_num_rows($daycoz15); //Rows Ngapi?
	while($row_course15 = mysql_fetch_array($daycoz15)){
	$course15= $row_course15['CourseCode'];
	$endcourse15= $row_course15['EndTime'];
	$room15= $row_course15['Room'];
$span15=$endcourse15-15;//Box Ngapi?
}
if($rspan15>$rows)
$rows=$rspan15;
						
	$query_day16 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='16' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz16 = mysql_query($query_day16, $zalongwa) or die(mysql_error());
$rspan16 = mysql_num_rows($daycoz16); //Rows Ngapi?
	while($row_course16 = mysql_fetch_array($daycoz16)){
	$course16= $row_course16['CourseCode'];
	$endcourse16= $row_course16['EndTime'];
	$room16= $row_course16['Room'];
$span16=$endcourse16-16;//Box Ngapi?
}
if($rspan16>$rows)
$rows=$rspan16;
						
	$query_day17 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='17' AND Semester='$sem' AND Faculty='$degree' ";
	$daycoz17 = mysql_query($query_day17, $zalongwa) or die(mysql_error());
$rspan17 = mysql_num_rows($daycoz17);//Rows Ngapi?
	while($row_course17 = mysql_fetch_array($daycoz17)){
	$course17= $row_course17['CourseCode'];
	$endcourse17= $row_course17['EndTime'];
	$room17= $row_course17['Room'];
$span17=$endcourse17-17;//Box Ngapi?
}
if($rspan17>$rows)
$rows=$rspan17;
						
	$query_day18 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='18' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz18 = mysql_query($query_day18, $zalongwa) or die(mysql_error());
$rspan18 = mysql_num_rows($daycoz18); //Rows Ngapi?
	while($row_course18 = mysql_fetch_array($daycoz18)){
	$course18= $row_course18['CourseCode'];
	$endcourse18= $row_course18['EndTime'];
	$room18= $row_course18['Room'];
$span18=$endcourse18-18;//Box Ngapi?
}
if($rspan18>$rows)
$rows=$rspan18;
						
	$query_day19 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='19' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz19 = mysql_query($query_day19, $zalongwa) or die(mysql_error());
$rspan19 = mysql_num_rows($daycoz19); //Rows Ngapi?
	while($row_course19 = mysql_fetch_array($daycoz19)){
	$course19= $row_course19['CourseCode'];
	$endcourse19= $row_course19['EndTime'];
	$room19= $row_course19['Room'];
$span19=$endcourse19-19;//Box Ngapi?
}
if($rspan19>$rows)
$rows=$rspan19;

$nxt=$rows*40;

if($nxt != 0){
#print courses
$x=50;
$y=$y+$nxt;

$ysave=$y;

$y=$y-$nxt;
$pdf->setFont('Arial', '', 10); 
$pdf->line($x, $y, $xpoint+1, $y); 
$pdf->line($x, $y+40, $xpoint+1, $y+40);
$pdf->line($x, $y, $x, $y+40);	$pdf->text($x+5, $y+25, strtoupper($day));
//$y=$y-40;

$query_day8 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='8' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz8 = mysql_query($query_day8, $zalongwa) or die(mysql_error());
//$rspan81 = mysql_query("SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='8'");
$rspan8 = mysql_num_rows($daycoz8); $count=0;
	while($row_course8 = mysql_fetch_array($daycoz8)){
	$course8 = $row_course8['CourseCode'];
	$endcourse8 = $row_course8['EndTime'];
	$room8 = $row_course8['Room'];
$span8=$endcourse8-8;//Box Ngapi?


$count=$count+1;
$pdf->line($x, $y, $x, $y+40);
$pdf->line($x, $y, $xpoint+1, $y); 
$pdf->line($x, $y+40, $xpoint+1, $y+40);

$pdf->line($x, $y, $x, $y+40);	$pdf->text($x+5, $y+25, strtoupper(''));
if($span8>1)
{$t="HRS";}else {$t="HR";}

$pdf->line($x+70, $y, $x+70, $y+40);	$pdf->text($x+71, $y+12, $course8);$course8="";
$pdf->text($x+73, $y+25, $room8); $room8=""; $pdf->text($x+73, $y+35, $span8.$t); $span8="";
$pdf->line($x+120, $y, $x+120, $y+40);	$pdf->text($x+125, $y+12, ' ');
		$pdf->line($x+175, $y, $x+175, $y+40);	$pdf->text($x+176, $y+12, ' ');
		$pdf->line($x+230, $y, $x+230, $y+40);	$pdf->text($x+232, $y+12, ' ');
		$pdf->line($x+287, $y, $x+287, $y+40);	$pdf->text($x+288, $y+12, ' ');
		$pdf->line($x+345, $y, $x+345, $y+40);	$pdf->text($x+348, $y+12, ' ');
		$pdf->line($x+405, $y, $x+405, $y+40);	$pdf->text($x+408, $y+12, ' ');
		$pdf->line($x+465, $y, $x+465, $y+40);	$pdf->text($x+468, $y+12, ' ');
		$pdf->line($x+525, $y, $x+525, $y+40);	$pdf->text($x+528, $y+12, ' ');
		$pdf->line($x+585, $y, $x+585, $y+40);	$pdf->text($x+588, $y+12, ' ');
		$pdf->line($x+644, $y, $x+644, $y+40);	$pdf->text($x+644, $y+12, ' ');
		$pdf->line($x+698, $y, $x+698, $y+40);	$pdf->text($x+698, $y+12, ' ');

$pdf->line($xpoint+1, $y, $xpoint+1, $y+40);//right border line

$y=$y+40;

#Go to next page
					
					  if($y>=530.28){
$ynew=$y;$nxt=$count*40;
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 7);     
						$pdf->text(50, 580.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text(800.89,580.28, 'Page '.$pg); 
#set table header for new page
		$pdf->setFont('Arial', '', 10); 
		$pdf->line($x, $y, $xpoint+1, $y); 
		$pdf->line($x, $y+15, $xpoint+1, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'DAY/START');
		//$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, '8:00-9:00');
		$pdf->line($x+70, $y, $x+70, $y+15);	$pdf->text($x+80, $y+12, '8:00');
		$pdf->line($x+120, $y, $x+120, $y+15);	$pdf->text($x+130, $y+12, '9:00');
		$pdf->line($x+175, $y, $x+175, $y+15);	$pdf->text($x+180, $y+12, '10:00');
		$pdf->line($x+230, $y, $x+230, $y+15);	$pdf->text($x+240, $y+12, '11:00');
		$pdf->line($x+287, $y, $x+287, $y+15);	$pdf->text($x+295, $y+12, '12:00');
		$pdf->line($x+345, $y, $x+345, $y+15);	$pdf->text($x+355, $y+12, '13:00');
		$pdf->line($x+405, $y, $x+405, $y+15);	$pdf->text($x+410, $y+12, '14:00');
		$pdf->line($x+465, $y, $x+465, $y+15);	$pdf->text($x+475, $y+12, '15:00');
		$pdf->line($x+525, $y, $x+525, $y+15);	$pdf->text($x+535, $y+12, '16:00');
		$pdf->line($x+585, $y, $x+585, $y+15);	$pdf->text($x+595, $y+12, '17:00');
		$pdf->line($x+644, $y, $x+644, $y+15);	$pdf->text($x+655, $y+12, '18:00');
		$pdf->line($x+698, $y, $x+698, $y+15);	$pdf->text($x+705, $y+12, '19:00');
		$pdf->line($xpoint+1, $y, $xpoint+1, $y+15);  
		$y=$y+15;

}	
}// end 8

if($ynew>=530.28){
$y=$y;
$ysave=$y;
$ynew=$y;
}else{
$y=$ysave;
}
		

	$query_day9 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='9' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz9 = mysql_query($query_day9, $zalongwa) or die(mysql_error());
//$rspan91 = mysql_query("SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='9'");
$rspan9 = mysql_num_rows($daycoz9); $count=0; $y=$y-$nxt;
	while($row_course9 = mysql_fetch_array($daycoz9)){
	$course9= $row_course9['CourseCode'];
	$endcourse9= $row_course9['EndTime'];
	$room9= $row_course9['Room'];
$span9=$endcourse9-9;//Box Ngapi?

$count=$count+1;
$pdf->line($x, $y, $xpoint+1, $y); 
$pdf->line($x, $y+40, $xpoint+1, $y+40);

$pdf->line($x, $y, $x, $y+40);	$pdf->text($x+5, $y+25, strtoupper(''));
if($span9>1)
{$t="HRS";}else {$t="HR";}

$pdf->line($x+70, $y, $x+70, $y+40);	$pdf->text($x+75, $y+12, ' ');
$pdf->line($x+120, $y, $x+120, $y+40);	$pdf->text($x+123, $y+12, $course9); $course9="";
$pdf->text($x+124, $y+25, $room9); $room9="";$pdf->text($x+124, $y+35, $span9.$t); $span9="";
$pdf->line($x+175, $y, $x+175, $y+40);	$pdf->text($x+176, $y+12, ' ');
		$pdf->line($x+230, $y, $x+230, $y+40);	$pdf->text($x+232, $y+12, ' ');
		$pdf->line($x+287, $y, $x+287, $y+40);	$pdf->text($x+288, $y+12, ' ');
		$pdf->line($x+345, $y, $x+345, $y+40);	$pdf->text($x+348, $y+12, ' ');
		$pdf->line($x+405, $y, $x+405, $y+40);	$pdf->text($x+408, $y+12, ' ');
		$pdf->line($x+465, $y, $x+465, $y+40);	$pdf->text($x+468, $y+12, ' ');
		$pdf->line($x+525, $y, $x+525, $y+40);	$pdf->text($x+528, $y+12, ' ');
		$pdf->line($x+585, $y, $x+585, $y+40);	$pdf->text($x+588, $y+12, ' ');
		$pdf->line($x+644, $y, $x+644, $y+40);	$pdf->text($x+644, $y+12, ' ');
		$pdf->line($x+698, $y, $x+698, $y+40);	$pdf->text($x+698, $y+12, ' ');
$pdf->line($xpoint+1, $y, $xpoint+1, $y+40);//right border line

$y=$y+40;

#Go to next page
					
					  if($y>=530.28){
$ynew=$y;$nxt=$count*40;
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 7);     
						$pdf->text(50, 580.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text(800.89,580.28, 'Page '.$pg); 
#set table header for new page
		$pdf->setFont('Arial', '', 10); 
		$pdf->line($x, $y, $xpoint+1, $y); 
		$pdf->line($x, $y+15, $xpoint+1, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'DAY/START');
		//$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, '8:00-9:00');
		$pdf->line($x+70, $y, $x+70, $y+15);	$pdf->text($x+80, $y+12, '8:00');
		$pdf->line($x+120, $y, $x+120, $y+15);	$pdf->text($x+130, $y+12, '9:00');
		$pdf->line($x+175, $y, $x+175, $y+15);	$pdf->text($x+180, $y+12, '10:00');
		$pdf->line($x+230, $y, $x+230, $y+15);	$pdf->text($x+240, $y+12, '11:00');
		$pdf->line($x+287, $y, $x+287, $y+15);	$pdf->text($x+295, $y+12, '12:00');
		$pdf->line($x+345, $y, $x+345, $y+15);	$pdf->text($x+355, $y+12, '13:00');
		$pdf->line($x+405, $y, $x+405, $y+15);	$pdf->text($x+410, $y+12, '14:00');
		$pdf->line($x+465, $y, $x+465, $y+15);	$pdf->text($x+475, $y+12, '15:00');
		$pdf->line($x+525, $y, $x+525, $y+15);	$pdf->text($x+535, $y+12, '16:00');
		$pdf->line($x+585, $y, $x+585, $y+15);	$pdf->text($x+595, $y+12, '17:00');
		$pdf->line($x+644, $y, $x+644, $y+15);	$pdf->text($x+655, $y+12, '18:00');
		$pdf->line($x+698, $y, $x+698, $y+15);	$pdf->text($x+705, $y+12, '19:00');
		$pdf->line($xpoint+1, $y, $xpoint+1, $y+15);  
		$y=$y+15;

}	
}//end 9

if($ynew>=530.28){
$y=$y;
$ysave=$y;
$ynew=$y;
}else{
$y=$ysave;
}
					
	$query_day10 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='10' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz10 = mysql_query($query_day10, $zalongwa) or die(mysql_error());
//$rspan101 = mysql_query("SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='10'");
$rspan10 = mysql_num_rows($daycoz10);$count=0;$y=$y-$nxt;
	while($row_course10 = mysql_fetch_array($daycoz10)){
	$course10= $row_course10['CourseCode'];
	$endcourse10= $row_course10['EndTime'];
	$room10= $row_course10['Venue'];
$span10=$endcourse10-10;//Box Ngapi?


$count=$count+1;
$pdf->line($x, $y, $xpoint+1, $y); 
$pdf->line($x, $y+40, $xpoint+1, $y+40);

$pdf->line($x, $y, $x, $y+40);	$pdf->text($x+5, $y+25, strtoupper(''));
if($span10>1)
{$t="HRS";}else {$t="HR";}

$pdf->line($x+70, $y, $x+70, $y+40);	$pdf->text($x+75, $y+12, ' ');
		$pdf->line($x+120, $y, $x+120, $y+40);	$pdf->text($x+125, $y+12, ' ');
$pdf->line($x+175, $y, $x+175, $y+40);	$pdf->text($x+176, $y+12, $course10); $course10="";
$pdf->text($x+177, $y+25, $room10); $room10="";$pdf->text($x+177, $y+35, $span10.$t); $span10="";
$pdf->line($x+230, $y, $x+230, $y+40);	$pdf->text($x+232, $y+12, ' ');
		$pdf->line($x+287, $y, $x+287, $y+40);	$pdf->text($x+288, $y+12, ' ');
		$pdf->line($x+345, $y, $x+345, $y+40);	$pdf->text($x+348, $y+12, ' ');
		$pdf->line($x+405, $y, $x+405, $y+40);	$pdf->text($x+408, $y+12, ' ');
		$pdf->line($x+465, $y, $x+465, $y+40);	$pdf->text($x+468, $y+12, ' ');
		$pdf->line($x+525, $y, $x+525, $y+40);	$pdf->text($x+528, $y+12, ' ');
		$pdf->line($x+585, $y, $x+585, $y+40);	$pdf->text($x+588, $y+12, ' ');
		$pdf->line($x+644, $y, $x+644, $y+40);	$pdf->text($x+644, $y+12, ' ');
		$pdf->line($x+698, $y, $x+698, $y+40);	$pdf->text($x+698, $y+12, ' ');
$pdf->line($xpoint+1, $y, $xpoint+1, $y+40);//right border line

$y=$y+40;

#Go to next page
					
					  if($y>=530.28){
$ynew=$y;$nxt=$count*40;
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 7);     
						$pdf->text(50, 580.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text(800.89,580.28, 'Page '.$pg); 
#set table header for new page
		$pdf->setFont('Arial', '', 10); 
		$pdf->line($x, $y, $xpoint+1, $y); 
		$pdf->line($x, $y+15, $xpoint+1, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'DAY/START');
		//$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, '8:00-9:00');
		$pdf->line($x+70, $y, $x+70, $y+15);	$pdf->text($x+80, $y+12, '8:00');
		$pdf->line($x+120, $y, $x+120, $y+15);	$pdf->text($x+130, $y+12, '9:00');
		$pdf->line($x+175, $y, $x+175, $y+15);	$pdf->text($x+180, $y+12, '10:00');
		$pdf->line($x+230, $y, $x+230, $y+15);	$pdf->text($x+240, $y+12, '11:00');
		$pdf->line($x+287, $y, $x+287, $y+15);	$pdf->text($x+295, $y+12, '12:00');
		$pdf->line($x+345, $y, $x+345, $y+15);	$pdf->text($x+355, $y+12, '13:00');
		$pdf->line($x+405, $y, $x+405, $y+15);	$pdf->text($x+410, $y+12, '14:00');
		$pdf->line($x+465, $y, $x+465, $y+15);	$pdf->text($x+475, $y+12, '15:00');
		$pdf->line($x+525, $y, $x+525, $y+15);	$pdf->text($x+535, $y+12, '16:00');
		$pdf->line($x+585, $y, $x+585, $y+15);	$pdf->text($x+595, $y+12, '17:00');
		$pdf->line($x+644, $y, $x+644, $y+15);	$pdf->text($x+655, $y+12, '18:00');
		$pdf->line($x+698, $y, $x+698, $y+15);	$pdf->text($x+705, $y+12, '19:00');
		$pdf->line($xpoint+1, $y, $xpoint+1, $y+15);  
		$y=$y+15;

}
}//end 10
if($ynew>=530.28){
$y=$y;
$ysave=$y;
$ynew=$y;
}else{
$y=$ysave;
}
					
	$query_day11 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='11' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz11 = mysql_query($query_day11, $zalongwa) or die(mysql_error());
//$rspan111 = mysql_query("SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='11'");
$rspan11 = mysql_num_rows($daycoz11);$count=0;$y=$y-$nxt;
	while($row_course11 = mysql_fetch_array($daycoz11)){
	$course11= $row_course11['CourseCode'];
	$endcourse11= $row_course11['EndTime'];
	$room11= $row_course11['Room'];
$span11=$endcourse11-11;//Box Ngapi?

$count=$count+1;
$pdf->line($x, $y, $xpoint+1, $y); 
$pdf->line($x, $y+40, $xpoint+1, $y+40);

$pdf->line($x, $y, $x, $y+40);	$pdf->text($x+5, $y+25, strtoupper(''));
if($span11>1)
{$t="HRS";}else {$t="HR";}

$pdf->line($x+70, $y, $x+70, $y+40);	$pdf->text($x+75, $y+12, ' ');
		$pdf->line($x+120, $y, $x+120, $y+40);	$pdf->text($x+125, $y+12, ' ');
		$pdf->line($x+175, $y, $x+175, $y+40);	$pdf->text($x+176, $y+12, ' ');
$pdf->line($x+230, $y, $x+230, $y+40);	$pdf->text($x+232, $y+12, $course11); $course11="";
$pdf->text($x+233, $y+25, $room11); $room11="";$pdf->text($x+233, $y+35, $span11.$t); $span11="";
$pdf->line($x+287, $y, $x+287, $y+40);	$pdf->text($x+288, $y+12, ' ');
		$pdf->line($x+345, $y, $x+345, $y+40);	$pdf->text($x+348, $y+12, ' ');
		$pdf->line($x+405, $y, $x+405, $y+40);	$pdf->text($x+408, $y+12, ' ');
		$pdf->line($x+465, $y, $x+465, $y+40);	$pdf->text($x+468, $y+12, ' ');
		$pdf->line($x+525, $y, $x+525, $y+40);	$pdf->text($x+528, $y+12, ' ');
		$pdf->line($x+585, $y, $x+585, $y+40);	$pdf->text($x+588, $y+12, ' ');
		$pdf->line($x+644, $y, $x+644, $y+40);	$pdf->text($x+644, $y+12, ' ');
		$pdf->line($x+698, $y, $x+698, $y+40);	$pdf->text($x+698, $y+12, ' ');
$pdf->line($xpoint+1, $y, $xpoint+1, $y+40);//right border line

$y=$y+40;

#Go to next page
					
					  if($y>=530.28){
$ynew=$y;$nxt=$count*40;
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 7);     
						$pdf->text(50, 580.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text(800.89,580.28, 'Page '.$pg); 
#set table header for new page
		$pdf->setFont('Arial', '', 10); 
		$pdf->line($x, $y, $xpoint+1, $y); 
		$pdf->line($x, $y+15, $xpoint+1, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'DAY/START');
		//$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, '8:00-9:00');
		$pdf->line($x+70, $y, $x+70, $y+15);	$pdf->text($x+80, $y+12, '8:00');
		$pdf->line($x+120, $y, $x+120, $y+15);	$pdf->text($x+130, $y+12, '9:00');
		$pdf->line($x+175, $y, $x+175, $y+15);	$pdf->text($x+180, $y+12, '10:00');
		$pdf->line($x+230, $y, $x+230, $y+15);	$pdf->text($x+240, $y+12, '11:00');
		$pdf->line($x+287, $y, $x+287, $y+15);	$pdf->text($x+295, $y+12, '12:00');
		$pdf->line($x+345, $y, $x+345, $y+15);	$pdf->text($x+355, $y+12, '13:00');
		$pdf->line($x+405, $y, $x+405, $y+15);	$pdf->text($x+410, $y+12, '14:00');
		$pdf->line($x+465, $y, $x+465, $y+15);	$pdf->text($x+475, $y+12, '15:00');
		$pdf->line($x+525, $y, $x+525, $y+15);	$pdf->text($x+535, $y+12, '16:00');
		$pdf->line($x+585, $y, $x+585, $y+15);	$pdf->text($x+595, $y+12, '17:00');
		$pdf->line($x+644, $y, $x+644, $y+15);	$pdf->text($x+655, $y+12, '18:00');
		$pdf->line($x+698, $y, $x+698, $y+15);	$pdf->text($x+705, $y+12, '19:00');
		$pdf->line($xpoint+1, $y, $xpoint+1, $y+15);  
		$y=$y+15;

}

}//end 11

if($ynew>=530.28){
$y=$y;
$ysave=$y;
$ynew=$y;
}else{
$y=$ysave;
}
							
	$query_day12 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='12' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz12 = mysql_query($query_day12, $zalongwa) or die(mysql_error());
//$rspan121 = mysql_query("SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='12'");
$rspan12 = mysql_num_rows($daycoz12);$count=0; $y=$y-$nxt;
	while($row_course12 = mysql_fetch_array($daycoz12)){
	$course12= $row_course12['CourseCode'];
	$endcourse12= $row_course12['EndTime'];
	$room12= $row_course12['Room'];
$span12=$endcourse12-12;//Box Ngapi?

$count=$count+1;
$pdf->line($x, $y, $xpoint+1, $y); 
$pdf->line($x, $y+40, $xpoint+1, $y+40);

$pdf->line($x, $y, $x, $y+40);	$pdf->text($x+5, $y+25, strtoupper(''));
if($span12>1)
{$t="HRS";}else {$t="HR";}

$pdf->line($x+70, $y, $x+70, $y+40);	$pdf->text($x+75, $y+12, ' ');
		$pdf->line($x+120, $y, $x+120, $y+40);	$pdf->text($x+125, $y+12, ' ');
		$pdf->line($x+175, $y, $x+175, $y+40);	$pdf->text($x+176, $y+12, ' ');
		$pdf->line($x+230, $y, $x+230, $y+40);	$pdf->text($x+232, $y+12, ' ');
$pdf->line($x+287, $y, $x+287, $y+40);	$pdf->text($x+288, $y+12, $course12); $course12="";
$pdf->text($x+289, $y+25, $room12); $room12=""; $pdf->text($x+289, $y+35, $span12.$t); $span12="";
$pdf->line($x+345, $y, $x+345, $y+40);	$pdf->text($x+348, $y+12, ' ');
		$pdf->line($x+405, $y, $x+405, $y+40);	$pdf->text($x+408, $y+12, ' ');
		$pdf->line($x+465, $y, $x+465, $y+40);	$pdf->text($x+468, $y+12, ' ');
		$pdf->line($x+525, $y, $x+525, $y+40);	$pdf->text($x+528, $y+12, ' ');
		$pdf->line($x+585, $y, $x+585, $y+40);	$pdf->text($x+588, $y+12, ' ');
		$pdf->line($x+644, $y, $x+644, $y+40);	$pdf->text($x+644, $y+12, ' ');
		$pdf->line($x+698, $y, $x+698, $y+40);	$pdf->text($x+698, $y+12, ' ');
$pdf->line($xpoint+1, $y, $xpoint+1, $y+40);//right border line

$y=$y+40;

#Go to next page
					
					  if($y>=530.28){
$ynew=$y;$nxt=$count*40;
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 7);     
						$pdf->text(50, 580.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text(800.89,580.28, 'Page '.$pg); 
#set table header for new page
		$pdf->setFont('Arial', '', 10); 
		$pdf->line($x, $y, $xpoint+1, $y); 
		$pdf->line($x, $y+15, $xpoint+1, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'DAY/START');
		//$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, '8:00-9:00');
		$pdf->line($x+70, $y, $x+70, $y+15);	$pdf->text($x+80, $y+12, '8:00');
		$pdf->line($x+120, $y, $x+120, $y+15);	$pdf->text($x+130, $y+12, '9:00');
		$pdf->line($x+175, $y, $x+175, $y+15);	$pdf->text($x+180, $y+12, '10:00');
		$pdf->line($x+230, $y, $x+230, $y+15);	$pdf->text($x+240, $y+12, '11:00');
		$pdf->line($x+287, $y, $x+287, $y+15);	$pdf->text($x+295, $y+12, '12:00');
		$pdf->line($x+345, $y, $x+345, $y+15);	$pdf->text($x+355, $y+12, '13:00');
		$pdf->line($x+405, $y, $x+405, $y+15);	$pdf->text($x+410, $y+12, '14:00');
		$pdf->line($x+465, $y, $x+465, $y+15);	$pdf->text($x+475, $y+12, '15:00');
		$pdf->line($x+525, $y, $x+525, $y+15);	$pdf->text($x+535, $y+12, '16:00');
		$pdf->line($x+585, $y, $x+585, $y+15);	$pdf->text($x+595, $y+12, '17:00');
		$pdf->line($x+644, $y, $x+644, $y+15);	$pdf->text($x+655, $y+12, '18:00');
		$pdf->line($x+698, $y, $x+698, $y+15);	$pdf->text($x+705, $y+12, '19:00');
		$pdf->line($xpoint+1, $y, $xpoint+1, $y+15);  
		$y=$y+15;

}
}//end 12
if($ynew>=530.28){
$y=$y;
$ysave=$y;
$ynew=$y;
}else{
$y=$ysave;
}
							
	$query_day13 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='13' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz13 = mysql_query($query_day13, $zalongwa) or die(mysql_error());
//$rspan131 = mysql_query("SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='13'");
$rspan13 = mysql_num_rows($daycoz13);$count=0; $y=$y-$nxt;
	while($row_course13 = mysql_fetch_array($daycoz13)){
	$course13= $row_course13['CourseCode'];
	$endcourse13= $row_course13['EndTime'];
	$room13= $row_course13['Room'];
$span13=$endcourse13-13;//Box Ngapi?

$count=$count+1;
$pdf->line($x, $y, $xpoint+1, $y); 
$pdf->line($x, $y+40, $xpoint+1, $y+40);

$pdf->line($x, $y, $x, $y+40);	$pdf->text($x+5, $y+25, strtoupper(''));
if($span13>1)
{$t="HRS";}else {$t="HR";}

$pdf->line($x+70, $y, $x+70, $y+40);	$pdf->text($x+75, $y+12, ' ');
		$pdf->line($x+120, $y, $x+120, $y+40);	$pdf->text($x+125, $y+12, ' ');
		$pdf->line($x+175, $y, $x+175, $y+40);	$pdf->text($x+176, $y+12, ' ');
		$pdf->line($x+230, $y, $x+230, $y+40);	$pdf->text($x+232, $y+12, ' ');
		$pdf->line($x+287, $y, $x+287, $y+40);	$pdf->text($x+288, $y+12, ' ');
$pdf->line($x+345, $y, $x+345, $y+40);	$pdf->text($x+348, $y+12, $course13); $course13="";
$pdf->text($x+349, $y+25, $room13); $room13="";$pdf->text($x+349, $y+35, $span13.$t); $span13="";
$pdf->line($x+405, $y, $x+405, $y+40);	$pdf->text($x+408, $y+12, ' ');
		$pdf->line($x+465, $y, $x+465, $y+40);	$pdf->text($x+468, $y+12, ' ');
		$pdf->line($x+525, $y, $x+525, $y+40);	$pdf->text($x+528, $y+12, ' ');
		$pdf->line($x+585, $y, $x+585, $y+40);	$pdf->text($x+588, $y+12, ' ');
		$pdf->line($x+644, $y, $x+644, $y+40);	$pdf->text($x+644, $y+12, ' ');
		$pdf->line($x+698, $y, $x+698, $y+40);	$pdf->text($x+698, $y+12, ' ');
$pdf->line($xpoint+1, $y, $xpoint+1, $y+40);//right border line

$y=$y+40;

#Go to next page
					
					  if($y>=530.28){
$ynew=$y;$nxt=$count*40;
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 7);     
						$pdf->text(50, 580.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text(800.89,580.28, 'Page '.$pg); 
#set table header for new page
		$pdf->setFont('Arial', '', 10); 
		$pdf->line($x, $y, $xpoint+1, $y); 
		$pdf->line($x, $y+15, $xpoint+1, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'DAY/START');
		//$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, '8:00-9:00');
		$pdf->line($x+70, $y, $x+70, $y+15);	$pdf->text($x+80, $y+12, '8:00');
		$pdf->line($x+120, $y, $x+120, $y+15);	$pdf->text($x+130, $y+12, '9:00');
		$pdf->line($x+175, $y, $x+175, $y+15);	$pdf->text($x+180, $y+12, '10:00');
		$pdf->line($x+230, $y, $x+230, $y+15);	$pdf->text($x+240, $y+12, '11:00');
		$pdf->line($x+287, $y, $x+287, $y+15);	$pdf->text($x+295, $y+12, '12:00');
		$pdf->line($x+345, $y, $x+345, $y+15);	$pdf->text($x+355, $y+12, '13:00');
		$pdf->line($x+405, $y, $x+405, $y+15);	$pdf->text($x+410, $y+12, '14:00');
		$pdf->line($x+465, $y, $x+465, $y+15);	$pdf->text($x+475, $y+12, '15:00');
		$pdf->line($x+525, $y, $x+525, $y+15);	$pdf->text($x+535, $y+12, '16:00');
		$pdf->line($x+585, $y, $x+585, $y+15);	$pdf->text($x+595, $y+12, '17:00');
		$pdf->line($x+644, $y, $x+644, $y+15);	$pdf->text($x+655, $y+12, '18:00');
		$pdf->line($x+698, $y, $x+698, $y+15);	$pdf->text($x+705, $y+12, '19:00');
		$pdf->line($xpoint+1, $y, $xpoint+1, $y+15);  
		$y=$y+15;

}

}//end 13
if($ynew>=530.28){
$y=$y;
$ysave=$y;
$ynew=$y;
}else{
$y=$ysave;
}
				
	$query_day14 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='14' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz14 = mysql_query($query_day14, $zalongwa) or die(mysql_error());
//$rspan141 = mysql_query("SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='14'");
$rspan14 = mysql_num_rows($daycoz14);$count=0; $y=$y-$nxt;
	while($row_course14 = mysql_fetch_array($daycoz14)){
	$course14= $row_course14['CourseCode'];
	$endcourse14= $row_course14['EndTime'];
	$room14= $row_course14['Room'];
$span14=$endcourse14-14;//Box Ngapi?

$count=$count+1;
$pdf->line($x, $y, $xpoint+1, $y); 
$pdf->line($x, $y+40, $xpoint+1, $y+40);

$pdf->line($x, $y, $x, $y+40);	$pdf->text($x+5, $y+25, strtoupper(''));
if($span14>1)
{$t="HRS";}else {$t="HR";}

$pdf->line($x+70, $y, $x+70, $y+40);	$pdf->text($x+75, $y+12, ' ');
		$pdf->line($x+120, $y, $x+120, $y+40);	$pdf->text($x+125, $y+12, ' ');
		$pdf->line($x+175, $y, $x+175, $y+40);	$pdf->text($x+176, $y+12, ' ');
		$pdf->line($x+230, $y, $x+230, $y+40);	$pdf->text($x+232, $y+12, ' ');
		$pdf->line($x+287, $y, $x+287, $y+40);	$pdf->text($x+288, $y+12, ' ');
		$pdf->line($x+345, $y, $x+345, $y+40);	$pdf->text($x+348, $y+12, ' ');
$pdf->line($x+405, $y, $x+405, $y+40);	$pdf->text($x+408, $y+12, $course14); $course14="";
$pdf->text($x+409, $y+25, $room14); $room14="";$pdf->text($x+409, $y+35, $span14.$t); $span14="";
$pdf->line($x+465, $y, $x+465, $y+40);	$pdf->text($x+468, $y+12, ' ');
		$pdf->line($x+525, $y, $x+525, $y+40);	$pdf->text($x+528, $y+12, ' ');
		$pdf->line($x+585, $y, $x+585, $y+40);	$pdf->text($x+588, $y+12, ' ');
		$pdf->line($x+644, $y, $x+644, $y+40);	$pdf->text($x+644, $y+12, ' ');
		$pdf->line($x+698, $y, $x+698, $y+40);	$pdf->text($x+698, $y+12, ' ');
$pdf->line($xpoint+1, $y, $xpoint+1, $y+40);//right border line

$y=$y+40;

#Go to next page
					
					  if($y>=530.28){
$ynew=$y;$nxt=$count*40;
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 7);     
						$pdf->text(50, 580.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text(800.89,580.28, 'Page '.$pg); 
#set table header for new page
		$pdf->setFont('Arial', '', 10); 
		$pdf->line($x, $y, $xpoint+1, $y); 
		$pdf->line($x, $y+15, $xpoint+1, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'DAY/START');
		//$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, '8:00-9:00');
		$pdf->line($x+70, $y, $x+70, $y+15);	$pdf->text($x+80, $y+12, '8:00');
		$pdf->line($x+120, $y, $x+120, $y+15);	$pdf->text($x+130, $y+12, '9:00');
		$pdf->line($x+175, $y, $x+175, $y+15);	$pdf->text($x+180, $y+12, '10:00');
		$pdf->line($x+230, $y, $x+230, $y+15);	$pdf->text($x+240, $y+12, '11:00');
		$pdf->line($x+287, $y, $x+287, $y+15);	$pdf->text($x+295, $y+12, '12:00');
		$pdf->line($x+345, $y, $x+345, $y+15);	$pdf->text($x+355, $y+12, '13:00');
		$pdf->line($x+405, $y, $x+405, $y+15);	$pdf->text($x+410, $y+12, '14:00');
		$pdf->line($x+465, $y, $x+465, $y+15);	$pdf->text($x+475, $y+12, '15:00');
		$pdf->line($x+525, $y, $x+525, $y+15);	$pdf->text($x+535, $y+12, '16:00');
		$pdf->line($x+585, $y, $x+585, $y+15);	$pdf->text($x+595, $y+12, '17:00');
		$pdf->line($x+644, $y, $x+644, $y+15);	$pdf->text($x+655, $y+12, '18:00');
		$pdf->line($x+698, $y, $x+698, $y+15);	$pdf->text($x+705, $y+12, '19:00');
		$pdf->line($xpoint+1, $y, $xpoint+1, $y+15);  
		$y=$y+15;

}

}//end 14

if($ynew>=530.28){
$y=$y;
$ysave=$y;
$ynew=$y;
}else{
$y=$ysave;

}
				
	$query_day15 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='15' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz15 = mysql_query($query_day15, $zalongwa) or die(mysql_error());
//$rspan151 = mysql_query("SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='15'");
$rspan15 = mysql_num_rows($daycoz15);$count=0; $y=$y-$nxt;
	while($row_course15 = mysql_fetch_array($daycoz15)){
	$course15= $row_course15['CourseCode'];
	$endcourse15= $row_course15['EndTime'];
	$room15= $row_course15['Room'];
$span15=$endcourse15-15;//Box Ngapi?


$count=$rspan15-1;
$pdf->line($x, $y, $xpoint+1, $y); 
$pdf->line($x, $y+40, $xpoint+1, $y+40);

$pdf->line($x, $y, $x, $y+40);	$pdf->text($x+5, $y+25, strtoupper(''));
if($span15>1)
{$t="HRS";}else {$t="HR";}

$pdf->line($x+70, $y, $x+70, $y+40);	$pdf->text($x+75, $y+12, ' ');
		$pdf->line($x+120, $y, $x+120, $y+40);	$pdf->text($x+125, $y+12, $y);
		$pdf->line($x+175, $y, $x+175, $y+40);	$pdf->text($x+176, $y+12, ' ');
		$pdf->line($x+230, $y, $x+230, $y+40);	$pdf->text($x+232, $y+12, ' ');
		$pdf->line($x+287, $y, $x+287, $y+40);	$pdf->text($x+288, $y+12, ' ');
		$pdf->line($x+345, $y, $x+345, $y+40);	$pdf->text($x+348, $y+12, ' ');
		$pdf->line($x+405, $y, $x+405, $y+40);	$pdf->text($x+408, $y+12, ' ');
$pdf->line($x+465, $y, $x+465, $y+40);	$pdf->text($x+468, $y+12, $course15); $course15="";
$pdf->text($x+469, $y+25, $room15); $room15=""; $pdf->text($x+469, $y+35, $span15.$t); $span15="";
$pdf->line($x+525, $y, $x+525, $y+40);	$pdf->text($x+528, $y+12, ' ');
		$pdf->line($x+585, $y, $x+585, $y+40);	$pdf->text($x+588, $y+12, ' ');
		$pdf->line($x+644, $y, $x+644, $y+40);	$pdf->text($x+644, $y+12, ' ');
		$pdf->line($x+698, $y, $x+698, $y+40);	$pdf->text($x+698, $y+12, ' ');
$pdf->line($xpoint+1, $y, $xpoint+1, $y+40);//right border line

//$nxt=$count*40;
//$y=$nxt;
$y=$y+40;

#Go to next page
					
					  if($y>=530.28){
//$count=$count-1;
$ynew=$y;$nxt=$count*40;
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 7);     
						$pdf->text(50, 580.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text(800.89,580.28, 'Page '.$pg); 
#set table header for new page
		$pdf->setFont('Arial', '', 10); 
		$pdf->line($x, $y, $xpoint+1, $y); 
		$pdf->line($x, $y+15, $xpoint+1, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'DAY/START');
		//$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, '8:00-9:00');
		$pdf->line($x+70, $y, $x+70, $y+15);	$pdf->text($x+80, $y+12, '8:00');
		$pdf->line($x+120, $y, $x+120, $y+15);	$pdf->text($x+130, $y+12, '9:00');
		$pdf->line($x+175, $y, $x+175, $y+15);	$pdf->text($x+180, $y+12, '10:00');
		$pdf->line($x+230, $y, $x+230, $y+15);	$pdf->text($x+240, $y+12, '11:00');
		$pdf->line($x+287, $y, $x+287, $y+15);	$pdf->text($x+295, $y+12, '12:00');
		$pdf->line($x+345, $y, $x+345, $y+15);	$pdf->text($x+355, $y+12, '13:00');
		$pdf->line($x+405, $y, $x+405, $y+15);	$pdf->text($x+410, $y+12, '14:00');
		$pdf->line($x+465, $y, $x+465, $y+15);	$pdf->text($x+475, $y+12, '15:00');
		$pdf->line($x+525, $y, $x+525, $y+15);	$pdf->text($x+535, $y+12, '16:00');
		$pdf->line($x+585, $y, $x+585, $y+15);	$pdf->text($x+595, $y+12, '17:00');
		$pdf->line($x+644, $y, $x+644, $y+15);	$pdf->text($x+655, $y+12, '18:00');
		$pdf->line($x+698, $y, $x+698, $y+15);	$pdf->text($x+705, $y+12, '19:00');
		$pdf->line($xpoint+1, $y, $xpoint+1, $y+15);  
		$y=$y+15;

}

}//end 15
if($ynew>=530.28){
$y=$y;
$ysave=$y;
$ynew=$y;
}else{
$y=$ysave;
}
					
	$query_day16 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='16' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz16 = mysql_query($query_day16, $zalongwa) or die(mysql_error());
//$rspan161 = mysql_query("SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='16'");
$rspan16 = mysql_num_rows($daycoz16);$count=0; $y=$y-$nxt;
	while($row_course16 = mysql_fetch_array($daycoz16)){
	$course16= $row_course16['CourseCode'];
	$endcourse16= $row_course16['EndTime'];
	$room16= $row_course16['Room'];
$span16=$endcourse16-16;//Box Ngapi?


$count=$count+1;
$pdf->line($x, $y, $xpoint+1, $y); 
$pdf->line($x, $y+40, $xpoint+1, $y+40);

$pdf->line($x, $y, $x, $y+40);	$pdf->text($x+5, $y+25, strtoupper(''));
if($span16>1)
{$t="HRS";}else {$t="HR";}

$pdf->line($x+70, $y, $x+70, $y+40);	$pdf->text($x+75, $y+12, ' ');
		$pdf->line($x+120, $y, $x+120, $y+40);	$pdf->text($x+125, $y+12, $y);
		$pdf->line($x+175, $y, $x+175, $y+40);	$pdf->text($x+176, $y+12, ' ');
		$pdf->line($x+230, $y, $x+230, $y+40);	$pdf->text($x+232, $y+12, ' ');
		$pdf->line($x+287, $y, $x+287, $y+40);	$pdf->text($x+288, $y+12, ' ');
		$pdf->line($x+345, $y, $x+345, $y+40);	$pdf->text($x+348, $y+12, ' ');
		$pdf->line($x+405, $y, $x+405, $y+40);	$pdf->text($x+408, $y+12, ' ');
		$pdf->line($x+465, $y, $x+465, $y+40);	$pdf->text($x+468, $y+12, ' ');
$pdf->line($x+525, $y, $x+525, $y+40);	$pdf->text($x+528, $y+12, $course16); $course16="";
$pdf->text($x+529, $y+25, $room16); $room16=""; $pdf->text($x+529, $y+35, $span16.$t); $span16="";
$pdf->line($x+585, $y, $x+585, $y+40);	$pdf->text($x+588, $y+12, ' ');
$pdf->line($x+644, $y, $x+644, $y+40);	$pdf->text($x+644, $y+12, ' ');
$pdf->line($x+698, $y, $x+698, $y+40);	$pdf->text($x+698, $y+12, ' ');
$pdf->line($xpoint+1, $y, $xpoint+1, $y+40);//right border line


$y=$y+40;

#Go to next page
					
					  if($y>=530.28){
$ynew=$y;$nxt=$count*40;
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 7);     
						$pdf->text(50, 580.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text(800.89,580.28, 'Page '.$pg); 
#set table header for new page
		$pdf->setFont('Arial', '', 10); 
		$pdf->line($x, $y, $xpoint+1, $y); 
		$pdf->line($x, $y+15, $xpoint+1, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'DAY/START');
		//$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, '8:00-9:00');
		$pdf->line($x+70, $y, $x+70, $y+15);	$pdf->text($x+80, $y+12, '8:00');
		$pdf->line($x+120, $y, $x+120, $y+15);	$pdf->text($x+130, $y+12, '9:00');
		$pdf->line($x+175, $y, $x+175, $y+15);	$pdf->text($x+180, $y+12, '10:00');
		$pdf->line($x+230, $y, $x+230, $y+15);	$pdf->text($x+240, $y+12, '11:00');
		$pdf->line($x+287, $y, $x+287, $y+15);	$pdf->text($x+295, $y+12, '12:00');
		$pdf->line($x+345, $y, $x+345, $y+15);	$pdf->text($x+355, $y+12, '13:00');
		$pdf->line($x+405, $y, $x+405, $y+15);	$pdf->text($x+410, $y+12, '14:00');
		$pdf->line($x+465, $y, $x+465, $y+15);	$pdf->text($x+475, $y+12, '15:00');
		$pdf->line($x+525, $y, $x+525, $y+15);	$pdf->text($x+535, $y+12, '16:00');
		$pdf->line($x+585, $y, $x+585, $y+15);	$pdf->text($x+595, $y+12, '17:00');
		$pdf->line($x+644, $y, $x+644, $y+15);	$pdf->text($x+655, $y+12, '18:00');
		$pdf->line($x+698, $y, $x+698, $y+15);	$pdf->text($x+705, $y+12, '19:00');
		$pdf->line($xpoint+1, $y, $xpoint+1, $y+15);  
		$y=$y+15;

}
}//end 16
if($ynew>=530.28){
$y=$y;
$ysave=$y;
$ynew=$y;
}else{
$y=$ysave;
}
				
	$query_day17 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='17' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz17 = mysql_query($query_day17, $zalongwa) or die(mysql_error());
//$rspan171 = mysql_query("SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='17'");
$rspan17 = mysql_num_rows($daycoz17);$count=0; $y=$y-$nxt;
	while($row_course17 = mysql_fetch_array($daycoz17)){
	$course17= $row_course17['CourseCode'];
	$endcourse17= $row_course17['EndTime'];
	$room17= $row_course17['Room'];
$span17=$endcourse17-17;//Box Ngapi?


$count=$count+1;
$pdf->line($x, $y, $xpoint+1, $y); 
$pdf->line($x, $y+40, $xpoint+1, $y+40);

$pdf->line($x, $y, $x, $y+40);	$pdf->text($x+5, $y+25, strtoupper(''));
if($span17>1)
{$t="HRS";}else {$t="HR";}

$pdf->line($x+70, $y, $x+70, $y+40);	$pdf->text($x+75, $y+12, ' ');
$pdf->line($x+120, $y, $x+120, $y+40);	$pdf->text($x+125, $y+12, ' ');
		$pdf->line($x+175, $y, $x+175, $y+40);	$pdf->text($x+176, $y+12, ' ');
		$pdf->line($x+230, $y, $x+230, $y+40);	$pdf->text($x+232, $y+12, ' ');
		$pdf->line($x+287, $y, $x+287, $y+40);	$pdf->text($x+288, $y+12, ' ');
		$pdf->line($x+345, $y, $x+345, $y+40);	$pdf->text($x+348, $y+12, ' ');
		$pdf->line($x+405, $y, $x+405, $y+40);	$pdf->text($x+408, $y+12, ' ');
		$pdf->line($x+465, $y, $x+465, $y+40);	$pdf->text($x+468, $y+12, ' ');
		$pdf->line($x+525, $y, $x+525, $y+40);	$pdf->text($x+528, $y+12, ' ');
		$pdf->line($x+585, $y, $x+585, $y+40);	$pdf->text($x+588, $y+12, $course17); $course17="";
		$pdf->text($x+589, $y+25, $room17); $room17=""; $pdf->text($x+589, $y+35, $span17.$t); $span17="";
		$pdf->line($x+644, $y, $x+644, $y+40);	$pdf->text($x+644, $y+12, ' ');
		$pdf->line($x+698, $y, $x+698, $y+40);	$pdf->text($x+698, $y+12, ' ');
$pdf->line($xpoint+1, $y, $xpoint+1, $y+40);//right border line

$y=$y+40;

#Go to next page
					
					  if($y>=530.28){
$ynew=$y;$nxt=$count*40;
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 7);     
						$pdf->text(50, 580.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text(800.89,580.28, 'Page '.$pg); 
#set table header for new page
		$pdf->setFont('Arial', '', 10); 
		$pdf->line($x, $y, $xpoint+1, $y); 
		$pdf->line($x, $y+15, $xpoint+1, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 	$pdf->text($x+2, $y+12, 'DAY/START');
		//$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, '8:00-9:00');
		$pdf->line($x+70, $y, $x+70, $y+15);	$pdf->text($x+80, $y+12, '8:00');
		$pdf->line($x+120, $y, $x+120, $y+15);	$pdf->text($x+130, $y+12, '9:00');
		$pdf->line($x+175, $y, $x+175, $y+15);	$pdf->text($x+180, $y+12, '10:00');
		$pdf->line($x+230, $y, $x+230, $y+15);	$pdf->text($x+240, $y+12, '11:00');
		$pdf->line($x+287, $y, $x+287, $y+15);	$pdf->text($x+295, $y+12, '12:00');
		$pdf->line($x+345, $y, $x+345, $y+15);	$pdf->text($x+355, $y+12, '13:00');
		$pdf->line($x+405, $y, $x+405, $y+15);	$pdf->text($x+410, $y+12, '14:00');
		$pdf->line($x+465, $y, $x+465, $y+15);	$pdf->text($x+475, $y+12, '15:00');
		$pdf->line($x+525, $y, $x+525, $y+15);	$pdf->text($x+535, $y+12, '16:00');
		$pdf->line($x+585, $y, $x+585, $y+15);	$pdf->text($x+595, $y+12, '17:00');
		$pdf->line($x+644, $y, $x+644, $y+15);	$pdf->text($x+655, $y+12, '18:00');
		$pdf->line($x+698, $y, $x+698, $y+15);	$pdf->text($x+705, $y+12, '19:00');
		$pdf->line($xpoint+1, $y, $xpoint+1, $y+15);  
		$y=$y+15;


}

}//end 17
if($ynew>=530.28){
$y=$y;
$ysave=$y;
$ynew=$y;
}else{
$y=$ysave;
}
						
	$query_day18 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='18'AND Semester='$sem' AND Faculty='$degree'";
	$daycoz18 = mysql_query($query_day18, $zalongwa) or die(mysql_error());
//$rspan181 = mysql_query("SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='18'");
$rspan18 = mysql_num_rows($daycoz18);$count=0; $y=$y-$nxt;

	while($row_course18 = mysql_fetch_array($daycoz18)){
	$course18= $row_course18['CourseCode'];
	$endcourse18= $row_course18['EndTime'];
	$room18= $row_course18['Room'];
$span18=$endcourse18-18;//Box Ngapi?

$count=$count+1;
$pdf->line($x, $y, $xpoint+1, $y); 
$pdf->line($x, $y+40, $xpoint+1, $y+40);

$pdf->line($x, $y, $x, $y+40);	$pdf->text($x+5, $y+25, strtoupper(''));
if($span18>1)
{$t="HRS";}else {$t="HR";}

$pdf->line($x+70, $y, $x+70, $y+40);	$pdf->text($x+75, $y+12, ' ');
$pdf->line($x+120, $y, $x+120, $y+40);	$pdf->text($x+125, $y+12, ' ');
		$pdf->line($x+175, $y, $x+175, $y+40);	$pdf->text($x+176, $y+12, ' ');
		$pdf->line($x+230, $y, $x+230, $y+40);	$pdf->text($x+232, $y+12, ' ');
		$pdf->line($x+287, $y, $x+287, $y+40);	$pdf->text($x+288, $y+12, ' ');
		$pdf->line($x+345, $y, $x+345, $y+40);	$pdf->text($x+348, $y+12, ' ');
		$pdf->line($x+405, $y, $x+405, $y+40);	$pdf->text($x+408, $y+12, ' ');
		$pdf->line($x+465, $y, $x+465, $y+40);	$pdf->text($x+468, $y+12, ' ');
		$pdf->line($x+525, $y, $x+525, $y+40);	$pdf->text($x+528, $y+12, ' ');
		$pdf->line($x+585, $y, $x+585, $y+40);	$pdf->text($x+588, $y+12, ' ');
$pdf->line($x+644, $y, $x+644, $y+40);	$pdf->text($x+644, $y+12, $course18); $course18="";
$pdf->text($x+645, $y+25, $room18); $room18=""; $pdf->text($x+646, $y+35, $span18.$t); $span18="";
$pdf->line($x+698, $y, $x+698, $y+40);	$pdf->text($x+698, $y+12, ' ');
$pdf->line($xpoint+1, $y, $xpoint+1, $y+40);//right border line

$y=$y+40;

#Go to next page
					
					  if($y>=530.28){
$ynew=$y;$nxt=$count*40; //save y and reset rows

					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 7);     
						$pdf->text(50, 580.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text(800.89,580.28, 'Page '.$pg); 
#set table header for new page
		$pdf->setFont('Arial', '', 10); 
		$pdf->line($x, $y, $xpoint+1, $y); 
		$pdf->line($x, $y+15, $xpoint+1, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'DAY/START');
		//$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, '8:00-9:00');
		$pdf->line($x+70, $y, $x+70, $y+15);	$pdf->text($x+80, $y+12, '8:00');
		$pdf->line($x+120, $y, $x+120, $y+15);	$pdf->text($x+130, $y+12, '9:00');
		$pdf->line($x+175, $y, $x+175, $y+15);	$pdf->text($x+180, $y+12, '10:00');
		$pdf->line($x+230, $y, $x+230, $y+15);	$pdf->text($x+240, $y+12, '11:00');
		$pdf->line($x+287, $y, $x+287, $y+15);	$pdf->text($x+295, $y+12, '12:00');
		$pdf->line($x+345, $y, $x+345, $y+15);	$pdf->text($x+355, $y+12, '13:00');
		$pdf->line($x+405, $y, $x+405, $y+15);	$pdf->text($x+410, $y+12, '14:00');
		$pdf->line($x+465, $y, $x+465, $y+15);	$pdf->text($x+475, $y+12, '15:00');
		$pdf->line($x+525, $y, $x+525, $y+15);	$pdf->text($x+535, $y+12, '16:00');
		$pdf->line($x+585, $y, $x+585, $y+15);	$pdf->text($x+595, $y+12, '17:00');
		$pdf->line($x+644, $y, $x+644, $y+15);	$pdf->text($x+655, $y+12, '18:00');
		$pdf->line($x+698, $y, $x+698, $y+15);	$pdf->text($x+705, $y+12, '19:00');
		$pdf->line($xpoint+1, $y, $xpoint+1, $y+15);  
		$y=$y+15;



}

}//end 18
if($ynew>=530.28){
$y=$y; //reset y after new page
$ysave=$y;
$ynew=$y;
}else{
$y=$ysave;
}
						
	$query_day19 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='19'AND Semester='$sem' AND Faculty='$degree'";
	$daycoz19 = mysql_query($query_day19, $zalongwa) or die(mysql_error());
//$rspan191 = mysql_query("SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='19'");
$rspan19 = mysql_num_rows($daycoz19);$count=0; $y=$y-$nxt;

	while($row_course19 = mysql_fetch_array($daycoz19)){
	$course19= $row_course19['CourseCode'];
	$endcourse19= $row_course19['EndTime'];
	$room19= $row_course19['Room'];
$span19=$endcourse19-19;//Box Ngapi?


$count=$count+1;
$pdf->line($x, $y, $xpoint+1, $y); 
$pdf->line($x, $y+40, $xpoint+1, $y+40);
$pdf->line($x, $y, $x, $y+40);	$pdf->text($x+5, $y+25, strtoupper(''));
if($span19>1)
{$t="HRS";}else {$t="HR";}

$pdf->line($x+70, $y, $x+70, $y+40);	$pdf->text($x+75, $y+12, ' ');
		$pdf->line($x+120, $y, $x+120, $y+40);	$pdf->text($x+125, $y+12, ' ');
		$pdf->line($x+175, $y, $x+175, $y+40);	$pdf->text($x+176, $y+12, ' ');
		$pdf->line($x+230, $y, $x+230, $y+40);	$pdf->text($x+232, $y+12, ' ');
		$pdf->line($x+287, $y, $x+287, $y+40);	$pdf->text($x+288, $y+12, ' ');
		$pdf->line($x+345, $y, $x+345, $y+40);	$pdf->text($x+348, $y+12, ' ');
		$pdf->line($x+405, $y, $x+405, $y+40);	$pdf->text($x+408, $y+12, ' ');
		$pdf->line($x+465, $y, $x+465, $y+40);	$pdf->text($x+468, $y+12, ' ');
		$pdf->line($x+525, $y, $x+525, $y+40);	$pdf->text($x+528, $y+12, ' ');
		$pdf->line($x+585, $y, $x+585, $y+40);	$pdf->text($x+588, $y+12, ' ');
		$pdf->line($x+644, $y, $x+644, $y+40);	$pdf->text($x+644, $y+12, ' ');
$pdf->line($x+698, $y, $x+698, $y+40);	$pdf->text($x+698, $y+12, $course19); $course19="";
$pdf->text($x+700, $y+25, $room19); $room19=""; $pdf->text($x+699, $y+35, $span19.$t); $span19="";
$pdf->line($xpoint+1, $y, $xpoint+1, $y+40);//right border line

$y=$y+40;

#Go to next page
					
					  if($y>=530.28){
$ynew=$y;$nxt=$count*40;
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 7);     
						$pdf->text(50, 580.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text(800.89,580.28, 'Page '.$pg); 
#set table header for new page
		$pdf->setFont('Arial', '', 10); 
		$pdf->line($x, $y, $xpoint+1, $y); 
		$pdf->line($x, $y+15, $xpoint+1, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'DAY/START');
		//$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, '8:00-9:00');
		$pdf->line($x+70, $y, $x+70, $y+15);	$pdf->text($x+80, $y+12, '8:00');
		$pdf->line($x+120, $y, $x+120, $y+15);	$pdf->text($x+130, $y+12, '9:00');
		$pdf->line($x+175, $y, $x+175, $y+15);	$pdf->text($x+180, $y+12, '10:00');
		$pdf->line($x+230, $y, $x+230, $y+15);	$pdf->text($x+240, $y+12, '11:00');
		$pdf->line($x+287, $y, $x+287, $y+15);	$pdf->text($x+295, $y+12, '12:00');
		$pdf->line($x+345, $y, $x+345, $y+15);	$pdf->text($x+355, $y+12, '13:00');
		$pdf->line($x+405, $y, $x+405, $y+15);	$pdf->text($x+410, $y+12, '14:00');
		$pdf->line($x+465, $y, $x+465, $y+15);	$pdf->text($x+475, $y+12, '15:00');
		$pdf->line($x+525, $y, $x+525, $y+15);	$pdf->text($x+535, $y+12, '16:00');
		$pdf->line($x+585, $y, $x+585, $y+15);	$pdf->text($x+595, $y+12, '17:00');
		$pdf->line($x+644, $y, $x+644, $y+15);	$pdf->text($x+655, $y+12, '18:00');
		$pdf->line($x+698, $y, $x+698, $y+15);	$pdf->text($x+705, $y+12, '19:00');
		$pdf->line($xpoint+1, $y, $xpoint+1, $y+15);  
		$y=$y+15;



}


}//end 19
if($ynew>=530.28){
$y=$y;
$ysave=$y;
$ynew=$y;
}else{
$y=$ysave;
}
					
}//end for loop
}	
	}
 	 #output file
	 $filename = ereg_replace("[[:space:]]+", "",$sem."timetable");
	 $pdf->output($filename.'.pdf');
	 
}//end of print pdf
?>
<?php 
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('lecturerMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Timetable';
	$szSubSection = 'Get TimeTable';
	$szTitle = 'Semester Timetable';
	include('lecturerheader.php');
$editFormAction = $_SERVER['PHP_SELF'];

mysql_select_db($database_zalongwa, $zalongwa);
$query_ayear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$ayear = mysql_query($query_ayear, $zalongwa) or die(mysql_error());
$row_ayear = mysql_fetch_assoc($ayear);
$totalRows_ayear = mysql_num_rows($ayear);

mysql_select_db($database_zalongwa, $zalongwa);
$query_sem = "SELECT Semester FROM terms ORDER BY Semester";
$sem = mysql_query($query_sem, $zalongwa) or die(mysql_error());
$row_sem = mysql_fetch_assoc($sem);
$totalRows_sem = mysql_num_rows($sem);

mysql_select_db($database_zalongwa, $zalongwa);
$query_dept = "SELECT FacultyName FROM faculty ORDER BY FacultyName ASC";
$dept = mysql_query($query_dept, $zalongwa) or die(mysql_error());
$row_dept = mysql_fetch_assoc($dept);
$totalRows_dept = mysql_num_rows($dept);
?>
<?php
			
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")||$_GET['page']) {
?>
<style type="text/css">
<!--
.style1 {color: #FFFFFF}
-->
</style>

<h4 align="center">

<?php 
$prog=$_POST['degree'];
$cohotyear = $_POST['cohot'];
$ayear = $_POST['ayear'];
$qprog= "SELECT * FROM faculty WHERE FacultyName='$prog'";
$dbprog = mysql_query($qprog);
$row_prog = mysql_fetch_array($dbprog);
$progname = $row_prog['FacultyName'];

$qyear= "SELECT AYear FROM academicyear WHERE AYear='$cohotyear'";
$dbyear = mysql_query($qyear);
$row_year = mysql_fetch_array($dbyear);
$year = $row_year['AYear'];
echo $progname;
//echo " - ".$year;

?>

<br>
</h4>
<?php

@$checkdegree = addslashes($_POST['checkdegree']);
@$checkyear = addslashes($_POST['checkyear']);
@$checksem = addslashes($_POST['checksem']);
$checkcohot = addslashes($_POST['checkcohot']);

$c=0;

if (($checkdegree =='on') && ($checkyear == 'on') && ($checksem == 'on')){

$deg=addslashes($_POST['degree']);
$degree=addslashes($_POST['degree']);
$year = addslashes($_POST['ayear']);
$cohot = addslashes($_POST['cohot']);
$sem = addslashes($_POST['sem']); 
 if($sem=="Semester I" || $sem == "Semester II")
   $class="for 1ST Year";
else if($sem=="Semester III" || $sem == "Semester IV")
   $class="for 2ND Year";
else if($sem=="Semester V" || $sem == "Semester VI")
   $class=" for 3RD Year";
else if($sem=="Semester VII" || $sem == "Semester VIII")
   $class="for 4TH Year";
else if($sem=="Semester IX" || $sem == "Semester X")
   $class=" for 5TH Year";
else 
   $class=" ";

echo "<p><b>The Timetable ". $class." for Academic Year - ".$ayear;
echo"</b></p>";
?>
			
			<table class='resView width="100%" height="100%">
				  <tr>
					<td class='resViewhd' nowrap scope="col"><div align="left"></div> &nbsp;</td>
					<td class='resViewhd' ><div align="center">&nbsp; </div></td>
						<td class='resViewhd'><div align="center">8:00-9:00 </div></td>
						<td class='resViewhd'><div align="center">9:00-10:00</div></td>
						<td class='resViewhd'><div align="center">10:00-11:00</div></td>
						<td class='resViewhd'><div align="center">11:00-12:00</div></td>
						<td class='resViewhd'><div align="center">12:00-13:00</div></td>
						<td class='resViewhd'><div align="center">13:00-14:00</div></td>
						<td class='resViewhd'><div align="center">14:00-15:00</div></td>
						<td class='resViewhd'><div align="center">15:00-16:00</div></td>
						<td class='resViewhd'><div align="center">16:00-17:00</div></td>
						<td class='resViewhd'><div align="center">17:00-18:00</div></td>
						<td class='resViewhd'><div align="center">18:00-19:00</div></td>
						<td class='resViewhd'><div align="center">19:00-20:00</div></td>
							
				  </tr>
<?php 
//$loop=5;
for ($i=1; $i<=5; $i++){
 switch ($i){
case 1: {$day="Monday"; break; }
case 2: {$day="Tuesday"; break; }
case 3: {$day="Wednesday"; break; }
case 4: {$day="Thursday"; break; }
case 5: {$day="Friday"; break; }
default: "";
} 
//$day='Monday';
mysql_select_db($database_zalongwa, $zalongwa);
$query_day = "SELECT * FROM examregisterlecturer WHERE Day='$day'";
$daycoz = mysql_query($query_day, $zalongwa) or die(mysql_error());
//$row_day = mysql_fetch_assoc($daycoz);
$totalRows_day = mysql_num_rows($daycoz);?>
<tr><td class='resViewhd' rowspan="<?php echo $totalRows_day ?>"><div align='center'><?php echo $day ?></td>
<?php while($row_day = mysql_fetch_array($daycoz)){
$id=$row_day['PCode'];
//Endelea hapa
$count=$totalRows_day;
//$counting=0;
 ?>
				  
		<?php 
	$query_day8 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='8' AND PCode='$id' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz8 = mysql_query($query_day8, $zalongwa) or die(mysql_error());
$rspan8 = mysql_num_rows($daycoz8);

	while($row_course8 = mysql_fetch_array($daycoz8)){
	$course8 = $row_course8['CourseCode'];
	$endcourse8 = $row_course8['EndTime'];
	$room8 = $row_course8['Venue'];
$span8=$endcourse8-8;//Box Ngapi?
}
//if($course8==""){//Nothing
//}else
//echo"<td class='resViewtd'><div align='center'> 8 &nbsp;$room8 <br><br> $course8</div></td>";
//$course8="";
						

	$query_day9 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='9' AND PCode='$id' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz9 = mysql_query($query_day9, $zalongwa) or die(mysql_error());
$rspan9 = mysql_num_rows($daycoz9);
	while($row_course9 = mysql_fetch_array($daycoz9)){
	$course9= $row_course9['CourseCode'];
	$endcourse9= $row_course9['EndTime'];
	$room9= $row_course9['Venue'];
$span9=$endcourse9-9;//Box Ngapi?
}
//if($course9==""){ //Nothing
//}else
//echo"<td class='resViewtd'><div align='center'> 9 &nbsp;$room9 <br><br> $course9</div></td>";
//$course9="";
						
	$query_day10 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='10' AND PCode='$id' AND Semester='$sem' AND Faculty='$degree' ";
	$daycoz10 = mysql_query($query_day10, $zalongwa) or die(mysql_error());
$rspan10 = mysql_num_rows($daycoz10);
	while($row_course10 = mysql_fetch_array($daycoz10)){
	$course10= $row_course10['CourseCode'];
	$endcourse10= $row_course10['EndTime'];
	$room10= $row_course10['Venue'];
$span10=$endcourse10-10;//Box Ngapi?
}
//if($course10==""){ //Nothing
//}else
//echo"<td class='resViewtd'><div align='center'>10 $room10 <br><br> $course10</div></td>";
//$course10="";
						
	$query_day11 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='11'AND PCode='$id' AND Semester='$sem' AND Faculty='$degree' ";
	$daycoz11 = mysql_query($query_day11, $zalongwa) or die(mysql_error());
$rspan11 = mysql_num_rows($daycoz11);
	while($row_course11 = mysql_fetch_array($daycoz11)){
	$course11= $row_course11['CourseCode'];
	$endcourse11= $row_course11['EndTime'];
	$room11= $row_course11['Venue'];
$span11=$endcourse11-11;//Box Ngapi?
}
//if($course11==""){ //Nothing
//}else
//echo"<td class='resViewtd'><div align='center'>11 $room11 <br><br> $course11</div></td>";
//$course11="";
						
	$query_day12 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='12' AND PCode='$id' AND Semester='$sem' AND Faculty='$degree' ";
	$daycoz12 = mysql_query($query_day12, $zalongwa) or die(mysql_error());
$rspan12 = mysql_num_rows($daycoz12);
	while($row_course12 = mysql_fetch_array($daycoz12)){
	$course12= $row_course12['CourseCode'];
	$endcourse12= $row_course12['EndTime'];
	$room12= $row_course12['Venue'];
$span12=$endcourse12-12;//Box Ngapi?
}
//if($course12==""){ //Nothing
//}else
//echo"<td class='resViewtd'><div align='center'>12 $room12 <br><br> $course12</div></td>";
//$course12="";
						
	$query_day13 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='13' AND PCode='$id' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz13 = mysql_query($query_day13, $zalongwa) or die(mysql_error());
$rspan13 = mysql_num_rows($daycoz13);
	while($row_course13 = mysql_fetch_array($daycoz13)){
	$course13= $row_course13['CourseCode'];
	$endcourse13= $row_course13['EndTime'];
	$room13= $row_course13['Venue'];
$span13=$endcourse13-13;//Box Ngapi?
}
//if($course13==""){ //Nothing
//}else
//echo"<td class='resViewtd'><div align='center'>13 $room13 <br><br> $course13</div></td>";
//$course13="";
						
	$query_day14 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='14' AND PCode='$id' AND Semester='$sem' AND Faculty='$degree' ";
	$daycoz14 = mysql_query($query_day14, $zalongwa) or die(mysql_error());
$rspan14 = mysql_num_rows($daycoz14);
	while($row_course14 = mysql_fetch_array($daycoz14)){
	$course14= $row_course14['CourseCode'];
	$endcourse14= $row_course14['EndTime'];
	$room14= $row_course14['Venue'];
$span14=$endcourse14-14;//Box Ngapi?
}
//if($course14==""){ //Nothing
//}else
//echo"<td class='resViewtd'><div align='center'>14 $room14 <br><br> $course14</div></td>";
//$course14="";
						
	$query_day15 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='15' AND PCode='$id' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz15 = mysql_query($query_day15, $zalongwa) or die(mysql_error());
$rspan15 = mysql_num_rows($daycoz15);
	while($row_course15 = mysql_fetch_array($daycoz15)){
	$course15= $row_course15['CourseCode'];
	$endcourse15= $row_course15['EndTime'];
	$room15= $row_course15['Venue'];
$span15=$endcourse15-15;//Box Ngapi?
}
//if($course15==""){ //Nothing
//}else
//echo"<td class='resViewtd'><div align='center'>15 $room15 <br><br> $course15</div></td>";
//$course15="";
						
	$query_day16 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='16' AND PCode='$id' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz16 = mysql_query($query_day16, $zalongwa) or die(mysql_error());
$rspan16 = mysql_num_rows($daycoz16);
	while($row_course16 = mysql_fetch_array($daycoz16)){
	$course16= $row_course16['CourseCode'];
	$endcourse16= $row_course16['EndTime'];
	$room16= $row_course16['Venue'];
$span16=$endcourse16-16;//Box Ngapi?
}
//if($course16==""){ //Nothing
//}else
//echo"<td class='resViewtd'><div align='center'>16 $room16 <br><br> $course16</div></td>";
//$course16="";
						
	$query_day17 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='17' AND PCode='$id'AND Semester='$sem' AND Faculty='$degree'";
	$daycoz17 = mysql_query($query_day17, $zalongwa) or die(mysql_error());
$rspan17 = mysql_num_rows($daycoz17);
	while($row_course17 = mysql_fetch_array($daycoz17)){
	$course17= $row_course17['CourseCode'];
	$endcourse17= $row_course17['EndTime'];
	$room17= $row_course17['Venue'];
$span17=$endcourse17-17;//Box Ngapi?
}
//if($course18==""){ //Nothing
//}else
//echo"<td class='resViewtd'><div align='center'>17 &nbsp;$room17 <br><br> $course17</div></td>";
//$course17="";
						
	$query_day18 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='18' AND PCode='$id'AND Semester='$sem' AND Faculty='$degree'";
	$daycoz18 = mysql_query($query_day18, $zalongwa) or die(mysql_error());
$rspan18 = mysql_num_rows($daycoz18);
	while($row_course18 = mysql_fetch_array($daycoz18)){
	$course18= $row_course18['CourseCode'];
	$endcourse18= $row_course18['EndTime'];
	$room18= $row_course18['Venue'];
$span18=$endcourse18-18;//Box Ngapi?
}
//if($course18==""){ //Nothing
//}else
//echo"<td class='resViewtd'><div align='center'>18 $room18 <br><br> $course18</div></td>";
//$course18="";
						
	$query_day19 = "SELECT * FROM examregisterlecturer WHERE Day='$day' AND StartTime='19' AND PCode='$id' AND Semester='$sem' AND Faculty='$degree'";
	$daycoz19 = mysql_query($query_day19, $zalongwa) or die(mysql_error());
$rspan19 = mysql_num_rows($daycoz19);
	while($row_course19 = mysql_fetch_array($daycoz19)){
	$course19= $row_course19['CourseCode'];
	$endcourse19= $row_course19['EndTime'];
	$room19= $row_course19['Venue'];
$span19=$endcourse19-19;//Box Ngapi?
}

$bgcol=	"#B9C2C6";																											
?> 
	
	<td class='resViewtd' bgcolor="<?php if($course8!='') $color=$bgcol; echo $color;$color=''; ?>" colspan="<?php echo $span8;$span8=''; ?>"><div align="center"><?php echo $room8 ."<br><br>".$course8; $course8="";  ?></div></td>
	<td class='resViewtd' bgcolor="<?php if($course9!='') $color=$bgcol; echo $color;$color=''; ?>" colspan="<?php echo $span9;$span9=''; ?>"><div align="center"><?php echo $room9 ."<br><br>".$course9; $course9="";  ?></div></td>
	<td class='resViewtd' bgcolor="<?php if($course10!='') $color=$bgcol; echo $color;$color=''; ?>" colspan="<?php echo $span10;$span10=''; ?>"><div align="center"><?php echo $room10 ."<br><br>".$course10; $course10="";  ?></div></td>
	<td class='resViewtd' bgcolor="<?php if($course11!='') $color=$bgcol; echo $color;$color=''; ?>" colspan="<?php echo $span11;$span11=''; ?>"><div align="center"><?php echo $room11 ."<br><br>".$course11; $course11=""; ?></div></td>
	<td class='resViewtd' bgcolor="<?php if($course12!='') $color=$bgcol; echo $color;$color=''; ?>" colspan="<?php echo $span12;$span12=''; ?>"><div align="center"><?php echo $room12 ."<br><br>".$course12; $course12="";?></div></td>
	<td class='resViewtd' bgcolor="<?php if($course13!='') $color=$bgcol; echo $color;$color=''; ?>" colspan="<?php echo $span13;$span13=''; ?>"><div align="center"><?php echo $room13 ."<br><br>".$course13; $course13="";  ?></div></td>
	<td class='resViewtd' bgcolor="<?php if($course14!='') $color=$bgcol; echo $color;$color=''; ?>" colspan="<?php echo $span14;$span14=''; ?>"><div align="center"><?php echo $room14 ."<br><br>".$course14; $course14="";  ?></div></td>
	<td class='resViewtd' bgcolor="<?php if($course15!='') $color=$bgcol; echo $color;$color=''; ?>" colspan="<?php echo $span15;$span15=''; ?>"><div align="center"><?php echo $room15 ."<br><br>".$course15; $course15=""; ?></div></td>
	<td class='resViewtd' bgcolor="<?php if($course16!='') $color=$bgcol; echo $color;$color=''; ?>" colspan="<?php echo $span16;$span16=''; ?>"><div align="center"><?php echo $room16 ."<br><br>".$course16; $course16="";?></div></td>
	<td class='resViewtd' bgcolor="<?php if($course17!='') $color=$bgcol; echo $color;$color=''; ?>" colspan="<?php echo $span17;$span17=''; ?>"><div align="center"><?php echo $room17 ."<br><br>".$course17; $course17=""; ?></div></td>
	<td class='resViewtd' bgcolor="<?php if($course18!='') $color=$bgcol; echo $color;$color=''; ?>" colspan="<?php echo $span18;$span18=''; ?>"><div align="center"><?php echo $room18 ."<br><br>".$course18; $course18=""; ?></div></td>
	<td class='resViewtd' bgcolor="<?php if($course19!='') $color=$bgcol; echo $color;$color=''; ?>" colspan="<?php echo $span19;$span19='';?>"><div align="center"><?php echo $room19 ."<br><br>".$course19; $course19="";?></div></td>
	<!--<td class='resViewtd'><div align="center"><?php echo $room20 ."<br><br>".$course20 ?></div></td> -->
				
				
		    </tr>
<?php }}


 ?>
         </table>
         <?php 
		   } //ends if $total_rows
		}//ends $rowstudent loop

else{
?>
<fieldset><legend> Fill in Properly to get a correct Timetable </legend>
<form name="form1" method="post" action="<?php echo $editFormAction ?>">
            <div align="left">
		<table class='resView' width="200">
                <tr>
<input name="checkdegree" type="hidden" id="checkdegree" value="on">
<input name="checksem" type="hidden" id="checksem" value="on">
<input name="checkyear" type="hidden" id="checkyear" value="on">

                  <!--<td class='resViewhd' nowrap><input name="checkdegree" type="checkbox" id="checkdegree" value="on"></td>-->
                  <td class='resViewhd' nowrap><div align="right">Faculty:</div></td>
                  <td class='resViewtd' colspan='2'>
                      <div align="right">
                        <select name="degree" id="degree">
                          <?php
do {  
?>
                          <option value="<?php echo $row_dept['FacultyName']?>"><?php echo $row_dept['FacultyName']?></option>
                          <?php
} while ($row_dept = mysql_fetch_assoc($dept));
  $rows = mysql_num_rows($dept);
  if($rows > 0) {
      mysql_data_seek($dept, 0);
	  $row_dept = mysql_fetch_assoc($degree);
  }
?>
                        </select>
                    </div></td></tr>
                <tr>
                  <!--<td class='resViewhd'><input name="checkcohot" type="checkbox" id="checkcohot" value="on"></td>-->
                  <td class='resViewhd' nowrap><div align="right">Academic Year: </div></td>
                  <td class='resViewtd' colspan='2'><div align="left">
                    <select name="ayear" id="ayear">
                        <?php
do {  
?>
                        <option value="<?php echo $row_ayear['AYear']?>"><?php echo $row_ayear['AYear']?></option>
                        <?php
} while ($row_ayear = mysql_fetch_assoc($ayear));
  $rows = mysql_num_rows($ayear);
  if($rows > 0) {
      mysql_data_seek($ayear, 0);
	  $row_ayear = mysql_fetch_assoc($ayear);
  }
?>
                    </select>
                  </div></td>
                </tr>
            	<tr>
                 <!-- <td class='resViewhd'><input name="checkyear" type="checkbox" id="checkyear" value="on"></td> -->
                  <td class='resViewhd' nowrap><div align="right">Semester: </div></td>
                  <td class='resViewtd' colspan='2'><div align="left">
                    <select name="sem" id="sem" >
                        <?php
do {  
?>
                        <option value="<?php echo $row_sem['Semester']?>"><?php echo $row_sem['Semester']?></option>
                        <?php
} while ($row_sem = mysql_fetch_assoc($sem));
  $rows = mysql_num_rows($sem);
  if($rows > 0) {
      mysql_data_seek($sem, 0);
	  $$row_sem = mysql_fetch_assoc($sem);
  }
?>
                    </select>
                  </div></td>
                </tr>
            	<tr>
                  <td class='resViewhd' colspan="3" nowrap><div align="center">Paper Size:
					A4
                    <input name="paper" type="radio" value="a4" checked>	
					A3
					<input name="paper" type="radio" value="a3">
                   </div></td> 
                </tr>
            	<tr>
                  <td class='resViewhd' colspan="3" nowrap><div align="center">Layout:
					Landscape
                    <input name="layout" type="radio" value="L" checked>	
					Portrait
					<input name="layout" type="radio" value="P">
                   </div></td> 
                </tr>
                <tr>
                  <td class='resViewhd' colspan="2"><input name="action" type="submit" id="action" value="View Timetable"onmouseover="this.style.background='#DEFEDE'"
onmouseout="this.style.background='lightblue'" style='background-color:lightblue;color:black;font-size:9pt;font-weight:bold' title="Click to View the Timetable"></td>
          		  <td class='resViewhd' nowrap><input type="submit" name="PDF"  id="PDF" value="Print PDF"onmouseover="this.style.background='#DEFEDE'"
onmouseout="this.style.background='lightblue'" style='background-color:lightblue;color:black;font-size:9pt;font-weight:bold' title="Click to Create a PDF File"></td>
              </tr>
              </table>
              <input name="MM_update" type="hidden" id="MM_update" value="form1"onmouseover="this.style.background='#DEFEDE'"
onmouseout="this.style.background='lightblue'" style='background-color:lightblue;color:black;font-size:9pt;font-weight:bold' title="Click to Save the Changes">       
  </div>
</form>
</fieldset>
<?php
}
echo"<br><br>";
//include('../footer/footer.php');
?>
