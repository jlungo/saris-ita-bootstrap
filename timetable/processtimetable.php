	<?php 
	require_once('../Connections/zalongwa.php'); 
	require_once('../Connections/sessioncontrol.php');
	
	$course = $_POST['course'];
	$venue = $_POST['venue'];
	$venuecapacity = trim ($_POST['venuecapa'],")(");
	$coursepacity = trim ($_POST['coursecapa'],")(");
	$start = $_POST["start"];
	$end =$_POST["end"];
	$day= $_POST['day'];
	$lecturer= $_POST['lecturer'];
	$ayear = $_POST['ayear'];
	$programme = $_POST['programme'];
	$type = $_POST['type'];
	$teaching=$_POST['teaching'];
	$id = $_POST['id'];
	$actio = $_POST['action'];
	$klass = trim($_POST['klass']);
	
	$error = FALSE;
	if(empty($course)){
		$error =TRUE;
		echo '<div  style="color:red; margin:0px 0px 0px 20px;">Drag Course first</div>';
		}
	if(empty($venue)){
		$error =TRUE;
		echo '<div  style="color:red; margin:0px 0px 0px 20px;">Drag Venue first</div>';
		}
	if(empty($start)){
		$error =TRUE;
		echo '<div  style="color:red; margin:0px 0px 0px 20px;">Select start time</div>';
		}
	if(empty($end)){
		$error =TRUE;
		echo '<div  style="color:red; margin:0px 0px 0px 20px;">Select End time </div>';
		}
	if(empty($day)){
		$error =TRUE;
		echo '<div  style="color:red; margin:0px 0px 0px 20px;">Select Day please</div>';
		}
	
	if(empty($teaching)){
		$error =TRUE;
		echo '<div  style="color:red; margin:0px 0px 0px 20px;">Select Teaching type</div>';
		}
	
	if($coursepacity > $venuecapacity){
		//$error =TRUE;
		//echo '<div  style="color:red; margin:0px 0px 0px 20px;">Venue Capacity is not enough to accomodate total number of student in that course</div>';
		}
	
	if($start > $end || ($start == $end)){
		$error =TRUE;
		echo '<div  style="color:red; margin:0px 0px 0px 20px;">Start time should be less that end time</div>';
		}
	
	
	//class stream category
	if(empty($klass)){ 
		$klas = "";
		}
	else{
		$klas = " class='$klass' OR class!='$klass'";
		}
	
	//SAVING TIMETABLE
	if($id == 0 && $actio =="SAVE"){
		
		$get_year_of_study ="SELECT * FROM courseprogramme WHERE ProgrammeID='$programme' AND AYear='$ayear' AND CourseCode='$course'";
		$class=0;
		
		$result = mysqli_query($zalongwa, $get_year_of_study);
		$found = mysqli_num_rows($result);
		
		if($found == 1){
			$ftchclass= mysqli_fetch_array($result);
			$class = $ftchclass['YearofStudy'];
			} 

		// check if venue is free at that time 
		if($error ==FALSE){
		
			//check if the venue is still in use
			$sql_check = "SELECT * FROM timetable WHERE end<='$end' AND venue='$venue' AND day='$day' AND timetable_category='$type' AND AYear='$ayear' AND CourseCode !='$course'";
			$result = mysqli_query($zalongwa, $sql_check);
			$found = mysqli_num_rows($result);
			
			if($found > 0 || $found2 > 0){
				$test = TRUE;
				
				while($ftch = mysqli_fetch_array($result)){
					$strt_time = $ftch['start'];
					$endtest_time = $ftch['end'];
					
					if($strt_time < $start && $endtest_time > $start){
						$test = false;
						break;
						}
					}
		
				if($test == FALSE){
					$error =TRUE;
					echo '<div  style="color:red; margin:0px 0px 0px 20px;">Venue is occupied in that time</div>';
					}
				}
	
			
			//check if the venue is still in use
			$sql_check = "SELECT * FROM timetable WHERE start>='$start' AND end <='$end' AND venue='$venue' AND day='$day' AND timetable_category='$type' AND AYear='$ayear' AND lecturer='$lecturer' AND ProgrammeID !='$programme'";
			$result = mysqli_query($zalongwa, $sql_check);
			$found = mysqli_num_rows($result);
			
			$sql_check2 = "SELECT * FROM timetable WHERE start<='$start' AND end>='$end' AND venue='$venue' AND day='$day' AND timetable_category='$type' AND AYear='$ayear' AND lecturer!='$lecturer'";
			$result2 = mysqli_query($zalongwa, $sql_check2);
			$found2 = mysqli_num_rows($result2);
			
			if($found > 0 || $found2>0){
				$error =TRUE;
				echo '<div  style="color:red; margin:0px 0px 0px 20px;">Venue is occupied in that time</div>';
				}
			
			//check if the venue is occupied in later time
			$sql_check = "SELECT * FROM timetable WHERE start>='$start' AND end <='$end' AND venue='$venue' AND day='$day' AND timetable_category='$type' AND AYear='$ayear' AND CourseCode !='$course'";
			$result = mysqli_query($zalongwa, $sql_check);
			$found = mysqli_num_rows($result);
						
			if($found > 0){
				$error = TRUE;
				echo '<div  style="color:red; margin:0px 0px 0px 20px;">Venue will be occupied in that time</div>';
				}

			//check if the programme has a different session at the same time
			$sql_check = "SELECT * FROM timetable WHERE (start>='$start' AND end <='$end' AND Programme='$programme' AND day='$day' AND timetable_category='$type' AND AYear='$ayear' AND YoS='$class') AND ($klas)";
			$result = mysqli_query($zalongwa, $sql_check);
			$found = mysqli_num_rows($result);
			
			$sql_check2 = "SELECT * FROM timetable WHERE start>='$start' AND end <='$end' AND day='$day' AND timetable_category='$type' AND AYear='$ayear' AND lecturer !='$lecturer'";
			$result2 = mysqli_query($zalongwa, $sql_check);
			$found2 = mysqli_num_rows($result);
			
			//check if the venue is occupied
			if($found > 0 || $found2 > 0){
				$error = TRUE;
				$cl ='';
				
				$studyClass = array(1=>'FIRST YEAR',2=>'SECOND YEAR',3=>'THIRD YEAR',4=>'FOURTH YEAR',5=>'FIFTH YEAR',6=>'SIXTH YEAR');
				
				$cl = (empty($klass))? $studyClass[$class]:$studyClass[$class].' of '.$klass;
				$cl .=' students have another Lecture in that Time';
				echo '<div  style="color:red; margin:0px 0px 0px 20px;">'.$cl.'</div>';
				}
			
			
			//check if start time is in the middle of another class
			$sql_check = "SELECT * FROM timetable WHERE  (end <='$end' AND Programme='$programme' AND day='$day' AND timetable_category='$type' AND AYear='$ayear' AND YoS='$class') AND ($klas)";
			$result = mysqli_query($zalongwa, $sql_check);
			$found = mysqli_num_rows($result);
			
			if($found > 0){
				$test = TRUE;
			
				while($ftch = mysqli_fetch_array($result)){
					$strt_time = $ftch['start'];
					$endtest_time = $ftch['end'];

					if($strt_time < $start && $endtest_time > $start){
						$test = false;
						break;
						}
					}
	
				if($test == FALSE){
					$error =TRUE;
					$cl ='';
					$studyClass = array(1=>'FIRST YEAR',2=>'SECOND YEAR',3=>'THIRD YEAR',4=>'FOURTH YEAR',5=>'FIFTH YEAR',6=>'SIXTH YEAR');
					$cl = (empty($klass))? $studyClass[$class]:$studyClass[$class].' of '.$klass;
					$cl .=' student has another Lecture in that Time';
					echo '<div  style="color:red; margin:0px 0px 0px 20px;">'.$cl.'</div>';
					}
				}


			//check if lecturer is allocated a different class to teach at the same time
			if($error == FALSE){
				$sql_check = "SELECT * FROM timetable WHERE start>='$start' AND end<='$end' AND timetable_category='$type' AND day='$day' AND  AYear='$ayear' AND lecturer='$lecturer' AND venue != '$venue'";
				$result = mysqli_query($zalongwa, $sql_check);
				$found = mysqli_num_rows($result);	
				
				if($found > 0){
					$error = TRUE;
					echo '<div  style="color:red; margin:0px 0px 0px 20px;">Lecturer has the Period in that time</div>';
					}
				
				if($found > 0){
					$test = TRUE;
				
					while($ftch = mysqli_fetch_array($result)){
						$strt_time = $ftch['start'];
						$endtest_time = $ftch['end'];

						if($strt_time < $start && $endtest_time > $start){
							$test = false;
							break;
							}
						}
	
					if($test == FALSE){
						$error =TRUE;
						echo '<div  style="color:red; margin:0px 0px 0px 20px;">Lecturer has the Period in that time</div>';
						}	
					}
	
				//check if the student has a different class at the same time
				//carryover students
				$sem = '';
				
				if($type == 1 || $type==3){
					$sem .=" AND Semester ='1'";
					}
				elseif($type == 2 || $type==4){
					$sem .=" AND Semester ='2'";
					}
	
				//get year of the CourseCode
				$sql_check = "SELECT e.CourseCode,s.RegNo FROM examregister e, profile_db.security s
								WHERE e.AYear=AYear='$ayear' $sem AND 
								(
								(e.RegNo=s.RegNo AND s.Position='Student') 
								OR (e.RegNo=s.Username AND s.Position='Student')
								)";
				$result = mysqli_query($zalongwa, $sql_check);
				$num = mysqli_num_rows($result);
				
				if($num > 0){
					$error = FALSE;
					while(list($co_code,$co_regno) = mysqli_fetch_array($result)){
						$sql_check = "SELECT * FROM timetable WHERE (start>='$start' AND end<='$end' AND AYear='$ayear' AND CourseCode='$co_code' AND day='$day' AND Programme='$programme') AND ($klas)";
						$res = mysqli_query($zalongwa, $sql_check);
						$num = mysqli_num_rows($res);
						
						$sql_check2 = "SELECT * FROM timetable WHERE (end>='$start' AND AYear='$ayear' AND CourseCode='$co_code' AND day='$day' AND Programme='$programme') AND ($klas)";
						$res2 = mysqli_query($zalongwa, $sql_check2);
						$num2 = mysqli_num_rows($res2);
						
						if($num > 0 || $num2 > 0){
							$error = TRUE;
							echo '<div  style="color:red; margin:0px 0px 0px 20px;">Some carryover students have class this time</div>';
							exit;
							}
						}
					}
	
				if($error == TRUE){
					exit;
					}
				
				//alert if merging the venue
				$sql_check = "SELECT * FROM timetable WHERE start>='$start' AND end<='$end' AND timetable_category='$type' AND day='$day' AND  AYear='$ayear' AND lecturer='$lecturer' AND venue='$venue' AND CourseCode='$course' AND Programme !='$programme'";
				$result = mysqli_query($zalongwa, $sql_check);
				$found = mysqli_num_rows($result);
				if($found > 0){
					?>
					<script type="text/javascript">
						collision();
					</script>
					<?php
					}
				if($error == FALSE){
					$interval = $end - $start;
					$sql_insert = "INSERT INTO timetable
									(AYear,Programme,timetable_category,CourseCode,
									start,end,venue,lecturer,Recorder,start_end,day,teachingtype,
									YoS,published,class) 
									VALUE
									('$ayear','$programme','$type','$course','$start','$end',
									'$venue','$lecturer','$username','$interval','$day','$teaching',
									'$class',DEFAULT,'$klass')";

				
					$insert = mysqli_query($zalongwa, $sql_insert) or die(mysql_error());
					if($insert){
						echo '<div  style="color:green; margin:0px 0px 0px 20px;">Data successfully saved, CourseCode:'.$course.'</div>';
						}
					else{
						echo '<div  style="color:red; margin:0px 0px 0px 20px;">Data failed to save!</div>';
						}
					}
				}
			}
		}
	
	//EDITING TIMETABLE	
	elseif($id > 0 && $actio =="EDIT"){ 
		// start editing from here
		$get_year_of_study ="SELECT * FROM courseprogramme WHERE ProgrammeID='$programme' AND AYear='$ayear' AND CourseCode='$course'";
		$class=0;
		$result = mysqli_query($zalongwa, $get_year_of_study);
		$found = mysqli_num_rows($result);
		
		if($found == 1){
			$ftchclass= mysqli_fetch_array($result);
			$class = $ftchclass['YearofStudy'];
			}
		
		$sql_check = "SELECT * FROM timetable WHERE (end <='$end' AND venue='$venue' id !='$id' AND day='$day' AND timetable_category='$type' AND AYear='$ayear') AND ($klas)"; 
		$result = mysqli_query($zalongwa, $sql_check);
		$found = mysqli_num_rows($result);
		
		if($found > 0){
			$test = TRUE;
		
			while($ftch = mysqli_fetch_array($result)){
				$strt_time = $ftch['start'];
				$endtest_time = $ftch['end'];

				if($strt_time < $start && $endtest_time > $start){
					$test = false;
					break;
					}
				}
		
			if($test == FALSE){
				$error =TRUE;
				echo '<div  style="color:red; margin:0px 0px 0px 20px;">Venue occupied in that time</div>';
				}
			}
	
		// check if venue is free at that time
		if($error ==FALSE){
			$sql_check = "SELECT * FROM timetable WHERE start>='$start' AND end <='$end' AND id !='$id' AND venue='$venue' AND day='$day' AND timetable_category='$type' AND AYear='$ayear'";
			$result = mysqli_query($zalongwa, $sql_check);
			$found = mysqli_num_rows($result);
			
			// check if the venue is in use
			if($found > 0){
				$error = TRUE;
				echo '<div  style="color:red; margin:0px 0px 0px 20px;">Venue occupied in that time</div>';
				}
		
			//check if lecturer is allocated a different class at the same time
			if($error == FALSE){
				$sql_check = "SELECT * FROM timetable WHERE start>='$start' AND end <='$end' AND id !='$id' AND timetable_category='$type' AND day='$day' AND  AYear='$ayear' AND lecturer='$lecturer'";
				$result = mysqli_query($zalongwa, $sql_check);
				$found = mysqli_num_rows($result);
					
				if($found > 0){
					$error = TRUE;
					echo '<div  style="color:red; margin:0px 0px 0px 20px;">Lecturer has the Period in that time</div>';
					}
	
				$sql_check = "SELECT * FROM timetable WHERE end<='$end' AND timetable_category='$type' AND id !='$id' AND day='$day' AND  AYear='$ayear' AND lecturer='$lecturer'";
				$result = mysqli_query($zalongwa, $sql_check);
				$found = @mysqli_num_rows($result);
		
				if($found > 0){
					$test = TRUE;
					
					while($ftch = mysqli_fetch_array($result)){
						$strt_time = $ftch['start'];
						$endtest_time = $ftch['end'];

						if($strt_time < $start && $endtest_time > $start){
							$test = false;
							break;
							}
						}
				
					if($test == FALSE){
						$error =TRUE;
						echo '<div  style="color:red; margin:0px 0px 0px 20px;">Lecturer has the Period in that time</div>';
						}	
					}
				
				//check if the venue is still in use
				$sql_check = "SELECT * FROM timetable WHERE start>='$start' AND end <='$end' AND id !='$id' AND venue='$venue' AND day='$day' AND timetable_category='$type' AND AYear='$ayear' AND lecturer!='$lecturer'";
				$result = mysqli_query($zalongwa, $sql_check);
				$found = mysqli_num_rows($result);
				
				if($found > 0){
					$test = TRUE;
					
					while($ftch = mysqli_fetch_array($result)){
						$strt_time = $ftch['start'];
						$endtest_time = $ftch['end'];
						
						if($strt_time < $start && $endtest_time > $start){
							$test = false;
							break;
							}
						}
			
					if($test == FALSE){
						$error =TRUE;
						echo '<div  style="color:red; margin:0px 0px 0px 20px;">Venue is occupied in that time</div>';
						}
					}
				
				//check if the student has a different class at the same time
				//carryover students
				$sem = '';
				
				if($type == 1 || $type==3){
					$sem .=" AND Semester ='1'";
					}
				elseif($type == 2 || $type==4){
					$sem .=" AND Semester ='2'";
					}
	
				//get year of the CourseCode
				$sql_check = "SELECT e.CourseCode,s.RegNo FROM examregister e, profile_db.security s
								WHERE e.AYear=AYear='$ayear' $sem AND 
								((e.RegNo=s.RegNo AND s.Position='Student') 
								OR (e.RegNo=s.Username AND s.Position='Student'))";
				$result = mysqli_query($zalongwa, $sql_check);
				$num = mysqli_num_rows($get_course);
				
				if($num > 0){
					$error = FALSE;
					while(list($co_code,$co_regno) = mysqli_fetch_array($result)){
						$sql_check = "SELECT * FROM timetable WHERE (start>='$start' AND end<='$end' AND AYear='$ayear' AND CourseCode='$co_code' AND day='$day' AND Programme='$programme') AND ($klas)";
						$res = mysqli_query($zalongwa, $sql_check);
						$num = mysqli_num_rows($res);
						
						$sql_check2 = "SELECT * FROM timetable WHERE (end>='$start' AND AYear='$ayear' AND CourseCode='$co_code' AND day='$day' AND Programme='$programme') AND ($klas)";
						$res2 = mysqli_query($zalongwa, $sql_check2);
						$num2 = mysqli_num_rows($res2);
						
						if($num > 0 || $num2 > 0){
							$error = TRUE;
							echo '<div  style="color:red; margin:0px 0px 0px 20px;">Some carryover students have class this time</div>';
							exit;
							}
						}
					}
				
				if($error == TRUE){
					exit;
					}
				
				// haijakaa vizuri
				
				if($error == FALSE){
					//check for merged courses
					$sql_check = "SELECT id, Programme FROM timetable WHERE start<='$start' AND end>='$end' AND venue='$venue' AND day='$day' AND timetable_category='$type' AND AYear='$ayear' AND CourseCode='$course'";
					$result = mysqli_query($zalongwa, $sql_check);
					$found = mysqli_num_rows($result);
					
					$update = 0;
					$output = $course;
					
					if($found>1){
						while(list($ID,$programme) = mysqli_fetch_array($result)){
							$sql_update = "UPDATE timetable SET lecturer='$lecturer' WHERE id='$ID'";
							$update = mysqli_query($zalongwa, $sql_update);
							
							$output .= ($output==$course)? ' for<br/>'.$programme:', '.$programme;
							}
						}
					else{
						$interval = $end - $start;
						$sql_update = "UPDATE timetable SET CourseCode = '$course', start ='$start',end='$end',venue='$venue',lecturer='$lecturer',Recorder='$username',start_end='$interval',day='$day', teachingtype='$teaching', YoS='$class', class='$klass' WHERE id='$id'";
						$update = mysqli_query($zalongwa, $sql_update);
						}					
					
					if($update){
						echo '<div  style="color:green; margin:0px 0px 0px 20px;">Data successfully Updated, CourseCode:'.$output.'</div>';
						}
					else{
						echo '<div  style="color:red; margin:0px 0px 0px 20px;">Fail to update!!!!</div>';
						}
					}
				}
			}
		}
	
	?>
	
	<script type="text/javascript">
		function collision(){
			var coll = confirm("Are you sure you want to merge classes for the module and lecturer?");
			if(coll){
				return true;
				}
			else{
				return false;
				}
			}				
	</script>
