/**
 * function showComponent()
 * ajax function to display drop menu on selected value
 * author Lackson David <lacksinho@gmail.com>
 * written on Monday,June 10 2013
 * */

function Ajax(){
	if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
   return xmlhttp;
	}
	
function showExamDate(str)
{
if (str=="")
  {
  document.getElementById("examDate").innerHTML="";
  return;
  } 
   var xmlhttp= Ajax();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("examDate").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","showDateBox.php?q="+str,true);
xmlhttp.send(null);
}


