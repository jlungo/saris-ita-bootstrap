<?php /*
	This is the header to be included for all files.
	Variables to be set before including this file are as follows:
	
	szSection - the name of the section.
	szSubSection - the name of the subsection.
*/ 
	
	# this script will use the following globals
	global $szSection, $szSubSection, $szSubSubSection,$szSubSubSectionTitle,$szSubSectionTitle, $szTitle, $additionalStyleSheet, $arrStructure, $szRootURL, $blnHideNav, $arrVariations;

	//if (!isset($blnHideNav)){$blnHideNav = false;}
	if (isset($_GET['hidenav'])){$blnHideNav=true;}else{$blnHideNav = false;}
	
	// change language if necessary...
	if (isset($_GET['chooselang']) && isset($arrVariations[$_GET['chooselang']])){
		if ($_GET['chooselang'] ==1){
			$_SESSION['arrVariationPreference'] = array (
				1 => 1,
				2 => 2
			);
		}elseif ($_GET['chooselang'] ==2){
			$_SESSION['arrVariationPreference'] = array (
				1 => 2,
				2 => 1
			);
		}
	}
	
	$intDisplayLanguage = $_SESSION['arrVariationPreference'][1];
	
	// Need to figure out what section we are currently in, to be able to display the relevant colours.
	for ( $i=1; $i<= count( $arrStructure ); $i++ ){	
		# is this the current section?
		if ( $arrStructure[$i]['name1'] == $szSection ) $intCurrentSectionID = $i;
	}
	#get organisation name
	
	$qname = 'SELECT Name, Address FROM organisation';
	$dbname = mysql_query($qname);
	$name_row = mysql_fetch_assoc($dbname);
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>SARIS -> <?php echo  $szSection ?> -> <?php echo  $szSubSection ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="../login_files/Generic.css" type=text/css rel=stylesheet>
<link rel="stylesheet" type="text/css" href="../css/header.css">
<link rel="stylesheet" type="text/css" href="../css/mucostyle.css">
<link href="../template/styleInLonglist.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="central"><!-- ********Whole Boby********** -->

<!-- ********Start Banner body**********  -->

<div id="header"></div>
	<ul id="navtop">
		<!--<li><a id="nt1" href="">About SARIS</a></li>
		<li><a id="nt2" href="">FAQ</a></li>
		<li><a id="nt3" href="">News</a></li>
		<li><a id="nt4" href="">Contact us</a></li>
		<li><a id="nt5" href="">Help</a></li>
		<li><a id="nt6" href=""></a></li>-->
		
		
	</ul>
	
	<div id="searchspacer"> </div>
		<div id="searchbox">
			<div id="logo"></div>
		<!--<form action="http://www.makumira.ac.tz" method="post">
		<span>search</span> 
		<input id="search" name="searchterm" type="text" size="15" /><input name="go" type="image" id="go" value="Search" src="images/searchbutton.gif" />
		</form> -->
	</div> 
	<div id="imageheader"></div>

	<ul id="navleft">
		<!--<li><a id="nr1" href="http://www.freewebsitetemplates.com">Register</a></li>
		<li><a id="nr2" href="http://www.freewebsitetemplates.com">Reports</a></li>
		<li><a id="nr3" href="saris/index.php">&nbsp SARIS Old</a></li>-->
		<li id="nr5"><?php echo $name ?></li>
</ul>


<!-- ************ End Banner Contents ***********   -->
<div id="contentbox"> <!-- ********************* Create Content body ******************************   -->
  
<div id="nav">	<!-- *********Right Navigation Links********* -->	
      <table cellspacing="0" cellpadding="0" border="0"width="100%" >
		  <tr>
		      <td align="left" style="padding-left: 1px;" valign=top>
			
			<div class="EnclosureBox">
			<?php # loop around and write out the Sections
				for ( $i=1; $i<= count( $arrStructure ); $i++ ){	
					
					# is this the current section?
					if ( $arrStructure[$i]['name1'] == $szSection ) $intSectionID = $i;
					
					# only output parent sections
					
					if ( $arrStructure[$i]['parentID'] == 0){
					
						# Section Enclosure
						echo '<div class="SectionEnclosureBox">';
						
						# is this the current section
						if ( $i == $intSectionID ) { 
							# Section On
						echo '<div class="SectionOnBox"><a class="NavigationLink" href="'.$arrStructure[$i]['url'].'">'.$arrStructure[$i]['name'.$intDisplayLanguage].'</a></div>';
							
							# Subsections
							
							# loop around and write out the sub-sections
							$arrSubStructure = $arrStructure[$intSectionID]['subsections'];
							for ($j=1; $j<= count($arrSubStructure); $j++){	
							
								# Sub Section Enclosure
								echo '<div class="SubSectionEnclosureBox">';
								
								# is this the current subsection?
								if ( $arrSubStructure[$j]['name1'] == $szSubSection ) $intSubSectionID = $j;
								
								# is this a section page?
								
								# is this the current subsection
								if ( $j == $intSubSectionID ) { 
									# SubSection On
									echo '<div class="SubSectionOnBox"><a class="NavigationLink" href="'.$arrSubStructure[$j]['url'].'">'.$arrSubStructure[$j]['name'.$intDisplayLanguage].'</a></div>';
									
									# loop around and write out the sub-sections
									$arrSubSubStructure = $arrStructure[$intSectionID]['subsections'][$intSubSectionID]['subsections'];
									
								for ($k=1; $k<= count($arrSubSubStructure); $k++){	
		
										# is this the current subsubsection?
										if ( $arrSubSubStructure[$k]['name1'] == $szSubSubSection ) $intSubSubSectionID = $k;
										
										# is this the current subsubsection
										if ( $k == $intSubSubSectionID ) { 
											// ON
											echo '<div class="SubSubSectionOnBox">'.$arrSubSubStructure[$k]['name'.$intDisplayLanguage].'</div>';
										}else{
											echo '<div class="SubSubSectionOffBox"><a class="NavigationLink" href="'.$arrSubSubStructure[$k]['url'].'">'.$arrSubSubStructure[$k]['name'.$intDisplayLanguage].'</a></div>';
										}
										
									}
								}else{
									# subsection is off
									echo '<div class="SubSectionOffBox"><a class="NavigationLink" href="'.$arrSubStructure[$j]['url'].'">'.$arrSubStructure[$j]['name'.$intDisplayLanguage].'</a></div>';
								}
								
								# End of Sub Section Enclosure
								echo '</div>';
								
							}
						}else{
							# section is off
							echo '<div class="SectionOffBox"><a class="NavigationLink" href="'.$arrStructure[$i]['url'].'">'.$arrStructure[$i]['name'.$intDisplayLanguage].'</a></div>';
							//echo '<tr><td style="padding-left: 10px;" height="20" align="left">&nbsp;<a class="navoff"href="'.$arrStructure[$i]['url'].'">'.$arrStructure[$i]['name'.$intDisplayLanguage].'</a>&nbsp;</td></tr>';
						}
						
						# End of Section Enclosure
						echo '</div>';
					}
				}
				
			?>
			</div>
			<div>
			&nbsp;
			</div>
			<?php if (isset($arrSpecialStructure)){?>
				<div class="EnclosureBox">
				<?php # loop around and write out the Sections
					for ( $i=1; $i<= count( $arrSpecialStructure ); $i++ ){	
						
						# only output parent sections
						if ( $arrSpecialStructure[$i]['parentID'] == 0){
						
							# Section Enclosure
							echo '<div class="SectionEnclosureBox">';
							
							# section is off
							echo '<div class="SpecialSectionBox"><a class="NavigationLink" href="'.$arrSpecialStructure[$i]['url'].'">'.$arrSpecialStructure[$i]['name'.$intDisplayLanguage].'</a></div>';

							
							# End of Section Enclosure
							echo '</div>';
						}
					}
					
				?>
				</div>
				<div>
				&nbsp;
				</div>
			<?php } ?>
			</td>
		</tr>
	</table></div><!-- ********** Close navRight ***********   -->

 <div id="content"><!--**************** Start Content part ******************   -->

		<?php if (strlen($szTitle)){
				echo '<h1>'.$szTitle.'</h1><hr noshade style="color:#B9C2C6;margin-left:20px;" size=1 >';
			}elseif (strlen($szSubSubSectionTitle)){
				echo '<h1>'.$szSubSubSectionTitle.'</h1><hr noshade style="color:#B9C2C6;margin-left:20px;" size=1>';
			}elseif (strlen($szSubSectionTitle)){
				echo '<h1>'.$szSubSectionTitle.'</h1><hr noshade style="color:#B9C2C6;;margin-left:20px; " size=1>';
			}elseif (strlen($szSectionTitle)){
				echo '<h1>'.$szSectionTitle.'</h1><hr noshade style="color:#B9C2C6;;margin-left:20px;" size=1>';
			} ?>





