<?php 
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('lecturerMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Timetable';
	$szSubSection = 'Course Allocation';
	$szTitle = 'Lecturer Course Allocation';
	include('lecturerheader.php');
?>

<script type='text/javascript'>
function confirmAllocate( data)
{
if(confirm("Are you sure you want to Allocate "+data))
{
return true;
}else
{
return false;
}
}
</script>

<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

//check if is Faculty examination officer
$query_userfac = "SELECT Faculty FROM security where UserName = '$username' AND Dept=0";
$userfac = mysql_query($query_userfac, $zalongwa) or die(mysql_error());
$row_userfac = mysql_fetch_assoc($userfac);
$totalRows_userfac = mysql_num_rows($userfac);
$fac = $row_userfac["Faculty"];
//Get faculty name
$query_userfac1 = "SELECT FacultyName FROM faculty where FacultyID = '$fac'";
$userfac1 = mysql_query($query_userfac1, $zalongwa) or die(mysql_error());
$row_userfac1 = mysql_fetch_assoc($userfac1);
$totalRows_userfac1 = mysql_num_rows($userfac1);
$fac1 = $row_userfac1["FacultyName"];

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
$pcode=$_POST['day'].$_POST['starttime'].$_POST['lecturer'];

$plecturer=$_POST['lecturer'];
$pcourse=$_POST['course'];
$pstarttime=$_POST['starttime'];
$pendtime=$_POST['endtime'];
$pday=$_POST['day'];
$proom=$_POST['room'];

//Check Time Collision
mysql_select_db($database_zalongwa, $zalongwa);
$query_Code = "SELECT * FROM examregisterlecturer where PCode='$pcode'";
$pcodetake = mysql_query($query_Code, $zalongwa) or die(mysql_error());
$totalRows_pcode = mysql_num_rows($pcodetake);
$pcozread = mysql_fetch_array($pcodetake);
$pcoz=$pcozread['CourseCode'];

//Check if the teacher is in the Class at that time
mysql_select_db($database_zalongwa, $zalongwa);
$query_plength = "SELECT * FROM examregisterlecturer where RegNo='$plecturer' AND Day='$pday'";
$pEndfind = mysql_query($query_plength, $zalongwa) or die(mysql_error());
while($row_Endtime = mysql_fetch_array($pEndfind)){
$end=$row_Endtime['EndTime'];
$start=$row_Endtime['StartTime'];
if(($pstarttime < $end) && ($pstarttime >= $start)){//rekebisha condition
$gotp=1;
}
elseif(($pstarttime < $start1) && ($pendtime <= $end1) && ($pendtime > $start1)){
$gotp=1;
}
}

//Check if the same Course is in the Class at that time
mysql_select_db($database_zalongwa, $zalongwa);
$query_coz = "SELECT * FROM examregisterlecturer where CourseCode='$pcourse' AND Day='$pday'";
$cozfind = mysql_query($query_coz, $zalongwa) or die(mysql_error());
while($row_cozin = mysql_fetch_array($cozfind)){
$end1=$row_cozin['EndTime'];
$start1=$row_cozin['StartTime'];
if(($pstarttime < $start1) && ($pendtime <= $end1) && ($pendtime > $start1)){
$gotcoz=1;
}
elseif(($pstarttime < $end) && ($pstarttime >= $start)){
$gotcoz=1;
}
}

//Check if the same Room at the same time
mysql_select_db($database_zalongwa, $zalongwa);
$query_room = "SELECT * FROM examregisterlecturer where Room ='$proom' AND Day='$pday'";
$roomfind = mysql_query($query_room, $zalongwa) or die(mysql_error());
while($row_room = mysql_fetch_array($roomfind)){
$start2=$row_room['StartTime'];
$end2=$row_room['EndTime'];
if(($pstarttime < $end2) && ($pstarttime >= $start2)){// rekebisha condition
$gotroom=1;
}
elseif(($pstarttime < $start2) && ($pendtime <= $end2) && ($pendtime > $start2)){
$gotroom=1;
}
}

//Check if the End time > Start time at the same time
if(($pstarttime >= $pendtime)){
$gotendt=1;
}


  $insertSQL = sprintf("INSERT INTO examregisterlecturer (PCode,RegNo, CourseCode, AYear, Semester, Recorder, Faculty, Day, StartTime, Room, EndTime) VALUES ('$pcode', %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['lecturer'], "text"),
                       GetSQLValueString($_POST['course'], "text"),
                       GetSQLValueString($_POST['Ayear'], "text"),
                       GetSQLValueString($_POST['semester'], "text"),
		       GetSQLValueString($_POST['user'], "text"),
			  GetSQLValueString($fac1, "text"),
			GetSQLValueString($_POST['day'], "text"),
			GetSQLValueString($_POST['starttime'], "text"),
			GetSQLValueString($_POST['room'], "text"),
			GetSQLValueString($_POST['endtime'], "text"));

/****************Check Collisions**********************/
if($totalRows_pcode ==0) //Not found the same start time for the same lecturer
{

if($gotp==1)
{
echo"<br><font color='red'> The teacher will be in the class till  ", $end,":00! <br> Check Time Please!</font>";  
}
else if($gotcoz==1)
{
echo"<br><font color='red'> The Course ",$pcourse," already allocated to another teacher at the same time! <br> Check Time Please!</font>";
}
else if($gotroom==1)
{
echo"<br><font color='red'> The Room ",$proom," will be in use till ".$end2.":00 from ".$start2.":00! <br> Check Time Please!</font>";
}
else if($gotendt==1)
{
echo"<br><font color='red'> The Period End Time ",$pendtime,":00 can not be less than OR equal to Start Time ".$pstarttime.":00! <br> Check Time Please!</font>";
}
else{
mysql_select_db($database_zalongwa, $zalongwa);
  $Result1 = mysql_query($insertSQL, $zalongwa) or die(mysql_error());
} 

}else{
echo" <br><font color='red'> The teacher will be Starting  ", $pcoz ," at this time!<br> Check Time Please! </font>";
}

}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_ExamOfficerGradeBook = 20;
$pageNum_ExamOfficerGradeBook = 0;
if (isset($_GET['pageNum_ExamOfficerGradeBook'])) {
  $pageNum_ExamOfficerGradeBook = $_GET['pageNum_ExamOfficerGradeBook'];
}
$startRow_ExamOfficerGradeBook = $pageNum_ExamOfficerGradeBook * $maxRows_ExamOfficerGradeBook;

mysql_select_db($database_zalongwa, $zalongwa);
$query_ExamOfficerGradeBook = "SELECT CourseCode, AYear, Semester FROM examregisterlecturer";
$query_limit_ExamOfficerGradeBook = sprintf("%s LIMIT %d, %d", $query_ExamOfficerGradeBook, $startRow_ExamOfficerGradeBook, $maxRows_ExamOfficerGradeBook);
$ExamOfficerGradeBook = mysql_query($query_limit_ExamOfficerGradeBook, $zalongwa) or die(mysql_error());
$row_ExamOfficerGradeBook = mysql_fetch_assoc($ExamOfficerGradeBook);

if (isset($_GET['totalRows_ExamOfficerGradeBook'])) {
  $totalRows_ExamOfficerGradeBook = $_GET['totalRows_ExamOfficerGradeBook'];
} else {
  $all_ExamOfficerGradeBook = mysql_query($query_ExamOfficerGradeBook);
  $totalRows_ExamOfficerGradeBook = mysql_num_rows($all_ExamOfficerGradeBook);
}
$totalPages_ExamOfficerGradeBook = ceil($totalRows_ExamOfficerGradeBook/$maxRows_ExamOfficerGradeBook)-1;

mysql_select_db($database_zalongwa, $zalongwa);
$query_Ayear = "SELECT AYear FROM academicyear where Status='1'";
$Ayear = mysql_query($query_Ayear, $zalongwa) or die(mysql_error());
$row_Ayear = mysql_fetch_assoc($Ayear);
$mwaka=$row_Ayear['AYear'];
$totalRows_Ayear = mysql_num_rows($Ayear);

mysql_select_db($database_zalongwa, $zalongwa);
$query_semester = "SELECT Description FROM terms ORDER BY Semester ASC";
$semester = mysql_query($query_semester, $zalongwa) or die(mysql_error());
$row_semester = mysql_fetch_assoc($semester);
$totalRows_semester = mysql_num_rows($semester);

mysql_select_db($database_zalongwa, $zalongwa);
$query_course = "SELECT CourseCode, CourseName FROM course where Faculty='$fac' ORDER BY CourseCode ASC";
$course = mysql_query($query_course, $zalongwa) or die(mysql_error());
$row_course = mysql_fetch_assoc($course);
$totalRows_course = mysql_num_rows($course);

mysql_select_db($database_zalongwa, $zalongwa);
$query_venue = "SELECT VenueCode FROM venue ORDER BY VenueCode ASC";
$venue = mysql_query($query_venue, $zalongwa) or die(mysql_error());
$row_venue = mysql_fetch_assoc($venue);
$totalRows_venue = mysql_num_rows($venue);

mysql_select_db($database_zalongwa, $zalongwa);
$query_lecturer = "SELECT UserName, FullName, Position FROM security WHERE Position = 'Lecturer' ORDER BY FullName";
$lecturer = mysql_query($query_lecturer, $zalongwa) or die(mysql_error());
$row_lecturer = mysql_fetch_assoc($lecturer);
$totalRows_lecturer = mysql_num_rows($lecturer);

if ((isset($_GET['RegNo'])) && ($_GET['RegNo'] != "")) {
$regno=$_GET['RegNo'];
$ayear=$_GET['ayear'];
$key=$_GET['key'];

mysql_select_db($database_zalongwa, $zalongwa);
$get1 = "SELECT security.UserName, 
security.FullName,                 
security.Position,                 
examregisterlecturer.RegNo,                 
examregisterlecturer.AYear,                 
examregisterlecturer.Semester,                 
examregisterlecturer.CourseCode,
examregisterlecturer.StartTime,
examregisterlecturer.EndTime,
examregisterlecturer.Day,
examregisterlecturer.Room  
FROM examregisterlecturer    
	INNER JOIN security ON (examregisterlecturer.RegNo = security.UserName)where examregisterlecturer.RegNo='$regno' AND examregisterlecturer.AYear = '$ayear'AND examregisterlecturer.CourseCode = '$key'
	 ORDER BY  examregisterlecturer.AYear DESC, examregisterlecturer.Day ASC";



}


$maxRows_courseallocation = 50;
$pageNum_courseallocation = 0;
if (isset($_GET['pageNum_courseallocation'])) {
  $pageNum_courseallocation = $_GET['pageNum_courseallocation'];
}
$startRow_courseallocation = $pageNum_courseallocation * $maxRows_courseallocation;

mysql_select_db($database_zalongwa, $zalongwa);
$query_courseallocation = "SELECT security.UserName, 
security.FullName,                 
security.Position,                 
examregisterlecturer.RegNo,                 
examregisterlecturer.AYear,                 
examregisterlecturer.Semester,                 
examregisterlecturer.CourseCode,
examregisterlecturer.Room  
FROM examregisterlecturer    
	INNER JOIN security ON (examregisterlecturer.RegNo = security.UserName)
	 ORDER BY  examregisterlecturer.AYear DESC, examregisterlecturer.CourseCode ASC";
$query_limit_courseallocation = sprintf("%s LIMIT %d, %d", $query_courseallocation, $startRow_courseallocation, $maxRows_courseallocation);
$courseallocation = mysql_query($query_limit_courseallocation, $zalongwa) or die(mysql_error());
$row_courseallocation = mysql_fetch_assoc($courseallocation);

if (isset($_GET['totalRows_courseallocation'])) {
  $totalRows_courseallocation = $_GET['totalRows_courseallocation'];
} else {
  $all_courseallocation = mysql_query($query_courseallocation);
  $totalRows_courseallocation = mysql_num_rows($all_courseallocation);
}
$totalPages_courseallocation = ceil($totalRows_courseallocation/$maxRows_courseallocation)-1;

$queryString_courseallocation = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_courseallocation") == false && 
        stristr($param, "totalRows_courseallocation") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_courseallocation = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_courseallocation = sprintf("&totalRows_courseallocation=%d%s", $totalRows_courseallocation, $queryString_courseallocation);

$queryString_ExamOfficerGradeBook = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_ExamOfficerGradeBook") == false && 
        stristr($param, "totalRows_ExamOfficerGradeBook") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_ExamOfficerGradeBook = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_ExamOfficerGradeBook = sprintf("&totalRows_ExamOfficerGradeBook=%d%s", $totalRows_ExamOfficerGradeBook, $queryString_ExamOfficerGradeBook);
 
?>
<fieldset><legend>Course Allocation Academic Year <?php echo $mwaka; ?>  </legend>
<form name="form1" method="POST" action="<?php echo $editFormAction; ?>">
              <table class='resView' width="54%">
                <tr>
                  
                  <td class='resViewhd' nowrap><div align="right">Course Code: </div></td>
                  <td class='resViewtd' width="52%"><select name="course" id="course">
                    <?php
do {  
?>
                <option value="<?php echo $row_course['CourseCode']?>"><?php echo $row_course['CourseCode']?></option>
                    <?php
} while ($row_course = mysql_fetch_assoc($course));
  $rows = mysql_num_rows($course);
  if($rows > 0) {
      mysql_data_seek($course, 0);
	  $row_course = mysql_fetch_assoc($course);
  }
?>
                  </select></td>

                </tr>
                <tr>
             
                  <td class='resViewhd' nowrap><div align="right">
                    <input name="user" type="hidden" id="user" value="<?php echo $username;?>">
                    Lecturer ID: </div></td>
                  <td class='resViewtd'><select name="lecturer" id="lecturer">
                    <?php
do {  
?>
                    <option value="<?php echo $row_lecturer['UserName']?>"><?php echo $row_lecturer['FullName']?></option>
                    <?php
} while ($row_lecturer = mysql_fetch_assoc($lecturer));
  $rows = mysql_num_rows($lecturer);
  if($rows > 0) {
      mysql_data_seek($lecturer, 0);
	  $row_lecturer = mysql_fetch_assoc($lecturer);
  }
?>
                  </select></td>
                </tr>
<tr>
                  <td class='resViewhd' nowrap><div align="right">Semester:</div></td>
                  <td class='resViewtd'><select name="semester" id="semester">
                    <?php
do {  
?>
                    <option value="<?php echo $row_semester['Description']?>"><?php echo $row_semester['Description']?></option>
                    <?php
} while ($row_semester = mysql_fetch_assoc($semester));
  $rows = mysql_num_rows($semester);
  if($rows > 0) {
      mysql_data_seek($semester, 0);
	  $row_semester = mysql_fetch_assoc($semester);
  }
?>
                  </select></td>
                  
                </tr>
<tr>
                  <td class='resViewhd' nowrap><div align="right">Day:</div></td>
                  <td class='resViewtd'><select name="day" id="day">
            		<option value=" ">Select A Period Day</option>
			<option value="Monday">Monday</option>
			<option value="Tuesday">Tuesday</option>
			<option value="Wednesday">Wednesday</option>
			<option value="Thursday">Thursday</option>
			<option value="Friday">Friday</option>
               	</select></td>
                  
                </tr>
<tr>
             
                  <td class='resViewhd' nowrap><div align="right">
                    Start Time: </div></td>
                  <td class='resViewtd'><select name="starttime" id="starttime">
                    	<option value=" ">Select Period Start Time</option>
			<option value="8">8:00</option>
			<option value="9">9:00</option>
			<option value="10">10:00</option>
			<option value="11">11:00</option>
			<option value="12">12:00</option>
			<option value="13">13:00</option>
			<option value="14">14:00</option>
			<option value="15">15:00</option>
			<option value="16">16:00</option>
			<option value="17">17:00</option>
			<option value="18">18:00</option>
			<option value="19">19:00</option>
			
                  </select></td>
                </tr>
<tr>
             
                  <td class='resViewhd' nowrap><div align="right">
                    End Time: </div></td>
                  <td class='resViewtd'><select name="endtime" id="endtime">
                    <option value=" ">Select Period End Time</option>
			<option value="8">8:00</option>
			<option value="9">9:00</option>
			<option value="10">10:00</option>
			<option value="11">11:00</option>
			<option value="12">12:00</option>
			<option value="13">13:00</option>
			<option value="14">14:00</option>
			<option value="15">15:00</option>
			<option value="16">16:00</option>
			<option value="17">17:00</option>
			<option value="18">18:00</option>
			<option value="19">19:00</option>
			<option value="20">20:00</option>
                  </select></td>
                </tr>
<tr>
                  <td class='resViewhd' nowrap><div align="right">Room:</div></td>
                  <td class='resViewtd'><select name="room" id="room">
                    <?php
do {  
?>
                    <option value="<?php echo $row_venue['VenueCode']?>"><?php echo $row_venue['VenueCode']?></option>
                    <?php
} while ($row_venue = mysql_fetch_assoc($venue));
  $rows = mysql_num_rows($venue);
  if($rows > 0) {
      mysql_data_seek($venue, 0);
	  $$row_venue = mysql_fetch_assoc($venue);
  }
?>
                  </select></td>
                  
                </tr>
                <tr>
                  <td class='resViewhd' colspan="4"><div align="center">
                      <input type="submit" name="Submit" value="Save Lecturer"onmouseover="this.style.background='#DEFEDE'"
onmouseout="this.style.background='lightblue'" style='background-color:lightblue;color:black;font-size:9pt;font-weight:bold' title="Click to Allocate Course"
id="this Course?" onClick="return confirmAllocate(this.id)">
                  </div></td>
                </tr>
  </table>
 <p>
<input type="hidden" name="Ayear" id="Ayear" value="<?php echo $mwaka;?>">
                  <input type="hidden" name="MM_insert" value="form1"> </p>               
</form>
</fieldset>
<?php
echo"<br>";
include('../footer/footer.php');
?>
