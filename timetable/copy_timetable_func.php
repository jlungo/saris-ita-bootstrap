	<?php
	/**
	* @file copy_timetable_func.php
	* @ created on Sun,March 10 2013
	* @ author lackson david
	* */

	function copyTimetable($ayearfrom,$ayearto,$cat1,$cat2){
		 
	 /**
	  *  validate input data
	  * */
	  
	  // timetable recorder
	  global $username;
	  
	 // reset value to no error occured
	 $err=FALSE;
	 $error="";
	 
	 // check empty
	 if($ayearfrom =='' || $ayearto == '' || $cat1 == '' || $cat2==''){
		 $err= TRUE;
		 $error= "Fill all fields, can not process empty data";
		 echo '<meta http-equiv = "refresh" content ="0; url = copytimetable.php?error='.urlencode($error).'">';
		 exit;
		 }	 
	 
	 // check academic year format
	$year = explode('/',$ayearto);
	$yrdiff = $year[1]-$year[0];	 
	if($yrdiff != 1){
		 $err= TRUE;
		 $error= "Sorry, System can not copy timetable due to wrong academic year format";
		 echo '<meta http-equiv = "refresh" content ="0; url = copytimetable.php?error='.urlencode($error).'">';
		 exit;
		}		
	 
	 if($ayearfrom >= $ayearto){
		 $err= TRUE;
		 $error= "You can not copy timetable to same or previous academic year";
		 echo '<meta http-equiv = "refresh" content ="0; url = copytimetable.php?error='.urlencode($error).'">';
		 exit;
		 }	 
		 
	// check timetable categoty exist
	if($cat1===$cat2){
	  $sql_timetablecategory= "SELECT name FROM timetableCategory WHERE Id=".$cat1;
	 } else{
		  $sql_timetablecategory= "SELECT name FROM timetableCategory WHERE Id=".$cat1." OR Id =".$cat2;
		 }
	$numrow_cat = mysql_num_rows(mysql_query($sql_timetablecategory));

	if($numrow_cat < 1 && $cat1 === $cat2)	 {
		 $err= TRUE;
		 $error= "The timetable category you pick does not exis,please pick correct one";
		 echo '<meta http-equiv = "refresh" content ="0; url = copytimetable.php?error='.urlencode($error).'">';
		 exit;
		}
		 
		if($numrow_cat < 2 && $cat1 !== $cat2)	 {
		 $err= TRUE;
		 $error= "The timetable category you pick does not exis,please pick correct one";
		 echo '<meta http-equiv = "refresh" content ="0; url = copytimetable.php?error='.urlencode($error).'">';
		 exit;
		}	 
		
		/*
		 *  if error free start copying
		 * */
		if(!$err){
			
		 // find previous timetable before copying
       $sql_prev = "SELECT * FROM timetable WHERE AYear = '$ayearfrom' AND timetable_category = '$cat1'";
       $result_prev = mysql_query($sql_prev);
       $rowreq= mysql_num_rows($result_prev);
       $countrow=0;
       
			// return timetable category title	   
		$sql_timet1= "SELECT name FROM timetableCategory WHERE Id=".$cat1;
		$result_timet1 = mysql_query($sql_timet1);
		$row_timet1 = mysql_fetch_array($result_timet1);
		$catname1 = $row_timet1['name'];
		
		$sql_timet2= "SELECT name FROM timetableCategory WHERE Id=".$cat2;
		$result_timet2 = mysql_query($sql_timet2);
		$row_timet2 = mysql_fetch_array($result_timet2);
    $catname2 = $row_timet2['name'];
       if($rowreq > 0){	  
		   // check the yr if timetable have already arranged
		   $sql_next="SELECT *FROM timetable WHERE AYear = '$ayearto' AND timetable_category = '$cat2'";
		   $row_next=mysql_num_rows(mysql_query($sql_next));
		   if($row_next==0){
	   while($row_prev = mysql_fetch_array($result_prev)){
	   $Programme = $row_prev['Programme'];
	   $CourseCode = $row_prev['CourseCode'];
	   $start = $row_prev['start'];
	   $end = $row_prev['end'];
	   $venue = $row_prev['venue'];
	   $lecturer = $row_prev['lecturer'];
	   $start_end = $row_prev['start_end'];
	   $day = $row_prev['day'];
	   $Date = $row_prev['Date'];
	   $teachingtype = $row_prev['teachingtype'];
	   $YoS = $row_prev['YoS'];
	   $sql_insert_new = "INSERT INTO	timetable SET AYear='$ayearto',Programme='$Programme',timetable_category='$cat2',
					CourseCode='$CourseCode',start='$start',end='$end',venue='$venue',lecturer='$lecturer',Recorder='$username',
					start_end='$start_end',day='$day',Date='$Date',teachingtype='$teachingtype',YoS='$YoS'"; 
      $result_insert_new =  mysql_query($sql_insert_new);
	 if($result_insert_new){
		 $countrow++;
		 }                 
		   }

	if($countrow > 1){
		$rowdiff= $rowreq-$countrow;
		 $success= "Successfully saved</br>";
		 $summary="Timetable for $ayearfrom - $catname1 is copied to $ayearto - $catname2 <br/>";
		 $summary.= "Rows required to copied : $rowreq<br/>Rows copied : $countrow<br/>Rows not copied : $rowdiff";
		 echo '<meta http-equiv = "refresh" content ="0; url = copytimetable.php?success='.urlencode($success).'&det='.$summary.'">';
		 exit;
		}
			   
			   }else{
		 session_start();		   
		$_SESSION['clear']= TRUE;
	    $error= "The timetable for $ayearto - $catname2 already exist, please clear it and copy again ";
	    $jstx= "'Click Ok to Clear or Cancel to Back to previous'";
	    $_SESSION['cleartxt']='<a href="copytimetable.php?clear=1&ayear='.$ayearto.'&cat='.$cat2.'" onclick="return onsubConfirm('.$jstx.'); ">CLEAR</a>';
		echo '<meta http-equiv = "refresh" content ="0; url = copytimetable.php?error='.urlencode($error).'">';
	    exit;
				   } 		
			}else{
		$err= TRUE;
	    $error= "The timetable for $ayearfrom - $catname1 you want to copy does not exist ";
		echo '<meta http-equiv = "refresh" content ="0; url = copytimetable.php?error='.urlencode($error).'">';
	    exit;
				}
		
		   }
       
	}
	?>
