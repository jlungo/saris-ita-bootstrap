<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Policy Setup';
$szSubSection = 'Policy Setup';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /*background-color: #009688;*/
        }

        .card {
            /*background-color: #324359;*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /*color: white;*/
            padding: 0px;
            border-radius: 0px !important;
        }

        @media (max-width: 34em) {
            .card {
                margin-top: 20px;
            }
        }

        @media (max-width: 48em) {
            .card {
                margin-top: 20px;
            }
        }

        .row {
            margin-top: 20px;
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'admissionNavBar.php'; ?>

<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <h3 class="card-header">
                    <?php echo $szTitle; ?>
                </h3>
                <div class="card-block">
                    <?php
                    if (isset($_GET['content'])) {
                        if ($_POST['update']) {
                            $Name = addslashes($_POST["Name"]);
                            $Address = addslashes($_POST["Address"]);
                            $comment = addslashes($_POST["comment"]);

                            $sql = "UPDATE sponsors SET Name='$Name', Address = '$Address',
    comment='$comment' WHERE SponsorID=" . $_GET['content'];
                            if ($zalongwa->query($sql)) {
                                echo "<p>Updated successfully</p>";
                            } else {
                                echo "<p>Failed to update..</p>";
                            }
                        }

                        $result = $zalongwa->query("SELECT * FROM sponsors WHERE SponsorID=" . $_GET['content']);
                        $institution = $result->fetch_assoc();
                        ?>
                        <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Name:</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" id="Name" name="Name"
                                               value="<?php echo $institution['Name']; ?>"
                                               size="40">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Address</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" id="Address" name="Address"
                                               value="<?php echo $institution['Address']; ?>"
                                               size="40">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Comment</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" id="comment" name="comment"
                                               value="<?php echo $institution['comment']; ?>" size="40">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"></label>
                                    <div class="col-sm-8">
                                        <input type="submit" id="submit" name="update" value="update"
                                               class="btn btn-success btn-md btn-block"
                                               title="Click to update information">
                                    </div>
                                </div>
                            </div>
                        </form>

                        <?php

                        function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
                        {
                            $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

                            switch ($theType) {
                                case "text":
                                    $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                                    break;
                                case "long":
                                case "int":
                                    $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                                    break;
                                case "double":
                                    $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                                    break;
                                case "date":
                                    $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                                    break;
                                case "defined":
                                    $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                                    break;
                            }
                            return $theValue;
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>
