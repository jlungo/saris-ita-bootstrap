<?php
#get connected to the database and verfy current session
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

# include the header
global $szSection, $szSubSection;
$szSection = 'Admission Process';
$szSubSection = 'Registration Form';
$szTitle = 'Student Registration Form';

#include calendar and other stylesheets
require_once('../includes/javascript.inc');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="https://cdn.datatables.net/responsive/2.2.0/css/responsive.bootstrap4.min.css">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /* background-color: #009688; */
        }
        .card {
            /* background-color: #324359; */
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /* color: white; */
            padding: 0px;
            border-radius: 0px !important;
        }
        .row {
            margin-top: 20px;
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>


</head>
<body>
<!-- navbar -->
<?php include 'admissionNavBar.php'; ?>

<div class="container-flex">
</div>

<?php include '../includes/registrationform.php'; ?>

<br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<!--script for datatable-->
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details for ' + data[0] + ' ' + data[1];
                        }
                    }),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            }
        });
    });
</script>
</body>
</html>