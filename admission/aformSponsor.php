<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Sponsor Information';
$szSubSection = 'Policy Setup';

//include("../includes/datatable_bootstrap.inc");

if (isset($_GET['delete'])) {
    $zalongwa->query("DELETE FROM sponsors WHERE SponsorID=" . $_GET['delete']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="https://cdn.datatables.net/responsive/2.2.0/css/responsive.bootstrap4.min.css">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <style>
        body {
            /*background-color: #009688;*/
        }

        .card {
            /*background-color: #324359;*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /*color: white;*/
            padding: 0px;
            border-radius: 0px !important;
        }

        @media (max-width: 34em) {
            .card {
                margin-top: 20px;
            }
        }

        @media (max-width: 48em) {
            .card {
                margin-top: 20px;
            }
        }

        .row {
            margin-top: 20px;
        }
    </style>

    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>


</head>
<body>
<!-- navbar -->
<?php include 'admissionNavBar.php'; ?>

<div class="container-flex">
</div>
<div class="container">
    <h3 class="h3">Institution Information</h3>
    <?php
    switch ($_GET['content']) {
        default:
            $query = "SELECT * FROM sponsors";
            $result_sql = $zalongwa->query($query) or die("Cannot query the database.<br>" . $zalongwa->connect_error);
            ?>
            <table class="table table-striped table-bordered table-responsive" width="100%" cellspacing="0">
                <thead class="table-inverse">
                <tr>
                    <th><a href="aformSponsor.php?content=AddSponsor">+Add</a></th>
                    <th>Sponsor</th>
                    <th>Address</th>
                    <th>Comment</th>
                    <th>View</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($result = $result_sql->fetch_array()) {
                    $SponsorID = $result['SponsorID'];
                    $Name = stripslashes($result["Name"]);
                    $Address = stripslashes($result["Address"]);
                    $comment = stripslashes($result["comment"]);
                    $fax = stripslashes($result["fax"]);
                    $Email = stripslashes($result["Email"]);
                    $Location = stripslashes($result["Location"]);
                    ?>
                    <tr>
                        <td><a href="aformSponsorEdit.php?content=<?php echo $SponsorID; ?>">Edit</a></td>
                        <td><?php echo $Name; ?></td>
                        <td><?php echo $Address; ?></td>
                        <td><?php echo $comment; ?></td>

                        <td><a href="aformSponsorDetails.php?details=<?php echo $SponsorID; ?>">Details</a></td>
                        <td class="center"><a href="aformSponsor.php?delete=<?php echo $SponsorID; ?>"
                                              onClick="return confirm('Are you sure you want to delete <?php echo $Name; ?>')"><i
                                        class="fa fa-trash"></i></a></td>
                    </tr>
                    <?php
                } ?>
                </tbody>
            </table>
            <?php
            break;

        case "AddSponsor":
            function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
            {
                $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

                switch ($theType) {
                    case "text":
                        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                        break;
                    case "long":
                    case "int":
                        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                        break;
                    case "double":
                        $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                        break;
                    case "date":
                        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                        break;
                    case "defined":
                        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                        break;
                }
                return $theValue;
            }

            if (isset($_POST["submit"])) {
                $sql_insert = sprintf("INSERT INTO sponsors (Name, Address, comment)
	  													VALUES (%s, %s, %s)",
                    GetSQLValueString($_POST['Name'], "text"),
                    GetSQLValueString($_POST['Address'], "text"),
                    GetSQLValueString($_POST['comment'], "text"));
                if ($zalongwa->query($sql_insert)) {
                    echo "<script>location.href = 'aformSponsor.php';</script>";
                } else {
                    die("Cannot query the database.<br>" . $zalongwa->connect_error);
                }
            }
            ?>
            <div class="row ">
                <div class="col-sm-8">
                    <div class="card">
                        <h3 class="card-header">
                            <?php echo $szTitle; ?></h3>
                        <div class="card-block">
                            <form action="aformSponsor.php?content=AddSponsor" method="POST">
                                <div class="container">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Name:</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" id="Name" name="Name" size="40"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Address</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" id="Location" name="Location"
                                                   size="40" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-sm-4 col-form-label">Comment</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" id="comment" name="comment"
                                                   size="40">
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-sm-4 col-form-label"></label>
                                        <div class="col-sm-8">
                                            <input type="submit" id="submit" name="submit" value="create"
                                                   class="btn btn-success btn-md btn-block"
                                                   title="Click to Save Changes">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
    }
    ?>

</div>
<br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<!--script for datatable-->
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details for ' + data[0] + ' ' + data[1];
                        }
                    }),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            }
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('#zalongwa_datatable').dataTable();
    })
</script>
<style>
    .center {
        text-align: center;
    }

    .remove-blue-link {
        color: #333339;
    }

    tbody {
        font-weight: normal;
    }
</style>

</body>
</html>
