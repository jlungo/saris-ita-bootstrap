<?php

#get connected to the database and verfy current session

require_once('../Connections/sessioncontrol.php');

require_once('../Connections/zalongwa.php');

global $szSection, $szSubSection;

$szSection = 'Admission Process';

$szSubSection = 'Restore logs';

$szTitle = 'Restore Deleted Student Records';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
        body {
            /*background-color: #009688;*/
        }

        .card {
            /*background-color: #324359;*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /*color: white;*/
            padding: 0px;
            border-radius: 0px !important;
        }

        .row {
            margin-top: 20px;
            margin-bottom: 20px;
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>

</head>
<body>
<!-- navbar -->
<?php include 'admissionNavBar.php'; ?>

<div class="container ">
    <h3 class="h3">Select Students to Delete</h3>

    <?php
    if (isset($_POST['delete'])) {

        $Eq = $_POST[Eq];

        $count = count($Eq);

        if ($count <= 0) {
            ?>
            <div class="alert alert-danger" role="alert">
                <strong>Please choose students first!</strong>
            </div>
            <?php
        } else {

            $e = 1;

            for ($j = 0; $j < $count; $j++) {

#Fetch Records

                $sql = "SELECT * FROM student WHERE Id ='$Eq[$j]'";

                $update = mysqli_query($zalongwa, $sql) or die(mysqli_error($zalongwa));

                $update_row = mysqli_fetch_array($update) or die(mysqli_error($zalongwa));

                $regno = addslashes($update_row['RegNo']);

                $stdid = addslashes($update_row['Id']);

                $AdmissionNo = addslashes($update_row['AdmissionNo']);

                $degree = addslashes($update_row['ProgrammeofStudy']);

                $faculty = addslashes($update_row['Faculty']);

                $ayear = addslashes($update_row['EntryYear']);

                $combi = addslashes($update_row['Subject']);

                $campus = addslashes($update_row['Campus']);

                $manner = addslashes($update_row['MannerofEntry']);

                $surname = addslashes($update_row['Name']);

                $dtDOB = addslashes($update_row['DBirth']);

                $age = addslashes($update_row['age']);

                $sex = addslashes($update_row['Sex']);

                $sponsor = addslashes($update_row['Sponsor']);

                $country = addslashes($update_row['Nationality']);

                $district = addslashes($update_row['District']);

                $region = addslashes($update_row['Region']);

                $maritalstatus = addslashes($update_row['MaritalStatus']);

                $address = addslashes($update_row['Address']);

                $religion = addslashes($update_row['Religion']);

                $denomination = addslashes($update_row['Denomination']);

                $postaladdress = addslashes($update_row['postaladdress']);

                $residenceaddress = addslashes($update_row['residenceaddress']);

                $disabilityCategory = addslashes($update_row['disabilityCategory']);

                $status = addslashes($update_row['Status']);

                $gyear = addslashes($update_row['GradYear']);

                $phone1 = addslashes($update_row['Phone']);

                $email1 = addslashes($update_row['Email']);

                $formsix = addslashes($update_row['formsix']);

                $formfour = addslashes($update_row['formfour']);

                $diploma = addslashes($update_row['diploma']);

                $School_attended_olevel = addslashes($update_row['School_attended_olevel']);

                $School_attended_alevel = addslashes($update_row['School_attended_alevel']);

                $name = $surname;//", ".$firstname." ".$middlename;

//Added fields

                $account_number = addslashes($update_row['account_number']);

                $bank_branch_name = addslashes($update_row['bank_branch_name']);

                $bank_name = addslashes($update_row['bank_name']);

                $form4no = addslashes($update_row['form4no']);

                $form4name = addslashes($update_row['form4name']);

                $form6name = addslashes($update_row['form6name']);

                $form6no = addslashes($update_row['form6no']);

                $form7name = addslashes($update_row['form7name']);

                $form7no = addslashes($update_row['form7no']);

                $paddress = addslashes($update_row['paddress']);

                $currentaddaress = addslashes($update_row['currentaddaress']);

                $f4year = addslashes($update_row['f4year']);

                $f6year = addslashes($update_row['f6year']);

                $f7year = addslashes($update_row['f7year']);

#next of kin info

                $kin = addslashes($update_row['kin']);

                $kin_phone = addslashes($update_row['kin_phone']);

                $kin_address = addslashes($update_row['kin_address']);

                $kin_job = addslashes($update_row['kin_job']);

                $studylevel = addslashes($update_row['studylevel']);

                $ActUser = $_SESSION['username'];

                $Action = "Deleted";

//***********

#insert record

                $sql = "INSERT INTO studentlog

(Name,AdmissionNo,

Sex,DBirth,

MannerofEntry,MaritalStatus,

Campus,ProgrammeofStudy,

Faculty,

Sponsor,GradYear,

EntryYear,Status,

Address,Nationality,

Region,District,Country,

Received,user,

Denomination, Religion,

Disability,formfour,

formsix,diploma,

f4year,f6year,f7year,

kin,kin_phone,

kin_address,kin_job,

disabilityCategory,Subject,

account_number,

bank_branch_name,

bank_name,

form4no,

form4name,

form6name,

form6no,

form7name,

form7no,

paddress,

Phone,

Email,

currentaddaress,

RegNo,

studylevel,

ActionDate,

Action,

ActUser

) 

VALUES

('$name','$AdmissionNo',

'$sex','$dtDOB',

'$manner','$maritalstatus',

'$campus','$degree',

'$faculty',' $sponsor',

'$gyear','$ayear',

'$status','$address',

'$country',

'$region','$district',

'$country',now(),

'$username','$denomination', 

'$religion','$disability',

'$formfour','$formsix',

'$diploma','$f4year',

'$f6year','$f7year',

'$kin','$kin_phone',

'$kin_address','$kin_job',

'$disabilityCategory','$Subject',

'$account_number',

'$bank_branch_name',

'$bank_name',

'$form4no',

'$form4name',

'$form6name',

'$form6no',

'$form7name',

'$form7no',

'$paddress',

'$phone1',

'$email1',

'$currentaddaress',

'$regno',

'$studylevel',

now(),

'$Action',

'$ActUser'

)";

//echo $sql;

                $plg = mysqli_query($zalongwa, $sql) or die(mysqli_error($zalongwa));

                if ($plg) {

                    $delete = mysqli_query($zalongwa, "delete from student where Id='$Eq[$j]'");

                } else {


                }

            }

        }

    }
    //Begin of Filter Panel
    ?>
    <div class="row " style="margin: 20px 0;">
        <div class="col-md-5">
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method='GET'>
                <div class="input-group">
                    <input name='key' type="text" class="form-control" placeholder="Enter RegNo/Admission/Name"
                           maxlength="50">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" name='search'>
                        <i class="fa fa-search"></i>Search
                    </button>
                </span>
                </div>
            </form>
        </div>
    </div>

    <?php

    if (isset($_GET['search']) || $_GET['key']) {

        $key = $_GET['key'];

        $look = "SELECT * FROM  student where RegNo like '%$key'OR Name like '%$key%' OR AdmissionNo like '%$key'";

    } else {

        $look = "SELECT * FROM  student order by Faculty";

    }


    //Begin of Pagination

    ///START DISPLAYING RECORDS

    $rowPerPage = 20;

    $pageNum = 1;

    if (isset($_GET['page'])) {

        $pageNum = $_GET['page'];

    }

    $offset = ($pageNum - 1) * $rowPerPage;

    $k = $offset + 1;

    $query = $look . " LIMIT $offset,$rowPerPage";

    $result = mysqli_query($zalongwa, $query) or die(mysqli_error($zalongwa));
    ?>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method='POST' name='frm1'>
        <table class="table table-striped table-bordered table-responsive" width="100%" cellspacing="0">
            <thead class="table-inverse">
            <tr>
                <th width="10%">SN</th>
                <th width="40%">Student Name</th>
                <th width="20%">RegNo</th>
                <th width="20%">Faculty</th>
                <th width="10%">Select</th>
            </tr>
            </thead>
            <?php
            while ($r = mysqli_fetch_array($result)) {
                ?>
                <tr>
                    <td>&nbsp;<?php echo $k; ?></td>
                    <td>&nbsp;<?php echo $r['Name']; ?></td>
                    <td>&nbsp;<?php echo $r['RegNo']; ?></td>
                    <td>&nbsp;<?php echo $r['Faculty']; ?></td>
                    <td>
                        <input type='checkbox' name='Eq[]' value='<?php echo $r['Id']; ?>'>
                    </td>
                </tr>
                <?php
                $k++;
            }
            ?>
        </table>

        <?php
        $data = mysqli_query($zalongwa, $look);

        $numrows = mysqli_num_rows($data);

        $result = mysqli_query($zalongwa, $data);

        $row = mysqli_fetch_array($data);


        $maxPage = ceil($numrows / $rowPerPage);


        $self = $_SERVER['PHP_SELF'];

        $nav = '';

        for ($page = 1; $page <= $maxPage; $page++) {

            if ($page == $pageNum) {

                $nav .= " $page";

                $nm = $page;

            } else {

                $nav .= "<a href=\"$self?page=$page&key=$key\">$page</a>";

            }

        }

        if ($pageNum > 1) {

            $page = $pageNum - 1;

            $prev = "<a href=\"$self?page=$page&key=$key\">Previous</a>";

            $first = "<a href=\"$self?page=1\">[First]</a>";

        } else {

            $prev = '&nbsp;';

            $first = '&nbsp;';

        }

        if ($pageNum < $maxPage) {

            $page = $pageNum + 1;

            $next = "<a href=\"$self?page=$page&key=$key\">Next</a>";

            $last = "<a href=\"$self?page=$maxPage\" class='mymenu'>[Last Page]</a>";

        } else {

            $next = '&nbsp;';

            $last = '&nbsp;';

        }

        echo "<table>

<tr>

<td width='200'>&nbsp;&nbsp;&nbsp;&nbsp;$prev&nbsp;&nbsp;</td>

<td width='200'>&nbsp;&nbsp;Page $nm of $maxPage&nbsp;&nbsp;</td>

<td width='200'>&nbsp;&nbsp;$next&nbsp;&nbsp;</td>

<td>&nbsp;&nbsp;&nbsp;<font color='#CCCCCC'></font></td>

</tr></table></center>";
        //End of Pagination

        ?>
        <div class="container">
            <div class="form-group row">
                <div class="col-sm-3 offset-sm-3">
                    <input type='submit' name='delete' class="btn btn-danger btn-block btn-md" value='Delete'
                           id="All selected Students ?"
                           onClick="return confirmdelete(this.id)">
                </div>
            </div>
        </div>
    </form>

    <?php
    mysqli_free_result($courselist);
    ?>
</div>
<br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
<!--script for datatable-->
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details for ' + data[0] + ' ' + data[1];
                        }
                    }),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            }
        });
    });
</script>

<script type="text/javascript">
    checked = false;
    function checkedAll(frm1) {
        var aa = document.getElementById('frm1');
        if (checked == false) {
            checked = true
        }
        else {
            checked = false
        }
        for (var i = 0; i < aa.elements.length; i++) {
            aa.elements[i].checked = checked;
        }
    }
</script>
<script type='text/javascript'>

    function confirmdelete( data)

    {

        if(confirm("Are you sure you want to restore the record      "+data))

        {

            return true;

        }else

        {

            return false;

        }

    }

</script>

</body>
</html>
