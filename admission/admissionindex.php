<?php
require_once '../Connections/sessioncontrol.php';
require_once '../Connections/zalongwa.php';

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;

$szSection = 'Home';

$szTitle = 'Home';

$szSubSection = 'Home';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css" />
    <style>

        .row {
            margin-top: 20px;
        }
        .card {
            border-top: 7px solid #263238;
            padding-top: 5%;
        }
        .card:focus, .card:hover {
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            /*box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.20);*/
        }
        .card-inverse .card-img-overlay {
            background-color: rgba(51, 51, 51, 0.85);
            border-color: rgba(51, 51, 51, 0.85);
        }
    </style>
    <title>SARIS | <?php echo  $szSection ?> | <?php echo  $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>

<?php include 'admissionNavBar.php'; ?>

<div class="container text-muted">
    <!-- cards -->
    <div class="row">

        <div class="col-sm-4 col-md-4 ">
            <div class="card" align="center">
                <a href="admissionProcess.php"> <img class="card-img-top img-fluid "
                                                              style="width: 112px; height: inherit; padding-top: 15px; "
                                                              src="./img/admission.svg"></a>
                <div class="card-block">
                    <a href="admissionProcess.php"><h5 class="card-title">Admission</h5></a>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-md-4 ">
            <div class="card" align="center">

                <a href="admissionpolicy.php"> <img class="card-img-top img-fluid "
                                                    style="width: 112px; height: inherit; padding-top: 15px; "
                                                    src="./img/elearning.svg"></a>

                <div class="card-block">
                    <a href="admissionpolicy.php"><h5 class="card-title">Policy Setup</h5></a>

                </div>
            </div>
        </div>
        <div class="col-sm-4 col-md-4">
            <div class="card" align="center">

                <a href="admissionEvoting.php"> <img class="card-img-top img-fluid "
                                                    style="width: 112px; height: inherit; padding-top: 15px; "
                                                    src="./img/evoting.svg"></a>

                <div class="card-block">
                    <a href="admissionEvoting.php"><h5 class="card-title">E-Voting</h5></a>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 col-md-4 ">
            <div class="card" align="center">

                <a href="admissionComm.php"> <img class="card-img-top img-fluid "
                                                  style="width: 112px; height: inherit; padding-top: 15px; "
                                                  src="./img/communication.svg"></a>

                <div class="card-block">
                    <a href="admissionComm.php"><h5 class="card-title">Communication</h5></a>

                </div>
            </div>
        </div>
        <div class="col-sm-4 col-md-4 ">
            <div class="card" align="center">

                <a href="admissionSecurity.php"> <img class="card-img-top img-fluid "
                                                      style="width: 112px; height: inherit; padding-top: 15px; "
                                                      src="./img/security.svg"></a>

                <div class="card-block">
                    <a href="admissionSecurity.php"><h5 class="card-title">Security</h5></a>
                </div>
            </div>
        </div>
    </div>
</div><!-- end .container -->
<br><br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>
