/***
 * @ Normal javascript file with no jquery library
 * @ file func.js
 * @ description control form
 * @ author Lackson David
 * */

function lackOnsubmit(){
	var programme_error='';
	var cohort_error='';
	var userfile= '';
	if(studentclasslist.programme.value==0){
	 programme_error = 'programme';
	}
	
	if (programme_error != ''){
	alert('Please select Programme');
	studentclasslist.programme.focus();
	return false;
		} else{
					
	if(studentclasslist.cohort.value == 0){
	cohort_error ="cohort";
	}
	
	if(cohort_error != '') {
	alert('Please select Cohort Year');
	studentclasslist.cohort.focus();
	return false;
	} else{
		if(studentclasslist.userfile.value==''){
	     userfile_error = 'userfile';
	        }
		if($userfile_error != '') {
	  alert('Please Choose file');
	  studentclasslist.userfile.focus();
	  return false;
			} else{
			return true;	
				}
		}	
	
	}
	
}
