<?php 
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('admissionMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Examination';
	$szSubSection = 'Grade Book';
	$szTitle = 'Examination GradeBook';
	include('admissionheader.php');

#save user statistics
$browser  = $_SERVER["HTTP_USER_AGENT"];   
$ip  =  $_SERVER["REMOTE_ADDR"];   
$sql="INSERT INTO stats(ip,browser,received,page) VALUES('$ip','$browser',now(),'$username')";   
$result = mysql_query($sql) or die("Siwezi kuingiza data.<br>" . mysql_error());

#Control Refreshing the page
#if not refreshed set refresh = 0
@$refresh = 0;
#------------
#populate academic year combo box
mysql_select_db($database_zalongwa, $zalongwa);
$query_AYear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$AYear = mysql_query($query_AYear, $zalongwa) or die(mysql_error());
$row_AYear = mysql_fetch_assoc($AYear);
$totalRows_AYear = mysql_num_rows($AYear);

//check if is a Departmental examination officer
$query_userdept = "SELECT Dept FROM security where UserName = '$username' AND Dept<>0";
$userdept = mysql_query($query_userdept, $zalongwa) or die(mysql_error());
$row_userdept = mysql_fetch_assoc($userdept);
$totalRows_userdept = mysql_num_rows($userdept);
mysql_select_db($database_zalongwa, $zalongwa);

//check if is Faculty examination officer
if($privilege == 2){
	$query_userfac = "SELECT Faculty FROM security where UserName = '$username' AND Dept=0";
	}
else{
	$query_userfac = "SELECT Faculty FROM security where UserName = '$username'";
	}
$userfac = mysql_query($query_userfac, $zalongwa) or die(mysql_error());
$row_userfac = mysql_fetch_assoc($userfac);
$totalRows_userfac = mysql_num_rows($userfac);
$fac = $row_userfac["Faculty"];

if($totalRows_userdept>0){
	$query_dept = "SELECT department.DeptName	FROM department
					INNER JOIN security ON (department.DeptID = security.Dept)
					WHERE (UserName = '$username') ORDER BY department.DeptName";
	}
elseif($privilege == 2){
	$query_dept = "SELECT FacultyID, FacultyName FROM faculty WHERE (FacultyID = '$fac')";
	}
else{
	$query_dept = "SELECT DeptID, DeptName	FROM department ORDER BY DeptName ASC";
	}
								
$dept = mysql_query($query_dept, $zalongwa) or die(mysql_error());
$row_dept = mysql_fetch_assoc($dept);
$totalRows_dept = mysql_num_rows($dept);

#process form submission
$editFormAction = $_SERVER['PHP_SELF'];
if ((isset($_POST["frmSubmit"])) && ($_POST["frmSubmit"] == "yes")) {
#set refresh = 1
$refresh = 1;

#..............
@$ayear = addslashes($_POST['ayear']);
@$faculty = addslashes($_POST['faculty']);
@$sem = $_POST['sem'];

if($sem=="Choice"){
	echo "<p>Choose Semester of Study First<p>";
	exit;
}

$upload = mysql_query("SELECT * FROM uploadlimit WHERE AYear='$ayear' AND Semester='$sem' AND DeptID='$fac'");
while($values = mysql_fetch_array($upload)){
		$limc = $values['Checked'];
		$limd = $values['Date'];
	}

$limd2 = date('Y-m-d');
if ($privilege !='2' && $limc == 1 && $limd != "" && ( $limd < $limd2)){
	echo "<p style='color:maroon'>Results uploading season has passed, please consult your Department Examinations officer<p>";
	exit;
	}

#populate examcayegory combo box
mysql_select_db($database_zalongwa, $zalongwa);

if($fac==1){
$query_examcategory = "SELECT Id,Description FROM examcategory WHERE (Id > 2) ORDER BY Id";
}else{
$query_examcategory = "SELECT Id,Description FROM examcategory ORDER BY Id";
}
$examcategory = mysql_query($query_examcategory, $zalongwa) or die(mysql_error());
$row_examcategory = mysql_fetch_assoc($examcategory);
$totalRows_examcategory = mysql_num_rows($examcategory);

#populate Exam Marker combo box
mysql_select_db($database_zalongwa, $zalongwa);
$query_exammarker = "SELECT Id, Name FROM exammarker ORDER BY Name";
$exammarker = mysql_query($query_exammarker, $zalongwa) or die(mysql_error());
$row_exammarker = mysql_fetch_assoc($exammarker);
$totalRows_exammarker = mysql_num_rows($exammarker);

#populate CourseCode combo box
/*
if ($privilege ==3) {
$query_coursecode = "
		SELECT DISTINCT course.CourseCode, 
						examregister.AYear
		FROM examregister 
			INNER JOIN course ON (examregister.CourseCode = course.CourseCode)
		WHERE (examregister.AYear ='$ayear') 
		AND (examregister.RegNo='$username')  ORDER BY examregister.CourseCode ASC";
}else{
$query_coursecode = "
		SELECT DISTINCT course.CourseCode, 
						examregister.AYear
		FROM examregister 
			INNER JOIN course ON (examregister.CourseCode = course.CourseCode)
		WHERE (examregister.AYear ='$ayear') 
		AND (course.Faculty = '$faculty') ORDER BY examregister.CourseCode ASC";
}
*/
if ($privilege ==3){
$query_coursecode = "SELECT DISTINCT course.CourseCode, examregister.AYear
		FROM examregister INNER JOIN course ON (examregister.CourseCode = course.CourseCode)
		WHERE (examregister.AYear ='$ayear') AND (examregister.RegNo='$username') 
		ORDER BY examregister.CourseCode ASC";
}else{
$query_coursecode = "SELECT DISTINCT CourseCode FROM course ORDER BY CourseCode ASC";
		
 $query_coursecode2 = "
		SELECT DISTINCT course.CourseCode, examregister.AYear
		FROM examregister INNER JOIN course ON (examregister.CourseCode = course.CourseCode)
		WHERE (examregister.AYear ='$ayear') AND (examregister.RegNo='$username') 
		ORDER BY examregister.CourseCode ASC";

$coursecode2 = mysql_query($query_coursecode, $zalongwa) or die(mysql_error());
}


$coursecode = mysql_query($query_coursecode, $zalongwa) or die(mysql_error());

?>
 Select Appropriate Entries  For : 
	<?php 
	echo $_POST['sem'].' - '.$_POST['ayear'];
	?>
		<form action="lecturerGradebookAdd.php" method="post" enctype="multipart/form-data" name="frmCourse" target="_self">
						
		<table class="table_form" cellspacing="0" cellpadding="0">
          <tr class="table_form_header">
            <th nowrap="nowrap" class="td_label"  scope="col">Module Code 
				<input name="ayear" type="hidden" value="<?php echo $ayear ?>">
				<input name="sem" type="hidden" value="<?php echo $_POST['sem'] ?>">
			</th>
            <th nowrap="nowrap" class="td_label" scope="col">Exam Category </th>
            <th nowrap="nowrap" class="td_label" scope="col">Exam Date </th>
           
          </tr>
          <tr>
            <td ><select style="width:auto;" name="course" size="1">
              <option value="0">[Select Course Code]</option>
              <?php
				if($privilege == 2){
					while($row_coursecode2 = mysql_fetch_array($coursecode2)){
						echo "<option value='".$row_coursecode2['CourseCode']."'>".$row_coursecode2['CourseCode']."</option>";
						}
					}
				do {  
						?>
              <option value="<?php echo $row_coursecode['CourseCode']?>"><?php echo $row_coursecode['CourseCode']?></option>
              <?php
							} while ($row_coursecode = mysql_fetch_assoc($coursecode));
									$rows = mysql_num_rows($coursecode);
									if($rows > 0) {
						mysql_data_seek($coursecode, 0);
						$row_coursecode = mysql_fetch_assoc($coursecode);
  					}
               ?>
            </select></td>
            <td ><select style="width:auto;" name="examcat" size="1">
              <option value="0">[Select Examcategory]</option>
              <?php
				do {  
						?>
              <option value="<?php echo $row_examcategory['Id']?>"><?php echo $row_examcategory['Description']?></option>
              <?php
							} while ($row_examcategory = mysql_fetch_assoc($examcategory));
									$rows = mysql_num_rows($examcategory);
									if($rows > 0) {
						mysql_data_seek($examcategory, 0);
						$row_examcategory = mysql_fetch_assoc($examcategoryr);
  					}
               ?>
            </select></td>
            <td >			<!-- A Separate Layer for the Calendar -->
					<script language="JavaScript" src="datepicker/Calendar1-901.js" type="text/javascript"></script>
					 <table border="0">
									<tr>
										<td><input style="width:150px;" name="examdate" type="text" size="10" maxlength="10"></td>
										<td><input type="button" class="button" name="rpDate_button" value="Pick Date" onClick="show_calendar('frmCourse.examdate', '','','YYYY-MM-DD', 'POPUP','AllowWeekends=Yes;Nav=No;SmartNav=Yes;PopupX=325;PopupY=325;')"></td>
									</tr>
		    </table></td>
            
          </tr>
        </table>
        <table class="table_form">
        <tr class="submit">
        <td>
        <input onclick="history.back(-1)" type="button"  value="Back">
        <input name="view" type="submit" value="Edit Records" />
        </td>
        </tr>
        </table>
		</form>	
<?php
//end of the form display
}

#display the form when refresh is zero
if ($refresh == 0) {
?> 

<form action="<?php echo $editFormAction ?>" method="post" enctype="multipart/form-data" name="form1">
<div >Select Appropriate Academic Year and Semester</div>
              <table class="table_form" cellpadding="0" cellspacing="0">
              <tr class="table_form_header">
              <td class="td_label">Field</td><td>Field Value</td>
              </tr>
                <tr>
                  <td class="td_label" nowrap><div align="right">Academic Year: </div></td>
                  <td><select name="ayear" id="ayear">
                      <?php
do {  
?>
                      <option value="<?php echo $row_AYear['AYear']?>"><?php echo $row_AYear['AYear']?></option>
                      <?php
} while ($row_AYear = mysql_fetch_assoc($AYear));
  $rows = mysql_num_rows($AYear);
  if($rows > 0) {
      mysql_data_seek($AYear, 0);
	  $row_AYear = mysql_fetch_assoc($AYear);
  }
?>
                  </select></td>
                </tr>
                <tr>
                  <td class="td_label" nowrap><div align="right">Semester: </div></td>
                  <td><select name="sem" id="sem">
			   <option value="Choice">Choose Semester</option>
                        <?php
mysql_select_db($database_zalongwa, $zalongwa);
$query_sem = "SELECT Semester FROM terms ORDER BY Semester ";
$sem = mysql_query($query_sem, $zalongwa);
$row_sem = mysql_fetch_assoc($sem);
$totalRows_sem = mysql_num_rows($sem);
do {  
?>
                        <option value="<?php echo $row_sem['Semester']?>"><?php echo $row_sem['Semester']?></option>
                        <?php
} while ($row_sem = mysql_fetch_assoc($sem));
  $rows = mysql_num_rows($sem);
  if($rows > 0) {
      mysql_data_seek($sem, 0);
	  $row_sem = mysql_fetch_assoc($sem);
  }
?>
                    </select>
                  </td>
                </tr>
			<?php if ($privilege =='2') { ?>
                <tr>
                  <td class="td_label" nowrap><div align="right">Faculty:</div></td>
                  <td><select name="faculty" id="faculty">
                      <?php
do {  
?>
                      <option value="<?php echo $row_dept['FacultyID']?>"><?php echo $row_dept['FacultyName']?></option>
                      <?php
} while ($row_dept = mysql_fetch_assoc($dept));
  $rows = mysql_num_rows($dept);
  if($rows > 0) {
      mysql_data_seek($dept, 0);
	  $row_dept = mysql_fetch_assoc($dept);
  }
?>
                  </select></td>
                </tr>
				<?php } ?>
              
  </table>
  <table class="table_form">
  <tr class="submit">
  <td>
  <input onclick="history.back(-1)" type="button"  value="Back">
  <input name="frmSubmit" type="hidden" id="frmSubmit" value="yes">
  <input type="submit" name="action" value="View Courses">
  </td>
  </tr>
  </table>
</form>
<?php

}
include('../footer/footer.php');
?>
