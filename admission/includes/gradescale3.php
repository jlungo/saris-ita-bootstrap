<?php
		#draw a line
		$pdf->line($x, $y, 570.28, $y);       
		$pdf->line($x, $y+56, 570.28, $y+56); 
		$pdf->line($x, $y, $x, $y+56); 
		$pdf->line(570.28, $y, 570.28, $y+56);
		#vertical lines
		$pdf->line($x+65, $y, $x+65, $y+56);  
		$pdf->line($x+145, $y, $x+145, $y+56); 
		$pdf->line($x+225, $y, $x+225, $y+56); 
		$pdf->line($x+305, $y, $x+305, $y+56); 
		$pdf->line($x+455, $y, $x+455, $y+56); 
		
		#horizontal lines
		$pdf->line($x, $y+14, 570.28, $y+14); 
		$pdf->line($x, $y+28, 570.28, $y+28);  
		$pdf->line($x, $y+42, 570.28, $y+42); 
		#row 1 text
		$pdf->text($x+2, $y+12, 'Grade   '); 
		$pdf->text($x+105, $y+12, '  A   ');
		$pdf->text($x+175, $y+12, '  B  ');
		$pdf->text($x+265, $y+12, '  C   ');
		$pdf->text($x+345, $y+12, '  F   ');
		$pdf->text($x+480, $y+12, '     ');
		#row 2 text
		$pdf->text($x+2, $y+24, 'Marks  '); 
		$pdf->text($x+95, $y+24, '  80-100%   ');
		$pdf->text($x+165, $y+24, '  65-79%  ');
		$pdf->text($x+255, $y+24, '  50-64%   ');
		$pdf->text($x+335, $y+24, '  0-49%   ');
		$pdf->text($x+470, $y+24, '     ');
		#row 3 text
		$pdf->text($x+2, $y+37, 'Grade Points  '); 
		$pdf->text($x+105, $y+37, '  4   ');
		$pdf->text($x+175, $y+37, '  3  ');
		$pdf->text($x+265, $y+37, '  2   ');
		$pdf->text($x+345, $y+37, '  0   ');
		$pdf->text($x+480, $y+37, '     ');		
		#row 4 text
		$pdf->text($x+2, $y+50, 'Remarks  '); 
		$pdf->text($x+95, $y+50, '  Excellent   ');
		$pdf->text($x+165, $y+50, '  Good  ');
		$pdf->text($x+265, $y+50, '  Average   ');
		$pdf->text($x+320, $y+50, '  Failure   ');
		$pdf->text($x+455, $y+50, '  Failure   ');
?>
