<?php
	$pdf->text(190.28, $yind, '          ######## END OF PROGRESS REPORT ########');  
  $pdf->setFont('Arial', 'I', 9);    
 
   $pdf->text(50, $yind+15, '1. This is the provisional statement of results and is not an Academic Transcript. The Institute reserves the right to correct') ;
$yind = $yind+15;
     $pdf->text(55, $yind+12, 'the information given in this statement which will be confirmed by the issue of Academic Transcript');

	$pdf->text(50, $yind + 24, '2. Key for Course Units: ONE UNIT IS EQUIVALENT TO 15 CONTACT HOURS.  '); 	
	$pdf->text(110, $yind + 36, 'POINTS = GRADE POINTS MULTIPLIED BY NUMBER OF UNITS.');  
	$pdf->text(50, $yind + 48, '3.	Key to the Grades and other Symbols for Institute Examinations: SEE THE TABLE BELOW ');
	$x=50;
	$y= $yind + 54;
	#table 1
	if($levelcode==4 || $levelcode==5){
		include 'gradescale2.php';
		}
	else{
		include 'gradescale.php';
		}

	
?>
