<?php
 /** PHPExcel */
    require_once '../Classes/PHPExcel/IOFactory.php';
    $objPHPExcel = PHPExcel_IOFactory::load("./temp/$filename");echo "<pre>";
        function convertSerialDate($bdate)  {
        $dobi = ($bdate - 25569) * 86400;
        return date("Y-m-d",$dobi);
    }

    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
    {
        $worksheetTitle = $worksheet->getTitle();
        $highestRow = $worksheet->getHighestRow();
        $highestColumn = $worksheet->getHighestColumn();
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        $nrColumns = ord($highestColumn) - 64;
        echo "<br>The worksheet " . $worksheetTitle . " has ";
        echo $nrColumns . ' columns (A-' . $highestColumn . ') ';
        echo ' and ' . $highestRow . ' row.';

         echo '<br>Data: <table border="1"><tr>';
            echo '<tr>';
            $q = $zalongwa->query("SELECT ProgrammeName FROM programme WHERE ProgrammeCode='$impprog'");
            $gprog = $q->fetch_assoc();
            $pg = explode(".", $gprog['ProgrammeName']);
            $scode = $pg[0];
            $pname = $pg[1];

                    //programme code in array, add as many as you can
                    # these programmes are not yet allocated 105,106,107 i gave them default value 06

                    $certificate_codes_one_semester = array(100);
                    $certificate_codes = array(101);
                    $diploma_codes = array(102);
                    $bachelor_codes = array(103);
                    $postgraduate_codes = array(104,108,109);

                    if(in_array($impprog,$certificate_codes_one_semester)){
                          $programme_level = '01';
                        }elseif(in_array($impprog,$certificate_codes)){
                          $programme_level = '02';
                        }elseif(in_array($impprog,$diploma_codes)){
                          $programme_level = '03';
                        }elseif(in_array($impprog,$bachelor_codes)){
                          $programme_level = '04';
                        }elseif(in_array($impprog,$postgraduate_codes)){
                          $programme_level = '05';
                        }else{
                          $programme_level = '06';
                            }

                      $array_mwaka = explode("/", $impayear);
                      $array_mwaka = $array_mwaka[0];
                      $array_mwaka = substr($array_mwaka,-2);

                for ($i = 2; $i <= $highestRow; ++$i)
                {
                  $feedback = true;

                 if ($overwrite==1 AND $nrColumns>5){

                    # get the value of serial number
                $cell = $worksheet->getCellByColumnAndRow(0, $i);
                $sno = trim($cell->getValue());

                $cell = $worksheet->getCellByColumnAndRow(1, $i);
                $fname = trim($cell->getValue());

                $cell = $worksheet->getCellByColumnAndRow(2, $i);
                $mname = trim($cell->getValue());

                # get the value of Surname
                $cell = $worksheet->getCellByColumnAndRow(3, $i);
                $surname = trim($cell->getValue());

                $cell = $worksheet->getCellByColumnAndRow(4, $i);
                $sex = trim($cell->getValue());

                $cell = $worksheet->getCellByColumnAndRow(5, $i);
                $sponsor = trim($cell->getValue());

                $cell = $worksheet->getCellByColumnAndRow(6, $i);
                $bdate = ($cell->getValue());

                $cell = $worksheet->getCellByColumnAndRow(7, $i);
                $f4index = trim($cell->getValue());

                $cell = $worksheet->getCellByColumnAndRow(8, $i);
                $f4year = trim($cell->getValue());

                $cell = $worksheet->getCellByColumnAndRow(9, $i);
                $f6index = trim($cell->getValue());

                $cell = $worksheet->getCellByColumnAndRow(10, $i);
                $f6year = trim($cell->getValue());

                $cell = $worksheet->getCellByColumnAndRow(11, $i);
                $regdate = trim($cell->getValue());
                $regdate = convertSerialDate($regdate);

                $cell = $worksheet->getCellByColumnAndRow(12, $i);
                $nation = trim($cell->getValue());

                $dob = convertSerialDate($bdate);

                #arrange the name in the right format (i.e. SURNAME, Firstname Othernames).
                $surname=trim(addslashes($surname));
                $fname=trim(addslashes(strtolower($fname)));
                $mname=trim(addslashes(strtolower($mname)));
                if($mname<>""){
                    $othername = $fname." ".$mname;
                }
                else{
                    $othername = $fname;
                }
                      $name=strtoupper($surname).", ".ucwords($othername);

               #generate number
               while ($feedback == true){
                      $get_last_number = $zalongwa->query("SELECT RegNo FROM student WHERE entryyear='$impayear' AND ProgrammeOfStudy='$impprog' ORDER BY RegNo DESC Limit 1");

                      $rows_available = $get_last_number->num_rows;

                      if($rows_available  == '0'){
                          #if No Last student start to auto-generate new numbers
                          $number = '0001';
                     }else{
                           #filter for the largest registration number
                         list($numlist) = $get_last_number->fetch_array();
                              $numlist = explode("-",$numlist);
                              $actual_number = $numlist[1];
                              $actual_number = $actual_number+1;
                               if(strlen($actual_number) == '1'){
                                   $number = "000".$actual_number;
                                   }
                               elseif(strlen($actual_number) == '2'){
                                   $number = "00".$actual_number;
                                   }
                               elseif(strlen($actual_number) == '3'){
                                   $number = "0".$actual_number;
                                   }
                          else{
                              #echo end of number format limit
                              $number = $actual_number;
                              }
                    }

                  $regno = $programme_level."-".$number."-".$array_mwaka;
                  $check = $zalongwa->query("SELECT RegNo FROM student WHERE RegNo='$regno'");
                  $rows_fetched = $check->num_rows;

                    if($rows_fetched  == '0'){
                         $feedback = false;
                      }else{
                   $feedback = true;
                    $regno = '';
              }

}
                  $gender = array('M','F');
                  $bool = in_array($sex,$gender);
                  if(!$bool){
                            echo $sex;
                            exit;
                          echo "<tr><td nowrap>Row ".$i.' - '.$regno."</td><td nowrap>has <b>Unrecognised Gender</b></td><td nowrap> - Not Imported!</td></tr>";
                          }
                  else{
                  if($rows_fetched == '0'){
                      $get_last_number = $zalongwa->query("SELECT RegNo FROM student WHERE entryyear='$impayear' AND ProgrammeOfStudy='$impprog' ORDER BY RegNo ASC Limit 1");
                      $rows_fetched = $get_last_number->num_rows;
                      $sql ="INSERT INTO student SET
                                                    EntryYear='$impayear',
                                                    ProgrammeofStudy = '$impprog',
                                                    user = '$username',
                                                    Received = '$regdate',
                                                    Sponsor = '$sponsor',
                                                    AdmissionNo = '$regno',
                                                    RegNo = '$regno',
                                                    Sex = '$sex',
                                                    Name = '$name',
                                                    DBirth='$dob',
                                                    form4no='$f4index',
                                                    f4year='$f4year',
                                                    form6no='$f6index',
                                                    f6year='$f6year',
                                                    YearOfStudy='$impyearofstudy ',
                                                    Nationality='$nation',
                                                    Status ='3'
                                                    ";
                     $sql2="INSERT INTO class SET RegNo='$regno',YearOfStudy='$impyearofstudy',AYear='$impayear',Recorder='$username'";
                    }
                    else{
                        $sql ="UPDATE student SET
                                                    EntryYear='$impayear',
                                                    Sponsor = '$sponsor',
                                                    ProgrammeofStudy = '$impprog',
                                                    user = '$username',
                                                    Received = '$regdate',
                                                    AdmissionNo = '$regno',
                                                    RegNo = '$regno',
                                                    Sex = '$sex',
                                                    Name = '$name',
                                                    DBirth='$dob',
                                                    form4no='$f4index',
                                                    f4year='$f4year',
                                                    form6no='$f6index',
                                                    f6year='$f6year',
                                                    YearOfStudy='$impyearofstudy ',
                                                    Nationality='$nation',
                                                    Status ='3'
                                                    WHERE RegNo = '$regno'";
                    $sql2="UPDATE class
                     SET RegNo='$regno',YearOfStudy='$impyearofstudy',AYear='$impayear',Recorder='$username' WHERE  RegNo='$regno'";
                    }
                    # update class table
                    $zalongwa->query($sql);
                    $zalongwa->query($sql2);
                    if($zalongwa->connect_error) {
                         echo "<tr><td nowrap>Row ".$i.': '.$surname.' ('.$regno.')'."</td><td nowrap> is Not Imported! Due to</td><td nowrap>".$zalongwa->connect_error."</td>";
                        }
                    else{
                        $zalongwa->query($sql2);
                         echo "<tr><td nowrap>Row ".$i.': '.$surname.' ('.$regno.')'.' '.$sponsor." </td><td nowrap>Imported Successfuly!</td><td></td></tr>";
                        }
                      }
              }
              else{
                  if($nrColumns < 5){
                      echo "<tr><td nowrap>Row ".$i.': '.$surname.' ('.$regno.')'."</td><td nowrap>has <b>Less Columns</b></td><td nowrap> - Not Imported!</td></tr>";
                      }
                  else{
                      # get the value of serial number
                    $cell = $worksheet->getCellByColumnAndRow(0, $i);
                    $sno = trim($cell->getValue());

                    # get the value of Surname
                    $cell = $worksheet->getCellByColumnAndRow(1, $i);
                    $surname = strtoupper(strtolower(trim($cell->getValue())));

                    $cell = $worksheet->getCellByColumnAndRow(2, $i);
                    $othername = trim($cell->getValue());
                    $othernamearray=explode(' ',$othername);
                    $othername=(count($othername>1)) ? ucfirst(strtolower($othernamearray[0])).' '.ucfirst(strtolower($othernamearray[1])) : ucfirst(strtolower($othernamearray[0]));
                    $cell = $worksheet->getCellByColumnAndRow(3, $i);
                    $sex = trim($cell->getValue());

                    $cell = $worksheet->getCellByColumnAndRow(4, $i);
                    $sponsor = trim($cell->getValue());

                    #arrange the name in the right format (i.e. SURNAME, Firstname Othernames).
                    $surname=addslashes($surname);
                    $othername=addslashes($othername);
                    $name=$surname.", ".$othername;
                    $sex = trim(strtoupper($sex));

                     // $get_last_number = $zalongwa->query("SELECT RegNo FROM student WHERE entryyear='$impayear' AND ProgrammeOfStudy='$impprog' ORDER BY RegNo ASC");
                     // $rows_fetched = ->num_rows($get_last_number);




                      $entryyear = substr($impayear,0,4);
                     // $regno = $scode."/".$entryyear."/".$pname." ".$number;

                      #|------------------------------------------------------
                      #| RECORDING OF NEW STUDENT IN DATABASE
                      #|_______________________________________________________

                                                // session= '$session',
                      $sql ="INSERT INTO student SET
                                                EntryYear='$impayear',
                                                Sponsor = '$sponsor',
                                                ProgrammeofStudy = '$impprog',
                                                user = '$username',
                                                Received = '$today',
                                                AdmissionNo = '$regno',
                                                RegNo = '$regno',
                                                Sex = '$sex',
                                                Name = '$name',
                                                DBirth='$dob',
                                                form4no='$f4index',
                                                f4year='$f4year',
                                                form6no='$f6index',
                                                f6year='$f6year',
                                                YearOfStudy='$impyearofstudy ',
                                                Status ='3'
                                                ";
                                                // ,session='$session'
                      $sql2="INSERT INTO class SET RegNo='$regno',YearOfStudy='$impyearofstudy',AYear='$impayear',Recorder='$username'";
                       if($number == '000'){
                          echo "<tr><td nowrap>Row ".$i.': '.$surname.' ('.$regno.')'."</td><td> <b>Registration number auto generation has reached max limit</td><td> - Not Imported!</td></tr>";
                          }
                      else{
                         // $zalongwa->query($sql);
                          if($zalongwa->connect_error) {
                              echo "<tr><td nowrap>Row ".$i.': '.$surname.' ('.$regno.')'."</td><td nowrap> is Not Imported! Due to</td><td nowrap>".$zalongwa->connect_error."</td>";
                              }
                          else{
                              # insert into class table
                              $zalongwa->query($sql2);
                               echo "<tr><td nowrap>Row ".$i.': '.$surname.' ('.$regno.')'.' '.$sponsor." </td><td nowrap>Imported Successfuly!</td><td></td></tr>";
                              }
                          }
                      }
                }
            }
        }
        // SELECT RegNo, AYear FROM class  WHERE RegNo IN (SELECT RegNo FROM student WHERE ProgrammeofStudy IN (SELECT ProgrammeID FROM courseprogramme WHERE AYear='2014/2015' AND CourseCode='ACC 111' AND session='Day') ORDER BY student.Name) AND AYear='2014/2015'
?>
