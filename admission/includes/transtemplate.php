<?php
	#signatory
	$signatory = '                              Date                                                                REGISTRAR';

	$pdf->image('images/logo.jpg', 260, 66);   
	$pdf->setFont('Arial', 'I', 8);     
	$pdf->text(530.28, 820.89, 'Page '.$pg);   
	$pdf->setFont('Arial', 'B', 26);   	
         include('../includes/orgname.php');
      // $pdf->text(66, 50, strtoupper($org));   
  	$pdf->setFont('Arial', 'I', 8);     
	$pdf->text(50, 820.89, 'Dar es Salaam, '.$today = date("d-m-Y H:i:s"));  
	$pdf->text(300, 820.89, $copycount);   
 
	$yadd=109;
	#University Addresses
	$post = 'P. O. Box 1968';
	$website = 'http://www.cbe.ac.tz';
	$pdf->setFont('Arial', '', 11.3);     
	$pdf->text(105, $yadd, 'Phone: +255-22-2150177');
	$pdf->text(105, $yadd+12, 'Fax: +255-22-2150122');    
	$pdf->text(105, $yadd+24, 'Email: principalcbe@cbe.ac.tz');
	$pdf->text(350, $yadd, strtoupper($post));    
	$pdf->text(350, $yadd+12, strtoupper($city)); 
	$pdf->text(350, $yadd+24, $website);  
	
	#candidate photo box
	$pdf->line(490, 59, 490, 139);       // leftside. 
	$pdf->line(490, 59, 570, 59);        // upperside. 
	$pdf->line(570, 59, 570, 139);       // rightside. 
	$pdf->line(490, 139, 570, 139);       // bottom side. 
	if ($nophoto == 1){
		$pdf->text(492, 85, 'Invalid  ');  
		$pdf->text(492, 99, 'Without Photo: ');  
	}else{
		$pdf->image($imgfile, 490, 58);  
	}
?>
