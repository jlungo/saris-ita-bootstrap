<?php
/*		//header
		#reset values of x,y
		//$x=50; $y=$y+48;
 		#table course details
		$pdf->line($x, $y, 570.28, $y);
		$pdf->line($x, $y+15, 570.28, $y+15); 
		$pdf->line($x, $y+30, 570.28, $y+30); 
		$pdf->line($x, $y, $x, $y+30); 
		$pdf->line($x+68, $y, $x+68, $y+30);
		$pdf->line($x+468, $y, $x+468, $y+30);
		$pdf->line(570.28, $y, 570.28, $y+30);
		$pdf->setFont('Arial', 'B', 13); 
			$pdf->text($x, $y+12, 'Code'); 
			$pdf->text($x+70, $y+12, 'Module Name'); 
			$pdf->text($x+470, $y+12, 'Credits'); 
		$pdf->setFont('Arial', '', 11); 
		$pdf->text($x, $y+27, $coursecode); 
		$pdf->text($x+70, $y+27, $coursename); 
		$pdf->text($x+470, $y+27, $courseunit);

		#reset the value of y
		$y=$y+30;
		//ends here
*/
		
		#control headers
		if($coursecode=='CET 04106' || $coursecode=='EET 306' || $coursecode=='EET 406' || $coursecode=='EET 410'){
			$CA = "CA/60";
			$SE = "SE/40";
		}
		elseif($coursecode=='EEB 104' || $coursecode=='EET 305' || $coursecode=='EET 307' || $coursecode=='ETU 07314' || $coursecode=='ETU 07316' || $coursecode=='EET 04204' || $coursecode=='EET 04104' || $coursecode=='EET 05203'){
			$CA = "CA/50";
			$SE = "SE/50";
		}
		else{
			$CA = "CA/40";
			$SE = "SE/60";
		}
		
		#if exam type is Final Exam
		$pdf->setFont('Arial', '', 11); 
		$pdf->line($x+267, $y, 513, $y);
		$pdf->line($x+267, $y+15, 513, $y+15); $pdf->text($x+320, $y+12, 'MARKS AS PER');
		$pdf->setFont('Arial', '', 7); 
		$pdf->line($x+267, $y, $x+267, $y+30); $pdf->text($x+273, $y+25, 'COURSE WORK');
		$pdf->line($x+336, $y+30, $x+336, $y+15); $pdf->text($x+342, $y+22, 'FINAL EXAM'); $pdf->text($x+358, $y+28, 'I/E'); 
		$pdf->line($x+400, $y+30, $x+400, $y+15); $pdf->text($x+406, $y+22, 'FINAL EXAM'); $pdf->text($x+422, $y+28, 'E/E'); 
		//$pdf->line($x+336, $y, $x+336, $y+15);
		$pdf->line(513, $y, 513, $y+30);
		 		
		//$pdf->text($x+470, $y+27, $courseunit);
		$y=$y+30;

		$pdf->setFont('Arial', 'B', 9); 
		$pdf->line($x, $y, 570.28, $y); 
		$pdf->line($x, $y+30, 570.28, $y+30);		 
		$pdf->line($x, $y, $x, $y+30); 		$pdf->text($x+2, $y+12, 'S/No');
		$pdf->line($x+30, $y, $x+30, $y+30);	$pdf->text($x+35, $y+12, 'Name');
		$pdf->line($x+173, $y, $x+173, $y+30);	$pdf->text($x+177, $y+12, 'Sex'); 
		$pdf->line($x+200, $y, $x+200, $y+30);	$pdf->text($x+202, $y+12, 'RegNo'); 
		$pdf->line($x+267, $y, $x+267, $y+30);	$pdf->text($x+270, $y+12, $CA); //$pdf->text($x+312, $y+24, 'X/40');//-40
		$pdf->line($x+301, $y, $x+301, $y+30);	$pdf->text($x+303, $y+12, 'CA/100'); //$pdf->text($x+312, $y+24, 'X/40');//-40
		$pdf->line($x+336, $y, $x+336, $y+30);	$pdf->text($x+338, $y+12, $SE); //$pdf->text($x+342, $y+24, 'X/100');
		$pdf->line($x+366, $y, $x+366, $y+30);	$pdf->text($x+368, $y+12, 'SE/100'); //$pdf->text($x+374, $y+24, 'X/60');/-3,2
		$pdf->line($x+400, $y, $x+400, $y+30);	$pdf->text($x+402, $y+12, $SE); //$pdf->text($x+402, $y+24, 'X/100');
		$pdf->line($x+430, $y, $x+430, $y+30);	$pdf->text($x+432, $y+12, 'SE/100'); 
		$pdf->line($x+463, $y, $x+463, $y+30);	$pdf->text($x+465, $y+12, 'Remark');
		$pdf->line(570.28, $y, 570.28, $y+30);   
		$pdf->setFont('Arial', '', 9); 
      
		#get coursename
		$qcourse = "Select CourseName, Department, StudyLevel from course where CourseCode = '$coursecode'";
		$dbcourse = mysql_query($qcourse);
		$row_course = mysql_fetch_array($dbcourse);
		$coursename = $row_course['CourseName'];
		$coursefaculty = $row_course['Department'];

		#initiate grade counter
		$countgradeA=0;
		$countgradeBplus=0;
		$countgradeB=0;
		$countgradeC=0;
		$countgradeD=0;
		$countgradeE=0;
		$countgradeF=0;
		$countgradeI=0;

		$countgradeAm=0;
		$countgradeBplusm=0;
		$countgradeBm=0;
		$countgradeCm=0;
		$countgradeDm=0;
		$countgradeEm=0;
		$countgradeFm=0;
		$countgradeIm=0;

		$countgradeAf=0;
		$countgradeBplusf=0;
		$countgradeBf=0;
		$countgradeCf=0;
		$countgradeDf=0;
		$countgradeEf=0;
		$countgradeFf=0;
		$countgradeIf=0;
		#print title
		$sn=0;
		while($row_regno = mysql_fetch_array($dbregno)){
				$key= $row_regno['RegNo'];
				$course= $coursecode;
				$ayear = $year;
				$units= $row_course['Units'];
				$sn=$sn+1;
				$remarks = 'remarks';
				$grade='';

				#get name and sex of the candidate
				$qstudent = "SELECT Name, Sex from student WHERE RegNo = '$key'";
				$dbstudent = mysql_query($qstudent); 
				$row_result = mysql_fetch_array($dbstudent);
				$name = $row_result['Name'];
				$sex = strtoupper($row_result['Sex']);
				
				# grade marks
				$RegNo = $key;
				include 'includes/choose_studylevel.php';

			  #update grade counter
			   if ($grade=='A'){
				$countgradeA=$countgradeA+1;
					if($sex=='M'){
						$countgradeAm=$countgradeAm+1;
					}else{
						$countgradeAf=$countgradeAf+1;
					}
				}elseif($grade=='B+'){
					$countgradeBplus=$countgradeBplus+1;
					if($sex=='M'){
						$countgradeBplusm=$countgradeBplusm+1;
					}else{
						$countgradeBplusf=$countgradeBplusf+1;
					}
				}elseif($grade=='B'){
					$countgradeB=$countgradeB+1;
					if($sex=='M'){
						$countgradeBm=$countgradeBm+1;
					}else{
						$countgradeBf=$countgradeBf+1;
					}
			    }elseif($grade=='C'){
					$countgradeC=$countgradeC+1;
					if($sex=='M'){
						$countgradeCm=$countgradeCm+1;
					}else{
						$countgradeCf=$countgradeCf+1;
					}
			   }elseif($grade=='D'){
					$countgradeD=$countgradeD+1;
					if($sex=='M'){
						$countgradeDm=$countgradeDm+1;
					}else{
						$countgradeDf=$countgradeDf+1;
					}
			   }elseif($grade=='E'){
					$countgradeE=$countgradeE+1;
					if($sex=='M'){
						$countgradeEm=$countgradeEm+1;
					}else{
						$countgradeEf=$countgradeEf+1;
					}
			   }elseif($grade=='F'){
					$countgradeF=$countgradeF+1;
					if($sex=='M'){
						$countgradeFm=$countgradeFm+1;
					}else{
						$countgradeFf=$countgradeFf+1;
					}
			   }else{
					$countgradeI=$countgradeI+1;
					if($sex=='M'){
						$countgradeIm=$countgradeIm+1;
					}else{
						$countgradeIf=$countgradeIf+1;
					}
				}
			 // }
			 
				
		#display results
		
		#calculate summary areas
		$yind = $y+15;
		$dataarea = 820.89-$yind;
		if ($dataarea< 20){
				$pdf->addPage();  
	
				$x=50;
				$y=50;
				$pg=$pg+1;
				$tpg =$pg;
				$pdf->setFont('Arial', 'I', 8);     
				$pdf->text(530.28, 820.89, 'Page '.$pg);  
				$pdf->text(300, 820.89, $copycount);    
				$pdf->text(50, 825.89, 'Printed On '.$today = date("d-m-Y H:i:s"));   
				$yind = $y; 
				$pdf->setFont('Arial', '', 10);  
				#reset the value of y
				#if exam type is Final Exam
				$pdf->setFont('Arial', 'B', 9); 
				$pdf->line($x, $y, 570.28, $y); 
				$pdf->line($x, $y+30, 570.28, $y+30);
				//$pdf->line($x+270,)
				//$pdf->line($x, $y, 570.28, $y+30);
				$pdf->line($x, $y, $x, $y+30); 		$pdf->text($x+2, $y+12, 'S/No');
				$pdf->line($x+30, $y, $x+30, $y+30);	$pdf->text($x+35, $y+12, 'Name');
				$pdf->line($x+173, $y, $x+173, $y+30);	$pdf->text($x+177, $y+12, 'Sex'); 
				$pdf->line($x+200, $y, $x+200, $y+30);	$pdf->text($x+202, $y+12, 'RegNo');
				$pdf->line($x+267, $y, $x+267, $y+30);	$pdf->text($x+272, $y+12, $CA); //$pdf->text($x+312, $y+24, 'X/40');//-40
				$pdf->line($x+301, $y, $x+301, $y+30);	$pdf->text($x+302, $y+12, 'CA/100'); //$pdf->text($x+312, $y+24, 'X/40');//-40
				$pdf->line($x+336, $y, $x+336, $y+30);	$pdf->text($x+338, $y+12, $SE); //$pdf->text($x+342, $y+24, 'X/100');
				$pdf->line($x+366, $y, $x+366, $y+30);	$pdf->text($x+368, $y+12, 'SE/100'); //$pdf->text($x+374, $y+24, 'X/60');
				$pdf->line($x+400, $y, $x+400, $y+30);	$pdf->text($x+402, $y+12, $SE); //$pdf->text($x+402, $y+24, 'X/100');
				$pdf->line($x+430, $y, $x+430, $y+30);	$pdf->text($x+432, $y+12, 'SE/100'); 
				$pdf->line($x+463, $y, $x+463, $y+30);	$pdf->text($x+465, $y+12, 'Remark');
				$pdf->line(570.28, $y, 570.28, $y+30);   
				$pdf->setFont('Arial', '', 9); 
		}
		if ($test2score ==-1){
			$test2score = 'PASS';
		}
		if ($aescore ==-1){
			$aescore = 'PASS';
		} 
		if ($marks == -2) {
			$marks = 'PASS'; 
		}
		$y=$y+15;
			$pdf->setFont('Arial', '', 8.7);    
		$pdf->line($x, $y, 570.28, $y);
		$pdf->line($x, $y+15, 570.28, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, $sn);
		$pdf->line($x+30, $y, $x+30, $y+15);	
				if ($show=='Y') {
				$stname = explode(',',$name);
				$pdf->text($x+35, $y+12, strtoupper($stname[0]).', '.ucwords(strtolower($stname[1])));
				}
		
		//CONTROL  THE MARKS PRINTING
		if($coursecode=='CET 04106' || $coursecode=='EET 306' || $coursecode=='EET 406' || $coursecode=='EET 410'){
			$CAscore = round($test2score*5/3,1);
			$aescore=number_format($aescore100*40/100,1);
		}
		elseif($coursecode=='EEB 104' || $coursecode=='EET 305' || $coursecode=='EET 307' || $coursecode=='EET 04203' || $coursecode=='ETU 07314' || $coursecode=='ETU 07316' || $coursecode=='EET 04204' || $coursecode=='EET 04104' || $coursecode=='EET 05203'){
			$CAscore = round($test2score*2,1);
			$aescore=number_format($aescore100*50/100,1);
		}
		else{
			$CAscore = round($test2score*2.5,1);
		}
		
		$pdf->line($x+173, $y, $x+173, $y+15);	$pdf->text($x+181, $y+12, strtoupper($sex)); //-10
		$pdf->line($x+200, $y, $x+200, $y+15);	$pdf->text($x+202, $y+12, strtoupper($key)); //-10
		$pdf->line($x+267, $y, $x+267, $y+15);	$pdf->text($x+275, $y+12, $test2score); //-43
		$pdf->line($x+301, $y, $x+301, $y+15);	$pdf->text($x+308, $y+12, $CAscore); //-3,2
		$pdf->line($x+336, $y, $x+336, $y+15);	$pdf->text($x+341, $y+12, $aescore);
		$pdf->line($x+366, $y, $x+366, $y+15);	$pdf->text($x+372, $y+12, $aescore100);
		$pdf->line($x+400, $y, $x+400, $y+15);	$pdf->text($x+405, $y+12, ''); //$marks
		$pdf->line($x+430, $y, $x+430, $y+15);	$pdf->text($x+439, $y+12, ''); //$grade
			#check CA specific remarks
			if($caremark==1){
				$remark='Failed CWK';
			}
		$pdf->line($x+463, $y, $x+463, $y+15);	$pdf->text($x+465, $y+12, $remark);
		$pdf->line(570.28, $y, 570.28, $y+15);   
		$pdf->setFont('Arial', '', 10);
		$remark='';
	}
	
		# results summary table 
				
		#print signature lines
		$pdf->text(80.28, $y+45, 'I/E .............................................................SIGNATURE............................DATE.......................');    						
		$pdf->text(80.28, $y+60, 'E/E.............................................................SIGNATURE............................DATE.......................');    	
?>
