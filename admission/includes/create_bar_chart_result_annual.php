<?php
class Create_bar {
	
	
	
function create_image($ydata1,$ydata2,$xdata=''){

include_once  '../../jpgraph/src/jpgraph.php';
include_once  '../../jpgraph/src/jpgraph_bar.php';




// Size of the overall graph
$width=620;
$height=300;

// Create the graph and set a scale.
// These two calls are always required
$graph = new Graph($width,$height);
$graph->SetScale('textlin');


// Setup margin and titles
$graph->SetMargin(40,20,20,40);
$graph->title->Set('OCCURANCE OF GRADES BY GENDER');
$graph->xaxis->title->Set('Grades');
$graph->yaxis->title->Set('# Grades');

$graph->yaxis->title->SetFont( FF_FONT1 , FS_BOLD );
$graph->xaxis->title->SetFont( FF_FONT1 , FS_BOLD );

$graph->xaxis->SetTickLabels($xdata);

$graph->yaxis->SetColor('blue');
$graph->xaxis->SetColor('blue');


$bp1  = new BarPlot($ydata1);

$bp1->SetFillGradient("navy","lightsteelblue",GRAD_WIDE_MIDVER);
$bp1->SetLegend('MALE');


$bp2  = new BarPlot($ydata2);
$bp2->SetFillGradient("navy","green",GRAD_WIDE_MIDVER);
$bp2->SetLegend('FEMALE');


$groupbar = new GroupBarPlot(array($bp1,$bp2));

//
$graph->Add( $groupbar);

$bp1->SetFillColor('lightsteelblue');
$bp1->value->show();
$bp2->SetFillColor('green');
$bp2->value->show();


// Display the graph
$graph->Stroke('images/bar.png');


	}
}
?>