<?php
#get connected to the database and verfy current session
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');

# initialise globals

global $szSection, $szSubSection;
$szSection = 'Admission Process';
$szSubSection = 'Import Student';
$szTitle = 'Import Students Class List';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /* background-color: #009688; */
        }
        .row {
            margin-top: 20px;
        }
        .card {
            /* background-color: #324359; */
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /* color: white; */
            padding: 0px;
            border-radius: 0px !important;
        }

        @media (max-width: 34em) {
            .card {
                margin-top: 20px;
            }
        }

        @media (max-width: 48em) {
            .card {
                margin-top: 20px;
            }
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>

    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'admissionNavBar.php'; ?>

<div class="container ">
    <div class="row ">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <h3 class="card-header">
                    <?php echo $szSubSection; ?>
                </h3>
                <div class="card-block">
                    <?php
                    if (isset($_POST['import']) && ($_POST['import'] == "Import Data")) {

                        #STEP 1.0 Upload data file

                        #constants
                        $fileavailable = 0;

                        #validate the file
                        $file_name = ($_POST['userfile']);
                        $overwrite = addslashes($_POST['checkbox']);

                        $file = $_FILES['userfile']['name'];
                        //The original name of the file on the client machine.

                        $filetype = $_FILES['userfile']['type'];
                        //The mime type of the file, if the browser provided this information. An example would be "image/gif".

                        $filesize = $_FILES['userfile']['size'];
                        //The size, in bytes, of the uploaded file.

                        $filetmp = $_FILES['userfile']['tmp_name'];
                        //The temporary filename of the file in which the uploaded file was stored on the server.

                        $filetype_error = $_FILES['userfile']['error'];

                        $filename = time() . $_FILES['userfile']['name'];
                        ?>
                        <?php
                        // In PHP earlier than 4.1.0, $HTTP_POST_FILES  should be used instead of $_FILES.
                        if (is_uploaded_file($filetmp)) {
                            $filename = time() . $file;
                            copy($filetmp, "$filename");
                        } else {
                            echo "<p style='color:maroon'>File not uploaded, error(" . $filetype_error . ") <br>The chosen file is not a CSV file</p>";
                        }
                        move_uploaded_file($filetmp, "./temp/$filename");

                        #check file extension

                        $str = $filename;
                        $i = strrpos($str, ".");
                        if (!$i) {
                            return "";
                        }

                        $l = strlen($str) - $i;
                        $ext = substr($str, $i + 1, $l);

                        $pext = strtolower($ext);
                        if ($pext != "xls") {

                            print "<h2>ERROR</h2>File Extension Unknown.<br>";
                            print "<p>Please Upload a File with the Extension .csv ONLY<br>";
                            print "To convert your Excel File to csv, go to File -> Save As, then Select Save as Type CSV (Comma delimeded) (*.csv)</p>\n";
                            print "The file you uploaded have this extension: $pext</p>\n";
                            echo '<meta http-equiv = "refresh" content ="10; url = admissionImport.php">';
                            include('../footer/footer.php');
                            unlink($filename);
                            exit();

                        }

                        ?>
                        <?php
                        if ($pext == "xls") {
                            $fileavailable = 1;
                        }

                        #STEP 2.0 exceute sql scripts
                        if ($fileavailable == 1) {
                            echo "<strong>Data Import in Process</strong><br /><br>";

                            echo $file . " Import Progress Status.............<br>";

                            #get impo year
                            $impayear = addslashes($_POST['cohort']);

                            #get impo programme
                            $impprog = addslashes($_POST['programme']);

                            #get impo yearofstudy
                            $impyearofstudy = addslashes($_POST['yearofstudy']);

                            #get indicator with number or not
                            $withnumber = addslashes($_POST['withnumber']);


                            if ($impprog == 0 && $impayear == 0) {
                                echo "<p style='color:maroon'><b>ERROR:</b> Select Study Programme and Intake Year First</p>";
                                include('../footer/footer.php');
                                exit;
                            }

                            if ($impprog == 0) {
                                echo "<p style='color:maroon'><b>ERROR:</b> Select Study Programme First</p>";
                                include('../footer/footer.php');
                                exit;
                            }

                            if ($impayear == 0) {
                                echo "<p style='color:maroon'><b>ERROR:</b> Select Study Intake Year First</p>";
                                include('../footer/footer.php');
                                exit;
                            }

                            $fcontents = file("./temp/$filename");
                            # expects the csv file to be in the same dir as this script

                            echo "<table border='0' cellpadding='3' cellspacing='0'>";
                            // //////////////////import start here///////////////////////
                            if (!is_null($withnumber) && $withnumber == 1) {
                                # existing student with their reg no
                                include 'includes/2014_above_import_with_number.php';

                            } else {
                                # new student with autogenerated reg no
                                include 'includes/2014_above_import.php';
                            }

                            echo "</table>";
                            echo "<br /><br /><strong>Zalongwa Data Import Process Completed</strong>";
                            unlink("./temp/$filename");
                            unlink("$filename");

                        }

                    } else {
                        function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
                        {
                            $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

                            switch ($theType) {
                                case "text":
                                    $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                                    break;
                                case "long":
                                case "int":
                                    $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                                    break;
                                case "double":
                                    $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                                    break;
                                case "date":
                                    $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                                    break;
                                case "defined":
                                    $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                                    break;
                            }
                            return $theValue;
                        }

                        $editFormAction = $_SERVER['PHP_SELF'];
                        if (isset($_SERVER['QUERY_STRING'])) {
                            $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
                        }


                        $query_AcademicYear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
                        $AcademicYear = $zalongwa->query($query_AcademicYear) or die($zalongwa->connect_error);
                        $row_AcademicYear = $AcademicYear->fetch_assoc();
                        $totalRows_AcademicYear = $AcademicYear->num_rows;


                        $query_Hostel = "SELECT ProgrammeCode, ProgrammeName FROM programme ORDER BY ProgrammeName ASC";
                        $Hostel = $zalongwa->query($query_Hostel) or die($zalongwa->connect_error);
                        $row_Hostel = $Hostel->fetch_assoc();
                        $totalRows_Hostel = $Hostel->num_rows;

                        ?>
                        <form enctype="multipart/form-data" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST"
                              name="studentclasslist" id="studentclasslist">

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"
                                       for="exampleFormControlSelect1">Programme:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="programme" id="programme" required>
                                        <option value="0" disabled="disabled">--------------------------------</option>
                                        <?php
                                        do {
                                            ?>
                                            <option value="<?php echo $row_Hostel['ProgrammeCode'] ?>"><?php echo $row_Hostel['ProgrammeName'] ?></option>
                                            <?php
                                        } while ($row_Hostel = $Hostel->fetch_assoc());
                                        $rows = $Hostel->num_rows;
                                        if ($rows > 0) {
                                            mysqli_data_seek($Hostel, 0);
                                            $row_Hostel = $Hostel->fetch_assoc();
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Group Cohort:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="cohort" id="cohort" required>
                                        <option value="0" disabled="disabled">--------------------------------</option>
                                        <?php
                                        do {
                                            ?>
                                            <option value="<?php echo $row_AcademicYear['AYear'] ?>"><?php echo $row_AcademicYear['AYear'] ?></option>
                                            <?php
                                        } while ($row_AcademicYear = $AcademicYear->fetch_assoc());
                                        $rows = $AcademicYear->num_rows;
                                        if ($rows > 0) {
                                            mysqli_data_seek($AcademicYear, 0);
                                            $row_AcademicYear = $AcademicYear->fetch_assoc();
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="exampleFormControlSelect1">Year of
                                    Study:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="yearofstudy" required>
                                        <option value="" disabled="disabled">--------------------------------</option>
                                        <?php include '../includes/year_of_study.php' ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">With Registration Number:</label>
                                <div class="col-sm-8">
                                    <input name="withnumber" type="checkbox" value="1" checked>Yes
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Student File:</label>
                                <div class="col-sm-8">
                                    <input type="hidden" name="MAX_FILE_SIZE" value="55646039">
                                    <input class="form-control" name="userfile" type="file">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Templates:</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static"><a href="doc/registration.xls" target="_blank">With
                                            Registration Number&nbsp;<img
                                                    src="doc/excel.gif"></a><br/></p>
                                    <p class="form-control-static"><a href="doc/noreg.xls" target="_blank">No
                                            Registration Number&nbsp;<img
                                                    src="doc/excel.gif"></a><br/></p>
                                </div>
                            </div>
                            <div class="form-group row" style="display:none;">
                                <label class="col-sm-4 col-form-label">Existing Data:</label>
                                <div class="col-sm-8">
                                    <input name="checkbox" type="checkbox" value="1" checked>Yes
                                    Overwrite
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"></label>
                                <div class="col-sm-8">
                                    <input name="import" type="submit" id="import" value="Import Data"
                                           class="btn btn-success btn-md btn-block">
                                </div>
                            </div>
                        </form>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>

