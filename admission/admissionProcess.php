<?php
require_once '../Connections/sessioncontrol.php';
require_once '../Connections/zalongwa.php';

global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;

$szSection = 'Admission';

$szTitle = 'Admission Process';

$szSubSection = 'Admission Process';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->

    <style>


        .row {
            margin: -7%;
        }


        .row {
            margin-top: 20px;
        }

        .card {
            margin: 2%;
            padding-top: 5%;
            align-content: center;
            background: none!important;
            border: 0px !important;
        }

    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css" />
    <title>SARIS | <?php echo  $szSection ?> | <?php echo  $szSubSection ?></title>
    <!--modernaizer here-->
    <script src="modernizr-custom.js">
    </script>
</head>
<body>
<!-- navbar -->
<?php include 'admissionNavBar.php'; ?>

<div class="container text-muted">
    <!-- cards -->
    <div class="col-10 offset-1">
        <div class="row" style="margin: 10px 0">
            <div class="col-sm-4 col-md-4 ">
                <div class="card" align="center">

                    <a href="admissionSearchStudent.php"> <img class="card-img-top img-fluid "
                                                               style="width: 180px; height: inherit;"
                                                               src="./img/searchStudent.svg"></a>

                    <div class="card-block">
                        <a href="admissionSearchStudent.php"><h5 class="card-title">Search Students</h5></a>

                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-4 ">
                <div class="card" align="center">

                    <a href="admissionRegistrationForm.php"> <img class="card-img-top img-fluid "
                                                                style="width: 180px; height: inherit;"
                                                                src="./img/registrationForm.svg"></a>

                    <div class="card-block">
                        <a href="admissionRegistrationForm.php"><h5 class="card-title">Registration Form</h5></a>

                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-4">
                <div class="card" align="center">

                    <a href="admissionImport.php"> <img class="card-img-top img-fluid "
                                                            style="width: 180px; height: inherit; "
                                                            src="./img/importStudent.svg"></a>

                    <div class="card-block">
                        <a href="admissionImport.php"><h5 class="card-title">Import Student</h5></a>

                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin: 10px 0">

            <div class="col-sm-4 col-md-4">
                <div class="card" align="center">

                    <a href="admissionNominalRoll.php"> <img class="card-img-top img-fluid "
                                                             style="width: 180px; height: inherit; "
                                                             src="./img/classList.svg"></a>

                    <div class="card-block">
                        <a href="admissionNominalRoll.php"><h5 class="card-title">Class List</h5></a>

                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-4">
                <div class="card" align="center">

                    <a href="DeleteStudent.php"> <img class="card-img-top img-fluid "
                                                            style="width: 180px; height: inherit; "
                                                            src="./img/deleteStudent.svg"></a>

                    <div class="card-block">
                        <a href="DeleteStudent.php"><h5 class="card-title">Delete Students</h5></a>

                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-4">
                <div class="card" align="center">

                    <a href="RestoreStudents.php"> <img class="card-img-top img-fluid "
                                                            style="width: 180px; height: inherit; "
                                                            src="./img/restoreRecords.svg"></a>

                    <div class="card-block">
                        <a href="RestoreStudents.php"><h5 class="card-title">Restore Logs</h5></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>
<!-- end .container -->

<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>
