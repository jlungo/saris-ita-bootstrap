<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header
include('admissionMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Policy Setup';
$szSubSection = 'Policy Setup';
include("admissionheader.php");

if (isset($_GET['details'])) {
    $result = $zalongwa->query("SELECT * FROM campus WHERE CampusID=" . $_GET['details']);
    $institution = $result->fetch_assoc();
    if ($result->num_rows > 0) {
        ?>
        <table class='table_view'>
            <tr class='header'>
                <td> Field</td>
                <td> Field Value</td>
            </tr>
            <tr class='list'>
                <td> Campus</td>
                <td><?php echo $institution['Campus']; ?></td>
            </tr>
            <tr class='list'>
                <td> Location:</td>
                <td><?php echo $institution['Location']; ?></td>
            </tr>
            <tr class='list'>
                <td> Physical Address:</td>
                <td><?php echo $institution['Address']; ?></td>
            </tr>
            <tr class='list'>
                <td> Telephone:</td>
                <td><?php echo $institution['Tel']; ?></td>
            </tr>
            <tr class='list'>
                <td> Email:</td>
                <td><?php echo $institution['Email']; ?></td>
            </tr>
        </table>
        <br>
        <table class='table_view'>
            <tr style='float: right' class='list'>
                <td><a href="admissionInst.php">Back</a></td>
            </tr>
        </table>
        <?php
    } else {
        echo "Sorry, No Records Found <br>";
    }
}
# include the footer
include("../footer/footer.php");
?>