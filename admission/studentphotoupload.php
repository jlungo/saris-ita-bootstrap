<?php
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	# initialise globals
	include('admissionMenu.php');
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Admission Process';
	$szSubSection = 'Search Student';
	$szTitle = 'Search Student Record';
	include('admissionheader.php');
     function getFileExtension($str) {
        $i = strrpos($str,".");
        if (!$i) { return ""; }
        $l = strlen($str) - $i;
        $ext = substr($str,$i+1,$l);
        return $ext;
    }
if(isset($_POST['studentregno'])){
	//process scanned photo
	$studentregno = addslashes($_POST['studentregno']);
	$iddata = addslashes($_POST['iddata']);
	
	#validate the file
	$imgfile_name = addslashes($_POST['userfile']);
	$imgfile = $_FILES['userfile']['name'];
	$imgfiletype = $_FILES['userfile']['type'];
	$imgfilesize = $_FILES['userfile']['size'];
	$imgfiletmp = $_FILES['userfile']['tmp_name'];
	$imgfiletype_error = $_FILES['userfile']['error'];
	$filename=time().$_FILES['userfile']['name'];
	$extension = getFileExtension($imgfile);
	$extension = strtolower($extension);

	if (($extension != "jpg") && ($extension != "jpeg")) {
		echo '<meta http-equiv = "refresh" content ="0; url =studentphoto.php?id='.$iddata.'&RegNo='.$studentregno.'&error=Invalid Image extension only jpg,jpeg,png and gif is required">';
		exit;
	}else{
		//$filename=time().$imgfile;
		if (is_uploaded_file($imgfiletmp)) {
			//$filename=time().$imgfile;
			copy($imgfiletmp, "images/$filename");
		}else{
			move_uploaded_file($imgfiletmp, "images/$filename");
		}
		$url = "images/$filename";
		$query = "update student set Photo =  '$url' where RegNo='$studentregno'";
		$result = mysqli_query($zalongwa, $query);
		if($result){
			echo '<meta http-equiv = "refresh" content ="0; url =admissionSearchStudent.php?id='.$iddata.'&RegNo='.$studentregno.'">';
		}else{
			echo '<meta http-equiv = "refresh" content ="0; url =studentphoto.php?id='.$iddata.'&RegNo='.$studentregno.'&error=Uploaded failed, Please try again">';
		}
	}	
}else{
	//process live captured
 @$regnophoto = $_SESSION['stdphotoregno'];
 @$phtoid = $_SESSION['phtoid'];

 $image = file_get_contents('php://input');
 
 if(!$image)
 {
 	print "ERROR: Failed to read the uploaded image data.\n";
 	exit();
 }
 
 $name = date('YmdHis');
 $newname='images/'.$name.'.jpg';
 $result = file_put_contents($newname, $image);
 
 if(!$result)
 {
 	print "ERROR: Failed to write data to $filename, check permissions.\n";
 	exit();
 }
 else
 {	
 	$sql = "update student set Photo =  '$newname' where (RegNo='$regnophoto' and Id='$phtoid')";
 	$result = mysqli_query($zalongwa, $sql);
 	if($result){
 		echo '<meta http-equiv = "refresh" content ="0; url =admissionSearchStudent.php?id='.$iddata.'&RegNo='.$studentregno.'">';
 	}else{
 		echo '<meta http-equiv = "refresh" content ="0; url =studentphoto.php?id='.$iddata.'&RegNo='.$studentregno.'&error=Uploaded failed, Please try again">';
 	}
 }
 $url = 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI']).'/'.$newname;
 print "$url\n";
}
?>
