<?php
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
    $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

    switch ($theType) {
        case "text":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "long":
        case "int":
            $theValue = ($theValue != "") ? intval($theValue) : "NULL";
            break;
        case "double":
            $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
            break;
        case "date":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "defined":
            $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
            break;
    }
    return $theValue;
}

//control the display table
@$new = 2;

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
    $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmInst")) {
    $insertSQL = sprintf("INSERT INTO campus (Campus, Location, Address, Tel, Email) VALUES (%s, %s, %s, %s, %s)",
        GetSQLValueString($_POST['txtName'], "text"),
        GetSQLValueString($_POST['txtPhyAdd'], "text"),
        GetSQLValueString($_POST['txtAdd'], "text"),
        GetSQLValueString($_POST['txtTel'], "text"),
        GetSQLValueString($_POST['txtEmail'], "text"));


    $Result1 = $zalongwa->query($insertSQL) or die($zalongwa->connect_error);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmInstEdit")) {
    $updateSQL = sprintf("UPDATE campus SET Campus=%s, Location=%s, Address=%s, Tel=%s, Email=%s WHERE CampusID=%s",
        GetSQLValueString($_POST['txtName'], "text"),
        GetSQLValueString($_POST['txtPhyAdd'], "text"),
        GetSQLValueString($_POST['txtAdd'], "text"),
        GetSQLValueString($_POST['txtTel'], "text"),
        GetSQLValueString($_POST['txtEmail'], "text"),
        GetSQLValueString($_POST['id'], "int"));


    $Result1 = $zalongwa->query($updateSQL) or die($zalongwa->connect_error);

    $updateGoTo = "admissionInst.php";
    if (isset($_SERVER['QUERY_STRING'])) {
        $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
        $updateGoTo .= $_SERVER['QUERY_STRING'];
    }
    header(sprintf("Location: %s", $updateGoTo));
}

$maxRows_inst = 10;
$pageNum_inst = 0;
if (isset($_GET['pageNum_inst'])) {
    $pageNum_inst = $_GET['pageNum_inst'];
}
$startRow_inst = $pageNum_inst * $maxRows_inst;

$query_inst = "SELECT CampusID, Campus, Location, Address, Tel, Email FROM campus ORDER BY Campus ASC";
$query_limit_inst = sprintf("%s LIMIT %d, %d", $query_inst, $startRow_inst, $maxRows_inst);
$inst = $zalongwa->query($query_limit_inst) or die($zalongwa->connect_error);
$row_inst = $inst->fetch_assoc();

if (isset($_GET['totalRows_inst'])) {
    $totalRows_inst = $_GET['totalRows_inst'];
} else {
    $all_inst = $zalongwa->query($query_inst);
    $totalRows_inst = $all_inst->num_rows;
}
$totalPages_inst = ceil($totalRows_inst / $maxRows_inst) - 1;

require_once('../Connections/sessioncontrol.php');
# include the header
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Institution Information';
$szSubSection = 'Institution';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="https://cdn.datatables.net/responsive/2.2.0/css/responsive.bootstrap4.min.css">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            /* background-color: #009688; */
        }

        .card {
            /* background-color: #324359; */
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
            /* color: white; */
            padding: 0px;
            border-radius: 0px !important;
        }

        .row {
            margin-top: 20px;
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>

</head>
<body>
<!-- navbar -->
<?php include 'admissionNavBar.php'; ?>

<div class="container">
    <h3 class="h3"><?php echo $szTitle; ?></h3>
    <div>
        <p>
            <?php echo "<a href=\"admissionInst.php?new=1\" class='btn btn-success'>" ?>Add New Institution</a>
        </p>
    </div>
    <?php @$new = $_GET['new'];
    if (@$new <> 1) {
        ?>
        <table class="table table-striped table-bordered table-responsive" width="100%" cellspacing="0">
            <thead class="table-inverse">
            <tr>
                <th>Campus</th>
                <th>Location</th>
                <th>Address</th>
                <th>Tel</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            <?php do { ?>
                <tr>
                    <td class="resViewtd" nowrap><?php $id = $row_inst['CampusID'];
                        $name = $row_inst['Campus'];
                        echo "<a href=\"admissionInst.php?edit=$id\">$name</a>" ?></td>
                    <td><?php echo $row_inst['Location']; ?></td>
                    <td><?php echo $row_inst['Address']; ?></td>
                    <td><?php echo $row_inst['Tel']; ?></td>
                    <td><?php echo $row_inst['Email']; ?></td>
                </tr>
            <?php } while ($row_inst = $inst->fetch_assoc()); ?>
            </tbody>
        </table>
        <a href="<?php printf("%s?pageNum_inst=%d%s", $currentPage, max(0, $pageNum_inst - 1), $queryString_inst); ?>">Previous</a>
        <span class="style1">.............</span><?php echo min($startRow_inst + $maxRows_inst, $totalRows_inst) ?>/<?php echo $totalRows_inst ?>
        <span class="style1">..............</span><a
                href="<?php printf("%s?pageNum_inst=%d%s", $currentPage, min($totalPages_inst, $pageNum_inst + 1), $queryString_inst); ?>">Next</a>
        <br>
    <?php } else {
        ?>

        <div class="row ">
            <div class="col-sm-8">
                <div class="card">
                    <h3 class="card-header">
                        <?php echo $szTitle; ?></h3>
                    <div class="card-block">
                        <form action="<?php echo $editFormAction; ?>" method="POST" name="frmInst" id="frmInst">
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Institution</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="txtName" type="text" id="txtName" size="40">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Address</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="txtAdd" type="text" id="txtAdd" size="40">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Physical Address</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="txtPhyAdd" type="text" id="txtPhyAdd"
                                               size="40">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Telephone</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="txtTel" type="text" id="txtTel" size="40">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Email</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="txtEmail" type="text" id="txtEmail" size="40">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"></label>
                                    <div class="col-sm-8">
                                        <input class="btn btn-success btn-md btn-block" type="submit" name="Submit"
                                               value="Add Record"
                                               title="Click to Save Changes">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="MM_insert" value="frmInst">
                        </form>
                    </div>
                </div>
            </div>
        </div>

    <?php }
    if (isset($_GET['edit'])) {
#get post variables
        $key = $_GET['edit'];

        $query_instEdit = "SELECT * FROM campus WHERE CampusID ='$key'";
        $instEdit = $zalongwa->query($query_instEdit) or die($zalongwa->connect_error);
        $row_instEdit = $instEdit->fetch_assoc();
        $totalRows_instEdit = $instEdit->num_rows;

        $queryString_inst = "";
        if (!empty($_SERVER['QUERY_STRING'])) {
            $params = explode("&", $_SERVER['QUERY_STRING']);
            $newParams = array();
            foreach ($params as $param) {
                if (stristr($param, "pageNum_inst") == false &&
                    stristr($param, "totalRows_inst") == false
                ) {
                    array_push($newParams, $param);
                }
            }
            if (count($newParams) != 0) {
                $queryString_inst = "&" . htmlentities(implode("&", $newParams));
            }
        }
        $queryString_inst = sprintf("&totalRows_inst=%d%s", $totalRows_inst, $queryString_inst);
        ?>
        <div class="row ">
            <div class="col-sm-8">
                <div class="card">
                    <h3 class="card-header">
                        <?php echo $szTitle; ?></h3>
                    <div class="card-block">
                        <form action="<?php echo $editFormAction; ?>" method="POST" name="frmInstEdit" id="frmInstEdit">
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Institution</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="txtName" type="text" id="txtName"  value="<?php echo $row_instEdit['Campus']; ?>" size="40">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Address</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="txtAdd" type="text" id="txtAdd" value="<?php echo $row_instEdit['Address']; ?>" size="40">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Physical Address</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="txtPhyAdd" type="text" id="txtPhyAdd" value="<?php echo $row_instEdit['Location']; ?>"
                                               size="40">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Telephone</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="txtTel" type="text" id="txtTel" value="<?php echo $row_instEdit['Tel']; ?>" size="40">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Email</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="txtEmail" type="text" id="txtEmail" value="<?php echo $row_instEdit['Email']; ?>" size="40">
                                        <input name="id" type="hidden" id="id" value="<?php echo $key ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"></label>
                                    <div class="col-sm-8">
                                        <input class="btn btn-success btn-md btn-block" type="submit" name="edit"
                                               value="Edit Record"
                                               title="Click to Edit Records">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="MM_update" value="frmInstEdit">
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }
    # include the footer
    include("../footer/footer.php");

    @mysqli_free_result($inst);

    @mysqli_free_result($instEdit);
    ?>

</div>
<br><br>
<!--footer-->
<footer class="bd-footer text-muted fixed-bottom ">
    <div class="container">
        <p>Designed and Maintained by <a href="http://41.86.162.35/zwebsite/" target="_blank"> Zalongwa
                Technologies</a>
        <p>Copyright &copy;2017</p></p>
    </div>
</footer>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>

</body>
</html>
