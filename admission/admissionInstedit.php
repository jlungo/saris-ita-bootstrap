<?php
require_once('../Connections/zalongwa.php');
require_once('../Connections/sessioncontrol.php');
# include the header
include('admissionMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Policy Setup';
$szTitle = 'Policy Setup';
$szSubSection = 'Policy Setup';
include("admissionheader.php");

if (isset($_GET['content'])) {
    if ($_POST['update']) {
        $Campus = addslashes($_POST["Campus"]);
        $Location = addslashes($_POST["Location"]);
        $Address = addslashes($_POST["Address"]);
        $Tel = addslashes($_POST["Tel"]);
        $Email = addslashes($_POST["Email"]);

        $sql = "UPDATE campus SET Campus='$Campus', Location='$Location', Address = '$Address', Tel = '$Tel',
			Email='$Email' WHERE CampusID=" . $_GET['content'];
        if ($zalongwa->query($sql)) {
            echo "<p>Updated successfully</p>";
            header("Location: admissionInst.php");
        } else {
            echo "<p>Failed to update..</p>";
        }
    }

    $result = $zalongwa->query("SELECT * FROM campus WHERE CampusID=" . $_GET['content']);
    $institution = $result->fetch_assoc();
    ?>
    <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
        <table class='table_view'>
            <tr class='header'>
                <td> Field</td>
                <td> Field Value</td>
            </tr>
            <tr class='list'>
                <td> Campus</td>
                <td><input type="text" id="Campus" name="Campus" value="<?php echo $institution['Campus']; ?>"
                           size="40"></td>
            </tr>
            <tr class='list'>
                <td> Location:</td>
                <td><input type="text" id="Location" name="Location" value="<?php echo $institution['Location']; ?>"
                           size="40"></td>
            </tr>
            <tr class='list'>
                <td> Physical Address:</td>
                <td><input type="text" id="Address" name="Address" value="<?php echo $institution['Address']; ?>"
                           size="40"></td>
            </tr>
            <tr class='list'>
                <td> Telephone:</td>
                <td><input type="text" id="Tel" name="Tel" value="<?php echo $institution['Tel']; ?>" size="40"></td>
            </tr>
            <tr class='list'>
                <td> Email:</td>
                <td><input type="text" id="Email" name="Email" value="<?php echo $institution['Email']; ?>" size="40">
                </td>
            </tr>
        </table>
        <br>
        <table class='table_view'>
            <tr style='float: right' class='list'>
                <td>
                    <button formaction="admissionInst.php">Back</button>
                </td>
                <td><input type="submit" id="submit" name="update" value="update"></td>
            </tr>
        </table>

    </form>

    <?php

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
        $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}
# include the footer
include("../footer/footer.php");
?>