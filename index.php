<!DOCTYPE HTML>
<html>
	<head>
		<title>SARIS</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
						<h2>INSTITUTE OF TAX ADMINSTRATION</h2>	
						<div class="logo" align="center">
							<img src="images/logo.png">
						<!-- 	<span class="icon fa-diamond"> </span> -->
						</div>
						<div class="content">
						
<div id="right-column" align="center">
<h2>Saris-ITA, TRA</h2>
<div class="login">
<form class="myform" action="userlogin.php" method="post">
   
   
   <input id="textusername" name="textusername" type="text" class="inputvalues" placeholder="USERNAME" ><br>

  
   <input id="textpassword" name="textpassword" type="password" class="inputvalues" placeholder="**********" ><br>

   <input name="login" type="submit" id="login-button" value="Login"><br>
   <?php

                if (isset($loginerror ) && $loginerror !="")
                {
                    ?>
					<b class="small" color="Red">
                        <?php  echo $loginerror?>
					</>
					</b>
                    <?php
                    session_cache_limiter('nocache');
                    $_SESSION = array();
                    session_unset();
                    session_destroy();
                }
                ?>
</form>

<h4 style=" font-size:11px; padding-left:100px;">No account yet? <a href="registration.php" style="color:#FFF100;">Create New</a></h4> 
<h4 style=" font-size:11px; padding-left:90px;">Forget Password? <a href="passwordrecover.php"  style="color:#FFF100;">Get Help</a></h4>

</div>
</div>


						</div>
						<!--  <nav>
							<ul>
								<li><a href="#intro">Intro</a></li>
								<li><a href="#work">Work</a></li>
								<li><a href="#about">About</a></li>
								<li><a href="#contact">Contact</a></li>
								<li><a href="#elements">Elements</a></li>
							</ul>
						</nav>  -->

						 <div class="inner" style="  color:#FFF100;">
<footer id="footer" >
						<p class="copyright" style=" font-size:13px; padding:0px; color:#FFF;">&copy; Institute of Tax Administration. Designed by Zalongwa Technologies </p>
					</footer>

							</div>
					</header>

				<!-- Main -->
					<div id="main">

						<!-- Elements -->
							<article id="elements">
								<h2 class="major">Elements</h2>

								<section>
								
									<pre><code>i = 0;

while (!deck.isInOrder()) {
    print 'Iteration ' + i;
    deck.shuffle();
    i++;
}

print 'It took ' + i + ' iterations to sort the deck.';</code></pre>
								</section>

							
							</article>

					</div>

                            
				<!-- Footer -->
					

			</div>

		<!-- BG -->
			<div id="bg"></div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
