
	<style type="text/css">
		#table{
			border-radius:5px;
			background:#CCCCCC;
			font-family:Courier New, Monospace;
			}
		#table tr th{
			background:#CCCCCC;
			}
		#table tr td{							
			font-size:14px;
			font-family:Courier New, Monospace;
			}
		#table tr:hover{
			opacity:0.7;
			}
		.total{
			background:#CCCCCC;
			font-weight:bold;
			}
	</style>
	
<?php 
	require_once('../Connections/sessioncontrol.php');
	require_once('../Connections/zalongwa.php');
	
	# include the header
	include('admissionMenu.php');
	global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
	$szSection = 'Accommodation';
	$szTitle = 'Room Allocation for the Current Year ';
	$szSubSection = 'Current Allocation';
	//$additionalStyleSheet = './general.css';
	include('admissionheader.php');

	//get current year
	$qyear="SELECT AYear FROM academicyear WHERE Status=1";	
	$dbyear = mysqli_query($zalongwa, $qyear) or die("No Single year");
	$row_year = mysqli_fetch_array($dbyear);
	$currentyear = $row_year['AYear'];

	# get all users
	$quser="SELECT RegNo FROM allocation WHERE AYear='$currentyear' ORDER BY HID,RegNo";	
	$dbuser = mysqli_query($zalongwa, $quser) or die("No Single User");
	$dbusertenant = mysqli_query($zalongwa, $quser) or die("No Single User");
	$row_usertenant = mysqli_fetch_array($dbuser);
			
	$sn = 0;
	?>
	<table border="1" cellspacing="0" cellpadding="3" bordercolor='#006600' id='table'>
	  <tr class='total'>
		<td><div align="center">S/No</div></td>
		 <td><div align="left">Name</div></td>
		<td nowrap><div align="center">RegNo</div></td>
		<td nowrap><div align="center">RoomNo</div></td>
		<td><div align="center">Hostel</div></td>
		<td><div align="center">Year</div></td>
	  </tr>
	<?php
	while($row_user = mysqli_fetch_array($dbusertenant)){

		$regno= $row_user['RegNo'];
		$qtenant = "SELECT student.Name, allocation.RegNo, hostel.HName, allocation.RNumber, allocation.AYear, hostel.HID
                    FROM (allocation INNER JOIN student ON allocation.RegNo = student.RegNo) 
                    INNER JOIN hostel ON allocation.HID = hostel.HID
		            WHERE (allocation.RegNo='$regno') ORDER BY hostel.HName, allocation.RegNo ";
		            
		$dbtenant = mysqli_query($zalongwa, $qtenant) or die("No Single User");
		$total_rows = mysqli_num_rows($dbtenant);
		
		if($total_rows==1){
			$sn = $sn+1;
			
			$row_tenant = mysqli_fetch_array($dbtenant);
			$tenantname= $row_tenant['Name'];
			$room= $row_tenant['RNumber'];
			$hall= $row_tenant['HName'];
			$mwaka= $row_tenant['AYear'];
			
			$class = (fmod($sn,2) == '0')? "bgcolor='#CCCCCC'":"bgcolor='#ffffff'";
		?>
			<tr <?php echo $class;?>>
				<td><div align="center"><?=$sn?></div></td>
				<td nowrap><div align="left"><?=$tenantname?></div></td>
				<td nowrap><div align="center"><?=$regno?></div></td>
				<td nowrap><div align="right"><?=$room?></div></td>
				<td nowrap><div align="left"><?=$hall?></div></td>
				<td><div align="center"><?=$mwaka?></div></td>
			</tr>
		<?php
			}
		}
	echo '</table>';
	include("../footer/footer.php");
?>
