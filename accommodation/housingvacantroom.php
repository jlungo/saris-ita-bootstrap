	<style type="text/css">
		#table{
			border-radius:5px;
			background:#CCCCCC;
			font-family:Courier New, Monospace;
			}
		#table tr th{
			background:#CCCCCC;
			}
		#table tr td{							
			font-size:14px;
			font-family:Courier New, Monospace;
			}
		#table tr:hover{
			opacity:0.7;
			}
		.total{
			background:#CCCCCC;
			font-weight:bold;
			}
	</style>
	
<?php 
	#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('admissionMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Accommodation';
	$szSubSection = 'Vacant Rooms';
	$szTitle = 'Vacant Rooms Report';
	include('admissionheader.php');
	$today = date("Y-m-d");
	//control form display

	
	$query_AcademicYear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
	$AcademicYear = mysqli_query($zalongwa, $query_AcademicYear) or die(mysqli_error($zalongwa));
	$row_AcademicYear = mysqli_fetch_assoc($AcademicYear);
	$totalRows_AcademicYear = mysqli_num_rows($AcademicYear);

	
	$query_Hostel = "SELECT HID, HName FROM hostel";
	$Hostel = mysqli_query($zalongwa, $query_Hostel) or die(mysqli_error($zalongwa));
	$row_Hostel = mysqli_fetch_assoc($Hostel);
	$totalRows_Hostel = mysqli_num_rows($Hostel);


	if (isset($_POST["MM_search"]) && $_POST["MM_search"] == 'room'){
		//get the posted values
		$ayear=addslashes($_POST['ayear']);
		$hall = addslashes($_POST['Hall']);
		
		//create array of all rooms from this hostel
		$qroom = "SELECT HID, RNumber, Capacity FROM room WHERE HID='$hall'";
		$dbroom = mysqli_query($zalongwa, $qroom);
		$roomcount = mysqli_num_rows($dbroom);

		if($roomcount>0){
			//print report
			$qhall = "select HName from hostel where HID='$hall'";
			$dbhall = mysqli_query($zalongwa, $qhall);
			$row_hall = mysqli_fetch_assoc($dbhall);
			$hallname = $row_hall['HName'];
			
			echo"$ayear Vacant Beds Report for Hostel '$hallname'";
		?>
			<table border='1' cellspacing='0' cellpadding='3' bordercolor='#006600' id='table'>
				  <tr class='total'>
					<td>S/No</td>
					<td>Hostel/Hall</td>
					<td nowrap>Room Number</td>
					<td>Capacity</td>
					<td>Vacants</td>
				  </tr>
		<?php
			$i=0;
			//compare allocated students and room capacity
			while($row_room=mysqli_fetch_array($dbroom)){
				$i=$i+1;
				$room = $row_room['RNumber'];
				$capacity = intval($row_room['Capacity']);
				
				$qstudent = "SELECT RegNo FROM allocation WHERE HID='$hall' AND RNumber='$room' AND AYear='$ayear' AND CheckOut>'$today'";
				$dbstudent=mysqli_query($zalongwa, $qstudent);
				$totalstudent=mysqli_num_rows($dbstudent);
				$vacant = $capacity - $totalstudent;
				
				$class = (fmod($i,2) == '0')? "bgcolor='#CCCCCC'":"bgcolor='#ffffff'";
		?>
				  <tr <?php echo $class;?>>
					<td><?=$i?></td>
					<td nowrap><?=$hallname?></td>
					<td nowrap><?=$room?></td>
					<td><div align="center"><?=$capacity?></div> </td>
					<td><div align="center"><?=$vacant?></div></td>
				  </tr>
		<?php
				}
			echo '</table>';
			}
		}
	else{
		?>
        <form action="<?=$_SERVER['PHP_SELF']?>" method="POST" name="housingvacantRoom" id="housingvacantRoom">
            <fieldset style='border-radius:6px'>
				<legend>Search Vacant Rooms</legend>
			<table border='1' cellspacing='0' cellpadding='3' bordercolor='#006600' id='table'>
				<tr>
				  <td nowrap><div align="right">Academic Year: </div></td>
				  <td>
					  <select name="ayear" id="select2" required>
						<option value="0">SelectAcademicYear</option>
				<?php
					do {  
				?>
						<option value="<?php echo $row_AcademicYear['AYear']?>"><?php echo $row_AcademicYear['AYear']?></option>
				<?php
						} while ($row_AcademicYear = mysqli_fetch_assoc($AcademicYear));
						
					  $rows = mysqli_num_rows($AcademicYear);
					  if($rows > 0) {
						mysqli_data_seek($AcademicYear, 0);
						$row_AcademicYear = mysqli_fetch_assoc($AcademicYear);
						}
				?>
					</select>
				  </td>
				</tr>
				<tr>
				  <td nowrap><div align="right"> Hall/Hostel:</div></td>
				  <td>
					  <select name="Hall" id="select" required>
						<option value="0">Select Student Hostel</option>
				<?php
					do {  
				?>
						<option value="<?php echo $row_Hostel['HID']?>"><?php echo $row_Hostel['HName']?></option>
				<?php
						} while ($row_Hostel = mysqli_fetch_assoc($Hostel));
						
					  $rows = mysqli_num_rows($Hostel);
					  if($rows > 0) {
						mysqli_data_seek($Hostel, 0);
						$row_Hostel = mysqli_fetch_assoc($Hostel);
						}
				?>
					</select>
				  </td>
				</tr>
				<tr>
				  <th colspan="2" nowrap>
					  <div align="center"><input type="submit" name="Submit" value="Search Rooms"></div>
				  </th>
				</tr>
		  </table>
		  </fieldset>
             <input type="hidden" name="MM_search" value="room">
       </form>
	<?php	
	}
	# include the footer
	include('../footer/footer.php');
?>
