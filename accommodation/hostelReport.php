	<style type="text/css">
		#table{
			border-radius:5px;
			background:#CCCCCC;
			font-family:Courier New, Monospace;
			}
		#table tr th{
			background:#CCCCCC;
			}
		#table tr td{							
			font-size:14px;
			font-family:Courier New, Monospace;
			}
		#table tr:hover{
			opacity:0.7;
			}
		.total{
			background:#CCCCCC;
			}
	</style>


<?php
	require_once('../Connections/zalongwa.php');

	$currentPage = $_SERVER["PHP_SELF"];

	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
	{
	  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

	  switch ($theType) {
		case "text":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;    
		case "long":
		case "int":
		  $theValue = ($theValue != "") ? intval($theValue) : "NULL";
		  break;
		case "double":
		  $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
		  break;
		case "date":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
		case "defined":
		  $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
		  break;
	  }
	  return $theValue;
	}
	//control the display table
	@$new=2;

	$editFormAction = $_SERVER['PHP_SELF'];
	if (isset($_SERVER['QUERY_STRING'])) {
	  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
	}

	if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmInst")) {
	  $insertSQL = sprintf("INSERT INTO room (RNumber, HID, Capacity, BID, FloorName) VALUES (%s, %s, %s, %s, %s)",
						   GetSQLValueString($_POST['txtName'], "text"),
						   GetSQLValueString($_POST['txtAdd'], "text"),
						   GetSQLValueString($_POST['txtPhyAdd'], "text"),
						   GetSQLValueString($_POST['txtTel'], "text"),
						   GetSQLValueString($_POST['txtEmail'], "text"));

	  
	  $Result1 = mysqli_query($zalongwa, $insertSQL) or die(mysqli_error($zalongwa));
	}

	if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmInstEdit")) {
	  $updateSQL = sprintf("UPDATE room SET BID=%s, Capacity=%s, FloorName=%s WHERE HID=%s AND RNumber=%s",
						   GetSQLValueString($_POST['txtTel'], "text"),
						   GetSQLValueString($_POST['txtAdd'], "text"),
						   GetSQLValueString($_POST['txtEmail'], "text"),
						   GetSQLValueString($_POST['txtPhyAdd'], "text"),
						   GetSQLValueString($_POST['txtName'], "text"));

	  
	  $Result1 = mysqli_query($zalongwa, $updateSQL) or die(mysqli_error($zalongwa));

	  $updateGoTo = "roomRegister.php";
	  if (isset($_SERVER['QUERY_STRING'])) {
		$updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
		$updateGoTo .= $_SERVER['QUERY_STRING'];
	  }
	  header(sprintf("Location: %s", $updateGoTo));
	}

	$maxRows_inst = 50;
	$pageNum_inst = 0;
	if (isset($_GET['pageNum_inst'])) {
	  $pageNum_inst = $_GET['pageNum_inst'];
	}
	$startRow_inst = $pageNum_inst * $maxRows_inst;

	
	$query_inst = "SELECT HID, BID, RNumber, Capacity, FloorName FROM room ORDER BY HID, RNumber ASC";
	$query_limit_inst = sprintf("%s LIMIT %d, %d", $query_inst, $startRow_inst, $maxRows_inst);
	$inst = mysqli_query($zalongwa, $query_limit_inst) or die(mysqli_error($zalongwa));
	$row_inst = mysqli_fetch_assoc($inst);

	if (isset($_GET['totalRows_inst'])) {
	  $totalRows_inst = $_GET['totalRows_inst'];
	} else {
	  $all_inst = mysqli_query($zalongwa, $query_inst);
	  $totalRows_inst = mysqli_num_rows($all_inst);
	}
	$totalPages_inst = ceil($totalRows_inst/$maxRows_inst)-1;

	require_once('../Connections/sessioncontrol.php');
	# include the header
	include('admissionMenu.php');
		global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
		$szSection = 'Policy Setup';
		$szTitle = 'Hostel Room Information';
		$szSubSection = 'Hostel Report';
		include("admissionheader.php");
		
	?>

	<table border="1" cellspacing="0" cellpadding="3" bordercolor='#006600' id='table'>
	  <tr class='total'>
		<td nowrap><strong>S/No</strong></td>
		<td nowrap><strong>Room Number</strong></td>
		<td><strong>Capacity</strong></td>
		<td><strong>Hostel</strong></td>
		<td><strong>Block</strong></td>
		<td><strong>Floor</strong></td>
	  </tr>
	  <?php
		$i=1; 
		do { 
			$class = (fmod($i,2) == '0')? "bgcolor='#CCCCCC'":"bgcolor='#ffffff'";			  
	  ?>
	  <tr <?php echo $class;?>>
		<td nowrap><?php echo $i ?></td>
		<td nowrap>
			<?php 
				$id = $row_inst['HID']; $name = $row_inst['HID']; $rnumber = $row_inst['RNumber']; 
				echo $rnumber;
			?>
		</td>
		<td><?php echo $row_inst['Capacity']; ?></td>
		<td>
			<?php
				$hid=$row_inst['HID']; 
				$qhostel = "SELECT HName from hostel where HID = '$hid'";
				$dbhostel = mysqli_query($zalongwa, $qhostel);
				$row_hostel=mysqli_fetch_assoc($dbhostel);
				echo $row_hostel['HName'];	$i++;
			?>
		</td>
		<td><?php echo $row_inst['BID']; ?></td>
		<td><?php echo $row_inst['FloorName']; ?></td>
	  </tr>
	  <?php 
			} while ($row_inst = mysqli_fetch_assoc($inst)); 
	  ?>
	</table>
	<a href="<?php printf("%s?pageNum_inst=%d%s", $currentPage, max(0, $pageNum_inst - 1), $queryString_inst); ?>">Previous</a><span class="style1">.............</span><?php echo min($startRow_inst + $maxRows_inst, $totalRows_inst) ?>/<?php echo $totalRows_inst ?> <span class="style1">..............</span><a href="<?php printf("%s?pageNum_inst=%d%s", $currentPage, min($totalPages_inst, $pageNum_inst + 1), $queryString_inst); ?>">Next</a><br>

	<?php
	# include the footer
	include("../footer/footer.php");

	@mysqli_free_result($inst);
	@mysqli_free_result($instEdit);
?>
