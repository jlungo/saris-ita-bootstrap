<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */

/** Error reporting */
error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

/** PHPExcel */
require_once '../Classes/PHPExcel.php';

	$papersize='PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4';
	$fontstyle='Arial';
	$font=10.5;
	
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("Zalongwa")
							 ->setLastModifiedBy("Lackson David")
							 ->setTitle($pname)
							 ->setSubject("Student ACCOMMODATION_REPORT")
							 ->setDescription("Student ACCOMMODATION_REPORT.")
							 ->setKeywords("zalongwa saris software")
							 ->setCategory("ACCOMMODATION_REPORT FILE");
	
	$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);
	
	# Set protected sheets to 'true' kama hutaki waandike waziedit sheets zako. Kama unataka wazi-edit weka 'false'
	$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
	
	#set worksheet orientation and size
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize($papersize);
	
	#Set page fit width to true
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
	
	#Set footer page numbers
	$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
	#Show or hide grid lines
	$objPHPExcel->getActiveSheet()->setShowGridlines(false);
	
	#Set sheet style (fonts and font size)
	$objPHPExcel->getDefaultStyle()->getFont()->setName($fontstyle);
	$objPHPExcel->getDefaultStyle()->getFont()->setSize($font); 
	
	#Set page margins
	$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(1);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(1);
	
	# Set Rows to repeate in each page
	$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 5);
	
	
	$styleArray = array(
		'borders' => array(
			'outline' => array(
				'style' => PHPExcel_Style_Border::BORDER_THICK,
				'color' => array('argb' => 'FF000000'),
			),
		),
	);



	for($col=A;$col<'ZZ';$col++) { 
		$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
	}
	
	
	
	# Print Report header	
	$rpttitle="$hall HOSTEL - ROOM ALLOCATOIN REPORT";
	$objPHPExcel->getActiveSheet()->mergeCells('D1:Z1');
	$objPHPExcel->getActiveSheet()->mergeCells('D2:Z2');
	$objPHPExcel->getActiveSheet()->mergeCells('D3:Z3');
	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('D1', strtoupper($org))
		    ->setCellValue('D2', $rpttitle)
		    ->setCellValue('D3', '[ TOTAL OCCUPANTS IN '.$year.' IS: '.$totalRows_query.' ]');
	$objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setSize(18); 
	$objPHPExcel->getActiveSheet()->getStyle('D2:D3')->getFont()->setSize(16); 
	$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getStyle('D1:D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
	$colm = "C";
	$rows = 4;	
	
	# Centre and Shed title cells
	//$objPHPExcel->getActiveSheet()->getStyle(4,$totalcolms)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ff808080');
	$objPHPExcel->getActiveSheet()->getStyle('C4:DE5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
	
	#prepare headers
	$header=array('S/NO','REGSTRATION #','NAME','SEX','ROOM NUMBER','CHECK IN','CHECK OUT');
	
	
	#get student details
	$i=1;
	$totalcolms =0;
	$fees_array = array(); //holds feecodes for later explanation
	
	while($result = mysqli_fetch_array($query)) {
		$id = stripslashes($result["Id"]);
		$Name = stripslashes($result["Name"]);
		$RegNo = stripslashes($result["RegNo"]);
		$sex = stripslashes($result["Sex"]);
		$hall = stripslashes($result["HName"]);
		$citeria = stripslashes($result["RNumber"]);
		$checkin = stripslashes($result["CheckIn"]);
		$checkout = stripslashes($result["CheckOut"]);
		
		$rum = explode("-",$citeria);
		$x = strtoupper($rum[0]);
		
		$colm = "C";
		$rows1 = $rows +1;		
		$colm1 = $colm;				 
			
		
		#HEADERS PRINTING
		if($i==1){
			$y=$x;
			$colm1 = $colm;				 
			for ($col=1;$col<12;$col++) {  
				$colm1++;
				}
				// set header color here
				$bgcolor='D4E8C3';
			#PRINT STUDENT INFORMATION HEADERS 
			$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$colm1.$rows1)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$colm1.$rows1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$colm1.$rows1)->getFill()->getStartColor()->setRGB($bgcolor);
			$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm1.$rows1)->getStyle($colm.$rows.':'.$colm1.$rows1)->applyFromArray($styleArray);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $y);
			$colm = $colm1;
			$colm++;
			$rows=$rows1;
			$rows++;
			
			$rows1 = $rows;
			$rows1++; 	    

			
			$ZEROKEY=0;
			$colm = "C";
			foreach($header as $val){
				$colm1 = $colm;
				
				if($ZEROKEY=='0' || $ZEROKEY=='3'){
					}
				else{
					for ($col=1;$col<2;$col++) {  
						$colm1++;
						}
					}
				
				$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$colm1.$rows1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$colm1.$rows1)->getFill()->getStartColor()->setRGB($bgcolor);
				$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm1.$rows1)->getStyle($colm.$rows.':'.$colm1.$rows1)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $val);
				$colm = $colm1;
				$colm++; 	    
				$ZEROKEY++;
				}
			
			$rows=$rows1;
			$rows++;
			}
		else{
			if($x != $y){
				$y=$x;
				$i=1;
				$colm = "C";
				$colm1 = $colm;
				 
				for ($col=1;$col<12;$col++) {  
					$colm1++;
					}
					
				#PRINT STUDENT INFORMATION HEADERS 
				$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$colm1.$rows1)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$colm1.$rows1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$colm1.$rows1)->getFill()->getStartColor()->setRGB($bgcolor);
				$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm1.$rows1)->getStyle($colm.$rows.':'.$colm1.$rows1)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $y);
				$colm = $colm1;
				$colm++;
				$rows=$rows1;
				$rows++;
				
				$rows1 = $rows;
				$rows1++; 	
				
				$ZEROKEY=0;
				$colm = "C";
				foreach($header as $val){
					$colm1 = $colm;					
					
					if($ZEROKEY=='0' || $ZEROKEY=='3'){
						}
					else{
						for ($col=1;$col<2;$col++) {  
							$colm1++;
							}
						}
					
					$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$colm1.$rows1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$colm1.$rows1)->getFill()->getStartColor()->setRGB($bgcolor);
					$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm1.$rows1)->getStyle($colm.$rows.':'.$colm1.$rows1)->applyFromArray($styleArray);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $val);
					$colm = $colm1;
					$colm++; 	    
					$ZEROKEY++;
					}
				
				$rows=$rows1;
				$rows++;
				}
			}
		
		#ENDS HEADERS PRINTING
		
		
		/*********
		 * *
		 * * PRINT ACCOMMODATION DETAILS
		 * *
		 *********/
		
		$colm = "C";
		$colm1 = $colm;
		$colmx = $colm;
		
		#control odd and even shadding
		for ($col=1;$col<12;$col++) {  
			$colmx++;
			}
		
		if(fmod($i,2) == '0'){
			$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$colmx.$rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle($colm.$rows.':'.$colmx.$rows)->getFill()->getStartColor()->setRGB($bgcolor);
			}
			
		#print sno
		$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm1.$rows)->getStyle($colm.$rows.':'.$colm1.$rows)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $i);
		$colm = $colm1;
		$colm++; 
		$colm1=$colm;
		$colm1++;
		
		#print registration number
		$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm1.$rows)->getStyle($colm.$rows.':'.$colm1.$rows)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($colm.$rows, $RegNo, PHPExcel_Cell_DataType::TYPE_STRING);
		//$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $RegNo);
		$colm = $colm1;
		$colm++; 
		$colm1=$colm;
		$colm1++;
		
		#print name
		$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm1.$rows)->getStyle($colm.$rows.':'.$colm1.$rows)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $Name);
		$colm = $colm1;
		$colm++; 
		$colm1=$colm;		
		
		#print sex
		$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm1.$rows)->getStyle($colm.$rows.':'.$colm1.$rows)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $sex);
		$colm = $colm1;
		$colm++; 
		$colm1=$colm;
		$colm1++;
		
		#print room number
		$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm1.$rows)->getStyle($colm.$rows.':'.$colm1.$rows)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $citeria);
		$colm = $colm1;
		$colm++; 
		$colm1=$colm;
		$colm1++;
		
		#print check in time
		$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm1.$rows)->getStyle($colm.$rows.':'.$colm1.$rows)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $checkin);
		$colm = $colm1;
		$colm++; 
		$colm1=$colm;
		$colm1++;
		
		#print checkout time
		$objPHPExcel->getActiveSheet()->mergeCells($colm.$rows.':'.$colm1.$rows)->getStyle($colm.$rows.':'.$colm1.$rows)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($colm.$rows, $checkout);
		$colm = $colm1;
		$colm++; 
		
		$rows++;
		$i++;
		}	
	
	
// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('ROOM ALLOCATION REPORT');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="SARIS_ROOM_ALLOCATION_REPORT.xls"');
header('Cache-Control: max-age=0');
ob_clean();
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

/*
#make pdf
header('Content-Type: application/pdf');
header('Content-Disposition: attachment;filename="SARIS_Exam_Results.pdf"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
$objWriter->save('php://output');
*/

exit;
