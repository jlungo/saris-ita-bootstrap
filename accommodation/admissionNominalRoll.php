
<?php 
#start pdf
	?>
	
	<style type="text/css">
		#table{
			border-radius:5px;
			background:#CCCCCC;
			font-family:Courier New, Monospace;
			}
		#table tr th{
			background:#CCCCCC;
			}
		#table tr td{							
			font-size:14px;
			font-family:Courier New, Monospace;
			}
		#table tr:hover{
			opacity:0.7;
			}
		.total{
			background:#CCCCCC;
			font-weight:bold;
			}
	</style>
	
	<?php
	#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('admissionMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Accommodation';
	$szSubSection = 'Class Lists';
	$szTitle = 'Printing Class Lists';
	include('admissionheader.php');
	

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

	
	$query_AcademicYear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
	$AcademicYear = mysqli_query($zalongwa, $query_AcademicYear) or die(mysqli_error($zalongwa));
	$row_AcademicYear = mysqli_fetch_assoc($AcademicYear);
	$totalRows_AcademicYear = mysqli_num_rows($AcademicYear);

	
	$query_Hostel = "SELECT ProgrammeCode, ProgrammeName FROM programme ORDER BY ProgrammeName ASC";
	$Hostel = mysqli_query($zalongwa, $query_Hostel) or die(mysqli_error($zalongwa));
	$row_Hostel = mysqli_fetch_assoc($Hostel);
	$totalRows_Hostel = mysqli_num_rows($Hostel);

	
	$query_faculty = "SELECT FacultyID, FacultyName FROM faculty ORDER BY FacultyName ASC";
	$faculty = mysqli_query($zalongwa, $query_faculty) or die(mysqli_error($zalongwa));
	$row_faculty = mysqli_fetch_assoc($faculty);
	$totalRows_faculty = mysqli_num_rows($faculty);

if (isset($_POST['print']) && ($_POST['print'] == "PreView")) {
	#get post values
	$programme = addslashes($_POST['programme']);
	$cohort = addslashes($_POST['cohort']);
	$ryear = addslashes($_POST['ayear']);
	$status = '';//addslashes($_POST['status']);
	$sponsor = '';//addslashes($_POST['sponsor']);
	$checkbill = addslashes($_POST['checkbill']);
	$housingmanager=1;
	#get programme name
	$qprogram = "SELECT ProgrammeName FROM programme WHERE ProgrammeCODE ='$programme'";
	$dbprogram = mysqli_query($zalongwa, $qprogram);
	$row_program = mysqli_fetch_assoc($dbprogram);
	$pname = $row_program['ProgrammeName'];
	
	#create report title
	if($programme==''){
		$title = 'LIST OF ALL STUDENTS SELECTED IN '.$cohort.' ACADEMIC AUDIT YEAR';
	}else{
		$listitle = $pname;
	}
		
		include '../admission/includes/filter_students.php';

	/* Printing Results in html */
	if (mysqli_num_rows($query_std) > 0){
		$degree = $pname;
		?>
		
		
		<table border='0' cellpadding='0' cellspacing='0' id='table' bordercolor='#006600'>
          <tr bgcolor='#ffffff'>
            <td><div align="center" class="style4">
              <h1><?php echo $org?></h1>
            </div></td>
          </tr>
          <tr bgcolor='#ffffff'>
            <td><div align="center">
              <h4><?php echo ($cohort<>'') ? 'Cohort ' : '';?> <?php echo $cohort?> Nominal Roll</h4>
            </div></td>
          </tr>
          <tr bgcolor='#ffffff'>
            <td><div align="center">
              <h4>A <?php echo $ryear?> Status Report</h4>
            </div></td>
          </tr>
		  <?php if ($programme!= ''){?>
          <tr bgcolor='#ffffff'>
            <td><div align="center">
              <h4><span class="style4"> <?php echo $listitle ?></span></h4>
            </div></td>
          </tr>
		   <?php } ?>
        </table>
        
		<table border='1' cellpadding='3' cellspacing='0' id='table' bordercolor='#006600'>
		<tr class='total'><td> S/No </td><td> Name </td>
		<td> RegNo </td>
		<td> Sex </td>
		<td> Degree </td>
		<td> Stream Session </td>
		<td> Sponsor </td>
		 <?php if($checkbill=='on') {      $tinvoice=0;   $tpaid=0;      $tdue=0;  ?>
			<td> Invoice </td>
			<td> Paid</td>
			<td> Balance </td>
		<?php } ?>
		<td> Status </td>
		</tr>
		<?php
		$i=1;
		#count unregistered
		$j=0;
		#count sex
		$$fmcount = 0;
		$$mcount = 0;
		$$fcount = 0;
		while($result = mysqli_fetch_array($query_std)) {
				$id = stripslashes($result["Id"]);
				$Name = stripslashes($result["Name"]);
				$RegNo = stripslashes($result["RegNo"]);
				$sex = stripslashes($result["Sex"]);
				$degreecode = stripslashes($result["ProgrammeofStudy"]);
				$faculty = stripslashes($result["Faculty"]);
				$sponsor = stripslashes($result["Sponsor"]);
				$class = stripslashes($result["Class"]);
				#initialise
				$entryyear = stripslashes($result['EntryYear']);
			    $ststatus = stripslashes($result['Status']);
				#compute year of study
				$current_yeartranc = substr($cyear,0,4);
				$entryyear = substr($entryyear,0,4);
				$yearofstudy=$current_yeartranc-$entryyear+1;
				
				if($checkbill=='on') {
					# check if paid
					$regno=$RegNo;
					#initialise billing values
					$grandtotal=0;
					$degree=$degreecode;
					$feerate=0;
					$invoice=0;
					$paid=0;
					$debtorlimit=0;
					$tz103=0;
					$amount=0;
					$subtotal=0;
					$cfee=0;
					include('../billing/includes/getgrandtotalpaid.php');
				}
				
				#get study programe name
				$qprogram = "SELECT ProgrammeName FROM programme WHERE ProgrammeCODE ='$degreecode'";
				$dbprogram = mysqli_query($zalongwa, $qprogram);
				$row_program = mysqli_fetch_assoc($dbprogram);
				$degree = $row_program['ProgrammeName'];
				
				#get study status name
				$qstatus = "SELECT Status FROM studentstatus WHERE StatusID ='$ststatus'";
				$dbstatus = mysqli_query($zalongwa, $qstatus);
				$row_status = mysqli_fetch_assoc($dbstatus);
				$status = $row_status['Status'];
				
				/*	
				 #determine student status auntomatically
				$qstatus = "SELECT DISTINCT RegNo FROM examresult WHERE RegNo='$RegNo' AND AYear='$ryear'";
				$dbstatus = mysqli_query($zalongwa, $qstatus);
				$statusvalue = mysqli_num_rows($dbstatus);
				if($statusvalue>0){
				$status  = stripslashes($result["Status"]);
				}else{
				#check in examregister
					$qstatus = "SELECT DISTINCT RegNo FROM examregister WHERE RegNo='$RegNo' AND AYear='$ryear'";
					$dbstatus = mysqli_query($zalongwa, $qstatus);
					$statusvalue = mysqli_num_rows($dbstatus);
					if($statusvalue>0){
						$status  = stripslashes($result["Status"]);
						}else{
						$status = 'Not Registered';
						$j=$j+1;
						}
				}
				*/
				#get line color
				$remainder = $i%2;
				if ($remainder==0){
					$linecolor = 'bgcolor="#FFFFCC"';
				}else{
				 $linecolor = 'bgcolor="#FFFFFF"';
				}

				echo "<tr><td $linecolor>$i</td>";
					?>
					<td <?php echo $linecolor?> nowrap><?php echo ($ststatus != 3)?'<span class="style1">':'';?><?php echo $Name?></td>
					<td <?php echo $linecolor?> nowrap><?php echo ($ststatus != 3)?'<span class="style1">':'';?><?php echo $RegNo?></td>
					<td <?php echo $linecolor?> nowrap><?php echo ($ststatus != 3)?'<span class="style1">':'';?><?php echo $sex?></td>
					<td <?php echo $linecolor?> nowrap><?php echo ($ststatus != 3)?'<span class="style1">':'';?><?php echo $degree?></td>
					<td <?php echo $linecolor?> nowrap><?php echo ($ststatus != 3)?'<span class="style1">':'';?><?php echo $class?></td>
					<td <?php echo $linecolor?> nowrap><?php echo ($ststatus != 3)?'<span class="style1">':'';?><?php echo $sponsor?></td>
					<?php if($checkbill=='on') { $tinvoice=$tinvoice+$invoice;   $tpaid=$tpaid+$paid;      $tdue=$tdue+$due; ?>	
						<td <?php echo $linecolor?> nowrap> <?php echo ($ststatus != 3)?'<span class="style1">':'';?><?php echo number_format($invoice,2,'.',',')?> </td>
						<td <?php echo $linecolor?> nowrap> <?php echo ($ststatus != 3)?'<span class="style1">':'';?><?php echo number_format($paid,2,'.',',')?></td>
						<td <?php echo $linecolor?> nowrap> <?php echo ($ststatus != 3)?'<span class="style1">':'';?><?php echo number_format($due,2,'.',',')?> </td>
					<?php } ?>
					<td <?php echo $linecolor?> nowrap><?php echo ($ststatus != 3)?'<span class="style1">':'';?><?php echo $status?></td>
					<?php
					echo "</tr>";
				    $i=$i+1;
					if ($sex=='F'){
						$fcount = $fcount +1;
					}elseif($sex=='M'){
						$mcount = $mcount +1;
					}else{
						$fmcount = $fmcount +1;
					}
				}
				#end while loop

		?><tr class='total'><td> </td><td> Total </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td> </td>
		<td>  </td>
		 <?php if($checkbill=='on') { ?>
			<td <?php echo $linecolor?> nowrap> <?php echo ($ststatus != 3)?'<span class="style1">':'';?><?php echo number_format($tinvoice,2,'.',',')?>  </td>
			<td <?php echo $linecolor?> nowrap> <?php echo ($ststatus != 3)?'<span class="style1">':'';?><?php echo number_format($tpaid,2,'.',',')?>     </td>
			<td <?php echo $linecolor?> nowrap> <?php echo ($ststatus != 3)?'<span class="style1">':'';?><?php echo number_format($tdue,2,'.',',')?>      </td>
		<?php } ?>
		<td> </td>
		</tr>
			<?php echo "</table>";
			#print statistics
			$gt=$i-1;
			echo 'Grand Total: '.$gt;
			echo '<hr>';
			if ($display==1){
				echo 'Total Unregistered Students  are: '.$j.'('.round($j/$gt*100,2).'%)';
			}
				echo '<hr> Total Female Students are: '.$fcount.'('.round($fcount/$gt*100,2).'%)';
				echo '<hr> Total Male Students are: '.$mcount.'('.round($mcount/$gt*100,2).'%)';
			if($fmcount<>0){
				echo '<hr> Total Male/Female Unspecified Students are '.$fmcount.'('.round($fmcount/$gt*100,2).'%)';
			}
			}else{
					echo "Sorry, No Records Found <br>";
				}
}else{

?>

<form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" name="studentclasslist" id="studentclasslist">
      <table border='1' cellpadding='3' cellspacing='0' id='table' bordercolor='#006600'>
        <tr class='total'>
          <td colspan="2" nowrap><div align="center">PRINTING STUDENTS CLASS LISTS </div></td>
          </tr>
		  <tr>
			  <td rowspan="1" nowrap><div align="right">Programme:</div></td>
			  <td colspan="1"><select name="programme" id="programme">
				   <option value="">--------------------------------</option>
			            <?php
						do {  
						?>
						            <option value="<?php echo $row_Hostel['ProgrammeCode']?>"><?php echo $row_Hostel['ProgrammeName']?></option>
						            <?php
						} while ($row_Hostel = mysqli_fetch_assoc($Hostel));
						  $rows = mysqli_num_rows($Hostel);
						  if($rows > 0) {
						      mysqli_data_seek($Hostel, 0);
							  $row_Hostel = mysqli_fetch_assoc($Hostel);
						  }
						?>
          			</select></td>
		  </tr>
  		  <tr>
			  <td rowspan="1" nowrap><div align="right">Group Cohort:</div></td>
					<td colspan="1"><select name="cohort" id="cohort">
					  <option value="">--------------------------------</option>
			            <?php
					do {  
					?>
			            <option value="<?php echo $row_AcademicYear['AYear']?>"><?php echo $row_AcademicYear['AYear']?></option>
			            <?php
					} while ($row_AcademicYear = mysqli_fetch_assoc($AcademicYear));
					  $rows = mysqli_num_rows($AcademicYear);
					  if($rows > 0) {
					      mysqli_data_seek($AcademicYear, 0);
						  $row_AcademicYear = mysqli_fetch_assoc($AcademicYear);
					  }
					?>
				   </select></td>
		   </tr>     
   		  <tr>
			  <td rowspan="1" nowrap><div align="right">Audit Year:</div></td>
				<td colspan="1"><select name="ayear" id="ayear">
					 <option value="">--------------------------------</option>
			            <?php
				do {  
				?>
		            <option value="<?php echo $row_AcademicYear['AYear']?>"><?php echo $row_AcademicYear['AYear']?></option>
		            <?php
				} while ($row_AcademicYear = mysqli_fetch_assoc($AcademicYear));
				  $rows = mysqli_num_rows($AcademicYear);
				  if($rows > 0) {
				      mysqli_data_seek($AcademicYear, 0);
					  $row_AcademicYear = mysqli_fetch_assoc($AcademicYear);
				  }
				?>
        	  </select></td>
		   </tr>
 		   <tr style='display:none'>
          <td nowrap><div align="right">Class Stream:</div></td>
          	<td colspan="1"><select name="stream" id="stream">
		       <option value="0">--------------------------------</option>
				<?php
					$query_class = "SELECT name FROM classstream ORDER BY name ASC";
					$nm=mysqli_query($zalongwa, $query_class);
					while($show = mysqli_fetch_array($nm) )
					{  										 
					 echo"<option  value='$show[name]'>$show[name]</option>";      	    
					}
					?>
	      </select></td>
	     </tr>
	     <tr style='display:none'>
           <td nowrap><div align="right">Reg. Status:</div></td>
          	<td colspan="1"><select name="status" id="status">
		       <option value="0">--------------------------------</option>
			      <?php  
					$query_studentStatus = "SELECT StatusID,Status FROM studentstatus ORDER BY StatusID";
					$nm=mysqli_query($zalongwa, $query_studentStatus);
					while($show = mysqli_fetch_array($nm) )
					{  										 
						echo"<option  value='$show[StatusID]'>$show[Status]</option>";      
					      
					}
				 ?>
	      </select></td>
	     </tr>
  		 <tr style='display:none'>
          <td nowrap><div align="right">  Sponsorship:</div></td>
            <td colspan="1"><select name="sponsor" id="sponsor">
             <option value="0">--------------------------------</option>
		       <?php
				if($sponsor)
				{
					echo"<option value='$sponsor'>$sponsor</option>";
				}  
				$query_sponsor = "SELECT Name FROM sponsors ORDER BY SponsorID ASC";
				$nm=mysqli_query($zalongwa, $query_sponsor);
				while($show = mysqli_fetch_array($nm) )
				{  										 
				   echo"<option  value='$show[Name]'>$show[Name]</option>";      	    
				}
				?>
     		</select></td>
     	  </tr>
          <tr style='display:none'>
		    <td nowrap><div align="right">  Show Billing:</div></td> 
		    <td colspan="1">
		    <input name="checkbill" type="checkbox" id="checkbill" value="on" >Yes
		    </td>
         </tr>
        <tr class='total'>
          <td colspan='2'>
		    <div align="center">
		      <input name="print" type="submit" id="print" value="PreView">
            </div></td>
	        </tr>
	      </table>
        </form>
<?php
}
mysqli_free_result($AcademicYear);

mysqli_free_result($Hostel);
include('../footer/footer.php');
?>
