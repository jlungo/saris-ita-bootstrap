<?php
# include the header
if($student)
include('studentMenu.php');
else
include('lecturerMenu.php');
global $szSection, $szSubSection, $szTitle, $additionalStyleSheet;
$szSection = 'Timetable';
$szTitle = 'View Timetable';
$szSubSection = 'Get TimeTable';
if($student)
include("studentheader.php");
else
include("lecturerheader.php");

mysql_select_db($zalongwa_database,$zalongwa);
//select all academic year
$sql_ayear= "SELECT * FROM academicyear ORDER BY AYear DESC";
$result_ayear=mysql_query($sql_ayear);

// select all timetable type/category
$sql_timetablecategory= "SELECT * FROM timetableCategory";
$result_timetablecategory=mysql_query($sql_timetablecategory);

//select all programme
$sql_programme= "SELECT * FROM programme";
$result_programme=mysql_query($sql_programme);

//select all streams
$sql_str = "SELECT * FROM classstream";
$res_str = mysql_query($sql_str);

// select all lecturer
$query_lecturer = "SELECT UserName, FullName, Position FROM security WHERE Position = 'Lecturer' OR Position = 'Exam Officer' ORDER BY FullName";
$lecturer_result=mysql_query($query_lecturer);

// select all fuculty
$query_fuculty = "SELECT * FROM faculty ";
$fuculty_result=mysql_query($query_fuculty);


if(isset($_POST['load'])){
$ayear=$_POST['ayear'];
$programme = '';
$type =$_POST['tcategory'];
$filter =$_POST['fby'];
$yos=$_POST['yos'];
$stream = $_POST['stream'];
$venue = $_POST['venue'];

if($filter == 1){
$programme = $_POST['programme'];
}
else if($filter == 2){
$programme=$_POST['faculty'];
}
else if($filter == 3){
$programme = $_POST['lecturer'];
}
else if($filter == 4){
$programme = $_POST['stream'];
}
else if($filter == 5){
$programme = $venue;
}

//header('Location: createtimetable.php?create=1&ayear='.$ayear.'&programe='.$programme.'&type='.$type);
echo '<meta http-equiv = "refresh" content ="0; url = gettimetable.php?create=1&ayear='.$ayear.'&fby='.$filter.'&programme='.$programme.'&type='.$type.'&yos='.$yos.'&str='.$stream.'">';
exit;
}

if(!isset($_GET['create'])){
?>
<style type="text/css">
    .hide{
        display:none;
    }
</style>

<form action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
    <table class="resView">
        <tr>
            <td class="resViewhd">Academic Year:</td>
            <td class="resViewtd">
                <select name="ayear">
                    <?php

                    while($row = mysql_fetch_array($result_ayear)){
                        echo '<option value="'.$row['AYear'].'">'.$row['AYear'].'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="resViewhd">Filter By</td>
            <td class="resViewtd">
                <select name="fby" id="filter">
                    <option value="1">Programme</option>
                    <option value="2">Faculty</option>
                    <option value="3">Lecturer</option>
                    <option value="4">Stream</option>
                    <option value="5">Venue</option>
                </select>
            </td>
        </tr>
        <tr id="pr">
            <td class="resViewhd">Programme:</td>
            <td class="resViewtd">
                <select name="programme">
                    <?php
                    while($row = mysql_fetch_array($result_programme)){
                        echo '<option value="'.$row['ProgrammeCode'].'">'.$row['ProgrammeName'].'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr class="hide" id="fc">
            <td class="resViewhd">Faculty:</td>
            <td class="resViewtd">
                <select name="faculty">
                    <?php
                    while($row = mysql_fetch_array($fuculty_result)){
                        echo '<option value="'.$row['FacultyID'].'">'.$row['FacultyName'].'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr class="hide" id="str">
            <td class="resViewhd">Stream:</td>
            <td class="resViewtd">
                <select name="stream">
                    <?php
                    while($row = mysql_fetch_array($res_str)){
                        echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr class="hide" id="lect">
            <td class="resViewhd">Lecturer:</td>
            <td class="resViewtd">
                <select name="lecturer">
                    <?php
                    while($row = mysql_fetch_array($lecturer_result)){
                        echo '<option value="'.$row['UserName'].'">'.$row['FullName'].'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr class="hide" id="venu">
            <td class="resViewhd">Venue:</td>
            <td class="resViewtd">
                <select name="venue">
                    <?php
                    $get_venue = mysql_query("SELECT VenueCode,VenueName FROM venue ORDER BY VenueName ASC");
                    while(list($code,$name) = mysql_fetch_array($get_venue)){
                        echo '<option value="'.$code.'">'.$name.'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="resViewhd">Year of Study</td>
            <td  class="resViewtd">
                <select name="yos">
                    <option value="1">First Year</option>
                    <option value="2">Second Year</option>
                    <option value="3">Third Year</option>
                    <option value="4">Fourth Year</option>
                    <option value="5">Fifth Year</option>
                    <option value="6">Sixth Year</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="resViewhd">Time table Category:</td>
            <td class="resViewtd">
                <select name="tcategory">
                    <?php
                    while($row = mysql_fetch_array($result_timetablecategory)){
                        echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="resViewhd" colspan="2" align="center">
                <input type="submit" name="load" value="Load Timetable"/>
            </td>
        </tr>
    </table>
</form>
<br/><br/>
<?php
if(isset($_GET['error'])){
    echo '<div style="color:red; margin:0px 0px 0px 50px;">'.$_GET['error'].'</div>';
}
}

else{
$ayear = $_GET['ayear'];
$programme= $_GET['programme'];
$type = $_GET['type'];
$fby = $_GET['fby'];
$yos=$_GET['yos'];
$stream = $_GET['str'];
$cls='';
$sem='';


//Programme timetable
if($fby == 1){
    $sql = "SELECT * FROM timetable WHERE AYear='$ayear' AND YoS='$yos' AND Programme='$programme' 
					AND timetable_category='$type' ORDER BY day,start ASC";

    // generate timetable title
    $sql_title = "SELECT * FROM programme WHERE ProgrammeCode='$programme'";
    $title_result = mysql_query($sql_title);
    $fetch_title = mysql_fetch_array($title_result);
    $title_timetable = $fetch_title['Title'].' - [ '.$fetch_title['ProgrammeName'].' ] ';


    //SEMESTER ONE
    if($type == 1){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER TWO
    elseif($type == 2){
        $stdyClass = array(1=>array('FIRST YEAR','II'),2=>array('SECOND YEAR','II'),
            3=>array('THIRD YEAR','II'),4=>array('FOURTH YEAR','II'),
            5=>array('FIFTH YEAR','II'),6=>array('SIXTH YEAR','II'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER ONE EXAMINATION
    elseif($type == 3){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER TWO EXAMINATION
    elseif($type == 4){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SUPP/SPECIAL EXAMINATION
    elseif($type == 5){
        $stdyClass = array(1=>array('FIRST YEAR',''),2=>array('SECOND YEAR',''),
            3=>array('THIRD YEAR',''),4=>array('FOURTH YEAR',''),
            5=>array('FIFTH YEAR',''),6=>array('SIXTH YEAR',''),
        );

        $subtitle_timetable = 'SUPPL/SPECIAL EXAMINATION TIMETABLE - '.$ayear .' - '.$stdyClass[$yos][0];
    }
}

//Faculty timetable
else if($fby == 2){

    $sql_faculty = "SELECT * FROM faculty WHERE FacultyID='$programme'";
    $result_faculty = mysql_query($sql_faculty);
    $data=mysql_fetch_array($result_faculty);
    $fc_ID = $programme;
    $fc_name = $data['FacultyName'];
    $sql_progr = "SELECT DISTINCT ProgrammeCode FROM programme WHERE Faculty='$fc_ID' OR Faculty='$fc_name'";

    $facult_prog = mysql_query($sql_progr);
    //$loop_prog_value='';
    while ($row = mysql_fetch_array($facult_prog)) {
        $pg = $row['ProgrammeCode'];
        $loop_prog_value.="  Programme='$pg' OR ";
    }

    $loop_prog_value =rtrim($loop_prog_value," OR ");
    $loop_prog_value = " AND (". $loop_prog_value.' ) ';
    $sql = "SELECT * FROM timetable WHERE AYear='$ayear' $loop_prog_value AND YoS='$yos' 
					AND  timetable_category='$type' ORDER BY day,start ASC";

    // get title
    $title_timetable = $fc_name;

    //SEMESTER ONE
    if($type == 1){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER TWO
    elseif($type == 2){
        $stdyClass = array(1=>array('FIRST YEAR','II'),2=>array('SECOND YEAR','II'),
            3=>array('THIRD YEAR','II'),4=>array('FOURTH YEAR','II'),
            5=>array('FIFTH YEAR','II'),6=>array('SIXTH YEAR','II'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER ONE EXAMINATION
    elseif($type == 3){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER TWO EXAMINATION
    elseif($type == 4){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SUPP/SPECIAL EXAMINATION
    elseif($type == 5){
        $stdyClass = array(1=>array('FIRST YEAR',''),2=>array('SECOND YEAR',''),
            3=>array('THIRD YEAR',''),4=>array('FOURTH YEAR',''),
            5=>array('FIFTH YEAR',''),6=>array('SIXTH YEAR',''),
        );

        $subtitle_timetable = 'SUPPL/SPECIAL EXAMINATION TIMETABLE - '.$ayear .' - '.$stdyClass[$yos][0];
    }
}

//Lecturer timetable
elseif($fby == 3){
    $sql = "SELECT * FROM timetable WHERE  AYear='$ayear' AND lecturer='$programme' AND timetable_category='$type' ORDER BY day,start ASC";

    // generate timetable title
    $sql_title = "SELECT * FROM security WHERE UserName='$programme'";
    $title_result = mysql_query($sql_title);
    $fetch_title = mysql_fetch_array($title_result);
    $title_timetable = $fetch_title['FullName'];


    //SEMESTER ONE
    if($type == 1){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER TWO
    elseif($type == 2){
        $stdyClass = array(1=>array('FIRST YEAR','II'),2=>array('SECOND YEAR','II'),
            3=>array('THIRD YEAR','II'),4=>array('FOURTH YEAR','II'),
            5=>array('FIFTH YEAR','II'),6=>array('SIXTH YEAR','II'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER ONE EXAMINATION
    elseif($type == 3){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER TWO EXAMINATION
    elseif($type == 4){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SUPP/SPECIAL EXAMINATION
    elseif($type == 5){
        $stdyClass = array(1=>array('FIRST YEAR',''),2=>array('SECOND YEAR',''),
            3=>array('THIRD YEAR',''),4=>array('FOURTH YEAR',''),
            5=>array('FIFTH YEAR',''),6=>array('SIXTH YEAR',''),
        );

        $subtitle_timetable = 'SUPPL/SPECIAL EXAMINATION TIMETABLE - '.$ayear .' - '.$stdyClass[$yos][0];
    }
}

//Stream Timetable
else if($fby == 4){

    $sql_stream = "SELECT * FROM classstream WHERE id='$stream'";
    $result_stream = mysql_query($sql_stream);
    $data=mysql_fetch_assoc($result_stream);
    $str = $data['name'];

    $sql = "SELECT * FROM timetable WHERE AYear='$ayear' AND YoS='$yos' 
					AND  timetable_category='$type' AND class='$str' ORDER BY day,start ASC";
    // get title
    $title_timetable = $str;

    //SEMESTER ONE
    if($type == 1){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER TWO
    elseif($type == 2){
        $stdyClass = array(1=>array('FIRST YEAR','II'),2=>array('SECOND YEAR','II'),
            3=>array('THIRD YEAR','II'),4=>array('FOURTH YEAR','II'),
            5=>array('FIFTH YEAR','II'),6=>array('SIXTH YEAR','II'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER ONE EXAMINATION
    elseif($type == 3){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER TWO EXAMINATION
    elseif($type == 4){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SUPP/SPECIAL EXAMINATION
    elseif($type == 5){
        $stdyClass = array(1=>array('FIRST YEAR',''),2=>array('SECOND YEAR',''),
            3=>array('THIRD YEAR',''),4=>array('FOURTH YEAR',''),
            5=>array('FIFTH YEAR',''),6=>array('SIXTH YEAR',''),
        );

        $subtitle_timetable = 'SUPPL/SPECIAL EXAMINATION TIMETABLE - '.$ayear .' - '.$stdyClass[$yos][0];
    }
}

//Stream venueTimetable
else if($fby == 5){

    $sql_stream = "SELECT * FROM classstream WHERE VenueCode='$programme'";
    $result_stream = mysql_query($sql_stream);
    list($venue_name)=mysql_fetch_array($result_stream);

    $sql = "SELECT * FROM timetable WHERE AYear='$ayear' AND YoS='$yos' 
					AND  timetable_category='$type' AND venue='$programme' ORDER BY day,start ASC";
    // get title
    $title_timetable = $venue_name;

    //SEMESTER ONE
    if($type == 1){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER TWO
    elseif($type == 2){
        $stdyClass = array(1=>array('FIRST YEAR','II'),2=>array('SECOND YEAR','II'),
            3=>array('THIRD YEAR','II'),4=>array('FOURTH YEAR','II'),
            5=>array('FIFTH YEAR','II'),6=>array('SIXTH YEAR','II'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER ONE EXAMINATION
    elseif($type == 3){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE  SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SEMESTER TWO EXAMINATION
    elseif($type == 4){
        $stdyClass = array(1=>array('FIRST YEAR','I'),2=>array('SECOND YEAR','I'),
            3=>array('THIRD YEAR','I'),4=>array('FOURTH YEAR','I'),
            5=>array('FIFTH YEAR','I'),6=>array('SIXTH YEAR','I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE SEMESTER '.$stdyClass[$yos][1].' - '.$ayear .' - '.$stdyClass[$yos][0];
    }
    //SUPP/SPECIAL EXAMINATION
    elseif($type == 5){
        $stdyClass = array(1=>array('FIRST YEAR',''),2=>array('SECOND YEAR',''),
            3=>array('THIRD YEAR',''),4=>array('FOURTH YEAR',''),
            5=>array('FIFTH YEAR',''),6=>array('SIXTH YEAR',''),
        );

        $subtitle_timetable = 'SUPPL/SPECIAL EXAMINATION TIMETABLE - '.$ayear .' - '.$stdyClass[$yos][0];
    }
}

$result = mysql_query($sql);
$num = mysql_num_rows($result);


$timetable=array();
if($num < 1){
    echo '<meta http-equiv = "refresh" content ="0; url = gettimetable.php?error=No data found!!!">';
    exit;
}
else{
$keys=array();
while ($row = mysql_fetch_array($result)) {
    $usern = $row['lecturer'];
    $sql_lect = "SELECT FullName FROM security WHERE UserName='$usern'";
    $result_lect = mysql_query($sql_lect);
    list($lecturer_name) = mysql_fetch_array($result_lect);

    $teaching = $row['teachingtype'];
    $sql_teach = "SELECT * FROM teachingtype WHERE id='$teaching'";
    $result_teach = mysql_query($sql_teach);
    $teach_type = mysql_fetch_array($result_teach);

    //define class stream
    $klass = explode('-',trim($row['class'],' '));
    if(count($klass)==0){
        $klass = '';
    }
    else{
        $klass = $klass[1].' - '.substr(trim($klass[0]),-1);
    }

    $keys[$row['CourseCode']]=$row['CourseCode'];
    $timetable[$row['day']][$row['start']][]=array(
        'interval'=>$row['start_end'],
        'start'=>$row['start'],
        'end'=>$row['end'],
        'course'=>$row['CourseCode'],
        'venue'=>$row['venue'],
        'lecturer'=>$lecturer_name,
        'teaching'=>$teach_type['name'],
        'class'=>$klass
    );
}

//echo '<pre>';
//print_r($timetable);
//echo '</pre>';
?>
<style type="text/css">
    #content span{
        color:black;
        font-weight:normal;
        font-size:12px;
    }
    .view_timetable{
        table-layout:fixed;
        width:900px;
        padding:0px;
        margin:0px;
        position:relative;
        border:1px solid #CCCCCC;
        color:#000000;
    }

    .view_timetable tr td{
        border-bottom:1px solid #CCCCCC;
        border-right:1px solid #CCCCCC;
        position:relative;
    }
    .one{
        display:block;
        background-color:#DFD7CF;
        border:1px solid blue;
        text-align:center;
        padding:0px 0px 0px 0px;
        height:80px;
        overflow:hidden;
        margin:5px 0px 5px 0px;
    }
    .wiz{
        display:block;
        margin:7px 0px 0px 0px;
        padding:5px 0px 0px 0px;
    }

    .one span{
        display:block;
        color:black;
    }

    .two{
        display:block;
        background-color: #DFD7CF;
        border:1px solid blue;
        z-index:100;
        position:relative;
        text-align:center;
        padding:0px 0px 0px 0px;
        height:80px;
        overflow:hidden;
        margin:5px 0px 5px 0px;
    }

    .two span{
        display:block;
        color:#000000;
    }

    .two-clas{
        width:130px;
    }

    .three{
        display:block;
        background-color:#DFD7CF;
        border:1px solid blue;
        text-align:center;
        height:80px;
        overflow:hidden;
        padding:0px 0px 0px 0px;
        margin:5px 0px 5px 0px;
    }

    .three span{
        display:block;
        color:#000000;
    }

</style>
<div id="hello">
    <div style="text-indent:20px; padding:10px 0px 0px 0px; width:900px;">
        <?php
        echo '<h2 style="padding:0px; margin:0px; width:900px; color:black; font-size:20px; display:block; text-indent:100px;">'.strtoupper($title_timetable).'</h2>';
        echo '<h3 style="padding:0px; margin:0px; width:900px; color:black; font-size:20px; display:block; text-indent:100px;">'.$subtitle_timetable.'</h3>';
        ?>
    </div>

    <div style="color:blue; text-align:right; font-size:15px;font-weight:bold;  padding:10px 100px 10px 0px;">
        <form action="printtimetable.php" method="post">
            <input value="<?php echo $ayear;?>" name="ayear" type="hidden"/>
            <input value="<?php echo $programme?>" name="programme" type="hidden" />
            <input value="<?php echo $type?>" name="type" type="hidden"/>
            <input value="<?php echo $fby?>" name="fby" type="hidden"/>
            <input value="<?php echo $yos; ?>" name="yos" type="hidden"/>
            <input value="<?php echo $stream;?>" name="str" type="hidden">
            <input id="m" style="color:blue; font-size:15px;font-weight:bold; cursor: pointer; text-decoration:underline; border:0px; background-color:transparent;" type="submit" value="Export Timetable" name="PRINT"/>
        </form>
    </div>

    <table border="0" cellpadding="0" cellspacing="0" class="view_timetable">
        <tr>
            <td style="width:90px;">Day/Time</td>
            <td class="time">07:00 - 08:00</td>
            <td class="time">08:00 - 09:00</td>
            <td class="time">09:00 - 10:00</td>
            <td class="time">10:00 - 11:00</td>
            <td class="time">11:00 - 12:00</td>
            <td class="time">12:00 - 13:00</td>
            <td class="time">13:00 - 14:00</td>
            <td class="time">14:00 - 15:00</td>
            <td class="time">15:00 - 16:00</td>
            <td class="time">16:00 - 17:00</td>
            <td class="time">17:00 - 18:00</td>
            <td class="time">18:00 - 19:00</td>
            <td class="time">19:00 - 20:00</td>
            <td class="time">20:00 - 21:00</td>
            <td class="time">21:00 - 22:00</td>
        </tr>
        <?php
        $sql_day = "SELECT * FROM days ORDER BY id ASC";
        $get = mysql_query($sql_day);

        while ($row = mysql_fetch_array($get)) {
        ?>
        <tr>
            <td style="width:100px;"><?php echo $row['name'];?></td>
            <?php
            if(array_key_exists($row['id'], $timetable)){
            for ( $p=7; $p < 22;$p++) {
            if(array_key_exists($p, $timetable[$row['id']])){
            ?>
            <td valign="top">
                <?php
                foreach ($timetable[$row['id']][$p] as $key => $value) {
                    if($value['interval'] == 1){
                        ?>
                        <div class="one">
                            <span style="background-color:gray; padding:5px 0px 5px 0px;"><?php echo $value['teaching']?></span>
                            <!--<span style="padding-top:2px;"><?php echo "<font size=1>".$value['course']."</font>"?></span> -->
                            <span style="padding-top:2px;"><?php echo $value['course']?></span>
                            <span><?php echo $value['venue'];?></span>
                            <span><?php echo $value['lecturer'];?></span>
                            <span><?php echo $value['class'];?></span>
                        </div>
                        <?php
                    }
                    else if($value['interval'] == 2){
                        ?>
                        <div class="two">
                            <span style="background-color:gray; padding:5px 0px 5px 0px;"><?php echo $value['teaching']?></span>
                            <span style="padding-top:5px;"><?php echo $value['course']?></span>
                            <span><?php echo $value['venue'];?></span>
                            <span><?php echo $value['lecturer'];?></span>
                            <span><?php echo $value['class'];?></span>
                        </div>
                        <?php
                    }
                    else if($value['interval'] == 3){
                        ?>
                        <div class="three">
                            <span style="background-color:gray; padding:5px 0px 5px 0px;"><?php echo $value['teaching']?></span>
                            <span style="padding-top:5px;"><?php echo $value['course']?></span>
                            <span><?php echo $value['venue'];?></span>
                            <span><?php echo $value['lecturer'];?></span>
                            <span><?php echo $value['class'];?></span>
                        </div>
                    <?php 	}
                }
                echo "</td>";
                }
                else{
                    echo '<td valign="top">&nbsp;</td>';
                }
                }
                }
                echo "</tr>";
                }

                echo "</table>
				</div>
				<br/><p>";

                if(count($keys) > 0){
                    echo '<u><b>KEY</b></u><br/>';
                    foreach($keys as $k=>$v){
                        $sele = "SELECT * FROM course WHERE CourseCode='$k'";
                        $my=mysql_query($sele);
                        $fetc = mysql_fetch_array($my);
                        echo $k .' &nbsp; : &nbsp; &nbsp;'. $fetc['CourseName'].'<br/>';
                    }
                }

                echo "</p><br/><br/><br/>";
                }
                }
                ?>

                <script type="text/javascript" src="jquery.js"></script>
                <script type="text/javascript">
                    $(document).ready(function(){


                        $('#fc').hide();
                        $('#lect').hide();
                        $('#str').hide();
                        $('#venu').hide();

                        $('#filter').change(function(){
                            onchange_val = $(this).val();

                            if(onchange_val == 3){
                                $('#lect').show();
                                $('#str').hide();
                                $('#fc').hide();
                                $('#pr').hide();
                                $('#venu').hide();

                            }else if(onchange_val == 2){
                                $('#lect').hide();
                                $('#str').hide();
                                $('#fc').show();
                                $('#pr').hide();
                                $('#venu').hide();

                            }else if(onchange_val == 1){
                                $('#lect').hide();
                                $('#fc').hide();
                                $('#str').hide();
                                $('#pr').show();
                                $('#venu').hide();

                            }else if(onchange_val == 4){
                                $('#str').show();
                                $('#lect').hide();
                                $('#fc').hide();
                                $('#pr').hide();
                                $('#venu').hide();
                            }else if(onchange_val == 5){
                                $('#venu').show();
                                $('#str').hide();
                                $('#lect').hide();
                                $('#fc').hide();
                                $('#pr').hide();
                            }

                        });


                        var single=60;

//period with 1hr interval
                        $('.view_timetable').find('div[class=one]').each(function(){
                            single = $(this).width();

                        });

                        // period with 3hrs interval
                        $('.view_timetable').find('div[class=three]').each(function(){
                            $(this).width(3*single); // overwrite the width to fix 3 td

                            width = $(this).width();
                            offset = $(this).position();  // get position of the div
                            height = $(this).height(); // get height of the div
                            var ll = 0;
                            $($(this).parent().next()).find('div').each(function(){  // loop all div in the next td
                                $(this).css('position','relative');
                                $(this).css('z-index','2000');

                                off = $(this).position();  // get position of the div in the next td
                                he = $(this).height();
                                if(width > single){
                                    if(off.top == offset.top){  // if the div fall in the same possition append div aon top

                                        ass = height;

                                        $('<div class="wiz" style="position:relative;z-index:2000;height:'+ass+'px">ll</div>').insertBefore(this); // insert div before the overlaping div
                                    }
                                }
                            });

                            //loop next td /// the 3rd td, the same procedure as above
                            $($(this).parent().next().next()).find('div').each(function(){
                                $(this).css('position','relative');
                                $(this).css('z-index','2000');
                                // alert('sdsd');
                                off = $(this).position();
                                he = $(this).height();
                                if(width > single){
                                    if(off.top == offset.top){

                                        ass = height;

                                        $('<div class="wiz" style="position:relative;z-index:3000;height:'+ass+'px"></div>').insertBefore(this);
                                    }
                                }
                            });
                        });



                        // deal with period of 2hrs interval
                        $('.view_timetable').find('div[class=two]').each(function(){

                            $(this).width(2*single);
                            width = $(this).width();

                            //$(this).append($(this).width());
                            offset = $(this).position();
                            height = $(this).height();
                            var ll = 0;
                            $($(this).parent().next()).find('div').each(function(){
                                $(this).css('position','relative');
                                $(this).css('z-index','2000');

                                off = $(this).position();
                                he = $(this).height();
                                if(width > single){
                                    if(off.top == offset.top){

                                        ass = height;

                                        $('<div class="wiz" style="position:relative;z-index:2000;height:'+ass+'px"></div>').insertBefore(this);
                                    }
                                }
                            });

                        });

                    });
                </script>