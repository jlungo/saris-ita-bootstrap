<form action="<?php echo $editFormAction; ?>" method=POST id=fmAdd name=fmAdd LANGUAGE=javascript
      onsubmit="return fmAdd_onsubmit()" enctype="multipart/form-data">

    <div class="container">
        <div class="form-group row">
            <label class="col-sm-4 col-form-label">Name</label>
            <div class="col-sm-8">
                <p class="form-control-static"><?php echo $row_changepassword['FullName']; ?></p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="form-group row">
            <label class="col-sm-4 col-form-label">Reg No.</label>
            <div class="col-sm-8">
                <p class="form-control-static"><?php echo $row_changepassword['RegNo']; ?></p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="form-group row">
            <label class="col-sm-4 col-form-label">Position</label>
            <div class="col-sm-8">
                <p class="form-control-static"><?php echo $row_changepassword['Position']; ?></p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="form-group row">
            <label class="col-sm-4 col-form-label">Login</label>
            <div class="col-sm-8">
                <p class="form-control-static"><?php echo $row_changepassword['UserName']; ?></p>
                <input name="username" type="hidden" id="username"
                       value="<?php echo $row_changepassword['UserName']; ?>">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-4 col-form-label">Old Password</label>
            <div class="col-sm-8">
                <input class="form-control" name="txtoldPWD" type="password" id="txtoldPWD">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-4 col-form-label">New Password</label>
            <div class="col-sm-8">
                <input class="form-control" name="txtnewPWD" type="password" id="txtnewPWD">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-4 col-form-label">Retype Password</label>
            <div class="col-sm-8">
                <input class="form-control" name="txtrenewPWD" type="password" id="txtrenewPWD">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-4 col-form-label"></label>
            <div class="col-sm-8">
                <input type="submit" value="Submit" name="Submit" class="btn btn-success btn-md btn-block">
            </div>
        </div>
    </div>
    <input type="hidden" name="MM_insert" value="true">
    <input type="hidden" name="MM_update" value="fmAdd">
</form>