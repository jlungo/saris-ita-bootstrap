<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="./css/navstyle.css?v=1.0" rel="stylesheet" type="text/css"/>
    <style>
        body {
            background-color: #eff0f1;
        }

        a:hover {
            text-decoration: none;
            color: #0056b3;
        }

        .navbar {
            width: 100%;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            -webkit-transition: .20s box-shadow;
            transition: .20s box-shadow;
        }

        .navbar-toggler {
            cursor: pointer;
            outline: 0;
            padding-top: inherit;
        }

        @media (max-width: 34em) {
            .navbar {
                padding-top: 5px;
                padding-bottom: 0px;
                background-color: #FAFAFA;
                overflow: hidden;
            }

            .navbar-toggler {
                cursor: pointer;
                outline: 0;
                padding-top: inherit;
            }

            .nav-link {
                color: whitesmoke;
            }

            .nav-item {
                padding-top: 30px;
                padding-bottom: 0px;
                margin-bottom: -3px;
            }

            .card {
                background-color: #324359;
                box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
                -webkit-transition: .20s box-shadow;
                transition: .20s box-shadow;
                color: white;
                padding: 0px;
                border-radius: 0px !important;
            }

            .row {
                margin-top: 20px;
            }
        }

        @media (max-width: 48em) {
            .card h5 {
                font-size: 14px;
            }
        }

        footer {
            bottom: 0;
            width: 100%;
            margin-top: 20px;
            padding: 2px;
        }

        footer p {
            margin-top: 0;
            margin-bottom: 1rem;
            font-size: small;
            margin: 0px;
        }

        a {
            -webkit-transition: .25s all;
            transition: .25s all;
        }

        .card:focus, .card:hover {
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 2px 5px 0 rgba(0, 0, 0, 0.20);
            /*box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.20);*/
        }

        .card-inverse .card-img-overlay {
            background-color: rgba(51, 51, 51, 0.85);
            border-color: rgba(51, 51, 51, 0.85);
        }
    </style>
    <title>SARIS | <?php echo $szSection ?> | <?php echo $szSubSection ?></title>
    <!--modernaizer here-->

    <script src="modernizr-custom.js">
    </script>
    <!--script loaded for datatable-->
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>

</head>
<body>
<!-- navbar -->
<?php include '../academic/academicNavBar.php';

require_once('../Connections/zalongwa.php');
//mysql_select_db($zalongwa_database,$zalongwa);
//select all academic year
$sql_ayear = "SELECT * FROM academicyear ORDER BY AYear DESC";
$result_ayear = mysqli_query($zalongwa, $sql_ayear);

// select all timetable type/category
$sql_timetablecategory = "SELECT * FROM timetablecategory";
$result_timetablecategory = mysqli_query($zalongwa, $sql_timetablecategory);

//select all programme
$sql_programme = "SELECT * FROM programme";
$result_programme = mysqli_query($zalongwa, $sql_programme);

//select all streams
$sql_str = "SELECT * FROM classstream";
$res_str = mysqli_query($zalongwa, $sql_str);

// select all lecturer
$query_lecturer = "SELECT UserName, FullName, Position FROM security WHERE Position = 'Lecturer' OR Position = 'Exam Officer' ORDER BY FullName";
$lecturer_result = mysqli_query($zalongwa, $query_lecturer);

// select all fuculty
$query_fuculty = "SELECT * FROM faculty ";
$fuculty_result = mysqli_query($zalongwa, $query_fuculty);

$query_student = "SELECT AdmissionNo, ProgrammeofStudy, YearofStudy, ProgrammeCode, ProgrammeName, 
Title FROM student INNER JOIN programme ON ProgrammeofStudy=ProgrammeCode where AdmissionNo='$RegNo'";

$student_result = mysqli_query($zalongwa, $query_student);
$row_student = mysqli_fetch_array($student_result);

$programme = $studprog;
$filter = 1;
$ayear = $cyear;
$yos = $row_student['YearofStudy'];

if (isset($_POST['load'])) {
    $type = $_POST['tcategory'];

    if ($filter == 1) {
        $programme = $row_student['ProgrammeofStudy'];
    }

    //header('Location: createtimetable.php?create=1&ayear='.$ayear.'&programe='.$programme.'&type='.$type);
    echo '<meta http-equiv = "refresh" content ="0; url = gettimetable.php?create=1&ayear=' . $ayear . '&fby=' . $filter . '&programme=' . $programme . '&type=' . $type . '&yos=' . $yos . '">';
    exit;
}

if (!isset($_GET['create'])){
    ?>
    <style type="text/css">
        .hide {
            display: none;
        }
    </style>
    <div class="container">
        <div class="row ">
            <div class="col-sm-8 offset-sm-2">
                <div class="card">
                    <h3 class="card-header">
                        <?php echo $szTitle; ?></h3>
                    <div class="card-block">
                        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Time table Category</label>
                                <select name="tcategory" class="form-control">
                                    <?php
                                    while ($row = mysqli_fetch_array($result_timetablecategory)) {
                                        echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1"></label>
                                <input type="submit" class="btn btn-success btn-md btn-block" name="load"
                                       value="Load Timetable"/>
                            </div>
                        </form>
                        <div style="color: red">
                            <?php if (isset($_GET['error'])) {
                                echo $_GET['error'];
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/><br/>
    <?php
}

else{
$ayear = $_GET['ayear'];
$programme = $_GET['programme'];
$type = $_GET['type'];
$fby = $_GET['fby'];
$yos = $_GET['yos'];
$stream = $_GET['str'];
$cls = '';
$sem = '';

//Programme timetable
if ($fby == 1) {
    $sql = "SELECT * FROM timetable WHERE AYear='$ayear' AND YoS='$yos' AND Programme='$programme' 
					AND timetable_category='$type' ORDER BY day,start ASC";

    // generate timetable title
    $sql_title = "SELECT * FROM programme WHERE ProgrammeCode='$programme'";
    $title_result = mysqli_query($zalongwa, $sql_title);
    $fetch_title = mysqli_fetch_array($title_result);
    $title_timetable = $fetch_title['Title'] . ' - [ ' . $fetch_title['ProgrammeName'] . ' ] ';

    //SEMESTER ONE
    if ($type == 1) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER TWO
    elseif ($type == 2) {
        $stdyClass = array(1 => array('FIRST YEAR', 'II'), 2 => array('SECOND YEAR', 'II'),
            3 => array('THIRD YEAR', 'II'), 4 => array('FOURTH YEAR', 'II'),
            5 => array('FIFTH YEAR', 'II'), 6 => array('SIXTH YEAR', 'II'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER ONE EXAMINATION
    elseif ($type == 3) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER TWO EXAMINATION
    elseif ($type == 4) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SUPP/SPECIAL EXAMINATION
    elseif ($type == 5) {
        $stdyClass = array(1 => array('FIRST YEAR', ''), 2 => array('SECOND YEAR', ''),
            3 => array('THIRD YEAR', ''), 4 => array('FOURTH YEAR', ''),
            5 => array('FIFTH YEAR', ''), 6 => array('SIXTH YEAR', ''),
        );

        $subtitle_timetable = 'SUPPL/SPECIAL EXAMINATION TIMETABLE - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    }
} //Faculty timetable
else if ($fby == 2) {

    $sql_faculty = "SELECT * FROM faculty WHERE FacultyID='$programme'";
    $result_faculty = mysqli_query($zalongwa, $sql_faculty);
    $data = mysqli_fetch_array($result_faculty);
    $fc_ID = $programme;
    $fc_name = $data['FacultyName'];
    $sql_progr = "SELECT DISTINCT ProgrammeCode FROM programme WHERE Faculty='$fc_ID' OR Faculty='$fc_name'";

    $facult_prog = mysqli_query($zalongwa, $sql_progr);
    //$loop_prog_value='';
    while ($row = mysqli_fetch_array($facult_prog)) {
        $pg = $row['ProgrammeCode'];
        $loop_prog_value .= "  Programme='$pg' OR ";
    }

    $loop_prog_value = rtrim($loop_prog_value, " OR ");
    $loop_prog_value = " AND (" . $loop_prog_value . ' ) ';
    $sql = "SELECT * FROM timetable WHERE AYear='$ayear' $loop_prog_value AND YoS='$yos' 
					AND  timetable_category='$type' ORDER BY day,start ASC";

    // get title
    $title_timetable = $fc_name;

    //SEMESTER ONE
    if ($type == 1) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER TWO
    elseif ($type == 2) {
        $stdyClass = array(1 => array('FIRST YEAR', 'II'), 2 => array('SECOND YEAR', 'II'),
            3 => array('THIRD YEAR', 'II'), 4 => array('FOURTH YEAR', 'II'),
            5 => array('FIFTH YEAR', 'II'), 6 => array('SIXTH YEAR', 'II'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER ONE EXAMINATION
    elseif ($type == 3) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER TWO EXAMINATION
    elseif ($type == 4) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SUPP/SPECIAL EXAMINATION
    elseif ($type == 5) {
        $stdyClass = array(1 => array('FIRST YEAR', ''), 2 => array('SECOND YEAR', ''),
            3 => array('THIRD YEAR', ''), 4 => array('FOURTH YEAR', ''),
            5 => array('FIFTH YEAR', ''), 6 => array('SIXTH YEAR', ''),
        );

        $subtitle_timetable = 'SUPPL/SPECIAL EXAMINATION TIMETABLE - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    }
} //Lecturer timetable
elseif ($fby == 3) {
    $sql = "SELECT * FROM timetable WHERE  AYear='$ayear' AND lecturer='$programme' AND timetable_category='$type' ORDER BY day,start ASC";

    // generate timetable title
    $sql_title = "SELECT * FROM security WHERE UserName='$programme'";
    $title_result = mysqli_query($zalongwa, $sql_title);
    $fetch_title = mysqli_fetch_array($title_result);
    $title_timetable = $fetch_title['FullName'];


    //SEMESTER ONE
    if ($type == 1) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER TWO
    elseif ($type == 2) {
        $stdyClass = array(1 => array('FIRST YEAR', 'II'), 2 => array('SECOND YEAR', 'II'),
            3 => array('THIRD YEAR', 'II'), 4 => array('FOURTH YEAR', 'II'),
            5 => array('FIFTH YEAR', 'II'), 6 => array('SIXTH YEAR', 'II'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER ONE EXAMINATION
    elseif ($type == 3) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER TWO EXAMINATION
    elseif ($type == 4) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SUPP/SPECIAL EXAMINATION
    elseif ($type == 5) {
        $stdyClass = array(1 => array('FIRST YEAR', ''), 2 => array('SECOND YEAR', ''),
            3 => array('THIRD YEAR', ''), 4 => array('FOURTH YEAR', ''),
            5 => array('FIFTH YEAR', ''), 6 => array('SIXTH YEAR', ''),
        );

        $subtitle_timetable = 'SUPPL/SPECIAL EXAMINATION TIMETABLE - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    }
} //Stream Timetable
else if ($fby == 4) {

    $sql_stream = "SELECT * FROM classstream WHERE id='$stream'";
    $result_stream = mysqli_query($zalongwa, $sql_stream);
    $data = mysqli_fetch_assoc($result_stream);
    $str = $data['name'];

    $sql = "SELECT * FROM timetable WHERE AYear='$ayear' AND YoS='$yos' 
					AND  timetable_category='$type' AND class='$str' ORDER BY day,start ASC";
    // get title
    $title_timetable = $str;

    //SEMESTER ONE
    if ($type == 1) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER TWO
    elseif ($type == 2) {
        $stdyClass = array(1 => array('FIRST YEAR', 'II'), 2 => array('SECOND YEAR', 'II'),
            3 => array('THIRD YEAR', 'II'), 4 => array('FOURTH YEAR', 'II'),
            5 => array('FIFTH YEAR', 'II'), 6 => array('SIXTH YEAR', 'II'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER ONE EXAMINATION
    elseif ($type == 3) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER TWO EXAMINATION
    elseif ($type == 4) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SUPP/SPECIAL EXAMINATION
    elseif ($type == 5) {
        $stdyClass = array(1 => array('FIRST YEAR', ''), 2 => array('SECOND YEAR', ''),
            3 => array('THIRD YEAR', ''), 4 => array('FOURTH YEAR', ''),
            5 => array('FIFTH YEAR', ''), 6 => array('SIXTH YEAR', ''),
        );

        $subtitle_timetable = 'SUPPL/SPECIAL EXAMINATION TIMETABLE - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    }
} //Stream venueTimetable
else if ($fby == 5) {

    $sql_stream = "SELECT * FROM classstream WHERE VenueCode='$programme'";
    $result_stream = mysqli_query($zalongwa, $sql_stream);
    list($venue_name) = mysqli_fetch_array($result_stream);

    $sql = "SELECT * FROM timetable WHERE AYear='$ayear' AND YoS='$yos' 
					AND  timetable_category='$type' AND venue='$programme' ORDER BY day,start ASC";
    // get title
    $title_timetable = $venue_name;

    //SEMESTER ONE
    if ($type == 1) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER TWO
    elseif ($type == 2) {
        $stdyClass = array(1 => array('FIRST YEAR', 'II'), 2 => array('SECOND YEAR', 'II'),
            3 => array('THIRD YEAR', 'II'), 4 => array('FOURTH YEAR', 'II'),
            5 => array('FIFTH YEAR', 'II'), 6 => array('SIXTH YEAR', 'II'),
        );

        $subtitle_timetable = 'ACADEMIC TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER ONE EXAMINATION
    elseif ($type == 3) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE  SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SEMESTER TWO EXAMINATION
    elseif ($type == 4) {
        $stdyClass = array(1 => array('FIRST YEAR', 'I'), 2 => array('SECOND YEAR', 'I'),
            3 => array('THIRD YEAR', 'I'), 4 => array('FOURTH YEAR', 'I'),
            5 => array('FIFTH YEAR', 'I'), 6 => array('SIXTH YEAR', 'I'),
        );

        $subtitle_timetable = 'EXAMINATION TIMETABLE SEMESTER ' . $stdyClass[$yos][1] . ' - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    } //SUPP/SPECIAL EXAMINATION
    elseif ($type == 5) {
        $stdyClass = array(1 => array('FIRST YEAR', ''), 2 => array('SECOND YEAR', ''),
            3 => array('THIRD YEAR', ''), 4 => array('FOURTH YEAR', ''),
            5 => array('FIFTH YEAR', ''), 6 => array('SIXTH YEAR', ''),
        );

        $subtitle_timetable = 'SUPPL/SPECIAL EXAMINATION TIMETABLE - ' . $ayear . ' - ' . $stdyClass[$yos][0];
    }
}

$result = mysqli_query($zalongwa, $sql);
$num = mysqli_num_rows($result);


$timetable = array();
if ($num < 1){
    echo '<meta http-equiv = "refresh" content ="0; url = gettimetable.php?error=No data found!!!">';
    exit;
}
else{
$keys = array();
while ($row = mysqli_fetch_array($result)) {
    $usern = $row['lecturer'];
    $sql_lect = "SELECT FullName FROM security WHERE UserName='$usern'";
    $result_lect = mysqli_query($zalongwa, $sql_lect);
    list($lecturer_name) = mysqli_fetch_array($result_lect);

    $teaching = $row['teachingtype'];
    $sql_teach = "SELECT * FROM teachingtype WHERE id='$teaching'";
    $result_teach = mysqli_query($zalongwa, $sql_teach);
    $teach_type = mysqli_fetch_array($result_teach);

    //define class stream
    $klass = explode('-', trim($row['class'], ' '));
    if (count($klass) == 0) {
        $klass = '';
    } else {
        $klass = $klass[1] . ' - ' . substr(trim($klass[0]), -1);
    }

    $keys[$row['CourseCode']] = $row['CourseCode'];
    $timetable[$row['day']][$row['start']][] = array(
        'interval' => $row['start_end'],
        'start' => $row['start'],
        'end' => $row['end'],
        'course' => $row['CourseCode'],
        'venue' => $row['venue'],
        'lecturer' => $lecturer_name,
        'teaching' => $teach_type['name'],
        'class' => $klass
    );
}
?>
    <style type="text/css">
        #content span {
            color: black;
            font-weight: normal;
            font-size: 12px;
        }

        .view_timetable {
            table-layout: fixed;
            width: 900px;
            padding: 0px;
            margin: 0px;
            position: relative;
            border: 1px solid #CCCCCC;
            color: #000000;
        }

        .view_timetable tr td {
            border-bottom: 1px solid #CCCCCC;
            border-right: 1px solid #CCCCCC;
            position: relative;
        }

        .one {
            display: block;
            background-color: #DFD7CF;
            border: 1px solid blue;
            text-align: center;
            padding: 0px 0px 0px 0px;
            height: 80px;
            overflow: hidden;
            margin: 5px 0px 5px 0px;
        }

        .wiz {
            display: block;
            margin: 7px 0px 0px 0px;
            padding: 5px 0px 0px 0px;
        }

        .one span {
            display: block;
            color: black;
        }

        .two {
            display: block;
            background-color: #DFD7CF;
            border: 1px solid blue;
            z-index: 100;
            position: relative;
            text-align: center;
            padding: 0px 0px 0px 0px;
            height: 80px;
            overflow: hidden;
            margin: 5px 0px 5px 0px;
        }

        .two span {
            display: block;
            color: #000000;
        }

        .two-clas {
            width: 130px;
        }

        .three {
            display: block;
            background-color: #DFD7CF;
            border: 1px solid blue;
            text-align: center;
            height: 80px;
            overflow: hidden;
            padding: 0px 0px 0px 0px;
            margin: 5px 0px 5px 0px;
        }

        .three span {
            display: block;
            color: #000000;
        }

    </style>
    <div id="hello" style="margin-left: 7px">
        <div style="text-align: center; padding:10px 0px 0px 0px; ">
            <?php
            echo '<h2 style="padding:0px; margin:0px; color:black; font-size:20px; display:block; ">' . strtoupper($title_timetable) . '</h2>';
            echo '<h3 style="padding:0px; margin:0px; color:black; font-size:20px; display:block; ">' . $subtitle_timetable . '</h3>';
            ?>
        </div>

        <div style="color:blue; text-align:center; font-size:15px;font-weight:bold;  padding:10px 10px 10px 0px;">
            <form action="../includes/printtimetable.php" method="post">
                <input value="<?php echo $ayear; ?>" name="ayear" type="hidden"/>
                <input value="<?php echo $programme ?>" name="programme" type="hidden"/>
                <input value="<?php echo $type ?>" name="type" type="hidden"/>
                <input value="<?php echo $fby ?>" name="fby" type="hidden"/>
                <input value="<?php echo $yos; ?>" name="yos" type="hidden"/>
                <input value="<?php echo $stream; ?>" name="str" type="hidden">
                <input class="btn btn-primary" id="m" type="submit" value="Export Timetable" name="PRINT"/>
            </form>
        </div>


            <table class="table table-striped table-bordered table-responsive" width="100%" cellspacing="0">
            <tr>
                <td style="width:80px;">Day/Time</td>
                <td style="width:80px;">07:00 - 08:00</td>
                <td style="width:80px;">08:00 - 09:00</td>
                <td style="width:80px;">09:00 - 10:00</td>
                <td style="width:80px;">10:00 - 11:00</td>
                <td style="width:80px;">11:00 - 12:00</td>
                <td style="width:80px;">12:00 - 13:00</td>
                <td style="width:80px;">13:00 - 14:00</td>
                <td style="width:80px;">14:00 - 15:00</td>
                <td style="width:80px;">15:00 - 16:00</td>
                <td style="width:80px;">16:00 - 17:00</td>
                <td style="width:80px;">17:00 - 18:00</td>
                <td style="width:80px;">18:00 - 19:00</td>
                <td style="width:80px;">19:00 - 20:00</td>
                <td style="width:80px;">20:00 - 21:00</td>
                <td style="width:80px;">21:00 - 22:00</td>
            </tr>
            <?php
            $sql_day = "SELECT * FROM days ORDER BY id ASC";
            $get = mysqli_query($zalongwa, $sql_day);

            while ($row = mysqli_fetch_array($get)) {
            ?>
            <tr>
                <td style="width:50px;"><?php echo $row['name']; ?></td>
                <?php
                if (array_key_exists($row['id'], $timetable)){
                for ($p = 7;
                $p < 22;
                $p++) {
                if (array_key_exists($p, $timetable[$row['id']])){
                ?>
                <td valign="top">
                    <?php
                    foreach ($timetable[$row['id']][$p] as $key => $value) {
                        if ($value['interval'] == 1) {
                            ?>
                            <div class="one">
                                <span style="background-color:gray; padding:5px 0px 5px 0px;"><?php echo $value['teaching'] ?></span>
                                <span style="padding-top:2px;"><?php echo $value['course'] ?></span>
                                <span><?php echo $value['venue']; ?></span>
                                <span><?php echo $value['lecturer']; ?></span>
                                <span><?php echo $value['class']; ?></span>
                            </div>
                            <?php
                        } else if ($value['interval'] == 2) {
                            ?>
                            <div class="two">
                                <span style="font-size:12px; background-color:gray; padding:2px 0px 2px 0px;"><?php echo $value['teaching'] ?></span>
                                <span style="font-size:12px; padding-top:2px;"><?php echo $value['course'] ?></span>
                                <span style="font-size:12px;"><?php echo $value['venue']; ?></span>
                                <span style="font-size:12px;"><?php echo $value['lecturer']; ?></span>
                                <span><?php echo $value['class']; ?></span>
                            </div>
                            <?php
                        } else if ($value['interval'] == 3) {
                            ?>
                            <div class="three">
                                <span style="background-color:gray; padding:5px 0px 5px 0px;"><?php echo $value['teaching'] ?></span>
                                <span style="padding-top:5px;"><?php echo $value['course'] ?></span>
                                <span><?php echo $value['venue']; ?></span>
                                <span><?php echo $value['lecturer']; ?></span>
                                <span><?php echo $value['class']; ?></span>
                            </div>
                        <?php }
                    }
                    echo "</td>";
                    }
                    else {
                        echo '<td valign="top">&nbsp;</td>';
                    }
                    }
                    }
                    echo "</tr>";
                    }
                    ?>
        </table>
    </div>
<br/>
<div style="margin-left: 7px">
    <?php
    if (count($keys) > 0) {
        echo '<u><b>KEY</b></u><br/>';
        foreach ($keys as $k => $v) {
            $sele = "SELECT * FROM course WHERE CourseCode='$k'";
            $my = mysqli_query($zalongwa, $sele);
            $fetc = mysqli_fetch_array($my);
            echo $k . ' &nbsp; : &nbsp; &nbsp;' . $fetc['CourseName'] . '<br/>';
        }
    }

    echo "</p><br/><br/><br/>";
    }
    }
    ?>
</div>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('#fc').hide();
        $('#lect').hide();
        $('#str').hide();
        $('#venu').hide();

        $('#filter').change(function () {
            onchange_val = $(this).val();

            if (onchange_val == 3) {
                $('#lect').show();
                $('#str').hide();
                $('#fc').hide();
                $('#pr').hide();
                $('#venu').hide();

            } else if (onchange_val == 2) {
                $('#lect').hide();
                $('#str').hide();
                $('#fc').show();
                $('#pr').hide();
                $('#venu').hide();

            } else if (onchange_val == 1) {
                $('#lect').hide();
                $('#fc').hide();
                $('#str').hide();
                $('#pr').show();
                $('#venu').hide();

            } else if (onchange_val == 4) {
                $('#str').show();
                $('#lect').hide();
                $('#fc').hide();
                $('#pr').hide();
                $('#venu').hide();
            } else if (onchange_val == 5) {
                $('#venu').show();
                $('#str').hide();
                $('#lect').hide();
                $('#fc').hide();
                $('#pr').hide();
            }

        });


        var single = 60;

//period with 1hr interval
        $('.view_timetable').find('div[class=one]').each(function () {
            single = $(this).width();

        });

        // period with 3hrs interval
        $('.view_timetable').find('div[class=three]').each(function () {
            $(this).width(3 * single); // overwrite the width to fix 3 td

            width = $(this).width();
            offset = $(this).position();  // get position of the div
            height = $(this).height(); // get height of the div
            var ll = 0;
            $($(this).parent().next()).find('div').each(function () {  // loop all div in the next td
                $(this).css('position', 'relative');
                $(this).css('z-index', '2000');

                off = $(this).position();  // get position of the div in the next td
                he = $(this).height();
                if (width > single) {
                    if (off.top == offset.top) {  // if the div fall in the same possition append div aon top

                        ass = height;

                        $('<div class="wiz" style="position:relative;z-index:2000;height:' + ass + 'px">ll</div>').insertBefore(this); // insert div before the overlaping div
                    }
                }
            });

            //loop next td /// the 3rd td, the same procedure as above
            $($(this).parent().next().next()).find('div').each(function () {
                $(this).css('position', 'relative');
                $(this).css('z-index', '2000');
                // alert('sdsd');
                off = $(this).position();
                he = $(this).height();
                if (width > single) {
                    if (off.top == offset.top) {

                        ass = height;

                        $('<div class="wiz" style="position:relative;z-index:3000;height:' + ass + 'px"></div>').insertBefore(this);
                    }
                }
            });
        });


        // deal with period of 2hrs interval
        $('.view_timetable').find('div[class=two]').each(function () {

            $(this).width(2 * single);
            width = $(this).width();

            //$(this).append($(this).width());
            offset = $(this).position();
            height = $(this).height();
            var ll = 0;
            $($(this).parent().next()).find('div').each(function () {
                $(this).css('position', 'relative');
                $(this).css('z-index', '2000');

                off = $(this).position();
                he = $(this).height();
                if (width > single) {
                    if (off.top == offset.top) {

                        ass = height;

                        $('<div class="wiz" style="position:relative;z-index:2000;height:' + ass + 'px"></div>').insertBefore(this);
                    }
                }
            });

        });

    });
</script>

<br><br>
<!--footer-->
<?php include '../footer/footer.php'; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!--adding tooltip-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!--Modernaizer here check if not svg supported replace with png-->
<script>
    if (!Modernizr.svg) var i = document.getElementsByTagName("img"), j, y;
    for (j = i.length; j--;) y = i[j].src, y.match(/svg$/) && (i[j].src = y.slice(0, -3) + "png")
</script>
</body>
</html>
