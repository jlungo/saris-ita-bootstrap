<?php
#start html
if (isset($_POST['HTML']) && ($_POST['HTML'] == "Print HTML")){
	
	#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
	require_once('../Connections/zalongwa.php');
	
	# include menu
	include('billingMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Financial Reports';
	$szSubSection = 'Debtors Report';
	$szTitle = 'Creating Debtors Report';
	include('billingheader.php');
	
	include '../billing/includes/print_html_debtor_report.php';
	include('../footer/footer.php');
	exit;
}
elseif (isset($_POST['PDF']) && ($_POST['PDF'] == "Print PDF")){
	#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
	require_once('../Connections/zalongwa.php');

	#Get Organisation Name
	$qorg = "SELECT * FROM organisation";
	$dborg = mysql_query($qorg);
	$row_org = mysql_fetch_assoc($dborg);
	$org = $row_org['Name'];

	@$checkdegree = addslashes($_POST['checkdegree']);
	@$checkyear = addslashes($_POST['checkyear']);
	@$checkdept = addslashes($_POST['checkdept']);
	@$checkcohot = addslashes($_POST['checkcohot']);
	@$paper = addslashes($_POST['paper']);
	@$layout = addslashes($_POST['layout']);
	if ($paper=='a3')
	{
		$xpoint = 1050.00;
		$ypoint = 800.89;
	}else
	{
		$xpoint = 800.89;
		$ypoint = 580.28;
	}

	$prog=$_POST['degree'];
	$cohotyear = $_POST['cohot'];
	$ayear = $_POST['ayear'];
	$qprog= "SELECT ProgrammeCode, Title FROM programme WHERE ProgrammeCode='$prog'";
	$dbprog = mysql_query($qprog);
	$row_prog = mysql_fetch_array($dbprog);
	$progname = $row_prog['Title'];
		
	//calculate year of study
	$entry = intval(substr($cohotyear,0,4));
	$current = intval(substr($ayear,0,4));	
	$yearofstudy=$current-$entry;
	
	if($yearofstudy==0){
		$class="FIRST YEAR";
		}elseif($yearofstudy==1){
		$class="SECOND YEAR";
		}elseif($yearofstudy==2){
		$class="THIRD YEAR";
		}elseif($yearofstudy==3){
		$class="FOURTH YEAR";
		}elseif($yearofstudy==4){
		$class="FIFTH YEAR";
		}elseif($yearofstudy==5){
		$class="SIXTH YEAR";
		}elseif($yearofstudy==6){
		$class="SEVENTHND YEAR";
		}else{
		$class="";
	}
	$yearofstudy = $yearofstudy +1;
	
	if (($checkdegree=='on') && ($checkyear == 'on') && ($checkcohot == 'on')){
		$deg=addslashes($_POST['degree']);
		$year = addslashes($_POST['ayear']);
		$cohot = addslashes($_POST['cohot']);
		$dept = addslashes($_POST['dept']);
		$sem = addslashes($_POST['sem']);
		
		#determine total number of columns
		$qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')";
		$dbstd = mysql_query($qstd);
		$totalstudent = mysql_num_rows($dbstd);
		$totalcolms = 0;
		if($totalstudent < 1){
			$error = urlencode("Sorry! There are no records for the choices you have made");
			$location = "billingDebtorsreport.php?error=$error";
			echo '<meta http-equiv="refresh" content="0; url='.$location.'">';
			exit;
			}
			
		while($rowstd = mysql_fetch_array($dbstd)) {
			$stdregno = $rowstd['RegNo'];
			//$qstdcourse = "SELECT DISTINCT coursecode FROM examresult WHERE RegNo='$stdregno' and AYear='$year' and Semester = '$sem'";
			$qstdfee = "SELECT DISTINCT feecode FROM studentpayments WHERE RegNo='$stdregno' and AYear='$year'";
			$dbstdfee = mysql_query($qstdfee);
			$totalstdfee = mysql_num_rows($dbstdfee);
			if ($totalstdfee>$totalcolms){
				$totalcolms = $totalstdfee;
			}
		}
		
		#exit if there are no transactions made
		if($totalcolms < 1){
			$error = urlencode("Sorry! There are transactions recorded yet");
			$location = "billingDebtorsreport.php?error=$error";
			echo '<meta http-equiv="refresh" content="0; url='.$location.'">';
			exit;
			}
			
		#start pdf
		include('includes/PDF.php');
		$pdf = &PDF::factory($layout, $paper);      
		$pdf->open();                         
		$pdf->setCompression(true);           
		$pdf->addPage();  
		$pdf->setFont('Arial', 'I', 8);     
		$pdf->text(50, $ypoint, 'Printed On '.$today = date("d-m-Y H:i:s"));  		
 
		#put page header
		$x=60;
		$y=50;
		$i=1;
		$pg=1;
		$pdf->text($xpoint,$ypoint, 'Page '.$pg);   
		
		#count unregistered
		$j=0;
		#count sex
		$fmcount = 0;
		$mcount = 0;
		$fcount = 0;
		
		#print header for landscape paper layout 
		$playout ='l'; 
		include '../includes/orgname.php';
		
		$pdf->setFillColor('rgb', 0, 0, 0);    
		$pdf->setFont('Arial', '', 13);      
		$pdf->text($x+225, $y+14, 'DEBTORS OUTSTANDING REPORT');
		$pdf->text($x+240, $y+28, 'ACADEMIC YEAR: '.$year); 
		$pdf->text($x+6, $y+48, $class.' - '.strtoupper($progname)); 
		#reset values of x,y
		$x=50; $y=$y+54;
		
		#set table header
		$pdf->setFont('Arial', '', 8); 
		$pdf->line($x, $y, $xpoint+25, $y); 
		$pdf->line($x, $y+15, $xpoint+25, $y+15); 
		$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'S/No');
		$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, 'RegNo');
		#adjust x vlaue
		$x=$x-45;
		$pdf->line($x+176, $y, $x+176, $y+15);	$pdf->text($x+180, $y+12, 'Sex');
		$pdf->line($x+200, $y, $x+200, $y+15);	$pdf->text($x+275, $y+12, 'FEES'); 
		#set colm width
		$clmw = 30;
		#get column width factor
		$cwf = 30;
		#calculate course clumns widths
		$cw = $cwf*$totalcolms;
		$x=$x+200;
		$pdf->line($x+$cw, $y, $x+$cw, $y+15);	$pdf->text($x+$cw+1, $y+12, 'PAID');
		$pdf->line($x+$cw+60, $y, $x+$cw+60, $y+15);	$pdf->text($x+$cw+62, $y+12, 'CHARGEABLE'); 
		$pdf->line($x+$cw+125, $y, $x+$cw+125, $y+15);	$pdf->text($x+$cw+127, $y+12, 'OUTSTANDING'); 
		$pdf->line($x+$cw+195, $y, $x+$cw+195, $y+15);	$pdf->text($x+$cw+197, $y+12, 'Remark');
		$pdf->line($xpoint+25, $y, $xpoint+25, $y+15);  
		$y=$y+15;
		
		#query student list
		$qstudent = "SELECT * FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY RegNo";
		$dbstudent = mysql_query($qstudent);
		$totalstudent = mysql_num_rows($dbstudent);
		$i=1;
		$grandtotal = 0;
		$grate =0;
		$gbf =0;
		$chas = 0;
			
			//chas
			$fet = mysql_fetch_array($dbstudent);
			foreach($fet as $val){
				$regno = $val[1];		
				$dbfeeunit = mysql_query("SELECT DISTINCT studentpayments.feecode FROM studentpayments WHERE 
										(studentpayments.regno='$regno') AND (studentpayments.ayear='$year')");
				$chas1 = mysql_num_rows($dbfeeunit);
				$chas = ($chas>$chas1)? $chas:$chas1;
				}			
				
			while($rowstudent = mysql_fetch_array($dbstudent)) {
					$name = $rowstudent['Name'];
					$regno = $rowstudent['RegNo'];
					$sex = $rowstudent['Sex'];
					$prog = $rowstudent['ProgrammeofStudy'];
					$sponsor = $rowstudent['Sponsor'];
					$clac = $rowstudent['Class'];					
					
					#check if basic technician course
					$qprograms = "SELECT Title FROM programme WHERE ProgrammeCODE ='$prog'";
					$dbprograms = mysql_query($qprograms);
					$row_programs = mysql_fetch_assoc($dbprograms);
					$degrees = $row_programs['Title'];
					
					#get all fee rates for this candidate					
					/*$qrate="SELECT feecode, amount FROM feesrates WHERE (programmecode='$deg') AND 
							(ayear='$ayear') AND (sponsor ='$sponsor') AND (yearofstudy='$yearofstudy')";
					$dbrates = mysql_query($qrate);
						$rate = 0;
						while($row_rates = mysql_fetch_array($dbrates)) {
								$rateinstall = $row_rates['amount'];
								//$rate = $rate + $rateinstall;
								$rate = $rateinstall;
						}*/
					
					$qdebtorlimit = "SELECT Amount, Debtorlimit FROM invoicelimit WHERE (AYear='$ayear') AND (RegNo='$regno')";
					
					$dbdebtorlimit=mysql_query($qdebtorlimit);
					$row_debtorlimit=mysql_fetch_array($dbdebtorlimit);
					$debtorlimit = $row_debtorlimit['Debtorlimit'];
					$rate = $row_debtorlimit['Amount'];
					
					# get all feecodes for this candidate
					$qfee="SELECT DISTINCT studentpayments.feecode FROM 
								studentpayments WHERE (studentpayments.regno='$regno') AND 
								(studentpayments.ayear='$year')";	
								
					$dbfee = mysql_query($qfee);
					$dbfeeunit = mysql_query($qfee);
					$total_rows = mysql_num_rows($dbfee);
					
					if($total_rows>0){
		
						#initialise computation
						$totalamount=0;
						$amount=0;
						$outstand=0;
						$subtotal = 0;
						$balance=0;
						$key = $regno; 
						$x=50;
						
						#print student info
						$pdf->setFont('Arial', '', 7); 
						$pdf->line($x, $y, $xpoint+25, $y); 
						$pdf->line($x, $y+28, $xpoint+25, $y+28); 
						$pdf->line($x, $y, $x, $y+28); 			$pdf->text($x+2, $y+16, $i);
						$pdf->line($x+25, $y, $x+25, $y+28); $pdf->text($x+27, $y+12, $name);  $pdf->text($x+27, $y+25, strtoupper($regno)); 
						$x=$x-45;
						$pdf->line($x+176, $y, $x+176, $y+28);	$pdf->text($x+183, $y+26, strtoupper($sex));
						$pdf->line($x+200, $y, $x+200, $y+28);	
						
						#calculate fees clumns widths
						$clnspace = $x+200; 
							while($rowfee = mysql_fetch_array($dbfee)) { 
								$stdfee = $rowfee['feecode']; 
								$feefull = explode(" ",$stdfee);
								$feeleft = $feefull[0];
								$feeright = $feefull[1];
								$pdf->text($clnspace+1, $y+12, $stdfee);
								$pdf->line($clnspace, $y+28, $clnspace+$clmw, $y+28);
								$pdf->line($clnspace+$clmw, $y, $clnspace+$clmw, $y+28);
								$clnspace = $clnspace+$clmw;	
							 } 
							#reset colm space
							$clmspace = $x+200; 
								$subtotal = 0;
																	
								while($row_fee = mysql_fetch_array($dbfeeunit)){
									$fee= $row_fee['feecode'];
									#get all payments related to this fee
									$qamount="SELECT amount FROM studentpayments WHERE 
															(studentpayments.regno='$regno') AND 
															(studentpayments.feecode='$fee') AND 
															(studentpayments.ayear='$year')";	
															
									$dbamount = mysql_query($qamount);
										#get total amount paid
										$amount = 0;
										while($row_amount = mysql_fetch_array($dbamount)){
											$install = $row_amount['amount'];
											$amount = $amount + $install;
										}
									#print subtotal
									$pdf->text($clmspace+1, $y+24, '('.$amount.')');
															
									$subtotal = $subtotal + $amount;
									$pdf->line($clmspace+$clmw, $y, $clmspace+$clmw, $y+28);
									$clmspace = $clmspace+$clmw;	
								}
									$balance = $rate - $subtotal;
									
									#calculate feecode column width
									$cw = $cwf*$totalcolms;
									$x=$x+200;
									$pdf->line($x+$cw, $y, $x+$cw, $y+28);	$pdf->text($x+$cw+3, $y+24, number_format($subtotal,2,'.',',')); 
									$pdf->line($x+$cw+60, $y, $x+$cw+60, $y+28); $pdf->text($x+$cw+62, $y+24, number_format($rate,2,'.',',')); 
									$pdf->line($x+$cw+125, $y, $x+$cw+125, $y+28); $pdf->text($x+$cw+127, $y+24, number_format($balance,2,'.',',')); 
									#calculate column totals
									$grate = $grate+$rate;
									$gbf = $gbf +$balance;
									$gpaid = $gpaid + $subtotal;
									
									#get student remarks
									$qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$regno'";
									$dbremarks = mysql_query($qremarks);
									$row_remarks = mysql_fetch_assoc($dbremarks);
									$totalremarks = mysql_num_rows($dbremarks);
									$studentremarks = $row_remarks['Remark'];
									if(($totalremarks>0)&&($studentremarks<>'')){
										$remark = $studentremarks;
									}
									$pdf->line($x+$cw+195, $y, $x+$cw+195, $y+28);	$pdf->text($x+$cw+197, $y+14, $remark);
									$pdf->line($xpoint+25, $y, $xpoint+25, $y+28); 
									#empty remark value
									$remark = '';
						
					  $i=$i+1;
					  $y=$y+28;
					  if($y>=$ypoint-50){
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 8);     
						$pdf->text(50, $ypoint, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text($xpoint,$ypoint, 'Page '.$pg);   
						
						#count unregistered
						$j=0;
						#count sex
						$fmcount = 0;
						$mcount = 0;
						$fcount = 0;
						#set table header
						$pdf->setFont('Arial', '', 9); 
						$pdf->line($x, $y, $xpoint+25, $y); 
						$pdf->line($x, $y+15, $xpoint+25, $y+15); 
						$pdf->line($x, $y, $x, $y+15); 			$pdf->text($x+2, $y+12, 'S/No');
						$pdf->line($x+25, $y, $x+25, $y+15);	$pdf->text($x+27, $y+12, 'RegNo');
						#adjust x vlaue
						$x=$x-45;
						$pdf->line($x+176, $y, $x+176, $y+15);	$pdf->text($x+180, $y+12, 'Sex');
						$pdf->line($x+200, $y, $x+200, $y+15);	$pdf->text($x+275, $y+12, 'FEES'); 
						
						#calculate fees clumns widths
						$cw = $cwf*$totalcolms;
						$x=$x+200;
						$pdf->line($x+$cw, $y, $x+$cw, $y+15);	$pdf->text($x+$cw+1, $y+12, 'PAID');
						$pdf->line($x+$cw+60, $y, $x+$cw+60, $y+15);	$pdf->text($x+$cw+62, $y+12, 'CHARGEABLE'); 
						$pdf->line($x+$cw+125, $y, $x+$cw+125, $y+15);	$pdf->text($x+$cw+127, $y+12, 'OUTSTANDING'); 
						$pdf->line($x+$cw+195, $y, $x+$cw+195, $y+15);	$pdf->text($x+$cw+197, $y+12, 'Remark');
						$pdf->line($xpoint+25, $y, $xpoint+25, $y+15);  
						$y=$y+15;
						
					  }
				   } //ends if $total_rows
				   $grandtotal = $grandtotal + $subtotal;
				}//ends $rowstudent loop
		$pdf->setFont('Arial', 'B', 8); 
		$pdf->line($x+$cw+60, $y, $x+$cw+60, $y+18);
		$pdf->line($x+$cw+125, $y, $x+$cw+125, $y+18);
		$pdf->line($x+$cw+195, $y, $x+$cw+195, $y+18);
		$pdf->line($x, $y, $xpoint+25, $y); 
		$pdf->line($xpoint+25, $y, $xpoint+25, $y+18); 
		$pdf->text($x+$cw+1, $y+12, number_format($gpaid,2,'.',',')); 
		$pdf->text($x+$cw+62, $y+12, number_format($grate,2,'.',','));
		$pdf->text($x+$cw+127, $y+12, number_format($gbf,2,'.',','));
		$pdf->line($x+$cw, $y, $x+$cw, $y+18);
		$x = 50;
		$pdf->text($x+3, $y+12, 'Grand Total:'); 
		$pdf->line($x, $y, $x, $y+18);
		$pdf->line($x, $y+18, $xpoint+25, $y+18); 
		$pdf->setFont('Arial', '', 9); 
		#start new page for the keys
		$space = $ypoint-50 - $y;
		$yind = $y+30; 
		if($space<70){
			$pdf->addPage();  

			$x=50;
			$y=50;
			$pg=$pg+1;
			$tpg =$pg;
			$pdf->setFont('Arial', 'I', 8);     
			$pdf->text(50, $ypoint-50, 'Printed On '.$today = date("d-m-Y H:i:s"));   
			$pg=$pg+1;
			$pdf->text($xpoint,$ypoint-50, 'Page '.$pg);   
			$yind = $y; 
 }
 /*
 	$pdf->text(450, $yind, 'Signature of The Dean:  ���������     ');  
	$pdf->text(450, $yind+12, 'Date: ���������������������       ');
 	$pdf->text(450, $yind+24, 'Signature of the Chairperson of the Senate:  ����������');  
	$pdf->text(450, $yind+36, 'Date: ����������������������');  
*/
	$pdf->setFont('Arial', 'I', 9); 
	$pdf->text(190.28, $yind, '          ######## END OF DEBTORS REPORT ########');   
	/*
	$pdf->text(50, $yind + 12, '1. Key for Fees Codes:'); 
	
	#get feecodes desciption
	$qfees = "select * from fees order by feecode";
	$dbfee =mysql_query($qfees);
	$h = 24;
	while ($row_fees = mysql_fetch_assoc($dbfee)) {
	$pdf->text(50, $yind + $h, $row_fees['feecode'].'= '.$row_fees['name']); 
	$h = $h+12;
	}
	*/
	$x=50;
	$y= $yind + $h+44;
	#table 1
 	 #output file
	 $filename = ereg_replace("[[:space:]]+", "",$progname);
	 $pdf->output($filename.'.pdf');
	 }
}//end of print pdf

 
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# include menu
	include('billingMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Financial Reports';
	$szSubSection = 'Debtors Report';
	$szTitle = 'Creating Debtors Report';
	include('billingheader.php');

$editFormAction = $_SERVER['PHP_SELF'];

mysql_select_db($database_zalongwa, $zalongwa);
$query_studentlist = "SELECT RegNo, Name, ProgrammeofStudy FROM student ORDER BY ProgrammeofStudy  ASC";
$studentlist = mysql_query($query_studentlist, $zalongwa) or die(mysql_error());
$row_studentlist = mysql_fetch_assoc($studentlist);
$totalRows_studentlist = mysql_num_rows($studentlist);

mysql_select_db($database_zalongwa, $zalongwa);
$query_degree = "SELECT ProgrammeCode, ProgrammeName FROM programme ORDER BY ProgrammeName ASC";
$degree = mysql_query($query_degree, $zalongwa) or die(mysql_error());
$row_degree = mysql_fetch_assoc($degree);
$totalRows_degree = mysql_num_rows($degree);

mysql_select_db($database_zalongwa, $zalongwa);
$query_ayear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$ayear = mysql_query($query_ayear, $zalongwa) or die(mysql_error());
$row_ayear = mysql_fetch_assoc($ayear);
$totalRows_ayear = mysql_num_rows($ayear);

mysql_select_db($database_zalongwa, $zalongwa);
$query_sem = "SELECT Semester FROM terms ORDER BY Semester";
$sem = mysql_query($query_sem, $zalongwa) or die(mysql_error());
$row_sem = mysql_fetch_assoc($sem);
$totalRows_sem = mysql_num_rows($sem);

mysql_select_db($database_zalongwa, $zalongwa);
$query_dept = "SELECT Faculty, DeptName FROM department ORDER BY DeptName, Faculty ASC";
$dept = mysql_query($query_dept, $zalongwa) or die(mysql_error());
$row_dept = mysql_fetch_assoc($dept);
$totalRows_dept = mysql_num_rows($dept);

mysql_select_db($database_zalongwa, $zalongwa);
$query_fee = "SELECT feecode, name FROM fees ORDER BY name ASC";
$fee = mysql_query($query_fee, $zalongwa);
$row_fee = mysql_fetch_assoc($fee);
$totalRows_fee = mysql_num_rows($fee);

mysql_select_db($database_zalongwa, $zalongwa);
$query_sponsor = "SELECT Name FROM sponsors ORDER BY Name ASC";
$sponsor = mysql_query($query_sponsor, $zalongwa);
$row_sponsor = mysql_fetch_assoc($sponsor);
$totalRows_sponsor = mysql_num_rows($sponsor);
?>
<?php
			
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {


}else{
?>

<form name="form1" method="post" action="<?php echo $editFormAction ?>">
            <div align="center">
			<table width="200" border="0" bgcolor="#CCCCCC">
            <tr>
                  <td colspan="3"><span class="style61">if you want to filter the results by  criteria <span class="style34">Tick the corresponding check box first</span> then select appropriately </span></td>
                </tr>
                <tr>
                  <td nowrap><input name="checkdegree" type="checkbox" id="checkdegree" value="on" checked></td>
                  <td nowrap><div align="left">Programme of Study:</div></td>
                  <td>
                      <div align="left">
                        <select name="degree" id="degree">
                          <?php
do {  
?>
                          <option value="<?php echo $row_degree['ProgrammeCode']?>"><?php echo $row_degree['ProgrammeName']?></option>
                          <?php
} while ($row_degree = mysql_fetch_assoc($degree));
  $rows = mysql_num_rows($degree);
  if($rows > 0) {
      mysql_data_seek($degree, 0);
	  $row_degree = mysql_fetch_assoc($degree);
  }
?>
                        </select>
                    </div></td></tr>
                <tr>
                  <td><input name="checkcohot" type="checkbox" id="checkcohot" value="on" checked></td>
                  <td nowrap><div align="left">Intake of the  Year: </div></td>
                  <td><div align="left">
                    <select name="cohot" id="cohot">
                        <?php
do {  
?>
                        <option value="<?php echo $row_ayear['AYear']?>"><?php echo $row_ayear['AYear']?></option>
                        <?php
} while ($row_ayear = mysql_fetch_assoc($ayear));
  $rows = mysql_num_rows($ayear);
  if($rows > 0) {
      mysql_data_seek($ayear, 0);
	  $row_ayear = mysql_fetch_assoc($ayear);
  }
?>
                    </select>
                  </div></td>
                </tr>
            	<tr>
                  <td><input name="checkyear" type="checkbox" id="checkyear" value="on" checked></td>
                  <td nowrap><div align="left">Audit Year: </div></td>
                  <td><div align="left">
                    <select name="ayear" id="ayear">
                        <?php
do {  
?>
                        <option value="<?php echo $row_ayear['AYear']?>"><?php echo $row_ayear['AYear']?></option>
                        <?php
} while ($row_ayear = mysql_fetch_assoc($ayear));
  $rows = mysql_num_rows($ayear);
  if($rows > 0) {
      mysql_data_seek($ayear, 0);
	  $row_ayear = mysql_fetch_assoc($ayear);
  }
?>
                    </select>
                  </div></td>
                </tr>
                <tr>
                  <td><input name="checksponsor" type="checkbox" id="checksponsor" value="on" ></td>
                  <td nowrap><div align="left">Sponsorship: </div></td>
                  <td><div align="left">
                    <select name="sponsor" id="sponsor">
                        <?php
do {  
?>
                        <option value="<?php echo $row_sponsor['Name']?>"><?php echo $row_sponsor['Name']?></option>
                        <?php
} while ($row_sponsor = mysql_fetch_assoc($sponsor));
  $rows = mysql_num_rows($sponsor);
  if($rows > 0) {
      mysql_data_seek($sponsor, 0);
	  $row_sponsor = mysql_fetch_assoc($sponsor);
  }
?>
                    </select>
                  </div></td>
                </tr>
                <tr>
                  <td><input name="checkfee" type="checkbox" id="checkfee" value="on" ></td>
                  <td nowrap><div align="left">Fee Category: </div></td>
                  <td><div align="left">
                    <select name="fee" id="fee">
                        <?php
do {  
?>
                        <option value="<?php echo $row_fee['feecode']?>"><?php echo $row_fee['name']?></option>
                        <?php
} while ($row_fee = mysql_fetch_assoc($fee));
  $rows = mysql_num_rows($fee);
  if($rows > 0) {
      mysql_data_seek($ayear, 0);
	  $row_fee = mysql_fetch_assoc($fee);
  }
?>
                    </select>
                  </div></td>
                </tr>
            	
            	<tr>
                  <td colspan="3" nowrap><div align="center">Paper Size:
					A4
                    <input name="paper" type="radio" value="a4" checked>	
					A3
					<input name="paper" type="radio" value="a3">
                   </div></td> 
                </tr>
            	<tr>
                  <td colspan="3" nowrap><div align="center">Layout:
					Landscape
                    <input name="layout" type="radio" value="L" checked>	
					Portrait
					<input name="layout" type="radio" value="P">
                   </div></td> 
                </tr>
                
                <tr>
                <td colspan="2"><div align="center">
	                    <input type="submit" name="HTML"  id="HTML" value="Print HTML">
	                  </div></td>
                  <td colspan="1"><div align="center">
                    <input type="submit" name="PDF"  id="PDF" value="Print PDF">
                  </div></td>
              </tr>
              </table>
              <input name="MM_update" type="hidden" id="MM_update" value="form1">       
  </div>
</form>
<?php
	if(isset($_GET['error'])){
		$error = urldecode($_GET['error']);
		echo "<p style='color:maroon'>$error</p>";
		}
}
include('../footer/footer.php');
?>
