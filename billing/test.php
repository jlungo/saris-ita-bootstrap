<?php
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
	require_once('../Connections/zalongwa.php');

#start pdf
require('includes/fpdf.php');
//Select the Products you want to show in your PDF file
$result=mysql_query("select * from fees order by id");


//Initialize the 3 columns 
$column_id = "";
$column_code = "";
$column_name= "";


//For each row, add the field to the corresponding column
while($row = mysql_fetch_array($result))
{
    $id = $row["id"];
    $code= $row["feecode"];
    $name= $row["name"];

    $column_id = $column_id.$id."\n";
    $column_code = $column_code.$code."\n";
    $column_name = $column_name.$name."\n";
}
mysql_close();
//Create a new PDF file
$pdf=new FPDF();
$pdf->AddPage();

//Fields Name position
$Y_Fields_Name_position = 20;
//Table position, under Fields Name
$Y_Table_Position = 26;

//First create each Field Name
//Gray color filling each Field Name box
$pdf->SetFillColor(232,232,232);
//Bold Font for Field Name
$pdf->SetFont('Arial','B',12);
$pdf->SetY($Y_Fields_Name_position);
$pdf->SetX(45);
$pdf->Cell(20,6,'S/No',1,0,'L',1);
$pdf->SetX(65);
$pdf->Cell(30,6,'CODE',1,0,'L',1);
$pdf->SetX(90);
$pdf->Cell(75,6,'Description',1,0,'L',1);
$pdf->Ln();

//Now show the 3 columns
$pdf->SetFont('Arial','',11);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(45);
$pdf->MultiCell(20,6,$column_id,1,'L');
$pdf->SetY($Y_Table_Position);
$pdf->SetX(65);
$pdf->MultiCell(100,6,$column_code,1,'L');
$pdf->SetY($Y_Table_Position);
$pdf->SetX(90);
$pdf->MultiCell(75,6,$column_name,1,'L');

//Create lines (boxes) for each ROW (Product)
//If you don't use the following code, you don't create the lines separating each row
$i = 0;
$pdf->SetY($Y_Table_Position);
while ($i < $result)
{
    $pdf->SetX(45);
    $pdf->MultiCell(120,6,'',1);
    $i = $i +1;
}

$pdf->Output();
?> 