<?php
 if ($module <> 7) {
		session_start(); 
		session_cache_limiter('nocache');
		$_SESSION = array();
		session_unset(); 
		session_destroy(); 
		echo '<meta http-equiv = "refresh" content ="0;	url = ../index.php">';		
	}

	# start the session
	session_start();
	
	# include the global settings
	
	require_once('../Connections/zalongwa.php'); 
	global $blnPrivateArea,$szHeaderPath,$szFooterPath,$szRootPath;
	$blnPrivateArea = false;
	$szHeaderPath = 'header.php';
	$szFooterPath = 'footer.php';

	# define Top level Navigation Array if not defined already
	
	$arrStructure = array();$i=1;
		
			//Help
	$arrStructure[$i] = array( 'name1' => 'Help', 'name2' => 'Usalama', 'url' => 'accountingUserManual.php', 'image' => '',  'width' => '', 'height' => '');
	$arrStructure[$i]['subsections'] = array(); $j = 1;
	$arrStructure[$i]['subsections'][$j] = array( 'name1' => 'User Manual', 'name2' => 'Usaidizi', 'url' => 'accountingUserManual.php', 'width' => '', 'height' => '');
	$j++;
	$i++;
	// Profile
	$arrStructure[$i] = array( 'name1' => 'Profile', 'name2' => 'Profile', 'url' => 'billingprofile.php', 'width' => '20', 'height' => '50');
	$i++;
	
	// Policy Setup
	#check if user is a manager
	if ($privilege==2){
		$arrStructure[$i] = array( 'name1' => 'Policy Setup', 'name2' => 'Plicy Setup', 'url' => 'billingpolicy.php', 'image' => '',  'width' => '2', 'height' => '3');
		$arrStructure[$i]['subsections'] = array(); $j=1;
		$arrStructure[$i]['subsections'][$j] = array( 'name1' => 'Billing Rates', 'name2' => 'Shahada', 'url' => 'billingPayrate.php', 'width' => '', 'height' => '');
		$j++;
		$arrStructure[$i]['subsections'][$j] = array( 'name1' => 'Billing Categories', 'name2' => 'Shahada', 'url' => 'billingCategories.php', 'width' => '', 'height' => '');
		$j++;
		$arrStructure[$i]['subsections'][$j] = array( 'name1' => 'Billing Structures', 'name2' => 'Shahada', 'url' => 'billingFreestructure.php', 'width' => '', 'height' => '');
		$j++;
		$i++;
	}
		// Financial reports
	#check if user is a manager
	if ($privilege==2){
		$arrStructure[$i] = array( 'name1' => 'Financial Reports', 'name2' => 'Plicy Setup', 'url' => 'billingRecordbilling.php', 'image' => '',  'width' => '2', 'height' => '3');
		$arrStructure[$i]['subsections'] = array(); $j=1;
		$arrStructure[$i]['subsections'][$j] = array( 'name1' => 'Record Payment', 'name2' => 'Chuo', 'url' => 'billingRecordbilling.php', 'width' => '', 'height' => '');
		$j++;
		$arrStructure[$i]['subsections'][$j] = array( 'name1' => 'Debtors Report', 'name2' => 'Chuo', 'url' => 'billingDebtorsreport.php', 'width' => '', 'height' => '');
		$j++;
		$arrStructure[$i]['subsections'][$j] = array( 'name1' => 'Timely Reports', 'name2' => 'Wadeni', 'url' => 'billingTimeReport.php', 'width' => '', 'height' => '');
		$j++;
		$arrStructure[$i]['subsections'][$j] = array( 'name1' => 'Receipts', 'name2' => 'Chuo', 'url' => 'billingStudentpayment.php', 'width' => '', 'height' => '');
		$j++;
		$arrStructure[$i]['subsections'][$j] = array( 'name1' => 'Refunds', 'name2' => 'Shahada', 'url' => 'billingpaymentrefund.php', 'width' => '', 'height' => '');
		$j++;
		$i++;
	}
	// Communication
	$arrStructure[$i] = array( 'name1' => 'Communication', 'name2' => 'Mawasiliano', 'url' => 'billingCheckMessage.php', 'image' => '',  'width' => '', 'height' => '');
	$arrStructure[$i]['subsections'] = array(); $j = 1;
	$arrStructure[$i]['subsections'][$j] = array( 'name1' => 'Check Message', 'name2' => 'Pata Habari', 'url' => 'billingCheckMessage.php', 'width' => '', 'height' => '');
	$j++;
	$arrStructure[$i]['subsections'][$j] = array( 'name1' => 'News & Events', 'name2' => 'Pata Habari', 'url' => 'studentNews.php', 'width' => '', 'height' => '');
	$j++;
	$i++;
	//Security
	$arrStructure[$i] = array( 'name1' => 'Security', 'name2' => 'Usalama', 'url' => 'billingSecurity.php', 'image' => '',  'width' => '', 'height' => '');
	$arrStructure[$i]['subsections'] = array(); $j = 1;
	$arrStructure[$i]['subsections'][$j] = array( 'name1' => 'Change Password', 'name2' => 'Badili Password', 'url' => 'billingChangepassword.php', 'width' => '', 'height' => '');
	$j++;
	$arrStructure[$i]['subsections'][$j] = array( 'name1' => 'Login History', 'name2' => 'Historia ya Kulogin', 'url' => 'billingLoginHistory.php', 'width' => '', 'height' => '');
	$j++;
	$i++;
	$arrStructure[$i] = array( 'name1' => 'Sign Out', 'name2' => 'Funga Program', 'url' => '../signout.php', 'image' => '',  'width' => '', 'height' => '');
    $i++;
?>
