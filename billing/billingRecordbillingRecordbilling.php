<?php
#get connected to the database and verify current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('admissionMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Financial Reports';
	$szSubSection = 'Record Payment';
	$szTitle = 'Record Candidate Payment Receipts';
	include('admissionheader.php');

	#populate Payment Category Combo Box
	$query_paytype = "SELECT Id, Description FROM paytype ORDER BY Id ASC";
	$paytype = mysql_query($query_paytype, $zalongwa) or die(mysql_error());
	$row_paytype = mysql_fetch_assoc($paytype);
	$totalRows_paytype = mysql_num_rows($paytype);
	
	#populate Candidate Combo Box
	$query_std = "SELECT  Name, RegNo FROM student ORDER BY RegNo ASC";
	$std = mysql_query($query_std, $zalongwa) or die(mysql_error());
	$row_std = mysql_fetch_assoc($std);
	$totalRows_std = mysql_num_rows($std);
	
	#populate academic year combo box
	mysql_select_db($database_zalongwa, $zalongwa);
	$query_Ayear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
	$Ayear = mysql_query($query_Ayear, $zalongwa) or die(mysql_error());
	$row_Ayear = mysql_fetch_assoc($Ayear);
	$totalRows_Ayear = mysql_num_rows($Ayear);

function add($f){
  global $errorindicator,$errorclass,$Javascript;
  $tocheck=explode(',',','.$_POST['required']);
  preg_match('/id="(.*?)"/i',$f,$i);
  preg_match('/name="(.*?)"/i',$f,$n);
  preg_match('/type="(.*?)"/i',$f,$t);
  preg_match('/value="(.*?)"/i',$f,$iv);
  $n=$n[1];$iv=$iv[1];$i=str_replace('_',' ',$i[1]);
  if(preg_match('/<textarea/',$f)){
    $v=$_POST[$n]==''?$i:$_POST[$n];
    $f=preg_replace('/<textarea(.*?)>(.*?)<\/textarea>/',
    '<textarea\\1>'.stripslashes(htmlentities($v)).'</textarea>',$f);
        if($Javascript){$f=preg_replace('/<textarea/',
        '<textarea onfocus="this.value=this.value==\''.
        $i.'\'?\'\':this.value"',$f);}
  }  
  if(@preg_match('/<select/',$f)){
    @preg_match('/<select.*?>
<style type="text/css">
<!--
.style4 {color: #CCCCCC}
-->
</style>
/',$f,$st);
    @preg_match_all('/<option value="(.*?)">(.*?)<\/option>/',$f,$allopt);
    foreach ($allopt[0] as $k=>$a){
      if($_POST[$n]==$allopt[1][$k] || 
      ($_POST[$n]=='' && $k==0)){
        $preg='/<option value="';
        $preg.=$allopt[1][$k].'">'.$allopt[2][$k].
        '<\/option>/si';
        $rep='<option selected="selected" value="';
        $rep.=$allopt[1][$k].'">'.$allopt[2][$k].
        '</option>';
        $f=preg_replace($preg,$rep,$f);
      }
    }
  }else{
    switch ($t[1]){
      case 'text':
        $v=$_POST[$n]==''?'value="'.$i.'"':'value="'.
        stripslashes(htmlentities($_POST[$n])).'"';
        $f=preg_replace('/<input/','<input '.$v,$f);
        if($Javascript){$f=preg_replace('/<input/',
        '<input onfocus="this.value=this.value==\''
        .$i.'\'?\'\':this.value"',$f);}
      break;
    }
  }
  $f.='<input type="hidden" name="'.$n.'initvalue" value="'.$i.'" />';
  if (array_search($n,$tocheck) and ($_POST[$n]=='' or $_POST[$n]==$i)){
      if($errorindicator!=''){$f=$errorindicator.$f;}
      if($errorclass!=''){$f=preg_replace('/name=/i','class="'.
      $errorclass.'" name=',$f);}
  }
  return $f;
}

// check the form
function check(){
  if ($_POST!=''){
    $sentarray=array();
    $tocheck=explode(',',$_POST['required']);
    $error[0]="Errors:";
    foreach ($tocheck as $t){if(!array_key_exists($t,$_POST)){
    $error[]=$t;}}
    foreach (array_keys($_POST) as $p){
      if(!preg_match('/initvalue/',$p) and 
      !preg_match('/required/',$p)){
        $sentarray[$p]=$_POST[$p]==
        $_POST[$p.'initvalue']?'':$_POST[$p];
      }
      foreach ($tocheck as $c){
        if ($p==$c and $sentarray[$p]==''){
          $error[]=$_POST[$p.'initvalue'];
        }
      }
    }
    return $error[1]==''?$sentarray:$error;  
    }
}
?>

<?PHP
//use the function
  $errorindicator='<img src="images/delete.gif" width="14" height="14" 
  alt="Alert" title="Indicator for missing form element" border="0" />';
  $errorclass="error";
  $Javascript=true;
  $results=check();
  if($results[0]=='Errors:'){
    
?>
  <h3>There has been an error:</h3>
  <p>You forgot to enter the following field(s)</p>
  <ul>
  <?PHP foreach ($results as $i=>$e){if ($i>0){
    echo "<li>$e</li>";
  }}
  @$errored=1;
  $regnos = $_POST['regno'];
  ?>
  </ul>
<?PHP 
  }else{
  // Add send functionality here
		$regno = addslashes($_POST['regno']);
		$category = addslashes($_POST['category']);
		$amount = addslashes($_POST['amount']);
		$receiptno = addslashes($_POST['receiptno']);
		$rpDate = addslashes($_POST['rpDate']);
		$comment = addslashes($_POST['comments']);
		$method = addslashes($_POST['type']);
		$ayear = addslashes($_POST['AYear']);
		$curr = addslashes($_POST['currency']);
		#check if receiptno exist
		if(!empty($regno)){
			$sql = "SELECT Receipt FROM studentpayments WHERE Receipt = '$receiptno'";
			$result = mysql_query($sql);
			$num_row = mysql_num_rows($result);
			if ($num_row > 0) 
			{
				echo "Can't Save Records, ZALONGWA Imegundua Kuwa,<br> Receipt Number Hii, $receiptno, Ishatumika Tayari";
				echo "<br> Tafadhari Chagua Nyingine!<hr><br>";
				#insert paymentform
				include'includes/paymentform.php';
			}elseif(intval($category)==0)
			{
				echo "Can't Save Records, <br> Please Select Category! <hr>";
				#insert paymentform
				include'includes/paymentform.php';
			}elseif(strlen($curr)<3)
			{
				echo "Can't Save Records, <br> Please Select Receipt Currency! <hr>";
				#insert paymentform
				include'includes/paymentform.php';
			}else
			{
				$sql="INSERT INTO studentpayments(RegNo, Amount, Category, Method, Receipt, ReceiptDate, AYear, Comment, User, Received, currency) 
										VALUES('$regno','$amount','$category', '$method', '$receiptno','$rpDate', '$ayear', '$comment', '$username',now(), '$curr')";   
				$result = mysql_query($sql) or die("Kuna matatizo fulani, Jaribu baadaye" . mysql_error());
			}
		}
  }  
?>
<?php

//Search Candidate
if (isset($_POST["candidate"]))
{
	$key=trim($_POST["candidate"]);
	$query_candidate = "SELECT student.Name, student.RegNo
							  FROM student 
							  WHERE (student.RegNo ='$key')";
	$candidate = mysql_query($query_candidate, $zalongwa) or die(mysql_error());
	$row_candidate = mysql_fetch_assoc($candidate);
	$totalRows_candidate = mysql_num_rows($candidate);
	//display the form if candidate is found
	if((@$totalRows_candidate>0)||($errored==1))
	{ 
		#insert paymentform
		include'includes/paymentform.php';
	}
		//display privious refund report
		$qrefunded = "SELECT * FROM studentpayments WHERE (RegNo = '$key') Order By AYear Desc";
		$refunded = mysql_query($qrefunded);
		$num_row_refunded = mysql_num_rows($refunded);
		if ($num_row_refunded > 0) 
		{
			?>
			<br>Candidate - <?php echo $key ?> -  has Previously Paid the Following:
			<table border='1'cellpadding='0' cellspacing='0' bordercolor='#006600'>
			<tr>
				<td><strong>S/No. </strong></td>
				<td><strong>Year</strong></td>
				<td><strong> Amount </strong></td>
				<td><strong>PayType</strong></td>
				<td><strong>Recorded</strong></td>
				<td><strong>Recorder</strong></td>
				<td><strong>Comments</strong></td>
			</tr>
			<?php 
			$i=1;
			while($row_refunded = mysql_fetch_assoc($refunded)) 
			{
				//search payment category
				$pay=$row_refunded['Category'];
				$qpay="select Description from paytype where Id='$pay'";
				$dbpay=mysql_query($qpay);
				$row_pay=mysql_fetch_assoc($dbpay);
				//print student report
				?>
				<tr>
					<td> <?php echo $i ?> </td>
					<td> <?php echo $row_refunded['AYear'] ?> </td>
					<td> <div align="right"><?php echo number_format($row_refunded['Amount'],2,'.',',') ?> </div></td>
					<td> <?php echo $row_pay['Description'] ?></td>
					<td> <?php echo $row_refunded['Received'] ?></td>
					<td> <?php echo $row_refunded['User'] ?></td>
					<td> <?php echo $row_refunded['Comment'] ?></td>
				</tr>
				<?php 
				$subtotal = $subtotal+$row_refunded['Amount'];
				$i=$i+1;
			}
		?> 
		<tr>
			<td colspan="5"><strong>Grand Total:</strong></td>
			<td colspan="2"><div align="right"><strong><?php echo number_format($subtotal,2,'.',',') ?></strong></div></td>
		</tr>
		</table> 
		<?php
		}
	//}
 }else{
  ?>
  <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" name="search" id="search">
						<fieldset>
						 <legend>Search Candidate </legend>
						 <table width="200" border="0" align="right" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
			  <tr>
				<td nowrap><div align="right"><strong>Enter RegNo:</strong></div></td>
				<td><input name="candidate" type="text" id = "candidate" value="" size = "25" maxlength="20"></td>
				<td><input type="submit" name="Submit" value="GO"></td>
			  </tr>
			</table>
			</fieldset>
			</form>
	<?php
  }
	# include the footer
	include('../footer/footer.php');
?>