<?php 
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('admissionMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Accommodation';
	$szSubSection = 'Allocation Form';
	$szTitle = 'Room Allocation Form';
	include('admissionheader.php');
	$today = date("Y-m-d");

//control form display
mysql_select_db($database_zalongwa, $zalongwa);
$query_AcademicYear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$AcademicYear = mysql_query($query_AcademicYear, $zalongwa) or die(mysql_error());
$row_AcademicYear = mysql_fetch_assoc($AcademicYear);
$totalRows_AcademicYear = mysql_num_rows($AcademicYear);

mysql_select_db($database_zalongwa, $zalongwa);
$query_Hostel = "SELECT HID, HName FROM hostel";
$Hostel = mysql_query($query_Hostel, $zalongwa) or die(mysql_error());
$row_Hostel = mysql_fetch_assoc($Hostel);
$totalRows_Hostel = mysql_num_rows($Hostel);

?>

<?php
if (isset($_POST["MM_search"]) && $_POST["MM_search"] == 'room'){
	//get the posted values
	$ayear=addslashes($_POST['ayear']);
	$hall = addslashes($_POST['Hall']);
	$block = addslashes($_POST['Block']);
	
	//create array of all rooms from this hostel
	$qroom = "SELECT HID, RNumber, Capacity FROM room WHERE HID='$hall' AND BID='$block'";
	$dbroom = mysql_query($qroom);
	$roomcount = mysql_num_rows($dbroom);

	if($roomcount>0){
	//print report
	$qhall = "select HName from hostel where HID='$hall'";
	$dbhall = mysql_query($qhall);
	$row_hall = mysql_fetch_assoc($dbhall);
	$hallname = $row_hall['HName'];
	echo"$ayear Vacant Beds Report for Hostel '$hallname'";
		?>
<table width="200" border="1" cellspacing="0" cellpadding="0">
						  <tr>
							<td>S/No</td>
							<td>Hostel/Hall</td>
							<td nowrap>Room Number</td>
							<td>Capacity</td>
							<td>Vacants</td>
						  </tr>
		<?php
		$i=0;
				//compare allocated students and room capacity
		while($row_room=mysql_fetch_array($dbroom)){
				$room = $row_room['RNumber'];
				$capacity = intval($row_room['Capacity']);
				$qstudent = "SELECT RegNo FROM allocation WHERE HID='$hall' AND RNumber='$room' AND AYear='$ayear' AND CheckOut>'$today'";
				$dbstudent=mysql_query($qstudent);
				$totalstudent=mysql_num_rows($dbstudent);
				$vacant = $capacity - $totalstudent;
				if($vacant>0){
				//increment the serial #
				$i=$i+1;
				?>
                  <tr>
                    <td><?=$i?></td>
                    <td nowrap><?=$hallname?></td>
                    <? echo "<td nowrap><a href=\"housingroomAllocationform.php?id=$room&hostel=$hall&ayear=$ayear\">$room</a></td>";?> 
                    <td><div align="center"><?=$capacity?></div> </td>
                    <td><div align="center"><?=$vacant?></div></td>
                  </tr>
				<?php
				}
		}
		?>
</table>
		<?
	}
}
elseif(isset($_POST['save'])){
//get posted values
$hall = addslashes($_POST['hostel']);
$room = addslashes($_POST['room']);
$regno = addslashes($_POST['regno']);

$ayear = addslashes($_POST['ayear']);
$checkin = addslashes($_POST['checkin']);
$checkout = addslashes($_POST['checkout']);

if ($checkout==''){
echo 'Please Enter Checkout Date';
exit;
}
if ($checkin==''){
echo 'Please Enter Checkin Date';
exit;
}

if ($checkin>$checkout){
	$error = urlencode("Checkout Date is Older than Checkin Date!");
	$location="housingroomAllocationform.php?id=$room&hostel=$hall&ayear=$ayear&error=$error&reg=$regno";
	echo '<meta http-equiv="refresh" content="0; url='.$location.'">';
	}

if ($regno==''){
echo 'Please Enter RegNo !';
exit;
}

//validate this regno
$qregno =  "select Name, RegNo from student where RegNo='$regno'";
$dbregno=mysql_query($qregno);
if(mysql_num_rows($dbregno)>0){
//check if the room is empty
		$qroom = "SELECT HID, RNumber, Capacity FROM room WHERE HID='$hall' AND RNumber='$room'";
		$dbroom = mysql_query($qroom);
		$row_room=mysql_fetch_array($dbroom);
		$room = $row_room['RNumber'];
		$capacity = intval($row_room['Capacity']);
		$qstudent = "SELECT RegNo FROM allocation WHERE HID='$hall' AND RNumber='$room' AND AYear='$ayear' AND CheckOut>'$today'";
		$dbstudent=mysql_query($qstudent);
		$totalstudent=mysql_num_rows($dbstudent);
		$vacant = $capacity - $totalstudent;
		if($vacant>0){
			//save entries
			$qsave = "INSERT INTO allocation(HID,RNumber,RegNo,AYear,CheckOut,CheckIn) VALUES('$hall','$room','$regno','$ayear','$checkout','$checkin')";
			mysql_query($qsave);
			
			if(mysql_error()){
				$error = urlencode("Sorry! The candidate with RegNo. <b>$regno</b> has been allocated a room already");
				$location = "housingroomAllocationform.php?id=$room&hostel=$hall&ayear=$ayear&error=$error";
				echo '<meta http-equiv="refresh" content="0; url='.$location.'">';
				exit;
				}
		}
//display room list
	//create array of all rooms from this hostel
	$qroom = "SELECT HID, RNumber, Capacity FROM room WHERE HID='$hall'";
	$dbroom = mysql_query($qroom);
	$roomcount = mysql_num_rows($dbroom);

	if($roomcount>0){
	//print report
	$qhall = "select HName from hostel where HID='$hall'";
	$dbhall = mysql_query($qhall);
	$row_hall = mysql_fetch_assoc($dbhall);
	$hallname = $row_hall['HName'];
	echo"$ayear Vacant Beds Report for Hostel '$hallname'";
		?>
	<table width="200" border="1" cellspacing="0" cellpadding="0">
						  <tr>
							<td>S/No</td>
							<td>Hostel/Hall</td>
							<td nowrap>Room Number</td>
							<td>Capacity</td>
							<td>Vacants</td>
						  </tr>
		<?php
		$i=0;
				//compare allocated students and room capacity
		while($row_room=mysql_fetch_array($dbroom)){
				$room = $row_room['RNumber'];
				$capacity = intval($row_room['Capacity']);
				$qstudent = "SELECT RegNo FROM allocation WHERE HID='$hall' AND RNumber='$room' AND AYear='$ayear' AND CheckOut>'$today'";
				$dbstudent=mysql_query($qstudent);
				$totalstudent=mysql_num_rows($dbstudent);
				$vacant = $capacity - $totalstudent;
				if($vacant>0){
				//increment the serial #
				$i=$i+1;
				?>
                  <tr>
                    <td><?=$i?></td>
                    <td nowrap><?=$hallname?></td>
                    <? echo "<td nowrap><a href=\"housingroomAllocationform.php?id=$room&hostel=$hall&ayear=$ayear\">$room</a></td>";?> 
                    <td><div align="center"><?=$capacity?></div> </td>
                    <td><div align="center"><?=$vacant?></div></td>
                  </tr>
				<?php
				}
		}
	}
		?>
</table>
<?
}
else{
	$error = urlencode("Sorry, Registration Number $regno does not exist");
	$location="housingroomAllocationform.php?id=$room&hostel=$hall&ayear=$ayear&error=$error&reg=$regno";
	echo '<meta http-equiv="refresh" content="0; url='.$location.'">';
	}
}
elseif(isset($_GET['id'])){
//received get values
$room = addslashes($_GET['id']);
$hall = addslashes($_GET['hostel']);
$ayear = addslashes($_GET['ayear']);

//create a room allocation form
?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" enctype="multipart/form-data" name="save" target="_self">
Room Allocation:  <?=$ayear?>
<table width="200" border="1" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
  <tr>
    <td><strong>Hostel:</strong></td>
    <td>
		<input name="hostel" type="hidden" value="<?=$hall?>" required>
    <?php
		$qhall = "select HName from hostel where HID='$hall'";
		$dbhall = mysql_query($qhall);
		$row_hall = mysql_fetch_assoc($dbhall);
		echo $row_hall['HName'];
	?>
		<input name="ayear" type="hidden" value="<?=$ayear?>" required>
	</td>
  </tr>
  <tr>
    <td><strong>Room:</strong></td>
    <td>
		<input name="room" type="hidden" value="<?=$room?>" required><?=$room?>
    </td>
  </tr>
  <tr>
    <td><strong>RegNo:</strong></td>
    <td><input name="regno" type="text" maxlength="20" value="<?php echo $_GET['reg'];?>" required></td>
  </tr>
  <tr>
    <td><strong>CheckIN:</strong></td>
	<!-- A Separate Layer for the Calendar -->
	<script language="JavaScript" src="datepicker/Calendar1-901.js" type="text/javascript"></script>
    <td><input name="checkin" type="text" maxlength="20" value=<?php echo $today ?> required></td>
	<td><input type="button" class="button" name="dtDate_button" value="Calendar" onClick="show_calendar('save.checkin', '','','YYYY-MM-DD', 'POPUP','AllowWeekends=Yes;Nav=No;SmartNav=Yes;PopupX=300;PopupY=300;')"></td>
  </tr>
  <tr>
    <td><strong>CheckOUT:</strong></td>
    <td>
		<input name="checkout" type="text" maxlength="20" value="<?php $date = date('Y-m-d', strtotime("+6 months")); echo $date?>" required>
	</td>
	<td><input type="button" class="button" name="dtDate_button" value="Calendar" onClick="show_calendar('save.checkout', '','','YYYY-MM-DD', 'POPUP','AllowWeekends=Yes;Nav=No;SmartNav=Yes;PopupX=300;PopupY=300;')"></td>

  </tr>
  <tr>
    <td></td>
    <td><input name="save" type="submit" value="Update"></td>
  </tr>
</table>
</form>

<?php
	if(isset($_GET['error'])){
	 $error = urldecode($_GET['error']);
	 echo "<p style='color:maroon'>$error</p>";
	 }
}
else{
?>                
            <fieldset bgcolor="#CCCCCC">
				<legend>Search Vacant Rooms</legend>
			<table width="284" border="0" bgcolor="#CCCCCC">
	<?php
		if(!isset($_GET['Hostel'])){
	?>
		<form action="<?=$_SERVER['PHP_SELF']?>" method="GET" name="housingvacantRoom" id="housingvacantRoom">
        <tr>
          <td nowrap><div align="right">Academic Year: </div></td>
          <td bgcolor="#CCCCCC">
			  <select name="ayear" id="select2" required>
				<option value="">SelectAcademicYear</option>
            <?php
do {  
?>
            <option value="<?php echo $row_AcademicYear['AYear']?>"><?php echo $row_AcademicYear['AYear']?></option>
            <?php
} while ($row_AcademicYear = mysql_fetch_assoc($AcademicYear));
  $rows = mysql_num_rows($AcademicYear);
  if($rows > 0) {
      mysql_data_seek($AcademicYear, 0);
	  $row_AcademicYear = mysql_fetch_assoc($AcademicYear);
  }
?>
          </select></td>
        </tr>
        <tr>
          <td nowrap><div align="right"> Hall/Hostel:</div></td>
          <td bgcolor="#CCCCCC">
			  <select name="Hall" id="select" required>
				<option value="">Select Student Hostel</option>
            <?php
do {  
?>
            <option value="<?php echo $row_Hostel['HID']?>"><?php echo $row_Hostel['HName']?></option>
              <?php
} while ($row_Hostel = mysql_fetch_assoc($Hostel));
  $rows = mysql_num_rows($Hostel);
  if($rows > 0) {
      mysql_data_seek($Hostel, 0);
	  $row_Hostel = mysql_fetch_assoc($Hostel);
  }
?>
          </select></td>
        </tr>
        <tr>
          <td colspan="2" nowrap><div align="center">
            <input type="submit" name="Hostel" value="Search Blocks">
          </div></td>
        </tr>
    <?php
		}
		if(isset($_GET['Hostel'])){
			$hostel = $_GET['Hall'];
			$year = $_GET['ayear'];
    ?>
		<form action="<?=$_SERVER['PHP_SELF']?>" method="POST" name="housingvacantRoom" id="housingvacantRoom">
        <tr>
          <td nowrap><div align="right"> Block:</div></td>
          <td bgcolor="#CCCCCC">
			  <select name="Block" id="select" required>
				<option value="">Select Student Block</option>
            <?php
				$getblock = mysql_query("SELECT Id, BName FROM block WHERE HID='$hostel'");
				while(list($blockID, $block) = mysql_fetch_array($getblock)){
					echo "<option value='$block'>$block</option>";
					}
            ?>
			  </select>
			  <input type="hidden" name="ayear" value="<?php echo $year;?>" />
			  <input type="hidden" name="Hall" value="<?php echo $hostel;?>" />
          </td>
        </tr>
        <tr>
          <td colspan="2" nowrap><div align="center">
            <input type="submit" name="Submit" value="Search Rooms">
          </div></td>
          </tr>
      
                    <input type="hidden" name="MM_search" value="room">
<?php }
	echo " 	</form>
			</table>
			</fieldset>";
}
# include the footer
	include('../footer/footer.php');
?>
