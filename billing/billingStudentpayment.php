<?php 
#start pdf
if (isset($_POST['PrintPDF']) && ($_POST['PrintPDF'] == "Print PDF")) {
	#get post variables
	$rawkey = addslashes(trim($_POST['key']));
	$key = ereg_replace("[[:space:]]+", " ",$rawkey);
	#get content table raw height
	$rh= addslashes(trim($_POST['sex']));
	$temp= addslashes(trim($_POST['temp']));
	$award= addslashes(trim($_POST['award']));
	$realcopy= addslashes(trim($_POST['real']));
	
	#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
	require_once('../Connections/zalongwa.php');
	# check if is a trial print
	if($realcopy==1){
		$copycount = 'TRIAL COPY';
	}
	#check if is a reprint
	$qtranscounter = "SELECT RegNo, received FROM transcriptcount where RegNo='$key'";
	$dbtranscounter = mysql_query($qtranscounter);
	@$transcounter = mysql_num_rows($dbtranscounter);
	
	if ($transcounter>0){
		$row_transcounter = mysql_fetch_array($dbtranscounter);
		$lastprinted = $row_result['received'];
	}
	#Get Organisation Name
	$qorg = "SELECT * FROM organisation";
	$dborg = mysql_query($qorg);
	$row_org = mysql_fetch_assoc($dborg);
	$pname = $row_org['ParentName'];
	$org = $row_org['Name'];
	$post = $row_org['Address'];
	$phone = $row_org['tel'];
	$fax = $row_org['fax'];
	$email = $row_org['email'];
	$website = $row_org['website'];
	$city = $row_org['city'];

	include('includes/PDF.php');

	$i=0;
	$pg=1;
	$tpg =$pg;

	$qstudent = "SELECT * from student WHERE regno = '$key'";
	$dbstudent = mysql_query($qstudent); 
	$row_result = mysql_fetch_array($dbstudent);
		$sname = $row_result['Name'];
		$regno = $row_result['RegNo'];
		$degree = $row_result['ProgrammeofStudy'];
		$sex = $row_result['Sex'];
		$dbirth = $row_result['DBirth'];
		$entry = $row_result['EntryYear'];
		$faculty = $row_result['Faculty'];
		$citizen = $row_result['Nationality'];
		$address = $row_result['Address'];
		$gradyear = $row_result['GradYear'];
		$admincriteria = $row_result['MannerofEntry'];
		$campus = $row_result['Campus'];
		$faculty = $row_result['Faculty'];
		$subjectid = $row_result['Subject'];
		$photo = $row_result['Photo'];
		$checkit = strlen($photo);
		
		if ($checkit > 8){
		
		$imgfile = '../admission/'.$photo;
		#resize photo
			$full_url = $photo;
			$imageInfo = @getimagesize($imgfile);
			$src_width = $imageInfo[0];
			$src_height = $imageInfo[1];
			
			$dest_width = 80;//$src_width / $divide;
			$dest_height = 80;//$src_height / $divide;
			
			$src_img = @imagecreatefromjpeg($imgfile);
			$dst_img = imagecreatetruecolor($dest_width,$dest_height);
			@imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $dest_width, $dest_height, $src_width, $src_height);
			@imagejpeg($dst_img,$full_url);
			@imagedestroy($src_img);
		#new resized image file
		$imgfile = $full_url;
		#NB: ili hii ifanye kazi lazima images folder kwenye academic liwe writable!!!
		
		}else{
		$nophoto = 1;
		}
		#get degree name
		$qdegree = "Select Title FROM programme WHERE ProgrammeCode = '$degree'";
		$dbdegree = mysql_query($qdegree);
		$row_degree = mysql_fetch_array($dbdegree);
		$programme = $row_degree['Title'];
		
		#get subject combination
		$qsubjectcomb = "SELECT SubjectName FROM subjectcombination WHERE SubjectID='$subjectid'";
		$dbsubjectcom = mysql_query($qsubjectcomb);
		$row_subjectcom = mysql_fetch_assoc($dbsubjectcom);
		$counter = mysql_num_rows($dbsubjectcom );
		if ($counter>0){
		$subject = $row_subjectcom['SubjectName'];
		}

	//require 'PDF.php';                    // Require the lib. 
	$pdf = &PDF::factory('p', 'a4');      // Set up the pdf object. 
	$pdf->open();                         // Start the document. 
	$pdf->setCompression(true);           // Activate compression. 
	$pdf->addPage();  
	
	#print header
	if($temp ==1){
	#include transcript address
	include '../academic/includes/transtemplate.php';
	}
	
	$ytitle = $yadd+72;
	$pdf->setFillColor('rgb', 1, 0, 0);   
	$pdf->setFont('Arial', '', 13);     
	$pdf->text(150, $ytitle, 'TRANSCRIPT OF PAYMENTS TRANSACTIONS'); 
	$pdf->setFillColor('rgb', 0, 0, 0);    

	#title line
	$pdf->line(50, $ytitle+3, 570, $ytitle+3);

	$pdf->setFont('Arial', 'B', 10.3);     
	#set page header content fonts
	#line1
	$pdf->line(50, $ytitle+3, 50, $ytitle+15);       
	$pdf->line(383, $ytitle+3, 383, $ytitle+15);       
	$pdf->line(432, $ytitle+3, 432, $ytitle+15);
	$pdf->line(570, $ytitle+3, 570, $ytitle+15);       
	$pdf->line(50, $ytitle+15, 570, $ytitle+15); 
	#format name
	$candname = explode(",",$sname);
	$surname = $candname[0];
	$othername = $candname[1];

	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(50, $ytitle+13, 'NAME:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(90, $ytitle+13, strtoupper($surname).', '.ucwords(strtolower($othername))); 
	$pdf->setFont('Arial', 'B', 10.3); 	$pdf->text(385, $ytitle+13, 'SEX:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(415, $ytitle+13, $sex); 
	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(435, $ytitle+13, 'RegNo.:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(480, $ytitle+13, $regno); 
	
	#line2
	$pdf->line(50, $ytitle+15, 50, $ytitle+27);       
	$pdf->line(188, $ytitle+15, 188, $ytitle+27);       
	$pdf->line(570, $ytitle+15, 570, $ytitle+27);       
	$pdf->line(50, $ytitle+27, 570, $ytitle+27); 
	
	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(50, $ytitle+25, 'CITIZENSHIP:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(118, $ytitle+25, $citizen); 
	$pdf->setFont('Arial', 'B', 10.3); 	$pdf->text(190, $ytitle+25, 'ADDRESS:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(250, $ytitle+25, $address); 
	#line3
	$pdf->line(50, $ytitle+27, 50, $ytitle+39);       
	$pdf->line(188, $ytitle+27, 188, $ytitle+39);       
	$pdf->line(383, $ytitle+27, 383, $ytitle+39);       
	$pdf->line(570, $ytitle+27, 570, $ytitle+39);       
	$pdf->line(50, $ytitle+39, 570, $ytitle+39); 
	
	#Format grad year
	$graddate = explode("-",$gradyear);
	$gradday = $graddate[2];
	$gradmon = $graddate[1];
	$grady = $graddate[0];

	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(50, $ytitle+37, 'BIRTH DATE:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(120, $ytitle+37, $dbirth); 
	$pdf->setFont('Arial', 'B', 10.3); 	$pdf->text(190, $ytitle+37, 'ADMITTED:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(250, $ytitle+37, $entry); 
	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(385, $ytitle+37, 'COMPLETED:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(456, $ytitle+37, $gradday.' - '.$gradmon.' - '.$grady); 
/*
	#line4
	$pdf->line(50, $ytitle+39, 50, $ytitle+51);       
	$pdf->line(570, $ytitle+39, 570, $ytitle+51);       
	$pdf->line(50, $ytitle+51, 570, $ytitle+51); 
	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(50, $ytitle+49, 'ADMITTED ON THE BASIS OF:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(205, $ytitle+49, $admincriteria); 
	*/
	#line5
	$pdf->line(50, $ytitle+39, 50, $ytitle+51);       
	$pdf->line(238, $ytitle+39, 238, $ytitle+51);       
	$pdf->line(570, $ytitle+39, 570, $ytitle+51);       
	$pdf->line(50, $ytitle+51, 570, $ytitle+51); 

	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(50, $ytitle+49, 'CAMPUS:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(100, $ytitle+49, $campus); 
	$pdf->setFont('Arial', 'B', 10.3); 	$pdf->text(240, $ytitle+49, 'FACULTY:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(290, $ytitle+49, $faculty); 

	#line6
	$pdf->line(50, $ytitle+51, 50, $ytitle+63);       
	$pdf->line(570, $ytitle+51, 570, $ytitle+63);       
	$pdf->line(50, $ytitle+63, 570, $ytitle+63); 
	$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(50, $ytitle+61, 'NAME OF PROGRAMME:'); $pdf->setFont('Arial', 'I', 10.3); $pdf->text(175, $ytitle+61, $programme); 

	$sub =$subjectid;
	if($sub<>0){
		#line7
		$pdf->line(50, $ytitle+75, 50, $ytitle+87);       
		$pdf->line(570, $ytitle+75, 570, $ytitle+87);       
		$pdf->line(50, $ytitle+87, 570, $ytitle+87); 
		$pdf->setFont('Arial', 'B', 10.3);  $pdf->text(50, $ytitle+85, 'MAJOR STUDY AREA:'); $pdf->text(175, $ytitle+85,$subject); 
	}
	#initialize x and y
	$x=50;
	$y=$ytitle+83;
	
	$subtotal=0;
	$grandtotal=0;
	
	$yval=$y+33;
	$y=$y+33;

	#set page body content fonts
	$pdf->setFont('Arial', '', 9.5);    

	//query academeic year
	$qayear = "SELECT DISTINCT AYear from studentpayments WHERE RegNo = '$regno' ORDER BY AYear ASC";
	$dbayear = mysql_query($qayear);
	#initialise ayear
	$examyear = 0;
	//query exam results sorted per years
	while($rowayear = mysql_fetch_object($dbayear))
	{
		$examyear = $examyear +1;
		$currentyear = $rowayear->AYear;
		$query_examresult = "
							  SELECT * FROM studentpayments
							  WHERE (RegNo='$regno') AND 
									  (AYear = '$currentyear') 
							  ORDER BY AYear DESC";	
		$result = mysql_query($query_examresult); 
		$query = @mysql_query($query_examresult);
		$dbcourseUnit = mysql_query($query_examresult);
						
		if (mysql_num_rows($query) > 0)
		{
		
			$subtotal=0;
			#check if u need to sart a new page
			$blank=$y-12;
			$space = 820.89 - $blank;
			if ($space<150)
			{
				#start new page
				$pdf->addPage();  
			
				$x=50;
				$yadd=50;
	
				$y=80;
				$pg=$pg+1;
				$tpg =$pg;
			}
			#create table header
			if($examyear==1){
				$pdf->text($x, $y-$rh, 'FIRST YEAR PAYMENTS REPORT: '.$rowayear->AYear); 
			}elseif($examyear==2){
				$pdf->text($x, $y-$rh, 'SECOND YEAR PAYMENTS REPORT: '.$rowayear->AYear); 
			}elseif($examyear==3){
				$pdf->text($x, $y-$rh, 'THIRD YEAR PAYMENTS REPORT: '.$rowayear->AYear); 
			}elseif($examyear==4){
				$pdf->text($x, $y-$rh, 'FOURTH YEAR PAYMENTS REPORT: '.$rowayear->AYear); 
			}elseif($examyear==5){
				$pdf->text($x, $y-$rh, 'FIFTH YEAR PAYMENTS REPORT: '.$rowayear->AYear); 
			}elseif($examyear==6){
				$pdf->text($x, $y-$rh, 'SIXTH YEAR PAYMENTS REPORT: '.$rowayear->AYear); 
			}elseif($examyear==7){
				$pdf->text($x, $y-$rh, 'SEVENTH YEAR PAYMENTS REPORT: '.$rowayear->AYear); 
			}
			$pdf->setFont('Arial', 'B', 8.5);     
			$pdf->text($x+10, $y, 'Date'); 
			$pdf->text($x+100, $y, 'Payment Category'); 
			$pdf->text($x+250, $y, 'Receipt No'); 
			$pdf->text($x+350, $y, 'Method'); 
			$pdf->text($x+450, $y, 'Amount'); 
			$pdf->setFont('Arial', '', 8.5);  		
			#calculate results
			$i=1;
			while($row_course = mysql_fetch_array($dbcourseUnit))
			{
				$rdate= $row_course['receiptdate'];
				$amount = $row_course['amount'];
				$method = $row_course['method'];
				$receiptno = $row_course['receipt'];
				$pay=$row_course['feecode'];
				$curr=$row_course['currency'];
				$sn=$sn+1;
				
				#search payment category
				$qpay="select name from fees where feecode='$pay'";
				$dbpay=mysql_query($qpay);
				$row_pay=mysql_fetch_assoc($dbpay);
				$category = $row_pay['name'];
				
				#search for paymethod
				$qpaymethod="select name from fees where id='$method'";
				$dbpaymethod=mysql_query($qpaymethod);
				$row_paymethod=mysql_fetch_assoc($dbpaymethod);
				$method = $row_paymethod['name'];
				
				#sum-up amount
				$subtotal = $subtotal+$amount;
				#print results
				$pdf->text($x+10, $y+$rh, $rdate); 
				$pdf->text($x+100, $y+$rh, $category ); 
				$pdf->text($x+250, $y+$rh, $receiptno); 
				$pdf->text($x+350, $y+$rh, $curr); 
				$pdf->text($x+450, $y+$rh, number_format($amount,2,'.',',')); 
				#check if the page is full
				$x=$x;
				#draw a line
				$pdf->line($x, $y-$rh+2, 570.28, $y-$rh+2);        
				$pdf->line($x, $y-$rh+2, $x, $y);       
				$pdf->line(570.28, $y-$rh+2, 570.28, $y);      
				$pdf->line($x, $y-$rh+2, $x, $y+$rh+4);              
				$pdf->line(570.28, $y-$rh+2, 570.28, $y+$rh+4);      
				//$pdf->line($x+498, $y-$rh+2, $x+498, $y+$rh+4);       
				$pdf->line($x+448, $y-$rh+2, $x+448, $y+$rh+4);     
				$pdf->line($x+348, $y-$rh+2, $x+348, $y+$rh+4);       
				$pdf->line($x+248, $y-$rh+2, $x+248, $y+$rh+4);       
				$pdf->line($x+98, $y-$rh+2, $x+98, $y+$rh+2); 
				#get space for next year
				$y=$y+$rh;

				if ($y>800)
				{
					#put page header
					$pdf->addPage();  
		
					$x=50;
					$yadd=50;
	
					$y=80;
					$pg=$pg+1;
					$tpg =$pg;
					#title line
					$pdf->line(50, 135, 570, 135);
					#set page body content fonts
					$pdf->setFont('Arial', '', 9.5);    
				}
				#draw a line
				$pdf->line($x, $y+$rh+2, 570.28, $y+$rh+2);       
				$pdf->line($x, $y-$rh+2, $x, $y+$rh+2); 
				$pdf->line(570.28, $y-$rh+2, 570.28, $y+$rh+2);      
				//$pdf->line($x+498, $y-$rh+2, $x+498, $y+$rh+2);       
				$pdf->line($x+448, $y-$rh+2, $x+448, $y+$rh+2);     
				$pdf->line($x+348, $y-$rh+2, $x+348, $y+$rh+2);       
				$pdf->line($x+248, $y-$rh+2, $x+248, $y+$rh+2);       
				$pdf->line($x+98, $y-$rh+2, $x+98, $y+$rh+2); 
			 }//ends while loop
			$pdf->setFont('Arial', 'BI', 9.5);     
			$pdf->text($x+100, $y+$rh+1, 'Sub-total');
			$pdf->text($x+413, $y+$rh+1, ''); 
			$pdf->text($x+450, $y+$rh+1, number_format($subtotal,2,'.',',')); 
			$pdf->text($x+504, $y+$rh+1, ''); 
			$pdf->setFont('Arial', '', 9.5); 
			#compute grand total
			$grandtotal = $grandtotal + $subtotal;
				
			#check x,y values
			$y=$y+3.5*$rh;
			if ($y==800)
			{
				$pdf->addPage();  
				$x=50;
				$yadd=50;

				$y=80;
				$pg=$pg+1;
				$tpg =$pg;
				#title line
				$pdf->line(50, 135, 570, 135);
				#set page body content fonts
				$pdf->setFont('Arial', '', 9.5);    
			}
		 }
	 }
		#print grandtotal
		$pdf->text($x+250, $y+$rh+1, 'Grand-Total: '); 			
		$pdf->text($x+450, $y+$rh+1, number_format($grandtotal,2,'.',',')); 	
		
		#get officer fullname
		$qname="SELECT FullName FROM security WHERE UserName='$username'";
		$dbname=mysql_query($qname);
		$row_name=mysql_fetch_assoc($dbname);
		$ofname=$row_name['FullName'];
		#see if we have a space to rint signature
		$b=$y+17;
		if ($b<820.89)
		{
			#print signature lines
			$pdf->text($x+190, $y+47, '.................................................');    						
			$pdf->text($x+200, $y+57, strtoupper($ofname));  
		} 						
		$pdf->setFont('Arial', 'I', 8);     
		$pdf->text(50, 820.89, 'Dar es Salaam, '.$today = date("d-m-Y H:i:s"));   

		#print the key index
		$pdf->setFont('Arial', 'I', 9); 
		$yind = $y+87;
		
		#check if there is enough printing area
		$indarea = 820.89-$yind;
		if ($indarea< 203)
		{
			$pdf->addPage();  

			$x=50;
			$y=80;
			$pg=$pg+1;
			$tpg =$pg;
			$pdf->setFont('Arial', 'I', 8);     
			$pdf->text(530.28, 820.89, 'Page '.$pg);  
			$pdf->text(50, 820.89, 'Dar es Salaam, '.$today = date("d-m-Y H:i:s")); 
			$yind = $y; 
    }
	#print the file
	$pdf->output($key.'.pdf');              // Output the 
}/*ends is isset*/
#ends pdf

#get connected to the database and verfy current session
require_once('../Connections/sessioncontrol.php');
require_once('../Connections/zalongwa.php');
include('billingMenu.php');

# include the header
global $szSection, $szSubSection;
$szSection = 'Financial Reports';
$szSubSection = 'Receipts';
$szTitle = 'Candidates Payments Transcript';
include('billingheader.php');

mysql_select_db($database_zalongwa, $zalongwa);
$query_campus = "SELECT FacultyName FROM faculty WHERE FacultyID='$userFaculty' ORDER BY FacultyName ASC";
$campus = mysql_query($query_campus, $zalongwa) or die(mysql_error());
$row_campus = mysql_fetch_assoc($campus);
$totalRows_campus = mysql_num_rows($campus);

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) 
  {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) 
{
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($_POST['search']) && ($_POST['search'] == "PreView")) 
{
	#get post variables
	$rawkey = $_POST['key'];
	$key = ereg_replace("[[:space:]]+", " ",$rawkey);
	
	#select student
	$qstudent = "SELECT * from student WHERE RegNo = '$key'";
	$dbstudent = mysql_query($qstudent) or die("Mwanafunzi huyu hana matokeo".  mysql_error()); 
	$row_result = mysql_fetch_array($dbstudent);
	$name = $row_result['Name'];
	$regno = $row_result['RegNo'];
	$degree = $row_result['ProgrammeofStudy'];
	$RegNo = $regno;
	
	#get degree name
	$qdegree = "Select Title from programme where ProgrammeCode = '$degree'";
	$dbdegree = mysql_query($qdegree);
	$row_degree = mysql_fetch_array($dbdegree);
	$programme = $row_degree['Title'];
	
	echo  "$name - $regno <br> $programme";	

	#display privious refund report
	$qrefunded = "SELECT * FROM studentpayments WHERE (RegNo = '$key') Order By AYear Desc";
	$refunded = mysql_query($qrefunded);
	$num_row_refunded = mysql_num_rows($refunded);
	if ($num_row_refunded > 0) 
	{
		?>

		<br>This Candidate has Previously Paid the Following:
		<table border='1'cellpadding='0' cellspacing='0' bordercolor='#006600'>
		<tr>
			<td><strong>S/No. </strong></td>
			<td><strong>Year</strong></td>
			<td><strong>Amount</strong></td>
			<td><strong>PayType</strong></td>
			<td><strong>Recorded</strong></td>
			<td><strong>Recorder</strong></td>
			<td><strong>Comments</strong></td>
		</tr>
		<?php 
		$i=1;
		while($row_refunded = mysql_fetch_assoc($refunded)) 
		{
			//search payment category
			$pay=$row_refunded['feecode'];
			$qpay="select name from fees where feecode='$pay'";
			$dbpay=mysql_query($qpay);
			$row_pay=mysql_fetch_assoc($dbpay);
			//print student report
			?>
			<tr>
				<td> <?php echo $i ?> </td>
				<td> <?php echo $row_refunded['aYear'] ?> </td>
				<td> <div align="right"><?php echo number_format($row_refunded['amount'],2,'.',',') ?> </div></td>
				<td> <?php echo $row_pay['name'] ?></td>
				<td> <?php echo $row_refunded['received'] ?></td>
				<td> <?php echo $row_refunded['user'] ?></td>
				<td> <?php echo $row_refunded['comment'] ?></td>
			</tr>
			<?php 
			$subtotal = $subtotal+$row_refunded['amount'];
			$i=$i+1;
			}
		?> 
		<tr>
			<td colspan="5"><strong>Grand Total:</strong></td>
			<td colspan="2"><div align="right"><strong><?php echo number_format($subtotal,2,'.',',') ?></strong></div></td>
		</tr>
		</table> 
		<?php
	}else
	{ 
			echo "Sorry, No Records Found for '$reg[$c]'<br><hr>";
	}
	mysql_close($zalongwa);
}else
{
	?>
	<form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" name="searchform" id="searchform">
        <table width="284" border="1" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
        <tr>
          <td colspan="9" nowrap><div align="center"></div></td>
        </tr>
        <tr>
          <td width="110"><div align="right"><strong></strong></div></td>
          <td colspan="8" bordercolor="#ECE9D8" bgcolor="#CCCCCC"><span class="style67"><input name="cmbInst" type="hidden" id="cmbInst" value="<?php echo $row_campus['FacultyName']?>"></span></td>
        </tr>
        <tr>
          <td nowrap><div align="right"><strong>Programme:</strong></div><div align="center"></div></td>
          <td colspan="2" nowrap><div align="right">Degree</div></td>
          <td width="31" nowrap><input type="radio" value="1" id="award1" name="award" checked></td>
          <td colspan="2" nowrap><div align="right">Diploma</div></td>
          <td width="31" nowrap><input type="radio" value="2" id="award2" name="award" ></td>
          <td width="89" nowrap><div align="right">Certificate</div></td>
		  <td width="30" nowrap><input type="radio" value="3" id="award2" name="award" ></td>
        </tr>
		<tr> 
			<td align="right"><strong>Category:</strong></td> 
			<td colspan="4">Finalist:<input type="radio" value="1" id="cat" name="cat" checked>  </td> 
			<td colspan="4">Continuing:<input type="radio" value="2" id="cat" name="cat"></td>
		</tr> 
		<tr>
          <td><div align="right"><strong><span class="style67">RegNo:</span></strong></div></td>
          <td colspan="8" bordercolor="#ECE9D8" bgcolor="#CCCCCC"><span class="style67"><input name="key" type="text" id="key" size="40" maxlength="40"></span></td>
        </tr>
		<tr> 
			<td align="right" nowrap><strong>Table:</strong></td> 
			<td width="35"><div align="center">11<input type="radio" value="11" id="sex" name="sex"></div></td> 
			<td width="35"><div align="center">12<input type="radio" value="12" id="sex" name="sex" checked></div></td> 
			<td width="35"><div align="center">13<input type="radio" value="13" id="sex" name="sex" ></div></td> 
			<td width="35"><div align="center">14<input type="radio" value="14" id="sex" name="sex" ></div></td> 
			<td width="35"><div align="center">15<input type="radio" value="15" id="sex" name="sex" ></div></td> 
			<td width="35"><div align="center">16<input type="radio" value="16" id="sex" name="sex" ></div></td> 
			<td width="35"> <div align="center">-</div></td> 
			<td><div align="left">17<input type="radio" value="17" id="sex" name="sex" ></div></td>
		</tr>
        <tr>
          <td nowrap><div align="right"><strong> </strong></div></td>
          <td bgcolor="#CCCCCC" colspan="4"><div align="left"><input type="submit" name="search" value="PreView"></div></td>
          <td bgcolor="#CCCCCC" colspan="4"><div align="right"><input name="PrintPDF" type="submit" id="PrintPDF" value="Print PDF"></div></td>
        </tr>
	  </table>
	</form>
	<?php
}
include('../footer/footer.php');
?>
