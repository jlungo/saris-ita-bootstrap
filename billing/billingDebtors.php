<?php 
#start pdf
if (isset($_POST['PDF']) && ($_POST['PDF'] == "Print PDF")){
	#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
	require_once('../Connections/zalongwa.php');

	#Get Organisation Name
	$qorg = "SELECT * FROM organisation";
	$dborg = mysql_query($qorg);
	$row_org = mysql_fetch_assoc($dborg);
	$org = $row_org['Name'];

	@$checkdegree = addslashes($_POST['checkdegree']);
	@$checkyear = addslashes($_POST['checkyear']);
	@$checkcomb = addslashes($_POST['checkcomb']);
	@$checkdept = addslashes($_POST['checkdept']);
	@$checkcohot = addslashes($_POST['checkcohot']);
	@$checkfinalyear = addslashes($_POST['checkfinalyear']);
	@$subjectcombi = addslashes($_POST['combination']);
	
	if($checkcomb=='on'){
		$query_comb = "SELECT SubjectName FROM subjectcombination WHERE SubjectID='$subjectcombi'";
		$comb = mysql_query($query_comb, $zalongwa) or die(mysql_error());
		$row_comb = mysql_fetch_assoc($comb);
		$combiname = $row_comb['SubjectName'];
	}

	$prog=$_POST['degree'];
	$cohotyear = $_POST['cohot'];
	$ayear = $_POST['ayear'];
	$qprog= "SELECT ProgrammeCode, Title FROM programme WHERE ProgrammeCode='$prog'";
	$dbprog = mysql_query($qprog);
	$row_prog = mysql_fetch_array($dbprog);
	if($checkcomb=='on'){
		$progname = $row_prog['Title'].' - '.strtoupper($combiname);
	}else{
		$progname = $row_prog['Title'];
	}
		
	//calculate year of study
	$entry = intval(substr($cohotyear,0,4));
	$current = intval(substr($ayear,0,4));
	$yearofstudy=$current-$entry;
	
	if($yearofstudy==0){
		$class="FIRST YEAR";
		}elseif($yearofstudy==1){
		$class="SECOND YEAR";
		}elseif($yearofstudy==2){
		$class="THIRD YEAR";
		}elseif($yearofstudy==3){
		$class="FOURTH YEAR";
		}elseif($yearofstudy==4){
		$class="FIFTH YEAR";
		}elseif($yearofstudy==5){
		$class="SIXTH YEAR";
		}elseif($yearofstudy==6){
		$class="SEVENTHND YEAR";
		}else{
		$class="";
	}
	
	if (($checkdegree=='on') && ($checkyear == 'on') && ($checkcohot == 'on')){
		$deg=addslashes($_POST['degree']);
		$year = addslashes($_POST['ayear']);
		$cohot = addslashes($_POST['cohot']);
		$dept = addslashes($_POST['dept']);
		$comb = addslashes($_POST['checkcomb']);
				
		#determine total number of columns
		if($comb=='on'){
		$qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') AND (Subject='$subjectcombi')";
		}else{
		$qstd = "SELECT RegNo FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot')";
		}
		$dbstd = mysql_query($qstd);
		$totalcolms = 0;
		$combination = 0;
		$major = 0;
		$arrStructure = array();
		while($rowstd = mysql_fetch_array($dbstd)) {
			$stdregno = $rowstd['RegNo'];
			$qstdcourse = "SELECT DISTINCT CourseCode FROM examresult WHERE RegNo='$stdregno' and AYear='$year'";
			$dbstdcourse = mysql_query($qstdcourse);
			while($rowstdcourse = mysql_fetch_array($dbstdcourse)){
				$stdcoursecode = $rowstdcourse['CourseCode'];
				$qmajorsubject = "SELECT DISTINCT combination FROM coursecombination WHERE coursecode = '$stdcoursecode'";
				$dbmojorsubject = mysql_query($qmajorsubject);
				@$rowmajorsubject = mysql_fetch_assoc($dbmojorsubject);
				$major = $rowmajorsubject['combination'];
				
				#get subject major name
				$qmajor = "SELECT SubjectName FROM majorsubject WHERE SubjectID='$major'";
				$dbmajor = mysql_query($qmajor);
				$dbmajorcount = mysql_query($qmajor);
				$row_mahor = mysql_fetch_assoc($dbmajor);
				$majorsubject = $row_mahor['SubjectName'];
				$majorcount = mysql_num_rows($dbmajorcount);
				if ($majorcount>0){
					#save major subject to tempo table
					#check if coursecode exist
					$sql ="SELECT SubjectID FROM tempmajorsubject WHERE (SubjectID  = '$major')";
					$result = mysql_query($sql);
					$coursecodeFound = mysql_num_rows($result);
					if (!$coursecodeFound) {
						$insertSQL = "INSERT INTO tempmajorsubject (SubjectID, SubjectName) VALUES ('$major', '$majorsubject')";
						mysql_select_db($database_zalongwa, $zalongwa);
					   $Result1 = mysql_query($insertSQL, $zalongwa);
					 }
				}
			}
		}
		#start pdf
		include('includes/PDF.php');
		$pdf = &PDF::factory('l', 'a4');      
		$pdf->open();                         
		$pdf->setCompression(true);           
		$pdf->addPage();  
		$pdf->setFont('Arial', 'I', 8);     
		$pdf->text(50, 580.28, 'Printed On '.$today = date("d-m-Y H:i:s"));  		
 
		#put page header
		$x=60;
		$y=50;
		$i=1;
		$pg=1;
		$pdf->text(800.89,580.28, 'Page '.$pg);   
		
		#count unregistered
		$j=0;
		#count sex
		$fmcount = 0;
		$mcount = 0;
		$fcount = 0;
		
		#print header
		$pdf->setFont('Arial', 'B', 32);    
		$pdf->setFillColor('rgb', 0, 0, 0); 
		$pdf->text($x+130, $y, '');   
		$y=$y+20;
		$pdf->setFont('Arial', 'B', 15);    
		$pdf->text($x+166, $y, strtoupper($org));   
		$pdf->setFillColor('rgb', 0, 0, 0);   
		
		$pdf->setFillColor('rgb', 0, 0, 0);    
		$pdf->setFont('Arial', '', 15);      
		$pdf->text($x+213, $y+15, 'PROVISIONAL EXAMINATIONS RESULTS');
		$pdf->text($x+250, $y+30, 'ACADEMIC YEAR: '.$year); 
		$pdf->text($x+6, $y+48, $class.' - '.strtoupper($progname)); 
		#reset values of x,y
		$x=50; $y=$y+54;
		
		#query student list
		if($comb=='on'){
		$qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') AND (Subject='$subjectcombi') ORDER BY Subject, Name";
		}else{
		$qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY Subject, Name";
		}
		$dbstudent = mysql_query($qstudent);
		$totalstudent = mysql_num_rows($dbstudent);
		$i=1;
		while($rowstudent = mysql_fetch_array($dbstudent)) {
			$name = $rowstudent['Name'];
			$origname = explode(",",$name);
			$surname = $origname[0];
			$othername = $origname[1];
			$regno = $rowstudent['RegNo'];
			$sex = $rowstudent['Sex'];
			$gpoints = 0;
			$gunits = 0;
			#Make a List of Major Subjects
			$qmajors ="SELECT SubjectID, SubjectName FROM tempmajorsubject ORDER BY SubjectID";
			$dbmajors = mysql_query($qmajors);
			$totalmajors = mysql_num_rows($dbmajors);
			$dbmajorheader = mysql_query($qmajors);
		#=============================
		if ($i==1){
			#set table header
			$pdf->setFont('Arial', '', 8); 
			$pdf->line($x, $y, 825.89, $y); 
			$pdf->line($x, $y+30, 825.89, $y+30); 
			$pdf->line($x, $y, $x, $y+30); 			$pdf->text($x+2, $y+12, 'S/No');
			$pdf->line($x+25, $y, $x+25, $y+30);	$pdf->text($x+27, $y+12, 'Name');
			$pdf->line($x+86, $y, $x+86, $y+30);	$pdf->text($x+90, $y+12, 'RegNo');
			$pdf->line($x+146, $y, $x+146, $y+30);	$pdf->text($x+150, $y+12, 'Sex');
			$pdf->setLineWidth(1.5); 
			$pdf->line($x+170, $y, $x+170, $y+30);	
			$pdf->setLineWidth(1); 
			$cw = 60;
			//$cw = 60*$totalcolms;
			$x=$x+170;
			while($rowa_majorheader = mysql_fetch_assoc($dbmajorheader)){
				$majorsubject = $rowa_majorheader['SubjectName'];
				$pdf->text($x+2, $y+12, $majorsubject); 
				$pdf->setLineWidth(1.5); 
				$pdf->line($x+$cw, $y, $x+$cw, $y+30);
				$pdf->setLineWidth(1); 
				$pdf->line($x, $y+15, $x+$cw, $y+15); 
				$pdf->text($x+3, $y+24, 'TU'); 
				$pdf->line($x+$cw, $y+15, $x+$cw, $y+30);
				$pdf->text($x+22, $y+24, 'SGP'); 
				$pdf->line($x+20, $y+15, $x+20, $y+30);
				$pdf->text($x+42, $y+24, 'GPA'); 
				$pdf->line($x+41, $y+15, $x+41, $y+30);
				$x=$x+$cw;
			}
			$cw = 0;
			#compute science gpa
				$pdf->line($x, $y+15, $x+$cw+95, $y+15);
				$pdf->line($x+$cw, $y+15, $x+$cw, $y+30);	$pdf->text($x+$cw+1, $y+24, 'TU');
				$pdf->line($x+$cw+25, $y+15, $x+$cw+25, $y+30);	$pdf->text($x+$cw+27, $y+24, 'SGP'); 
				$pdf->line($x+$cw+50, $y+15, $x+$cw+50, $y+30);	$pdf->text($x+$cw+20, $y+12, 'SCIENCE'); $pdf->text($x+$cw+57, $y+24, 'GPA'); 
				$pdf->setLineWidth(1.5); 
				$pdf->line($x+$cw+80, $y, $x+$cw+80, $y+30);	
				$pdf->setLineWidth(1); 
            $cw = 80;
			if ($checkfinalyear<>'on'){
				$pdf->line($x, $y+15, $x+$cw+85, $y+15);
				$pdf->line($x+$cw, $y+15, $x+$cw, $y+30);	$pdf->text($x+$cw+1, $y+24, 'TU');
				$pdf->line($x+$cw+25, $y+15, $x+$cw+25, $y+30);	$pdf->text($x+$cw+27, $y+24, 'SGP'); 
				$pdf->line($x+$cw+50, $y+15, $x+$cw+50, $y+30);	$pdf->text($x+$cw+20, $y+12, 'TOTAL'); $pdf->text($x+$cw+57, $y+24, 'GPA'); 
				$pdf->line($x+$cw+85, $y, $x+$cw+85, $y+30);	
			}
			#check if is final year
			if ($checkfinalyear=='on'){
				#count number of years
				$x = $x+$cw+85;
				$qyears = "select distinct AYear from examresult where RegNo='$regno' order by AYear";
				$dbyears = mysql_query($qyears);
				$yr = 1;
				while ($row_years = mysql_fetch_assoc($dbyears)){
						$x = $x+2;
						$pdf->text($x, $y+12, $yr.'-YR');
						$x = $x+29;
						$pdf->line($x, $y, $x, $y+30);	
						$yr=$yr+1;
					}
			}
			$pdf->line($x+$cw+85, $y, $x+$cw+85, $y+15);	$pdf->text($x+$cw+87, $y+12, 'Remarks');
			$pdf->line(825.89, $y, 825.89, $y+30);  
			$y=$y+30;
		}#end setting table header
		#================			
					$qcourse="SELECT DISTINCT examresult.CourseCode FROM 
								examresult
									 WHERE examresult.RegNo='$regno' AND examresult.AYear='$year'";	
					$dbcourse = mysql_query($qcourse);
					$dbcourseUnit = mysql_query($qcourse);
					$total_rows = mysql_num_rows($dbcourse);
		
					#initialise
					$totalunit=0;
					$unittaken=0;
					$sgp=0;
					$totalsgp=0;
					$gpa=0;
					$scunits=0;
					$scsgp=0;
					$scgpa=0;
					$key = $regno; 
					$x=50;
					
					#print student info
					$pdf->setFont('Arial', '', 8); 
					$pdf->line($x, $y, 825.89, $y); 
					$pdf->line($x, $y+45, 825.89, $y+45); 
					$pdf->line($x, $y, $x, $y+45); 			$pdf->text($x+2, $y+26, $i);
					$pdf->line($x+25, $y, $x+25, $y+45);	 
					$pdf->text($x+27, $y+12, strtoupper($surname).','); 
					$pdf->text($x+27, $y+26, $othername); 
					$pdf->line($x+86, $y, $x+86, $y+45);	$pdf->text($x+90, $y+26, strtoupper($regno)); 
					$pdf->line($x+146, $y, $x+146, $y+45);	$pdf->text($x+150, $y+26, strtoupper($sex));
					$pdf->setLineWidth(1.5); 
					$pdf->line($x+170, $y, $x+170, $y+45);	
					$pdf->setLineWidth(1); 
					$x=$x+170;
					$cw = 60;
					#start the real action
					$majorsub = 0;
					while($row_major = mysql_fetch_array($dbmajors)){
						$majorunittaken=0;
						$majortotalsgp=0;
						$majorsub= $row_major['SubjectID'];
						#get courselist for this major
						$qcourselist = "SELECT coursecode FROM coursecombination WHERE combination='$majorsub'";
						$dbcourselist = mysql_query($qcourselist);
						while($row_course = mysql_fetch_assoc($dbcourselist)){
							$course = $row_course['coursecode'];
							#get course unit
							$unit = 0;
							$cunit = "SELECT Units FROM course WHERE coursecode='$course'";
							$dbunit=mysql_query($cunit);
							$row_unit=mysql_fetch_array($dbunit);
							$unit = $row_unit['Units'];
							$sn=$sn+1;
							$remarks = 'remarks';
							$grade='';
							
							#get course results
								@$updateSQL = "SELECT 
											   examresult.RegNo,
											   examresult.ExamNo,
											   examresult.CourseCode,
											   examresult.Grade,
											   examresult.AYear,
											   examresult.Remarks,
											   examresult.Count
								FROM examresult
								WHERE coursecode='$course' and RegNo = '$regno' AND examresult.AYear='$year'";
								
								$result = mysql_query($updateSQL) or die("Mwanafunzi huyu hana matokeo" . mysql_error());
								$query = @mysql_query($updateSQL) or die("Cannot query the database.<br>" . mysql_error());
										$total_rows = mysql_num_rows($query);
									while($result = mysql_fetch_array($query)) {
										$ayear = $result['AYear'];
										$remarks = $result['Remarks'];
										$grade = $result['Grade'];
										$count = $result['Count'];
										if ($grade==''){
											$igrade='I';
											$grade='I';
											}
										
										# check if this is optional course or not
										if ($count == 1){
											$sgp = 0;
											$unit = 0;
										}else{
										if ($grade=='A'){
												$point=5;
												$sgp=$point*$unit;
												$totalsgp=$totalsgp+$sgp;
												$unittaken=$unittaken+$unit;
												$majorunittaken=$majorunittaken+$unit;
												$majortotalsgp=$majortotalsgp+$sgp;
											}elseif($grade=='B+'){
												$point=4;
												$sgp=$point*$unit;
												$totalsgp=$totalsgp+$sgp;
												$unittaken=$unittaken+$unit;
												$majorunittaken=$majorunittaken+$unit;
												$majortotalsgp=$majortotalsgp+$sgp;
											}elseif($grade=='B'){
												$point=3;
												$sgp=$point*$unit;
												$totalsgp=$totalsgp+$sgp;
												$unittaken=$unittaken+$unit;
												$majorunittaken=$majorunittaken+$unit;
												$majortotalsgp=$majortotalsgp+$sgp;
											}elseif($grade=='C'){
												$point=2;
												$sgp=$point*$unit;
												$totalsgp=$totalsgp+$sgp;
												$unittaken=$unittaken+$unit;
												$majorunittaken=$majorunittaken+$unit;
												$majortotalsgp=$majortotalsgp+$sgp;
											}elseif($grade=='D'){
												$point=1;
												$sgp=$point*$unit;
												$totalsgp=$totalsgp+$sgp;
												$unittaken=$unittaken+$unit;
												$majorunittaken=$majorunittaken+$unit;
												$majortotalsgp=$majortotalsgp+$sgp;
												$supp='!';
											}elseif($grade=='E'){
												$point=0;
												$sgp=$point*$unit;
												$totalsgp=$totalsgp+$sgp;
												$unittaken=$unittaken+$unit;
												$majorunittaken=$majorunittaken+$unit;
												$majortotalsgp=$majortotalsgp+$sgp;
												$supp='!';
											}else
												$sgp='';
										}
								$coursecode = $result['CourseCode'];
							  }
							}
							$mgpa = @substr($majortotalsgp/$majorunittaken, 0,3);
							$pdf->setLineWidth(1.5); 
							$pdf->line($x+$cw, $y, $x+$cw, $y+15);
							$pdf->setLineWidth(1); 
							if($majorunittaken>0){
							$pdf->text($x+2, $y+24, $majorunittaken); 
							}
							$pdf->setLineWidth(1.5); 
							$pdf->line($x+$cw, $y, $x+$cw, $y+45);
							$pdf->setLineWidth(1); 
							if($majortotalsgp>0){
							$pdf->text($x+21, $y+24, $majortotalsgp);
							} 
							$pdf->line($x+20, $y, $x+20, $y+45);
							$pdf->text($x+42, $y+24, $mgpa); 
							//$pdf->setLineWidth(2); 
							$pdf->line($x+41, $y, $x+41, $y+45);
							//$pdf->setLineWidth(1); 
						   #compute gpa
								if ($mgpa  >= 4.4){
									$mgpagrade='A';
									}
								elseif ($mgpa >= 3.5){
									$mgpagrade= 'B+';
									}
								elseif ($mgpa >= 2.7){
									$mgpagrade= 'B';
									}
								elseif ($mgpa >= 2.0){
									$mgpagrade= 'C';
									}
								elseif ($mgpa >= 1.0){
									$mgpagrade= 'D';
									}
								else{
									$mgpagrade= 'E';
									}
							if($mgpa>0){
							$pdf->text($x+42, $y+40, '['.$mgpagrade.']'); 
							}
							#end gpa
							include'includes/checkdomain.php';
							if ($domain == 'Science'){
								#add to science gpa
								$scunits = $scunits + $majorunittaken;
								$scsgp = $scsgp + $majortotalsgp;
								$scgpa = @substr($scsgp/$scunits, 0,3);
								if ($scgpa  >= 4.4){
									$scgpagrade='A';
									}
								elseif ($scgpa >= 3.5){
									$scgpagrade= 'B+';
									}
								elseif ($scgpa >= 2.7){
									$scgpagrade= 'B';
									}
								elseif ($scgpa >= 2.0){
									$scgpagrade= 'C';
									}
								elseif ($scgpa >= 1.0){
									$scgpagrade= 'D';
									}
								else{
									$scgpagrade= 'E';
									}
							}
						$x=$x+$cw;
						$clmspace = $clmspace+20;	
						}#end while row_course
						$gunits = $unittaken +$gunits;
						$gpoints = $totalsgp + $gpoints;
						$gpa = @substr($totalsgp/$unittaken, 0,3);
						$cw = 0;
						#compute science gpa
								$pdf->line($x+$cw, $y, $x+$cw, $y+45);	$pdf->text($x+$cw+1, $y+24, $scunits); //if (($igrade<>'I')&&($supp<>'!')){$pdf->text($x+$cw+1, $y+24, $unittaken); }
								$pdf->line($x+$cw+25, $y, $x+$cw+25, $y+45);	$pdf->text($x+$cw+27, $y+24, $scsgp); //if (($igrade<>'I')&&($supp<>'!')){$pdf->text($x+$cw+27, $y+24, $totalsgp); }
								$pdf->line($x+$cw+50, $y, $x+$cw+50, $y+45);	$pdf->text($x+$cw+57, $y+24, $scgpa); //if (($igrade<>'I')&&($supp<>'!')){$pdf->text($x+$cw+67, $y+24, $gpa); }
								if($scgpa>0){
								$pdf->text($x+$cw+57, $y+40, '['.$scgpagrade.']'); 
								}
                        $cw = 80;
						if ($checkfinalyear<>'on'){
								$pdf->setLineWidth(1.5); 
								$pdf->line($x+$cw, $y, $x+$cw, $y+45);	
								$pdf->setLineWidth(1); 
								$pdf->text($x+$cw+1, $y+24, $unittaken); //if (($igrade<>'I')&&($supp<>'!')){$pdf->text($x+$cw+1, $y+24, $unittaken); }
								$pdf->line($x+$cw+25, $y, $x+$cw+25, $y+45);	$pdf->text($x+$cw+27, $y+24, $totalsgp); //if (($igrade<>'I')&&($supp<>'!')){$pdf->text($x+$cw+27, $y+24, $totalsgp); }
								$pdf->line($x+$cw+50, $y, $x+$cw+50, $y+45);	$pdf->text($x+$cw+57, $y+24, $gpa); //if (($igrade<>'I')&&($supp<>'!')){$pdf->text($x+$cw+67, $y+24, $gpa); }
								
								#get student remarks
								$qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$regno'";
								$dbremarks = mysql_query($qremarks);
								@$row_remarks = mysql_fetch_assoc($dbremarks);
								@$totalremarks = mysql_num_rows($dbremarks);
								$studentremarks = $row_remarks['Remark'];
								$gpagrade= '';
								if(($totalremarks>0)&&($studentremarks<>'')){
									$remark = $studentremarks;
										if ($gpa  >= 4.4){
											$remark = 'Pass';
											$gpagrade='A';
											$class = 'F';
											}
										elseif ($gpa >= 3.5){
											$remark = 'Pass';
											$gpagrade= 'B+';
											$class = 'U';
											}
										elseif ($gpa >= 2.7){
											$remark = 'Pass';
											$gpagrade= 'B';
											$class = 'L';
											}
										elseif ($gpa >= 2.0){
											$remark = 'Pass';
											$gpagrade= 'C';
											$class = 'P';
											}
										elseif ($gpa >= 1.0){
											$remark = 'Fail';
											$gpagrade= 'D';
											}
										else{
											$remark = 'Fail';
											$gpagrade= 'E';
											}
									$pdf->line($x+$cw+50, $y, $x+$cw+50, $y+45);	$pdf->text($x+$cw+57, $y+40, $gpagrade); 
								}else{
								
										if ($gpa  >= 4.4){
											$remark = 'Pass';
											$gpagrade='A';
											$class = 'F';
											}
										elseif ($gpa >= 3.5){
											$remark = 'Pass';
											$gpagrade= 'B+';
											$class = 'U';
											}
										elseif ($gpa >= 2.7){
											$remark = 'Pass';
											$gpagrade= 'B';
											$class = 'L';
											}
										elseif ($gpa >= 2.0){
											$remark = 'Pass';
											$gpagrade= 'C';
											$class = 'P';
											}
										elseif ($gpa >= 1.0){
											$remark = 'Fail';
											$gpagrade= 'D';
											}
										else{
											$remark = 'Fail';
											$gpagrade= 'E';
											}
										}
									$pdf->line($x+$cw+50, $y, $x+$cw+50, $y+45);	$pdf->text($x+$cw+57, $y+40, $gpagrade); 
									//if (($igrade<>'I')&&($supp<>'!')){$pdf->text($x+$cw+67, $y+40, $gpagrade); }
									$pdf->line($x+$cw+50, $y+28, $x+$cw+85, $y+28);
								}
								#check if is final year
								if ($checkfinalyear=='on'){
									#count number of years
									$x = $x+$cw+85;
									$qyears = "select distinct AYear from examresult where RegNo='$regno' order by AYear";
									$dbyears = mysql_query($qyears);
									$yr = 1;
									while ($row_years = mysql_fetch_assoc($dbyears)){
											$yr=$yr+1;
											
											#calcuate GPA for this yeare
											$totalunit=0;
											$unittaken=0;
											$sgp=0;
											$totalsgp=0;
											$gpa=0;
											
											$currentyear = $row_years['AYear'];
											$query_examresult = "
												   SELECT student.Name,
												   student.ProgrammeofStudy,
												   examresult.RegNo,
												   examresult.ExamNo,
												   examresult.CourseCode,
												   course.Units,
												   course.CourseName,
												   examresult.Grade,
												   examresult.AYear,
												   examresult.Remarks,
												   examresult.SemesterID,
												   examresult.Status,
												   examresult.Count
											FROM examresult
											   INNER JOIN course ON (examresult.CourseCode = course.CourseCode)
											   INNER JOIN student ON (examresult.RegNo = student.RegNo)
											WHERE (examresult.RegNo = '$regno') AND (examresult.AYear =  '$currentyear')";
											$result = mysql_query($query_examresult) or die("Mwanafunzi huyu hana matokeo".  mysql_error()); 
													$query = @mysql_query($query_examresult) or die("Cannot query the database.<br>" . mysql_error());
													$row_result = mysql_fetch_array($result);
													$name = $row_result['Name'];
													$degree = $row_result['ProgrammeofStudy'];
													$regno = $row_result['RegNo'];
													
													while($result = mysql_fetch_array($query)) {
														$unit = $result['Units'];
														$totalunit=$totalunit+$unit;
														$regno = $result['RegNo'];
														$examno = $result['ExamNo'];
														$ayear = $result['AYear'];
														$cname = $result['CourseName'];
														$remarks = $result['Remarks'];
														$status = $result['Status'];
														$grade = $result['Grade'];
														$count = $result['Count'];
														
														# check if this is optional course or not
														if ($count == 1){
															$sgp = 0;
														}else{
														
															if ($grade=='A'){
																$point=5;
																$sgp=$point*$unit;
																$totalsgp=$totalsgp+$sgp;
																$unittaken=$unittaken+$unit;
															}elseif($grade=='B+'){
																$point=4;
																$sgp=$point*$unit;
																$totalsgp=$totalsgp+$sgp;
																$unittaken=$unittaken+$unit;
															}elseif($grade=='B'){
																$point=3;
																$sgp=$point*$unit;
																$totalsgp=$totalsgp+$sgp;
																$unittaken=$unittaken+$unit;
															}elseif($grade=='C'){
																$point=2;
																$sgp=$point*$unit;
																$totalsgp=$totalsgp+$sgp;
																$unittaken=$unittaken+$unit;
															}elseif($grade=='D'){
																$point=1;
																$sgp=$point*$unit;
																$totalsgp=$totalsgp+$sgp;
																$unittaken=$unittaken+$unit;
															}elseif($grade=='E'){
																$point=0;
																$sgp=$point*$unit;
																$totalsgp=$totalsgp+$sgp;
																$unittaken=$unittaken+$unit;
															}else
																$sgp='';
													   }
														$coursecode = $result['CourseCode'];
												}
												$pdf->line($x, $y, $x, $y+45);	
												$x = $x+2;
												$pdf->text($x, $y+12, $totalsgp);
												$pdf->text($x+4, $y+24, '/');
												$pdf->text($x, $y+36, $unittaken);
												$x = $x+29;
												$pdf->line($x, $y, $x, $y+45);	
												
												#get values for the final GPA
												$gradtotalunits=$gradtotalunits+$unittaken;
												$grandtotalpoint=$grandtotalpoint+$totalsgp;
										}
										$grandgpa = @substr($grandtotalpoint/$gradtotalunits, 0,3);
										$pdf->line($x, $y, $x, $y+45);	
										$x = $x+2;
										$pdf->text($x, $y+12, $grandtotalpoint);
										$pdf->text($x+4, $y+24, '/');
										$pdf->text($x, $y+36, $gradtotalunits);
										$x = $x+29;
										$cw = 0;
										$pdf->line($x, $y, $x, $y+45);	
										//$pdf->line($x+$cw, $y, $x+$cw, $y+45);	$pdf->text($x+$cw+1, $y+24, $gradtotalunits); 
										//$pdf->line($x+$cw+25, $y, $x+$cw+25, $y+45);	$pdf->text($x+$cw+27, $y+24, $grandtotalpoint); 
										$pdf->line($x+$cw, $y, $x+$cw, $y+45);	$pdf->text($x+$cw+7, $y+24, $grandgpa); 
										
										#get student remarks
										$qremarks = "SELECT Remark FROM studentremark WHERE RegNo='$regno'";
										$dbremarks = mysql_query($qremarks);
										@$row_remarks = mysql_fetch_assoc($dbremarks);
										@$totalremarks = mysql_num_rows($dbremarks);
										$studentremarks = $row_remarks['Remark'];
										$gpagrade= '';
										if(($totalremarks>0)&&($studentremarks<>'')){
											$remark = $studentremarks;
										}else{
										
										if ($grandgpa  >= 4.4){
											$remark = 'PASS';
											$gpagrade='A';
											$class = 'F';
											}
										elseif ($grandgpa >= 3.5){
											$remark = 'PASS';
											$gpagrade= 'B+';
											$class = 'U';
											}
										elseif ($grandgpa >= 2.7){
											$remark = 'PASS';
											$gpagrade= 'B';
											$class = 'L';
											}
										elseif ($grandgpa >= 2.0){
											$remark = 'PASS';
											$gpagrade= 'C';
											$class = 'P';
											}
										elseif ($grandgpa >= 1.0){
											$remark = 'FAIL';
											$gpagrade= 'D';
											}
										else{
											$remark = 'FAIL';
											$gpagrade= 'E';
											}
										}
										$pdf->line($x+$cw, $y, $x+$cw, $y+45);	$pdf->text($x+$cw+7, $y+40, $gpagrade); 
										$pdf->line($x+$cw, $y+28, $x+$cw+35, $y+28);
								} #end check if is final year
						if ($checkfinalyear=='on'){
						$pdf->line($x+$cw+35, $y, $x+$cw+35, $y+45);	
							if (($igrade<>'I')&&($supp<>'!')){
								$pdf->text($x+$cw+40, $y+14, $remark);
								#print classifications
								if ($class =='F'){
									$pdf->text($x+$cw+37, $y+33, 'I');
									$pdf->setFont('Arial', '', 6); 
									//$pdf->text($x+$cw+41, $y+28, '1');
									$pdf->setFont('Arial', '', 8); 
								}elseif($class =='U'){
									$pdf->text($x+$cw+37, $y+33, 'II');
									$pdf->setFont('Arial', '', 6); 
									$pdf->text($x+$cw+42, $y+28, '1');
									$pdf->setFont('Arial', '', 8); 
								}elseif($class =='L'){
									$pdf->text($x+$cw+37, $y+33, 'II');
									$pdf->setFont('Arial', '', 6); 
									$pdf->text($x+$cw+42, $y+28, '2');
									$pdf->setFont('Arial', '', 8); 
								}
							}
						$pdf->line(825.89, $y, 825.89, $y+45); 
						#delete class value
						$class ='';
								#check failed exam
								if ($fexm=='#'){
								$pdf->text($x+$cw+37, $y+26, $fexm.' = Fail Exam'); $fexm = ''; $remark ='';
								}
								#check failed exam
								if ($fcwk=='*'){
								$pdf->text($x+$cw+37, $y+38, $fcwk.' = Fail CWK'); $fcwk = ''; $remark ='';
								}
								
								if ($supp=='!'){
									$pdf->text($x+$cw+37, $y+12, 'Supp: ');
									#get all courses where grade = D
									$qgraded = "SELECT CourseCode FROM examresult WHERE RegNo='$regno' and AYear='$year' AND Grade='D'";
									$dbgrade = mysql_query($qgraded);
									$k = $x+$cw+127;
									for ($f=1; $f<4;$f++){
										$row_graded = mysql_fetch_assoc($dbgrade);			
										//$pdf->text($k, $y+12, $row_graded['CourseCode']); $supp = ''; $remark ='';
										$k = $k+40;
									}
									$k = 0;
									#get all courses where grade = E
									$qgraded = "SELECT CourseCode FROM examresult WHERE RegNo='$regno' and AYear='$year' AND Grade='E'";
									$dbgrade = mysql_query($qgraded);
									$k = $x+$cw+127;
									for ($f=1; $f<5;$f++){
										$row_graded = mysql_fetch_assoc($dbgrade);			
										//$pdf->text($k, $y+24, $row_graded['CourseCode']); $supp = ''; $remark ='';
										$k = $k+40;
									}
									$k = 0;
								$supp = ''; $remark ='';
								}
								if ($igrade=='I'){
									$pdf->text($x+$cw+37, $y+36, 'Incomp: ');
									#get all courses where grade = I
									$k = $x+$cw+127;
									$qgraded = "SELECT CourseCode from examresult WHERE RegNo='$regno' and AYear='$year' AND Grade=''";
									$dbgrade = mysql_query($qgraded);
									for ($f=1; $f<4;$f++){
										$row_graded = mysql_fetch_assoc($dbgrade);			
										//$pdf->text($k, $y+36, $row_graded['CourseCode']); $supp = ''; $remark ='';
										$k = $k+40;
										}
										$k = 0;
								$igrade = ''; $remark ='';
								}
								if ($egrade=='*'){
								$pdf->text($x+$cw+127, $y+26, $egrade.' = C/Repeat.'); $egrade = ''; $remark ='';
								}
						}else{
								$pdf->line($x+$cw+85, $y, $x+$cw+85, $y+45);	//if (($igrade<>'I')&&($supp<>'!')){$pdf->text($x+$cw+97, $y+14, $remark);}
								if (($igrade<>'I')&&($supp<>'!')){
									$pdf->text($x+$cw+87, $y+14, $remark);
									#print classifications
									if ($class =='F'){
										$pdf->text($x+$cw+87, $y+33, 'I');
										$pdf->setFont('Arial', '', 6); 
										//$pdf->text($x+$cw+101, $y+28, '1');
										$pdf->setFont('Arial', '', 8); 
									}elseif($class =='U'){
										$pdf->text($x+$cw+87, $y+33, 'II');
										$pdf->setFont('Arial', '', 6); 
										$pdf->text($x+$cw+92, $y+28, '1');
										$pdf->setFont('Arial', '', 8); 
									}elseif($class =='L'){
										$pdf->text($x+$cw+87, $y+33, 'II');
										$pdf->setFont('Arial', '', 6); 
										$pdf->text($x+$cw+92, $y+28, '2');
										$pdf->setFont('Arial', '', 8); 
									}
								}
								$pdf->line(825.89, $y, 825.89, $y+45); 
								$class ='';
								#check student remark
								if(($totalremarks>0)&&($studentremarks<>'')){
									$pdf->text($x+$cw+87, $y+14, $studentremarks); $studentremarks = ''; 
									$remark ='';
									 $fexm = '';
									 $supp = '';
									 $igrade = '';
								}else{
									#check failed exam
									if ($fexm=='#'){
									$pdf->text($x+$cw+87, $y+26, $fexm.' = Fail Exam'); $fexm = ''; $remark ='';
									}
									#check failed exam
									if ($fcwk=='*'){
									$pdf->text($x+$cw+87, $y+38, $fcwk.' = Fail CWK'); $fcwk = ''; $remark ='';
									}
									if ($supp=='!'){
										$pdf->text($x+$cw+87, $y+12, 'Supp: ');
										#get all courses where grade = D
										$qgraded = "SELECT CourseCode FROM examresult WHERE RegNo='$regno' and AYear='$year' AND Grade='D'";
										$dbgrade = mysql_query($qgraded);
										$k = $x+$cw+127;
										for ($f=1; $f<5;$f++){
											$row_graded = mysql_fetch_assoc($dbgrade);			
											$pdf->text($k, $y+12, $row_graded['CourseCode']); $supp = ''; $remark ='';
											$k = $k+30;
										}
										$k = 0;
										#get all courses where grade = E
										$qgraded = "SELECT CourseCode FROM examresult WHERE RegNo='$regno' and AYear='$year' AND Grade='E'";
										$dbgrade = mysql_query($qgraded);
										$k = $x+$cw+127;
										for ($f=1; $f<4;$f++){
											$row_graded = mysql_fetch_assoc($dbgrade);			
											$pdf->text($k, $y+24, $row_graded['CourseCode']); $supp = ''; $remark ='';
											$k = $k+30;
										}
										$k = 0;
									$supp = ''; $remark ='';
									}
									if ($igrade=='I'){
										$pdf->text($x+$cw+87, $y+36, 'Incomp: ');
										#get all courses where grade = I
										$k = $x+$cw+127;
										$qgraded = "SELECT CourseCode from examresult WHERE RegNo='$regno' and AYear='$year' AND Grade=''";
										$dbgrade = mysql_query($qgraded);
										for ($f=1; $f<4;$f++){
											$row_graded = mysql_fetch_assoc($dbgrade);			
											$pdf->text($k, $y+36, $row_graded['CourseCode']); $supp = ''; $remark ='';
											$k = $k+30;
											}
											$k = 0;
									$igrade = ''; $remark ='';
									}
									if ($egrade=='*'){
									$pdf->text($x+$cw+127, $y+26, $egrade.' = C/Repeat.'); $egrade = ''; $remark ='';
									}
								}
							}#end if studentremarks
					  $i=$i+1;
					  $y=$y+45;
					  if($y>=530.28){
					  #start new page
						$pdf->addPage();  
						$pdf->setFont('Arial', 'I', 8);     
						$pdf->text(50, 582.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
						
						$x=50;
						$y=50;
						$pg=$pg+1;
						$pdf->text(800.89,582.28, 'Page '.$pg);   
						
						#count unregistered
						$j=0;
						#count sex
						$fmcount = 0;
						$mcount = 0;
						$fcount = 0;
						#set table header
						$pdf->setFont('Arial', '', 8); 
						$pdf->line($x, $y, 825.89, $y); 
						$pdf->line($x, $y+30, 825.89, $y+30); 
			$pdf->line($x, $y, $x, $y+30); 			$pdf->text($x+2, $y+12, 'S/No');
			$pdf->line($x+25, $y, $x+25, $y+30);	$pdf->text($x+27, $y+12, 'Name');
			$pdf->line($x+86, $y, $x+86, $y+30);	$pdf->text($x+90, $y+12, 'RegNo');
			$pdf->line($x+146, $y, $x+146, $y+30);	$pdf->text($x+150, $y+12, 'Sex');
			$pdf->setLineWidth(1.5); 
			$pdf->line($x+170, $y, $x+170, $y+30);	
			$pdf->setLineWidth(1); 
			$cw = 60;
			//$cw = 60*$totalcolms;
			$x=$x+170;
			while($rowa_majorheader = mysql_fetch_assoc($dbmajorheader)){
				$majorsubject = $rowa_majorheader['SubjectName'];
				$pdf->text($x+2, $y+12, $majorsubject); 
				$pdf->setLineWidth(1.5); 
				$pdf->line($x+$cw, $y, $x+$cw, $y+30);
				$pdf->setLineWidth(1); 
				$pdf->line($x, $y+15, $x+$cw, $y+15); 
				$pdf->text($x+3, $y+24, 'TU'); 
				$pdf->line($x+$cw, $y+15, $x+$cw, $y+30);
				$pdf->text($x+22, $y+24, 'SGP'); 
				$pdf->line($x+20, $y+15, $x+20, $y+30);
				$pdf->text($x+42, $y+24, 'GPA'); 
				$pdf->line($x+41, $y+15, $x+41, $y+30);
				$x=$x+$cw;
			}
			$cw = 0;
			#compute science gpa
				$pdf->line($x, $y+15, $x+$cw+95, $y+15);
				$pdf->line($x+$cw, $y+15, $x+$cw, $y+30);	$pdf->text($x+$cw+1, $y+24, 'TU');
				$pdf->line($x+$cw+25, $y+15, $x+$cw+25, $y+30);	$pdf->text($x+$cw+27, $y+24, 'SGP'); 
				$pdf->line($x+$cw+50, $y+15, $x+$cw+50, $y+30);	$pdf->text($x+$cw+20, $y+12, 'SCIENCE'); $pdf->text($x+$cw+57, $y+24, 'GPA'); 
				$pdf->setLineWidth(1.5); 
				$pdf->line($x+$cw+80, $y, $x+$cw+80, $y+30);	
				$pdf->setLineWidth(1); 
            $cw = 80;
			if ($checkfinalyear<>'on'){
				$pdf->line($x, $y+15, $x+$cw+85, $y+15);
				$pdf->line($x+$cw, $y+15, $x+$cw, $y+30);	$pdf->text($x+$cw+1, $y+24, 'TU');
				$pdf->line($x+$cw+25, $y+15, $x+$cw+25, $y+30);	$pdf->text($x+$cw+27, $y+24, 'SGP'); 
				$pdf->line($x+$cw+50, $y+15, $x+$cw+50, $y+30);	$pdf->text($x+$cw+20, $y+12, 'TOTAL'); $pdf->text($x+$cw+57, $y+24, 'GPA'); 
				$pdf->line($x+$cw+85, $y, $x+$cw+85, $y+30);	
			}
			#check if is final year
			if ($checkfinalyear=='on'){
				#count number of years
				$x = $x+$cw+85;
				$qyears = "select distinct AYear from examresult where RegNo='$regno' order by AYear";
				$dbyears = mysql_query($qyears);
				$yr = 1;
				while ($row_years = mysql_fetch_assoc($dbyears)){
						$x = $x+2;
						$pdf->text($x, $y+12, $yr.'-YR');
						$x = $x+29;
						$pdf->line($x, $y, $x, $y+30);	
						$yr=$yr+1;
					}
			}
			$pdf->line($x+$cw+85, $y, $x+$cw+85, $y+15);	$pdf->text($x+$cw+87, $y+12, 'Remarks');
			$pdf->line(825.89, $y, 825.89, $y+30);  
			$y=$y+30;
		}#end setting table header
	}//ends $rowstudent loop

#start new page for the keys
$space = 530.28 - $y;
$yind = $y+20; 
if($space<70){
	$pdf->addPage();  

			$x=50;
			$y=50;
			$pg=$pg+1;
			$tpg =$pg;
			$pdf->setFont('Arial', 'I', 8);     
			$pdf->text(50, 530.28, 'Printed On '.$today = date("d-m-Y H:i:s"));   
			$pg=$pg+1;
			$pdf->text(800.89,530.28, 'Page '.$pg);   
			$yind = $y; 
 }
 	$pdf->text(450, $yind, 'Signature of the Dean:  ���������     ');  
	$pdf->text(450, $yind+12, 'Date: ���������������������       ');
 	$pdf->text(450, $yind+24, 'Signature of the Chairperson of the Senate:  ����������');  
	$pdf->text(450, $yind+36, 'Date: ����������������������');  

	$pdf->setFont('Arial', 'I', 9); 
	$pdf->text(190.28, $yind, '          ######## END OF EXAM RESULTS ########');   
	$pdf->text(50, $yind + 12, '1. Key for Course Units: ONE UNIT IS EQUIVALENT TO 15 CONTACT HOURS.  '); 	
	$pdf->text(110, $yind + 24, 'POINTS = GRADE POINTS MULTIPLIED BY NUMBER OF UNITS.');  
	$pdf->text(50, $yind + 36, '2.	Key to the Grades and other Symbols for Institute Examinations: SEE THE TABLE BELOW ');
	$x=50;
	$y= $yind + 44;
	#table 1
		#draw a line
		$pdf->line($x, $y, 570.28, $y);       
		$pdf->line($x, $y+56, 570.28, $y+56); 
		$pdf->line($x, $y, $x, $y+56); 
		$pdf->line(570.28, $y, 570.28, $y+56);
		#vertical lines
		$pdf->line($x+65, $y, $x+65, $y+56);  
		$pdf->line($x+145, $y, $x+145, $y+56); 
		$pdf->line($x+225, $y, $x+225, $y+56); 
		$pdf->line($x+305, $y, $x+305, $y+56); 
		$pdf->line($x+385, $y, $x+385, $y+56); 
		$pdf->line($x+455, $y, $x+455, $y+56); 
		
		#horizontal lines
		$pdf->line($x, $y+14, 570.28, $y+14); 
		$pdf->line($x, $y+28, 570.28, $y+28);  
		$pdf->line($x, $y+42, 570.28, $y+42); 
		#row 1 text
		$pdf->text($x+2, $y+12, 'Grade   '); 
		$pdf->text($x+105, $y+12, '  A   ');
		$pdf->text($x+175, $y+12, '  B+  ');
		$pdf->text($x+265, $y+12, '  B   ');
		$pdf->text($x+345, $y+12, '  C   ');
		$pdf->text($x+410, $y+12, '  D   ');
		$pdf->text($x+480, $y+12, '  E   ');
		#row 2 text
		$pdf->text($x+2, $y+24, 'Marks  '); 
		$pdf->text($x+95, $y+24, '  70-100%   ');
		$pdf->text($x+165, $y+24, '  60-69%  ');
		$pdf->text($x+255, $y+24, '  50-59%   ');
		$pdf->text($x+335, $y+24, '  40-49%   ');		$pdf->text($x+400, $y+24, '  35-39%   ');
		$pdf->text($x+470, $y+24, '  0-34%   ');
#row 3 text
		$pdf->text($x+2, $y+37, 'Grade Points  '); 
		$pdf->text($x+105, $y+37, '  5.0   ');
		$pdf->text($x+175, $y+37, '  4.0  ');
		$pdf->text($x+275, $y+37, '  3.0   ');
		$pdf->text($x+345, $y+37, '  2.0   ');
		$pdf->text($x+410, $y+37, '  1.0   ');
		$pdf->text($x+480, $y+37, '  0.0   ');
		#row 4 text
		$pdf->text($x+2, $y+50, 'Remarks  '); 
		$pdf->text($x+95, $y+50, '  Excellent   ');
		$pdf->text($x+165, $y+50, '  Very Good  ');
		$pdf->text($x+265, $y+50, '  Good   ');
		$pdf->text($x+320, $y+50, '  Satisfactory   ');
		$pdf->text($x+390, $y+50, '  Marginal Fail   ');
		$pdf->text($x+455, $y+50, '  Absolute Fail   ');
		#delete template file
		$qdel = "DELETE FROM tempmajorsubject";
		$result = mysql_query($qdel);
	 #output file
	 $filename = ereg_replace("[[:space:]]+", "",$progname);
	 $pdf->output($filename.'.pdf');
	 }
}//end of print pdf
?>
<?php  
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# include menu
	include('billingMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Financial Reports';
	$szSubSection = 'Debtors Report';
	$szTitle = 'Creating Debtors Report';
	include('billingheader.php');

$editFormAction = $_SERVER['PHP_SELF'];

mysql_select_db($database_zalongwa, $zalongwa);
$query_studentlist = "SELECT RegNo, Name, ProgrammeofStudy FROM student ORDER BY ProgrammeofStudy  ASC";
$studentlist = mysql_query($query_studentlist, $zalongwa) or die(mysql_error());
$row_studentlist = mysql_fetch_assoc($studentlist);
$totalRows_studentlist = mysql_num_rows($studentlist);

mysql_select_db($database_zalongwa, $zalongwa);
$query_degree = "SELECT ProgrammeCode, ProgrammeName FROM programme ORDER BY ProgrammeName ASC";
$degree = mysql_query($query_degree, $zalongwa) or die(mysql_error());
$row_degree = mysql_fetch_assoc($degree);
$totalRows_degree = mysql_num_rows($degree);

mysql_select_db($database_zalongwa, $zalongwa);
$query_ayear = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$ayear = mysql_query($query_ayear, $zalongwa) or die(mysql_error());
$row_ayear = mysql_fetch_assoc($ayear);
$totalRows_ayear = mysql_num_rows($ayear);

mysql_select_db($database_zalongwa, $zalongwa);
$query_dept = "SELECT Faculty, DeptName FROM department ORDER BY DeptName, Faculty ASC";
$dept = mysql_query($query_dept, $zalongwa) or die(mysql_error());
$row_dept = mysql_fetch_assoc($dept);
$totalRows_dept = mysql_num_rows($dept);

mysql_select_db($database_zalongwa, $zalongwa);
$query_comb = "SELECT SubjectID, SubjectName FROM subjectcombination ORDER BY SubjectName ASC";
$comb = mysql_query($query_comb, $zalongwa) or die(mysql_error());
$row_comb = mysql_fetch_assoc($comb);
$totalRows_comb = mysql_num_rows($comb);

?>
<?php 
			
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
?>
<style type="text/css">
<!--
.style1 {color: #FFFFFF}
-->
</style>

<h4 align="center"><?php  echo $name_row['Name']?><br>
					  
Annual Examination GPA Report <br>

<?php  

@$checkdegree = $_POST['checkdegree'];
@$checkyear = $_POST['checkyear'];
@$checkdept = $_POST['checkdept'];
$checkcohot = $_POST['checkcohot'];


$prog=$_POST['degree'];
$cohotyear = $_POST['cohot'];
$ayear = $_POST['ayear'];
$qprog= "SELECT ProgrammeCode, Title FROM programme WHERE ProgrammeCode='$prog'";
$dbprog = mysql_query($qprog);
$row_prog = mysql_fetch_array($dbprog);
$progname = $row_prog['Title'];
$qyear= "SELECT AYear FROM academicyear WHERE AYear='$cohotyear'";
$dbyear = mysql_query($qyear);
$row_year = mysql_fetch_array($dbyear);
$year = $row_year['AYear'];

$deg=$_POST['degree'];
$year = $_POST['ayear'];
$cohot = $_POST['cohot'];
$dept = $_POST['dept'];

//calculate year of study
$entry = intval(substr($cohot,0,4));
$current = intval(substr($year,0,4));
$yearofstudy=$current-$entry;

if($yearofstudy==0){
	$class="FIRST YEAR";
	}elseif($yearofstudy==1){
	$class="SECOND YEAR";
	}elseif($yearofstudy==2){
	$class="THIRDD YEAR";
	}elseif($yearofstudy==3){
	$class="FOURTH YEAR";
	}elseif($yearofstudy==4){
	$class="FIFTH YEAR";
	}elseif($yearofstudy==5){
	$class="SIXTH YEAR";
	}elseif($yearofstudy==6){
	$class="SEVENTHND YEAR";
	}else{
	$class="";
}

echo $progname;
echo " - ".$class;
echo "<br><br>Examination Results for Academic Year - ".$ayear;
?>
<br>
</h4>
<?php 

//$reg = $_POST['regno'];

$c=0;

if (($checkdegree=='on') && ($checkcohot == 'on') && ($checkdept == 'on') && ($checkyear == 'on')){
//query student list

$deg=$_POST['degree'];
$year = $_POST['ayear'];
$cohot = $_POST['cohot'];
$dept = $_POST['dept'];

$qstudent = "SELECT Name, RegNo, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY Name";
$dbstudent = mysql_query($qstudent);
$totalstudent = mysql_num_rows($dbstudent);
$i=1;
	while($rowstudent = mysql_fetch_array($dbstudent)) {
			$name = $rowstudent['Name'];
			$regno = $rowstudent['RegNo'];
			$gpoints = 0;
			$gunits = 0;
			
			//query depts where the student has courses registered for
			$qdept = "SELECT DISTINCT DeptName
						FROM course
						   INNER JOIN department ON (course.Department = department.DeptName)
						   INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
						WHERE 
						   (
							  (examresult.RegNo = '$regno') AND
							  (course.Department = '$dept')
						    )";
			
			$dbdept = mysql_query($qdept);
			$dbdeptUnit = mysql_query($qdept);
			$dbdeptGrade = mysql_query($qdept);
			
			?>
			<table width="100%" height="100%" border="1" cellpadding="0" cellspacing="0">
				  <tr>
					<td scope="col"><div align="left"></div> 
					  <?php echo $i.".0" ?>
							<br>
							<span class="style1">................................</span></td>
							<?php while($rowdept = mysql_fetch_array($dbdept)) { ?>
							<td colspan="4" nowrap scope="col"><?php echo $rowdept['DeptName']; ?></td> 
							<?php } ?>
							<td><div align="center">Total</div></td>
							<td><div align="center">Total</div></td>
							<td><div align="center">Total</div></td>
							<td><div align="center">Total</div></td>
							<td scope="col">Remarks</td>
				  </tr>
				  <tr>
							<td width="15"><?php echo $name; ?> </td>
							<?php while($rowdeptUnit = mysql_fetch_array($dbdeptUnit)) { ?>
							<td><div align="center">Units</div></td>
							<td><div align="center">SGP</div></td>
							<td><div align="center">GPA</div></td>
							<td><div align="center">Grade</div></td>
							<?php }?>
							<td><div align="center">Units</div></td>
							<td><div align="center">SGP</div></td>
							<td><div align="center">GPA</div></td>
							<td><div align="center">Grade</div></td>
							<td rowspan="2"><?php $qsupp = "SELECT CourseCode FROM examresult WHERE (RegNo = '$regno') AND (Remarks = 'Fail')";
								$dbsupp = mysql_query($qsupp);
								$dbsupptotal = mysql_query($qsupp);
								$total = mysql_num_rows($dbsupptotal);
								if($total >0){
								echo "Supp: ";
								while($rowsupp = mysql_fetch_array($dbsupp)) {
								echo $rowsupp['CourseCode'].", ";
								} 
								echo "\n";
								echo "\n";
								}//else{echo "PASS";}
							?>					          <div align="left"><?php $qinc = "SELECT CourseCode FROM examresult WHERE (RegNo = '$regno') AND (Remarks = 'I')";
								$dbqinc = @mysql_query($qinc);
								$dbinctotal = mysql_query($qinc);
								$totalinc = mysql_num_rows($dbinctotal);
								if($totalinc >0){
								echo "SE: ";
								while($rowinc = @mysql_fetch_array($dbqinc)) {
								echo $rowinc['CourseCode'] .", ";
								}
								echo "\n";
								}
							?>		
                                                     </div></td>
				  </tr>
        			<tr>
					<td width="15"><?php echo $regno ?></td>
					<?php while($rowdeptGrade = mysql_fetch_array($dbdeptGrade)) { 
						$dept = $rowdeptGrade['DeptName'];
						@$updateSQL = "SELECT 
										   examresult.RegNo,
										   examresult.ExamNo,
										   examresult.CourseCode,
										   course.Units,
										   examresult.Grade,
										   examresult.AYear,
										   examresult.Remarks,
										   examresult.Status,
										   examresult.SemesterID,
										   examresult.Comment,
										   examresult.Count,
										   course.Department
							FROM course
						       INNER JOIN department ON (course.Department = department.DeptName)
						       INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
							WHERE 
										   (
											(examresult.RegNo = '$regno') AND
											(examresult.AYear = '$year') AND
											(checked=1) AND (Count=0) AND 
											(course.Department = '$dept')
										   )";
           	$result = mysql_query($updateSQL) or die("Mwanafunzi huyu hana matokeo"); 
			$query = @mysql_query($updateSQL) or die("Cannot query the database.<br>" . mysql_error());
							
			if (mysql_num_rows($query) > 0){
					$totalunit=0;
					$unittaken=0;
					$sgp=0;
					$totalsgp=0;
					$gpa=0;	
			
				while($result = mysql_fetch_array($query)) {
					$unit = $result['Units'];
					$totalunit=$totalunit+$unit;
					$ayear = $result['AYear'];
					$remarks = $result['Remarks'];
					$status = $result['Status'];
					$grade = $result['Grade'];
					$comment = $result['Comment'];	
					$count = $result['Count'];
					
					# check if this is optional course or not
					if ($count == 1){
						$sgp = 0;
					}else{
					if ($grade=='A'){
							$point=5;
							$sgp=$point*$unit;
							$totalsgp=$totalsgp+$sgp;
							$unittaken=$unittaken+$unit;
						}elseif($grade=='B+'){
							$point=4;
							$sgp=$point*$unit;
							$totalsgp=$totalsgp+$sgp;
							$unittaken=$unittaken+$unit;
						}elseif($grade=='B'){
							$point=3;
							$sgp=$point*$unit;
							$totalsgp=$totalsgp+$sgp;
							$unittaken=$unittaken+$unit;
						}elseif($grade=='C'){
							$point=2;
							$sgp=$point*$unit;
							$totalsgp=$totalsgp+$sgp;
							$unittaken=$unittaken+$unit;
						}elseif($grade=='D'){
							$point=1;
							$sgp=$point*$unit;
							$totalsgp=$totalsgp+$sgp;
							$unittaken=$unittaken+$unit;
						}elseif($grade=='E'){
							$point=0;
							$sgp=$point*$unit;
							$totalsgp=$totalsgp+$sgp;
							$unittaken=$unittaken+$unit;
						}else
							$sgp='';
				  }
					$coursecode = $result['CourseCode'];
					}?>
					<td><div align="center"><?php $gunits = $unittaken +$gunits; echo $unittaken;  ?></div></td>
					<td><div align="center"><?php $gpoints = $totalsgp + $gpoints; echo $totalsgp;  ?></div></td>
					<td><div align="center"><?php $gpa = @substr($totalsgp/$unittaken, 0,3); echo $gpa; ?></div></td>
					<td><div align="center"><?php if ($gpa  >= 4.4){
								echo 'A';
											}
							elseif ($gpa >= 3.5){
								echo 'B+';
								}
							elseif ($gpa >= 2.7){
								echo 'B';
								}
							elseif ($gpa >= 2.0){
								echo 'C';
								}
							elseif ($gpa >= 1.0){
								echo 'D';
								}
							else{
								echo 'E';
								}
					?></div></td>
					<?php }
						
					}$i=$i+1;
						?>
						    <td><div align="center"><?php echo $gunits ?> </div></td>
							<td><div align="center"><?php echo $gpoints ?> </div></td>
							<td><div align="center"><?php $ggpa = @substr($gpoints/$gunits, 0,3); echo $ggpa; ?></div></td>
							<td><div align="center"><?php if ($ggpa  >= 4.4){
								echo 'A';
											}
							elseif ($ggpa >= 3.5){
								echo 'B+';
								}
							elseif ($ggpa >= 2.7){
								echo 'B';
								}
							elseif ($ggpa >= 2.0){
								echo 'C';
								}
							elseif ($ggpa >= 1.0){
								echo 'D';
								}
							else{
								echo 'E';
								}
					?></div></td>
					<?php }
					?>
			  </tr>
</table>
			<?php }elseif (($checkdegree=='on') && ($checkyear == 'on') && ($checkcohot == 'on')){

//query student list

$deg=$_POST['degree'];
$year = $_POST['ayear'];
$cohot = $_POST['cohot'];
$dept = $_POST['dept'];

$qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY Name";
$dbstudent = mysql_query($qstudent);
$totalstudent = mysql_num_rows($dbstudent);
//initialise S/No
$i=1;
	while($rowstudent = mysql_fetch_array($dbstudent)) {
			$name = $rowstudent['Name'];
			$regno = $rowstudent['RegNo'];
			$sex = $rowstudent['Sex'];
			$gpoints = 0;
			$gunits = 0;
			
			//query depts where the student has courses registered for
			$qdept = "SELECT DISTINCT DeptName
						FROM course
						   INNER JOIN department ON (course.Department = department.DeptName)
						   INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
						WHERE 
						   (
							  (examresult.RegNo = '$regno') AND
							  (examresult.AYear = '$year')
						    ) ORDER BY DeptName ASC";
			
			$dbdept = mysql_query($qdept);
			$dbdeptUnit = mysql_query($qdept);
			$dbdeptGrade = mysql_query($qdept);
			
			?>
			<table width="100%" height="100%" border="1" cellpadding="0" cellspacing="0">
       			<tr>
				  <td width="20" rowspan="2" nowrap><?php echo $i ?></td>
				  <td width="170" height="20" rowspan="2" nowrap><?php echo $name.": <br> ".$regno ?></td>
				  <td width="13" rowspan="2" nowrap><div align="center"><?php echo $sex ?></div></td>
				<?php while($rowdept = mysql_fetch_array($dbdept)) 
				{ ?>
							<td colspan="4" nowrap scope="col"><?php echo substr($rowdept['DeptName'],0,50); ?>
						    <div align="center"></div></td> 
				<?php } ?>
							<td width="20" nowrap><div align="center">Units</div></td>
							<td width="20" nowrap><div align="center">Points</div></td>
							<td width="20" nowrap><div align="center">GPA</div></td>
							<td width="20" nowrap><div align="center">Grade</div></td>
							<td width="100" nowrap>Remarks</td>
			  </tr>
				<tr>
					<?php while($rowdeptGrade = mysql_fetch_array($dbdeptGrade)) { 
						@$dept = $rowdeptGrade['DeptName'];
						@$updateSQL = "SELECT 
										   examresult.RegNo,
										   examresult.ExamNo,
										   examresult.CourseCode,
										   course.Units,
										   examresult.Grade,
										   examresult.AYear,
										   examresult.Remarks,
										   examresult.Status,
										   examresult.SemesterID,
										   examresult.Comment,
										   examresult.Count,
										   course.Department
							FROM course
						       INNER JOIN department ON (course.Department = department.DeptName)
						       INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
							WHERE 
										   (
											(examresult.RegNo = '$regno') AND
											(examresult.AYear = '$year')  AND
											(checked=1) AND (Count=0) AND 
											(course.Department = '$dept') 
											
										   )";
									$result = mysql_query($updateSQL) or die("Mwanafunzi huyu hana matokeo"); 
									$query = @mysql_query($updateSQL) or die("Cannot query the database.<br>" . mysql_error());
													
									if (mysql_num_rows($query) > 0){
											$totalunit=0;
											$unittaken=0;
											$sgp=0;
											$totalsgp=0;
											$gpa=0;	
									
										while($result = mysql_fetch_array($query)) {
											$unit = $result['Units'];
											$totalunit=$totalunit+$unit;
											$ayear = $result['AYear'];
											$remarks = $result['Remarks'];
											$status = $result['Status'];
											$grade = $result['Grade'];
											$comment = $result['Comment'];		
											$count = $result['Count'];
											
											# check if this is optional course or not
											if ($count == 1){
												$sgp = 0;
											}else{
											if ($grade=='A'){
													$point=5;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='B+'){
													$point=4;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='B'){
													$point=3;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='C'){
													$point=2;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='D'){
													$point=1;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='E'){
													$point=0;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}else
													$sgp='';
											 }
											$coursecode = $result['CourseCode'];
											}?>
											<td width="22" nowrap><div align="center"><?php $gunits = $unittaken +$gunits; echo $unittaken;  ?>
											</div></td>
											<td width="22" nowrap><div align="center"><?php $gpoints = $totalsgp + $gpoints; echo $totalsgp;  ?>
											</div></td>
											<td width="22" nowrap><div align="center"><?php $gpa = @substr($totalsgp/$unittaken, 0,3); echo $gpa; ?>
											</div></td>
											<td width="22" nowrap> <div align="center"><?php if ($gpa  >= 4.4){
														echo 'A';
																	}
													elseif ($gpa >= 3.5){
														echo 'B+';
														}
													elseif ($gpa >= 2.7){
														echo 'B';
														}
													elseif ($gpa >= 2.0){
														echo 'C';
														}
													elseif ($gpa >= 1.0){
														echo 'D';
														}
													else{
														echo 'E';
														}
											?>					  
											</div></td>
											<?php }
						
					}$i=$i+1;
						?>
						    <td width="20" nowrap><div align="center"><?php echo $gunits ?></div></td>
							<td width="20" nowrap><div align="center"><?php echo $gpoints ?></div></td>
							<td width="20" nowrap><div align="center"><?php $ggpa = @substr($gpoints/$gunits, 0,3); echo $ggpa; ?></div></td>
							<td width="20" nowrap><div align="center"><?php if ($ggpa  >= 4.4){
								echo 'A';
											}
							elseif ($ggpa >= 3.5){
								echo 'B+';
								}
							elseif ($ggpa >= 2.7){
								echo 'B';
								}
							elseif ($ggpa >= 2.0){
								echo 'C';
								}
							elseif ($ggpa >= 1.0){
								echo 'D';
								}
							else{
								echo 'E';
								}
					?>
						      </div></td>
					<td width="100" row span="2" nowrap>
						<?php if($ggpa<2){
										$remarkggpa = "DISCO";
										echo $remarkggpa;
										}elseif($gpa<1.5){
											$remarkgpa = "DISCO";
											echo $remarkgpa;
											}else{
											//check if has existing course
										$qcourse = "SELECT CourseCode from examresult WHERE (RegNo = '$regno') AND (AYear = '$year')";
										$dbcourse = mysql_query($qcourse);
										$totalcourse = mysql_num_rows($dbcourse);
										if($totalcourse == 0){
										echo "DISCO";
										}else{
													$qsupp = "SELECT CourseCode FROM examresult 
															  WHERE (RegNo = '$regno') AND (Remarks = 'Fail') AND (AYear = '$year')";
													$dbsupp = mysql_query($qsupp);
													$dbsupptotal = mysql_query($qsupp);
													$total = mysql_num_rows($dbsupptotal);
													if(intval($total) == 0){
															echo "PASS";  
																}else{
																	   echo "Supp: ";
																	   while($rowsupp = mysql_fetch_array($dbsupp)) {
																	     echo $rowsupp['CourseCode'].", ";
																		}
															   }
													?>					         
													<?php $qinc = "SELECT CourseCode FROM examresult 
															WHERE (RegNo = '$regno') AND (Remarks = 'I') AND (AYear = '$year')";
															$dbqinc = @mysql_query($qinc);
															$dbinctotal = mysql_query($qinc);
															$totalinc = mysql_num_rows($dbinctotal);
														if($totalinc >=1){
														echo "SE: ";
															while($rowinc = @mysql_fetch_array($dbqinc)) {
															echo $rowinc['CourseCode'] .", ";
													  }
													}
												}
											}
											?>		
                  </td>
					<?php }
					?>
			  </tr>
</table>
			<?php }elseif (($checkdegree=='on') && ($checkcohot == 'on') && ($checkdept == 'on')){
//query student list

$deg=$_POST['degree'];
$year = $_POST['ayear'];
$cohot = $_POST['cohot'];
$dept = $_POST['dept'];

$qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY Name";
$dbstudent = mysql_query($qstudent);
$totalstudent = mysql_num_rows($dbstudent);
$i=1;
	while($rowstudent = mysql_fetch_array($dbstudent)) {
			$name = $rowstudent['Name'];
			$regno = $rowstudent['RegNo'];
			$sex = $rowstudent['Sex'];
			$gpoints = 0;
			$gunits = 0;
			
			//query depts where the student has courses registered for
			$qdept = "SELECT DISTINCT DeptName
						FROM course
						   INNER JOIN department ON (course.Department = department.DeptName)
						   INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
						WHERE 
						   (
							  (examresult.RegNo = '$regno') AND
							  (course.Department = '$dept')
						    )";
			
			$dbdept = mysql_query($qdept);
			$dbdeptUnit = mysql_query($qdept);
			$dbdeptGrade = mysql_query($qdept);
			
			?>
			<table width="100%" height="100%" border="1" cellpadding="0" cellspacing="0">
       			<tr>
				  <td width="20" rowspan="2" nowrap><?php echo $i ?></td>
				  <td width="170" height="20" rowspan="2" nowrap><?php echo $name.": <br> ".$regno ?></td>
				  <td width="13" rowspan="2" nowrap><div align="center"><?php echo $sex ?></div></td>
				<?php while($rowdept = mysql_fetch_array($dbdept)) 
				{ ?>
							<td colspan="4" nowrap scope="col"><?php echo substr($rowdept['DeptName'],0,50); ?>
						    <div align="center"></div></td> 
				<?php } ?>
							<td width="20" nowrap><div align="center">Units</div></td>
							<td width="20" nowrap><div align="center">Points</div></td>
							<td width="20" nowrap><div align="center">GPA</div></td>
							<td width="20" nowrap><div align="center">Grade</div></td>
							<td width="100" nowrap>Remarks</td>
			  </tr>
				<tr>
					<?php while($rowdeptGrade = mysql_fetch_array($dbdeptGrade)) { 
						@$dept = $rowdeptGrade['DeptName'];
						@$updateSQL = "SELECT 
										   examresult.RegNo,
										   examresult.ExamNo,
										   examresult.CourseCode,
										   course.Units,
										   examresult.Grade,
										   examresult.AYear,
										   examresult.Remarks,
										   examresult.Status,
										   examresult.SemesterID,
										   examresult.Comment,
										   examresult.Count,
										   course.Department
							FROM course
						       INNER JOIN department ON (course.Department = department.DeptName)
						       INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
							WHERE 
										   (
											(examresult.RegNo = '$regno') AND
											(checked=1) AND (Count=0) AND 
											(course.Department = '$dept') 
											
										   )";
									$result = mysql_query($updateSQL) or die("Mwanafunzi huyu hana matokeo"); 
									$query = @mysql_query($updateSQL) or die("Cannot query the database.<br>" . mysql_error());
													
									if (mysql_num_rows($query) > 0){
											$totalunit=0;
											$unittaken=0;
											$sgp=0;
											$totalsgp=0;
											$gpa=0;	
									
										while($result = mysql_fetch_array($query)) {
											$unit = $result['Units'];
											$totalunit=$totalunit+$unit;
											$ayear = $result['AYear'];
											$remarks = $result['Remarks'];
											$status = $result['Status'];
											$grade = $result['Grade'];
											$comment = $result['Comment'];
											$count = $result['Count'];
											
											# check if this is optional course or not
											if ($count == 1){
												$sgp = 0;
											}else{
											if ($grade=='A'){
													$point=5;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='B+'){
													$point=4;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='B'){
													$point=3;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='C'){
													$point=2;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='D'){
													$point=1;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='E'){
													$point=0;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}else
													$sgp='';
											 }
											$coursecode = $result['CourseCode'];
											}?>
											<td width="22" nowrap><div align="center"><?php $gunits = $unittaken +$gunits; echo $unittaken;  ?>
											</div></td>
											<td width="22" nowrap><div align="center"><?php $gpoints = $totalsgp + $gpoints; echo $totalsgp;  ?>
											</div></td>
											<td width="22" nowrap><div align="center"><?php $gpa = @substr($totalsgp/$unittaken, 0,3); echo $gpa; ?>
											</div></td>
											<td width="22" nowrap> <div align="center"><?php if ($gpa  >= 4.4){
														echo 'A';
																	}
													elseif ($gpa >= 3.5){
														echo 'B+';
														}
													elseif ($gpa >= 2.7){
														echo 'B';
														}
													elseif ($gpa >= 2.0){
														echo 'C';
														}
													elseif ($gpa >= 1.0){
														echo 'D';
														}
													else{
														echo 'E';
														}
											?>					  
											</div></td>
											<?php }
						
					}$i=$i+1;
						?>
						    <td width="20" nowrap><div align="center"><?php echo $gunits ?></div></td>
							<td width="20" nowrap><div align="center"><?php echo $gpoints ?></div></td>
							<td width="20" nowrap><div align="center"><?php $ggpa = @substr($gpoints/$gunits, 0,3); echo $ggpa; ?></div></td>
							<td width="20" nowrap><div align="center"><?php if ($ggpa  >= 4.4){
								echo 'A';
											}
							elseif ($ggpa >= 3.5){
								echo 'B+';
								}
							elseif ($ggpa >= 2.7){
								echo 'B';
								}
							elseif ($ggpa >= 2.0){
								echo 'C';
								}
							elseif ($ggpa >= 1.0){
								echo 'D';
								}
							else{
								echo 'E';
								}
					?>
						      </div></td>
					<td width="100" nowrap>
						<?php $qsupp = "SELECT CourseCode FROM examresult WHERE (RegNo = '$regno') AND (Remarks = 'Fail')";
								$dbsupp = mysql_query($qsupp);
								$dbsupptotal = mysql_query($qsupp);
								$total = mysql_num_rows($dbsupptotal);
								if($total >0){
									echo "Supp: ";
									while($rowsupp = mysql_fetch_array($dbsupp)) {
									echo $rowsupp['CourseCode'].", ";
									} 
								//echo "\n";
								
								}
								/*
								elseif ($ggpa >= 2.0) {
										echo "PASS";
										echo "<br>";
								}else {echo "DISCO.";}
*/
							?>					         <?php $qinc = "SELECT CourseCode FROM examresult WHERE (RegNo = '$regno') AND (Remarks = 'I')";
								$dbqinc = @mysql_query($qinc);
								$dbinctotal = mysql_query($qinc);
								$totalinc = mysql_num_rows($dbinctotal);
								if($totalinc >0){
								echo "SE: ";
								while($rowinc = @mysql_fetch_array($dbqinc)) {
								echo $rowinc['CourseCode'] .", ";
								}
								//echo "\n";
								}
							?>		
                  </td>
					<?php }
					?>
			  </tr>
</table>
			<?php }elseif (($checkdegree=='on') && ($checkcohot == 'on')){				
//query student list

$deg=$_POST['degree'];
$year = $_POST['ayear'];
$cohot = $_POST['cohot'];
$dept = $_POST['dept'];

$qstudent = "SELECT Name, RegNo, Sex, ProgrammeofStudy FROM student WHERE (ProgrammeofStudy = '$deg') AND (EntryYear = '$cohot') ORDER BY Name";
$dbstudent = mysql_query($qstudent);
$totalstudent = mysql_num_rows($dbstudent);
$i=1;
	while($rowstudent = mysql_fetch_array($dbstudent)) {
			$name = $rowstudent['Name'];
			$regno = $rowstudent['RegNo'];
			$sex = $rowstudent['Sex'];
			$gpoints = 0;
			$gunits = 0;
			
			//query depts where the student has courses registered for
			$qdept = "SELECT DISTINCT AYear
						FROM examresult
					 WHERE 
						   (
							  (examresult.RegNo = '$regno')
						    ) 
							ORDER BY AYear ASC";
			
			$dbdept = mysql_query($qdept);
			$dbdeptUnit = mysql_query($qdept);
			$dbdeptGrade = mysql_query($qdept);
			
			?>
			<table width="100%" height="100%" border="1" cellpadding="0" cellspacing="0">
       			<tr>
				  <td width="20" rowspan="2" nowrap><?php echo $i ?></td>
				  <td width="170" height="20" rowspan="2" nowrap><?php echo $name.": <br> ".$regno ?></td>
				  <td width="13" rowspan="2" nowrap><div align="center"><?php echo $sex ?></div></td>
				<?php while($rowdept = mysql_fetch_array($dbdept)) 
				{ ?>
							<td colspan="4" nowrap scope="col"><?php echo substr($rowdept['AYear'],0,50); ?>
						    <div align="center"></div></td> 
				<?php } ?>
							<td width="20" nowrap><div align="center">Units</div></td>
							<td width="20" nowrap><div align="center">Points</div></td>
							<td width="20" nowrap><div align="center">GPA</div></td>
							<td width="20" nowrap><div align="center">Grade</div></td>
							<td width="100" nowrap>Remarks</td>
			  </tr>
				<tr>
					<?php while($rowdeptGrade = mysql_fetch_array($dbdeptGrade)) { 
						@$dept = $rowdeptGrade['AYear'];
						@$updateSQL = "SELECT 
										   examresult.RegNo,
										   examresult.ExamNo,
										   examresult.CourseCode,
										   course.Units,
										   examresult.Grade,
										   examresult.AYear,
										   examresult.Remarks,
										   examresult.Status,
										   examresult.SemesterID,
										   examresult.Comment,
										   examresult.Count,
										   course.Department
							FROM course
						       INNER JOIN department ON (course.Department = department.DeptName)
						       INNER JOIN examresult ON (course.CourseCode = examresult.CourseCode)
							WHERE 
										   (
											(examresult.RegNo = '$regno') AND
											(examresult.AYear = '$dept')  AND
											(checked=1) AND (Count=0) 											
										   )";
									$result = mysql_query($updateSQL) or die("Mwanafunzi huyu hana matokeo"); 
									$query = @mysql_query($updateSQL) or die("Cannot query the database.<br>" . mysql_error());
													
									if (mysql_num_rows($query) > 0){
											$totalunit=0;
											$unittaken=0;
											$sgp=0;
											$totalsgp=0;
											$gpa=0;	
									
										while($result = mysql_fetch_array($query)) {
											$unit = $result['Units'];
											$totalunit=$totalunit+$unit;
											$ayear = $result['AYear'];
											$remarks = $result['Remarks'];
											$status = $result['Status'];
											$grade = $result['Grade'];
											$comment = $result['Comment'];	
											$count = $result['Count'];
											
											# check if this is optional course or not
											if ($count == 1){
												$sgp = 0;
											}else{
											if ($grade=='A'){
													$point=5;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='B+'){
													$point=4;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='B'){
													$point=3;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='C'){
													$point=2;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='D'){
													$point=1;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}elseif($grade=='E'){
													$point=0;
													$sgp=$point*$unit;
													$totalsgp=$totalsgp+$sgp;
													$unittaken=$unittaken+$unit;
												}else
													$sgp='';
											 }
											$coursecode = $result['CourseCode'];
											}?>
											<td width="22" nowrap><div align="center"><?php $gunits = $unittaken +$gunits; echo $unittaken;  ?>
											</div></td>
											<td width="22" nowrap><div align="center"><?php $gpoints = $totalsgp + $gpoints; echo $totalsgp;  ?>
											</div></td>
											<td width="22" nowrap><div align="center"><?php $gpa = @substr($totalsgp/$unittaken, 0,3); echo $gpa; ?>
											</div></td>
											<td width="22" nowrap> <div align="center"><?php if ($gpa  >= 4.4){
														echo 'A';
																	}
													elseif ($gpa >= 3.5){
														echo 'B+';
														}
													elseif ($gpa >= 2.7){
														echo 'B';
														}
													elseif ($gpa >= 2.0){
														echo 'C';
														}
													elseif ($gpa >= 1.0){
														echo 'D';
														}
													else{
														echo 'E';
														}
											?>					  
											</div></td>
											<?php }
						
					}$i=$i+1;
						?>
						    <td width="20" nowrap><div align="center"><?php echo $gunits ?></div></td>
							<td width="20" nowrap><div align="center"><?php echo $gpoints ?></div></td>
							<td width="20" nowrap><div align="center"><?php $ggpa = @substr($gpoints/$gunits, 0,3); echo $ggpa; ?></div></td>
							<td width="20" nowrap><div align="center"><?php if ($ggpa  >= 4.4){
								echo 'A';
											}
							elseif ($ggpa >= 3.5){
								echo 'B+';
								}
							elseif ($ggpa >= 2.7){
								echo 'B';
								}
							elseif ($ggpa >= 2.0){
								echo 'C';
								}
							elseif ($ggpa >= 1.0){
								echo 'D';
								}
							else{
								echo 'E';
								}
					?>
						      </div></td>
					<td width="100" nowrap>
						<?php $qsupp = "SELECT CourseCode FROM examresult WHERE (RegNo = '$regno') AND (Remarks = 'Fail')";
								$dbsupp = mysql_query($qsupp);
								$dbsupptotal = mysql_query($qsupp);
								$total = mysql_num_rows($dbsupptotal);
								if($total >0){
									echo "Supp: ";
									while($rowsupp = mysql_fetch_array($dbsupp)) {
									echo $rowsupp['CourseCode'].", ";
									} 
								//echo "\n";
								
								}
								/*
								elseif ($ggpa >= 2.0) {
										echo "PASS";
										echo "<br>";
								}else {echo "DISCO.";}
*/
							?>					         <?php $qinc = "SELECT CourseCode FROM examresult WHERE (RegNo = '$regno') AND (Remarks = 'I')";
								$dbqinc = @mysql_query($qinc);
								$dbinctotal = mysql_query($qinc);
								$totalinc = mysql_num_rows($dbinctotal);
								if($totalinc >0){
								echo "SE: ";
								while($rowinc = @mysql_fetch_array($dbqinc)) {
								echo $rowinc['CourseCode'] .", ";
								}
								//echo "\n";
								}
							?>		
                  </td>
					<?php }
					?>
			  </tr>
</table>
			<?php }
}else{
?>

<form name="form1" method="post" action="<?php  echo $editFormAction ?>">
            <div align="center">
			<table width="200" border="0">
                <tr>
                  <td colspan="3"><span class="style61">if you want to filter the results by  criteria <span class="style34">Tick the corresponding check box first</span> then select appropriately </span></td>
                </tr>
                <tr>
                  <td nowrap><input name="checkdegree" type="checkbox" id="checkdegree" value="on"></td>
                  <td nowrap><div align="left">Degree Programme:</div></td>
                  <td>
                      <div align="left">
                        <select name="degree" id="degree">
                          <?php 
do {  
?>
                          <option value="<?php  echo $row_degree['ProgrammeCode']?>"><?php  echo $row_degree['ProgrammeName']?></option>
                          <?php 
} while ($row_degree = mysql_fetch_assoc($degree));
  $rows = mysql_num_rows($degree);
  if($rows > 0) {
      mysql_data_seek($degree, 0);
	  $row_degree = mysql_fetch_assoc($degree);
  }
?>
                        </select>
                    </div></td></tr>
                <tr>
                  <td><input name="checkcohot" type="checkbox" id="checkcohot" value="on"></td>
                  <td nowrap><div align="left">Cohort of the  Year: </div></td>
                  <td><div align="left">
                    <select name="cohot" id="cohot">
                        <?php 
do {  
?>
                        <option value="<?php  echo $row_ayear['AYear']?>"><?php  echo $row_ayear['AYear']?></option>
                        <?php 
} while ($row_ayear = mysql_fetch_assoc($ayear));
  $rows = mysql_num_rows($ayear);
  if($rows > 0) {
      mysql_data_seek($ayear, 0);
	  $row_ayear = mysql_fetch_assoc($ayear);
  }
?>
                    </select>
                  </div></td>
                </tr>
            	<tr>
                  <td><input name="checkyear" type="checkbox" id="checkyear" value="on"></td>
                  <td nowrap><div align="left">Results of the  Year: </div></td>
                  <td><div align="left">
                    <select name="ayear" id="ayear">
                        <?php 
do {  
?>
                        <option value="<?php  echo $row_ayear['AYear']?>"><?php  echo $row_ayear['AYear']?></option>
                        <?php 
} while ($row_ayear = mysql_fetch_assoc($ayear));
  $rows = mysql_num_rows($ayear);
  if($rows > 0) {
      mysql_data_seek($ayear, 0);
	  $row_ayear = mysql_fetch_assoc($ayear);
  }
?>
                    </select>
                  </div></td>
                </tr>
            	<tr>
                  <td><input name="checkcomb" type="checkbox" id="checkcomb" value="on"></td>
                  <td nowrap><div align="left">Subject Combination: 
                  </div></td>
                  <td><div align="left">
                    <select name="combination" id="combination">
                        <?php 
do {  
?>
                        <option value="<?php  echo $row_comb['SubjectID']?>"><?php  echo $row_comb['SubjectName']?></option>
                        <?php 
} while ($row_comb = mysql_fetch_assoc($comb));
  $rows = mysql_num_rows($comb);
  if($rows > 0) {
      mysql_data_seek($comb, 0);
	  $row_comb = mysql_fetch_assoc($comb);
  }
?>
                    </select>
                  </div></td>
                </tr>
            	<tr>
                  <td></td>
                  <td nowrap><div align="left">Create Final Year Report: </div></td>
                  <td><input name="checkfinalyear" type="checkbox" id="checkfinalyear" value="on"></td>
                </tr>
                <tr>
                  <td colspan="2"><input name="action" type="submit" id="action" value="PreView"></td>
          		  <td nowrap><input type="submit" name="PDF"  id="PDF" value="Print PDF"></td>
                </tr>
              </table>
              <input name="MM_update" type="hidden" id="MM_update" value="form1">       
  </div>
</form>
<?php 
}
include('../footer/footer.php');
?>
