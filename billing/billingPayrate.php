<?php 
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('billingMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Policy Setup';
	$szSubSection = 'Billing Rates';
	$szTitle = 'Fees Structure';
	include('billingheader.php');
	
	global $key, $y, $spon, $yos, $prog;
?>
<?php
$currentPage = $_SERVER["PHP_SELF"];
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmInst")) {
$rawcode = $_POST['txtAmount'];
$code = ereg_replace("[[:space:]]+", " ",$rawcode);
$ayear = $_POST['cmbAyear'];
$str = $_POST['cmbStr'];
$sponsor = $_POST['txtSponsor'];

#check if FeeCode exist
$sql ="SELECT feecode FROM feesrates WHERE (feecode = '$code' AND ayear = '$ayear' and sponsor='$sponsor')";

$result = mysql_query($sql);
$feecodeFound = mysql_num_rows($result);
if ($feecodeFound) {
          $feefound   = mysql_result($result,0,'Paytype');
			print " This FeeCode: '".$feefound."' Do Exists!!"; 
			exit;
}else{
	   $insertSQL = sprintf("INSERT INTO feesrates (ayear, feecode, programmecode, sponsor, amount, debtorlimit,yearofstudy)
					VALUES (%s, %s, %s, %s, %s, %s, %s)",
				   GetSQLValueString($_POST['cmbAyear'], "text"),
				   GetSQLValueString($_POST['cmbCode'], "text"),
				   GetSQLValueString($_POST['cmbStr'], "text"),
				   GetSQLValueString($_POST['txtSponsor'], "text"),
				   GetSQLValueString($_POST['txtAmount'], "text"),
				   GetSQLValueString($_POST['txtDebtorlimit'], "text"),
				   GetSQLValueString($_POST['YearofStudy'], "text"));


  mysql_select_db($database_zalongwa, $zalongwa);
  $Result1 = mysql_query($insertSQL, $zalongwa) or die(mysql_error());
  }
}
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmInstEdit")) {
		$year = $_POST['YearofStudy'];
		$amnt = $_POST['txtAmount'];
		$dblt = $_POST['txtDebtorlimit'];
		$spns = $_POST['txtSponsor'];
		$ayear = $_POST['cmbAyear'];
		$pcode = $_POST['cmbStr'];
		$fcode = $_POST['cmbCode'];		
		
		$updateSQL = "UPDATE feesrates SET amount='$amnt', debtorlimit='$dblt' WHERE sponsor='$spns' and 
					ayear='$ayear' and programmecode='$pcode' and feecode='$fcode' and yearofstudy='$year'";
		/*
		   $updateSQL = sprintf("UPDATE feesrates SET yearofstudy=%s, amount=%s, debtorlimit=%s  
					WHERE sponsor=%s and ayear=%s and programmecode=%s and feecode=%s",
				   GetSQLValueString($_POST['YearofStudy'], "text"),
				   GetSQLValueString($_POST['txtAmount'], "text"),
				   GetSQLValueString($_POST['txtDebtorlimit'], "text"),
				   GetSQLValueString($_POST['txtSponsor'], "text"),
				   GetSQLValueString($_POST['cmbAyear'], "text"),
				   GetSQLValueString($_POST['cmbStr'], "text"),
				   GetSQLValueString($_POST['cmbCode'], "text"));
		*/
  mysql_select_db($database_zalongwa, $zalongwa);
  $Result1 = mysql_query($updateSQL, $zalongwa);
 }
 
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
//control the display table
@$new=2;

mysql_select_db($database_zalongwa, $zalongwa);
$query_campus = "SELECT AYear FROM academicyear ORDER BY AYear DESC";
$campus = mysql_query($query_campus, $zalongwa);
$row_campus = mysql_fetch_assoc($campus);
$totalRows_campus = mysql_num_rows($campus);

mysql_select_db($database_zalongwa, $zalongwa);
$query_paytype = "SELECT id, feecode, name FROM fees ORDER BY Name ASC";
$paytype = mysql_query($query_paytype, $zalongwa);
$row_paytype = mysql_fetch_assoc($paytype);
$totalRows_paytype = mysql_num_rows($paytype);

mysql_select_db($database_zalongwa, $zalongwa);
$query_structure = "SELECT * FROM programme ORDER BY ProgrammeName ASC";
$structure = mysql_query($query_structure, $zalongwa);
$row_structure = mysql_fetch_assoc($structure);
$totalRows_structure = mysql_num_rows($structure);

#populate sponsor combobox
$query_sponsor = "SELECT * FROM sponsors ORDER BY Name ASC";
$sponsor = mysql_query($query_sponsor, $zalongwa);
$row_sponsor = mysql_fetch_assoc($sponsor);
$totalRows_sponsor = mysql_num_rows($sponsor);	

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$maxRows_inst = 40;
$pageNum_inst = 0;
if (isset($_GET['pageNum_inst'])) {
  $pageNum_inst = $_GET['pageNum_inst'];
}
$startRow_inst = $pageNum_inst * $maxRows_inst;

mysql_select_db($database_zalongwa, $zalongwa);
if (isset($_GET['course'])) {
  $key=$_GET['course'];
  $query_inst = "SELECT * FROM feesrates WHERE feecode Like '%$key%' ORDER BY feecode ASC";
}else{
$query_inst = "SELECT * FROM feesrates ORDER BY feecode ASC";
}
//$query_inst = "SELECT * FROM course ORDER BY CourseCode ASC";
$query_limit_inst = sprintf("%s LIMIT %d, %d", $query_inst, $startRow_inst, $maxRows_inst);
$inst = mysql_query($query_limit_inst, $zalongwa);
$row_inst = mysql_fetch_assoc($inst);

if (isset($_GET['totalRows_inst'])) {
  $totalRows_inst = $_GET['totalRows_inst'];
} else {
  $all_inst = mysql_query($query_inst);
  $totalRows_inst = mysql_num_rows($all_inst);
}
$totalPages_inst = ceil($totalRows_inst/$maxRows_inst)-1;

$queryString_inst = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_inst") == false && 
        stristr($param, "totalRows_inst") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_inst = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_inst = sprintf("&totalRows_inst=%d%s", $totalRows_inst, $queryString_inst);
 
?>
<style type="text/css">
<!--
.style1 {color: #000000}
.style2 {color: #FFFFFF}
-->
</style>


<p><?php echo "<a href=\"billingPayrate.php?new=1\">"?>Add New Payment Rate</p>
<?php @$new=$_GET['new'];
echo "</a>";
if (@$new<>1){
?>
<form name="form1" method="get" action="billingPayrate.php">
              Search by Pay Type
                <input name="course" type="text" id="course" maxlength="50">
              <input type="submit" name="Submit" value="Search">
</form>
	   
<table border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#CCCCCC"><strong>Academic Year </strong></td>
	<td bgcolor="#CCCCCC"><strong>Fee Stucture</strong></td>
	<td bgcolor="#CCCCCC"><strong>Sponsorship </strong></td>
    <td bgcolor="#CCCCCC"><strong>Fee Category </strong></td>
	<td bgcolor="#CCCCCC"><strong>Amount</strong></td>
	<td bgcolor="#CCCCCC"><strong>Debtorlimit</strong></td>
	<td bgcolor="#CCCCCC"><strong>Year</strong></td>
  </tr>
  <?php do { ?>
  <tr>
    <td nowrap><?php $y=$row_inst['ayear'];echo $y; ?>	</td>
	<?php
		mysql_select_db($database_zalongwa, $zalongwa);
		$id = $row_inst['programmecode'];
		$qsname = "SELECT ProgrammeName FROM programme WHERE ProgrammeCode ='$id'";
		$sname = mysql_query($qsname, $zalongwa);
		$row_sname = mysql_fetch_assoc($sname);
		$sname = $row_sname['ProgrammeName']; 
	?>
	<td nowrap><?php echo $sname ?>	</td>
	<td nowrap><?php $spon = $row_inst['sponsor']; echo $spon?>	</td>
    <td nowrap><?php $pid = $row_inst['feecode']; 
		mysql_select_db($database_zalongwa, $zalongwa);
		$qtitle = "SELECT name FROM fees WHERE feecode ='$pid'";
		$title = mysql_query($qtitle, $zalongwa);
		$row_title = mysql_fetch_assoc($title);
		$name = $row_title['name'];
		$fy = $row_inst['yearofstudy'];
		echo "<a href=\"billingPayrate.php?pageNum_inst=$pageNum_inst&edit=$pid&y=$y&prog=$id&sponsor=$spon&yos=$fy\">$pid = $name</a>"?></td>
	 <td><?php echo number_format($row_inst['amount'],2,'.',',') ?></td>
	 <td><?php echo number_format($row_inst['debtorlimit'],2,'.',',') ?></td>
	  <td><?php echo $fy;//echo $row_inst['yearofstudy'] ?></td>
  </tr>
  <?php } while ($row_inst = mysql_fetch_assoc($inst)); ?>
</table>
<a href="<?php printf("%s?pageNum_inst=%d%s", $currentPage, max(0, $pageNum_inst - 1), $queryString_inst); ?>">Previous</a><span class="style1"><span class="style2">......</span><?php echo min($startRow_inst + $maxRows_inst, $totalRows_inst) ?>/<?php echo $totalRows_inst ?> <span class="style1"></span><span class="style2">..........</span></span><a href="<?php printf("%s?pageNum_inst=%d%s", $currentPage, min($totalPages_inst, $pageNum_inst + 1), $queryString_inst); ?>">Next</a><br>
    			
<?php }else{?>
<form action="<?php echo $editFormAction; ?>" method="POST" name="frmInst" id="frmInst">
  <table width="200" border="1" cellpadding="0" cellspacing="0" bordercolor="#006600">
    <tr bgcolor="#CCCCCC">
      <th nowrap scope="row"><div align="right">Academic Year:</div></th>
<td><select name="cmbAyear" id="cmbAyear" title="<?php echo $row_campus['AYear']; ?>">
  <?php
do {  
?>
  <option value="<?php echo $row_campus['AYear']?>"><?php echo $row_campus['AYear']?></option>
  <?php
} while ($row_campus = mysql_fetch_assoc($campus));
  $rows = mysql_num_rows($campus);
  if($rows > 0) {
      mysql_data_seek($campus, 0);
	  $row_campus = mysql_fetch_assoc($campus);
  }
?>
      </select></td>
    </tr>
    <tr bgcolor="#CCCCCC">
      <th scope="row"><div align="right">Fee Structure:</div></th>
      <td><select name="cmbStr" id="cmbStr" title="<?php //echo $row_structure['ProgrammeName']; ?>">
        <?php
do {  
?>
        <option value="<?php echo $row_structure['ProgrammeCode']?>"><?php echo $row_structure['ProgrammeName']?></option>
        <?php
} while ($row_structure = mysql_fetch_assoc($structure));
  $rows = mysql_num_rows($structure);
  if($rows > 0) {
      mysql_data_seek($structure, 0);
	  $row_structure = mysql_fetch_assoc($structure);
  }
?>
      </select></td>
    </tr>
    <tr bgcolor="#CCCCCC">
      <th scope="row"><div align="right">Fee Code:</div></th>
      <td><select name="cmbCode" id="cmbCode" title="<?php echo $row_paytype['name']; ?>">
        <?php
do {  
?>
        <option value="<?php echo $row_paytype['feecode']?>"><?php echo $row_paytype['name']?></option>
        <?php
} while ($row_paytype = mysql_fetch_assoc($paytype));
  $rows = mysql_num_rows($paytype);
  if($rows > 0) {
      mysql_data_seek($paytype, 0);
	  $row_paytype = mysql_fetch_assoc($paytype);
  }
?>
      </select></td>
    </tr>
	    <tr bgcolor="#CCCCCC">
      <th scope="row"><div align="right">Sponsorship:</div></th>
      <td><select name="txtSponsor" id="txtSponsor" title="<?php echo $row_sponsor['Name']; ?>">
		  <option value="<?php echo $row_paytype['sponsor']?>"><?php echo $row_paytype['sponsor']?></option>
            <?php
do {  
?>
            <option value="<?php echo $row_sponsor['Name']?>"><?php echo $row_sponsor['Name']?></option>
            <?php
} while ($row_sponsor = mysql_fetch_assoc($sponsor));
  $rows = mysql_num_rows($sponsor);
  if($rows > 0) {
      mysql_data_seek($sponsor, 0);
	  $row_sponsor = mysql_fetch_assoc($sponsor);
  }
?>
</select></td>
</tr>
	    <tr bgcolor="#CCCCCC">
      <th scope="row"><div align="right">Year of Study:</div></th>
	<td><select name="YearofStudy" id="YearofStudy">
        <option value="1">First Year</option>
        <option value="2">Second Year</option>
        <option value="3">Third Year</option>
        <option value="4">Fourth Year</option>
        <option value="5">Fifth Year</option>
      </select>      </td>    
	  </tr>    
	  <tr bgcolor="#CCCCCC">
      <th nowrap scope="row"><div align="right">Amount:</div></th>
      <td><input name="txtAmount" type="text" id="txtAmount" size="25"></td>
    </tr>
    <tr bgcolor="#CCCCCC">
      <th nowrap scope="row"><div align="right">Debtorlimit:</div></th>
      <td><input name="txtDebtorlimit" type="text" id="txtDebtorlimit" size="25"></td>
    </tr>
    <tr bgcolor="#CCCCCC">
    </tr>
    <tr bgcolor="#CCCCCC">
      <th scope="row">&nbsp;</th>
      <td><div align="center">
        <input type="submit" name="Submit" value="Add Record">
      </div></td>
    </tr>
  </table>
    <input type="hidden" name="MM_insert" value="frmInst">
</form>
<?php } 
if (isset($_GET['edit'])){
#get post variables
$key = $_GET['edit'];
$y = $_GET['y'];
$spon = $_GET['sponsor'];
$prog = $_GET['prog'];
$yos = $_GET['yos'];

mysql_select_db($database_zalongwa, $zalongwa);
$query_instEdit = "SELECT * FROM feesrates WHERE feecode ='$key' and ayear='$y' and sponsor ='$spon' 
					and programmecode ='$prog' and yearofstudy='$yos'";
$instEdit = mysql_query($query_instEdit, $zalongwa);
$row_instEdit = mysql_fetch_assoc($instEdit);
$totalRows_instEdit = mysql_num_rows($instEdit);

$queryString_inst = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_inst") == false && 
        stristr($param, "totalRows_inst") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_inst = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_inst = sprintf("&totalRows_inst=%d%s", $totalRows_inst, $queryString_inst);

?>
<form action="<?php echo $editFormAction; ?>" method="POST" name="frmInstEdit" id="frmInstEdit">
 <table width="200" border="1" cellpadding="0" cellspacing="0" bordercolor="#006600">
    <tr bgcolor="#CCCCCC">
      <th nowrap scope="row"><div align="right">Academic Year:</div></th>
<td><input type="hidden" name="cmbAyear" id="cmbAyear"value="<?php echo $y;?>"><?php echo $row_instEdit['ayear'];?></td>
    </tr>
    <tr bgcolor="#CCCCCC">
      <th scope="row"><div align="right">Fee Structure:</div></th>
<td><input type="hidden" name="cmbStr" id="cmbStr" value="<?php echo $prog;?>"><?php echo $row_instEdit['programmecode'];?></td>
    </tr>
    <tr bgcolor="#CCCCCC">
      <th scope="row"><div align="right">Fee Code:</div></th>
<td><input type="hidden" name="cmbCode" id="cmbCode" value="<?php echo $key;?>"><?php echo $row_instEdit['feecode'];?></td>
    </tr>
	    <tr bgcolor="#CCCCCC">
      <th scope="row"><div align="right">Sponsorship:</div></th>
<td><input type="hidden" name="txtSponsor" id="txtSponsor" value="<?php echo $spon;?>">
    <?php echo $row_instEdit['sponsor'];//echo $row_instEdit['sponsor']?></td>
    </tr>
	    <tr bgcolor="#CCCCCC">
      <th scope="row"><div align="right">Year of Study:</div></th>
		<td>
		<?php 
		switch($yos){
			case 1:
				$yr = "First Year";
				break;
			case 2:
				$yr = "Second Year";
				break;
			case 3:
				$yr = "Third Year";
				break;
			case 4:
				$yr = "Fourth Year";
				break;
			case 5:
				$yr = "Fifth Year";
				break;			
				default;
				}
				
		?>
		<input type="hidden" name="YearofStudy" id="YearofStudy" value="<?php echo $yos;?>"><?php echo $yr;?>
		<!--
			<select name="YearofStudy" id="YearofStudy">
			<option value="<?php echo $yos;?>"><?php echo $row_instEdit['yearofstudy'];?></option>
			<option value="1">First Year</option>
			<option value="2">Second Year</option>
			<option value="3">Third Year</option>
			<option value="4">Fourth Year</option>
			<option value="5">Fifth Year</option>
			</select>
		-->
		</td>    
	  </tr>    
    <tr bgcolor="#CCCCCC">
    </tr>
	<tr bgcolor="#CCCCCC">
      <th nowrap scope="row"><div align="right">Amount:</div></th>
      <td><input name="txtAmount" type="text" id="txtAmount" value="<?php echo $row_instEdit['amount']; ?>"size="25"></td>
	</tr>
    <tr bgcolor="#CCCCCC">
      <th nowrap scope="row"><div align="right">Debtorlimit:</div></th>
      <td><input name="txtDebtorlimit" type="text" id="txtDebtorlimit" value="<?php echo $row_instEdit['debtorlimit']; ?>"size="25"></td>
    </tr>
    <tr bgcolor="#CCCCCC">
      <th scope="row"><input name="id" type="hidden" id="id" value="<?php echo $key ?>"></th>
      <td><div align="center">
        <input type="submit" name="Submit" value="Edit Record">
      </div></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="frmInstEdit">
</form>
<?php
}
	# include the footer
	include("../footer/footer.php");

@mysql_free_result($inst);

@mysql_free_result($instEdit);

@mysql_free_result($faculty);

@mysql_free_result($campus);
?>
