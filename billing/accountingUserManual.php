<?php
#get connected to the database and verfy current session
	require_once('../Connections/sessioncontrol.php');
    require_once('../Connections/zalongwa.php');
	
	# initialise globals
	include('billingMenu.php');
	
	# include the header
	global $szSection, $szSubSection;
	$szSection = 'Help';
	$szSubSection = 'User Manual';
	$szTitle = 'Accounting Module User Guide';
	include("billingheader.php");
?>
<h1><a name="_Toc125001729">Module 2: Financial Accounting Module </a></h1>
<p>This module has six (6) modules namely: Profile, Policy Setup, Financial Reports, Communication, Security, and Sign Out</p>
<p>In this module, all the functionalities are implemented by the Policy Setup and Financial Reports Menus which are now described:</p>
<h2><a name="_Toc125001730">Policy Setup Menu </a></h2>
<p>This menu contains, �Payment Rates� sub-menu. Payment Rates allows a user to set, edit, and view annual payment rates. </p>
<p>&#149;&nbsp; To add new payment rate, Click �Add New Payment Rate�. </p>
<p>&#149;&nbsp; To edit payment rate, Click �Pay Type� of the records </p>
<strong></strong>
<h2><a name="_Toc125001731">Financial Report Menu </a></h2>
<p> &#149;&nbsp; To generate a report of one candidate, use Search Candidate </p>
<p>&#149;&nbsp; To generate Tuition fee report selects �Entry Year� the click Print Report.</p>
<h2><a name="_Toc125001732">Recording Payments </a></h2>
<p>To record Tuition Fee, Caution Fee, Penalty Charge, Refund; see click the respective command, then Search the candidate.<strong>